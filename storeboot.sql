-- phpMyAdmin SQL Dump
-- version 4.4.1.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 21, 2015 at 09:21 PM
-- Server version: 5.6.24-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `storeboot`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotline_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotline_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotline_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotline_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `address`, `email`, `hotline_1`, `hotline_2`, `hotline_3`, `hotline_4`, `fax`) VALUES
(1, 'admin', '6/1/16 DT743 ,Đồng an 3 ,Bình Hòa ,Thuận An, Bình Dương.', 'support@gmail.com', '', '', '0933.149.990', '0168.258.6825', '');

-- --------------------------------------------------------

--
-- Table structure for table `ads_banner`
--

CREATE TABLE IF NOT EXISTS `ads_banner` (
  `id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `title` varchar(512) NOT NULL,
  `link` varchar(1024) NOT NULL,
  `target` varchar(64) NOT NULL DEFAULT '_self',
  `module_name` varchar(100) NOT NULL,
  `module_id` int(11) NOT NULL DEFAULT '0',
  `position` varchar(100) NOT NULL,
  `image` varchar(1024) NOT NULL,
  `height` int(11) NOT NULL,
  `width` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `attach`
--

CREATE TABLE IF NOT EXISTS `attach` (
  `id` int(11) NOT NULL,
  `idModel` int(11) NOT NULL DEFAULT '0',
  `nameModel` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `location` varchar(1024) DEFAULT NULL,
  `idAdmin` int(11) DEFAULT NULL,
  `bannerName` varchar(255) DEFAULT NULL,
  `logoName` varchar(255) DEFAULT NULL,
  `loai` int(3) DEFAULT NULL COMMENT '3: Right , 1: Mid , 2: Main',
  `status` int(3) NOT NULL DEFAULT '1',
  `sort_order` tinyint(3) DEFAULT NULL,
  `slug` varchar(1024) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=123 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attach`
--

INSERT INTO `attach` (`id`, `idModel`, `nameModel`, `name`, `location`, `idAdmin`, `bannerName`, `logoName`, `loai`, `status`, `sort_order`, `slug`) VALUES
(1, 0, 'banner', 'banner_c52f1bd6.png', 'http://vps3d11.vdrs.net/themes/upload/source/baone5fa7a50f-6961-4e07-9b9c-d068c082c1c8.jpg', 2, 'Banner 1', '', 3, 2, 2, 'vien-uong-giam-can'),
(2, 0, 'banner', '13.png', 'http://vps3d11.vdrs.net/themes/upload/source/baone5fa7a50f-6961-4e07-9b9c-d068c082c1c8.jpg', 2, 'Banner 2', '', 3, 2, 4, ''),
(3, 0, 'logo', 'logo_1657949_web.jpg', '1433019382_logo_1657949_web.jpg', 69, '', 'Logo', 0, 2, 1, 'http__thienduongkhoedep-com'),
(5, 0, 'banner', '12.png', 'http://vps3d11.vdrs.net/themes/upload/source/baone5fa7a50f-6961-4e07-9b9c-d068c082c1c8.jpg', 2, 'Banner khoe dep', 'Logo Website', 3, 2, 3, ''),
(40, 4, 'product', '1433319701_Untitled.jpg', 'upload/images/images/2015/06/03/1433319701_Untitled.jpg', 39, '', '', 0, 1, 0, ''),
(41, 4, 'product', '1433319758_Untitled.jpg', 'upload/images/images/2015/06/03/1433319758_Untitled.jpg', 39, '', '', 0, 1, 0, ''),
(42, 5, 'product', '1433319804_1433318336_Untitled.jpg', 'upload/images/images/2015/06/03/1433319804_1433318336_Untitled.jpg', 39, '', '', 0, 1, 0, ''),
(43, 6, 'product', '1433319833_1433318627_Untitled.jpg', 'upload/images/images/2015/06/03/1433319833_1433318627_Untitled.jpg', 39, '', '', 0, 1, 0, ''),
(44, 7, 'product', '1433319858_Untitled.jpg', 'upload/images/images/2015/06/03/1433319858_Untitled.jpg', 39, '', '', 0, 1, 0, ''),
(53, 0, 'logo', 'logo.png', 'http://vps3d11.vdrs.net/themes/upload/source/logo%20(1).png', 2, '', 'logo', 1, 1, 6, ''),
(57, 1, 'posts', '1434184989_download.jpg', 'upload/images/images/2015/06/13/1434184989_download.jpg', 69, '', '', 0, 1, 0, ''),
(58, 1, 'posts', '1434198421_tra.ok.jpg', 'upload/images/images/2015/06/13/1434198421_tra.ok.jpg', 69, '', '', 0, 1, 0, ''),
(59, 1, 'posts', '1434198456_sup.kk.jpg', 'upload/images/images/2015/06/13/1434198456_sup.kk.jpg', 69, '', '', 0, 1, 0, ''),
(60, 1, 'posts', '1434198486_sieu thi.jpg', 'upload/images/images/2015/06/13/1434198486_sieu thi.jpg', 69, '', '', 0, 1, 0, ''),
(61, 1, 'posts', '1434198525_stress.ok.jpg', 'upload/images/images/2015/06/13/1434198525_stress.ok.jpg', 69, '', '', 0, 1, 0, ''),
(62, 3, 'posts', '1434198705_hop thuoc tim.png', 'upload/images/images/2015/06/13/1434198705_hop thuoc tim.png', 69, '', '', 0, 1, 0, ''),
(64, 4, 'posts', '1434807849_gym.jpg', 'upload/images/images/2015/06/20/1434807849_gym.jpg', 69, '', '', 0, 1, 0, ''),
(65, 4, 'posts', '1434807893_ngoi thang lung.jpg', 'upload/images/images/2015/06/20/1434807893_ngoi thang lung.jpg', 69, '', '', 0, 1, 0, ''),
(66, 4, 'posts', '1434807897_ngoi thang lung.jpg', 'upload/images/images/2015/06/20/1434807897_ngoi thang lung.jpg', 69, '', '', 0, 1, 0, ''),
(67, 4, 'posts', '1434807908_van dong.jpg', 'upload/images/images/2015/06/20/1434807908_van dong.jpg', 69, '', '', 0, 1, 0, ''),
(68, 4, 'posts', '1434807961_LIC.jpg', 'upload/images/images/2015/06/20/1434807961_LIC.jpg', 69, '', '', 0, 1, 0, ''),
(69, 4, 'posts', '1434808074_lic.jpg', 'upload/images/images/2015/06/20/1434808074_lic.jpg', 69, '', '', 0, 1, 0, ''),
(98, 10, 'posts', '1436500057_311_1.jpg', 'upload/images/images/2015/07/10/1436500057_311_1.jpg', 2, '', '', 0, 1, 0, ''),
(99, 10, 'posts', '1436500057_2010.jpg', 'upload/images/images/2015/07/10/1436500057_2010.jpg', 2, '', '', 0, 1, 0, ''),
(100, 10, 'posts', '1436500057_2051_2041.jpg', 'upload/images/images/2015/07/10/1436500057_2051_2041.jpg', 2, '', '', 0, 1, 0, ''),
(102, 0, 'banner', '', '', 2, 'Demo banner Right', '', 3, 2, 7, ''),
(111, 12, 'product', '1437290556_IMG20150315192135.jpg', 'upload/images/images/2015/07/19/1437290556_IMG20150315192135.jpg', 71, '', '', 0, 1, 0, ''),
(112, 12, 'product', '1437290569_IMG20150425200152.jpg', 'upload/images/images/2015/07/19/1437290569_IMG20150425200152.jpg', 71, '', '', 0, 1, 0, ''),
(113, 12, 'product', '1437290583_IMG20150430022033.jpg', 'upload/images/images/2015/07/19/1437290583_IMG20150430022033.jpg', 71, '', '', 0, 1, 0, ''),
(114, 11, 'product', '1437291168_10614255_1472535003031605_1187555099707080186_n.jpg', 'upload/images/images/2015/07/19/1437291168_10614255_1472535003031605_1187555099707080186_n.jpg', 71, '', '', 0, 1, 0, ''),
(115, 11, 'product', '1437291182_Hika_1.jpg', 'upload/images/images/2015/07/19/1437291182_Hika_1.jpg', 71, '', '', 0, 1, 0, ''),
(116, 0, 'banner', 'game1170x70.gif', 'http://vps3d11.vdrs.net/themes/upload/source/tin-tuc/xu-huong-thoi-trang-thu-dong-cua-Eichitoo13.jpg', 2, 'mid', '', 1, 1, 1, ''),
(117, 0, 'banner', '2.png', '1437454410_2.png', 71, 'test', '', 3, 2, 8, ''),
(118, 0, 'banner', 'banner_c9e1074f.png', 'http://vps3d11.vdrs.net/themes/upload/source/baone5fa7a50f-6961-4e07-9b9c-d068c082c1c8.jpg', 2, 'banner3', '', 3, 2, 9, ''),
(119, 0, 'banner', 'seagate-52E4J1.png', 'http://vps3d11.vdrs.net/themes/upload/source/banner/5f0f9-cl3.png', 2, 'Event giáng sinh ấm áp', '', 2, 1, 10, ''),
(120, 0, 'banner', NULL, 'http://vps3d11.vdrs.net/themes/upload/source/banner/63e04-5a348-Banner-giang-sinh.png', 2, 'Event giáng sinh khuyến mãi lớn', '', 2, 1, 11, ''),
(121, 0, 'banner', NULL, 'http://vps3d11.vdrs.net/themes/upload/source/banner/63321-666-banner-870x360.jpg', 2, 'Elara', '', 2, 1, 12, ''),
(122, 0, 'banner', NULL, 'http://vps3d11.vdrs.net/themes/upload/source/tin-tuc/giay%20nu.jpg', 2, 'mid2', '', 1, 1, 13, '');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `show` tinyint(3) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: Bài viết, 2: Sản phẩm',
  `forwc` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `image_home` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_page` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(512) COLLATE utf8_unicode_ci NOT NULL DEFAULT ' ',
  `num_row_product_show_home` int(11) NOT NULL DEFAULT '0' COMMENT '0: Xem nhu show so luong default',
  `have_slide` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: Khong , 1: Có',
  `num_product_show_home` int(11) NOT NULL DEFAULT '0' COMMENT '0: Xem nhu show so luong default',
  `detail_slug` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `parent`, `order`, `show`, `status`, `type`, `forwc`, `create_date`, `modify_date`, `image_home`, `image_page`, `description`, `icon`, `num_row_product_show_home`, `have_slide`, `num_product_show_home`, `detail_slug`) VALUES
(12, 'Giày Nam', 'giay-nam', 0, 0, 1, 1, 2, 1, '2015-12-18 15:05:00', '2015-12-18 15:05:00', '', '', 'Mua giày nam đẹp về mẫu mã, bền về chất lượng không phải dễ dàng. mua giày nam đẹp thật vừa ý mà giá lại phải chăng tại thành phố hồ chí minh.', ' ', 0, 0, 0, 'elara.vn/giay-nam'),
(13, 'Giày Nữ', 'giay-nu', 0, 0, 1, 1, 2, 1, '2015-12-18 15:06:00', '2015-12-18 15:06:00', '', '', 'Giày dép nữ 2015 giá rẻ, chất lượng tốt, uy tín tại Thành Phố Hồ Chí Minh. Mua online ngay giày thời trang nữ giá cực cool, hàng cực chất, chính hãng.', ' ', 0, 0, 0, 'elara.vn/giay-nu'),
(14, 'Giày Thể Thao - Sneaker', 'giay-the-thao-sneaker', 0, 0, 0, 1, 2, 1, '2015-12-18 15:08:00', '2015-12-18 15:08:00', '', '', 'Giày Sneaker Đế Cao - Giày Thể Thao. Mua online giày thể thao - sneaker nam nữ chất lượng, giá cực tốt, giao hàng tận nơi.', ' ', 0, 0, 0, 'elara.vn/giay-the-thao-sneaker'),
(15, 'Giày Đi Chơi - Casual', 'giay-di-choi-casual', 0, 0, 0, 1, 2, 1, '2015-12-18 15:10:00', '2015-12-18 15:10:00', '', '', 'Giày Giày Đi Chơi - Casual. Mua online giày đi chơi - casual nam nữ chất lượng, giá cực tốt, giao hàng tận nơi.', ' ', 0, 0, 0, 'elara.vn/giay-di-choi-casual'),
(16, 'Giày Lười - Mọi', 'giay-luoi-giay-moi', 0, 0, 1, 1, 2, 1, '2015-12-18 15:11:00', '2015-12-20 13:08:00', '', '', 'Giày Lười - giày Mọi. Mua online giày thể thao - sneaker nam nữ uy tín, chất lượng, giá cực tốt, giao hàng tận nơi.', ' ', 0, 0, 0, 'elara.vn/giay-luoi-giay-moi'),
(17, 'Giày Vải', 'giay-vai', 0, 0, 1, 1, 2, 1, '2015-12-18 15:14:00', '2015-12-18 15:14:00', '', '', 'Giày vải 2015. Mua online giày vải thời trang nam nữ giá rẻ, uy tín chất lượng, giao hàng tận nơi.', ' ', 0, 0, 0, 'elara.vn/giay-vai'),
(18, 'Giày Bata', 'giay-bata', 0, 0, 1, 1, 2, 1, '2015-12-18 15:15:00', '2015-12-18 15:15:00', '', '', 'Giày bata. Mua online giày bata thời trang nam nữ uy tín, chất lượng, chính hãng giá rẻ nhất giao hàng miễn phí.', ' ', 0, 0, 0, 'elara.vn/giay-bata'),
(19, 'Tin tức khuyến mãi', 'tin-tuc-khuyen-mai', 0, 0, 0, 1, 1, 1, '2015-12-19 23:39:00', '2015-12-20 20:04:00', NULL, NULL, '', ' ', 0, 0, 0, NULL),
(20, 'Xu hướng thời trang', 'xu-huong-thoi-trang', 0, 0, 0, 1, 1, 1, '2015-12-19 23:40:00', '2015-12-20 20:04:00', NULL, NULL, '', ' ', 0, 0, 0, NULL),
(21, 'Tin tức sản phẩm', 'tin-tuc-san-pham', 0, 0, 0, 1, 1, 1, '2015-12-19 23:40:00', '2015-12-20 20:04:00', NULL, NULL, '', ' ', 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `color`
--

CREATE TABLE IF NOT EXISTS `color` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `image` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `color`
--

INSERT INTO `color` (`id`, `name`, `status`, `image`, `order`) VALUES
(1, 'Đỏ', 1, 'http://vps3d11.vdrs.net/themes/upload/source/color/images.png', 0),
(2, 'Xanh', 1, 'http://vps3d11.vdrs.net/themes/upload/source/color/2-d8c68.jpg', 0),
(3, 'Đen', 1, 'http://vps3d11.vdrs.net/themes/upload/source/color/250px-Solid_black.svg.png', 0),
(4, '233', 2, 'http://vps3d11.vdrs.net/themes/upload/source/12092198_936403776452414_2081106405_n.jpg', 0),
(5, 'Trắng', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) COLLATE utf8_bin NOT NULL,
  `path` varchar(1024) COLLATE utf8_bin NOT NULL,
  `size` int(11) NOT NULL,
  `w` int(11) NOT NULL,
  `h` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `create_by` int(11) NOT NULL,
  `update_by` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `ext` varchar(5) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13793 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `name`, `path`, `size`, `w`, `h`, `create_date`, `modify_date`, `create_by`, `update_by`, `status`, `ext`) VALUES
(13787, '10984796_915165225170910_892242273_n', '/2015/04/08/10984796_915165225170910_892242273_n.jpg', 114658, 0, 0, '2015-04-08 10:45:23', '2015-04-08 10:45:23', 39, 39, 1, 'jpg'),
(13788, '10984796_915165225170910_892242273_n', '/2015/04/08/10984796_915165225170910_892242273_n-1.jpg', 114658, 0, 0, '2015-04-08 10:51:47', '2015-04-08 10:51:47', 39, 39, 1, 'jpg'),
(13791, '10984796_915165225170910_892242273_n', '/2015/04/08/10984796_915165225170910_892242273_n-2.jpg', 114658, 0, 0, '2015-04-08 10:54:35', '2015-04-08 10:54:35', 39, 39, 1, 'jpg'),
(13792, 'cropped-headerdemo', '/2015/04/08/cropped-headerdemo.jpg', 80621, 0, 0, '2015-04-08 10:58:18', '2015-04-08 10:58:18', 39, 39, 1, 'jpg');

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE IF NOT EXISTS `info` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotline_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotline_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotline_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hotline_4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `username`, `address`, `email`, `hotline_1`, `hotline_2`, `hotline_3`, `hotline_4`, `fax`) VALUES
(1, 'admin', '245 Âu Cơ, Phường 9, Quận Tân Bình, TPHCM', 'thienduongkhoedep@thienduongkhoedep.com', '0902 144 565 - Tư vấn : 0934 143 565', '0902 144 565', '0902 144 565', 'Kinh doanh: 0902 144 565 - Tư vấn : 0934 143 565', '');

-- --------------------------------------------------------

--
-- Table structure for table `judge`
--

CREATE TABLE IF NOT EXISTS `judge` (
  `product_id` int(11) NOT NULL,
  `point` int(2) NOT NULL DEFAULT '1' COMMENT '1: Like -1 Khong like',
  `mem_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `judge`
--

INSERT INTO `judge` (`product_id`, `point`, `mem_id`) VALUES
(4, 1, 2),
(4, 1, 1),
(16, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) CHARACTER SET utf8 NOT NULL,
  `ip` varchar(40) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `browser` varchar(1024) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=468 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `name`, `ip`, `user_id`, `create_date`, `browser`) VALUES
(1, 'tuyen bui: Login', '::1', 39, '2015-05-16 13:12:49', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(2, 'tuyen bui: Login', '::1', 39, '2015-05-19 10:05:08', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(3, 'tuyen bui: Login', '::1', 39, '2015-05-19 10:08:03', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(4, 'tuyen bui: Thêm: 12312', '::1', 39, '2015-05-19 13:51:28', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(5, 'tuyen bui: Thêm: 12312', '::1', 39, '2015-05-19 13:51:38', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(6, 'tuyen bui: Thêm: 12312', '::1', 39, '2015-05-19 13:52:33', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(7, 'tuyen bui: Sửa: 12312', '::1', 39, '2015-05-19 13:53:06', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(8, 'tuyen bui: Thêm: 12412', '::1', 39, '2015-05-19 13:53:17', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(9, 'tuyen bui: Logout', '::1', 39, '2015-05-19 14:11:28', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(10, 'tuyen bui: Login', '::1', 39, '2015-05-19 14:11:29', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(11, 'tuyen bui: Thêm: 13', '::1', 39, '2015-05-19 14:21:19', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(12, 'tuyen bui: Thêm: 13', '::1', 39, '2015-05-19 14:21:23', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(13, 'tuyen bui: Thêm: 13', '::1', 39, '2015-05-19 14:22:37', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(14, 'tuyen bui: Thêm: 13', '::1', 39, '2015-05-19 14:22:47', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(15, 'tuyen bui: Thêm: 123', '::1', 39, '2015-05-19 14:23:54', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(16, 'tuyen bui: Thêm: 123', '::1', 39, '2015-05-19 14:31:59', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(17, 'tuyen bui: Thêm: 241', '::1', 39, '2015-05-19 14:32:40', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(18, 'tuyen bui: Thêm: 241', '::1', 39, '2015-05-19 14:35:15', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(19, 'tuyen bui: Thêm: 241', '::1', 39, '2015-05-19 14:35:34', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(20, 'tuyen bui: Thêm: 241', '::1', 39, '2015-05-19 14:39:32', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(21, 'tuyen bui: Thêm: 241', '::1', 39, '2015-05-19 14:39:59', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(22, 'tuyen bui: Sửa: 241', '::1', 39, '2015-05-19 14:40:53', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(23, 'tuyen bui: Sửa: 241', '::1', 39, '2015-05-19 14:41:03', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(24, 'tuyen bui: Sửa: 241', '::1', 39, '2015-05-19 14:41:47', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(25, 'tuyen bui: Sửa: 241', '::1', 39, '2015-05-19 14:42:29', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(26, 'tuyen bui: Sửa: 241', '::1', 39, '2015-05-19 14:43:21', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(27, 'tuyen bui: Thêm: 312', '::1', 39, '2015-05-19 14:43:30', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(28, 'tuyen bui: Login', '::1', 39, '2015-05-20 18:06:51', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(29, 'tuyen bui: Thêm: 123', '::1', 39, '2015-05-20 18:56:46', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(30, 'tuyen bui: Sửa: 123', '::1', 39, '2015-05-20 18:57:23', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(31, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-20 18:58:47', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(32, 'tuyen bui: Xóa: 1', '::1', 39, '2015-05-20 19:52:21', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(33, 'tuyen bui: Xóa: 1', '::1', 39, '2015-05-20 19:52:24', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(34, 'tuyen bui: Xóa: 1', '::1', 39, '2015-05-20 19:52:25', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(35, 'tuyen bui: Sửa: ', '::1', 39, '2015-05-20 19:52:36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(36, 'tuyen bui: Xóa: 1', '::1', 39, '2015-05-20 19:52:42', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(37, 'tuyen bui: Xóa: 1', '::1', 39, '2015-05-20 19:53:13', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(38, 'tuyen bui: Xóa: 1', '::1', 39, '2015-05-20 19:53:15', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(39, 'tuyen bui: Xóa: 1', '::1', 39, '2015-05-20 19:53:15', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(40, 'tuyen bui: Xóa: 16', '::1', 39, '2015-05-20 19:53:19', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(41, 'tuyen bui: Xóa: 16', '::1', 39, '2015-05-20 19:53:53', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(42, 'tuyen bui: Xóa: 16', '::1', 39, '2015-05-20 19:53:55', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(43, 'tuyen bui: Xóa: 16', '::1', 39, '2015-05-20 19:53:58', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(44, 'tuyen bui: Xóa: 16', '::1', 39, '2015-05-20 19:54:28', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(45, 'tuyen bui: Xóa: 1', '::1', 39, '2015-05-20 19:54:32', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(46, 'tuyen bui: Xóa: 1', '::1', 39, '2015-05-20 19:55:07', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(47, 'tuyen bui: Xóa: ', '::1', 39, '2015-05-20 19:59:50', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(48, 'tuyen bui: Xóa: ', '::1', 39, '2015-05-20 19:59:56', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(49, 'tuyen bui: Xóa: 1', '::1', 39, '2015-05-20 20:01:03', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(50, 'tuyen bui: Xóa: 1', '::1', 39, '2015-05-20 20:01:05', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(51, 'tuyen bui: Xóa: ', '::1', 39, '2015-05-20 20:05:30', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(52, 'tuyen bui: Xóa: ', '::1', 39, '2015-05-20 20:05:39', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(53, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-20 20:14:48', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(54, 'tuyen bui: Sửa: ', '::1', 39, '2015-05-20 20:15:28', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(55, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-20 20:15:35', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(56, 'tuyen bui: Thêm: 123', '::1', 39, '2015-05-20 20:15:59', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(57, 'tuyen bui: Thêm: 2313', '::1', 39, '2015-05-20 20:17:04', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(58, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-20 20:18:28', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(59, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-20 20:22:26', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(60, 'tuyen bui: Sửa: ', '::1', 39, '2015-05-20 20:22:50', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(61, 'tuyen bui: Sửa: ', '::1', 39, '2015-05-20 20:23:20', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(62, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-20 20:23:24', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(63, 'tuyen bui: Login', '::1', 39, '2015-05-21 15:18:51', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(64, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 15:51:07', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(65, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 15:52:58', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(66, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 15:56:15', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(67, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 15:56:24', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(68, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 15:56:42', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(69, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 15:56:50', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(70, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:00:09', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(71, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:00:15', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(72, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:11:14', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(73, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:11:17', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(74, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:11:17', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(75, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:11:17', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(76, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:11:17', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(77, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:11:18', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(78, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:11:34', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(79, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:11:34', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(80, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:11:35', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(81, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:11:46', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(82, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:15:27', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(83, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:20:02', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(84, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:21:23', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(85, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:21:33', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(86, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:21:42', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(87, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:21:54', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(88, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:29:13', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(89, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:30:32', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(90, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:31:08', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(91, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:31:43', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(92, 'tuyen bui: Sửa: 2313', '::1', 39, '2015-05-21 16:51:04', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(93, 'tuyen bui: Login', '::1', 39, '2015-05-22 09:55:11', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(94, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:35:29', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(95, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:36:02', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(96, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:36:03', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(97, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:36:03', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(98, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:36:03', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(99, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:44:03', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(100, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:44:04', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(101, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:44:04', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(102, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:44:05', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(103, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(104, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(105, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(106, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:01', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(107, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:01', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(108, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:01', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(109, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:01', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(110, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:01', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(111, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:01', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(112, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:01', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(113, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:01', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(114, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:02', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(115, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:02', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(116, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:02', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(117, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:02', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(118, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:02', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(119, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:02', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(120, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:02', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(121, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:02', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(122, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:34', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(123, 'tuyen bui: Thêm: ', '::1', 39, '2015-05-22 13:47:39', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(124, 'tuyen bui: Login', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(125, 'tuyen bui: Login', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(126, 'tuyen bui: Login', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(127, 'tuyen bui: Login', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(128, 'tuyen bui: Login', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(129, 'tuyen bui: Login', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(130, 'tuyen bui: Xóa: ', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(131, 'tuyen bui: Xóa: ', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(132, 'tuyen bui: Login', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(133, 'tuyen bui: Login', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(134, 'tuyen bui: Login', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(135, 'tuyen bui: Login', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(136, 'tuyen bui: Login', '::1', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(137, 'tuyen bui: Login', '171.250.249.91', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(138, 'tuyen bui: Add/edit user: Gia Bảo', '171.250.249.91', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(139, 'tuyen bui: Login', '171.249.48.25', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(140, 'tuyen bui: Login', '171.249.48.25', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(141, 'tuyen bui: Login', '171.250.249.91', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(142, 'tuyen bui: Login', '171.250.249.91', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(143, 'tuyen bui: Login', '171.250.249.91', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(144, 'tuyen bui: Login', '171.249.48.25', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(145, 'tuyen bui: Login', '171.250.249.91', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(146, 'tuyen bui: Login', '171.250.249.91', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(147, 'tuyen bui: Login', '171.250.249.91', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(148, 'tuyen bui: Login', '171.250.249.91', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(149, 'tuyen bui: Login', '171.249.48.25', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(150, 'tuyen bui: Login', '171.250.249.91', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(151, 'tuyen bui: Login', '116.100.4.92', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(152, 'tuyen bui: Login', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(153, 'tuyen bui: Login', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(154, 'tuyen bui: Login', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(155, 'tuyen bui: Login', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(156, 'tuyen bui: Login', '171.249.81.228', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(157, 'tuyen bui: Login', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(158, 'tuyen bui: Add/edit user: Thiên đường khỏe đẹp', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(159, 'tuyen bui: Add/edit user: Thiên đường khỏe đẹp', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(160, 'tuyen bui: Add/edit user: Thiên đường khỏe đẹp', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(161, 'tuyen bui: Logout', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(162, 'tuyen bui: Login', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(163, 'tuyen bui: Login', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(164, 'tuyen bui: Add/edit user: Thiên đường khỏe đẹp', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(165, 'tuyen bui: Add/edit user: Thiên đường khỏe đẹp', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(166, 'tuyen bui: Add/edit user: 123', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(167, 'tuyen bui: Add/edit user: Thiên đường khỏe đẹp', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(168, 'tuyen bui: Logout', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(169, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(170, 'tuyen bui: Add/edit user: 123', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(171, 'tuyen bui: Add/edit user: 123', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(172, 'tuyen bui: Logout', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(173, 'tuyen bui: Login', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(174, 'tuyen bui: Logout', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(175, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(176, 'Thiên đường khỏe đẹp: Add/edit user: 123123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(177, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(178, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(179, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(180, 'tuyen bui: Login', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(181, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(182, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(183, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(184, 'tuyen bui: Logout', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(185, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(186, 'Thiên đường khỏe đẹp: Logout', '14.161.3.240', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(187, 'Thiên đường khỏe đẹp: Logout', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(188, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(189, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(190, 'Thiên đường khỏe đẹp: Logout', '14.161.3.240', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(191, 'tuyen bui: Login', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(192, 'tuyen bui: Logout', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(193, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(194, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(195, 'Thiên đường khỏe đẹp: Logout', '14.161.3.240', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(196, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(197, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(198, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(199, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(200, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(201, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(202, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(203, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(204, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(205, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(206, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(207, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(208, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(209, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(210, 'Thiên đường khỏe đẹp: Add/edit user: 123', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(211, 'Thiên đường khỏe đẹp: Add/edit user: 1234', '115.79.59.190', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(212, 'Thiên đường khỏe đẹp: Logout', '14.161.3.240', 53, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(213, 'tuyen bui: Login', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(214, 'tuyen bui: Add/edit user: Thiên đường khỏe đẹp', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(215, 'tuyen bui: Logout', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(216, 'tuyen bui: Login', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(217, 'tuyen bui: Logout', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(218, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(219, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(220, 'tuyen bui: Login', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(221, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(222, 'tuyen bui: Login', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(223, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(224, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(225, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(226, 'tuyen bui: Login', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(227, 'tuyen bui: Login', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(228, 'Thiên đường khỏe đẹp: Login', '171.249.48.251', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(229, 'Thiên đường khỏe đẹp: Login', '1.52.126.80', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(230, 'Thiên đường khỏe đẹp: Login', '171.249.69.109', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(231, 'Thiên đường khỏe đẹp: Login', '171.249.69.109', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(232, 'Thiên đường khỏe đẹp: Login', '171.232.121.248', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(233, 'Thiên đường khỏe đẹp: Đổi mật khẩu', '171.232.121.248', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(234, 'Thiên đường khỏe đẹp: Đổi mật khẩu', '171.232.121.248', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(235, 'Thiên đường khỏe đẹp: Login', '171.232.121.248', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(236, 'Thiên đường khỏe đẹp: Đổi mật khẩu', '171.232.121.248', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(237, 'Thiên đường khỏe đẹp: Logout', '171.232.121.248', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(238, 'Thiên đường khỏe đẹp: Login', '171.232.121.248', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(239, 'Thiên đường khỏe đẹp: Login', '171.249.69.109', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36');
INSERT INTO `logs` (`id`, `name`, `ip`, `user_id`, `create_date`, `browser`) VALUES
(240, 'tuyen bui: Login', '14.161.3.240', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(241, 'Thiên đường khỏe đẹp: Login', '171.232.121.170', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(242, 'Thiên đường khỏe đẹp: Login', '171.232.121.170', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(243, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(244, 'Thiên đường khỏe đẹp: Login', '1.53.224.34', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'),
(245, 'tuyen bui: Login', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(246, 'tuyen bui: Logout', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(247, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(248, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(249, 'Thiên đường khỏe đẹp: Login', '171.232.122.47', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(250, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(251, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(252, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(253, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(254, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(255, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(256, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(257, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(258, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(259, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(260, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(261, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(262, 'Thiên đường khỏe đẹp: Xóa: ', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(263, 'Thiên đường khỏe đẹp: Login', '171.232.120.118', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(264, 'Thiên đường khỏe đẹp: Login', '1.53.224.34', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'),
(265, 'Thiên đường khỏe đẹp: Login', '115.78.132.147', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(266, 'Thiên đường khỏe đẹp: Login', '1.53.224.34', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36'),
(267, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(268, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(269, 'Thiên đường khỏe đẹp: Logout', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(270, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(271, 'Thiên đường khỏe đẹp: Logout', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(272, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.107_coc_coc Safari/537.36'),
(273, 'Thiên đường khỏe đẹp: Logout', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(274, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(275, 'Thiên đường khỏe đẹp: Login', '27.75.208.64', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(276, 'Thiên đường khỏe đẹp: Login', '171.249.69.109', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(277, 'Thiên đường khỏe đẹp: Login', '27.75.208.64', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(278, 'Thiên đường khỏe đẹp: Login', '183.80.221.89', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36'),
(279, 'Thiên đường khỏe đẹp: Login', '14.161.2.50', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.39 Safari/537.36'),
(280, 'Thiên đường khỏe đẹp: Login', '27.75.209.59', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(281, 'tuyen bui: Login', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.87_coc_coc Safari/537.36'),
(282, 'tuyen bui: Add/edit user: Gia Bảo', '115.79.59.190', 39, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.87_coc_coc Safari/537.36'),
(283, 'Thiên đường khỏe đẹp: Login', '115.78.132.147', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(284, 'Thiên đường khỏe đẹp: Login', '27.75.209.126', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(285, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(286, 'Thiên đường khỏe đẹp: Login', '115.78.132.147', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(287, 'Thiên đường khỏe đẹp: Login', '42.117.67.15', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.2; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'),
(288, 'Thiên đường khỏe đẹp: Login', '115.78.132.147', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(289, 'Thiên đường khỏe đẹp: Login', '27.75.210.123', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(290, 'Thiên đường khỏe đẹp: Login', '171.249.30.222', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(291, 'Thiên đường khỏe đẹp: Login', '115.78.132.147', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(292, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(293, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(294, 'Thiên đường khỏe đẹp: Login', '27.75.210.220', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(295, 'Thiên đường khỏe đẹp: Login', '115.78.132.147', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(296, 'Thiên đường khỏe đẹp: Login', '115.79.59.190', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(297, 'Thiên đường khỏe đẹp: Login', '115.78.132.147', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(298, 'Thiên đường khỏe đẹp: Login', '27.75.208.43', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(299, 'Thiên đường khỏe đẹp: Login', '27.75.209.158', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(300, 'Thiên đường khỏe đẹp: Login', '171.249.70.56', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(301, 'Thiên đường khỏe đẹp: Login', '115.78.132.147', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(302, 'Thiên đường khỏe đẹp: Login', '27.75.209.183', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(303, 'Thiên đường khỏe đẹp: Login', '27.75.208.67', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(304, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(305, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/48.0 Chrome/42.0.2311.152_coc_coc Safari/537.36'),
(306, 'Thiên đường khỏe đẹp: Login', '27.75.209.79', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.81 Safari/537.36'),
(307, 'Thiên đường khỏe đẹp: Login', '171.249.135.151', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(308, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(309, 'Thiên đường khỏe đẹp: Login', '14.161.3.240', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(310, 'Thiên đường khỏe đẹp: Login', '180.93.229.55', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.52 Safari/537.36'),
(311, 'Thiên đường khỏe đẹp: Login', '27.75.208.142', 69, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36'),
(312, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(313, 'Gia Bảo: Login', '::1', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(314, 'Gia Bảo: Login', '::1', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(315, 'Gia Bảo: Add/edit user: Gia Bảo', '::1', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(316, 'Gia Bảo: Login', '::1', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(317, 'Gia Bảo: Login', '::1', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(318, 'Gia Bảo: Login', '::1', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(319, 'Gia Bảo: Login', '::1', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(320, 'Gia Bảo: Login', '::1', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(321, 'Gia Bảo: Login', '171.250.248.49', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(322, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(323, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(324, 'Gia Bảo: Login', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36'),
(325, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(326, 'admin: Login', '14.176.102.181', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-TW; rv:1.9.2.10) Gecko/20100914 Firefox/3.6.10'),
(327, 'Gia Bảo: Login', '171.249.6.95', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(328, 'admin: Login', '42.119.154.63', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36'),
(329, 'admin: Login', '42.119.154.63', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'),
(330, 'admin: Login', '42.119.154.63', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:38.0) Gecko/20100101 Firefox/38.0'),
(331, 'Gia Bảo: Login', '171.250.248.26', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(332, 'Gia Bảo: Xóa: ', '171.250.248.26', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(333, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(334, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36'),
(335, 'Gia Bảo: Login', '171.250.249.237', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(336, 'admin: Login', '113.162.210.145', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.132 Safari/537.36'),
(337, 'Gia Bảo: Login', '171.250.249.237', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(338, 'Gia Bảo: Login', '171.250.249.237', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(339, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(340, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(341, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(342, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(343, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(344, 'admin: Login', '115.79.59.190', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(345, 'admin: Xóa: ', '115.79.59.190', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(346, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(347, 'Gia Bảo: Login', '171.250.249.230', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.126_coc_coc Safari/537.36'),
(348, 'Gia Bảo: Login', '171.250.249.230', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.138_coc_coc Safari/537.36'),
(349, 'Gia Bảo: Login', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(350, 'admin: Login', '115.79.59.190', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(351, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(352, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(353, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.138_coc_coc Safari/537.36'),
(354, 'Gia Bảo: Login', '171.250.252.221', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.138_coc_coc Safari/537.36'),
(355, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(356, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(357, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0'),
(358, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(359, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.144_coc_coc Safari/537.36'),
(360, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.144_coc_coc Safari/537.36'),
(361, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.144_coc_coc Safari/537.36'),
(362, 'Gia Bảo: Login', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(363, 'Gia Bảo: Login', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(364, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.144_coc_coc Safari/537.36'),
(365, 'Gia Bảo: Login', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(366, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(367, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.144 Safari/537.36'),
(368, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0'),
(369, 'admin: Login', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(370, 'admin: Xóa: ', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(371, 'admin: Xóa: ', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(372, 'admin: Xóa: ', '42.119.154.153', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(373, 'Gia Bảo: Login', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(374, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.144 Safari/537.36'),
(375, 'admin: Login', '118.69.120.247', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(376, 'Gia Bảo: Login', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(377, 'Gia Bảo: Xóa: ', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(378, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.144 Safari/537.36'),
(379, 'admin: Login', '1.54.186.155', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(380, 'Gia Bảo: Login', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(381, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.144 Safari/537.36'),
(382, 'admin: Login', '1.54.186.155', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(383, 'Gia Bảo: Login', '14.161.35.227', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(384, 'Gia Bảo: Login', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(385, 'Gia Bảo: Xóa: ', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(386, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.144 Safari/537.36'),
(387, 'admin: Login', '113.163.98.242', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36'),
(388, 'admin: Login', '42.118.20.188', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.134 Safari/537.36'),
(389, 'Gia Bảo: Login', '171.233.128.112', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0 Chrome/43.0.2357.144 Safari/537.36'),
(390, 'admin: Login', '42.118.20.188', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36'),
(391, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0.146 Chrome/43.0.2357.146 Safari/537.36'),
(392, 'Gia Bảo: Login', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36'),
(393, 'Gia Bảo: Logout', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36'),
(394, 'Gia Bảo: Login', '115.79.36.5', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36'),
(395, 'admin: Login', '113.162.209.75', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36'),
(396, 'admin: Xóa: ', '113.162.209.75', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36'),
(397, 'admin: Login', '42.119.167.155', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.107 Safari/537.36'),
(398, 'Gia Bảo: Login', '171.249.93.145', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0.146 Chrome/43.0.2357.146 Safari/537.36'),
(399, 'admin: Login', '113.22.245.177', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36'),
(400, 'Gia Bảo: Login', '118.69.62.113', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36'),
(401, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0.146 Chrome/43.0.2357.146 Safari/537.36'),
(402, 'Gia Bảo: Logout', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0.146 Chrome/43.0.2357.146 Safari/537.36'),
(403, 'seo: Login', '115.79.59.190', 74, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0.146 Chrome/43.0.2357.146 Safari/537.36'),
(404, 'Gia Bảo: Login', '14.161.35.227', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.125 Safari/537.36'),
(405, 'Gia Bảo: Login', '171.250.248.70', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/49.0.146 Chrome/43.0.2357.146 Safari/537.36'),
(406, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/50.0.125 Chrome/44.0.2403.125 Safari/537.36'),
(407, 'Gia Bảo: Login', '14.161.35.227', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/50.0.125 Chrome/44.0.2403.125 Safari/537.36'),
(408, 'Gia Bảo: Login', '115.79.59.190', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/50.0.125 Chrome/44.0.2403.125 Safari/537.36'),
(409, 'admin: Login', '113.162.208.68', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.157 CoRom/35.0.1916.157 Safari/537.36'),
(410, 'admin: Login', '113.162.208.68', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36'),
(411, 'admin: Login', '113.162.208.68', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.157 CoRom/35.0.1916.157 Safari/537.36'),
(412, 'admin: Login', '42.116.101.214', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36'),
(413, 'admin: Login', '42.118.58.114', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.85 Safari/537.36'),
(414, 'admin: Login', '42.115.143.124', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36'),
(415, 'admin: Login', '42.117.144.177', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36'),
(416, 'admin: Login', '1.52.158.83', 71, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36'),
(417, 'Gia Bảo: Login', '14.161.35.227', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/51.2.109 Chrome/45.2.2454.109 Safari/537.36'),
(418, 'Gia Bảo: Login', '14.161.35.227', 2, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/51.2.109 Chrome/45.2.2454.109 Safari/537.36'),
(419, 'Huy Phùng: Login', '115.75.208.102', 73, '0000-00-00 00:00:00', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/601.1.56 (KHTML, like Gecko) Version/9.0 Safari/601.1.56'),
(420, 'Huy Phùng: Login', '115.75.208.102', 73, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.86 Chrome/46.2.2490.86 Safari/537.36'),
(421, 'Đình Hưng: Login', '183.80.154.126', 72, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(422, 'Đình Hưng: Login', '183.80.154.126', 72, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(423, 'Đình Hưng: Xóa: ', '183.80.154.126', 72, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(424, 'Đình Hưng: Xóa: ', '183.80.154.126', 72, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(425, 'Đình Hưng: Xóa: ', '183.80.154.126', 72, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(426, 'Đình Hưng: Xóa: ', '183.80.154.126', 72, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(427, 'Đình Hưng: Xóa: ', '183.80.154.126', 72, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(428, 'Đình Hưng: Xóa: ', '183.80.154.126', 72, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'),
(429, 'Đình Hưng: Login', '113.162.211.53', 72, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.124 Safari/537.36'),
(430, 'Huy Phùng: Login', '115.75.209.191', 73, '0000-00-00 00:00:00', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(431, 'Huy Phùng: Login', '14.161.46.218', 73, '0000-00-00 00:00:00', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/601.1.56 (KHTML, like Gecko) Version/9.0 Safari/601.1.56'),
(432, 'Gia Bảo: Login', '112.78.13.114', 2, '2015-12-08 08:56:32', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(433, 'Gia Bảo: Login', '115.75.216.232', 2, '2015-12-09 21:08:09', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(434, 'Gia Bảo: Login', '112.78.13.114', 2, '2015-12-11 15:57:05', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(435, 'Gia Bảo: Login', '112.78.13.114', 2, '2015-12-12 09:51:46', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(436, 'Gia Bảo: Login', '116.118.80.176', 2, '2015-12-13 09:10:29', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(437, 'Gia Bảo: Login', '112.78.13.114', 2, '2015-12-15 10:08:23', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(438, 'Gia Bảo: Login', '115.75.216.232', 2, '2015-12-15 22:38:01', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(439, 'Gia Bảo: Login', '112.78.13.114', 2, '2015-12-16 08:14:07', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(440, 'Gia Bảo: Login', '115.75.216.232', 2, '2015-12-16 17:58:50', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(441, 'Gia Bảo: Login', '112.78.13.114', 2, '2015-12-17 09:11:11', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(442, 'Gia Bảo: Logout', '112.78.13.114', 2, '2015-12-17 09:58:56', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(443, 'Gia Bảo: Login', '180.148.136.27(172.30.119.25)', 2, '2015-12-17 09:58:59', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36'),
(444, 'Gia Bảo: Login', '112.78.13.114', 2, '2015-12-17 09:59:00', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(445, 'Gia Bảo: Login', '1.52.241.44', 2, '2015-12-17 14:36:44', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/601.1.56 (KHTML, like Gecko) Version/9.0 Safari/601.1.56'),
(446, 'Gia Bảo: Login', '171.249.6.242', 2, '2015-12-17 19:48:49', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(447, 'Gia Bảo: Login', '115.75.216.232', 2, '2015-12-18 09:54:12', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(448, 'Gia Bảo: Login', '115.75.209.113', 2, '2015-12-18 13:10:17', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(449, 'Gia Bảo: Xóa: ', '115.75.209.113', 2, '2015-12-18 18:04:49', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(450, 'Gia Bảo: Xóa: ', '115.75.209.113', 2, '2015-12-18 18:04:52', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(451, 'Gia Bảo: Xóa: ', '115.75.209.113', 2, '2015-12-18 18:32:29', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(452, 'Gia Bảo: Xóa: ', '115.75.209.113', 2, '2015-12-18 19:00:25', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(453, 'Gia Bảo: Xóa: ', '115.75.209.113', 2, '2015-12-18 19:00:28', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(454, 'Gia Bảo: Login', '171.249.6.242', 2, '2015-12-18 21:40:13', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(455, 'Gia Bảo: Login', '112.78.13.114', 2, '2015-12-19 11:28:19', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(456, 'Gia Bảo: Login', '42.118.13.90', 2, '2015-12-19 11:29:31', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/601.1.56 (KHTML, like Gecko) Version/9.0 Safari/601.1.56'),
(457, 'Gia Bảo: Xóa: ', '112.78.13.114', 2, '2015-12-19 11:33:27', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(458, 'Gia Bảo: Login', '115.75.209.113', 2, '2015-12-19 14:03:36', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(459, 'Gia Bảo: Login', '115.75.216.232', 2, '2015-12-19 17:16:01', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(460, 'Gia Bảo: Login', '171.249.6.242', 2, '2015-12-19 20:19:16', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(461, 'Gia Bảo: Login', '171.249.6.242', 2, '2015-12-20 12:48:35', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(462, 'Gia Bảo: Login', '115.75.216.232', 2, '2015-12-20 13:42:27', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(463, 'Gia Bảo: Login', '1.52.241.35', 2, '2015-12-20 21:45:42', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(464, 'Gia Bảo: Login', '1.52.241.35', 2, '2015-12-20 21:54:01', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(465, 'Gia Bảo: Login', '115.75.209.113', 2, '2015-12-21 11:04:13', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36'),
(466, 'Gia Bảo: Login', '112.78.13.114', 2, '2015-12-21 11:14:09', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/52.2.98 Chrome/46.2.2490.98 Safari/537.36'),
(467, 'Gia Bảo: Login', '1.52.241.151', 2, '2015-12-21 14:41:18', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11) AppleWebKit/601.1.56 (KHTML, like Gecko) Version/9.0 Safari/601.1.56');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE IF NOT EXISTS `member` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `phone` int(15) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sendmail_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sendmail_vong` int(11) NOT NULL DEFAULT '0',
  `set_email` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `email`, `name`, `status`, `phone`, `password`, `address`, `sendmail_date`, `sendmail_vong`, `set_email`) VALUES
(1, 'gbcntt93@gmail.com', 'trần đỗ gia bảo', 1, 1226998644, '63ee451939ed580ef3c4b6f0109d1fd0', '86 Trần hưng Đạo', '2015-12-21 11:35:57', 2, 1),
(2, 'minhtiencreater@gmail.com', 'Nguyễn Minh Tiến', 1, 933214339, '0f5e5008768ae08706def9f7d5f26f7d', '35/7/10A Tân Thuận Đông Q7', '0000-00-00 00:00:00', 0, 0),
(3, 'phunghuy28988@gmail.com', 'phung quoc huy', 1, 978831618, '63ee451939ed580ef3c4b6f0109d1fd0', 'di an, binh duong', '0000-00-00 00:00:00', 0, 0),
(4, 'gbcntt93@gmail.ccom', 'qwewqeqwe', 1, 2147483647, '63ee451939ed580ef3c4b6f0109d1fd0', '12412412412412412412', '0000-00-00 00:00:00', 0, 0),
(5, 'minhtiencreater2805@gmail.com', 'Minh Tiến', 1, 933214339, '0f5e5008768ae08706def9f7d5f26f7d', '35/7/10A Tân Thận Đông Quận 7', '0000-00-00 00:00:00', 0, 0),
(6, 'hoanglongpc1986@gmail.com', 'Longcheng', 1, 1656585298, 'c4d04edcef18736f70041b2a4a45b584', 'tphcm', '0000-00-00 00:00:00', 0, 0),
(7, 'huyphung2809@gmail.com', 'huy phung', 1, 978831618, '3c80fa3640d1831f6679dfd5ebd66ac4', 'thủ đức', '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL,
  `type` enum('category','link','videocategory') NOT NULL,
  `value` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `target` varchar(64) NOT NULL DEFAULT '_self',
  `sort_order` int(11) NOT NULL DEFAULT '0' COMMENT 'Sắp xếp menu',
  `parent` int(11) NOT NULL DEFAULT '0',
  `position` varchar(60) NOT NULL DEFAULT 'top',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `of_value` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `type`, `value`, `title`, `link`, `target`, `sort_order`, `parent`, `position`, `status`, `of_value`) VALUES
(1, 'category', '12', '', 'giay-nam', '_self', 1, 0, 'top', 1, 0),
(2, 'category', '14', '', 'giay-nam/giay-the-thao-sneaker', '_self', 1, 1, 'top', 1, 12),
(3, 'category', '15', '', 'giay-nam/giay-di-choi-casual', '_self', 2, 1, 'top', 1, 12),
(4, 'category', '16', '', 'giay-nam/giay-luoi-moi', '_self', 3, 1, 'top', 1, 12),
(5, 'category', '17', '', 'giay-nam/giay-vai', '_self', 5, 1, 'top', 1, 12),
(6, 'category', '13', '', 'giay-nu', '_self', 2, 0, 'top', 1, 0),
(7, 'category', '18', '', 'giay-nam/giay-bata', '_self', 4, 1, 'top', 1, 12),
(8, 'category', '14', '', 'giay-nu/giay-the-thao-sneaker', '_self', 1, 6, 'top', 1, 13),
(9, 'category', '15', '', 'giay-nu/giay-the-thao-sneaker', '_self', 2, 6, 'top', 1, 13),
(10, 'category', '16', '', 'giay-nu/giay-luoi-moi1', '_self', 3, 6, 'top', 1, 13),
(11, 'category', '17', '', 'giay-nu/giay-vai', '_self', 4, 6, 'top', 1, 13),
(12, 'category', '18', '', 'giay-nu/giay-bata', '_self', 5, 6, 'top', 1, 13),
(13, 'category', '14', '', 'giay-the-thao-sneaker/giay-nu', '_self', 4, 0, 'top', 1, 0),
(14, 'category', '12', '', 'giay-the-thao-sneaker/giay-nam', '_self', 2, 13, 'top', 1, 14),
(15, 'category', '13', '', 'giay-nu/giay-the-thao-sneaker', '_self', 1, 13, 'top', 1, 14),
(16, 'category', '15', '', 'giay-di-choi-casual', '_self', 3, 0, 'top', 1, 0),
(17, 'category', '12', '', 'giay-nam/giay-di-choi-casual', '_self', 2, 16, 'top', 1, 15),
(18, 'category', '12', '', 'giay-nam/giay-luoi-moi', '_self', 2, 20, 'top', 1, 16),
(19, 'category', '13', '', 'giay-nu/giay-di-choi-casual', '_self', 1, 16, 'top', 1, 15),
(20, 'category', '16', '', 'giay-luoi-moi', '_self', 5, 0, 'top', 1, 0),
(21, 'category', '13', '', 'giay-nu/giay-luoi-moi', '_self', 1, 20, 'top', 1, 16);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_date` datetime DEFAULT NULL,
  `total_price` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `mem_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`id`, `name`, `email`, `phone`, `address`, `order_date`, `total_price`, `status`, `mem_id`) VALUES
(1, '', '', '', '35/7/10A Tân Thận Đông Quận 7', '2015-12-18 09:37:42', 5805000, 0, 5),
(2, '', '', '', '86 Trần hưng Đạo', '2015-12-20 11:43:14', 200000, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE IF NOT EXISTS `order_detail` (
  `odd_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0',
  `color` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`odd_id`, `order_id`, `product_id`, `quantity`, `price`, `color`, `size`) VALUES
(21, 15, 3, 1, 850000, NULL, NULL),
(22, 15, 4, 1, 1100000, NULL, NULL),
(23, 16, 3, 2, 850000, NULL, NULL),
(24, 16, 4, 1, 1100000, NULL, NULL),
(25, 17, 12, 1, 1100000, NULL, NULL),
(26, 18, 16, 1, 11000, NULL, NULL),
(27, 19, 22, 4, 1300000, NULL, NULL),
(28, 19, 23, 1, 2200000, NULL, NULL),
(29, 1, 6, 4, 345000, NULL, NULL),
(30, 1, 13, 15, 295000, NULL, NULL),
(31, 2, 2, 1, 100000, 1, 322),
(32, 2, 8, 1, 100000, 1, 328);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `image` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `admin_note` varchar(2048) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `modify_by` int(11) NOT NULL,
  `mtitle` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdescription` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mkeyword` varchar(1024) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`, `slug`, `summary`, `content`, `status`, `image`, `created_by`, `admin_note`, `create_date`, `modify_date`, `modify_by`, `mtitle`, `mdescription`, `mkeyword`) VALUES
(4, 'Contact', '', '', '<p class="intro" style="box-sizing: border-box; margin: 0px 0px 15px; padding: 0px; border: 0px; font-family: ''Source Sans Pro''; font-size: 25px; font-weight: bold; font-stretch: inherit; line-height: 22.5px; vertical-align: baseline; color: #4a4a4a;"><input style="box-sizing: border-box; margin: 0px 0px 20px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: 37px; color: #707070; outline: none; vertical-align: top; padding-left: 7px; width: 395px; border: 1px solid #c0c0c0;" type="text" value="Your Email Address" /><textarea style="box-sizing: border-box; margin: 0px 0px 20px; font-family: inherit; font-size: 15px; font-style: inherit; font-variant: inherit; font-weight: inherit; font-stretch: inherit; line-height: inherit; color: #707070; overflow: auto; vertical-align: top; padding-left: 7px; width: 395px; border-color: #c0c0c0; resize: none; min-height: 100px;"></textarea></p>', 2, '/2014/12/15/viet-nam-vs-malaysia-1.jpg', 0, '', '2014-12-15 00:13:00', '2015-04-15 22:08:33', 0, 'Tỉ lệ kèo trực tuyến  | Ty le keo bong da truc tuyen hom nay', 'tỉ lệ kèo trực tuyến, tỉ lệ kèo bóng đá trực tuyến, tỉ lệ kèo online, tỉ lệ kèo trực tiếp, tỉ lệ bóng đá trực tuyến, tỉ lệ bóng đá.', NULL),
(5, 'Giới thiệu', 'gioi-thieu.html', '', '<p><span>Chuyên cung cấp thực phẩm chức năng làm đẹp như  thuốc giảm cân an toàn hiệu quả, mỹ phẩm cao cấp ngoại nhập chăm sóc cơ thể toàn diện cho phái đẹp. Chúng tôi luôn sẵn sàng chia sẻ cho các bạn những phương pháp làm đẹp, những cách giảm cân nhanh chóng. Để các bạn có được 1 thân hình thon gọn lý tưởng được nhiều người mong đợi.</span><br /><br /><strong>Với Phương Châm: Sự hài lòng của bạn là mục đích phục vụ của chúng tôi.</strong><br /><br /><span>Không ngừng cập nhật cũng như đổi mới các sản phẩm của mình nhằm mang lại cho quí khách sự chọn lựa hài lòng nhất. </span><br /><br /><span>Quý khách không chỉ được sử dụng những sản phẩm an toàn, hiệu quả mà còn được chúng tôi tư vấn rất nhiệt tình tận tâm và chu đáo trong suốt thời gian quí khách lựa chọn sử dụng sản phẩm của chúng tôi. </span><br /><br /><span>Đặc biệt cung cấp các dòng mỹ phẩm dưỡng da, các loại thuốc uống giảm cân, trẻ hoá, phòng chống bệnh của các nhãn hiệu nổi tiếng.</span><br /><span> </span><br /><span>Nhập khẩu từ:</span><br /><span>Pháp - Mỹ - Japan - Korea - HongKong... </span><br /><br /><span>Các dòng kem tắm trắng hiệu quả cao, an toàn khi sử dụng và không gây kích ứng da. Đã được kiểm nghiệm thực tế và sử rộng rãi tại các thẩm mỹ viện, spa...</span><br /><span> </span><br /><span>Bên cạnh đó chúng tôi còn chú trọng đến việc chăm sóc da toàn thân, giảm thiểu tối đa những  khuyết điểm trên cơ thể, nhằm trao tặng cho quí khách vẻ đẹp hài hoà và hoàn thiện từ bên trong lẫn bên ngoài. </span><br /><br /><span>Đến với chúng tôi để được giao hàng tận nơi trong phạm vi toàn quốc và quốc tế.</span></p>\n<div><strong>Hotline: 0902 144 565 - Email: thienduongkhoedep@gmail.com<br />NHẬN HÀNG NHANH CHÓNG - AN TOÀN - KÍN ĐÁO</strong></div>\n<p><br /><span>Giao hàng toàn quốc bằng chuyển phát nhanh tận nơi. Nhận hàng mới thanh toán.</span><br /><br /><span>Cam kết giao hàng cho quí khách trong vòng 24 - 36h kể từ khi giao dịch được thực hiện. Với hệ thống ngân Hàng thanh toán có chi nhánh rộng khắp các tỉnh thành trong cả nước.</span></p>', 1, '', 0, '', '2015-01-10 14:55:00', '2015-03-25 14:15:56', 0, '', '', NULL),
(6, 'Hướng dẫn mua hàng', 'huong-dan-mua-hang.html', '', '<p><strong>Khách hàng có thể mua hàng bằng 2 cách sau đây:</strong><br /><em>1. Đặt hàng qua điện thoại</em><br />Gọi điện trực tiếp đến số điện thoại 0902 144 565 để chúng tôi giải quyết đơn hành nhanh chóng nhất.<br /><em>2.  Đặt hàng online qua website</em><br /><strong>Để việc mua hàng trên website của quý khách thành công và dể dàng hãy làm theo hướng dẫn:</strong><br />Bước 1: Trong trang chi tiết sản phẩm, Quý khách click vào nút <strong>MUA NGAY</strong><br /><br />Bước 2: Sau khi kích vào nút <strong>MUA NGAY</strong> hệ thống sẽ có một bảng thông báo, trong thông báo này có 2 lựa chọn cho bạn:<br />- Kích vào nút <strong>Mua thêm sản phẩm khác</strong> nếu bạn muốn mua thêm sản phẩm<br />- Kích vào nút <strong>Tiếp tục</strong> để thanh toán đơn hàng của mình<br /><br />Bước 3:<br />- Kích vào nút <strong>Mua thêm sản phẩm khác</strong> nếu bạn muốn mua thêm sản phẩm khác từ website (lặp lại bước 1)<br />- Nếu kích vào nút <strong>Tiếp tục </strong>hệ thống sẽ đưa bạn đến trang giỏ hàng, tại đây bạn có một số lựa chọn sau:<br />+ Nếu bạn muốn mua 2 sản phẩm thay vì 1 sản phẩm ban đầu thì điền số 2 vào phần Số lượng (1) và kích vào nút Cập nhật giỏ hàng (2)<br />+ Nếu bạn muốn mua sản phẩm khác thì click vào nút <strong>Mua sản phẩm khác</strong><br /><br />Bước 4: Điền đầy đủ các thông tin vào form thông tin khách hàng để chúng tôi liên hệ lại với bạn<br /><br />Bước 5: Sau khi <strong>điền đầy đủ thông tin </strong>và <strong>mã số xác nhận</strong> rằng bạn không phải là spamer, bạn tiếp tục <strong>click </strong>vào nút <strong>Đặt hàng</strong> và vui lòng chờ trong giây lát để hệ thống xử lý đơn hàng của bạn<br />Chờ khi hệ thống đang xử lý đơn hàng<br />Nếu đơn hàng được xử lý thành công sẽ hiện ra thông báo như sau:<br /><br /><em><strong>Cảm ơn bạn đã đặt hàng, chúng tôi sẽ liên hệ với bạn trong thời gian sớm nhất</strong></em><br /><br />Bộ phận hỗ trợ khách hàng của chúng tôi sẽ liên hệ ngay với bạn sau khi nhận được đơn hàng<br /><br />3. Chính sách vận chuyển: Miễn phí toàn quốc<br />- Khách hàng ở xa: Hàng hóa được gửi nhanh thông qua nhà cung cấp dịch vụ EMS hoặc DHL .Thời gian quý khách nhận hàng trong khoảng từ 2 đến 4 ngày<br />4. Thanh  toán<br />- Nhận hàng thanh toán theo dịch vụ phát hàng COD (thanh toán khi nhận hàng tận nơi)<br />- Chuyển khoản ngân hàng theo các số tài khoản sau:</p>\n<p><strong>TÀI KHOẢN NGÂN HÀNG VIETCOMBANK:</strong></p>\n<p><em><strong>- Chủ tài khoản: Nguyễn Trần Diễm My - Số Tài khoản: 0251001790337 - Vietcombank CN TPHCM</strong></em></p>\n<p><strong>TÀI KHOẢN NGÂN HÀNG VIETINBANK:</strong></p>\n<p><em><strong>- Chủ tài khoản: Nguyễn Trần Diễm My - Số Tài khoản: 711AC0709032</strong></em></p>\n<p><span>5. Một số lưu ý</span><br /><span>- Chỉ chấp nhận những Đơn hàng đặt hàng khi cung cấp đủ thông tin về họ tên đầy đủ, địa chỉ, số điện thoại cố định của quý khách...</span><br /><br /><span>- Sau khi Quý khách đặt hàng, chúng tôi sẽ gọi điện lại để kiểm tra thông tin và thỏa thuận có liên quan.</span><br /><span>Cảm ơn quí khách.</span></p>', 2, '', 0, '', '2015-03-23 14:02:06', '2015-04-16 09:44:53', 0, '', '', NULL),
(7, 'Liên Hệ', 'lien-he.html', '', '<h4>SHOP GIÀY THỜI TRANG ELARA</h4>\n<p>Địa chỉ: 329 Nguyễn Trãi, Phường 7, Quận 5, HCM <br /><br />Hotline: 1900-6645</p>\n<p>Cám ơn quý khách đã đến với trang thông tin sản phẩm của chúng tôi, chúng tôi rất vui khi nhận được các thông tin phản hồi từ quý vị.<br />Bất cứ thắc mắc và góp ý đối với công ty xin vui lòng liên hệ với chúng tôi.</p>', 1, '', 0, '', '2015-03-25 14:18:09', '2015-03-25 14:18:09', 0, '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `title` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `image` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `public_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `cat_id`, `admin_id`, `title`, `content`, `description`, `image`, `public_date`, `status`) VALUES
(1, 21, 2, 'Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh', '<p><img src="http://channel.vcmedia.vn/thumb_w/640/prupload/164/2015/12/img20151214181350482.jpg" alt="" /></p>\n<div class="VCSortableInPreviewMode">&nbsp;</div>\n<div class="VCSortableInPreviewMode">\n<div><img title="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 2." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151214181350602.jpg" alt="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 2." /></div>\n<div class="PhotoCMS_Caption">&nbsp;</div>\n</div>\n<div class="VCSortableInPreviewMode">\n<div><img title="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 3." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151214181350700.jpg" alt="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 3." /></div>\n<div class="PhotoCMS_Caption">\n<p data-placeholder="[nhập chú thích]">Với những bạn yêu thích chất liệu vải canvas, denim sẽ có rất nhiều lựa chọn. Đầu tiên, mẫu giày banda với các logo Kappa nhỏ bên hông hay sau gót tạo điểm nhấn lạ mắt.</p>\n</div>\n</div>\n<div class="VCSortableInPreviewMode">\n<div><img title="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 4." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151214181350836.jpg" alt="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 4." /></div>\n<div class="PhotoCMS_Caption">&nbsp;</div>\n</div>\n<div class="VCSortableInPreviewMode">\n<div><img title="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 5." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151214181350959.jpg" alt="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 5." /></div>\n<div class="PhotoCMS_Caption">&nbsp;</div>\n</div>\n<div class="VCSortableInPreviewMode">\n<div><img title="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 6." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151214181351060.jpg" alt="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 6." /></div>\n<div class="PhotoCMS_Caption">\n<p data-placeholder="[nhập chú thích]">Cũng có thể lựa chọn các kiểu giày thanh lịch dáng thon gọn với chất liệu da phối da lộn, các gam màu tương phản nổi bật.</p>\n</div>\n</div>\n<div class="VCSortableInPreviewMode">\n<div><img title="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 7." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151214181351136.jpg" alt="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 7." /></div>\n<div class="PhotoCMS_Caption">\n<p data-placeholder="[nhập chú thích]">Nếu thích sự tiện lợi, bạn có thể chọn mẫu giày Slip on với bề mặt da có vân rạn nứt rất lạ và độc đáo.</p>\n</div>\n</div>\n<div class="VCSortableInPreviewMode">\n<div><img title="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 8." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151214181351226.jpg" alt="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 8." /></div>\n<div class="PhotoCMS_Caption">&nbsp;</div>\n</div>\n<div class="VCSortableInPreviewMode">\n<div><img title="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 9." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151214181351312.jpg" alt="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 9." /></div>\n<div class="PhotoCMS_Caption">&nbsp;</div>\n</div>\n<div class="VCSortableInPreviewMode">\n<div><img title="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 10." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151214181351416.jpg" alt="Các mẫu giày Kappa cho tín đồ thời trang dịp Giáng sinh - Ảnh 10." /></div>\n<div class="PhotoCMS_Caption">\n<p data-placeholder="[nhập chú thích]">Đối với các bạn gái điệu đà, trong tủ giày không thể thiếu những mẫu búp bê đáng yêu tôn vinh nét đẹp đôi chân nhỏ nhắn. Các mẫu giày từ cột dây đến xỏ chân với nhiều gam màu tươi ngọt quyến rũ giúp bạn tha hồ lựa chọn. Bạn có thể kết hợp với váy jean,&nbsp;quần short,&nbsp;jean lửng… trông thật đáng yêu.</p>\n</div>\n</div>\n<p>Khi biết khéo phối giày với trang phục, bạn sẽ khiến set đồ của mình thêm thời trang, năng động và cá tính. Hãy đến với chúng tôi&nbsp;để lựa chọn cho mình những đôi giày ưng ý nhất nhé.</p>', 'Hoà vào không khí nhộn nhịp của mùa lễ hội, Kappa vừa góp thêm nhiều mẫu giày mới cực cool đón chào giáng sinh. Nào cùng lướt qua các mẫu giày này nhé!', 'http://vps3d11.vdrs.net/themes/upload/source/tin-tuc/http-channel-vcmedia-vn-prupload-164-2015-12-img20151214181350836-1450135968984-34-57-691-1107-crop-1450135984490.jpg', '2015-12-19 00:00:00', 1),
(2, 21, 2, 'Sự trở lại của One Star – The One Star Pro', '<p>Sự trở lại của The One Star Pro nhận được sự ủng hộ nhiệt tình từ các skater cũng như các bạn trẻ yêu thời trang.</p>\n<div class="VCSortableInPreviewMode active">\n<div><img title="Sự trở lại của One Star – The One Star Pro - Ảnh 1." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151204230257103.jpg" alt="Sự trở lại của One Star – The One Star Pro - Ảnh 1." /></div>\n<div class="PhotoCMS_Caption">&nbsp;</div>\n</div>\n<div class="VCSortableInPreviewMode active">\n<div><img title="Sự trở lại của One Star – The One Star Pro - Ảnh 2." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151204230257223.jpg" alt="Sự trở lại của One Star – The One Star Pro - Ảnh 2." /></div>\n<div class="PhotoCMS_Caption">&nbsp;</div>\n</div>\n<div class="VCSortableInPreviewMode">\n<div><img title="Sự trở lại của One Star – The One Star Pro - Ảnh 3." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151204230257343.jpg" alt="Sự trở lại của One Star – The One Star Pro - Ảnh 3." /></div>\n<div class="PhotoCMS_Caption">\n<p>One Star Pro trở lại được nâng cao nhiều tính năng để đem lại sự thoải mái khi mang như chất liệu da lộn dẻo dai, lót giày Lunarlon cực êm và đế cao su mỏng nhẹ.</p>\n</div>\n</div>\n<div class="VCSortableInPreviewMode active">\n<div>\n<div>&nbsp;</div>\n<div><img title="Sự trở lại của One Star – The One Star Pro - Ảnh 4." src="http://channel.vcmedia.vn/prupload/164/2015/12/img_201512042304267387.png" alt="Sự trở lại của One Star – The One Star Pro - Ảnh 4." /></div>\n</div>\n<div class="PhotoCMS_Caption">\n<p>Đa số mọi người cho rằng One Star chỉ phù hợp với các Skater, nhưng khi xem các hình ảnh giới thiệu cách mix match với One Star Pro, mọi người sẽ thay đổi cách suy nghĩ của mình ngay thôi!</p>\n</div>\n</div>\n<div class="VCSortableInPreviewMode active">\n<div><img title="Sự trở lại của One Star – The One Star Pro - Ảnh 5." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151204230257605.jpg" alt="Sự trở lại của One Star – The One Star Pro - Ảnh 5." /></div>\n<div class="PhotoCMS_Caption">\n<p>NĂNG ĐỘNG THOẢI MÁI - Sơ mi sọc đen phối áo thun trắng cùng mũ lưỡi trai đơn giản, One Star Pro đỏ trở thành điểm nhấn chủ yếu, tăng thêm sự mạnh mẽ và nam tính.</p>\n</div>\n</div>\n<div class="VCSortableInPreviewMode active">\n<div><img title="Sự trở lại của One Star – The One Star Pro - Ảnh 6." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151204230257996.jpg" alt="Sự trở lại của One Star – The One Star Pro - Ảnh 6." /></div>\n<div class="PhotoCMS_Caption">\n<p>THU ĐÔNG CỔ ĐIỂN - Áo thun trắng quần lửng đen, nón bucket cùng màu với giày, kết hợp Vest cộc tay cho phong cách trang nhã nhẹ nhàng, chi tiết khăn mù soa cùng tông màu cũng là điểm nhấn quan trọng cho một phong cách Vintage cổ điển mùa Thu Đông.</p>\n</div>\n</div>\n<div class="VCSortableInPreviewMode active">\n<div><img title="Sự trở lại của One Star – The One Star Pro - Ảnh 7." src="http://channel.vcmedia.vn/prupload/164/2015/12/img20151204230258129.jpg" alt="Sự trở lại của One Star – The One Star Pro - Ảnh 7." /></div>\n<div class="PhotoCMS_Caption">\n<p>BÌNH DỊ GẦN GŨI – Người mẫu lần này là một nhân viên công sở bình thường, yêu thích âm nhạc, thoải mái đơn giản là phương châm chủ yếu của anh trong việc mix trang phục. Áo phao không chỉ dành riêng cho mùa đông, thời tiết cuối tháng 11 se lạnh vẫn khá thích hợp khi phối với quần khaki ngắn, đậm phong cách Nhật Bản tươi sáng và thoải mái, giày và mũ cùng tông trở thành điểm nhấn chính, các chi tiết hòa hợp tạo thành tổng thể bình dị, gần gũi với cuộc sống hằng ngày.</p>\n</div>\n</div>', 'Dòng sản phẩm One Star ra mắt từ thập niên 70, được biết đến với thiết kế ngôi sao năm cánh hai bên thân giày, mở ra một trải nghiệm mới cho các dòng sản phẩm giày lúc bấy giờ.', 'http://vps3d11.vdrs.net/themes/upload/source/tin-tuc/img20151204230257103.jpg', '2015-12-19 00:00:00', 1),
(3, 21, 2, 'Sự trở lại của The One Star Pro nhận được sự ủng hộ nhiệt tình từ các skater cũng như các bạn trẻ yêu thời trang.  Sự trở lại của One Star – The One Star Pro - Ảnh 1. Sự trở lại của One Star – The One', '<p><img src="http://channel.vcmedia.vn/thumb_w/640/prupload/164/2015/11/img20151121140141809.jpg" alt="" /></p>\n<p>Converse táo bạo khi áp dụng kỹ thuật mới cho mẫu mang tính kinh điển như Jack Purcell để tạo các bức phá mang tính hiện đại hơn.</p>\n<div class="VCSortableInPreviewMode">\n<div><img title="Jack Purcell Signature - Cảm hứng cổ điển, phong cách hiện đại - Ảnh 1." src="http://channel.vcmedia.vn/thumb_w/640/prupload/164/2015/11/img20151121140142058.jpg" alt="Jack Purcell Signature - Cảm hứng cổ điển, phong cách hiện đại - Ảnh 1." /></div>\n<div class="PhotoCMS_Caption">&nbsp;</div>\n</div>\n<p>Đầu tiên phải nói đến chất liệu, Converse Jack Purcell Signature Leather được cải tiến bởi chất liệu da cao cấp mềm mại, thoáng khí, không lo rạn nứt, thể hiện sự đẳng cấp về phong cách, thoải mái và bền bỉ vượt trội. Lấy cảm hứng từ bản gốc năm 1940, Converse Jack Purcell Signature Jungle Cloth sử dụng chất liệu dày, chống nước, vải lót Bedford bên trong giữ cho đôi chân luôn ấm áp, khô ráo trong điều kiện thời tiết xấu nhất.</p>\n<div class="VCSortableInPreviewMode">\n<div><img title="Jack Purcell Signature - Cảm hứng cổ điển, phong cách hiện đại - Ảnh 2." src="http://channel.vcmedia.vn/thumb_w/640/prupload/164/2015/11/img20151121140142224.jpg" alt="Jack Purcell Signature - Cảm hứng cổ điển, phong cách hiện đại - Ảnh 2." /></div>\n</div>\n<p>Lót giày OrthoLite đẩy sự thoải mái lên mức tối ưu trong suốt thời gian sử dụng. Lưỡi giày xòe dạng cánh, liền mạch và ổn định. Mặt dưới đế giày thiết kế bằng mô hình đa chiều cung cấp độ bám cho người tiêu dùng</p>\n<div class="VCSortableInPreviewMode">\n<div><img title="Jack Purcell Signature - Cảm hứng cổ điển, phong cách hiện đại - Ảnh 3." src="http://channel.vcmedia.vn/thumb_w/640/prupload/164/2015/11/img20151121140142662.jpg" alt="Jack Purcell Signature - Cảm hứng cổ điển, phong cách hiện đại - Ảnh 3." /></div>\n</div>\n<p>Phần hông đế được sáng tạo bằng cách đẩy midsole, phần trên cao hơn phần dưới nâng tính hiện đại, trẻ trung cho sản phẩm, không thể thiếu chi tiết miệng cười kinh điển, đặc trưng riêng của dòng Jack Purcell.</p>\n<div class="VCSortableInPreviewMode">\n<div><img title="Jack Purcell Signature - Cảm hứng cổ điển, phong cách hiện đại - Ảnh 4." src="http://channel.vcmedia.vn/thumb_w/640/prupload/164/2015/11/img20151121140143035.jpg" alt="Jack Purcell Signature - Cảm hứng cổ điển, phong cách hiện đại - Ảnh 4." /></div>\n</div>\n<p>Trở lại với 3 phối màu đơn giản, cùng các tính năng vượt trội, Jack Purcell Signature hứa hẹn sẽ mang lại trải nghiệm mới mẻ cho các bạn trẻ yêu thời trang.</p>', 'Jack Purcell Signature nay ra mắt BST Thu Đông đặc sắc bởi phần thiết kế được chú trọng đến từng chi tiết, đơn giản mà lịch lãm, chất liệu da mới cao cấp mềm mại có độ dày vừa phải, phù hợp với thời tiết dần chuyển sang đông.', 'http://vps3d11.vdrs.net/themes/upload/source/tin-tuc/img20151121140141809.jpg', '2015-12-19 00:00:00', 1),
(4, 20, 2, 'Boots béo UGG "hot" trở lại sau vài năm bị thất sủng', '<div>Nói đến boots mùa đông, hẳn các tín đồ thời trang không ai là không biết đến Ugg boots, đôi boots da cừu mang hình dáng béo tròn hết sức dễ thương và ấm áp của thương hiệu UGG Australia hay còn được gọi với cái tên ngộ nghĩnh là "boots béo". Cách đây khoảng 10 năm, boots béo bắt đầu bước vào thời kỳ hoàng kim của mình khi liên tục xuất hiện cùng các ngôi sao Âu Mỹ trên phố trong mùa đông lạnh giá. Mặc cho những lời chê bai, chỉ trích về dáng vẻ cục mịch, thậm chí còn bị cho là phi thời trang, boots béo vẫn có một lượng fan hùng hậu trải dài từ người nổi tiếng cho đến những cô gái bình thường và nói không ngoa đã trở thành biểu tượng của mùa đông.</div>\n<div><br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/16/6-48db7.jpg" alt="6-48db7" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/1-48db7.jpg" alt="1-48db7" /></div>\n<div><em>Những đôi boots béo dày sụ, ấm áp của Ugg từng là item siêu "hot" trong khoảng thời gian 2004 - 2011.</em></div>\n</div>\n<div>&nbsp;</div>\n<div>Trong vài năm trở lại đây, boots béo không còn thịnh hành như trước và dần tỏ ra lép vế hơn hẳn so với những kiểu boots sành điệu, thanh thoát khác. Dẫu biết đây là việc sớm muộn gì cũng xảy ra nhưng những tín đồ boots béo một thời vẫn không khỏi tiếc nuối đôi boots dày sụ, ấm áp, mềm mại và êm chân tuyệt đối. Vậy mà giữa lúc bất ngờ nhất, đôi boots huyền thoại này đã tái xuất dưới dáng vẻ mới thon gọn hơn, thanh thoát hơn nhưng vẫn không đánh mất nét bụ bẫm trứ danh. Với sự ra đời của thiết kế mới này, Ugg boots được kỳ vọng là sẽ trở thành hot item của mùa đông 2015, khởi động lại đế chế hoàng kim của mình.</div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/16/22-48db7.jpg" alt="22-48db7" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/16/23-48db7.jpg" alt="23-48db7" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/16/24-48db7.jpg" alt="24-48db7" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/16/25-48db7.jpg" alt="25-48db7" /></div>\n<div><em>Sau vài năm im ắng, boots béo Ugg đang dần "hot" trở lại và còn được bổ sung thêm thiết kế mới thon gọn, thời trang hơn.</em></div>\n</div>\n<div>&nbsp;</div>\n<div>Kể từ khi chính thức lên kệ vào đầu tháng 11 vừa rồi, Ugg boots phiên bản mới đã nhận được sự ủng hộ của khá nhiều ngôi sao nổi tiếng. Cách đây vài ngày, hai chị em Kendall và Kylie Jenner đã được bắt gặp shopping tại cửa hàng Ugg New York. Khi ra về, Kendall còn sốt sắng diện luôn đôi boots mình vừa tậu. Tại châu Á, Hwang Jung Eum và Son Dam Bi là hai trong số những ngôi sao đầu tiên đã nhanh chóng thể hiện sự ủng hộ của mình với đôi boots lạ mà quen này.</div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/16/14-48db7.JPG" alt="14-48db7" /></div>\n<div><em>Hai chị em Kendall và Kylie Jenner vừa được bắt gặp shopping tại cửa hàng của Ugg.</em></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/16/15-48db7.jpg" alt="15-48db7" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/16/16-48db7.jpg" alt="16-48db7" /></div>\n<div><em>Kendall diện luôn thiết kế mới của Ugg boots khi rời cửa hàng.</em></div>\n</div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/17-48db7.jpg" alt="17-48db7" /></div>\n<div><em>Hwang Jung Eum gần gũi và đáng yêu với Ugg boots màu nâu.</em></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/18-48db7.jpg" alt="18-48db7" /></div>\n<div><em>Son Dam Bi lại chọn boots béo màu đen ton-sur-ton với trang phục.</em></div>\n<div><em>&nbsp;</em></div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/16/19-48db7.jpg" alt="19-48db7" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/16/21-48db7.jpg" alt="21-48db7" /></div>\n</div>\n<div>&nbsp;</div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/16/20-48db7.jpg" alt="20-48db7" /></div>\n<div><em>Trong hậu trường show diễn của Marchesa vào tháng 9 vừa qua, các người mẫu đã tham gia tổng duyệt trong đôi boots béo ấm áp, mềm mại.</em></div>\n</div>\n<div>&nbsp;</div>\n<div>Song song với sự ra đời của mẫu boots mới thon gọn, mẫu boots béo cổ điển cũng được nhiều tín đồ thời trang "sủng ái" trở lại. Dạo một vòng quanh Instagram, không khó để bắt gặp sự xuất hiện của đôi boots ấm áp này trong những bức ảnh OOTD của giới trẻ quốc tế.</div>\n<div>\n<div><br />\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/17/27-9be97.jpg" alt="27-9be97" /></div>\n</div>\n<div><em>Ugg boots trong các bức ảnh OOTD của giới trẻ thế giới thời gian gần đây.&nbsp;</em></div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/7-48db7.jpg" alt="7-48db7" /></div>\n<div><em>Boots béo rất phù hợp để diện cùng những trang phục monochrome đơn giản...</em></div>\n<div><br />\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/5-48db7.jpg" alt="5-48db7" /></div>\n</div>\n<div><em>... hay quần jeans và áo len cổ điển nhưng không bao giờ lỗi mốt như thế này.</em></div>\n<div><em>&nbsp;</em></div>\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/10-48db7.jpg" alt="10-48db7" /></div>\n<div><em>Boots béo cũng có thể phối với những trang phục nữ tính khác như váy len mềm mại...</em></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/8-48db7.jpg" alt="8-48db7" /></div>\n<div><em>... hay những set đồ layering sáng tạo khác.</em></div>\n<div>&nbsp;</div>\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/3-48db7.jpg" alt="3-48db7" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/9-48db7.jpg" alt="9-48db7" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/11-48db7.jpg" alt="11-48db7" /></div>\n<div><em>Các bạn trẻ Hàn thích phối boots béo với những set đồ khỏe khoắn, phóng khoáng với item chính là áo khoác dày sụ.</em></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/12-48db7.jpg" alt="12-48db7" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/16/13-48db7.jpg" alt="13-48db7" /></div>\n</div>\n<div>&nbsp;</div>\n<div><img src="http://k14.vcmedia.vn/d9baecf2c8/2015/11/17/26-9be97.jpg" alt="26-9be97" /></div>\n</div>', 'Sau vài năm im ắng, nay những đôi boots béo đã trở lại và được dự đoán là sẽ trở thành hot item của mùa đông năm nay.', 'http://vps3d11.vdrs.net/themes/upload/source/tin-tuc/5-48db7.jpg', '2015-12-19 00:00:00', 1),
(5, 20, 2, '7 xu hướng giày mùa đông bạn cần update ngay thời điểm này', '<p><strong>1. Boots cao quá gối</strong></p>\n<div>&nbsp;</div>\n<div>Trong "đại gia đình" các kiểu boots, có lẽ boots cao quá gối (over-the-knee hay thigh high boots) vốn luôn được xem là loại boots kiểu cách và cuốn hút bậc nhất. Bùng nổ từ thập niên 70 và trong suốt hơn 40 năm tồn tại, xu hướng boots cao quá gối vẫn "âm ỉ" cháy và thực sự bùng nổ trong hai mùa thời trang đổ lại đây. Điều thú vị nhất mà đôi boots cá tính này mang lại là khả năng giữ ấm đôi chân tuyệt vời mà không hề phá hỏng phong cách của bạn, đảm bảo sẽ thu hút mọi ánh nhìn của những người xung quanh. Ngoài một số bất tiện nho nhỏ trong việc di chuyển và đứng lên ngồi xuống, boots cao quá gối luôn được yêu thích bởi vẻ ngoài hiện đại, sành điệu nhưng không kém phần quyến rũ, sang trọng.</div>\n<div>&nbsp;</div>\n<div>Nhiều cô nàng cho rằng kiểu boots này kén dáng. Điều này không mấy chính xác, bởi nếu lựa chọn cách phối đồ hợp lý thì dáng vóc nào cũng tương thích với kiểu boots này. Chẳng hạn với những nàng có chiều cao khiêm tốn một chút thì kết hợp boots cùng shorts hay váy ngắn theo style "giấu quần" luôn là gợi ý hoàn hảo nhất, gợi cảm vừa đủ lại không lo co ro trong cái lạnh mùa đông.\n<div><br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-1b-8fa38.jpg" alt="key-shoes-1b-8fa38" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-1d-8fa38.jpg" alt="key-shoes-1d-8fa38" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-1e-8fa38.jpg" alt="key-shoes-1e-8fa38" /></div>\n</div>\n<div><strong>2. Boots buộc dây</strong></div>\n<div>&nbsp;</div>\n<div>Không cao quá gối như&nbsp;thigh high boots, boots buộc dây lại được yêu thích bởi vẻ ngoài mang âm hưởng miền viễn tây - những nàng cao bồi xinh đẹp và quyến rũ. Cũng bởi kiểu boots này vốn đã rất nổi bật nên trang phục đi kèm nên chọn các tông màu đơn sắc, nhã nhặn, có độ dài vừa chớm đến giữa đùi là đẹp nhất!</div>\n<div><br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-f2-963cb.jpg" alt="key-shoes-f2-963cb" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-f3-963cb.jpg" alt="key-shoes-f3-963cb" /></div>\n</div>\n<div><strong>3. Giày đính lông</strong></div>\n<div>&nbsp;</div>\n<div>Đây cũng là một nét mới hoàn toàn của mùa lạnh năm nay, giày lông hứa hẹn tạo nên một "cơn sốt" trên đường phố trong ngày lạnh. Nghe thì lạ, nhưng giày lông có thể là những đôi cao gót bình thường nhưng có chi tiết đính lông mang đậm chất xứ lạnh, vừa sang chảnh sành điệu lại dễ tạo cảm giác ấm áp. Lăng xê kiểu mốt này là thương hiệu Gucci với những đôi giày đính lông ngựa xa xỉ.&nbsp;</div>\n<div><br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-2e-8a74e.jpg" alt="key-shoes-2e-8a74e" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-2f-8a74e.jpg" alt="key-shoes-2f-8a74e" /></div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/ha-ho-1-d6092.jpg" alt="ha-ho-1-d6092" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/ha-ho-2-d6092.jpg" alt="ha-ho-2-d6092" /></div>\n<em>Hà Hồ cũng vừa sắm 2 đôi giày đính lông tuyệt đẹp của thương hiệu Gucci</em></div>\n</div>\n<div><strong>4. Giày Ballet mũi nhọn buộc dây</strong></div>\n<div>&nbsp;</div>\n<div>Được chuộng từ mùa hè, nhưng có vẻ như giày ballet vẫn giữ được sức hút của mình trong mùa tới. Thêm vào đó, cách nhấn nhá cực ấn tượng với phần dây buộc chạy dọc từ bàn chân tới cổ chân (hoặc thậm chí là cao hơn) sẽ khiến kiểu giày ballet đế bằng trông trendy hơn so với các thiết kế đơn giản thường thấy. Với kiểu giày này, chỉ cần quần jeans và một chiếc áo khoác là trông bạn đã hợp mốt.</div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-3a-46773.jpg" alt="key-shoes-3a-46773" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-3b-46773.jpg" alt="key-shoes-3b-46773" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-3d-46773.jpg" alt="key-shoes-3d-46773" /></div>\n</div>\n<div><strong>5. Chelsea Boots</strong></div>\n<div>&nbsp;</div>\n<div>Đây là thiết kế lấy cảm hứng từ những đôi boots cưỡi ngựa phóng khoáng với đặc trưng là phần chun nối ở hai bên mắt cá chân, giúp ôm sát cổ chân và dễ dàng khi xỏ vào. Các mẫu Chelsea boots thường là boot cổ ngắn, gót vuông và thấp, tạo sự thoải mái khi di chuyển, chính vì thế mà chúng được rất nhiều người mẫu chọn làm item "ruột" mỗi khi ra ngoài. Sẽ không gì tuyệt hơn khi song hành cùng các cô nàng thời trang là một đôi giày đảm bảo 2 chức năng: giữ ấm và phong cách, rất mạnh mẽ như Chelsea boots. Đặc biệt, chúng rất thích hợp với xu hướng menswear.</div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-e1-96b4f.jpg" alt="key-shoes-e1-96b4f" /></div>\n<div>&nbsp;</div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-e3-96b4f.jpg" alt="key-shoes-e3-96b4f" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-e4-96b4f.jpg" alt="key-shoes-e4-96b4f" /></div>\n</div>\n<div><strong>6. Giày menswear</strong></div>\n<div>&nbsp;</div>\n<div>Menswear là phong cách mà nhiều cô nàng theo đuổi trong vài mùa mốt đổ lại đây. Tuy nhiên, thay vì diện cả bộ suit ra đường để chứng tỏ "phẩm chất quý ông" thì chỉ một đôi giày là đã đủ. Những kiểu giày menswear thường gặp nhất là oxford cổ điển, brogue đầy nam tính, loafer da bóng, slipper da lộn... chúng cực hợp để diện cùng váy ngắn nữ tính.</div>\n<div>&nbsp;</div>\n<div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-g1-52e18.jpg" alt="key-shoes-g1-52e18" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-g3-52e18.png" alt="key-shoes-g3-52e18" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-g4-52e18.png" alt="key-shoes-g4-52e18" /></div>\n</div>\n<div>&nbsp;</div>\n</div>\n<div><strong>7. Những đôi cao gót phá cách</strong></div>\n<div>&nbsp;</div>\n</div>\n<div>Là các sản phẩm được hiện thực hóa từ những ý tưởng táo bạo không giới hạn, những đôi giày này với muôn vàn dáng vẻ, màu sắc cũng như cách thức kết hợp luôn hấp dẫn các tín đồ thời trang cá tính và ưa thích thử nghiệm mới. Tiêu biểu nhất có thể kể đến sneel - sự kết hợp giữa sneakers và heels.</div>\n<div><br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-h2-668d2.jpg" alt="key-shoes-h2-668d2" /></div>\n<br />\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/17/key-shoes-h4-668d2.jpg" alt="key-shoes-h4-668d2" /></div>\n</div>', 'Kiểu giày nào đang "hot" và đáng tiền để đầu tư? Đó luôn là câu hỏi muôn thuở của các nàng mỗi khi bước chân vào mùa mới.', 'http://vps3d11.vdrs.net/themes/upload/source/tin-tuc/key-shoes-1b-8fa38.jpg', '2015-12-20 00:00:00', 1),
(6, 21, 2, 'Trọn bộ sưu tập Vans Slip On Thu Đông 2015', '<p><img class="gif-content" src="http://channel.vcmedia.vn/thumb_w/640/prupload/164/2015/11/img20151108021355556.jpg" alt="" /></p>\n<div class="VCSortableInPreviewMode">\n<div class="PhotoCMS_Caption">Kiểu dáng hay thiết kế đều mang tính kinh điển, cộng thêm một chút táo bạo, Slip On thu hút sự quan tâm của giới trẻ hơn. Cùng ngắm 4 mẫu mới nhất của BST Thu Đông năm nay, khám phá những đặc điểm “vạn người mê” của Slip On nhé.</div>\n<div class="PhotoCMS_Caption">&nbsp;</div>\n</div>\n<div class="VCSortableInPreviewMode">\n<div><img class="gif-content" title="Trọn bộ sưu tập Vans Slip On Thu Đông 2015 - Ảnh 2." src="http://channel.vcmedia.vn/thumb_w/640/prupload/164/2015/11/img20151108021355843.jpg" alt="Trọn bộ sưu tập Vans Slip On Thu Đông 2015 - Ảnh 2." /></div>\n</div>\n<p>Lần này Vans sử dụng da màu bạc, không chỉ thoải mái khi mang và còn đề cao tính thời trang bởi vẻ lịch lãm và sang trọng của da.</p>\n<div>&nbsp;</div>\n<div><img class="gif-content" src="http://channel.vcmedia.vn/prupload/164/2015/11/img_201511080216033948.jpg" alt="" /></div>\n<p>Hình in được quan tâm và chăm chút phần thiết kế, ý tưởng vạn ánh đèn lung linh, hay các chi tiết mùa đông vui nhộn, đều có phối màu mới lạ, đáp ứng cách mix trang phục đa phong cách của giới trẻ hiện nay.</p>\n<div class="VCSortableInPreviewMode">\n<div><img class="gif-content" title="Trọn bộ sưu tập Vans Slip On Thu Đông 2015 - Ảnh 3." src="http://channel.vcmedia.vn/thumb_w/640/prupload/164/2015/11/img20151108021356327.jpg" alt="Trọn bộ sưu tập Vans Slip On Thu Đông 2015 - Ảnh 3." /></div>\n</div>\n<div>&nbsp;</div>\n<div><img class="gif-content" src="http://channel.vcmedia.vn/prupload/164/2015/11/img_201511080216271281.jpg" alt="" /></div>', 'Là một trong những dòng giày mang phong cách unisex bán chạy nhất của Vans, mỗi năm Slip On đều gây ấn tượng bằng nhiều ý tưởng sáng tạo mới mẻ, cụ thể nhất là BST Vans Slip On 2015 phong phú từ chất liệu cho đến phối màu.', 'http://vps3d11.vdrs.net/themes/upload/source/tin-tuc/img20151108021355556.jpg', '2015-12-19 00:00:00', 1),
(7, 20, 2, 'City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị', '<p>Đường cắt may khéo léo được nhấn bằng vải canvas, không chỉ tạo nên một sự kết hợp tinh tế và thời trang, đồng thời cho một góc nhìn thon gọn với phom giày ôm chân hơn. Cổ giày bẻ gập nổi bật sự đột phá, cá tính trên nền vải đỏ.</p>\n<div class="VCSortableInPreviewMode">\n<div><img title="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 1." src="http://channel.vcmedia.vn/thumb_w/640/prupload/164/2015/11/img20151103125032744.jpg" alt="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 1." />\n<div>&nbsp;</div>\n<div><img title="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 1." src="http://channel.vcmedia.vn/prupload/164/2015/11/img_201511031253287433.jpg" alt="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 1." /></div>\n</div>\n</div>\n<p>Dòng dành cho nam cũng không hề thua kém, vải nylon không chỉ mỏng nhẹ mà với bề mặt sáng bóng đặc trưng đó, đã góp phần thể hiện cái “chất” rất riêng của Palladium, nét mạnh mẽ và hiện đại của các chàng trai thành thị.</p>\n<div class="VCSortableInPreviewMode">\n<div><img title="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 2." src="http://channel.vcmedia.vn/thumb_w/640/prupload/164/2015/11/img20151103125033130.jpg" alt="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 2." /></div>\n<div class="PhotoCMS_Caption">\n<div>&nbsp;</div>\n<div><img title="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 2." src="http://channel.vcmedia.vn/prupload/164/2015/11/img_201511031253431141.jpg" alt="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 2." /></div>\n<div>\n<div>&nbsp;</div>\n<div><img title="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 2." src="http://channel.vcmedia.vn/prupload/164/2015/11/img_201511031254272347.jpg" alt="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 2." /></div>\n<div>&nbsp;</div>\n</div>\n</div>\n</div>\n<div class="VCSortableInPreviewMode">\n<div><img title="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 3." src="http://channel.vcmedia.vn/prupload/164/2015/11/img_201511031253529677.jpg" alt="City To City – Nylon Collection cùng Palladium cảm nhận “nhịp đập” thành thị - Ảnh 3." /></div>\n</div>\n<p>Palladium Nylon cũng chính là BST mở đầu cho chủ đề City To City Thu Đông 2015, tất cả các BST đều lấy cảm hứng từ sự chuyển đổi từ thành phố này đến thành phố khác, bắt đầu từ Paris, London, New York, Tokyo, Hong Kong và Việt Nam, cùng hướng đến một phong cách thành thị hiện đại, đề cao tính thời trang bằng nhiều chất liệu mới lạ cùng các kết hợp mang điểm nhấn sâu sắc.</p>', 'Lần đầu tiên được giới thiệu tại Việt Nam, Palladium tạo bước đột phá lớn khi áp dụng chất liệu Nylon cho dòng sản phẩm Thu Đông 2015.', 'http://vps3d11.vdrs.net/themes/upload/source/tin-tuc/img20151103125032744.jpg', '2015-12-20 00:00:00', 1),
(8, 20, 2, 'Tips chăm sóc hay ho những tín đồ "nghiện" giày sneaker nên nắm rõ', '<div>Có lẽ chưa bao giờ xu hướng thời trang sporty lại "càn quét" thế giới của các bạn trẻ mạnh mẽ như trong vài năm đổ lại đây. Cùng với sự lên ngôi của xu hướng sporty, giày sneaker gần như đã trở thành item không thể thiếu của bất cứ tín đồ thời trang nào khi ai cũng đầu tư cho mình ít nhất một đôi. Không khó để lý giải tại sao những đôi giày sneaker khỏe khoắn, thời thượng lại được lòng giới trẻ đến vậy bởi so với những kiểu giày khác, giày sneaker vừa đảm bảo sự thoải mái, dễ chịu cho chủ nhân lại vừa đáp ứng được khoản "chất chơi", cool ngầu. Tuy nhiên, có một sự thật là mặc dù diện giày sneaker với tần suất khá lớn, không phải ai cũng biết cách chăm sóc thật tốt cho những "bé cưng" của mình. Nếu bạn là một tín đồ của giày sneaker thì loạt tips chăm sóc và bảo quản giày sneaker sau đây chính là kim chỉ nam dành cho bạn.</div>\n<div>&nbsp;</div>\n<div><strong>1. Bàn chải mềm - "Dũng sĩ" làm sạch giày sneaker</strong></div>\n<div>\n<div>&nbsp;</div>\n<div>Cách giặt giày sneaker hiệu quả và an toàn nhất chính là sử dụng bàn chải mềm với nước ấm có pha xà phòng. Chiếc bàn chải còn đặc biệt hữu ích trong việc làm sạch kỹ càng đôi giày có pha chất liệu lưới. Dây giày cũng nên được giặt bằng tay với nước ấm và xà phòng để giữ được độ bền. Tuyệt đối đừng bao giờ ném cả đôi giày của mình vào trong máy giặt nhé!</div>\n</div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/01/1-f0182.jpg" alt="1-f0182" /></div>\n</div>\n<div>&nbsp;</div>\n<div>\n<div><strong>2. Tẩy sạch vết bẩn trên giày với giấm trắng</strong></div>\n<div>&nbsp;</div>\n<div>Giày sneaker sáng màu tuy rất nổi bật nhưng lại có nhược điểm là rất dễ bám bẩn. Trong trường hợp đôi giày sneaker của bạn không may bị dính những vết bẩn nhỏ mà bạn lại không có thời gian để giặt toàn bộ giày, hãy sử dụng giấm trắng như một chất tẩy rửa. Việc bạn cần làm là nhúng khăn mềm vào chút giấm trắng rồi chà nhẹ lên những vết bẩn, chúng sẽ nhanh chóng biến mất.</div>\n</div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/01/2-f0182.jpg" alt="2-f0182" /></div>\n</div>\n<div>&nbsp;</div>\n<div>\n<div><strong>3. Mẹo sử dụng nước tẩy trắng an toàn đối với giày sneaker</strong></div>\n<div>&nbsp;</div>\n<div>Để khôi phục lại vẻ trắng sáng tinh tươm cho giày sneaker trắng đã đi lâu ngày, bạn có thể dùng nước tẩy trắng chuyên dụng. Tuy nhiên, để chất liệu giày không bị ảnh hưởng, hãy tuyệt đối ghi nhớ công thức 1 nước tẩy : 5 nước. Đây là tỉ lệ an toàn đối với giày trắng bởi nó vừa giúp tẩy trắng hiệu quả lại vừa không làm ảnh hưởng nhiều đến chất vải. Bên cạnh đó, việc quá tham nước tẩy sẽ khiến đôi giày trắng của bạn bị ngả vàng thay vì trở nên trắng sáng.</div>\n</div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/01/3-f0182.jpg" alt="3-f0182" /></div>\n</div>\n<div>&nbsp;</div>\n<div>\n<div><strong>4. Cách làm sạch chuẩn nhất cho giày sneaker bằng da</strong></div>\n<div>&nbsp;</div>\n<div>Đối với những đôi giày sneaker mang chất liệu da, bạn tuyệt đối không được giặt giày với nước bởi đây chính là cách giúp phá hỏng đôi giày của bạn nhanh nhất. Cách an toàn giúp làm sạch giày sneaker da chính là lau giày với khăn mềm hơi ẩm.&nbsp;</div>\n</div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/01/4-f0182.jpg" alt="4-f0182" /></div>\n</div>\n<div>&nbsp;</div>\n<div><strong>5. Đánh bay vết dầu mỡ với dầu gội nhẹ dịu</strong></div>\n<div>&nbsp;</div>\n<div>Khi bạn không may làm bắn dầu mỡ lên đôi giày sneaker của mình, đừng nghĩ đây là tận thế! Vết dầu mỡ có thể rất khó tẩy rửa nhưng không phải là không thể tẩy sạch. Để xử lý chúng rất đơn giản, bạn chỉ việc nhúng bàn chải mềm vào dung dịch nước ấm có pha chút dầu gội nhẹ dịu rồi chà lên giày, chỉ một lúc sau là vết dầu mỡ sẽ biến mất.</div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/01/5-f0182.jpg" alt="5-f0182" /></div>\n</div>\n<div>&nbsp;</div>\n<div><strong>6. Rút ngắn thời gian phơi khô giày với khăn giấy</strong></div>\n<div>&nbsp;</div>\n<div>Để giày sneaker khô nhanh hơn và khô tuyệt đối sau khi giặt, trước khi mang giày đi phơi, bạn hãy nhét thật nhiều khăn giấy vào bên trong giày. Chúng sẽ giúp thấm hút hết lượng nước còn sót lại bên trong giày vốn rất khó bốc hơi. Cách này vừa giúp bạn tiết kiệm thời gian phơi giày lại vừa đảm bảo giày không bị mùi khó chịu vì vẫn còn ẩm. Ngoài ra, hãy nhớ phơi giày trong bóng râm nhé!</div>\n<div>\n<div>&nbsp;</div>\n</div>\n<div><strong>7. Ánh nắng mặt trời - Kẻ thù của giày sneaker</strong></div>\n<div>&nbsp;</div>\n<div>Tuyệt đối không bao giờ để giày sneaker dưới ánh nắng mặt trời bởi việc này sẽ khiến đôi giày của chúng ta bị bạc màu và thậm chí là bị ố. Khi bạn không đi một đôi giày nào đó, cách tốt nhất là hãy cất chúng trong hộp đi kèm lúc mua và đặt hộp nơi thoáng mát, khô ráo và không có ánh nắng chiếu vào.&nbsp;</div>\n<div>&nbsp;</div>\n<div>\n<div><img src="http://k14.vcmedia.vn/thumb_w/600/d9baecf2c8/2015/11/01/7-f0182.jpg" alt="7-f0182" /></div>\n<div>&nbsp;</div>\n<div>\n<div>\n<div id="wrapt-gif-1" class="wrapt-gif"><img class="gif-content" src="http://k14.vcmedia.vn/thumb_w/500/13a4221bf5/2015/11/17/500x300-bfe1d.gif" alt="500x300-bfe1d" /></div>\n</div>\n</div>\n</div>\n<p>&nbsp;</p>', 'Bạn có thể là một fan của giày sneaker nhưng bạn có chắc là mình biết cách chăm sóc và bảo quản chúng thật chuẩn không?', 'http://vps3d11.vdrs.net/themes/upload/source/tin-tuc/3-f0182.jpg', '2015-12-20 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL DEFAULT '0',
  `admin_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) NOT NULL DEFAULT '1',
  `price_sale` int(11) DEFAULT NULL,
  `isVAT` tinyint(1) DEFAULT '0',
  `sendmail` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1: Send 0: Khong',
  `tình trang` tinyint(3) NOT NULL DEFAULT '1' COMMENT '0: Hết hàng, 1 Còn hàng , 2 Sẳn có',
  `vote` int(11) DEFAULT NULL,
  `weight` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `size` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_date` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `votes` int(11) DEFAULT NULL,
  `total_votes` int(11) DEFAULT NULL,
  `specification` text COLLATE utf8_unicode_ci,
  `highlights` tinyint(3) DEFAULT NULL,
  `code_product` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cat_extra` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `cat_id`, `admin_id`, `code`, `name`, `description`, `content`, `image`, `price`, `price_sale`, `isVAT`, `sendmail`, `tình trang`, `vote`, `weight`, `size`, `public_date`, `status`, `votes`, `total_votes`, `specification`, `highlights`, `code_product`, `cat_extra`) VALUES
(2, 14, 2, NULL, 'Giày thể thao dạ quang Fashion', 'Giày thể thao dạ quang Fashion. Giày thể thao dạ quang Fashion 2015, uy tín, chính hãng, chất lượng, vận chuyển miễn phí và toàn quốc.', '<p>- Loại sản phẩm : Giày thể thao nữ</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE002%20gi%C3%A0y%20th%E1%BB%83%20thao%20%20d%E1%BA%A1%20quang%20Fashion/TTE002/DSC_8324.jpg" alt="" width="1000" height="604" /></p>\n<p>- Đế giày được thiết kế bằng phẳng&nbsp;tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE002%20gi%C3%A0y%20th%E1%BB%83%20thao%20%20d%E1%BA%A1%20quang%20Fashion/TTE002/DSC_8344.jpg" alt="" width="1000" height="685" /></p>\n<p>- Chất liệu vải, độ mềm và cứng hòa hợp tạo cảm giác thoải mái cho hoạt động của đôi chân.</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE002%20gi%C3%A0y%20th%E1%BB%83%20thao%20%20d%E1%BA%A1%20quang%20Fashion/TTE002/DSC_9251.jpg" alt="" width="533" height="800" /></p>\n<p>- Giày được thiết kế trẻ trung, năng động,nhiều màu sắc và&nbsp;không gây cảm giác khó chịu khi di chuyển.</p>\n<p>- Phù hợp cho mọi hoạt động hăng ngày.</p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE002%20gi%C3%A0y%20th%E1%BB%83%20thao%20%20d%E1%BA%A1%20quang%20Fashion/TTE002/DSC_8322.jpg', 295000, 100000, 0, 0, 1, NULL, NULL, NULL, '2015-12-20 00:00:00', 1, NULL, NULL, '<p>- Giới tính: Nữ</p>\n<p>- Xuất xứ: Hàn Quốc</p>', 1, 'T034', NULL),
(3, 14, 2, NULL, 'Giày thể thao superstar trẻ trung nữ', 'Giày thể thao superstar trẻ trung nữ. Giày thể thao superstar trẻ trung nữ uy tín, chính hãng, giá rẻ, vận chuyển miễn phí.', '<p>- Loại sản phẩm : Giày thể thao nữ</p>\n<p>- Giày được thiết kế trẻ trung, năng động, không gây cảm giác khó chịu khi di chuyển.</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE001%20gi%C3%A0y%20th%E1%BB%83%20thao%20superstar%20tr%E1%BA%BB%20trung%20n%E1%BB%AF/TTE001/DSC_8928.jpg" alt="" width="1000" height="667" /></p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE001%20gi%C3%A0y%20th%E1%BB%83%20thao%20superstar%20tr%E1%BA%BB%20trung%20n%E1%BB%AF/TTE001/DSC_8904.jpg" alt="" width="1000" height="667" /></p>\n<p>- Chất liệu vải tổng hợp&nbsp;tạo cảm giác thoải mái cho hoạt động của đôi chân.</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE001%20gi%C3%A0y%20th%E1%BB%83%20thao%20superstar%20tr%E1%BA%BB%20trung%20n%E1%BB%AF/TTE001/DSC_8928.jpg" alt="" width="1000" height="667" /></p>\n<p>- Phù hợp cho mọi hoạt động hăng ngày.</p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE001%20gi%C3%A0y%20th%E1%BB%83%20thao%20superstar%20tr%E1%BA%BB%20trung%20n%E1%BB%AF/TTE001/DSC_8904.jpg', 295000, 0, 0, 0, 1, NULL, NULL, NULL, '2015-12-18 00:00:00', 1, NULL, NULL, '<p>- Giới tính: Nữ</p>\n<p>- Xuất xứ: Hàn Quốc</p>', 1, 'T033', NULL),
(6, 12, 2, NULL, 'Giày thể thao Sport', 'Giày thể thao Sport nữ, Giày thể thao Sport uy tín, chất lượng, chính hãng, vận chuyển toàn quốc miễn phí.', '<p>- Loại sản phẩm : Giày bata nam</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA009%20Gi%C3%A0y%20th%E1%BB%83%20thao%20sport/TTA009/DSC_8779.jpg" alt="" width="1000" height="667" /></p>\n<p>- Chất liệu vải, độ mềm và cứng hòa hợp tạo cảm giác thoải mái cho hoạt động của đôi chân.</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA009%20Gi%C3%A0y%20th%E1%BB%83%20thao%20sport/TTA009/DSC_8420.jpg" alt="" width="1000" height="667" /></p>\n<p>- Giày được thiết kế trẻ trung, năng động, không gây cảm giác khó chịu khi di chuyển.</p>\n<p><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA009%20Gi%C3%A0y%20th%E1%BB%83%20thao%20sport/TTA009/DSC_8427.jpg" alt="" width="1000" height="770" /></p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA009%20Gi%C3%A0y%20th%E1%BB%83%20thao%20sport/TTA009/DSC_8444.jpg" alt="" width="1000" height="790" /></p>\n<p>- Phù hợp cho mọi hoạt động hăng ngày.</p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA009%20Gi%C3%A0y%20th%E1%BB%83%20thao%20sport/TTA009/DSC_8420.jpg', 345000, 0, 0, 0, 1, NULL, NULL, NULL, '2015-12-18 00:00:00', 1, NULL, NULL, '<p>Xuất xứ: Hàn Quốc</p>\n<p>Loại: Giày bata nam</p>', 0, 'T031', NULL),
(7, 12, 2, NULL, 'Giày thể thao hoa văn', 'Giày thể thao hoa văn. Giày thể thao hoa văn cho Nam uy tín, chính hãng, giá rẻ, vận chuyển miễn phí.', '<p>- Loại sản phẩm : Giày thể thao&nbsp;nam</p>\n<p>- Chất liệu vải kết hợp với chất liệu PU&nbsp;hòa hợp tạo cảm giác thoải mái, trẻ trung&nbsp;.</p>\n<p>- Giày được thiết kế mới lạ, năng động, không gây cảm giác khó chịu khi di chuyển.</p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p>- Phù hợp cho mọi hoạt động hằng&nbsp;ngày.</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA008%20gi%C3%A0y%20th%E1%BB%83%20thao%20hoa%20v%C4%83n/TTA008/DSC_8516.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA008%20gi%C3%A0y%20th%E1%BB%83%20thao%20hoa%20v%C4%83n/TTA008/DSC_8527.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA008%20gi%C3%A0y%20th%E1%BB%83%20thao%20hoa%20v%C4%83n/TTA008/DSC_8529.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA008%20gi%C3%A0y%20th%E1%BB%83%20thao%20hoa%20v%C4%83n/TTA008/DSC_8531.jpg" alt="" width="1000" height="727" /></p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA008%20gi%C3%A0y%20th%E1%BB%83%20thao%20hoa%20v%C4%83n/TTA008/DSC_8516.jpg', 295000, 0, 0, 0, 1, NULL, NULL, NULL, '2015-12-18 00:00:00', 1, NULL, NULL, '<p>Xuất xứ: Việt Nam</p>\n<p>Loại: Giày thể thao thời trang</p>', 0, 'T030', NULL),
(8, 18, 2, NULL, 'Giày bata xỏ dây trắng', 'Giày bata xỏ dây trắng. Giày bata thời trang cho nam uy tín, chất lượng, chính hãng và vận chuyển miễn phí.', '<p>- Loại sản phẩm : Giày bata nam</p>\n<p>- Chất liệu vải, độ mềm và cứng hòa hợp tạo cảm giác thoải mái cho hoạt động của đôi chân.</p>\n<p>- Giày được thiết kế trẻ trung, năng động, không gây cảm giác khó chịu khi di chuyển.</p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p>- Phù hợp cho mọi hoạt động hăng ngày.</p>\n<p>&nbsp;</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/BTN017%20gi%C3%A0y%20bata%20x%E1%BB%8F%20d%C3%A2y%20tr%E1%BA%AFng/DSC_9881.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/BTN017%20gi%C3%A0y%20bata%20x%E1%BB%8F%20d%C3%A2y%20tr%E1%BA%AFng/DSC_9883.jpg" alt="" width="1000" height="770" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/BTN017%20gi%C3%A0y%20bata%20x%E1%BB%8F%20d%C3%A2y%20tr%E1%BA%AFng/DSC_9903.jpg" alt="" width="1000" height="692" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/BTN017%20gi%C3%A0y%20bata%20x%E1%BB%8F%20d%C3%A2y%20tr%E1%BA%AFng/DSC_9911.jpg" alt="" width="1000" height="674" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/BTN017%20gi%C3%A0y%20bata%20x%E1%BB%8F%20d%C3%A2y%20tr%E1%BA%AFng/DSC_9914.jpg" alt="" width="1000" height="667" /></p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN017%20gi%C3%A0y%20bata%20x%E1%BB%8F%20d%C3%A2y%20tr%E1%BA%AFng/DSC_9903.jpg', 295000, 100000, 0, 0, 1, NULL, NULL, NULL, '2015-12-20 00:00:00', 1, NULL, NULL, '<p>Xuất xứ: Nhật Bản</p>\n<p>Loại: Giày bata thời trang</p>', 1, 'B052', ',18,12,'),
(9, 18, 2, NULL, 'Giày bata nữ khóa kéo cổ cao', 'Giày bata nữ khóa kéo cổ cao. Giày bata thời trang nữ uy tín, chính hãng, giá rẻ, vận chuyển miễn phí.', '<p>- Loại sản phẩm : Giày bata nữ</p>\n<p>- Chất liệu vải tạo cảm giác thoải mái cho hoạt động của đôi chân, tạo nên cá tính cho người mang giày.</p>\n<p>- Giày được thiết kế trẻ trung, năng động, không gây cảm giác khó chịu khi di chuyển. Với sự kết hợp của&nbsp;vải cùng&nbsp;khóa kéo tạo điểm nhấn lạ mắt cho sản phẩm.</p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p>- Phù hợp cho mọi hoạt động hằng ngày.</p>\n<p>&nbsp;</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN016%20gi%C3%A0y%20bata%20kh%C3%B3a%20k%C3%A9o%20c%E1%BB%95%20cao/DSC_0169.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN016%20gi%C3%A0y%20bata%20kh%C3%B3a%20k%C3%A9o%20c%E1%BB%95%20cao/DSC_0174.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN016%20gi%C3%A0y%20bata%20kh%C3%B3a%20k%C3%A9o%20c%E1%BB%95%20cao/DSC_0175.jpg" alt="" width="1000" height="706" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN016%20gi%C3%A0y%20bata%20kh%C3%B3a%20k%C3%A9o%20c%E1%BB%95%20cao/DSC_0176.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN016%20gi%C3%A0y%20bata%20kh%C3%B3a%20k%C3%A9o%20c%E1%BB%95%20cao/DSC_0179.jpg" alt="" width="533" height="800" /></p>\n<p>&nbsp;</p>\n<p>&nbsp;</p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN016%20gi%C3%A0y%20bata%20kh%C3%B3a%20k%C3%A9o%20c%E1%BB%95%20cao/DSC_0169.jpg', 295000, 0, 0, 0, 1, NULL, NULL, NULL, '2015-12-20 00:00:00', 1, NULL, NULL, '<p>Loại: Giày Bata thời trang nữ</p>\n<p>Xuất xứ: Việt Nam</p>', 1, 'B051', ',13,18,'),
(10, 13, 2, NULL, 'Giày bata nữ khóa kéo chấm bi', 'Giày bata nữ khóa kéo chấm bi. Giày bata nữ khóa kéo chấm bi thời trang đẹp, uy tín, chất lượng, vận chuyển miễn phí.', '<p>- Loại sản phẩm : Giày bata nữ</p>\n<p>- Chất liệu vải, độ mềm và cứng hòa hợp tạo cảm giác thoải mái cho hoạt động của đôi chân.</p>\n<p>- Giày được thiết kế trẻ trung, năng động, không gây cảm giác khó chịu khi di chuyển.</p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p>- Phù hợp cho mọi hoạt động hằng ngày.</p>\n<p>&nbsp;</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN018%20gi%C3%A0y%20bata%20ch%E1%BA%A5m%20bi/DSC_0087.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN018%20gi%C3%A0y%20bata%20ch%E1%BA%A5m%20bi/DSC_0093.jpg" alt="" width="1000" height="686" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN018%20gi%C3%A0y%20bata%20ch%E1%BA%A5m%20bi/DSC_0095.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN018%20gi%C3%A0y%20bata%20ch%E1%BA%A5m%20bi/DSC_0099.jpg" alt="" width="555" height="800" /></p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN018%20gi%C3%A0y%20bata%20ch%E1%BA%A5m%20bi/DSC_0087.jpg', 295000, 0, 0, 0, 1, NULL, NULL, NULL, '2015-12-20 00:00:00', 1, NULL, NULL, '<p>Loại: Giày bata thời trang</p>\n<p>Giới tính: Nữ</p>\n<p>Xuất xứ: Việt Nam</p>', 0, 'B050', ',18,13,'),
(12, 13, 2, NULL, 'Giày bata in logo huy chương', 'Giày bata in logo huy chương. Giày bata nữ thời trang đẹp, uy tín, chất lượng, vận chuyển miễn phí.', '<p>- Loại sản phẩm : Giày bata nữ</p>\n<p>- Chất liệu vải, độ mềm và cứng hòa hợp tạo cảm giác thoải mái cho hoạt động của đôi chân.</p>\n<p>- Giày được thiết kế trẻ trung, năng động, không gây cảm giác khó chịu khi di chuyển.</p>\n<p>- Với nhiều màu sắc tươi trẻ theo xu hướng hiện nay.</p>\n<p>- Đế giày được thiết kế bằng phẳng&nbsp;tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p>- Phù hợp cho mọi hoạt động hăng ngày, đi chơi, đi học...vv.</p>\n<p>&nbsp;</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN014%20gi%C3%A0y%20bata%20in%20logo%20huy%20ch%C6%B0%C6%A1ng/DSC_0103.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN014%20gi%C3%A0y%20bata%20in%20logo%20huy%20ch%C6%B0%C6%A1ng/DSC_0104.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN014%20gi%C3%A0y%20bata%20in%20logo%20huy%20ch%C6%B0%C6%A1ng/DSC_0107.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN014%20gi%C3%A0y%20bata%20in%20logo%20huy%20ch%C6%B0%C6%A1ng/DSC_0115.jpg" alt="" width="533" height="800" /></p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN014%20gi%C3%A0y%20bata%20in%20logo%20huy%20ch%C6%B0%C6%A1ng/DSC_0103.jpg', 295000, 0, 0, 0, 1, NULL, NULL, NULL, '2015-12-20 00:00:00', 1, NULL, NULL, '<p>- Loại sản phẩm : Giày bata nữ</p>\n<p>- Xuất xứ: Việt Nam</p>', 1, 'B049', ',12,18,'),
(13, 12, 2, NULL, 'Giày bata hoa văn quân đội', 'Giày bata hoa văn quân đội. Giày bata hoa văn cá tính thời trang nam uy tín, chất lượng, giá rẻ, vận chuyển miễn phí', '<p>- Loại sản phẩm : Giày bata nữ</p>\n<p>- Chất liệu vải, độ mềm và cứng hòa hợp tạo cảm giác thoải mái cho hoạt động của đôi chân.</p>\n<p>- Giày được thiết kế trẻ trung, năng động, không gây cảm giác khó chịu khi di chuyển. Hoa văn, họa tiết quân đội đem đến sự mới lạ ,&nbsp;</p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p>-&nbsp;Với màu sắc nổi bật, kiểu dáng trẻ trung đó là những đặc tính mà giày&nbsp;bata&nbsp;mang lại. Giày không chỉ sử dụng khi đi học&nbsp;mà còn là trợ thủ đắc lực của bạn trong những buổi du lịch&nbsp;hay dạo phố cùng bạn bè. Màu sắc trẻ trung, kiểu dáng hiện đại&nbsp;và phong cách, tạo nên điểm nhấn tuyệt vời cho đôi chân bạn.</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/DSC_0007.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/DSC_0010.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/DSC_0012.jpg" alt="" width="1000" height="665" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/DSC_0026.jpg" alt="" width="533" height="800" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/DSC_0038.jpg" alt="" width="1000" height="662" /></p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/DSC_0007.jpg', 295000, 0, 0, 0, 1, NULL, NULL, NULL, '2015-12-20 00:00:00', 1, NULL, NULL, '<p>- Loại: Giày bata</p>\n<p>- Xuất xứ: Việt Nam</p>', 1, 'B048', ',18,12,'),
(14, 16, 2, NULL, 'Giày lười sọc đủ màu nữ cá tính', 'Giày lười nữ. Giày lười sọc đủ màu nữ cá tính cực đẹp, cực chất và uy tín, chất lượng, bảo đảm chính hãng vận chuyển toàn quốc.', '<p>- Loại sản phẩm : Giày lười nữ</p>\n<p>- Chất liệu vải tạo cảm giác thoải mái cho hoạt động của đôi chân với nhiều màu sắc trẻ trung năng động.</p>\n<p>- Giày được thiết kế trẻ trung, năng động, không gây cảm giác khó chịu khi di chuyển.</p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p>- Phù hợp cho mọi hoạt động hằng ngày.</p>\n<p>&nbsp;</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL012%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc%20%C4%91%E1%BB%A7%20m%C3%A0u/DSC_9920.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL012%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc%20%C4%91%E1%BB%A7%20m%C3%A0u/DSC_9925.jpg" alt="" width="1000" height="721" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL012%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc%20%C4%91%E1%BB%A7%20m%C3%A0u/DSC_9928.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL012%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc%20%C4%91%E1%BB%A7%20m%C3%A0u/DSC_9936.jpg" alt="" width="1000" height="745" /></p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL012%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc%20%C4%91%E1%BB%A7%20m%C3%A0u/DSC_9920.jpg', 295000, 100000, 0, 0, 1, NULL, NULL, NULL, '2015-12-21 00:00:00', 1, NULL, NULL, '<p>- Loại: Giày lười nữ thời trang</p>\n<p>- Xuất xứ: Hàn Quốc</p>\n<p>&nbsp;</p>', 1, 'L064', ',13,16,'),
(15, 16, 2, NULL, 'Giày lười sọc nữ', 'Giày lười sọc nữ. Giày lười sọc nữ thời trang uy tín, giá rẻ, chất lượng và vận chuyển miễn phí.', '<p>- Loại sản phẩm : Giày lười nữ</p>\n<p>- Chất liệu vải tạo cảm giác thoải mái cho hoạt động của đôi chân với nhiều màu sắc trẻ trung năng động.</p>\n<p>- Giày được thiết kế trẻ trung, năng động với những đường sọc khắp trên giày.</p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p>- Phù hợp cho mọi hoạt động hằng ngày.</p>\n<p>&nbsp;</p>\n<p style="text-align: center;">&nbsp;</p>\n<p>&nbsp;</p>\n<p><img style="display: block; margin-left: auto; margin-right: auto;" src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL010%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc/DSC_0133.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL010%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc/DSC_0136.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL010%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc/DSC_0144.jpg" alt="" width="493" height="800" /></p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL010%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc/DSC_0133.jpg', 295000, 0, 0, 0, 1, NULL, NULL, NULL, '2015-12-20 00:00:00', 1, NULL, NULL, '<p>- Xuất xứ: Hàn Quốc</p>\n<p>&nbsp;</p>', 0, 'L062', ',13,16,'),
(16, 13, 2, NULL, 'Giày lười họa tiết chữ hàn nữ', 'Bán giày lười nữ hàn quốc. Giày lười họa tiết chữ hàn nữ thời trang đẹp, uy tín, chất lượng và giao hàng toàn quốc.', '<p>- Loại sản phẩm : Giày lười nữ</p>\n<p>- Chất liệu vải tạo cảm giác thoải mái cho hoạt động của đôi chân với nhiều màu sắc trẻ trung năng động.</p>\n<p>- Giày được thiết kế trẻ trung, năng động, không gây cảm giác khó chịu khi di chuyển.</p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p>- Phù hợp cho mọi hoạt động hằng ngày.</p>\n<p>&nbsp;</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL009%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20h%E1%BB%8Da%20ti%E1%BA%BFt%20ch%E1%BB%AF%20h%C3%A0n/DSC_9949.jpg" alt="" width="1000" height="624" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL009%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20h%E1%BB%8Da%20ti%E1%BA%BFt%20ch%E1%BB%AF%20h%C3%A0n/DSC_9956.jpg" alt="" width="1000" height="667" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL009%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20h%E1%BB%8Da%20ti%E1%BA%BFt%20ch%E1%BB%AF%20h%C3%A0n/DSC_9958.jpg" alt="" width="579" height="800" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL009%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20h%E1%BB%8Da%20ti%E1%BA%BFt%20ch%E1%BB%AF%20h%C3%A0n/DSC_9959.jpg" alt="" width="1000" height="668" /></p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL009%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20h%E1%BB%8Da%20ti%E1%BA%BFt%20ch%E1%BB%AF%20h%C3%A0n/DSC_9949.jpg', 295000, 0, 0, 0, 1, NULL, NULL, NULL, '2015-12-20 00:00:00', 1, NULL, NULL, '<p>- Xuất xứ: Hàn Quốc</p>\n<p>- Chất liệu: giày vải</p>', 1, 'L061', ',16,13,'),
(19, 15, 2, NULL, 'Giày boot 3 sọc nam', 'Giày boot 3 sọc nam. Giày casual thời trang cho nam uy tín, chất lượng, chính hãng và vận chuyển toàn quốc', '<p>- Loại sản phẩm : Giày boot&nbsp;3 sọc&nbsp;nam.</p>\n<p>- Chất liệu vải, PU&nbsp; tạo cảm giác thoải mái, trẻ trung .</p>\n<p>- Giày được thiết kế mới lạ, năng động.</p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p>- Phù hợp cho mọi hoạt động hằng&nbsp;ngày.</p>\n<p>&nbsp;</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT001%20gi%C3%A0y%20boot%203%20s%E1%BB%8Dc%20nam/DSC_9051.jpg" alt="" width="800" height="533" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT001%20gi%C3%A0y%20boot%203%20s%E1%BB%8Dc%20nam/DSC_9059.jpg" alt="" width="800" height="533" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT001%20gi%C3%A0y%20boot%203%20s%E1%BB%8Dc%20nam/DSC_9067.jpg" alt="" width="800" height="533" /></p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT001%20gi%C3%A0y%20boot%203%20s%E1%BB%8Dc%20nam/DSC_9067.jpg', 345000, 100000, 0, 0, 1, NULL, NULL, NULL, '2015-12-21 00:00:00', 1, NULL, NULL, '<p>- Xuất xứ: Việt Nam</p>\n<p>- Chất liệu: Vải</p>', 0, 'C009', ',12,'),
(20, 15, 2, NULL, 'Giày boot nike', 'Giày boot nike. Giày boot nike thời trang nữ uy tín, chất lượng, giá rẻ và vận chuyển toàn quốc miễn phí.', '<p>Loại sản phẩm : Giày boot&nbsp;nữ</p>\n<p>- Chất liệu vải tổng hợp kết hợp với PU&nbsp;tạo cảm giác thoải mái và an toàn cho da.</p>\n<p>- Với màu sắc tươi trẻ , kiểu dáng trẻ trung đó là những đặc tính mà giày bata mang lại.&nbsp;</p>\n<p>- Giày được thiết kế trẻ trung, năng động, không gây cảm giác khó chịu khi di chuyển.&nbsp;</p>\n<p>- Đế giày được thiết kế tạo cảm giác thoải mái, thuận tiện cho việc di chuyển nhiều.</p>\n<p>&nbsp;</p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT003%20gi%C3%A0y%20boot%20nike/DSC_9124.jpg" alt="" width="843" height="600" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT003%20gi%C3%A0y%20boot%20nike/DSC_9125.jpg" alt="" width="901" height="600" /></p>\n<p style="text-align: center;"><img src="http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT003%20gi%C3%A0y%20boot%20nike/DSC_9134.jpg" alt="" width="847" height="600" /></p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT003%20gi%C3%A0y%20boot%20nike/DSC_9125.jpg', 295000, 100000, 0, 0, 1, NULL, NULL, NULL, '2015-12-21 00:00:00', 1, NULL, NULL, '<p>- Giới tính: Nữ</p>\n<p>- Loại: Giày boot nữ thể thao cá tính thời trang dùng để đi chơi, dạo phố...</p>\n<p>- Xuất xứ: Hàn Quốc</p>', 1, 'C010', ',17,18,12,'),
(22, 0, 2, NULL, 'Giày bata test', 'Test phần tóm tắt', '<p>Test phần nội dung</p>', 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/530-giaybataxodaytrang-463x322.jpg', 295000, 295000, 0, 0, 1, NULL, NULL, NULL, '2015-12-21 00:00:00', 1, NULL, NULL, '<p>Test phần thông số kỹ thuật</p>', 0, 'T001', ',12,18,');

-- --------------------------------------------------------

--
-- Table structure for table `product_color`
--

CREATE TABLE IF NOT EXISTS `product_color` (
  `id` int(11) NOT NULL,
  `id_product` int(11) DEFAULT NULL,
  `id_color` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=110 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_color`
--

INSERT INTO `product_color` (`id`, `id_product`, `id_color`) VALUES
(29, 1, 2),
(28, 1, 1),
(31, 3, 3),
(92, 2, 1),
(22, 104, 1),
(32, 4, 2),
(33, 5, 1),
(34, 6, 2),
(35, 7, 2),
(94, 8, 1),
(87, 9, 2),
(86, 10, 2),
(39, 11, 2),
(85, 12, 2),
(84, 13, 1),
(102, 14, 1),
(81, 15, 2),
(78, 16, 1),
(45, 17, 1),
(46, 18, 3),
(103, 19, 1),
(109, 20, 3),
(50, 21, 2),
(91, 2, 2),
(96, 22, 5),
(101, 14, 5),
(100, 14, 2);

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

CREATE TABLE IF NOT EXISTS `product_image` (
  `id` int(11) NOT NULL,
  `id_pc` int(11) NOT NULL,
  `image` varchar(1024) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`id`, `id_pc`, `image`) VALUES
(35, 22, 'http://vps3d11.vdrs.net/themes/upload/source/color/250px-Solid_black.svg.png'),
(36, 28, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE002%20gi%C3%A0y%20th%E1%BB%83%20thao%20%20d%E1%BA%A1%20quang%20Fashion/TTE002/DSC_8344s.jpg'),
(37, 28, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE002%20gi%C3%A0y%20th%E1%BB%83%20thao%20%20d%E1%BA%A1%20quang%20Fashion/TTE002/DSC_8324.jpg'),
(38, 29, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE002%20gi%C3%A0y%20th%E1%BB%83%20thao%20%20d%E1%BA%A1%20quang%20Fashion/TTE002/DSC_9254.jpg'),
(40, 31, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE001%20gi%C3%A0y%20th%E1%BB%83%20thao%20superstar%20tr%E1%BA%BB%20trung%20n%E1%BB%AF/TTE001/DSC_8904.jpg'),
(41, 34, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA009%20Gi%C3%A0y%20th%E1%BB%83%20thao%20sport/TTA009/DSC_8427.jpg'),
(42, 35, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTA008%20gi%C3%A0y%20th%E1%BB%83%20thao%20hoa%20v%C4%83n/TTA008/DSC_8527.jpg'),
(61, 50, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT004%20gi%C3%A0y%20casual%20xanh%20d%E1%BA%A1%20quang/DSC_0080.jpg'),
(62, 50, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT004%20gi%C3%A0y%20casual%20xanh%20d%E1%BA%A1%20quang/DSC_0081.jpg'),
(63, 50, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT004%20gi%C3%A0y%20casual%20xanh%20d%E1%BA%A1%20quang/DSC_0082.jpg'),
(97, 78, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL009%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20h%E1%BB%8Da%20ti%E1%BA%BFt%20ch%E1%BB%AF%20h%C3%A0n/DSC_9949.jpg'),
(98, 78, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL009%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20h%E1%BB%8Da%20ti%E1%BA%BFt%20ch%E1%BB%AF%20h%C3%A0n/DSC_9959.jpg'),
(101, 81, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL010%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc/DSC_0133.jpg'),
(106, 84, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/DSC_0007.jpg'),
(107, 84, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/DSC_0026.jpg'),
(108, 85, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN014%20gi%C3%A0y%20bata%20in%20logo%20huy%20ch%C6%B0%C6%A1ng/DSC_0103.jpg'),
(109, 86, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN018%20gi%C3%A0y%20bata%20ch%E1%BA%A5m%20bi/DSC_0087.jpg'),
(110, 86, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN018%20gi%C3%A0y%20bata%20ch%E1%BA%A5m%20bi/DSC_0095.jpg'),
(111, 87, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN016%20gi%C3%A0y%20bata%20kh%C3%B3a%20k%C3%A9o%20c%E1%BB%95%20cao/DSC_0169.jpg'),
(112, 87, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN016%20gi%C3%A0y%20bata%20kh%C3%B3a%20k%C3%A9o%20c%E1%BB%95%20cao/DSC_0176.jpg'),
(116, 91, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE001%20gi%C3%A0y%20th%E1%BB%83%20thao%20superstar%20tr%E1%BA%BB%20trung%20n%E1%BB%AF/TTE001/DSC_8904.jpg'),
(117, 92, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE002%20gi%C3%A0y%20th%E1%BB%83%20thao%20%20d%E1%BA%A1%20quang%20Fashion/TTE002/DSC_8344.jpg'),
(118, 92, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/TTE002%20gi%C3%A0y%20th%E1%BB%83%20thao%20%20d%E1%BA%A1%20quang%20Fashion/TTE002/DSC_9251.jpg'),
(120, 94, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20The%20Thao/BTN017%20gi%C3%A0y%20bata%20x%E1%BB%8F%20d%C3%A2y%20tr%E1%BA%AFng/DSC_9903.jpg'),
(123, 96, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/530-giaybataxodaytrang-463x322.jpg'),
(124, 96, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/531-giaybataxodaytrang-463x322.jpg'),
(125, 96, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Bata/BTN013%20gi%C3%A0y%20bata%20hoa%20v%C4%83n%20qu%C3%A2n%20%C4%91%E1%BB%99i%20ph%E1%BB%91i%20nhi%E1%BB%81u%20m%C3%A0u/532-giaybataxodaytrang-463x322.jpg'),
(128, 102, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL012%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc%20%C4%91%E1%BB%A7%20m%C3%A0u/DSC_9920.jpg'),
(129, 102, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Luoi/GNL012%20gi%C3%A0y%20l%C6%B0%E1%BB%9Di%20s%E1%BB%8Dc%20%C4%91%E1%BB%A7%20m%C3%A0u/DSC_9936.jpg'),
(130, 103, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT001%20gi%C3%A0y%20boot%203%20s%E1%BB%8Dc%20nam/DSC_9067.jpg'),
(136, 109, 'http://vps3d11.vdrs.net/themes/upload/source/Giay%20Casual/GT003%20gi%C3%A0y%20boot%20nike/DSC_9125.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_like`
--

CREATE TABLE IF NOT EXISTS `product_like` (
  `id_member` int(11) DEFAULT NULL,
  `id_product` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_like`
--

INSERT INTO `product_like` (`id_member`, `id_product`) VALUES
(2, 3),
(5, 8),
(5, 14),
(6, 8),
(7, 8),
(7, 14),
(7, 19);

-- --------------------------------------------------------

--
-- Table structure for table `product_size`
--

CREATE TABLE IF NOT EXISTS `product_size` (
  `id` int(11) NOT NULL,
  `id_pc` int(11) DEFAULT NULL,
  `size` varchar(10) DEFAULT NULL,
  `soluong` tinyint(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=400 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_size`
--

INSERT INTO `product_size` (`id`, `id_pc`, `size`, `soluong`) VALUES
(46, 22, '34', 1),
(45, 22, '41', 1),
(44, 22, '66', 2),
(51, 28, '2354', 2),
(52, 29, '535', 1),
(322, 92, '41', 1),
(321, 92, '40', 1),
(320, 92, '39', 1),
(319, 92, '38', 1),
(57, 31, '38', 1),
(58, 31, '39', 1),
(59, 31, '40', 1),
(60, 31, '41', 1),
(61, 34, '38', 1),
(62, 34, '39', 1),
(63, 34, '40', 1),
(64, 34, '41', 1),
(65, 35, '38', 1),
(66, 35, '39', 1),
(67, 35, '40', 1),
(68, 35, '41', 1),
(330, 94, '38', 1),
(329, 94, '39', 1),
(328, 94, '40', 1),
(327, 94, '41', 1),
(300, 87, '37', 1),
(299, 87, '38', 1),
(298, 87, '39', 1),
(297, 87, '40', 1),
(296, 87, '41', 1),
(295, 86, '38', 1),
(294, 86, '39', 1),
(293, 86, '40', 1),
(292, 86, '41', 1),
(291, 85, '41', 1),
(290, 85, '40', 1),
(289, 85, '39', 1),
(288, 85, '38', 1),
(287, 84, '41', 1),
(286, 84, '40', 1),
(285, 84, '39', 1),
(284, 84, '38', 1),
(354, 101, '36', 1),
(358, 101, '40', 1),
(357, 101, '39', 1),
(356, 101, '38', 1),
(355, 101, '37', 1),
(271, 81, '36', 1),
(270, 81, '37', 1),
(269, 81, '38', 1),
(268, 81, '39', 1),
(267, 81, '40', 1),
(258, 78, '40', 1),
(257, 78, '39', 1),
(256, 78, '38', 1),
(255, 78, '37', 1),
(254, 78, '36', 1),
(369, 103, '42', 1),
(368, 103, '41', 1),
(367, 103, '40', 1),
(366, 103, '39', 1),
(365, 103, '38', 1),
(399, 109, '36', 1),
(398, 109, '37', 1),
(397, 109, '38', 1),
(396, 109, '39', 1),
(395, 109, '40', 1),
(123, 50, '40', 1),
(122, 50, '39', 1),
(121, 50, '38', 1),
(120, 50, '37', 1),
(318, 91, '38', 1),
(317, 91, '39', 1),
(316, 91, '40', 1),
(315, 91, '42', 1),
(337, 96, '37', 1),
(353, 100, '36', 1),
(352, 100, '37', 1),
(351, 100, '38', 1),
(350, 100, '39', 1),
(349, 100, '40', 1),
(359, 102, '41', 1),
(360, 102, '40', 1),
(361, 102, '39', 1),
(362, 102, '38', 1),
(363, 102, '37', 1),
(364, 102, '36', 1);

-- --------------------------------------------------------

--
-- Table structure for table `provider`
--

CREATE TABLE IF NOT EXISTS `provider` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `image` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `sort_order` tinyint(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `provider`
--

INSERT INTO `provider` (`id`, `name`, `status`, `image`, `sort_order`) VALUES
(1, 'Camera No1', 2, '', 1),
(2, 'Vantech', 2, '', 2),
(3, 'Questek', 2, '', 3),
(4, 'Dahua', 2, '', 4),
(5, 'Benco', 2, '', 5),
(6, 'Avtech', 2, '', 6),
(7, 'Hikvision', 2, '', 7);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(50) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `slug`, `count`, `create_date`, `modify_date`, `status`) VALUES
(1, 'giày thể thao', 'giay-the-thao', 0, '2015-12-18 17:07:00', '2015-12-18 18:09:49', 1),
(2, 'Giày thể thao superstar', 'giay-the-thao-superstar', 0, '2015-12-18 17:57:00', '2015-12-18 17:57:00', 1),
(3, 'Giày nữ', 'giay-nu', 0, '2015-12-18 17:57:00', '2015-12-18 19:08:23', 1),
(4, 'Giày nam', 'giay-nam', 0, '2015-12-18 18:09:00', '2015-12-18 19:00:22', 1),
(5, 'giay the thao hoa van', 'giay-the-thao-hoa-van', 0, '2015-12-18 18:09:00', '2015-12-18 18:09:00', 1),
(6, 'giay bata', 'giay-bata', 0, '2015-12-18 18:15:00', '2015-12-18 18:40:06', 1),
(7, 'giày bata thời trang nam', 'giay-bata-thoi-trang-nam', 0, '2015-12-18 18:15:00', '2015-12-18 18:15:00', 1),
(8, 'giày bata nữ', 'giay-bata-nu', 0, '2015-12-18 18:20:00', '2015-12-18 18:32:26', 1),
(9, 'Giày bata nam', 'giay-bata-nam', 0, '2015-12-18 18:40:00', '2015-12-18 18:40:00', 1),
(10, 'Giày lười', 'giay-luoi', 0, '2015-12-18 18:45:00', '2015-12-18 18:53:00', 1),
(11, 'Giay luoi nu', 'giay-luoi-nu', 0, '2015-12-18 18:45:00', '2015-12-18 18:53:00', 1),
(12, 'Giay boot nam', 'giay-boot-nam', 0, '2015-12-18 18:58:00', '2015-12-18 19:00:22', 1),
(13, 'giay casual', 'giay-casual', 0, '2015-12-18 18:58:00', '2015-12-21 17:13:52', 1),
(14, 'giay casual nu', 'giay-casual-nu', 0, '2015-12-18 19:04:00', '2015-12-21 17:13:52', 1),
(15, 'giay di choi nu', 'giay-di-choi-nu', 0, '2015-12-18 19:04:00', '2015-12-18 19:04:00', 1),
(16, 'Giầy sneaker', 'giay-sneaker', 0, '2015-12-20 18:43:00', '2015-12-20 19:09:43', 1),
(17, 'giày bata thê thao', 'giay-bata-the-thao', 0, '2015-12-21 11:27:00', '2015-12-21 11:33:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags_content`
--

CREATE TABLE IF NOT EXISTS `tags_content` (
  `tag_id` int(11) NOT NULL,
  `module` varchar(128) COLLATE utf8_bin NOT NULL,
  `obj_id` int(11) NOT NULL,
  `sort_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `tags_content`
--

INSERT INTO `tags_content` (`tag_id`, `module`, `obj_id`, `sort_order`) VALUES
(16, 'posts', 8, 1),
(16, 'posts', 5, 1),
(13, 'product', 20, 1),
(14, 'product', 20, 2);

-- --------------------------------------------------------

--
-- Table structure for table `urlalias`
--

CREATE TABLE IF NOT EXISTS `urlalias` (
  `id` int(11) NOT NULL,
  `realurl` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `aliasurl` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `title` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `description` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `keyword` varchar(512) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) NOT NULL,
  `language` tinyint(1) NOT NULL DEFAULT '1',
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `urlalias`
--

INSERT INTO `urlalias` (`id`, `realurl`, `aliasurl`, `title`, `description`, `keyword`, `status`, `language`, `create_date`, `modify_date`) VALUES
(25, 'user', 'tai-khoan', '', '', '', 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 'category/detail/', 'loai/', '', '', '', 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gid` int(11) NOT NULL DEFAULT '1',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastlogin` datetime NOT NULL,
  `ip_connection` varchar(15) DEFAULT NULL,
  `avatar` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '1',
  `phone` int(15) NOT NULL,
  `type` tinyint(3) NOT NULL,
  `yahoo` varchar(255) NOT NULL,
  `skype` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `salt`, `gid`, `name`, `lastlogin`, `ip_connection`, `avatar`, `slug`, `create_date`, `modify_date`, `publish`, `phone`, `type`, `yahoo`, `skype`) VALUES
(1, 'admin', 'dev@vtvthethao.vn', '7a78850084e256712eccb4507f713832', '0b824', 1, '', '0000-00-00 00:00:00', '113.22.16.72', '', 'admin', '2014-05-01 02:03:58', '0000-00-00 00:00:00', 2, 0, 0, '', ''),
(2, 'giabao', 'gbcntt93@gmail.com', 'cbfc417c132ab108048cb3d3c030fc20', '189a6', 1, 'Gia Bảo', '0000-00-00 00:00:00', '1.52.241.151', '1435807198_Desert.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1226998644, 1, 'bin_sun93@yahoo.com', 'binsun93'),
(39, 'test1', 'tuyen.bui@yestech.vn', '55166951c35767085ecabae7153d49fd', '4fff8', 1, 'test1', '0000-00-00 00:00:00', '115.79.59.190', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 1226998611, 2, 'aibiet', 'aibiet'),
(70, 'test2', 'gbcntt93@gmail.com', '', '', 1, 'tes2', '0000-00-00 00:00:00', NULL, '1435807865_Penguins.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 1226998622, 3, 'testthoima@yahoo.com', 'binsun93'),
(71, 'admin', 'phunghuy28988@gmail.com', 'a0e130e7127b2d7e5a50d843c02a99f4', '9572b', 1, 'admin', '0000-00-00 00:00:00', '1.52.158.83', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 978831618, 1, 'quochuy_dct1074', 'huyphung28988'),
(72, 'support_box', 'ledinhhung.ktp@gmail.com', '8ea51fb65bff5dfca2066f238a3670b0', 'dbfb1', 1, 'Đình Hưng', '0000-00-00 00:00:00', '113.162.211.53', '1437386267_10580271_599203496866049_4671961836587820266_n.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 1682586825, 2, 'diarylove1988@yahoo.com', 'dinhhung1988'),
(73, 'huyphung', 'huyphung2809@gmail.com', 'e9e32947bc591b0b7e6bfa0010b46d59', 'f2549', 1, 'Huy Phùng', '0000-00-00 00:00:00', '14.161.46.218', '1437366083_yojimbo.jpg', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 978831618, 1, 'quochuy_dct1074', 'huyphung28988'),
(74, 'seo', 'phunghuy28988@gmail.com', '32f71715c577bda0a9b6e6f3d05a472b', '8112f', 1, 'seo', '0000-00-00 00:00:00', '115.79.59.190', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 978831618, 1, 'quochuy_dct1074', 'huyphung28988');

-- --------------------------------------------------------

--
-- Table structure for table `users_role`
--

CREATE TABLE IF NOT EXISTS `users_role` (
  `id` int(11) NOT NULL,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `permision` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_role`
--

INSERT INTO `users_role` (`id`, `name`, `create_date`, `modify_date`, `status`, `permision`) VALUES
(1, 'Admin', '2014-09-15 19:41:54', '2014-09-15 19:42:04', 1, ''),
(2, 'Biên tập video', '2014-09-15 19:41:54', '2014-09-12 01:13:00', 1, ''),
(3, 'Video Encoder', '2014-09-15 19:41:54', '2014-09-12 01:00:00', 1, 'videos_access,videos_add,videos_delete,videos_edit'),
(4, 'Cộng tác viên', '2014-09-15 19:41:54', '2014-10-22 13:51:48', 1, 'posts_add,posts_delete,posts_edit_owner,posts_stick_hot,posts_stick_banner,posts_slide,posts_stick_shortnews,posts_writing_access,tags_access'),
(5, 'Biên tập viên', '2014-09-15 19:41:54', '2014-10-03 18:41:00', 1, 'posts_management_access,posts_add,posts_delete,posts_edit_owner,posts_edit_other,posts_stick_hot,posts_stick_banner,posts_slide,posts_stick_shortnews,posts_estop,posts_writing_access,tags_access,videos_access,videos_add,videos_edit'),
(6, 'Chuyên gia', '2014-09-15 19:41:54', '2014-09-15 21:37:00', 2, 'users_access,posts_management_access,posts_edit_owner,posts_delete_published,posts_stick_banner,videos_access,videos_add,videos_delete,videos_edit,videos_publish,videos_section'),
(7, 'Marketings', '2014-12-17 13:35:06', '2014-12-17 13:35:06', 0, 'posts_management_access,posts_add,posts_edit_owner,posts_edit_other,posts_edit_published,posts_stick_hot,posts_stick_banner,posts_slide,posts_stick_shortnews,posts_writing_access,videos_access,videos_add,videos_delete,videos_delete_published,videos_publish,videos_section,hotiframe_access');

-- --------------------------------------------------------

--
-- Table structure for table `users_role_rel`
--

CREATE TABLE IF NOT EXISTS `users_role_rel` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users_role_rel`
--

INSERT INTO `users_role_rel` (`user_id`, `role_id`) VALUES
(20, 1),
(21, 1),
(29, 1),
(30, 1),
(33, 2),
(34, 2),
(35, 1),
(37, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(44, 1),
(45, 1),
(46, 4),
(47, 1),
(49, 4),
(50, 7),
(51, 1),
(52, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_loglogin`
--

CREATE TABLE IF NOT EXISTS `user_loglogin` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1776 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_loglogin`
--

INSERT INTO `user_loglogin` (`id`, `user_name`, `user_id`, `last_login`) VALUES
(1, 'admin', 1, '2014-08-06 01:58:12'),
(2, 'admin', 1, '2014-08-06 02:19:07'),
(3, 'admin', 1, '2014-08-06 09:38:18'),
(4, 'admin', 1, '2014-08-06 10:36:48'),
(5, 'admin', 1, '2014-08-06 10:37:21'),
(6, 'admin', 1, '2014-08-06 12:58:18'),
(7, 'admin', 1, '2014-08-06 13:41:25'),
(8, 'tuyenbui', 39, '2014-08-06 15:49:32'),
(9, 'thanhlt', 21, '2014-08-07 01:07:09'),
(10, 'tuyenbui', 39, '2014-08-07 01:53:22'),
(11, 'tungdx', 20, '2014-08-07 01:56:32'),
(12, 'thanhlt', 21, '2014-08-07 02:01:12'),
(13, 'thanhlt', 21, '2014-08-07 02:53:54'),
(14, 'tungdx', 20, '2014-08-07 03:03:59'),
(15, 'tuyenbui', 39, '2014-08-07 03:17:01'),
(16, 'tranhien', 36, '2014-08-07 03:41:14'),
(17, 'tranhien', 36, '2014-08-07 03:41:50'),
(18, 'tranhien', 36, '2014-08-07 03:59:51'),
(19, 'tuyenbui', 39, '2014-08-07 04:12:54'),
(20, 'tuyenbui', 39, '2014-08-07 06:17:39'),
(21, 'tungdx', 20, '2014-08-07 06:37:57'),
(22, 'tranhien', 36, '2014-08-07 07:14:13'),
(23, 'tuyenbui', 39, '2014-08-07 08:04:36'),
(24, 'tuyenbui', 39, '2014-08-07 08:33:30'),
(25, 'tungdx', 20, '2014-08-07 12:12:00'),
(26, 'tranhien', 36, '2014-08-07 12:39:18'),
(27, 'tranhien', 36, '2014-08-07 15:25:53'),
(28, 'thanhlt', 21, '2014-08-08 01:29:46'),
(29, 'tranhien', 36, '2014-08-08 01:35:18'),
(30, 'thanhlt', 21, '2014-08-08 01:42:49'),
(31, 'tungdx', 20, '2014-08-08 01:47:05'),
(32, 'tuyenbui', 39, '2014-08-08 01:52:45'),
(33, 'tuyenbui', 39, '2014-08-08 02:24:11'),
(34, 'tranhien', 36, '2014-08-08 03:10:57'),
(35, 'thanhlt', 21, '2014-08-08 05:56:54'),
(36, 'thanhlt', 21, '2014-08-08 06:35:19'),
(37, 'tungdx', 20, '2014-08-08 06:37:56'),
(38, 'tuyenbui', 39, '2014-08-08 07:13:46'),
(39, 'tranhien', 36, '2014-08-08 08:12:01'),
(40, 'tungdx', 20, '2014-08-08 09:02:53'),
(41, 'tranhien', 36, '2014-08-08 09:02:55'),
(42, 'tungdx', 20, '2014-08-08 14:15:20'),
(43, 'thanhlt', 21, '2014-08-08 23:47:47'),
(44, 'thanhlt', 21, '2014-08-09 01:03:05'),
(45, 'tungdx', 20, '2014-08-09 01:34:12'),
(46, 'tranhien', 36, '2014-08-09 01:47:42'),
(47, 'tuyenbui', 39, '2014-08-09 18:07:30'),
(48, 'tuyenbui', 39, '2014-08-11 14:16:15'),
(49, 'tuyenbui', 39, '2014-08-15 00:29:26'),
(50, 'tuyenbui', 39, '2014-08-15 01:12:50'),
(51, 'tuyenbui', 39, '2014-08-17 16:42:22'),
(52, 'tuyenbui', 39, '2014-08-17 17:11:26'),
(53, 'tuyenbui', 39, '2014-08-18 02:07:37'),
(54, 'tuyenbui', 39, '2014-08-18 05:23:27'),
(55, 'tuyenbui', 39, '2014-08-18 07:19:20'),
(56, 'tuyenbui', 39, '2014-08-18 12:44:09'),
(57, 'tuyenbui', 39, '2014-08-19 08:19:54'),
(58, 'tuyenbui', 39, '2014-08-19 09:19:37'),
(59, 'tuyenbui', 39, '2014-08-20 14:29:17'),
(60, 'tuyenbui', 39, '2014-08-21 02:11:38'),
(61, 'tuyenbui', 39, '2014-08-21 02:25:57'),
(62, 'tuyenbui', 39, '2014-08-21 07:43:14'),
(63, 'tuyenbui', 39, '2014-08-22 02:50:21'),
(64, 'tuyenbui', 39, '2014-08-22 06:17:17'),
(65, 'tuyenbui', 39, '2014-08-22 06:21:19'),
(66, 'tuyenbui', 39, '2014-08-23 06:54:17'),
(67, 'tuyenbui', 39, '2014-08-23 14:51:19'),
(68, 'tuyenbui', 39, '2014-08-23 17:23:36'),
(69, 'phut91', 39, '2014-08-23 17:26:16'),
(70, 'phut91', 39, '2014-08-24 04:33:27'),
(71, 'phut91', 39, '2014-08-24 13:06:08'),
(72, 'phut91', 39, '2014-08-26 08:46:58'),
(73, 'phut91', 39, '2014-08-26 16:57:12'),
(74, 'phut91', 39, '2014-08-30 05:50:14'),
(75, 'phut91', 39, '2014-08-30 07:24:48'),
(76, 'phut91', 39, '2014-08-30 07:25:23'),
(77, 'phut91', 39, '2014-09-01 04:20:38'),
(78, 'phut91', 39, '2014-09-09 07:02:37'),
(79, 'phut91', 39, '2014-09-09 07:14:58'),
(80, 'phut91', 39, '2014-09-09 07:17:20'),
(81, 'phut91', 39, '2014-09-10 07:49:36'),
(82, 'phut91', 39, '2014-09-10 13:49:29'),
(83, 'phut91', 39, '2014-09-11 07:39:41'),
(84, 'phut91', 39, '2014-09-11 07:39:43'),
(85, 'phut91', 39, '2014-09-11 07:39:45'),
(86, 'phut91', 39, '2014-09-11 07:39:58'),
(87, 'phut91', 39, '2014-09-11 07:42:46'),
(88, 'phut91', 39, '2014-09-11 07:42:54'),
(89, 'phut91', 39, '2014-09-11 07:42:55'),
(90, 'phut91', 39, '2014-09-11 07:45:41'),
(91, 'phut91', 39, '2014-09-11 07:45:49'),
(92, 'phut91', 39, '2014-09-11 07:46:05'),
(93, 'phut91', 39, '2014-09-11 07:46:11'),
(94, 'phut91', 39, '2014-09-11 07:55:25'),
(95, 'phut91', 39, '2014-09-12 09:07:56'),
(96, 'phut91', 39, '2014-09-13 12:11:08'),
(97, 'phut91', 39, '2014-09-14 09:46:19'),
(98, 'phut91', 39, '2014-09-15 11:41:13'),
(99, 'phut91', 39, '2014-09-15 12:23:34'),
(100, 'phut91', 39, '2014-09-17 06:48:06'),
(101, 'phut91', 39, '2014-09-17 07:38:13'),
(102, 'phut91', 39, '2014-09-17 08:25:21'),
(103, 'phut91', 39, '2014-09-18 09:25:14'),
(104, 'phut91', 39, '2014-09-18 12:57:11'),
(105, 'phut91', 39, '2014-09-18 13:19:32'),
(106, 'phut91', 39, '2014-09-20 03:49:49'),
(107, 'phut91', 39, '2014-09-20 04:40:31'),
(108, 'phut91', 39, '2014-09-20 07:08:25'),
(109, 'phut91', 39, '2014-09-20 07:26:42'),
(110, 'phut91', 39, '2014-09-20 13:45:48'),
(111, 'phut91', 39, '2014-09-21 14:08:36'),
(112, 'phut91', 39, '2014-09-22 04:50:41'),
(113, 'phut91', 39, '2014-09-22 10:09:39'),
(114, 'phut91', 39, '2014-09-22 10:21:52'),
(115, 'phut91', 39, '2014-09-22 14:32:36'),
(116, 'phut91', 39, '2014-09-23 03:06:04'),
(117, 'phut91', 39, '2014-09-24 01:55:44'),
(118, 'phut91', 39, '2014-09-24 03:29:08'),
(119, 'phut91', 39, '2014-09-24 05:57:38'),
(120, 'phut91', 39, '2014-09-24 05:58:05'),
(121, 'phut91', 39, '2014-09-24 07:44:13'),
(122, 'phut91', 39, '2014-09-24 10:57:02'),
(123, 'phut91', 39, '2014-09-25 03:14:39'),
(124, 'phut91', 39, '2014-09-25 04:38:31'),
(125, 'phut91', 39, '2014-09-26 10:40:46'),
(126, 'phut91', 39, '2014-09-26 11:25:06'),
(127, 'phut91', 39, '2014-09-29 04:43:38'),
(128, 'phut91', 39, '2014-10-01 15:31:54'),
(129, 'phut91', 39, '2014-10-06 11:48:22'),
(130, 'phut91', 39, '2014-10-08 12:00:03'),
(131, 'phut91', 39, '2014-10-09 11:15:22'),
(132, 'phut91', 39, '2014-10-10 06:54:08'),
(133, 'phut91', 39, '2014-10-10 11:50:28'),
(134, 'phut91', 39, '2014-10-11 07:30:23'),
(135, 'phut91', 39, '2014-10-11 08:50:55'),
(136, 'phut91', 39, '2014-10-11 08:55:13'),
(137, 'phut91', 39, '2014-10-12 01:46:06'),
(138, 'phut91', 39, '2014-10-16 18:34:21'),
(139, 'phut91', 39, '2014-10-21 15:05:42'),
(140, 'phut91', 39, '2014-10-22 04:06:20'),
(141, 'phut91', 39, '2014-10-22 13:02:52'),
(142, 'phut91', 39, '2014-10-22 13:28:38'),
(143, 'phut91', 39, '2014-10-23 07:03:31'),
(144, 'phut91', 39, '2014-10-23 14:39:50'),
(145, 'phut91', 39, '2014-10-24 04:17:52'),
(146, 'toilazaydo', 40, '2014-10-24 04:28:18'),
(147, 'phut91', 39, '2014-10-24 12:29:24'),
(148, 'phut91', 39, '2014-10-25 03:14:16'),
(149, 'phut91', 39, '2014-10-25 06:09:28'),
(150, 'toilazaydo', 40, '2014-10-25 06:56:42'),
(151, 'tuyenbui', 41, '2014-10-25 07:52:34'),
(152, 'phut91', 39, '2014-10-26 08:15:05'),
(153, 'toilazaydo', 40, '2014-10-26 08:32:29'),
(154, 'phut91', 39, '2014-10-26 15:52:20'),
(155, 'tuyenbui', 41, '2014-10-26 19:02:06'),
(156, 'phut91', 39, '2014-10-27 01:47:40'),
(157, 'toilazaydo', 40, '2014-10-27 01:47:50'),
(158, 'tuyenbui', 41, '2014-10-27 02:12:37'),
(159, 'phut91', 39, '2014-10-27 07:32:25'),
(160, 'tuyenbui', 41, '2014-10-27 08:14:12'),
(161, 'toilazaydo', 40, '2014-10-27 08:14:48'),
(162, 'toilazaydo', 40, '2014-10-27 13:23:44'),
(163, 'phut91', 39, '2014-10-28 03:05:56'),
(164, 'toilazaydo', 40, '2014-10-28 03:31:41'),
(165, 'toilazaydo', 40, '2014-10-28 04:06:58'),
(166, 'phut91', 39, '2014-10-28 04:08:36'),
(167, 'phut91', 39, '2014-10-28 04:21:03'),
(168, 'phut91', 39, '2014-10-28 06:42:48'),
(169, 'toilazaydo', 40, '2014-10-28 08:01:49'),
(170, 'toilazaydo', 40, '2014-10-28 08:46:02'),
(171, 'tuyenbui', 41, '2014-10-28 08:54:07'),
(172, 'phut91', 39, '2014-10-28 09:11:05'),
(173, 'phut91', 39, '2014-10-28 10:39:36'),
(174, 'phut91', 39, '2014-10-28 10:52:09'),
(175, 'phut91', 39, '2014-10-28 16:08:00'),
(176, 'toilazaydo', 40, '2014-10-28 16:38:55'),
(177, 'phut91', 39, '2014-10-29 02:26:08'),
(178, 'tuyenbui', 41, '2014-10-29 02:41:21'),
(179, 'phut91', 39, '2014-10-29 03:07:51'),
(180, 'toilazaydo', 40, '2014-10-29 04:15:10'),
(181, 'phut91', 39, '2014-10-29 07:54:37'),
(182, 'phut91', 39, '2014-10-29 09:22:56'),
(183, 'phut91', 39, '2014-10-29 09:34:26'),
(184, 'toilazaydo', 40, '2014-10-29 14:18:45'),
(185, 'phut91', 39, '2014-10-29 15:02:25'),
(186, 'phut91', 39, '2014-10-30 01:05:33'),
(187, 'phut91', 39, '2014-10-30 01:55:44'),
(188, 'toilazaydo', 40, '2014-10-30 02:33:23'),
(189, 'phut91', 39, '2014-10-30 03:01:14'),
(190, 'phut91', 39, '2014-10-30 07:58:27'),
(191, 'toilazaydo', 40, '2014-10-30 10:51:37'),
(192, 'phut91', 39, '2014-10-30 16:01:37'),
(193, 'phut91', 39, '2014-10-31 04:01:08'),
(194, 'toilazaydo', 40, '2014-10-31 04:19:52'),
(195, 'phut91', 39, '2014-10-31 04:29:41'),
(196, 'phut91', 39, '2014-10-31 15:46:05'),
(197, 'toilazaydo', 40, '2014-11-01 02:27:29'),
(198, 'phut91', 39, '2014-11-01 03:12:38'),
(199, 'toilazaydo', 40, '2014-11-01 05:52:57'),
(200, 'phut91', 39, '2014-11-01 08:55:39'),
(201, 'toilazaydo', 40, '2014-11-01 09:23:09'),
(202, 'phut91', 39, '2014-11-01 11:08:08'),
(203, 'phut91', 39, '2014-11-01 11:15:07'),
(204, 'tuyenbui', 41, '2014-11-01 11:17:26'),
(205, 'phut91', 39, '2014-11-01 12:56:02'),
(206, 'toilazaydo', 40, '2014-11-01 14:13:03'),
(207, 'phut91', 39, '2014-11-01 15:49:13'),
(208, 'phut91', 39, '2014-11-01 18:18:00'),
(209, 'tuyenbui', 41, '2014-11-01 19:30:13'),
(210, 'tuyenbui', 41, '2014-11-01 19:34:36'),
(211, 'toilazaydo', 40, '2014-11-02 02:02:07'),
(212, 'phut91', 39, '2014-11-02 02:27:57'),
(213, 'tuyenbui', 41, '2014-11-02 04:45:07'),
(214, 'phut91', 39, '2014-11-02 07:01:28'),
(215, 'phut91', 39, '2014-11-02 09:04:44'),
(216, 'phut91', 39, '2014-11-02 10:00:50'),
(217, 'phut91', 39, '2014-11-02 15:56:19'),
(218, 'phut91', 39, '2014-11-02 16:03:47'),
(219, 'toilazaydo', 40, '2014-11-02 16:36:49'),
(220, 'phut91', 39, '2014-11-03 01:16:54'),
(221, 'toilazaydo', 40, '2014-11-03 10:47:37'),
(222, 'tuyenbui', 41, '2014-11-03 16:19:16'),
(223, 'phut91', 39, '2014-11-04 02:08:30'),
(224, 'toilazaydo', 40, '2014-11-04 02:16:11'),
(225, 'phut91', 39, '2014-11-04 02:27:15'),
(226, 'phut91', 39, '2014-11-04 10:03:40'),
(227, 'toilazaydo', 40, '2014-11-04 10:04:39'),
(228, 'toilazaydo', 40, '2014-11-04 15:13:06'),
(229, 'toilazaydo', 40, '2014-11-04 18:40:14'),
(230, 'phut91', 39, '2014-11-05 01:19:48'),
(231, 'toilazaydo', 40, '2014-11-05 07:29:05'),
(232, 'toilazaydo', 40, '2014-11-05 16:23:02'),
(233, 'phut91', 39, '2014-11-06 01:19:24'),
(234, 'phut91', 39, '2014-11-06 04:04:21'),
(235, 'phut91', 39, '2014-11-06 05:55:22'),
(236, 'phut91', 39, '2014-11-06 17:30:25'),
(237, 'phut91', 39, '2014-11-07 01:27:00'),
(238, 'toilazaydo', 40, '2014-11-07 10:27:13'),
(239, 'toilazaydo', 40, '2014-11-07 16:45:24'),
(240, 'toilazaydo', 40, '2014-11-08 01:06:44'),
(241, 'phut91', 39, '2014-11-08 03:19:17'),
(242, 'phut91', 39, '2014-11-08 11:55:30'),
(243, 'phut91', 39, '2014-11-08 13:14:12'),
(244, 'phut91', 39, '2014-11-08 13:40:23'),
(245, 'toilazaydo', 40, '2014-11-08 16:16:12'),
(246, 'toilazaydo', 40, '2014-11-09 02:10:13'),
(247, 'phut91', 39, '2014-11-09 03:13:50'),
(248, 'tuyenbui', 41, '2014-11-09 06:30:55'),
(249, 'toilazaydo', 40, '2014-11-09 11:49:14'),
(250, 'toilazaydo', 40, '2014-11-09 12:56:36'),
(251, 'toilazaydo', 40, '2014-11-09 14:12:38'),
(252, 'toilazaydo', 40, '2014-11-09 16:56:37'),
(253, 'phut91', 39, '2014-11-09 17:56:40'),
(254, 'toilazaydo', 40, '2014-11-10 01:36:24'),
(255, 'phut91', 39, '2014-11-10 01:48:35'),
(256, 'phut91', 39, '2014-11-10 06:25:48'),
(257, 'phut91', 39, '2014-11-10 09:20:47'),
(258, 'tuyenbui', 41, '2014-11-10 13:39:05'),
(259, 'phut91', 39, '2014-11-10 13:59:46'),
(260, 'toilazaydo', 40, '2014-11-10 15:57:35'),
(261, 'phut91', 39, '2014-11-11 01:46:07'),
(262, 'toilazaydo', 40, '2014-11-11 07:17:20'),
(263, 'toilazaydo', 40, '2014-11-11 07:17:27'),
(264, 'phut91', 39, '2014-11-11 07:28:48'),
(265, 'phut91', 39, '2014-11-11 07:28:49'),
(266, 'phut91', 39, '2014-11-11 07:29:02'),
(267, 'phut91', 39, '2014-11-11 07:29:04'),
(268, 'phut91', 39, '2014-11-11 07:30:56'),
(269, 'phut91', 39, '2014-11-11 10:41:51'),
(270, 'tuyenbui', 41, '2014-11-11 12:07:01'),
(271, 'toilazaydo', 40, '2014-11-11 14:17:13'),
(272, 'phut91', 39, '2014-11-11 16:11:43'),
(273, 'phut91', 39, '2014-11-12 12:00:33'),
(274, 'toilazaydo', 40, '2014-11-12 14:50:48'),
(275, 'phut91', 39, '2014-11-12 17:46:32'),
(276, 'phut91', 39, '2014-11-12 17:49:56'),
(277, 'phut91', 39, '2014-11-13 01:34:23'),
(278, 'phut91', 39, '2014-11-13 03:17:04'),
(279, 'phut91', 39, '2014-11-13 08:45:37'),
(280, 'phut91', 39, '2014-11-14 10:28:10'),
(281, 'phut91', 39, '2014-11-14 15:19:28'),
(282, 'phut91', 39, '2014-11-15 01:24:52'),
(283, 'tuyenbui', 41, '2014-11-15 06:19:27'),
(284, 'phut91', 39, '2014-11-15 07:00:32'),
(285, 'phut91', 39, '2014-11-15 07:34:42'),
(286, 'phut91', 39, '2014-11-15 07:37:42'),
(287, 'toilazaydo', 40, '2014-11-15 10:19:16'),
(288, 'toilazaydo', 40, '2014-11-15 16:37:48'),
(289, 'toilazaydo', 40, '2014-11-16 08:52:41'),
(290, 'toilazaydo', 40, '2014-11-17 01:59:50'),
(291, 'toilazaydo', 40, '2014-11-17 09:58:33'),
(292, 'phut91', 39, '2014-11-18 06:05:51'),
(293, 'toilazaydo', 40, '2014-11-18 11:54:10'),
(294, 'toilazaydo', 40, '2014-11-19 01:37:31'),
(295, 'phut91', 39, '2014-11-19 01:48:06'),
(296, 'toilazaydo', 40, '2014-11-19 08:49:46'),
(297, 'phut91', 39, '2014-11-19 08:50:38'),
(298, 'toilazaydo', 40, '2014-11-19 09:43:30'),
(299, 'phut91', 39, '2014-11-20 07:31:27'),
(300, 'toilazaydo', 40, '2014-11-20 13:37:29'),
(301, 'toilazaydo', 40, '2014-11-20 16:46:32'),
(302, 'toilazaydo', 40, '2014-11-21 01:52:51'),
(303, 'phut91', 39, '2014-11-21 02:35:10'),
(304, 'toilazaydo', 40, '2014-11-21 08:35:13'),
(305, 'toilazaydo', 40, '2014-11-21 12:06:27'),
(306, 'phut91', 39, '2014-11-22 06:42:12'),
(307, 'tuyenbui', 41, '2014-11-22 07:26:30'),
(308, 'tuyenbui', 41, '2014-11-22 09:59:00'),
(309, 'phut91', 39, '2014-11-22 10:10:23'),
(310, 'toilazaydo', 40, '2014-11-22 10:52:01'),
(311, 'phut91', 39, '2014-11-22 11:48:00'),
(312, 'toilazaydo', 40, '2014-11-22 11:51:46'),
(313, 'phut91', 39, '2014-11-22 14:33:33'),
(314, 'phut91', 39, '2014-11-22 16:21:01'),
(315, 'tuyenbui', 41, '2014-11-22 19:35:31'),
(316, 'phut91', 39, '2014-11-23 00:34:28'),
(317, 'toilazaydo', 40, '2014-11-23 03:02:57'),
(318, 'phut91', 39, '2014-11-23 03:46:46'),
(319, 'phut91', 39, '2014-11-23 06:23:03'),
(320, 'phut91', 39, '2014-11-23 09:13:18'),
(321, 'toilazaydo', 40, '2014-11-23 11:01:33'),
(322, 'toilazaydo', 40, '2014-11-23 12:05:15'),
(323, 'phut91', 39, '2014-11-23 13:16:38'),
(324, 'toilazaydo', 40, '2014-11-23 15:46:35'),
(325, 'phut91', 39, '2014-11-23 19:11:16'),
(326, 'toilazaydo', 40, '2014-11-24 01:35:02'),
(327, 'tuyenbui', 41, '2014-11-24 02:10:39'),
(328, 'phut91', 39, '2014-11-24 02:37:07'),
(329, 'toilazaydo', 40, '2014-11-24 04:33:08'),
(330, 'toilazaydo', 40, '2014-11-24 10:24:13'),
(331, 'toilazaydo', 40, '2014-11-24 16:15:17'),
(332, 'toilazaydo', 40, '2014-11-25 01:37:33'),
(333, 'phut91', 39, '2014-11-25 02:02:49'),
(334, 'phut91', 39, '2014-11-25 04:11:03'),
(335, 'toilazaydo', 40, '2014-11-25 07:29:12'),
(336, 'phut91', 39, '2014-11-25 11:24:52'),
(337, 'toilazaydo', 40, '2014-11-25 11:51:05'),
(338, 'phut91', 39, '2014-11-25 14:24:27'),
(339, 'toilazaydo', 40, '2014-11-25 16:23:34'),
(340, 'phut91', 39, '2014-11-25 16:50:35'),
(341, 'phut91', 39, '2014-11-25 22:39:31'),
(342, 'phut91', 39, '2014-11-25 23:36:34'),
(343, 'phut91', 39, '2014-11-26 01:15:21'),
(344, 'phut91', 39, '2014-11-26 01:17:33'),
(345, 'toilazaydo', 40, '2014-11-26 01:37:50'),
(346, 'tuyenbui', 41, '2014-11-26 03:14:34'),
(347, 'phut91', 39, '2014-11-26 08:15:10'),
(348, 'toilazaydo', 40, '2014-11-26 09:10:48'),
(349, 'phut91', 39, '2014-11-26 13:38:48'),
(350, 'toilazaydo', 40, '2014-11-26 14:45:51'),
(351, 'phut91', 39, '2014-11-26 16:14:14'),
(352, 'phut91', 39, '2014-11-26 16:54:12'),
(353, 'phut91', 39, '2014-11-26 22:43:53'),
(354, 'phut91', 39, '2014-11-27 00:09:01'),
(355, 'toilazaydo', 40, '2014-11-27 01:35:56'),
(356, 'phut91', 39, '2014-11-27 02:05:44'),
(357, 'phut91', 39, '2014-11-27 08:42:51'),
(358, 'phut91', 39, '2014-11-27 13:44:17'),
(359, 'toilazaydo', 40, '2014-11-27 15:42:19'),
(360, 'toilazaydo', 40, '2014-11-28 01:53:43'),
(361, 'phut91', 39, '2014-11-28 01:59:01'),
(362, 'phut91', 39, '2014-11-28 02:45:09'),
(363, 'phut91', 39, '2014-11-28 03:21:09'),
(364, 'toilazaydo', 40, '2014-11-28 06:19:43'),
(365, 'phut91', 39, '2014-11-28 06:25:39'),
(366, 'phut91', 39, '2014-11-28 06:46:53'),
(367, 'toilazaydo', 40, '2014-11-28 11:14:10'),
(368, 'phut91', 39, '2014-11-28 15:44:19'),
(369, 'toilazaydo', 40, '2014-11-28 16:24:38'),
(370, 'phut91', 39, '2014-11-29 01:19:17'),
(371, 'phut91', 39, '2014-11-29 01:31:18'),
(372, 'phut91', 39, '2014-11-29 01:52:14'),
(373, 'toilazaydo', 40, '2014-11-29 02:42:16'),
(374, 'phut91', 39, '2014-11-29 02:49:21'),
(375, 'phut91', 39, '2014-11-29 04:29:00'),
(376, 'phut91', 39, '2014-11-29 05:48:17'),
(377, 'phut91', 39, '2014-11-29 06:30:25'),
(378, 'tuyenbui', 41, '2014-11-29 07:01:46'),
(379, 'toilazaydo', 40, '2014-11-29 08:50:23'),
(380, 'phut91', 39, '2014-11-29 09:13:36'),
(381, 'toilazaydo', 40, '2014-11-29 11:56:01'),
(382, 'phut91', 39, '2014-11-29 13:07:29'),
(383, 'phut91', 39, '2014-11-29 15:58:31'),
(384, 'toilazaydo', 40, '2014-11-29 18:44:24'),
(385, 'phut91', 39, '2014-11-30 00:42:54'),
(386, 'phut91', 39, '2014-11-30 03:02:34'),
(387, 'toilazaydo', 40, '2014-11-30 04:39:01'),
(388, 'toilazaydo', 40, '2014-11-30 10:35:06'),
(389, 'toilazaydo', 40, '2014-11-30 11:33:54'),
(390, 'phut91', 39, '2014-11-30 12:46:00'),
(391, 'phut91', 39, '2014-11-30 13:48:32'),
(392, 'phut91', 39, '2014-11-30 14:50:31'),
(393, 'phut91', 39, '2014-11-30 16:31:02'),
(394, 'toilazaydo', 40, '2014-12-01 01:33:16'),
(395, 'toilazaydo', 40, '2014-12-01 02:04:07'),
(396, 'phut91', 39, '2014-12-01 02:04:53'),
(397, 'phut91', 39, '2014-12-01 02:09:13'),
(398, 'toilazaydo', 40, '2014-12-01 02:19:09'),
(399, 'phut91', 39, '2014-12-01 06:40:47'),
(400, 'phut91', 39, '2014-12-01 07:53:02'),
(401, 'phut91', 39, '2014-12-01 13:59:24'),
(402, 'phut91', 39, '2014-12-01 14:21:37'),
(403, 'toilazaydo', 40, '2014-12-01 19:27:00'),
(404, 'phut91', 39, '2014-12-01 19:35:31'),
(405, 'toilazaydo', 40, '2014-12-02 02:04:46'),
(406, 'phut91', 39, '2014-12-02 02:17:00'),
(407, 'tuyenbui', 41, '2014-12-02 06:42:12'),
(408, 'phut91', 39, '2014-12-02 07:03:14'),
(409, 'phut91', 39, '2014-12-02 09:40:15'),
(410, 'phut91', 39, '2014-12-02 12:33:09'),
(411, 'toilazaydo', 40, '2014-12-02 14:46:15'),
(412, 'toilazaydo', 40, '2014-12-02 17:27:51'),
(413, 'phut91', 39, '2014-12-02 19:20:57'),
(414, 'phut91', 39, '2014-12-03 01:13:34'),
(415, 'phut91', 39, '2014-12-03 01:16:30'),
(416, 'toilazaydo', 40, '2014-12-03 02:06:38'),
(417, 'tuyenbui', 41, '2014-12-03 04:40:38'),
(418, 'toilazaydo', 40, '2014-12-03 04:43:25'),
(419, 'phut91', 39, '2014-12-03 05:13:24'),
(420, 'phut91', 39, '2014-12-03 06:52:27'),
(421, 'toilazaydo', 40, '2014-12-03 06:55:21'),
(422, 'phut91', 39, '2014-12-03 08:47:24'),
(423, 'phut91', 39, '2014-12-03 09:40:12'),
(424, 'phut91', 39, '2014-12-03 11:55:57'),
(425, 'phut91', 39, '2014-12-03 15:19:40'),
(426, 'toilazaydo', 40, '2014-12-03 15:53:54'),
(427, 'phut91', 39, '2014-12-03 19:24:46'),
(428, 'phut91', 39, '2014-12-03 19:58:23'),
(429, 'toilazaydo', 40, '2014-12-04 01:39:27'),
(430, 'phut91', 39, '2014-12-04 05:41:06'),
(431, 'toilazaydo', 40, '2014-12-04 06:14:28'),
(432, 'toilazaydo', 40, '2014-12-04 13:42:29'),
(433, 'toilazaydo', 40, '2014-12-04 14:27:35'),
(434, 'phut91', 39, '2014-12-04 15:41:44'),
(435, 'phut91', 39, '2014-12-05 01:21:31'),
(436, 'toilazaydo', 40, '2014-12-05 01:37:49'),
(437, 'phut91', 39, '2014-12-05 02:29:07'),
(438, 'phut91', 39, '2014-12-05 03:40:51'),
(439, 'phut91', 39, '2014-12-05 04:42:24'),
(440, 'toilazaydo', 40, '2014-12-05 07:11:24'),
(441, 'phut91', 39, '2014-12-05 07:24:25'),
(442, 'phut91', 39, '2014-12-05 11:03:36'),
(443, 'phut91', 39, '2014-12-05 13:50:49'),
(444, 'phut91', 39, '2014-12-05 14:37:03'),
(445, 'phut91', 39, '2014-12-05 14:50:22'),
(446, 'phut91', 39, '2014-12-06 01:24:20'),
(447, 'toilazaydo', 40, '2014-12-06 02:19:39'),
(448, 'phut91', 39, '2014-12-06 04:59:16'),
(449, 'tuyenbui', 41, '2014-12-06 06:23:25'),
(450, 'toilazaydo', 40, '2014-12-06 06:48:44'),
(451, 'tuyenbui', 41, '2014-12-06 07:02:30'),
(452, 'toilazaydo', 40, '2014-12-06 10:16:52'),
(453, 'toilazaydo', 40, '2014-12-06 12:46:02'),
(454, 'phut91', 39, '2014-12-06 14:47:17'),
(455, 'toilazaydo', 40, '2014-12-07 02:05:53'),
(456, 'phut91', 39, '2014-12-07 03:11:07'),
(457, 'toilazaydo', 40, '2014-12-07 03:51:03'),
(458, 'phut91', 39, '2014-12-07 04:19:12'),
(459, 'phut91', 39, '2014-12-07 04:58:38'),
(460, 'phut91', 39, '2014-12-07 07:04:33'),
(461, 'phut91', 39, '2014-12-07 07:19:15'),
(462, 'toilazaydo', 40, '2014-12-07 09:22:14'),
(463, 'phut91', 39, '2014-12-07 11:38:57'),
(464, 'phut91', 39, '2014-12-07 11:45:40'),
(465, 'toilazaydo', 40, '2014-12-07 13:27:01'),
(466, 'phut91', 39, '2014-12-07 15:11:29'),
(467, 'phut91', 39, '2014-12-07 22:45:27'),
(468, 'toilazaydo', 40, '2014-12-08 01:32:33'),
(469, 'phut91', 39, '2014-12-08 01:38:22'),
(470, 'phut91', 39, '2014-12-08 03:01:02'),
(471, 'toilazaydo', 40, '2014-12-08 06:12:51'),
(472, 'tuyenbui', 41, '2014-12-08 09:12:24'),
(473, 'phut91', 39, '2014-12-08 13:33:48'),
(474, 'toilazaydo', 40, '2014-12-08 13:41:25'),
(475, 'toilazaydo', 40, '2014-12-08 14:28:22'),
(476, 'toilazaydo', 40, '2014-12-09 00:49:44'),
(477, 'toilazaydo', 40, '2014-12-09 01:40:56'),
(478, 'phut91', 39, '2014-12-09 01:49:35'),
(479, 'phut91', 39, '2014-12-09 01:54:24'),
(480, 'tuyenbui', 41, '2014-12-09 02:16:31'),
(481, 'toilazaydo', 40, '2014-12-09 06:32:30'),
(482, 'phut91', 39, '2014-12-09 06:42:56'),
(483, 'toilazaydo', 40, '2014-12-09 08:54:11'),
(484, 'phut91', 39, '2014-12-09 09:55:09'),
(485, 'toilazaydo', 40, '2014-12-09 11:31:15'),
(486, 'toilazaydo', 40, '2014-12-09 13:04:02'),
(487, 'phut91', 39, '2014-12-09 16:02:13'),
(488, 'toilazaydo', 40, '2014-12-09 17:10:04'),
(489, 'toilazaydo', 40, '2014-12-09 19:27:27'),
(490, 'phut91', 39, '2014-12-10 01:21:54'),
(491, 'toilazaydo', 40, '2014-12-10 01:32:14'),
(492, 'tuyenbui', 41, '2014-12-10 03:02:50'),
(493, 'phut91', 39, '2014-12-10 03:28:50'),
(494, 'phut91', 39, '2014-12-10 04:35:51'),
(495, 'toilazaydo', 40, '2014-12-10 06:06:39'),
(496, 'phut91', 39, '2014-12-10 06:53:04'),
(497, 'phut91', 39, '2014-12-10 07:00:28'),
(498, 'phut91', 39, '2014-12-10 07:25:01'),
(499, 'phut91', 39, '2014-12-10 10:50:07'),
(500, 'phut91', 39, '2014-12-10 12:17:11'),
(501, 'phut91', 39, '2014-12-10 14:16:46'),
(502, 'toilazaydo', 40, '2014-12-10 14:53:32'),
(503, 'tuyenbui', 41, '2014-12-10 16:00:41'),
(504, 'phut91', 39, '2014-12-10 19:18:16'),
(505, 'phut91', 39, '2014-12-10 19:35:29'),
(506, 'phut91', 39, '2014-12-11 01:26:17'),
(507, 'phut91', 39, '2014-12-11 01:40:10'),
(508, 'toilazaydo', 40, '2014-12-11 01:51:20'),
(509, 'toilazaydo', 40, '2014-12-11 03:11:02'),
(510, 'toilazaydo', 40, '2014-12-11 03:22:39'),
(511, 'phut91', 39, '2014-12-11 06:17:04'),
(512, 'toilazaydo', 40, '2014-12-11 10:16:37'),
(513, 'toilazaydo', 40, '2014-12-11 12:28:32'),
(514, 'phut91', 39, '2014-12-11 12:32:24'),
(515, 'toilazaydo', 40, '2014-12-11 14:07:02'),
(516, 'phut91', 39, '2014-12-11 22:31:47'),
(517, 'phut91', 39, '2014-12-12 01:38:17'),
(518, 'toilazaydo', 40, '2014-12-12 01:44:01'),
(519, 'phut91', 39, '2014-12-12 02:05:04'),
(520, 'phut91', 39, '2014-12-12 02:17:03'),
(521, 'toilazaydo', 40, '2014-12-12 09:27:43'),
(522, 'phut91', 39, '2014-12-12 10:46:04'),
(523, 'toilazaydo', 40, '2014-12-12 11:39:42'),
(524, 'phut91', 39, '2014-12-12 15:33:20'),
(525, 'phut91', 39, '2014-12-12 19:17:40'),
(526, 'phut91', 39, '2014-12-13 01:34:48'),
(527, 'phut91', 39, '2014-12-13 01:57:11'),
(528, 'phut91', 39, '2014-12-13 03:11:56'),
(529, 'phut91', 39, '2014-12-13 03:13:11'),
(530, 'toilazaydo', 40, '2014-12-13 03:26:51'),
(531, 'phut91', 39, '2014-12-13 05:03:32'),
(532, 'tuyenbui', 41, '2014-12-13 05:06:10'),
(533, 'phut91', 39, '2014-12-13 05:22:19'),
(534, 'toilazaydo', 40, '2014-12-13 11:31:33'),
(535, 'phut91', 39, '2014-12-13 12:32:12'),
(536, 'phut91', 39, '2014-12-13 14:54:58'),
(537, 'phut91', 39, '2014-12-13 14:56:35'),
(538, 'phut91', 39, '2014-12-13 17:01:44'),
(539, 'phut91', 39, '2014-12-13 19:38:21'),
(540, 'phut91', 39, '2014-12-14 00:39:58'),
(541, 'phut91', 39, '2014-12-14 01:31:11'),
(542, 'phut91', 39, '2014-12-14 03:00:09'),
(543, 'toilazaydo', 40, '2014-12-14 03:59:16'),
(544, 'phut91', 39, '2014-12-14 04:23:13'),
(545, 'phut91', 39, '2014-12-14 05:46:30'),
(546, 'phut91', 39, '2014-12-14 07:04:54'),
(547, 'toilazaydo', 40, '2014-12-14 08:48:05'),
(548, 'phut91', 39, '2014-12-14 10:05:57'),
(549, 'toilazaydo', 40, '2014-12-14 10:44:42'),
(550, 'phut91', 39, '2014-12-14 13:22:09'),
(551, 'toilazaydo', 40, '2014-12-14 13:36:35'),
(552, 'toilazaydo', 40, '2014-12-14 15:00:51'),
(553, 'phut91', 39, '2014-12-14 15:31:18'),
(554, 'toilazaydo', 40, '2014-12-14 15:49:35'),
(555, 'tuyenbui', 41, '2014-12-14 16:41:04'),
(556, 'phut91', 39, '2014-12-14 17:28:22'),
(557, 'toilazaydo', 40, '2014-12-14 19:35:30'),
(558, 'toilazaydo', 40, '2014-12-15 01:53:24'),
(559, 'phut91', 39, '2014-12-15 02:39:52'),
(560, 'phut91', 39, '2014-12-15 03:15:21'),
(561, 'phut91', 39, '2014-12-15 07:21:51'),
(562, 'phut91', 39, '2014-12-15 07:34:37'),
(563, 'toilazaydo', 40, '2014-12-15 07:39:57'),
(564, 'phut91', 39, '2014-12-15 08:39:06'),
(565, 'toilazaydo', 40, '2014-12-15 14:57:24'),
(566, 'phut91', 39, '2014-12-15 16:11:36'),
(567, 'phut91', 39, '2014-12-15 16:24:47'),
(568, 'toilazaydo', 40, '2014-12-16 01:31:09'),
(569, 'phut91', 39, '2014-12-16 02:18:15'),
(570, 'phut91', 39, '2014-12-16 03:05:07'),
(571, 'phut91', 39, '2014-12-16 05:07:27'),
(572, 'toilazaydo', 40, '2014-12-16 06:04:23'),
(573, 'toilazaydo', 40, '2014-12-16 09:00:32'),
(574, 'phut91', 39, '2014-12-16 12:23:14'),
(575, 'phut91', 39, '2014-12-16 14:57:31'),
(576, 'phut91', 39, '2014-12-16 15:00:04'),
(577, 'phut91', 39, '2014-12-16 15:29:58'),
(578, 'toilazaydo', 40, '2014-12-16 15:42:54'),
(579, 'toilazaydo', 40, '2014-12-17 01:39:11'),
(580, 'phut91', 39, '2014-12-17 03:04:24'),
(581, 'phut91', 39, '2014-12-17 04:40:00'),
(582, 'phut91', 39, '2014-12-17 10:22:14'),
(583, 'toilazaydo', 40, '2014-12-17 10:25:56'),
(584, 'phut91', 39, '2014-12-17 14:43:32'),
(585, 'toilazaydo', 40, '2014-12-17 15:30:21'),
(586, 'phut91', 39, '2014-12-17 18:45:55'),
(587, 'phut91', 39, '2014-12-18 01:03:44'),
(588, 'tuyenbui', 41, '2014-12-18 01:51:34'),
(589, 'phut91', 39, '2014-12-18 07:27:46'),
(590, 'phut91', 39, '2014-12-18 08:12:49'),
(591, 'toilazaydo', 40, '2014-12-18 08:50:19'),
(592, 'phut91', 39, '2014-12-18 09:00:03'),
(593, 'toilazaydo', 40, '2014-12-18 15:44:19'),
(594, 'phut91', 39, '2014-12-19 02:30:14'),
(595, 'phut91', 39, '2014-12-19 03:11:31'),
(596, 'phut91', 39, '2014-12-19 07:50:27'),
(597, 'toilazaydo', 40, '2014-12-19 08:08:50'),
(598, 'toilazaydo', 40, '2014-12-19 17:03:45'),
(599, 'phut91', 39, '2014-12-19 17:10:00'),
(600, 'phut91', 39, '2014-12-19 17:11:56'),
(601, 'phut91', 39, '2014-12-19 19:06:34'),
(602, 'phut91', 39, '2014-12-19 19:16:03'),
(603, 'toilazaydo', 40, '2014-12-19 20:12:19'),
(604, 'phut91', 39, '2014-12-20 00:35:31'),
(605, 'toilazaydo', 40, '2014-12-20 02:57:49'),
(606, 'tuyenbui', 41, '2014-12-20 10:22:27'),
(607, 'phut91', 39, '2014-12-20 10:55:05'),
(608, 'toilazaydo', 40, '2014-12-20 11:19:46'),
(609, 'toilazaydo', 40, '2014-12-20 18:46:40'),
(610, 'phut91', 39, '2014-12-21 03:44:58'),
(611, 'toilazaydo', 40, '2014-12-21 04:47:19'),
(612, 'phut91', 39, '2014-12-21 08:37:53'),
(613, 'phut91', 39, '2014-12-21 10:47:37'),
(614, 'toilazaydo', 40, '2014-12-21 12:41:08'),
(615, 'phut91', 39, '2014-12-21 13:32:11'),
(616, 'phut91', 39, '2014-12-21 14:03:26'),
(617, 'phut91', 39, '2014-12-21 14:21:33'),
(618, 'phut91', 39, '2014-12-22 01:17:04'),
(619, 'phut91', 39, '2014-12-22 01:55:45'),
(620, 'tuyenbui', 41, '2014-12-22 04:24:23'),
(621, 'toilazaydo', 40, '2014-12-22 10:43:59'),
(622, 'phut91', 39, '2014-12-22 13:26:55'),
(623, 'phut91', 39, '2014-12-22 15:21:15'),
(624, 'toilazaydo', 40, '2014-12-22 15:22:40'),
(625, 'phut91', 39, '2014-12-23 01:22:34'),
(626, 'toilazaydo', 40, '2014-12-23 02:04:10'),
(627, 'phut91', 39, '2014-12-23 04:04:57'),
(628, 'phut91', 39, '2014-12-23 16:30:13'),
(629, 'phut91', 39, '2014-12-24 01:30:35'),
(630, 'phut91', 39, '2014-12-24 07:49:41'),
(631, 'toilazaydo', 40, '2014-12-24 08:23:48'),
(632, 'phut91', 39, '2014-12-24 08:24:53'),
(633, 'phut91', 39, '2014-12-24 10:27:31'),
(634, 'phut91', 39, '2014-12-24 13:39:47'),
(635, 'phut91', 39, '2014-12-24 17:44:27'),
(636, 'toilazaydo', 40, '2014-12-25 01:44:37'),
(637, 'tuyenbui', 41, '2014-12-25 01:55:59'),
(638, 'tuyenbui', 41, '2014-12-25 01:56:30'),
(639, 'phut91', 39, '2014-12-25 04:01:21'),
(640, 'phut91', 39, '2014-12-25 14:01:20'),
(641, 'phut91', 39, '2014-12-26 01:16:59'),
(642, 'toilazaydo', 40, '2014-12-26 02:00:38'),
(643, 'phut91', 39, '2014-12-26 03:12:47'),
(644, 'toilazaydo', 40, '2014-12-26 06:44:42'),
(645, 'tuyenbui', 41, '2014-12-26 07:39:16'),
(646, 'tuyenbui', 41, '2014-12-26 07:43:09'),
(647, 'tuyenbui', 41, '2014-12-26 07:44:13'),
(648, 'tuyenbui', 41, '2014-12-26 07:47:20'),
(649, 'toilazaydo', 40, '2014-12-26 12:05:31'),
(650, 'phut91', 39, '2014-12-26 12:19:52'),
(651, 'phut91', 39, '2014-12-26 14:56:09'),
(652, 'phut91', 39, '2014-12-26 16:01:58'),
(653, 'tuyenbui', 41, '2014-12-26 16:28:03'),
(654, 'phut91', 39, '2014-12-27 00:34:16'),
(655, 'toilazaydo', 40, '2014-12-27 01:50:07'),
(656, 'tuyenbui', 41, '2014-12-27 01:53:58'),
(657, 'phut91', 39, '2014-12-27 02:11:31'),
(658, 'tuyenbui', 41, '2014-12-27 02:20:15'),
(659, 'tuyenbui', 41, '2014-12-27 02:37:46'),
(660, 'phut91', 39, '2014-12-27 05:35:07'),
(661, 'phut91', 39, '2014-12-27 06:58:24'),
(662, 'phut91', 39, '2014-12-27 10:42:38'),
(663, 'phut91', 39, '2014-12-27 15:03:41'),
(664, 'phut91', 39, '2014-12-27 15:15:24'),
(665, 'phut91', 39, '2014-12-27 16:52:58'),
(666, 'phut91', 39, '2014-12-28 01:21:12'),
(667, 'toilazaydo', 40, '2014-12-28 05:49:57'),
(668, 'phut91', 39, '2014-12-28 10:18:50'),
(669, 'toilazaydo', 40, '2014-12-28 10:43:27'),
(670, 'phut91', 39, '2014-12-28 12:55:52'),
(671, 'phut91', 39, '2014-12-28 14:59:23'),
(672, 'toilazaydo', 40, '2014-12-28 16:54:09'),
(673, 'toilazaydo', 40, '2014-12-29 01:35:42'),
(674, 'tuyenbui', 41, '2014-12-29 02:32:24'),
(675, 'phut91', 39, '2014-12-29 03:12:10'),
(676, 'phut91', 39, '2014-12-29 04:57:13'),
(677, 'phut91', 39, '2014-12-29 05:51:39'),
(678, 'toilazaydo', 40, '2014-12-29 08:00:32'),
(679, 'phut91', 39, '2014-12-29 08:45:53'),
(680, 'phut91', 39, '2014-12-29 10:24:18'),
(681, 'phut91', 39, '2014-12-29 13:05:06'),
(682, 'phut91', 39, '2014-12-29 15:16:41'),
(683, 'toilazaydo', 40, '2014-12-29 17:28:55'),
(684, 'phut91', 39, '2014-12-30 01:43:42'),
(685, 'toilazaydo', 40, '2014-12-30 01:55:00'),
(686, 'toilazaydo', 40, '2014-12-30 03:14:49'),
(687, 'toilazaydo', 40, '2014-12-30 06:07:40'),
(688, 'phut91', 39, '2014-12-30 06:24:57'),
(689, 'phut91', 39, '2014-12-30 06:41:50'),
(690, 'phut91', 39, '2014-12-30 10:38:03'),
(691, 'toilazaydo', 40, '2014-12-30 14:41:36'),
(692, 'phut91', 39, '2014-12-30 15:50:06'),
(693, 'tuyenbui', 41, '2014-12-30 16:32:52'),
(694, 'phut91', 39, '2014-12-31 01:17:28'),
(695, 'phut91', 39, '2014-12-31 01:29:28'),
(696, 'toilazaydo', 40, '2014-12-31 01:40:19'),
(697, 'phut91', 39, '2014-12-31 04:00:34'),
(698, 'phut91', 39, '2014-12-31 06:00:04'),
(699, 'phut91', 39, '2014-12-31 06:26:19'),
(700, 'phut91', 39, '2014-12-31 14:39:59'),
(701, 'phut91', 39, '2014-12-31 16:01:40'),
(702, 'toilazaydo', 40, '2015-01-01 04:33:32'),
(703, 'toilazaydo', 40, '2015-01-01 07:28:38'),
(704, 'toilazaydo', 40, '2015-01-01 12:17:30'),
(705, 'phut91', 39, '2015-01-01 14:51:12'),
(706, 'phut91', 39, '2015-01-02 02:04:15'),
(707, 'phut91', 39, '2015-01-02 04:47:34'),
(708, 'toilazaydo', 40, '2015-01-02 06:53:18'),
(709, 'phut91', 39, '2015-01-02 08:01:46'),
(710, 'toilazaydo', 40, '2015-01-02 09:31:03'),
(711, 'toilazaydo', 40, '2015-01-02 09:31:05'),
(712, 'phut91', 39, '2015-01-02 11:53:31'),
(713, 'phut91', 39, '2015-01-02 14:35:47'),
(714, 'phut91', 39, '2015-01-02 17:14:19'),
(715, 'phut91', 39, '2015-01-03 01:08:29'),
(716, 'toilazaydo', 40, '2015-01-03 03:31:52'),
(717, 'phut91', 39, '2015-01-03 06:05:53'),
(718, 'phut91', 39, '2015-01-03 11:20:34'),
(719, 'phut91', 39, '2015-01-03 14:23:51'),
(720, 'toilazaydo', 40, '2015-01-03 14:29:17'),
(721, 'phut91', 39, '2015-01-03 16:04:01'),
(722, 'phut91', 39, '2015-01-04 00:54:58'),
(723, 'phut91', 39, '2015-01-04 01:29:45'),
(724, 'toilazaydo', 40, '2015-01-04 03:47:34'),
(725, 'phut91', 39, '2015-01-04 07:09:35'),
(726, 'toilazaydo', 40, '2015-01-04 07:17:31'),
(727, 'phut91', 39, '2015-01-04 08:21:23'),
(728, 'toilazaydo', 40, '2015-01-04 08:46:43'),
(729, 'phut91', 39, '2015-01-04 14:53:49'),
(730, 'toilazaydo', 40, '2015-01-04 14:56:14'),
(731, 'toilazaydo', 40, '2015-01-05 01:35:35'),
(732, 'phut91', 39, '2015-01-05 02:20:51'),
(733, 'phut91', 39, '2015-01-05 02:40:13'),
(734, 'tuyenbui', 41, '2015-01-05 02:45:09'),
(735, 'toilazaydo', 40, '2015-01-05 08:38:41'),
(736, 'phut91', 39, '2015-01-05 09:52:37'),
(737, 'phut91', 39, '2015-01-05 12:58:05'),
(738, 'toilazaydo', 40, '2015-01-05 14:23:48'),
(739, 'phut91', 39, '2015-01-05 15:09:19'),
(740, 'phut91', 39, '2015-01-05 16:03:21'),
(741, 'toilazaydo', 40, '2015-01-05 16:17:01'),
(742, 'phut91', 39, '2015-01-05 17:53:54'),
(743, 'phut91', 39, '2015-01-06 01:16:14'),
(744, 'toilazaydo', 40, '2015-01-06 01:58:30'),
(745, 'phut91', 39, '2015-01-06 06:31:52'),
(746, 'phut91', 39, '2015-01-06 06:57:02'),
(747, 'phut91', 39, '2015-01-06 11:23:50'),
(748, 'toilazaydo', 40, '2015-01-06 11:43:21'),
(749, 'phut91', 39, '2015-01-06 13:26:55'),
(750, 'phut91', 39, '2015-01-06 13:47:26'),
(751, 'toilazaydo', 40, '2015-01-06 15:53:33'),
(752, 'phut91', 39, '2015-01-06 16:27:10'),
(753, 'phut91', 39, '2015-01-06 17:33:26'),
(754, 'phut91', 39, '2015-01-07 01:04:06'),
(755, 'phut91', 39, '2015-01-07 01:10:59'),
(756, 'toilazaydo', 40, '2015-01-07 02:07:22'),
(757, 'tuyenbui', 41, '2015-01-07 08:03:20'),
(758, 'phut91', 39, '2015-01-07 08:44:46'),
(759, 'phut91', 39, '2015-01-07 14:00:19'),
(760, 'phut91', 39, '2015-01-07 17:15:34'),
(761, 'toilazaydo', 40, '2015-01-07 18:36:29'),
(762, 'phut91', 39, '2015-01-08 01:25:04'),
(763, 'phut91', 39, '2015-01-08 01:37:25'),
(764, 'tuyenbui', 41, '2015-01-08 01:59:30'),
(765, 'toilazaydo', 40, '2015-01-08 05:06:30'),
(766, 'phut91', 39, '2015-01-08 06:35:48'),
(767, 'phut91', 39, '2015-01-08 12:27:59'),
(768, 'phut91', 39, '2015-01-08 17:08:17'),
(769, 'toilazaydo', 40, '2015-01-09 01:51:22'),
(770, 'toilazaydo', 40, '2015-01-09 18:21:55'),
(771, 'phut91', 39, '2015-01-09 18:53:33'),
(772, 'toilazaydo', 40, '2015-01-10 04:30:32'),
(773, 'tuyenbui', 41, '2015-01-10 06:38:12'),
(774, 'phut91', 39, '2015-01-10 07:40:26'),
(775, 'phut91', 39, '2015-01-10 09:58:18'),
(776, 'toilazaydo', 40, '2015-01-10 12:03:09'),
(777, 'toilazaydo', 40, '2015-01-10 14:17:51'),
(778, 'toilazaydo', 40, '2015-01-10 14:25:45'),
(779, 'phut91', 39, '2015-01-10 14:39:27'),
(780, 'tuyenbui', 41, '2015-01-10 14:51:04'),
(781, 'phut91', 39, '2015-01-10 16:44:31'),
(782, 'phut91', 39, '2015-01-11 02:06:56'),
(783, 'phut91', 39, '2015-01-11 05:04:08'),
(784, 'phut91', 39, '2015-01-11 06:16:00'),
(785, 'phut91', 39, '2015-01-11 06:27:04'),
(786, 'phut91', 39, '2015-01-11 07:59:10'),
(787, 'toilazaydo', 40, '2015-01-11 08:14:52'),
(788, 'phut91', 39, '2015-01-11 08:55:25'),
(789, 'phut91', 39, '2015-01-11 10:09:15'),
(790, 'phut91', 39, '2015-01-11 13:47:04'),
(791, 'phut91', 39, '2015-01-11 15:57:08'),
(792, 'phut91', 39, '2015-01-11 16:37:24'),
(793, 'tuyenbui', 41, '2015-01-11 18:17:54'),
(794, 'phut91', 39, '2015-01-11 18:34:26'),
(795, 'phut91', 39, '2015-01-11 21:07:59'),
(796, 'phut91', 39, '2015-01-12 01:04:15'),
(797, 'toilazaydo', 40, '2015-01-12 02:18:22'),
(798, 'toilazaydo', 40, '2015-01-12 06:21:53'),
(799, 'phut91', 39, '2015-01-12 13:43:46'),
(800, 'phut91', 39, '2015-01-12 15:31:41'),
(801, 'phut91', 39, '2015-01-12 19:19:25'),
(802, 'phut91', 39, '2015-01-13 01:39:45'),
(803, 'toilazaydo', 40, '2015-01-13 02:32:15'),
(804, 'phut91', 39, '2015-01-13 03:53:28'),
(805, 'phut91', 39, '2015-01-13 05:59:00'),
(806, 'phut91', 39, '2015-01-13 11:43:13'),
(807, 'phut91', 39, '2015-01-13 12:10:27'),
(808, 'toilazaydo', 40, '2015-01-13 18:35:33'),
(809, 'phut91', 39, '2015-01-14 01:42:56'),
(810, 'toilazaydo', 40, '2015-01-14 02:39:21'),
(811, 'phut91', 39, '2015-01-14 03:04:53'),
(812, 'phut91', 39, '2015-01-14 04:52:36'),
(813, 'toilazaydo', 40, '2015-01-14 06:22:41'),
(814, 'phut91', 39, '2015-01-14 13:44:58'),
(815, 'phut91', 39, '2015-01-14 14:54:18'),
(816, 'toilazaydo', 40, '2015-01-14 15:26:37'),
(817, 'phut91', 39, '2015-01-14 16:55:35'),
(818, 'phut91', 39, '2015-01-15 01:48:32'),
(819, 'toilazaydo', 40, '2015-01-15 02:10:07'),
(820, 'toilazaydo', 40, '2015-01-15 07:29:46'),
(821, 'phut91', 39, '2015-01-15 09:12:36'),
(822, 'phut91', 39, '2015-01-15 15:09:03'),
(823, 'phut91', 39, '2015-01-15 16:22:06'),
(824, 'phut91', 39, '2015-01-15 18:59:23'),
(825, 'phut91', 39, '2015-01-15 23:38:45'),
(826, 'phut91', 39, '2015-01-16 01:24:57'),
(827, 'toilazaydo', 40, '2015-01-16 01:48:10'),
(828, 'phut91', 39, '2015-01-16 17:09:02'),
(829, 'toilazaydo', 40, '2015-01-16 17:29:12'),
(830, 'toilazaydo', 40, '2015-01-16 18:49:03'),
(831, 'phut91', 39, '2015-01-16 18:49:41'),
(832, 'phut91', 39, '2015-01-16 18:59:45'),
(833, 'toilazaydo', 40, '2015-01-17 05:07:54'),
(834, 'phut91', 39, '2015-01-17 05:17:34'),
(835, 'toilazaydo', 40, '2015-01-17 09:08:24'),
(836, 'toilazaydo', 40, '2015-01-17 12:00:54'),
(837, 'toilazaydo', 40, '2015-01-17 12:00:55'),
(838, 'phut91', 39, '2015-01-17 16:21:07'),
(839, 'phut91', 39, '2015-01-17 19:53:23'),
(840, 'phut91', 39, '2015-01-18 03:43:59'),
(841, 'toilazaydo', 40, '2015-01-18 06:49:04'),
(842, 'phut91', 39, '2015-01-18 08:50:49'),
(843, 'toilazaydo', 40, '2015-01-18 10:18:55'),
(844, 'phut91', 39, '2015-01-18 14:10:48'),
(845, 'toilazaydo', 40, '2015-01-18 18:39:47'),
(846, 'phut91', 39, '2015-01-18 19:35:40'),
(847, 'toilazaydo', 40, '2015-01-19 01:54:14'),
(848, 'phut91', 39, '2015-01-19 02:13:21'),
(849, 'phut91', 39, '2015-01-19 06:22:20'),
(850, 'toilazaydo', 40, '2015-01-19 10:15:59'),
(851, 'toilazaydo', 40, '2015-01-19 16:47:08'),
(852, 'phut91', 39, '2015-01-19 18:27:50'),
(853, 'toilazaydo', 40, '2015-01-20 03:22:38'),
(854, 'phut91', 39, '2015-01-20 04:07:52'),
(855, 'toilazaydo', 40, '2015-01-20 08:52:09'),
(856, 'phut91', 39, '2015-01-20 14:00:20'),
(857, 'phut91', 39, '2015-01-20 14:42:05'),
(858, 'toilazaydo', 40, '2015-01-20 17:21:04'),
(859, 'toilazaydo', 40, '2015-01-20 19:16:11'),
(860, 'phut91', 39, '2015-01-21 01:04:11'),
(861, 'toilazaydo', 40, '2015-01-21 02:00:36'),
(862, 'phut91', 39, '2015-01-21 08:30:57'),
(863, 'phut91', 39, '2015-01-21 17:26:08'),
(864, 'toilazaydo', 40, '2015-01-21 17:38:54'),
(865, 'phut91', 39, '2015-01-21 18:31:33'),
(866, 'phut91', 39, '2015-01-22 01:05:05'),
(867, 'toilazaydo', 40, '2015-01-22 01:55:55'),
(868, 'phut91', 39, '2015-01-22 06:11:08'),
(869, 'phut91', 39, '2015-01-22 06:17:58'),
(870, 'phut91', 39, '2015-01-22 14:35:04'),
(871, 'toilazaydo', 40, '2015-01-22 17:40:51'),
(872, 'phut91', 39, '2015-01-23 00:52:20'),
(873, 'toilazaydo', 40, '2015-01-23 02:19:42'),
(874, 'phut91', 39, '2015-01-23 06:12:01'),
(875, 'phut91', 39, '2015-01-23 20:12:03'),
(876, 'phut91', 39, '2015-01-24 02:18:04'),
(877, 'phut91', 39, '2015-01-24 04:53:23'),
(878, 'toilazaydo', 40, '2015-01-24 04:55:15'),
(879, 'phut91', 39, '2015-01-24 09:02:05'),
(880, 'phut91', 39, '2015-01-24 09:22:24'),
(881, 'phut91', 39, '2015-01-24 12:36:36'),
(882, 'toilazaydo', 40, '2015-01-24 13:43:34'),
(883, 'phut91', 39, '2015-01-24 19:56:57'),
(884, 'phut91', 39, '2015-01-25 02:51:47'),
(885, 'toilazaydo', 40, '2015-01-25 13:50:53'),
(886, 'phut91', 39, '2015-01-25 14:02:05'),
(887, 'toilazaydo', 40, '2015-01-25 16:56:54'),
(888, 'phut91', 39, '2015-01-26 00:50:47'),
(889, 'toilazaydo', 40, '2015-01-26 06:14:36'),
(890, 'phut91', 39, '2015-01-26 08:24:58'),
(891, 'phut91', 39, '2015-01-26 11:52:03'),
(892, 'phut91', 39, '2015-01-26 13:33:15'),
(893, 'phut91', 39, '2015-01-26 19:58:56'),
(894, 'phut91', 39, '2015-01-27 00:13:39'),
(895, 'toilazaydo', 40, '2015-01-27 04:46:51'),
(896, 'phut91', 39, '2015-01-27 08:18:54'),
(897, 'tuyenbui', 41, '2015-01-27 10:26:31'),
(898, 'toilazaydo', 40, '2015-01-27 12:33:22'),
(899, 'phut91', 39, '2015-01-27 18:12:47'),
(900, 'phut91', 39, '2015-01-28 05:27:05'),
(901, 'tuyenbui', 41, '2015-01-28 07:01:02'),
(902, 'phut91', 39, '2015-01-28 15:41:38'),
(903, 'toilazaydo', 40, '2015-01-28 17:42:42'),
(904, 'phut91', 39, '2015-01-28 20:09:25'),
(905, 'phut91', 39, '2015-01-29 01:27:15'),
(906, 'phut91', 39, '2015-01-29 01:48:53'),
(907, 'toilazaydo', 40, '2015-01-29 04:48:00'),
(908, 'toilazaydo', 40, '2015-01-29 06:40:52'),
(909, 'phut91', 39, '2015-01-29 12:51:23'),
(910, 'toilazaydo', 40, '2015-01-29 17:34:25'),
(911, 'phut91', 39, '2015-01-30 01:57:20'),
(912, 'phut91', 39, '2015-01-30 02:34:52'),
(913, 'phut91', 39, '2015-01-30 12:11:45'),
(914, 'phut91', 39, '2015-01-30 16:37:27'),
(915, 'toilazaydo', 40, '2015-01-30 18:58:21'),
(916, 'phut91', 39, '2015-01-31 02:41:34'),
(917, 'toilazaydo', 40, '2015-01-31 04:05:42'),
(918, 'tuyenbui', 41, '2015-01-31 11:01:41'),
(919, 'tuyenbui', 41, '2015-02-02 03:29:15'),
(920, 'phut91', 39, '2015-02-02 04:14:15'),
(921, 'phut91', 39, '2015-02-02 06:06:42'),
(922, 'phut91', 39, '2015-02-02 08:07:01'),
(923, 'phut91', 39, '2015-02-03 02:12:43'),
(924, 'tuyenbui', 41, '2015-02-03 02:55:43'),
(925, 'tuyenbui', 41, '2015-02-03 02:33:38'),
(926, 'phut91', 39, '2015-02-03 14:18:22'),
(927, 'tuyenbui', 41, '2015-02-04 02:14:46'),
(928, 'phut91', 39, '2015-02-04 03:26:51'),
(929, 'phut91', 39, '2015-02-04 06:43:55'),
(930, 'tuyenbui', 41, '2015-02-04 08:38:51'),
(931, 'phut91', 39, '2015-02-04 10:21:19'),
(932, 'phut91', 39, '2015-02-04 15:31:42'),
(933, 'phut91', 39, '2015-02-05 02:48:26'),
(934, 'phut91', 39, '2015-02-05 03:20:40'),
(935, 'phut91', 39, '2015-02-05 07:55:15'),
(936, 'tuyenbui', 41, '2015-02-05 08:48:56'),
(937, 'phut91', 39, '2015-02-05 12:38:31'),
(938, 'phut91', 39, '2015-02-06 02:01:01'),
(939, 'tuyenbui', 41, '2015-02-06 03:31:35'),
(940, 'phut91', 39, '2015-02-06 06:21:44'),
(941, 'tuyenbui', 41, '2015-02-06 06:56:41'),
(942, 'phut91', 39, '2015-02-06 15:22:47'),
(943, 'phut91', 39, '2015-02-07 04:10:53'),
(944, 'phut91', 39, '2015-02-07 07:20:45'),
(945, 'phut91', 39, '2015-02-07 12:03:14'),
(946, 'phut91', 39, '2015-02-07 15:59:43'),
(947, 'tuyenbui', 41, '2015-02-08 03:50:59'),
(948, 'phut91', 39, '2015-02-08 05:39:03'),
(949, 'phut91', 39, '2015-02-08 09:42:09'),
(950, 'phut91', 39, '2015-02-08 11:58:59'),
(951, 'phut91', 39, '2015-02-08 13:14:18'),
(952, 'phut91', 39, '2015-02-08 14:48:30'),
(953, 'phut91', 39, '2015-02-08 16:34:45'),
(954, 'tuyenbui', 41, '2015-02-08 18:22:02'),
(955, 'tuyenbui', 41, '2015-02-09 01:57:20'),
(956, 'phut91', 39, '2015-02-09 03:03:27'),
(957, 'phut91', 39, '2015-02-09 13:32:22'),
(958, 'phut91', 39, '2015-02-10 03:05:36'),
(959, 'phut91', 39, '2015-02-10 06:32:49'),
(960, 'phut91', 39, '2015-02-10 09:59:29'),
(961, 'phut91', 39, '2015-02-10 14:47:17'),
(962, 'phut91', 39, '2015-02-10 18:58:56'),
(963, 'phut91', 39, '2015-02-11 02:04:44'),
(964, 'phut91', 39, '2015-02-11 06:59:53'),
(965, 'phut91', 39, '2015-02-11 08:33:53'),
(966, 'phut91', 39, '2015-02-11 10:29:40'),
(967, 'phut91', 39, '2015-02-11 19:23:42'),
(968, 'phut91', 39, '2015-02-12 02:04:41'),
(969, 'tuyenbui', 41, '2015-02-12 02:13:37'),
(970, 'phut91', 39, '2015-02-12 06:29:17'),
(971, 'phut91', 39, '2015-02-12 07:53:37'),
(972, 'phut91', 39, '2015-02-12 08:04:12'),
(973, 'phut91', 39, '2015-02-12 08:05:53'),
(974, 'phut91', 39, '2015-02-12 08:56:48'),
(975, 'tuyenbui', 41, '2015-02-12 10:18:34'),
(976, 'phut91', 39, '2015-02-13 01:44:59'),
(977, 'phut91', 39, '2015-02-13 06:49:46'),
(978, 'tuyenbui', 41, '2015-02-13 09:59:59'),
(979, 'phut91', 39, '2015-02-14 02:28:46'),
(980, 'phut91', 39, '2015-02-14 06:57:26'),
(981, 'phut91', 39, '2015-02-14 10:53:50'),
(982, 'phut91', 39, '2015-02-14 17:15:03'),
(983, 'phut91', 39, '2015-02-14 17:24:17'),
(984, 'phut91', 39, '2015-02-14 19:17:10'),
(985, 'phut91', 39, '2015-02-15 09:13:48'),
(986, 'phut91', 39, '2015-02-15 12:21:38'),
(987, 'phut91', 39, '2015-02-15 17:04:23'),
(988, 'phut91', 39, '2015-02-16 16:13:41'),
(989, 'phut91', 39, '2015-02-16 19:46:40'),
(990, 'phut91', 39, '2015-02-17 13:31:16'),
(991, 'phut91', 39, '2015-02-17 15:22:13'),
(992, 'phut91', 39, '2015-02-18 01:56:02'),
(993, 'phut91', 39, '2015-02-18 18:32:24'),
(994, 'phut91', 39, '2015-02-19 05:19:18'),
(995, 'phut91', 39, '2015-02-21 21:44:08'),
(996, 'phut91', 39, '2015-02-22 05:47:29'),
(997, 'phut91', 39, '2015-02-22 10:52:19'),
(998, 'phut91', 39, '2015-02-23 16:54:26'),
(999, 'phut91', 39, '2015-02-24 01:37:15'),
(1000, 'phut91', 39, '2015-02-24 03:14:48'),
(1001, 'phut91', 39, '2015-02-24 17:51:01'),
(1002, 'tuyenbui', 41, '2015-02-25 02:31:01'),
(1003, 'phut91', 39, '2015-02-25 02:48:42'),
(1004, 'phut91', 39, '2015-02-25 04:54:18'),
(1005, 'tuyenbui', 41, '2015-02-25 07:00:17'),
(1006, 'phut91', 39, '2015-02-25 08:46:56'),
(1007, 'tuyenbui', 41, '2015-02-25 10:27:44'),
(1008, 'phut91', 39, '2015-02-25 16:23:20'),
(1009, 'tuyenbui', 41, '2015-02-25 16:51:21'),
(1010, 'tuyenbui', 41, '2015-02-25 19:05:45'),
(1011, 'tuyenbui', 41, '2015-02-26 01:51:18'),
(1012, 'phut91', 39, '2015-02-26 02:51:23'),
(1013, 'tuyenbui', 41, '2015-02-26 02:57:34'),
(1014, 'phut91', 39, '2015-02-26 06:18:33'),
(1015, 'phut91', 39, '2015-02-26 16:28:40'),
(1016, 'tuyenbui', 41, '2015-02-27 03:05:36'),
(1017, 'tuyenbui', 41, '2015-02-27 09:25:56'),
(1018, 'phut91', 39, '2015-02-27 12:29:07'),
(1019, 'phut91', 39, '2015-02-27 15:56:00'),
(1020, 'phut91', 39, '2015-02-27 18:16:49'),
(1021, 'phut91', 39, '2015-02-28 03:54:56'),
(1022, 'tuyenbui', 41, '2015-02-28 08:02:54'),
(1023, 'phut91', 39, '2015-02-28 11:46:48'),
(1024, 'phut91', 39, '2015-02-28 15:55:08'),
(1025, 'tuyenbui', 41, '2015-02-28 16:00:38'),
(1026, 'tuyenbui', 41, '2015-03-01 03:02:52'),
(1027, 'phut91', 39, '2015-03-01 08:13:49'),
(1028, 'phut91', 39, '2015-03-01 10:29:56'),
(1029, 'phut91', 39, '2015-03-01 14:08:12'),
(1030, 'phut91', 39, '2015-03-01 15:49:02'),
(1031, 'tuyenbui', 41, '2015-03-02 01:38:46'),
(1032, 'phut91', 39, '2015-03-02 02:00:49'),
(1033, 'tuyenbui', 41, '2015-03-02 03:00:50'),
(1034, 'phut91', 39, '2015-03-02 04:32:35'),
(1035, 'phut91', 39, '2015-03-02 04:40:29'),
(1036, 'phut91', 39, '2015-03-02 08:23:28'),
(1037, 'phut91', 39, '2015-03-02 15:13:28'),
(1038, 'tuyenbui', 41, '2015-03-02 15:35:38'),
(1039, 'phut91', 39, '2015-03-03 01:47:15'),
(1040, 'tuyenbui', 41, '2015-03-03 03:30:38'),
(1041, 'phut91', 39, '2015-03-03 06:32:07'),
(1042, 'tuyenbui', 41, '2015-03-03 08:18:13'),
(1043, 'phut91', 39, '2015-03-03 09:18:47'),
(1044, 'toilazaydo', 40, '2015-03-03 10:05:29'),
(1045, 'phut91', 39, '2015-03-03 19:23:52'),
(1046, 'tuyenbui', 41, '2015-03-04 01:57:56'),
(1047, 'phut91', 39, '2015-03-04 02:15:44'),
(1048, 'tuyenbui', 41, '2015-03-04 04:11:59'),
(1049, 'phut91', 39, '2015-03-04 09:21:26'),
(1050, 'phut91', 39, '2015-03-04 14:28:08'),
(1051, 'tuyenbui', 41, '2015-03-04 16:00:06'),
(1052, 'toilazaydo', 40, '2015-03-04 18:19:45'),
(1053, 'phut91', 39, '2015-03-04 21:42:55'),
(1054, 'phut91', 39, '2015-03-04 22:51:53'),
(1055, 'phut91', 39, '2015-03-05 02:30:46'),
(1056, 'tuyenbui', 41, '2015-03-05 04:24:06'),
(1057, 'phut91', 39, '2015-03-05 06:10:07'),
(1058, 'tuyenbui', 41, '2015-03-05 09:51:30'),
(1059, 'phut91', 39, '2015-03-05 17:24:05'),
(1060, 'phut91', 39, '2015-03-06 02:24:34'),
(1061, 'tuyenbui', 41, '2015-03-06 03:27:50'),
(1062, 'phut91', 39, '2015-03-06 06:13:23'),
(1063, 'phut91', 39, '2015-03-06 16:27:13'),
(1064, 'phut91', 39, '2015-03-07 02:00:07'),
(1065, 'phut91', 39, '2015-03-07 12:53:07'),
(1066, 'tuyenbui', 41, '2015-03-07 14:34:59'),
(1067, 'tuyenbui', 41, '2015-03-07 17:09:07'),
(1068, 'toilazaydo', 40, '2015-03-07 18:05:43'),
(1069, 'phut91', 39, '2015-03-08 00:43:36'),
(1070, 'phut91', 39, '2015-03-08 09:45:12'),
(1071, 'tuyenbui', 41, '2015-03-08 10:56:19'),
(1072, 'toilazaydo', 40, '2015-03-08 10:57:34'),
(1073, 'toilazaydo', 40, '2015-03-08 12:55:24'),
(1074, 'phut91', 39, '2015-03-08 14:11:21'),
(1075, 'toilazaydo', 40, '2015-03-08 14:28:36'),
(1076, 'phut91', 39, '2015-03-08 21:10:00'),
(1077, 'phut91', 39, '2015-03-09 03:09:31'),
(1078, 'tuyenbui', 41, '2015-03-09 03:27:51'),
(1079, 'tuyenbui', 41, '2015-03-09 10:19:29'),
(1080, 'phut91', 39, '2015-03-09 11:28:53'),
(1081, 'phut91', 39, '2015-03-09 14:38:32'),
(1082, 'phut91', 39, '2015-03-10 02:38:17'),
(1083, 'tuyenbui', 41, '2015-03-10 02:47:49'),
(1084, 'phut91', 39, '2015-03-10 08:24:09'),
(1085, 'phut91', 39, '2015-03-10 09:56:07'),
(1086, 'toilazaydo', 40, '2015-03-10 11:41:54'),
(1087, 'phut91', 39, '2015-03-11 02:32:41'),
(1088, 'tuyenbui', 41, '2015-03-11 06:14:51'),
(1089, 'phut91', 39, '2015-03-11 06:54:41'),
(1090, 'phut91', 39, '2015-03-11 15:52:40'),
(1091, 'phut91', 39, '2015-03-11 17:14:29'),
(1092, 'phut91', 39, '2015-03-11 18:01:14'),
(1093, 'phut91', 39, '2015-03-11 18:29:48'),
(1094, 'phut91', 39, '2015-03-11 18:44:43'),
(1095, 'toilazaydo', 40, '2015-03-11 19:52:51'),
(1096, 'phut91', 39, '2015-03-12 02:25:53'),
(1097, 'tuyenbui', 41, '2015-03-12 06:46:46'),
(1098, 'toilazaydo', 40, '2015-03-12 07:45:11'),
(1099, 'toilazaydo', 40, '2015-03-12 07:47:08'),
(1100, 'toilazaydo', 40, '2015-03-12 07:47:08'),
(1101, 'phut91', 39, '2015-03-12 15:14:25'),
(1102, 'phut91', 39, '2015-03-12 16:31:03'),
(1103, 'phut91', 39, '2015-03-12 16:41:09'),
(1104, 'phut91', 39, '2015-03-12 17:32:14'),
(1105, 'toilazaydo', 40, '2015-03-12 17:47:22'),
(1106, 'toilazaydo', 40, '2015-03-13 01:46:35'),
(1107, 'phut91', 39, '2015-03-13 02:29:47'),
(1108, 'tuyenbui', 41, '2015-03-13 04:54:26'),
(1109, 'phut91', 39, '2015-03-13 06:16:06'),
(1110, 'phut91', 39, '2015-03-13 16:46:01'),
(1111, 'toilazaydo', 40, '2015-03-14 04:07:17'),
(1112, 'tuyenbui', 41, '2015-03-14 09:26:35'),
(1113, 'phut91', 39, '2015-03-14 09:45:08'),
(1114, 'phut91', 39, '2015-03-14 14:23:40'),
(1115, 'toilazaydo', 40, '2015-03-14 14:24:27'),
(1116, 'toilazaydo', 40, '2015-03-14 18:44:11'),
(1117, 'phut91', 39, '2015-03-15 05:30:17'),
(1118, 'toilazaydo', 40, '2015-03-15 07:02:35'),
(1119, 'phut91', 39, '2015-03-15 07:44:24'),
(1120, 'phut91', 39, '2015-03-15 08:23:32'),
(1121, 'phut91', 39, '2015-03-15 13:22:18'),
(1122, 'phut91', 39, '2015-03-15 15:29:05'),
(1123, 'toilazaydo', 40, '2015-03-15 15:34:49'),
(1124, 'tuyenbui', 41, '2015-03-15 15:41:05'),
(1125, 'toilazaydo', 40, '2015-03-15 16:55:11'),
(1126, 'tuyenbui', 41, '2015-03-16 01:45:05'),
(1127, 'phut91', 39, '2015-03-16 01:57:27'),
(1128, 'tuyenbui', 41, '2015-03-16 02:00:26'),
(1129, 'toilazaydo', 40, '2015-03-16 02:06:11'),
(1130, 'phut91', 39, '2015-03-16 06:28:06'),
(1131, 'phut91', 39, '2015-03-16 17:08:29'),
(1132, 'phut91', 39, '2015-03-16 17:29:43'),
(1133, 'phut91', 39, '2015-03-17 02:18:53'),
(1134, 'tuyenbui', 41, '2015-03-17 03:34:17'),
(1135, 'phut91', 39, '2015-03-17 10:48:26'),
(1136, 'phut91', 39, '2015-03-17 15:14:59'),
(1137, 'toilazaydo', 40, '2015-03-17 19:12:22'),
(1138, 'phut91', 39, '2015-03-18 02:17:56'),
(1139, 'phut91', 39, '2015-03-18 06:07:31'),
(1140, 'phut91', 39, '2015-03-18 16:01:12'),
(1141, 'tuyenbui', 41, '2015-03-18 17:51:09'),
(1142, 'phut91', 39, '2015-03-18 17:58:17'),
(1143, 'phut91', 39, '2015-03-18 18:20:10'),
(1144, 'tuyenbui', 41, '2015-03-18 19:44:14'),
(1145, 'phut91', 39, '2015-03-19 02:00:12'),
(1146, 'tuyenbui', 41, '2015-03-19 02:43:32'),
(1147, 'phut91', 39, '2015-03-19 03:45:08'),
(1148, 'tuyenbui', 41, '2015-03-19 04:08:34'),
(1149, 'phut91', 39, '2015-03-19 06:10:01'),
(1150, 'toilazaydo', 40, '2015-03-19 09:39:24'),
(1151, 'phut91', 39, '2015-03-19 15:03:12'),
(1152, 'toilazaydo', 40, '2015-03-19 17:30:29'),
(1153, 'phut91', 39, '2015-03-20 01:59:08');
INSERT INTO `user_loglogin` (`id`, `user_name`, `user_id`, `last_login`) VALUES
(1154, 'phut91', 39, '2015-03-20 06:09:48'),
(1155, 'tuyenbui', 41, '2015-03-20 06:31:13'),
(1156, 'phut91', 39, '2015-03-20 06:42:25'),
(1157, 'tuyenbui', 41, '2015-03-20 10:11:05'),
(1158, 'phut91', 39, '2015-03-20 10:27:36'),
(1159, 'phut91', 39, '2015-03-20 18:09:15'),
(1160, 'phut91', 39, '2015-03-21 10:26:20'),
(1161, 'tuyenbui', 41, '2015-03-21 11:31:07'),
(1162, 'toilazaydo', 40, '2015-03-21 14:18:41'),
(1163, 'phut91', 39, '2015-03-21 16:47:39'),
(1164, 'toilazaydo', 40, '2015-03-21 17:30:34'),
(1165, 'phut91', 39, '2015-03-22 03:49:28'),
(1166, 'phut91', 39, '2015-03-22 07:45:37'),
(1167, 'phut91', 39, '2015-03-22 10:42:18'),
(1168, 'phut91', 39, '2015-03-22 10:44:01'),
(1169, 'phut91', 39, '2015-03-22 10:57:21'),
(1170, 'phut91', 39, '2015-03-22 12:02:29'),
(1171, 'toilazaydo', 40, '2015-03-22 13:05:32'),
(1172, 'toilazaydo', 40, '2015-03-22 13:50:13'),
(1173, 'phut91', 39, '2015-03-22 15:04:24'),
(1174, 'phut91', 39, '2015-03-22 15:13:22'),
(1175, 'tuyenbui', 41, '2015-03-23 01:56:33'),
(1176, 'phut91', 39, '2015-03-23 03:18:29'),
(1177, 'tuyenbui', 41, '2015-03-23 04:51:19'),
(1178, 'tuyenbui', 41, '2015-03-23 04:51:47'),
(1179, 'toilazaydo', 40, '2015-03-23 06:35:17'),
(1180, 'phut91', 39, '2015-03-23 06:36:54'),
(1181, 'tuyenbui', 41, '2015-03-23 06:47:30'),
(1182, 'tuyenbui', 41, '2015-03-23 08:40:08'),
(1183, 'toilazaydo', 40, '2015-03-23 14:38:33'),
(1184, 'phut91', 39, '2015-03-23 16:26:20'),
(1185, 'phut91', 39, '2015-03-24 01:48:16'),
(1186, 'tuyenbui', 41, '2015-03-24 06:18:02'),
(1187, 'phut91', 39, '2015-03-24 06:46:03'),
(1188, 'phut91', 39, '2015-03-25 02:08:01'),
(1189, 'toilazaydo', 40, '2015-03-25 06:47:09'),
(1190, 'tuyenbui', 41, '2015-03-25 07:16:06'),
(1191, 'tuyenbui', 41, '2015-03-25 08:06:40'),
(1192, 'phut91', 39, '2015-03-25 08:20:43'),
(1193, 'phut91', 39, '2015-03-25 16:36:17'),
(1194, 'phut91', 39, '2015-03-26 02:46:01'),
(1195, 'phut91', 39, '2015-03-26 05:39:35'),
(1196, 'phut91', 39, '2015-03-26 06:31:05'),
(1197, 'phut91', 39, '2015-03-26 18:15:06'),
(1198, 'phut91', 39, '2015-03-27 02:47:24'),
(1199, 'tuyenbui', 41, '2015-03-27 02:56:27'),
(1200, 'phut91', 39, '2015-03-27 03:03:56'),
(1201, 'tuyenbui', 41, '2015-03-27 03:28:50'),
(1202, 'phut91', 39, '2015-03-27 06:06:05'),
(1203, 'phut91', 39, '2015-03-27 11:36:19'),
(1204, 'phut91', 39, '2015-03-27 16:18:03'),
(1205, 'phut91', 39, '2015-03-28 17:42:47'),
(1206, 'phut91', 39, '2015-03-28 18:11:43'),
(1207, 'phut91', 39, '2015-03-29 04:59:53'),
(1208, 'tuyenbui', 41, '2015-03-29 08:24:17'),
(1209, 'phut91', 39, '2015-03-29 15:23:56'),
(1210, 'phut91', 39, '2015-03-29 17:50:36'),
(1211, 'phut91', 39, '2015-03-30 04:01:33'),
(1212, 'tuyenbui', 41, '2015-03-30 04:54:00'),
(1213, 'toilazaydo', 40, '2015-03-30 07:38:57'),
(1214, 'phut91', 39, '2015-03-30 13:47:06'),
(1215, 'phut91', 39, '2015-03-31 01:51:52'),
(1216, 'tuyenbui', 41, '2015-03-31 02:18:54'),
(1217, 'phut91', 39, '2015-03-31 04:43:35'),
(1218, 'phut91', 39, '2015-03-31 06:14:20'),
(1219, 'toilazaydo', 40, '2015-03-31 09:10:39'),
(1220, 'phut91', 39, '2015-03-31 15:57:17'),
(1221, 'phut91', 39, '2015-04-01 01:52:29'),
(1222, 'toilazaydo', 40, '2015-04-01 02:08:49'),
(1223, 'tuyenbui', 41, '2015-04-01 03:33:47'),
(1224, 'tuyenbui', 41, '2015-04-01 03:49:05'),
(1225, 'phut91', 39, '2015-04-01 07:53:19'),
(1226, 'tuyenbui', 41, '2015-04-01 08:59:01'),
(1227, 'phut91', 39, '2015-04-01 09:10:49'),
(1228, 'phut91', 39, '2015-04-01 13:52:29'),
(1229, 'phut91', 39, '2015-04-02 06:09:03'),
(1230, 'phut91', 39, '2015-04-02 13:01:40'),
(1231, 'toilazaydo', 40, '2015-04-03 01:38:43'),
(1232, 'phut91', 39, '2015-04-03 02:02:40'),
(1233, 'tuyenbui', 41, '2015-04-03 03:46:10'),
(1234, 'phut91', 39, '2015-04-03 06:13:35'),
(1235, 'phut91', 39, '2015-04-03 14:24:24'),
(1236, 'phut91', 39, '2015-04-03 19:31:16'),
(1237, 'toilazaydo', 40, '2015-04-04 03:20:24'),
(1238, 'phut91', 39, '2015-04-04 05:03:22'),
(1239, 'phut91', 39, '2015-04-04 06:34:42'),
(1240, 'phut91', 39, '2015-04-04 06:41:12'),
(1241, 'phut91', 39, '2015-04-04 08:27:49'),
(1242, 'phut91', 39, '2015-04-04 11:31:29'),
(1243, 'toilazaydo', 40, '2015-04-04 14:02:30'),
(1244, 'phut91', 39, '2015-04-04 18:11:39'),
(1245, 'phut91', 39, '2015-04-05 05:20:43'),
(1246, 'phut91', 39, '2015-04-05 06:54:43'),
(1247, 'phut91', 39, '2015-04-05 09:42:01'),
(1248, 'phut91', 39, '2015-04-05 11:32:01'),
(1249, 'phut91', 39, '2015-04-05 12:23:20'),
(1250, 'phut91', 39, '2015-04-05 16:52:29'),
(1251, 'phut91', 39, '2015-04-05 18:23:49'),
(1252, 'phut91', 39, '2015-04-05 18:27:01'),
(1253, 'phut91', 39, '2015-04-05 18:28:42'),
(1254, 'tuyenbui', 41, '2015-04-05 18:31:37'),
(1255, 'toilazaydo', 40, '2015-04-05 19:01:02'),
(1256, 'phut91', 39, '2015-04-06 02:03:55'),
(1257, 'phut91', 39, '2015-04-06 04:52:22'),
(1258, 'phut91', 39, '2015-04-06 06:07:17'),
(1259, 'phut91', 39, '2015-04-06 07:33:23'),
(1260, 'phut91', 39, '2015-04-06 10:04:33'),
(1261, 'phut91', 39, '2015-04-06 14:56:44'),
(1262, 'phut91', 39, '2015-04-07 01:51:50'),
(1263, 'phut91', 39, '2015-04-07 02:49:16'),
(1264, 'tuyenbui', 41, '2015-04-07 07:51:36'),
(1265, 'phut91', 39, '2015-04-07 08:49:04'),
(1266, 'phut91', 39, '2015-04-07 17:06:36'),
(1267, 'toilazaydo', 40, '2015-04-07 17:39:11'),
(1268, 'tuyenbui', 41, '2015-04-08 02:45:52'),
(1269, 'phut91', 39, '2015-04-08 03:24:17'),
(1270, 'phut91', 39, '2015-04-08 06:56:59'),
(1271, 'phut91', 39, '2015-04-08 09:06:24'),
(1272, 'toilazaydo', 40, '2015-04-08 16:21:08'),
(1273, 'phut91', 39, '2015-04-08 17:20:25'),
(1274, 'phut91', 39, '2015-04-08 19:15:00'),
(1275, 'phut91', 39, '2015-04-09 02:55:46'),
(1276, 'phut91', 39, '2015-04-09 04:12:59'),
(1277, 'phut91', 39, '2015-04-09 08:03:19'),
(1278, 'phut91', 39, '2015-04-09 08:55:04'),
(1279, 'tuyenbui', 41, '2015-04-09 10:32:30'),
(1280, 'phut91', 39, '2015-04-09 16:58:20'),
(1281, 'phut91', 39, '2015-04-10 01:44:13'),
(1282, 'phut91', 39, '2015-04-10 01:49:03'),
(1283, 'phut91', 39, '2015-04-10 01:54:08'),
(1284, 'tuyenbui', 41, '2015-04-10 01:59:40'),
(1285, 'phut91', 39, '2015-04-10 06:20:41'),
(1286, 'tuyenbui', 41, '2015-04-10 07:45:08'),
(1287, 'phut91', 39, '2015-04-10 14:50:21'),
(1288, 'phut91', 39, '2015-04-10 17:56:00'),
(1289, 'toilazaydo', 40, '2015-04-10 18:53:19'),
(1290, 'phut91', 39, '2015-04-11 11:00:20'),
(1291, 'phut91', 39, '2015-04-11 12:53:47'),
(1292, 'toilazaydo', 40, '2015-04-11 13:20:40'),
(1293, 'phut91', 39, '2015-04-11 15:15:14'),
(1294, 'phut91', 39, '2015-04-11 16:27:13'),
(1295, 'toilazaydo', 40, '2015-04-11 16:28:26'),
(1296, 'phut91', 39, '2015-04-11 18:01:03'),
(1297, 'phut91', 39, '2015-04-12 06:42:02'),
(1298, 'toilazaydo', 40, '2015-04-12 09:18:36'),
(1299, 'phut91', 39, '2015-04-12 09:33:08'),
(1300, 'phut91', 39, '2015-04-12 13:58:13'),
(1301, 'phut91', 39, '2015-04-12 15:38:23'),
(1302, 'phut91', 39, '2015-04-12 16:54:52'),
(1303, 'phut91', 39, '2015-04-12 18:54:55'),
(1304, 'tuyenbui', 41, '2015-04-13 07:01:52'),
(1305, 'phut91', 39, '2015-04-13 14:37:20'),
(1306, 'phut91', 39, '2015-04-13 17:55:52'),
(1307, 'toilazaydo', 40, '2015-04-13 18:28:31'),
(1308, 'phut91', 39, '2015-04-13 18:32:49'),
(1309, 'phut91', 39, '2015-04-14 01:45:36'),
(1310, 'tuyenbui', 41, '2015-04-14 02:48:52'),
(1311, 'tuyenbui', 41, '2015-04-14 09:48:27'),
(1312, 'phut91', 39, '2015-04-14 10:09:18'),
(1313, 'phut91', 39, '2015-04-14 14:44:37'),
(1314, 'toilazaydo', 40, '2015-04-14 18:30:44'),
(1315, 'tuyenbui', 41, '2015-04-15 02:24:22'),
(1316, 'phut91', 39, '2015-04-15 02:28:19'),
(1317, 'phut91', 39, '2015-04-15 07:03:50'),
(1318, 'phut91', 39, '2015-04-15 16:10:18'),
(1319, 'phut91', 39, '2015-04-15 16:59:27'),
(1320, 'tuyenbui', 41, '2015-04-16 04:17:22'),
(1321, 'phut91', 39, '2015-04-16 06:05:38'),
(1322, 'phut91', 39, '2015-04-16 06:40:17'),
(1323, 'tuyenbui', 41, '2015-04-16 07:08:44'),
(1324, 'phut91', 39, '2015-04-16 09:43:17'),
(1325, 'phut91', 39, '2015-04-16 11:48:36'),
(1326, 'phut91', 39, '2015-04-16 15:53:29'),
(1327, 'toilazaydo', 40, '2015-04-16 23:39:47'),
(1328, 'phut91', 39, '2015-04-17 01:44:46'),
(1329, 'tuyenbui', 41, '2015-04-17 02:09:54'),
(1330, 'phut91', 39, '2015-04-17 06:22:13'),
(1331, 'phut91', 39, '2015-04-17 08:19:11'),
(1332, 'phut91', 39, '2015-04-17 08:47:44'),
(1333, 'tuyenbui', 41, '2015-04-17 15:42:16'),
(1334, 'phut91', 39, '2015-04-17 15:47:33'),
(1335, 'phut91', 39, '2015-04-17 17:31:55'),
(1336, 'phut91', 39, '2015-04-17 18:18:53'),
(1337, 'phut91', 39, '2015-04-18 09:00:39'),
(1338, 'phut91', 39, '2015-04-18 10:25:08'),
(1339, 'phut91', 39, '2015-04-18 13:14:29'),
(1340, 'phut91', 39, '2015-04-18 18:05:33'),
(1341, 'phut91', 39, '2015-04-18 19:39:55'),
(1342, 'phut91', 39, '2015-04-18 20:05:12'),
(1343, 'phut91', 39, '2015-04-19 05:00:46'),
(1344, 'phut91', 39, '2015-04-19 08:24:36'),
(1345, 'toilazaydo', 40, '2015-04-19 09:59:59'),
(1346, 'toilazaydo', 40, '2015-04-19 11:45:36'),
(1347, 'phut91', 39, '2015-04-19 13:12:06'),
(1348, 'phut91', 39, '2015-04-19 17:56:41'),
(1349, 'phut91', 39, '2015-04-20 03:09:17'),
(1350, 'phut91', 39, '2015-04-20 04:06:44'),
(1351, 'phut91', 39, '2015-04-20 06:12:42'),
(1352, 'tuyenbui', 41, '2015-04-20 06:49:32'),
(1353, 'trungcode', 43, '2015-04-20 07:59:20'),
(1354, 'tuyenbui', 41, '2015-04-20 09:57:26'),
(1355, 'phut91', 39, '2015-04-20 14:46:32'),
(1356, 'phut91', 39, '2015-04-20 18:24:23'),
(1357, 'phut91', 39, '2015-04-21 02:27:11'),
(1358, 'tuyenbui', 41, '2015-04-21 03:57:00'),
(1359, 'phut91', 39, '2015-04-21 05:00:27'),
(1360, 'phut91', 39, '2015-04-21 06:10:38'),
(1361, 'tuyenbui', 41, '2015-04-21 07:38:41'),
(1362, 'phut91', 39, '2015-04-21 15:00:03'),
(1363, 'tuyenbui', 41, '2015-04-21 15:39:26'),
(1364, 'phut91', 39, '2015-04-21 17:50:06'),
(1365, 'phut91', 39, '2015-04-22 01:48:27'),
(1366, 'tuyenbui', 41, '2015-04-22 02:00:06'),
(1367, 'phut91', 39, '2015-04-22 07:42:06'),
(1368, 'tuyenbui', 41, '2015-04-22 08:25:01'),
(1369, 'phut91', 39, '2015-04-22 09:25:15'),
(1370, 'phut91', 39, '2015-04-22 10:11:23'),
(1371, 'phut91', 39, '2015-04-22 14:40:36'),
(1372, 'phut91', 39, '2015-04-22 18:20:38'),
(1373, 'phut91', 39, '2015-04-23 01:57:22'),
(1374, 'trungcode', 43, '2015-04-23 04:47:39'),
(1375, 'phut91', 39, '2015-04-23 06:05:06'),
(1376, 'phut91', 39, '2015-04-23 17:27:33'),
(1377, 'toilazaydo', 40, '2015-04-23 17:48:13'),
(1378, 'phut91', 39, '2015-04-24 02:24:24'),
(1379, 'toilazaydo', 40, '2015-04-24 03:26:33'),
(1380, 'phut91', 39, '2015-04-24 06:34:25'),
(1381, 'phut91', 39, '2015-04-24 16:11:47'),
(1382, 'phut91', 39, '2015-04-25 01:53:19'),
(1383, 'tuyenbui', 41, '2015-04-25 06:40:41'),
(1384, 'phut91', 39, '2015-04-25 08:18:08'),
(1385, 'phut91', 39, '2015-04-25 10:24:45'),
(1386, 'toilazaydo', 40, '2015-04-25 11:50:09'),
(1387, 'phut91', 39, '2015-04-25 12:32:07'),
(1388, 'tuyenbui', 41, '2015-04-25 12:50:01'),
(1389, 'toilazaydo', 40, '2015-04-25 13:26:47'),
(1390, 'phut91', 39, '2015-04-25 14:43:52'),
(1391, 'phut91', 39, '2015-04-25 15:57:28'),
(1392, 'phut91', 39, '2015-04-25 17:33:49'),
(1393, 'toilazaydo', 40, '2015-04-25 17:43:05'),
(1394, 'phut91', 39, '2015-04-26 08:01:47'),
(1395, 'toilazaydo', 40, '2015-04-26 11:33:38'),
(1396, 'toilazaydo', 40, '2015-04-26 12:36:44'),
(1397, 'phut91', 39, '2015-04-26 14:59:52'),
(1398, 'phut91', 39, '2015-04-26 15:07:25'),
(1399, 'phut91', 39, '2015-04-26 16:04:07'),
(1400, 'toilazaydo', 40, '2015-04-26 18:24:06'),
(1401, 'phut91', 39, '2015-04-27 02:07:29'),
(1402, 'phut91', 39, '2015-04-27 03:42:03'),
(1403, 'tuyenbui', 41, '2015-04-27 04:03:03'),
(1404, 'trungcode', 43, '2015-04-27 06:48:39'),
(1405, 'phut91', 39, '2015-04-27 14:03:02'),
(1406, 'phut91', 39, '2015-04-27 15:16:28'),
(1407, 'phut91', 39, '2015-04-27 17:01:20'),
(1408, 'phut91', 39, '2015-04-27 18:22:50'),
(1409, 'phut91', 39, '2015-04-28 08:45:42'),
(1410, 'phut91', 39, '2015-04-28 14:06:24'),
(1411, 'phut91', 39, '2015-04-28 17:13:32'),
(1412, 'phut91', 39, '2015-04-29 07:49:57'),
(1413, 'phut91', 39, '2015-04-29 13:37:42'),
(1414, 'phut91', 39, '2015-04-29 16:52:34'),
(1415, 'phut91', 39, '2015-05-02 05:52:40'),
(1416, 'phut91', 39, '2015-05-02 11:22:49'),
(1417, 'phut91', 39, '2015-05-02 11:53:53'),
(1418, 'phut91', 39, '2015-05-02 14:07:56'),
(1419, 'phut91', 39, '2015-05-02 14:56:17'),
(1420, 'phut91', 39, '2015-05-02 15:54:32'),
(1421, 'phut91', 39, '2015-05-03 05:29:33'),
(1422, 'phut91', 39, '2015-05-03 06:57:38'),
(1423, 'phut91', 39, '2015-05-03 07:46:58'),
(1424, 'phut91', 39, '2015-05-03 11:44:13'),
(1425, 'toilazaydo', 40, '2015-05-03 12:13:43'),
(1426, 'phut91', 39, '2015-05-03 12:28:37'),
(1427, 'phut91', 39, '2015-05-03 14:33:02'),
(1428, 'toilazaydo', 40, '2015-05-03 17:43:18'),
(1429, 'phut91', 39, '2015-05-04 02:12:42'),
(1430, 'phut91', 39, '2015-05-04 04:00:06'),
(1431, 'tuyenbui', 41, '2015-05-04 08:35:50'),
(1432, 'trungcode', 43, '2015-05-04 10:24:12'),
(1433, 'toilazaydo', 40, '2015-05-04 10:38:45'),
(1434, 'toilazaydo', 40, '2015-05-04 13:19:25'),
(1435, 'toilazaydo', 40, '2015-05-04 17:21:37'),
(1436, 'toilazaydo', 40, '2015-05-05 02:50:57'),
(1437, 'phut91', 39, '2015-05-05 02:52:42'),
(1438, 'toilazaydo', 40, '2015-05-05 02:52:47'),
(1439, 'phut91', 39, '2015-05-05 04:49:07'),
(1440, 'phut91', 39, '2015-05-05 06:24:49'),
(1441, 'tuyenbui', 41, '2015-05-05 07:36:27'),
(1442, 'phut91', 39, '2015-05-05 10:18:27'),
(1443, 'phut91', 39, '2015-05-05 12:48:24'),
(1444, 'phut91', 39, '2015-05-05 17:29:53'),
(1445, 'phut91', 39, '2015-05-06 02:23:37'),
(1446, 'tuyenbui', 41, '2015-05-06 03:31:06'),
(1447, 'phut91', 39, '2015-05-06 04:29:25'),
(1448, 'phut91', 39, '2015-05-06 06:01:25'),
(1449, 'phut91', 39, '2015-05-06 09:40:54'),
(1450, 'phut91', 39, '2015-05-06 10:16:03'),
(1451, 'phut91', 39, '2015-05-06 13:06:03'),
(1452, 'phut91', 39, '2015-05-06 18:38:34'),
(1453, 'phut91', 39, '2015-05-07 07:52:55'),
(1454, 'phut91', 39, '2015-05-07 16:23:15'),
(1455, 'phut91', 39, '2015-05-07 18:35:01'),
(1456, 'toilazaydo', 40, '2015-05-07 18:51:13'),
(1457, 'phut91', 39, '2015-05-08 02:11:01'),
(1458, 'phut91', 39, '2015-05-08 03:39:06'),
(1459, 'tuyenbui', 41, '2015-05-08 06:24:58'),
(1460, 'phut91', 39, '2015-05-08 06:38:56'),
(1461, 'phut91', 39, '2015-05-08 09:28:07'),
(1462, 'phut91', 39, '2015-05-08 17:15:56'),
(1463, 'phut91', 39, '2015-05-09 04:58:00'),
(1464, 'phut91', 39, '2015-05-09 07:36:07'),
(1465, 'phut91', 39, '2015-05-09 11:34:31'),
(1466, 'tuyenbui', 41, '2015-05-09 13:04:58'),
(1467, 'phut91', 39, '2015-05-09 13:50:40'),
(1468, 'phut91', 39, '2015-05-09 15:09:22'),
(1469, 'phut91', 39, '2015-05-09 16:27:29'),
(1470, 'phut91', 39, '2015-05-09 17:33:24'),
(1471, 'phut91', 39, '2015-05-09 18:44:42'),
(1472, 'phut91', 39, '2015-05-10 05:51:28'),
(1473, 'phut91', 39, '2015-05-10 06:51:52'),
(1474, 'toilazaydo', 40, '2015-05-10 09:50:14'),
(1475, 'phut91', 39, '2015-05-10 10:08:09'),
(1476, 'phut91', 39, '2015-05-10 11:35:29'),
(1477, 'phut91', 39, '2015-05-10 14:19:50'),
(1478, 'phut91', 39, '2015-05-10 16:59:06'),
(1479, 'phut91', 39, '2015-05-11 02:03:26'),
(1480, 'tuyenbui', 41, '2015-05-11 02:41:17'),
(1481, 'tuyenbui', 41, '2015-05-11 04:29:50'),
(1482, 'phut91', 39, '2015-05-11 07:49:27'),
(1483, 'phut91', 39, '2015-05-11 15:12:45'),
(1484, 'tuyenbui', 41, '2015-05-12 01:40:40'),
(1485, 'phut91', 39, '2015-05-12 02:18:54'),
(1486, 'phut91', 39, '2015-05-12 04:45:52'),
(1487, 'phut91', 39, '2015-05-12 10:31:38'),
(1488, 'phut91', 39, '2015-05-12 15:20:12'),
(1489, 'phut91', 39, '2015-05-12 18:48:47'),
(1490, 'tuyenbui', 41, '2015-05-13 01:58:58'),
(1491, 'phut91', 39, '2015-05-13 02:44:39'),
(1492, 'phut91', 39, '2015-05-13 08:31:51'),
(1493, 'trungcode', 43, '2015-05-13 08:54:21'),
(1494, 'phut91', 39, '2015-05-13 14:42:41'),
(1495, 'phut91', 39, '2015-05-13 17:30:32'),
(1496, 'tuyenbui', 41, '2015-05-13 18:25:43'),
(1497, 'phut91', 39, '2015-05-13 19:09:38'),
(1498, 'phut91', 39, '2015-05-14 07:01:37'),
(1499, 'tuyenbui', 41, '2015-05-14 07:16:48'),
(1500, 'phut91', 39, '2015-05-14 16:13:33'),
(1501, 'phut91', 39, '2015-05-15 02:17:26'),
(1502, 'tuyenbui', 41, '2015-05-15 02:40:30'),
(1503, 'phut91', 39, '2015-05-15 06:18:30'),
(1504, 'tuyenbui', 41, '2015-05-15 07:03:46'),
(1505, 'phut91', 39, '2015-05-15 10:06:32'),
(1506, 'phut91', 39, '2015-05-15 14:18:13'),
(1507, 'phut91', 39, '2015-05-16 11:50:58'),
(1508, 'phut91', 39, '2015-05-16 18:02:05'),
(1509, 'phut91', 39, '2015-05-17 14:00:38'),
(1510, 'phut91', 39, '2015-05-17 17:08:12'),
(1511, 'phut91', 39, '2015-05-17 18:26:03'),
(1512, 'phut91', 39, '2015-05-18 02:31:30'),
(1513, 'tuyenbui', 41, '2015-05-18 06:29:45'),
(1514, 'phut91', 39, '2015-05-18 07:05:57'),
(1515, 'loc', 44, '2015-05-18 07:32:36'),
(1516, 'loc', 44, '2015-05-18 08:35:54'),
(1517, 'phut91', 39, '2015-05-18 08:42:47'),
(1518, 'phut91', 39, '2015-05-18 15:58:54'),
(1519, 'phut91', 39, '2015-05-19 02:05:44'),
(1520, 'tuyenbui', 39, '2015-05-19 03:08:04'),
(1521, 'tuyenbui', 39, '2015-05-19 07:11:29'),
(1522, 'tuyenbui', 39, '2015-05-20 11:06:51'),
(1523, 'tuyenbui', 39, '2015-05-21 08:18:52'),
(1524, 'tuyenbui', 39, '2015-05-22 02:55:12'),
(1525, 'tuyenbui', 39, '2015-05-23 04:36:09'),
(1526, 'tuyenbui', 39, '2015-05-24 07:34:10'),
(1527, 'tuyenbui', 39, '2015-05-24 16:19:35'),
(1528, 'tuyenbui', 39, '2015-05-25 01:59:15'),
(1529, 'tuyenbui', 39, '2015-05-25 14:19:59'),
(1530, 'tuyenbui', 39, '2015-05-26 01:34:53'),
(1531, 'tuyenbui', 39, '2015-05-27 04:04:20'),
(1532, 'tuyenbui', 39, '2015-05-28 07:26:43'),
(1533, 'tuyenbui', 39, '2015-05-28 14:47:28'),
(1534, 'tuyenbui', 39, '2015-05-29 09:47:50'),
(1535, 'tuyenbui', 39, '2015-05-30 07:32:48'),
(1536, 'tuyenbui', 39, '2015-05-30 09:18:12'),
(1537, 'tuyenbui', 39, '2015-05-30 09:29:53'),
(1538, 'tuyenbui', 39, '2015-05-30 10:42:34'),
(1539, 'tuyenbui', 39, '2015-05-30 12:22:26'),
(1540, 'tuyenbui', 39, '2015-05-30 13:59:10'),
(1541, 'tuyenbui', 39, '2015-05-30 15:04:50'),
(1542, 'tuyenbui', 39, '2015-05-30 15:14:43'),
(1543, 'tuyenbui', 39, '2015-05-30 16:25:32'),
(1544, 'tuyenbui', 39, '2015-05-30 20:18:18'),
(1545, 'tuyenbui', 39, '2015-05-30 20:55:24'),
(1546, 'tuyenbui', 39, '2015-05-31 04:08:50'),
(1547, 'tuyenbui', 39, '2015-05-31 05:21:45'),
(1548, 'tuyenbui', 39, '2015-05-31 11:43:22'),
(1549, 'tuyenbui', 39, '2015-05-31 13:07:52'),
(1550, 'tuyenbui', 39, '2015-06-01 08:20:36'),
(1551, 'tuyenbui', 39, '2015-06-02 02:50:44'),
(1552, 'tuyenbui', 39, '2015-06-02 03:11:46'),
(1553, 'tuyenbui', 39, '2015-06-02 04:37:54'),
(1554, 'tuyenbui', 39, '2015-06-02 14:35:39'),
(1555, 'tuyenbui', 39, '2015-06-03 01:15:04'),
(1556, 'tuyenbui', 39, '2015-06-03 01:17:53'),
(1557, 'tuyenbui', 39, '2015-06-03 01:18:24'),
(1558, 'thienduongkd', 53, '2015-06-03 01:19:19'),
(1559, 'tuyenbui', 39, '2015-06-03 01:23:39'),
(1560, 'thienduongkd', 53, '2015-06-03 01:34:41'),
(1561, 'tuyenbui', 39, '2015-06-03 01:49:34'),
(1562, 'thienduongkd', 53, '2015-06-03 01:55:58'),
(1563, 'thienduongkd', 53, '2015-06-03 01:57:00'),
(1564, 'thienduongkd', 53, '2015-06-03 01:57:57'),
(1565, 'tuyenbui', 39, '2015-06-03 01:59:15'),
(1566, 'thienduongkd', 53, '2015-06-03 01:59:54'),
(1567, 'thienduongkd', 53, '2015-06-03 02:00:31'),
(1568, 'thienduongkd', 53, '2015-06-03 02:17:43'),
(1569, 'tuyenbui', 39, '2015-06-03 02:37:19'),
(1570, 'tuyenbui', 39, '2015-06-03 02:57:01'),
(1571, 'thienduongkd', 69, '2015-06-03 02:58:23'),
(1572, 'thienduongkd', 69, '2015-06-03 03:36:06'),
(1573, 'tuyenbui', 39, '2015-06-03 04:03:03'),
(1574, 'thienduongkd', 69, '2015-06-03 06:26:17'),
(1575, 'tuyenbui', 39, '2015-06-03 06:34:12'),
(1576, 'thienduongkd', 69, '2015-06-03 08:00:56'),
(1577, 'thienduongkd', 69, '2015-06-03 08:56:06'),
(1578, 'thienduongkd', 69, '2015-06-03 10:30:44'),
(1579, 'tuyenbui', 39, '2015-06-04 04:34:32'),
(1580, 'tuyenbui', 39, '2015-06-04 08:17:59'),
(1581, 'thienduongkd', 69, '2015-06-06 02:32:33'),
(1582, 'thienduongkd', 69, '2015-06-06 07:01:24'),
(1583, 'thienduongkd', 69, '2015-06-06 09:39:12'),
(1584, 'thienduongkd', 69, '2015-06-06 14:24:53'),
(1585, 'thienduongkd', 69, '2015-06-07 06:10:43'),
(1586, 'thienduongkd', 69, '2015-06-07 08:32:00'),
(1587, 'thienduongkd', 69, '2015-06-07 09:32:12'),
(1588, 'thienduongkd', 69, '2015-06-07 09:33:03'),
(1589, 'tuyenbui', 39, '2015-06-08 02:05:16'),
(1590, 'thienduongkd', 69, '2015-06-08 11:37:14'),
(1591, 'thienduongkd', 69, '2015-06-08 12:43:14'),
(1592, 'thienduongkd', 69, '2015-06-09 01:27:32'),
(1593, 'thienduongkd', 69, '2015-06-09 01:46:12'),
(1594, 'tuyenbui', 39, '2015-06-09 02:04:32'),
(1595, 'thienduongkd', 69, '2015-06-09 02:05:07'),
(1596, 'thienduongkd', 69, '2015-06-09 11:56:23'),
(1597, 'thienduongkd', 69, '2015-06-11 01:33:55'),
(1598, 'thienduongkd', 69, '2015-06-11 12:30:57'),
(1599, 'thienduongkd', 69, '2015-06-12 02:07:36'),
(1600, 'thienduongkd', 69, '2015-06-12 07:42:30'),
(1601, 'thienduongkd', 69, '2015-06-12 07:53:11'),
(1602, 'thienduongkd', 69, '2015-06-12 09:33:30'),
(1603, 'thienduongkd', 69, '2015-06-12 10:30:58'),
(1604, 'thienduongkd', 69, '2015-06-12 10:31:30'),
(1605, 'thienduongkd', 69, '2015-06-12 10:32:11'),
(1606, 'thienduongkd', 69, '2015-06-12 10:32:26'),
(1607, 'thienduongkd', 69, '2015-06-12 10:51:36'),
(1608, 'thienduongkd', 69, '2015-06-13 08:37:50'),
(1609, 'thienduongkd', 69, '2015-06-13 12:23:49'),
(1610, 'thienduongkd', 69, '2015-06-14 07:00:59'),
(1611, 'thienduongkd', 69, '2015-06-14 08:28:43'),
(1612, 'thienduongkd', 69, '2015-06-14 11:17:58'),
(1613, 'tuyenbui', 39, '2015-06-15 04:43:19'),
(1614, 'thienduongkd', 69, '2015-06-15 08:00:58'),
(1615, 'thienduongkd', 69, '2015-06-15 11:41:23'),
(1616, 'thienduongkd', 69, '2015-06-16 00:50:22'),
(1617, 'thienduongkd', 69, '2015-06-16 01:22:44'),
(1618, 'thienduongkd', 69, '2015-06-16 01:38:22'),
(1619, 'thienduongkd', 69, '2015-06-16 02:32:11'),
(1620, 'thienduongkd', 69, '2015-06-16 10:46:01'),
(1621, 'thienduongkd', 69, '2015-06-16 14:36:34'),
(1622, 'thienduongkd', 69, '2015-06-17 00:20:00'),
(1623, 'thienduongkd', 69, '2015-06-17 04:04:29'),
(1624, 'giabao', 2, '2015-06-17 04:17:09'),
(1625, 'thienduongkd', 69, '2015-06-17 11:59:32'),
(1626, 'thienduongkd', 69, '2015-06-18 00:25:28'),
(1627, 'thienduongkd', 69, '2015-06-18 02:16:07'),
(1628, 'thienduongkd', 69, '2015-06-18 04:11:48'),
(1629, 'thienduongkd', 69, '2015-06-18 12:46:17'),
(1630, 'thienduongkd', 69, '2015-06-19 10:46:26'),
(1631, 'thienduongkd', 69, '2015-06-20 05:08:31'),
(1632, 'thienduongkd', 69, '2015-06-20 07:35:07'),
(1633, 'thienduongkd', 69, '2015-06-20 13:41:27'),
(1634, 'thienduongkd', 69, '2015-06-21 00:01:14'),
(1635, 'giabao', 2, '2015-06-22 07:22:50'),
(1636, 'giabao', 2, '2015-06-22 08:39:06'),
(1637, 'thienduongkd', 69, '2015-06-22 12:03:51'),
(1638, 'thienduongkd', 69, '2015-06-22 14:30:37'),
(1639, 'giabao', 2, '2015-06-23 07:09:49'),
(1640, 'thienduongkd', 69, '2015-06-23 10:15:26'),
(1641, 'thienduongkd', 69, '2015-06-24 12:45:50'),
(1642, 'thienduongkd', 69, '2015-06-24 14:03:37'),
(1643, 'giabao', 2, '2015-06-25 04:45:15'),
(1644, 'giabao', 2, '2015-06-29 02:52:39'),
(1645, 'giabao', 2, '2015-07-01 07:34:02'),
(1646, 'giabao', 2, '2015-07-02 01:43:53'),
(1647, 'giabao', 2, '2015-07-03 10:34:54'),
(1648, 'giabao', 2, '2015-07-03 15:53:34'),
(1649, 'giabao', 2, '2015-07-04 18:42:48'),
(1650, 'giabao', 2, '2015-07-05 11:02:59'),
(1651, 'giabao', 2, '2015-07-06 03:10:26'),
(1652, 'giabao', 2, '2015-07-06 12:02:07'),
(1653, 'giabao', 2, '2015-07-07 02:24:39'),
(1654, 'giabao', 2, '2015-07-07 06:36:42'),
(1655, 'giabao', 2, '2015-07-10 03:44:54'),
(1656, 'giabao', 2, '2015-07-10 03:46:04'),
(1657, 'admin', 71, '2015-07-10 07:27:02'),
(1658, 'giabao', 2, '2015-07-10 14:20:56'),
(1659, 'admin', 71, '2015-07-12 02:04:49'),
(1660, 'admin', 71, '2015-07-12 04:42:26'),
(1661, 'admin', 71, '2015-07-12 05:21:57'),
(1662, 'giabao', 2, '2015-07-12 06:43:08'),
(1663, 'giabao', 2, '2015-07-13 01:30:32'),
(1664, 'admin', 71, '2015-07-13 11:58:48'),
(1665, 'giabao', 2, '2015-07-13 15:15:10'),
(1666, 'admin', 71, '2015-07-14 04:48:03'),
(1667, 'giabao', 2, '2015-07-14 15:57:24'),
(1668, 'giabao', 2, '2015-07-14 16:40:33'),
(1669, 'giabao', 2, '2015-07-15 01:54:04'),
(1670, 'giabao', 2, '2015-07-15 04:28:47'),
(1671, 'giabao', 2, '2015-07-15 06:33:05'),
(1672, 'admin', 71, '2015-07-15 08:15:25'),
(1673, 'admin', 71, '2015-07-15 08:23:10'),
(1674, 'admin', 71, '2015-07-15 09:20:27'),
(1675, 'giabao', 2, '2015-07-15 09:35:50'),
(1676, 'giabao', 2, '2015-07-15 16:51:48'),
(1677, 'giabao', 2, '2015-07-15 17:12:14'),
(1678, 'giabao', 2, '2015-07-16 01:49:03'),
(1679, 'admin', 71, '2015-07-16 01:55:16'),
(1680, 'admin', 71, '2015-07-16 05:26:17'),
(1681, 'admin', 71, '2015-07-16 11:35:12'),
(1682, 'giabao', 2, '2015-07-17 09:56:51'),
(1683, 'giabao', 2, '2015-07-17 09:59:27'),
(1684, 'admin', 71, '2015-07-18 09:01:44'),
(1685, 'admin', 71, '2015-07-19 05:04:27'),
(1686, 'admin', 71, '2015-07-19 05:06:53'),
(1687, 'admin', 71, '2015-07-19 07:02:06'),
(1688, 'admin', 71, '2015-07-19 09:54:16'),
(1689, 'admin', 71, '2015-07-19 13:51:28'),
(1690, 'giabao', 2, '2015-07-20 04:19:56'),
(1691, 'giabao', 2, '2015-07-20 04:23:33'),
(1692, 'giabao', 2, '2015-07-20 05:29:38'),
(1693, 'giabao', 2, '2015-07-20 05:38:25'),
(1694, 'giabao', 2, '2015-07-20 06:50:04'),
(1695, 'admin', 71, '2015-07-20 09:27:21'),
(1696, 'giabao', 2, '2015-07-20 09:30:14'),
(1697, 'admin', 71, '2015-07-20 09:55:18'),
(1698, 'admin', 71, '2015-07-20 10:45:40'),
(1699, 'giabao', 2, '2015-07-21 02:33:28'),
(1700, 'giabao', 2, '2015-07-21 02:51:33'),
(1701, 'admin', 71, '2015-07-21 04:52:28'),
(1702, 'giabao', 2, '2015-07-21 06:17:06'),
(1703, 'giabao', 2, '2015-07-21 08:05:13'),
(1704, 'admin', 71, '2015-07-21 09:23:20'),
(1705, 'giabao', 2, '2015-07-21 09:51:51'),
(1706, 'giabao', 2, '2015-07-21 09:54:18'),
(1707, 'admin', 71, '2015-07-21 15:36:17'),
(1708, 'giabao', 2, '2015-07-22 01:43:27'),
(1709, 'giabao', 2, '2015-07-22 04:03:21'),
(1710, 'giabao', 2, '2015-07-22 08:58:32'),
(1711, 'admin', 71, '2015-07-22 09:33:04'),
(1712, 'admin', 71, '2015-07-22 10:48:16'),
(1713, 'giabao', 2, '2015-07-22 15:16:51'),
(1714, 'admin', 71, '2015-07-27 15:18:05'),
(1715, 'giabao', 2, '2015-07-28 02:21:43'),
(1716, 'giabao', 2, '2015-07-30 02:13:02'),
(1717, 'giabao', 2, '2015-07-30 02:15:14'),
(1718, 'admin', 71, '2015-07-30 02:46:13'),
(1719, 'admin', 71, '2015-07-30 05:13:10'),
(1720, 'giabao', 2, '2015-07-30 08:22:26'),
(1721, 'admin', 71, '2015-07-30 09:38:09'),
(1722, 'giabao', 2, '2015-08-03 03:42:36'),
(1723, 'giabao', 2, '2015-08-04 10:01:54'),
(1724, 'seo', 74, '2015-08-04 10:03:06'),
(1725, 'giabao', 2, '2015-08-04 10:20:42'),
(1726, 'giabao', 2, '2015-08-04 15:04:47'),
(1727, 'giabao', 2, '2015-08-14 09:31:27'),
(1728, 'giabao', 2, '2015-09-01 09:24:37'),
(1729, 'giabao', 2, '2015-09-07 07:10:14'),
(1730, 'admin', 71, '2015-09-08 04:35:44'),
(1731, 'admin', 71, '2015-09-09 02:14:41'),
(1732, 'admin', 71, '2015-09-10 02:26:08'),
(1733, 'admin', 71, '2015-09-13 02:56:00'),
(1734, 'admin', 71, '2015-09-15 16:23:52'),
(1735, 'admin', 71, '2015-09-18 09:29:02'),
(1736, 'admin', 71, '2015-09-20 03:53:25'),
(1737, 'admin', 71, '2015-11-04 13:57:33'),
(1738, 'giabao', 2, '2015-11-12 03:46:24'),
(1739, 'giabao', 2, '2015-11-12 07:28:21'),
(1740, 'huyphung', 73, '2015-11-16 04:04:50'),
(1741, 'huyphung', 73, '2015-11-16 04:21:52'),
(1742, 'support_box', 72, '2015-11-17 15:55:15'),
(1743, 'support_box', 72, '2015-11-18 09:43:46'),
(1744, 'support_box', 72, '2015-11-20 04:08:03'),
(1745, 'huyphung', 73, '2015-12-04 02:41:10'),
(1746, 'huyphung', 73, '2015-12-05 07:45:31'),
(1747, 'giabao', 2, '2015-12-08 01:56:37'),
(1748, 'giabao', 2, '2015-12-09 14:08:20'),
(1749, 'giabao', 2, '2015-12-11 08:57:23'),
(1750, 'giabao', 2, '2015-12-12 02:52:06'),
(1751, 'giabao', 2, '2015-12-13 02:10:53'),
(1752, 'giabao', 2, '2015-12-15 03:08:28'),
(1753, 'giabao', 2, '2015-12-15 15:38:08'),
(1754, 'giabao', 2, '2015-12-16 01:14:15'),
(1755, 'giabao', 2, '2015-12-16 10:59:00'),
(1756, 'giabao', 2, '2015-12-17 02:11:24'),
(1757, 'giabao', 2, '2015-12-17 02:59:12'),
(1758, 'giabao', 2, '2015-12-17 02:59:13'),
(1759, 'giabao', 2, '2015-12-17 07:36:57'),
(1760, 'giabao', 2, '2015-12-17 12:49:03'),
(1761, 'giabao', 2, '2015-12-18 02:54:28'),
(1762, 'giabao', 2, '2015-12-18 06:10:35'),
(1763, 'giabao', 2, '2015-12-18 14:40:32'),
(1764, 'giabao', 2, '2015-12-19 04:28:39'),
(1765, 'giabao', 2, '2015-12-19 04:29:51'),
(1766, 'giabao', 2, '2015-12-19 07:03:57'),
(1767, 'giabao', 2, '2015-12-19 10:16:23'),
(1768, 'giabao', 2, '2015-12-19 13:19:38'),
(1769, 'giabao', 2, '2015-12-20 05:49:00'),
(1770, 'giabao', 2, '2015-12-20 06:42:51'),
(1771, 'giabao', 2, '2015-12-20 14:46:08'),
(1772, 'giabao', 2, '2015-12-20 14:54:27'),
(1773, 'giabao', 2, '2015-12-21 04:04:15'),
(1774, 'giabao', 2, '2015-12-21 04:14:11'),
(1775, 'giabao', 2, '2015-12-21 07:41:20');

-- --------------------------------------------------------

--
-- Table structure for table `vote`
--

CREATE TABLE IF NOT EXISTS `vote` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `from_date` datetime NOT NULL,
  `to_date` datetime NOT NULL,
  `total` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `vote_option`
--

CREATE TABLE IF NOT EXISTS `vote_option` (
  `id` int(11) NOT NULL,
  `vote_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `count` int(11) NOT NULL,
  `sort_order` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vote_option`
--

INSERT INTO `vote_option` (`id`, `vote_id`, `name`, `type`, `count`, `sort_order`) VALUES
(5, 3, '435', '', 0, '1'),
(6, 3, '124', '', 0, '2');

-- --------------------------------------------------------

--
-- Table structure for table `vote_result`
--

CREATE TABLE IF NOT EXISTS `vote_result` (
  `vote_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `ip` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads_banner`
--
ALTER TABLE `ads_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attach`
--
ALTER TABLE `attach`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `slug` (`slug`);

--
-- Indexes for table `color`
--
ALTER TABLE `color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `path` (`path`(255)),
  ADD KEY `date` (`create_date`);

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `judge`
--
ALTER TABLE `judge`
  ADD UNIQUE KEY `product_id` (`product_id`,`mem_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`odd_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slug` (`slug`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_color`
--
ALTER TABLE `product_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_image`
--
ALTER TABLE `product_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_like`
--
ALTER TABLE `product_like`
  ADD UNIQUE KEY `id_member` (`id_member`,`id_product`);

--
-- Indexes for table `product_size`
--
ALTER TABLE `product_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `provider`
--
ALTER TABLE `provider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tag` (`name`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `tags_content`
--
ALTER TABLE `tags_content`
  ADD KEY `tag_id` (`tag_id`),
  ADD KEY `module` (`module`,`obj_id`);

--
-- Indexes for table `urlalias`
--
ALTER TABLE `urlalias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_role_rel`
--
ALTER TABLE `users_role_rel`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_loglogin`
--
ALTER TABLE `user_loglogin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vote`
--
ALTER TABLE `vote`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vote_option`
--
ALTER TABLE `vote_option`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vote_id` (`vote_id`,`sort_order`);

--
-- Indexes for table `vote_result`
--
ALTER TABLE `vote_result`
  ADD PRIMARY KEY (`vote_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ads_banner`
--
ALTER TABLE `ads_banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `attach`
--
ALTER TABLE `attach`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `color`
--
ALTER TABLE `color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13793;
--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=468;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `odd_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `product_color`
--
ALTER TABLE `product_color`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=110;
--
-- AUTO_INCREMENT for table `product_image`
--
ALTER TABLE `product_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `product_size`
--
ALTER TABLE `product_size`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=400;
--
-- AUTO_INCREMENT for table `provider`
--
ALTER TABLE `provider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `urlalias`
--
ALTER TABLE `urlalias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `user_loglogin`
--
ALTER TABLE `user_loglogin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1776;
--
-- AUTO_INCREMENT for table `vote`
--
ALTER TABLE `vote`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vote_option`
--
ALTER TABLE `vote_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

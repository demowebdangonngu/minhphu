!function ($) {
  $(function(){
	// datepicker
	$(".datepicker-input").each(function(){ $(this).datepicker();});
	
	// fontawesome
	$(document).on('click', '.fontawesome-icon-list a', function(e){
		e && e.preventDefault();
	});

	// table select/deselect all
	$(document).on('change', 'table thead [type="checkbox"]', function(e){
		e && e.preventDefault();
		var $table = $(e.target).closest('table'), $checked = $(e.target).is(':checked');
		$('tbody [type="checkbox"]',$table).prop('checked', $checked);
	});

	$('#upload').click(function(){
		var formData = new FormData($('*formId*')[0]);
		$.ajax({
			url: 'script',  //server script to process data
			type: 'POST',
			xhr: function() {  // custom xhr
				myXhr = $.ajaxSettings.xhr();
				if(myXhr.upload){ // if upload property exists
					myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // progressbar
				}
				return myXhr;
			},
			//Ajax events
			success: completeHandler = function(data) {
				/*
				* workaround for crome browser // delete the fakepath
				*/
				if(navigator.userAgent.indexOf('Chrome')) {
					var catchFile = $(":file").val().replace(/C:\\fakepath\\/i, '');
				}
				else {
					var catchFile = $(":file").val();
				}
				var writeFile = $(":file");

				writeFile.html(writer(catchFile));

				$("*setIdOfImageInHiddenInput*").val(data.logo_id);

			},
			error: errorHandler = function() {
				alert("Något gick fel");
			},
			// Form data
			data: formData,
			//Options to tell JQuery not to process data or worry about content-type
			cache: false,
			contentType: false,
			processData: false
		}, 'json');
	});

  });
}(window.jQuery);
$(document).ready(function()
{
    // activate Nestable for list 1
    if ($('#categories').length)
	{
		$('#categories').nestable({
			group: 0,
			maxDepth:2
		}).on('change', updateOutput);
	}
	if ($('#menus').length)
	{
		$('#menus').nestable({
			group: 0,
			maxDepth:1
		}).on('change', updateOutput);
	}
});
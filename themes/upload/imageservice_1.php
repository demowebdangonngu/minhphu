<?php 
error_reporting(1);
//ini_set('display_errors',1);
define('DIR_APP', 'TMV');
include 'image_config.php';
include 'lib/GifCreator.php';
include 'lib/GifFrameExtractor.php';
//include 'lib/image.php';
//include 'lib/ImageWorkshop.php';

$etag = '"' . md5($_GET['p']) . '"';
if (!empty($_SERVER['HTTP_IF_NONE_MATCH']) && $_SERVER['HTTP_IF_NONE_MATCH'] == $etag) {
    header('HTTP/1.1 304 Not Modified');
    header('Content-Length: 0');
    exit;
}

$QRS = explode('/', $_GET['p']);
$req_whtv = explode('x', $QRS[0]);
/* PATERN
 * http://img.hayhaytv.com.vn:81/960x480/img_name
 * http://img.hayhaytv.com.vn:81/960x480x1/img_name
 * http://img.hayhaytv.com.vn:81/960x480xc/img_name
 * http://img.hayhaytv.com.vn:81/960x480xt/img_name
 * http://img.hayhaytv.com.vn:81/960x480xcx1/img_name
 */
if (count($req_whtv) >= 2) {
    $req_action = 'crop';
    $req_w = $req_whtv[0];
    $req_h = $req_whtv[1];
    if (count($req_whtv) >= 3) {
        if ($req_whtv[2] == 't')
            $req_action = 'thumb';
    }
} else
    return false;

    
$req_src = str_replace("http:", "http:/", implode('/', array_slice($QRS, 1)));

if (isset($req_src) && $req_src != "") {
    global $arr_size, $arr_src, $arr_type_image, $arr_image;
    $url = $req_src;
    $width = $req_w;
    $height = $req_h;
    $type_image = $req_action;

    $arr_w_h = $width . '_' . $height;

    if (!in_array($type_image, $arr_type_image)) {
        return false;
    }

    if (!in_array($arr_w_h, $arr_size)) {
        //return false;
    }

    $file_src = explode("/", $url);
    $filename = end($file_src);

    $name = substr($filename, 0, strrpos($filename, '.'));
    $type = substr($filename, strrpos($filename, '.'), strlen($filename));

    if (!in_array($type, $arr_image)) {
        return false;
    }

    $src_plx = explode("/", $url);

    $internal = 0;
    $base = "";

    if ($src_plx[2] == BASE_DOMAIN) {
        $internal = 1;
        $file_src = str_replace(BASE_NAME, "", $url);
    } else if (is_file($base_upload . $url)) {
        $internal = 1;
        $file_src = $base_upload . $url;
    } else {
        $noimage = "0";
        
        $content = file_get_contents($url);
        if ($content == "" || $content == false) {
            $noimage = "1";
        }
        $internal = 0;
    }
    
	$src_ori = $base_upload . $filename;
    if ($internal == '1') {
        $src_internal = $file_src;
        $size = getimagesize($src_internal);

        if (empty($size)) {
            return false;
        }

        $imgW = $size[0];
        $imgH = $size[1];

        if ($arr_w_h == "w_h") {
            $width = $imgW;
            $height = $imgH;
        }

        $content = file_get_contents($src_internal);

        $data = get_headers($src_internal, true);
        # Look up validity
        if (isset($data['Content-Length']))
        # Return file size
            $size_img = (int) $data['Content-Length'];

        $bytes = (int) ($size_img / 1024);
        if ($bytes > 2000) {
            return false;
        }
    } else {
        if ($noimage == "0") {
            if (!in_array($src_plx[2], $arr_src)) {
                return false;
            }
            $src = $url;
        } else {
            $src = $image_default;
        }

        $size = @getimagesize($src);

        if (empty($size)) {
            return false;
        }

        if ($noimage == "0") {
            $data = get_headers($src, true);
            # Look up validity
            if (isset($data['Content-Length']))
            # Return file size
                $size_img = (int) $data['Content-Length'];

            $bytes = (int) ($size_img / 1024);
            if ($bytes > 2000) {
                return false;
            }

            $imgW = $size[0];
            $imgH = $size[1];

            if ($arr_w_h == "w_h") {
                $width = $imgW;
                $height = $imgH;
            }

            mkdir($base_upload_external, 0777);
            $src_ori = $base_upload_external . $filename;
            if (!is_file($src_ori)) {
                $content = file_get_contents($src);
                file_put_contents($src_ori, $content); //343
                chmod($src_ori, 0777);

                if ($size['mime'] != "image/gif") {
                    $img_h = new Image();
                    $img_h->thumb($src_ori, $base_upload_external, $filename, $size[0], $size[1]);
                }
            } else {
                $content = file_get_contents($src_ori);
            }
        } else{
            
            $imgW = $size[0];
            $imgH = $size[1];

            if ($arr_w_h == "w_h") {
                $width = $imgW;
                $height = $imgH;
            }
            
            $content = file_get_contents($image_default);
        }
    }
	
    if ($content == "" || $content == false) {
        return false;
    }

    if ($size['mime'] == "image/jpeg") {
        $image_type = 'image/jpeg';
    } else if ($size['mime'] == "image/png") {
        $image_type = 'image/png';
    } else if ($size['mime'] == "image/gif") {
        $image_type = 'image/gif';
    } else {
        return false;
    }
	//echo $src_ori;exit;
    if ($image_type == 'image/gif') {
        //if (GifFrameExtractor::isAnimatedGif($src_ori)) {
        $gfe = new GifFrameExtractor();
        $gfe->extract($src_ori);

        $retouchedFrames = array();

        foreach ($gfe->getFrames() as $key => $frame) {
            $imgW = imagesx($frame['image']);
            $imgH = imagesy($frame['image']);

            if ($arr_w_h == "w_h") {
                $width = $imgW;
                $height = $imgH;
            }

            ob_start(); //Turn on output buffering
            imagejpeg($frame['image']);

            $output = ob_get_contents(); // get the image as a string in a variable

            ob_end_clean(); //Turn off output buffering and clean it

            $img_bg = imagecreatefromstring($output);
            //ob_flush();
            $newimg = imagecreatetruecolor($width, $height);

            imagecopyresampled($newimg, $img_bg, 0, 0, 0, 0, $width, $height, $imgW, $imgH);

            $retouchedFrames[] = $newimg;
        }

        $gc = new GifCreator();
        $gc->create($retouchedFrames, $gfe->getFrameDurations(), 0);

        header('Last-Modified: ' . gmdate("D, d M Y H:i:s", time() - 3600) . ' GMT');
        header('ETag: ' . $etag);
        header('Cache-control: public');
        header('Content-type: ' . $image_type);
        header('Cache-control: max-age=' . (60 * 60 * 24 * 365));
        header('Expires: ' . gmdate(DATE_RFC1123, time() + 60 * 60 * 24 * 365));

        $gifBinary = $gc->getGif();
        echo $gifBinary;
        exit;
        //}
    }

    $img_bg = imagecreatefromstring($content);

    //copy original image into new image at new size.
    //imagecopyresized($newimg, $img_bg, 0, 0, 0, 0, $width, $height, $imgW, $imgH);
    if ($type_image == "crop") {
        $src_x = $src_y = 0;
        $src_w = $width;
        $src_h = $height;
        $ratio_ori = $imgW / $imgH;
        $ratio = $width / $height;
        $check = 0;

        if ($ratio_ori > $ratio) {
            $ratio = $height / $width;

            $cmp_x = intval(($imgW - ($imgH * $width / $height)) / 2);
            $cmp_y = 0;

            $width_crop = intval($imgH * $width / $height);
            $height_crop = $imgH;
        } else if ($ratio_ori < $ratio) {
            $cmp_x = 0;
            $cmp_y = intval(($imgH - ($imgW * $height / $width)) / 2);

            $width_crop = $imgW;
            $height_crop = intval($imgW * $height / $width);

            if ($height_crop > $imgH) {
                $height_crop = intval($imgH / $ratio);
            }
        } else {
            $check = 1;
        }

        if ($check == 1) {
            $thumb_img = imagecreatetruecolor($width, $height);

            if ($size['mime'] == "image/png" || $size['mime'] == "image/gif") {
                imagealphablending($thumb_img, false);
                imagesavealpha($thumb_img, true);
                $transparent = imagecolorallocatealpha($thumb_img, 155, 155, 155, 27);
                imagefilledrectangle($thumb_img, 0, 0, $width, $height, $transparent);
            }
            imagecopyresampled($thumb_img, $img_bg, 0, 0, 0, 0, $width, $height, $imgW, $imgH);
        } else {
            $newimg = imagecreatetruecolor($width_crop, $height_crop);

            if ($size['mime'] == "image/png") {
                imagealphablending($newimg, false);
                imagesavealpha($newimg, true);
                $transparent = imagecolorallocatealpha($newimg, 155, 155, 155, 27);
                imagefilledrectangle($newimg, 0, 0, $width_crop, $height_crop, $transparent);
            }

            imagecopyresampled($newimg, $img_bg, 0, 0, $cmp_x, $cmp_y, $imgW, $imgH, $imgW, $imgH);

           

            $thumb_img = imagecreatetruecolor($width, $height);

            if ($size['mime'] == "image/png") {
                imagealphablending($thumb_img, false);
                imagesavealpha($thumb_img, true);
                $transparent = imagecolorallocatealpha($thumb_img, 155, 155, 155, 27);
                imagefilledrectangle($thumb_img, 0, 0, $width, $height, $transparent);
            }

            imagecopyresampled($thumb_img, $newimg, 0, 0, 0, 0, $width, $height, $width_crop, $height_crop);
        }
		
		show_image($thumb_img,$size,$image_type);
        
        exit;
    } else if ($type_image == "thumb") {
        $newimg = imagecreatetruecolor($width, $height);

        if ($size['mime'] == "image/png" || $size['mime'] == "image/gif") {
            imagealphablending($newimg, false);
            imagesavealpha($newimg, true);
            $transparent = imagecolorallocatealpha($newimg, 155, 155, 155, 27);
            imagefilledrectangle($newimg, 0, 0, $width, $height, $transparent);
        }

        imagecopyresampled($newimg, $img_bg, 0, 0, 0, 0, $width, $height, $imgW, $imgH);
		show_image($newimg,$size,$image_type);
        
        exit;
    }
}
function show_image($newimg,$size,$image_type){
		header('Last-Modified: ' . gmdate("D, d M Y H:i:s", time() - 3600) . ' GMT');
        header('ETag: ' . $etag);
        header('Cache-control: public');
        header('Content-type: ' . $image_type);
        header('Cache-control: max-age=' . (60 * 60 * 24 * 365));
        header('Expires: ' . gmdate(DATE_RFC1123, time() + 60 * 60 * 24 * 365));
        //ob_start(); //Turn on output buffering
        if ($size['mime'] == "image/jpeg") {
            imagejpeg($newimg, null, 100);
        } else if ($size['mime'] == "image/png") {
            imagepng($newimg, null, 9);
        } else if ($size['mime'] == "image/gif") {
            imagegif($newimg, null, 100);
        } else {
            return false;
        }
		global $thumb_img;
		imagedestroy($thumb_img);
}
?>
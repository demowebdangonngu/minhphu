/**
 * Justboil.me - a TinyMCE image upload plugin
 * ytimages/plugin.js
 *
 * Released under Creative Commons Attribution 3.0 Unported License
 *
 * License: http://creativecommons.org/licenses/by/3.0/
 * Plugin info: http://justboil.me/
 * Author: Viktor Kuzhelnyi
 *
 * Version: 2.3 released 23/06/2013
 */

tinymce.PluginManager.add('ytimages', function(editor, url) {
	
	function yestechBox() {
                if(typeof base_url=='undefined') {
                    alert("base_url not define please define it befor use this  plugin!,\n if you don't understand contact me tuyen.bui@yestech.vn");
                    return;
                }
		editor.windowManager.open({
			title: 'Upload some image',
			file : base_url + 'uploadfile/tinymce?iframe=1',
			width : 500,
			height: 500,
			buttons: [/*{
				text: 'Upload',
				classes:'widget btn primary first abs-layout-item',
				disabled : true,
				onclick: 'close'
			},*/
			{
				text: 'Close',
				onclick: 'close'
			}]
		});
	}
	
	// Add a button that opens a window
	editor.addButton('ytimages', {
		tooltip: 'Tìm những hình vừa mới upload lên',
		icon : 'image',
		text: '',
		onclick: yestechBox
	});

	// Adds a menu item to the tools menu
	editor.addMenuItem('ytimages', {
		text: 'Upload image',
		icon : 'image',
		context: 'insert',
		onclick: yestechBox
	});
});
MTool.Language = {
	L1: "Request Time out.",
	L2: "Parsing JSON Request failed.",
	L3: "You are offline! Please Check Your Network.",
	L4: "Requested URL not found.",
	L5: "Internel Server Error.",
	L6: "Error!",
	L7: "Unknown Error!",
	L8: "Empty",
	L9: "Close"
};
/************************************************************************
New API
************************************************************************/
getNow = function() {
	var currentTime = new Date();
	var Gio = currentTime.getHours();
	var Phut = currentTime.getMinutes();
	Phut = (Phut < 10)?"0" + Phut:Phut;
	var Giay = currentTime.getSeconds();
	Giay = (Giay < 10)?"0" + Giay:Giay;
	var Ngay = currentTime.getDate();
	Ngay = (Ngay < 10)?"0" + Ngay:Ngay;
	var Thang = currentTime.getMonth() + 1;
	Thang = (Thang < 10)?"0" + Thang:Thang;
	var Nam = currentTime.getFullYear();
	var TraVe = Gio + ":" + Phut + ":" + Giay + " " + Ngay + "/" + Thang + "/" + Nam;
	return TraVe;
}
$.fn.resizeComplete = function(callback) {
	var element = this;
	var height = element.height();
	var width = element.width();
	var monitoring = false;
	var timer;
	function monitorResizing() {
		monitoring = true;
		var newHeight = element.height();
		var newWidth = element.width();
		if (newHeight != height || newWidth != width) {
			height = newHeight;
			width = newWidth;
			timer = setTimeout(function() { monitorResizing() },200);
		}
		else {
			monitoring = false;
			clearTimeout(timer);
			callback();
		}
	}
	function onResize() {
		if(monitoring) return;
		monitorResizing();
	}
	if($.browser.mozilla) {
		element.resize(callback);
	}
	else {
		element.resize(onResize);
	}
}
$.fn.getDistanceWidth = function() {
	var distanceWidth = 0;
	
	var marginLeft = parseInt(this.css("marginLeft"));
	distanceWidth += M$.Is.Int(marginLeft) ? marginLeft : 0;
	
	var borderLeftWidth = parseInt(this.css("borderLeftWidth"));
	distanceWidth += M$.Is.Int(borderLeftWidth) ? borderLeftWidth : 0;
	
	var paddingLeft = parseInt(this.css("paddingLeft"));
	distanceWidth += M$.Is.Int(paddingLeft) ? paddingLeft : 0;
	
	var paddingRight = parseInt(this.css("paddingRight"));
	distanceWidth += M$.Is.Int(paddingRight) ? paddingRight : 0;
	
	var borderRightWidth = parseInt(this.css("borderRightWidth"));
	distanceWidth += M$.Is.Int(borderRightWidth) ? borderRightWidth : 0;
	
	var marginRight = parseInt(this.css("marginRight"));
	distanceWidth += M$.Is.Int(marginRight) ? marginRight : 0;
	
	return distanceWidth;
}
$.fn.getDistanceHeight = function() {
	var distanceHeight = 0;
	
	var marginTop = parseInt(this.css("marginTop"));
	distanceHeight += M$.Is.Int(marginTop) ? marginTop : 0;
	
	var borderTopWidth = parseInt(this.css("borderTopWidth"));
	distanceHeight += M$.Is.Int(borderTopWidth) ? borderTopWidth : 0;
	
	var paddingTop = parseInt(this.css("paddingTop"));
	distanceHeight += M$.Is.Int(paddingTop) ? paddingTop : 0;
	
	var paddingBottom = parseInt(this.css("paddingBottom"));
	distanceHeight += M$.Is.Int(paddingBottom) ? paddingBottom : 0;
	
	var borderBottomWidth = parseInt(this.css("borderBottomWidth"));
	distanceHeight += M$.Is.Int(borderBottomWidth) ? borderBottomWidth : 0;
	
	var marginBottom = parseInt(this.css("marginBottom"));
	distanceHeight += M$.Is.Int(marginBottom) ? marginBottom : 0;
	
	return distanceHeight;
}
$.scrollbarWidth=function(){var a,b,c;if(c===undefined){a=$('<div style="width:50px;height:50px;overflow:auto"><div/></div>').appendTo('body');b=a.children();c=b.innerWidth()-b.height(99).innerWidth();a.remove()}return c};
function json_decode (str_json) {
	// Decodes the JSON representation into a PHP value  
	// 
	// version: 1109.2015
	// discuss at: http://phpjs.org/functions/json_decode
	// +      original by: Public Domain (http://www.json.org/json2.js)
	// + reimplemented by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +      improved by: T.J. Leahy
	// +      improved by: Michael White
	// *        example 1: json_decode('[\n    "e",\n    {\n    "pluribus": "unum"\n}\n]');
	// *        returns 1: ['e', {pluribus: 'unum'}]
/*
		http://www.JSON.org/json2.js
		2008-11-19
		Public Domain.
		NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
		See http://www.JSON.org/js.html
	*/
 
	var json = this.window.JSON;
	if (typeof json === 'object' && typeof json.parse === 'function') {
		try {
			return json.parse(str_json);
		} catch (err) {
			if (!(err instanceof SyntaxError)) {
				throw new Error('Unexpected error type in json_decode()');
			}
			this.php_js = this.php_js || {};
			this.php_js.last_error_json = 4; // usable by json_last_error()
			return null;
		}
	}
 
	var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
	var j;
	var text = str_json;
 
	// Parsing happens in four stages. In the first stage, we replace certain
	// Unicode characters with escape sequences. JavaScript handles many characters
	// incorrectly, either silently deleting them, or treating them as line endings.
	cx.lastIndex = 0;
	if (cx.test(text)) {
		text = text.replace(cx, function (a) {
			return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
		});
	}
 
	// In the second stage, we run the text against regular expressions that look
	// for non-JSON patterns. We are especially concerned with '()' and 'new'
	// because they can cause invocation, and '=' because it can cause mutation.
	// But just to be safe, we want to reject all unexpected forms.
	// We split the second stage into 4 regexp operations in order to work around
	// crippling inefficiencies in IE's and Safari's regexp engines. First we
	// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
	// replace all simple value tokens with ']' characters. Third, we delete all
	// open brackets that follow a colon or comma or that begin the text. Finally,
	// we look to see that the remaining characters are only whitespace or ']' or
	// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.
	if ((/^[\],:{}\s]*$/).
	test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@').
	replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
	replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
 
		// In the third stage we use the eval function to compile the text into a
		// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
		// in JavaScript: it can begin a block or an object literal. We wrap the text
		// in parens to eliminate the ambiguity.
		j = eval('(' + text + ')');
 
		return j;
	}
 
	this.php_js = this.php_js || {};
	this.php_js.last_error_json = 4; // usable by json_last_error()
	return null;
}
$.fn.scrollTo = function(callback) {
	var element = this;
	if(element.length) {
		var top = element.position().top;
		$('html,body').animate({scrollTop: top}, "fast");
	}
}
form_add_error_field = function(array, field) {
	for(i in array) {
		if(array[i] == field) return false;
	}
	
	array[array.length] = field;
}
/************************************************************************
####################################################
jquery.MTool Version 1.0.6
Copyright by Méo 2010
Website: meo.com.vn
Building based on
	+ jQuery library at http://jquery.com/
	+ jQuery UI library at http://jqueryui.com/

Public: 2011-09-01 04:32:07
####################################################
************************************************************************/
MTool = M$ = {};

/************************************************************************
WebDevelopeTool 1.0.0
************************************************************************/
MTool.WebDevelopeTool = {
	title: "MTool_WebDevelopeTool_Expand",
	Begin: function() {
		if(M$.CheckIE56()) return;
		if(!$("#MTool_WebDevelopeTool").length && !$("#MTool_WebDevelopeTool_Expand").length) {
			$("body").prepend(
				"<div id=\"MTool_WebDevelopeTool\" style=\"height: 25px;\">"
					+"<div style=\"color: #15428b; font-size: 11px; height: 14px; padding: 5px 18px 4px 18px; width: 100%; background: #bfdbff; border-top: solid #737374 1px; border-bottom: solid #9bbbe3 1px; position: fixed; z-index: 100;\">"
						+"<a onclick=\"M$.WebDevelopeTool.EncodeURI();\">EncodeURI</a>"
					+"</div>"
					+"<div id=\"MTool_WebDevelopeTool_Expand\" title=\"MTool_WebDevelopeTool_Expand\" style=\"z-index: 101;\">Mở rộng</div>"
				+"</div>"
			);
			$("#MTool_WebDevelopeTool_Expand").dialog({
				dialogClass: "MTool_WebDevelopeTool_Expand",
				position: true,
				autoOpen: false,
				closeOnEscape: false,
				width: 500,
				minWidth: 500,
				height: 500,
				minHeight: 500
			});
			$("#MTool_WebDevelopeTool_Expand").bind("dialogresize", M$.WebDevelopeTool.SetTitle);
		}
	},
	SetTitle: function() {
		$("#ui-dialog-title-MTool_WebDevelopeTool_Expand").html(M$.WebDevelopeTool.title + " (" + parseInt($(".MTool_WebDevelopeTool_Expand.ui-dialog").css("width")) + " x " + parseInt($(".MTool_WebDevelopeTool_Expand.ui-dialog").css("height")) + ")");
	},
	EncodeURI: function() {
		$("#MTool_WebDevelopeTool_Expand").dialog("option", "position", true);
		$("#MTool_WebDevelopeTool_Expand").dialog("option", "width", 500);
		$("#MTool_WebDevelopeTool_Expand").dialog("option", "minWidth", 500);
		$("#MTool_WebDevelopeTool_Expand").dialog("option", "height", 160);
		$("#MTool_WebDevelopeTool_Expand").dialog("option", "minHeight", 160);
		
		M$.WebDevelopeTool.title = "EncodeURI";
		M$.WebDevelopeTool.SetTitle();
		
		$("#MTool_WebDevelopeTool_Expand").html(
			"<textarea style=\"width: 100%;\"></textarea>"
			+"<textarea style=\"width: 100%; margin-top: 5px;\"></textarea>"
			+"<center><input type=\"button\" style=\"margin-top: 5px;\" onclick=\"$('#MTool_WebDevelopeTool_Expand textarea:nth-child(2)').val($('#MTool_WebDevelopeTool_Expand textarea:nth-child(1)').val());\" value=\"EncodeURI\" /></center>"
		);
		
		$("#MTool_WebDevelopeTool_Expand input").click(function() {
			$("#MTool_WebDevelopeTool_Expand textarea:nth-child(2)").val(
				encodeURI($("#MTool_WebDevelopeTool_Expand textarea:nth-child(1)").val()).replace(/%20/g, "+").replace(/%0A/g, "\n")
			);
		});
		$("#MTool_WebDevelopeTool_Expand").dialog("open");
	}
};

/************************************************************************
Debug 1.0.0
************************************************************************/
MTool.Debug = {
	Begin: function() {
		if(M$.CheckIE56()) return;
		
		if(!$("#MTool_Debug").length) $("body").prepend("<div id=\"MTool_Debug\" title=\"MTool_Debug\"></div>");
		$("#MTool_Debug").dialog({
			position: M$.Debug.Option_FixedPosition_Position,
			dialogClass: "MTool_Debug",
			autoOpen: false,
			closeOnEscape: false,
			show: "slide",
			hide: "slide",
			height: 300,
			width: 300,
			buttons: [
				{
					text: M$.Language.L8,
					click: function() {
						$(this).html("");
					}
				},
				{
					text: M$.Language.L9,
					click: function() { $(this).dialog("close"); }
				}
			]
		});
	},
	Open: function() {
		if(M$.CheckIE56()) return;
		
		$("#MTool_Debug").dialog("open");
	},
	Close: function() {
		if(M$.CheckIE56()) return;
		
		$("#MTool_Debug").dialog("close");
	},
	Toggle: function() {
		if(M$.CheckIE56()) return;
		
		if($(".MTool_Debug.ui-dialog").css("display") == "none") $("#MTool_Debug").dialog("open");
		else $("#MTool_Debug").dialog("close");
	},
	Append: function(string) {
		if(M$.CheckIE56()) {
			//alert(string);
			return;
		}
		
		M$.Debug.Open();
		$("#MTool_Debug").append("<span style=\"padding: 2px;\">" + string + "</span><br />");
		$("#MTool_Debug span:last").effect("highlight", {}, 3000);
		if($("#MTool_Debug").length) $("#MTool_Debug").scrollTop($("#MTool_Debug")[0].scrollHeight);
	},
	Option: function(string, value) {
		if(M$.CheckIE56()) return;
		
		if(string == "fixedPosition") {
			$("#MTool_Debug").dialog("option", "draggable", !value);
			
			M$.Debug.Option_FixedPosition_Enable(value);
			$("#MTool_Debug").bind("dialogresizestop", function(event, ui) {
				M$.Debug.Option_FixedPosition_Enable(value);
			});
			
			if(value) {
				M$.Debug.Option_FixedPosition_Position = value;
				M$.Debug.Option_FixedPosition_SetPosition();
				
				$(window).bind("resize", M$.Debug.Option_FixedPosition_SetPosition);
				$("#MTool_Debug").bind("dialogresize", M$.Debug.Option_FixedPosition_SetPosition);
			}
			else {
				$(window).unbind("resize", M$.Debug.Option_FixedPosition_SetPosition);
				$("#MTool_Debug").unbind("dialogresize", M$.Debug.Option_FixedPosition_SetPosition);
			}
		}
		
		if(string == "width") {
			$("#MTool_Debug").dialog("option", "width", value);
		}
		
		if(string == "height") {
			$("#MTool_Debug").dialog("option", "height", value);
		}
	},
	Option_FixedPosition_Position: ['left', 'top'],
	Option_FixedPosition_Enable: function(value) {
		if(M$.CheckIE56()) return;
		
		if(value) $(".MTool_Debug.ui-dialog").css({position:"fixed"});
		else $(".MTool_Debug.ui-dialog").css({position:"absolute"});
	},
	Option_FixedPosition_SetPosition: function() {
		if(M$.CheckIE56()) return;
		
		$("#MTool_Debug").dialog("option", "position", M$.Debug.Option_FixedPosition_Position);
	}
};

/************************************************************************
Is 1.0.0
************************************************************************/
MTool.Is = {
	Int: function(x) {
		var y = parseInt(x);
		if(isNaN(y)) return false;
		return x == y && x.toString() == y.toString();
	},
	PositiveInt: function(x) {
		return /^\d+$/.test(x);
	}
}

/************************************************************************
CheckIE56 1.0.0
************************************************************************/
MTool.CheckIE56 = function() {
	if(/MSIE [56].*Windows/.test(navigator.userAgent)) return true;
	return false;
}

/************************************************************************
CheckBrowser 1.0.0
************************************************************************/
MTool.CheckBrowser = function(name, ver) {
	if(name == "IE") {
		if(ver) {
			switch(ver) {
				case 6:
					if(/MSIE [6].*Windows/.test(navigator.userAgent)) return true;
					return false;
				case 7:
					if(/MSIE [7].*Windows/.test(navigator.userAgent)) return true;
					return false;
				case 8:
					if(/MSIE [8].*Windows/.test(navigator.userAgent)) return true;
					return false;
				case 9:
					if(/MSIE [9].*Windows/.test(navigator.userAgent)) return true;
					return false;
				case 67:
					if(/MSIE [67].*Windows/.test(navigator.userAgent)) return true;
					return false;
				case 678:
					if(/MSIE [678].*Windows/.test(navigator.userAgent)) return true;
					return false;
				case 9:
					if(/MSIE [9].*Windows/.test(navigator.userAgent)) return true;
					return false;
			}
		}
		else {
			if(/MSIE.*Windows/.test(navigator.userAgent)) return true;
			return false;
		}
	}
	else return false;
}

/************************************************************************
GetHash 1.0.1
************************************************************************/
MTool.GetHash = function() {
	var url = window.location.href;
	var flag = url.indexOf("#");
	if(flag == "-1") return null;
	url = url.substring(flag);
	if(!url.substring(1)) return null;
	return url;
}

/************************************************************************
SetHash 1.0.0
************************************************************************/
MTool.SetHash = function(hash) {
	window.location.hash = hash;
}

/************************************************************************
UrlVariables 1.0.1
************************************************************************/
MTool.UrlVariables = {
	GetHash: function(url) {
		var flag = url.indexOf("#");
		if(flag == "-1") return null;
		url = url.substring(flag);
		return url;
	},
	GetQueryString: function(url) {
		var flag = url.indexOf("#");
		if(flag != "-1") {
			url = url.substring(0, flag);
		}
		flag = url.indexOf("?");
		if(flag == "-1") return null;
		url = url.substring(flag + 1);
		if(url == "") return null;
		return url;
	},
	GetFullScriptName: function(url) {
		var flag = url.indexOf("#");
		if(flag != "-1") {
			url = url.substring(0, flag);
		}
		flag = url.indexOf("?");
		if(flag != "-1") {
			url = url.substring(0, flag);
		}
		return url;
	},
	SetRandomKey: function(url, name) {
		return M$.UrlVariables.SetRequest(url, name, Math.random());
	},
	GetRequest: function(url, name) {
		var query_string = M$.UrlVariables.GetQueryString(url);
		if(!query_string) return null;
		var request_split = query_string.split("&");
		for(i = request_split.length - 1; i >= 0; i--) {
			if(request_split[i] == "") continue;
			var flag = request_split[i].indexOf("=");
			if(flag == "-1") {
				if(request_split[i] == name) return "";
				else continue;
			}
			var request_name = request_split[i].substring(0, flag);
			if(request_name != name) continue;
			var request = request_split[i].substring(flag + 1);
			return request;
		}
		return null;
	},
	SetRequest: function(url, name, value) {
		var url_temp = M$.UrlVariables.GetFullScriptName(url);
		var query_string = M$.UrlVariables.GetQueryString(url);
		if(!query_string) url_temp += "?" + name + "=" + value;
		else {
			if(M$.UrlVariables.GetRequest(url, name)) {
				var request_split = query_string.split("&");
				var flag;
				for(i = request_split.length - 1; i >= 0; i--) {
					if(request_split[i] == "") continue;
					var flag1 = request_split[i].indexOf("=");
					if(flag1 == "-1") {
						if(request_split[i] == name) {
							flag = i;
							break;
						}
						else continue;
					}
					var request_name = request_split[i].substring(0, flag1);
					if(request_name != name) continue;
					flag = i;
					break;
				}
				for(i = 0; i < request_split.length; i++) {
					if(i == 0) url_temp += "?";
					else url_temp += "&";
					if(i == flag) url_temp += name + "=" + value;
					else url_temp += request_split[i];
				}
			}
			else url_temp += "?" + M$.UrlVariables.GetQueryString(url) + "&" + name + "=" + value;
		}
		if(M$.UrlVariables.GetHash(url)) url_temp += M$.UrlVariables.GetHash(url);
		return url_temp;
	}
};

/************************************************************************
RightMenu_XP 1.0.0
require:
	* jquery.rightClick.js
	* MTool_RightMenu_XP_1152x864.css
	* Extend.png
************************************************************************/
MTool.RightMenu_XP = {
	active: false,
	css_dir: "",
	css: "MTool_RightMenu_XP_1152x864.css",
	image_dir: "",
	Begin: function() {
		if(!this.active) {
			this.active = true;
			$("head").append("<link rel=\"stylesheet\" type=\"text/css\" href=\"" + this.css_dir + this.css + "\" />");
			setTimeout("M$.RightMenu_XP.Begin_TimeOut()", 9);
		}
	},
	Begin_TimeOut: function() {
		$(".MTool_RightMenu_XP").css("opacity", 0);
	},
	BeginMenu: function(id) {
		$("body").prepend("<div id=\"" + id + "\" class=\"MTool_RightMenu_XP\"></div>");
		$("#" + id).mousedown(function(e) {
			return false;
		});
		$(document).mousedown(function(e) {
			if(!$.browser.mozilla || ($.browser.mozilla && e.button != 2)) {
				M$.RightMenu_XP.Hide(id);
			}
		});
	},
	AddFunction: function(id, image, content, extend, thaotac) {
		var insert = "<div class=\"Function\">";
		insert += "<span class=\"Image\">";
		if(image) insert += "<img width=\"13\" src=\"" + image + "\" />";
		insert += "</span>";
		insert += "<span class=\"Content\">";
		if(content[0]) insert += content[0];
		insert += "</span>";
		insert += "<span class=\"Extend\">";
		if(extend) insert += "<img src=\"" + this.image_dir + "Extend.png\" />";
		insert += "</span>";
		insert += "</div>";
		$("#" + id).append(insert);
		if(content[1] == "disable") $(".MTool_RightMenu_XP .Function:last span.Content").addClass("Content_Disable");
		$(".MTool_RightMenu_XP .Function:last").noContext()
		.mouseenter(function() {
			$(this).addClass("Function_Hover");
		}).mouseleave(function() {
			$(this).removeClass("Function_Hover");
		}).mouseup(function(e) {
			if(e.button == 0 || ($.browser.msie && e.button == 1)) {
				if($("span.Content", this).hasClass("Content_Disable")) return false;
				else if(thaotac) {
					thaotac();
				}
				M$.RightMenu_XP.Hide(id);
			}
		});
	},
	EndMenu: function(id) {
		if(/MSIE [567].*Windows/.test(navigator.userAgent)) {
			setTimeout("M$.RightMenu_XP.EndMenu_TimeOut('" + id + "')", 9);
		}
	},
	EndMenu_TimeOut: function(id) {
		var FixWidth = 0;
		$("#" + id + " .Function span.Content").each(function() {
			if($(this).outerWidth(true) > FixWidth) FixWidth = $(this).outerWidth(true);
		});
		var FixWidth = $(".MTool_RightMenu_XP .Function span.Image").outerWidth(true) + FixWidth + $(".MTool_RightMenu_XP .Function span.Extend").outerWidth(true);
		$(".MTool_RightMenu_XP .Function").css("width", FixWidth + "px");
	},
	Show: function(id, X, Y) {
		$("#" + id).css("margin-left", X - parseFloat($("body").css("margin-left"))).css("margin-top", Y - parseFloat($("body").css("margin-top")));
		$("#" + id).stop().css("visibility", "visible").animate({ opacity: 1 }, 100);
	},
	Hide: function(id) {
		$("#" + id).stop().css("visibility", "hidden").animate({ opacity: 0 });
	},
	Combine: function(Object, id) {
		$(Object).css("cursor", "default");
		$(Object).rightClick( function(e) {
			setTimeout("M$.RightMenu_XP.Show(\"" + id + "\", " + e.pageX + ", " + e.pageY + ")");
			return false;
		});
	}
};

/************************************************************************
Fix
-------------------------------------------------------------------------------------------
Fix.IE.png 1.0.2
require:
	* blank.gif

Fix.IE.minWidthHeight 1.0.0

Fix.IE.minWidth 1.0.0

Fix.All.aHref 1.0.2
************************************************************************/
MTool.Fix = {
	IE: {
		blank: "images/blank.gif",
		pngImage: function(obj, url) {
			if(!obj.length) return;
			
			if(/MSIE [56].*Windows/.test(navigator.userAgent)) {
				var blank;
				if(url) blank = url;
				else blank = this.blank;
				
				var src = obj.attr("src");
				if (!/\.png$/.test(src))
					return;
				obj.css({
					width: obj.width(),
					height: obj.height(),
					filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + src + "',sizingMethod='scale')"
				}).attr("src", blank);
			}
		},
		pngImageAll: function(url) {
			if(/MSIE [56].*Windows/.test(navigator.userAgent)) {
				$("img").each(function() {
					M$.Fix.IE.pngImage($(this), url);
				});
			}
		},
		pngBackgroundCheck: function(obj) {
			var bgSrc = obj.css("backgroundImage");
			var bgPNG = bgSrc.match(/^url[("']+(.*\.png[^\)"']*)[\)"']+[^\)]*$/i);
			var check = /-fix\.png$/.test(bgPNG);
			
			if(!check) return false;
			
			var src = bgPNG[1];
			src = src.replace(/-fix\.png$/, "");
			src = src.split("-");
			
			if(src[src.length - 1] == "normal") return [bgPNG[1], "normal"];
			
			var size = src[src.length - 1].split("x");
			
			if(size.length < 2) return false;
			
			var height = size[1];
			var width = size[0];
			
			if(!width || !height) return false;
			
			return [bgPNG[1], width, height];
		},
		pngBackgroundNormal: function(obj, url) {
			if(!obj.length) return;
			
			if(/MSIE [56].*Windows/.test(navigator.userAgent)) {
				var blank;
				if(url) blank = url;
				else blank = this.blank;
				
				var backgroundImage = M$.Fix.IE.pngBackgroundCheck(obj);
				
				if(backgroundImage) {
					obj.css({
						width: obj.width(),
						height: obj.height(),
						backgroundImage: 'url("' + blank + '")',
						filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src=" + backgroundImage[0] + ",sizingMethod='scale')"
					});
				}
			}
		},
		pngBackground: function(obj, url) {
			if(!obj.length) return;
			
			if(/MSIE [56].*Windows/.test(navigator.userAgent)) {
				var blank;
				if(url) blank = url;
				else blank = this.blank;
				
				var backgroundImage = M$.Fix.IE.pngBackgroundCheck(obj);
				
				if(backgroundImage) {
					var html = obj.html();
					
					var width = obj.width();
					var height = obj.height();
					
					var innerWidth = obj.innerWidth();
					var innerHeight = obj.innerHeight();
					
					var borderTopWidth = parseInt(obj.css("borderTopWidth"));
					var borderRightWidth = parseInt(obj.css("borderRightWidth"));
					var borderBottomWidth = parseInt(obj.css("borderBottomWidth"));
					var borderLeftWidth = parseInt(obj.css("borderLeftWidth"));
					
					var paddingTop = obj.css("paddingTop");
					var paddingRight = obj.css("paddingRight");
					var paddingBottom = obj.css("paddingBottom");
					var paddingLeft = obj.css("paddingLeft");
					
					var backgroundColor = obj.css("backgroundColor");
					var backgroundRepeat = obj.css("backgroundRepeat");
					
					var backgroundPositionX = obj.css("backgroundPositionX");
					var backgroundPositionY = obj.css("backgroundPositionY");
					
					// TH1: count background = 1, top, left, border = 0, position = 0
					if(backgroundImage[1] == innerWidth && backgroundImage[2] == innerHeight && !borderTopWidth && !borderRightWidth && !borderBottomWidth && !borderLeftWidth && !backgroundPositionX && !backgroundPositionY) {
						obj.css({
							width: width,
							height: height,
							backgroundImage: 'url("' + blank + '")',
							filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src=" + backgroundImage[0] + ",sizingMethod='scale')"
						});
					}
					else {
						obj.css({
							width: innerWidth,
							height: innerHeight,
							backgroundImage: "none",
							backgroundIColor: "transparent",
							padding: 0,
							overflow: "hidden"
						}).html("").prepend("<div /><div />");
						
						obj.children(":first").css({
							width: innerWidth,
							height: innerHeight,
							fontSize: 0,
							position: "absolute",
							overflow: "hidden"
						}).prepend("<div />");
						
						var numX = 1;
						var numY = 1;
						var marginLeft = 0;
						var marginTop = 0;
						
						switch(backgroundPositionX) {
							case "left":
								marginLeft = 0;
								break;
							case "center":
								marginLeft = (innerWidth - backgroundImage[1]) / 2;
								break;
							case "right":
								marginLeft = innerWidth - backgroundImage[1];
								break;
							default:
								marginLeft = parseInt(backgroundPositionX);
								break;
						}
						switch(backgroundPositionY) {
							case "top":
								marginTop = 0;
								break;
							case "center":
								marginTop = (innerHeight - backgroundImage[2]) / 2;
								break;
							case "bottom":
								marginTop = innerHeight - backgroundImage[2];
								break;
							default:
								marginTop = parseInt(backgroundPositionY);
								break;
						}
						
						if(backgroundRepeat != "no-repeat") {
							if(backgroundRepeat == "repeat-x")
								if(backgroundImage[1] == 1) numX = 1;
								else numX = Math.ceil(innerWidth / backgroundImage[1]);
							else if(backgroundRepeat == "repeat-y")
								if(backgroundImage[2] == 1) numY = 1;
								else numY = Math.ceil(innerHeight / backgroundImage[2]);
							else {
								numX = Math.ceil(innerWidth / backgroundImage[1]);
								numY = Math.ceil(innerHeight / backgroundImage[2]);
							}
							
							if(marginLeft && backgroundRepeat != "repeat-y") {
								while(Math.ceil(marginLeft / backgroundImage[1]) != 0 && Math.ceil(marginLeft / backgroundImage[1]) != 1) {
									if(marginLeft > 0) marginLeft -= backgroundImage[1];
									if(marginLeft < 0) marginLeft += backgroundImage[1];
								}
								if(marginLeft > 0) {
									marginLeft -= backgroundImage[1];
									numX++;
								}
							}
							if(marginTop && backgroundRepeat != "repeat-x") {
								while(Math.ceil(marginTop / backgroundImage[2]) != 0 && Math.ceil(marginTop / backgroundImage[2]) != 1) {
									if(marginTop > 0) marginTop -= backgroundImage[2];
									if(marginTop < 0) marginTop += backgroundImage[2];
								}
								if(marginTop > 0) {
									marginTop -= backgroundImage[2];
									numY++
								}
							}
						}
						
						obj.children(":first").children("div").css({
							width: (backgroundImage[1] * numX),
							height: (backgroundImage[2] * numY),
							marginLeft: marginLeft,
							marginTop: marginTop
						});
						
						if(backgroundImage[1] == 1) {
							obj.children(":first").children("div").css({
								width: innerWidth,
								height: backgroundImage[2],
								backgroundImage: 'url("' + blank + '")',
								filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + backgroundImage[0] + "',sizingMethod='scale')"
							});
						}
						else if(backgroundImage[2] == 1) {
							obj.children(":first").children("div").css({
								width: backgroundImage[1],
								height: innerHeight,
								backgroundImage: 'url("' + blank + '")',
								filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + backgroundImage[0] + "',sizingMethod='scale')"
							});
						}
						else {
							for(i = 0; i < (numX * numY); i++) {
								obj.children(":first").children("div").append("<div />");
							}
							obj.children(":first").children("div").children("div").css({
								width: backgroundImage[1],
								height: backgroundImage[2],
								backgroundImage: 'url("' + blank + '")',
								filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='" + backgroundImage[0] + "',sizingMethod='scale')",
								float: "left"
							});
						}
						
						obj.children(":last").css({
							width: width,
							height: height,
							marginTop: paddingTop,
							marginRight: paddingRight,
							marginBottom: paddingBottom,
							marginLeft: paddingLeft,
							position: "absolute"
						}).html(html);
					}
					
					$(this).remove();
				}
			}
		},
		pngBackgroundAll: function(url) {
			if(/MSIE [56].*Windows/.test(navigator.userAgent)) {
				getLevel = function(obj, level) {
					if(!obj.parent().get(0).tagName) return level;
					return getLevel(obj.parent(), level + 1);
				}
				sortArray = function(array) {
					for(i = 0; i < array.length - 1; i++) {
						for(j = i + 1; j < array.length; j++) {
							if(array[i][1] < array[j][1]) {
								var temp = array[i];
								array[i] = array[j];
								array[j] = temp;
							}
						}
					}
					return array;
				}
				var array = new Array();
				$("*:not(head, script, style, noscript)").each(function() {
					var backgroundImage = M$.Fix.IE.pngBackgroundCheck($(this));
					if(backgroundImage) {
						if(backgroundImage[1] == "normal") {
							M$.Fix.IE.pngBackgroundNormal($(this), url);
						}
						else {
							array[array.length] = [$(this), getLevel($(this), 0)];
						}
					}
				});
				array = sortArray(array);
				$(array).each(function(index, value) {
					M$.Fix.IE.pngBackground(value[0], url);
				});
			}
		},
		png: function(url) {
			if(/MSIE [56].*Windows/.test(navigator.userAgent)) {
				this.pngImageAll(url);
				this.pngBackgroundAll(url);
			}
		},
		minWidthHeight: function() {
			//alert($("*:not(head, script, style, noscript)"));
			if(M$.CheckIE56()) {
				$("div").add("p").add("a").add("span").each(function() {
					// minWidth
					var minWidth = parseInt($(this).css("minWidth"));
					var width = $(this).width();
					if(minWidth > 0 && width < minWidth) {
						$(this).width(minWidth);
					}
					// minHeight
					var minHeight = parseInt($(this).css("minHeight"));
					var height = $(this).height();
					if(minHeight > 0 && height < minHeight) {
						$(this).height(minHeight);
					}
				});
			}
		},
		minWidth: function(obj, width) {
			if(M$.CheckIE56()) {
				if(obj.width() < width) obj.width(width);
			}
		}
	},
	All: {
		aHref: function(string, obj) {
			if(!string) string = "javascript: void(0);";
			if(!obj) obj = $("a, area");
			
			obj.each(function() {
				if(!$(this).attr("href") || $(this).attr("href") == "#") {
					$(this).attr("href", string);
				}
			});
		}
	}
}


/************************************************************************
Ajax 1.0.2
************************************************************************/
MTool.Ajax = {
	ObjectToProcess: "#MTool.Ajax.ObjectToProcess",
	SetRandomKey: true,
	RandomKeyName: "MTool.Ajax.RandomKeyName",
	ProcessScroll: true,
	ProcessScrollTop: 0,
	TimeRecall: 100,
	OnFinish: function() {},
	Begin: function(url) {
		if(!M$.GetHash()) M$.SetHash(url);
	},
	Recall: function() {
		setInterval("M$.Ajax.AutoCheck()", this.TimeRecall);
	},
	Check: function(hash) {
		var url = M$.GetHash();
		if(url) url = url.substring(1);
		if(hash && hash.substring(0, 1) == "#") hash = hash.substring(1);
		if (hash == undefined || hash == url) {
			this.OnPrepare();
		}
		else M$.SetHash(hash);
	},
	AutoCheck: function() {
		var url = M$.GetHash();
		if(!url) {
			M$.SetHash(this.LastUrl);
			return;
		}
		if(url) url = url.substring(1);
		if(url != this.LastUrl) {
			this.LastUrl = url;
			this.Url = url;
			this.OnPrepare();
		}
	},
	OnPrepare: function() {
		this.OnLoad();
	},
	OnLoad: function() {
		if(this.SetRandomKey) {
			this.Url = M$.UrlVariables.SetRandomKey(this.Url, this.RandomKeyName);
		}
		$.ajax({
			url: this.Url,
			success: function(data) {
				M$.Ajax.OnComplete(data);
			},
			error: function(x, e) {
				M$.Debug.Append(M$.Language.L6 + "\n" + M$.Ajax.Error(x, e));
			}
		});
	},
	OnComplete: function(data) {
		$(M$.Ajax.ObjectToProcess).slideToggle("fast", function() {
			$(M$.Ajax.ObjectToProcess).html(data);
			$(M$.Ajax.ObjectToProcess).slideToggle("fast", function() {
				M$.Ajax.OnFinish();
			});
			if(M$.Ajax.ProcessScroll) $('html,body').animate({scrollTop: M$.Ajax.ProcessScrollTop}, "fast");
		});
	},
	Error: function(x, e) {
		if(e == "timeout") return M$.Language.L1;
		if(e == "parsererror") return M$.Language.L2;
		if(x.status == 0) return M$.Language.L3;
		if(x.status == 404) return M$.Language.L4;
		if(x.status == 500) return M$.Language.L5;
		return M$.Language.L7 + "\n" + x.responseText;
	}
};

/************************************************************************
DifferenceImage 1.0.0
************************************************************************/
MTool.DifferenceImage = {
	GetArray: function(standardWidth, standardHeight, width, height) {
		var typeArray;
		var size;
		var differenceSize;
		var standardRate = standardWidth / standardHeight;
		var rate = width / height;
		if(rate > standardRate) {
			typeArray = "w";
			if(width > standardWidth) {
				size = standardWidth;
				differenceSize = [0, parseInt(Math.abs((standardHeight - (standardWidth * height / width)) / 2))];
			}
			else {
				size = width;
				differenceSize = [parseInt(Math.abs((standardWidth - width) / 2)), parseInt(Math.abs((standardHeight - height) / 2))];
			}
		}
		else {
			typeArray = "h";
			if(height > standardHeight) {
				size = standardHeight;
				differenceSize = [parseInt(Math.abs((standardWidth - (width * standardHeight / height)) / 2)), 0];
			}
			else {
				size = height;
				differenceSize = [parseInt(Math.abs((standardWidth - width) / 2)), parseInt(Math.abs((standardHeight - height) / 2))];
			}
		}
		return [typeArray, size, differenceSize];
	},
	GetString: function(standardWidth, standardHeight, width, height) {
		var DI = this.GetArray(standardWidth, standardHeight, width, height);
		var insert;
		if(DI[0] == "w") {
			insert = "width='" + DI[1] + "' style='margin-left: " + DI[2][0] + "px; margin-top: " + DI[2][1] + "px' ";
		}
		else {
			insert = "height='" + DI[1] + "' style='margin-left: " + DI[2][0] + "px; margin-top: " + DI[2][1] + "px' ";
		}
		return insert;
	}
};

/************************************************************************
LoadJsCssFile 1.0.0
************************************************************************/
MTool.LoadJsCssFile = function(Name, Type) {
	if (Type == "js") {
		var append = document.createElement('script');
		append.setAttribute("type","text/javascript");
		append.setAttribute("src", Name);
	}
	else if (Type == "css") {
		var append = document.createElement("link");
		append.setAttribute("rel", "stylesheet");
		append.setAttribute("type", "text/css");
		append.setAttribute("href", Name);
	}
	if (typeof append != "undefined")
		document.getElementsByTagName("head")[0].appendChild(append);
}

/************************************************************************
Webcom 1.0.0
require:
	each module from webcom library or you can create your own
************************************************************************/
MTool.Webcom = {
	source: "webcom/",
	RandomKeyName: "MTool.Webcom.RandomKeyName",
	ReloadXml: false,
	Cache: {},
	Count: 0,
	ReplaceNode: function(obj) {
		M$.Webcom.Count++;
		
		var MWebcom = obj.attr("MWebcom");
		
		var prefix = eval("M$.Webcom.Cache." + MWebcom + ".prefix");
		var suffix = eval("M$.Webcom.Cache." + MWebcom + ".suffix");
		var fixdata = eval("M$.Webcom.Cache." + MWebcom + ".fixdata");
		var script = eval("M$.Webcom.Cache." + MWebcom + ".script");
		
		var html = $.trim(obj.html());
		
		obj.empty();
		$(prefix + html + suffix).appendTo(obj);
		
		if(fixdata) {
			var command = "";
			command += "M$.Webcom.Cache." + MWebcom + ".fix = function(again) {";
				command += "var obj_webcom = $(\"." + MWebcom + "[MWebcom_fixed!='done']\");";
				command += "if(again) obj_webcom = $(\"." + MWebcom + "\");";
				
				command += "obj_webcom.each(function() {";
					command += fixdata;
					
					command += "$(this).attr(\"MWebcom_fixed\", \"done\");";
				command += "});";
			command += "}";
			eval(command);
			
			$(function() {
				eval("M$.Webcom.Cache." + MWebcom + ".fix();");
			});
		}
		
		eval(script);
	},
	ReplaceAll: function(obj) {
		/* function */
		getLevel = function(obj, level) {
			if(!obj.parent().get(0).tagName) return level;
			return getLevel(obj.parent(), level + 1);
		}
		sortArray = function(array) {
			for(i = 0; i < array.length - 1; i++) {
				for(j = i + 1; j < array.length; j++) {
					if(array[i][1] < array[j][1]) {
						var temp = array[i];
						array[i] = array[j];
						array[j] = temp;
					}
				}
			}
			return array;
		}
		/* function */
		
		var array = new Array();
		
		if(!obj) obj = $("body");
		
		$("*[MWebcom]", obj).each(function() {
			var obj = $(this);
			var MWebcom = $(this).attr("MWebcom");
			
			/* cache */
			if(!eval("M$.Webcom.Cache." + MWebcom)) {
				eval("M$.Webcom.Cache." + MWebcom + " = {}");
				
				if (window.XMLHttpRequest) {
					xmlhttp=new XMLHttpRequest();
				}
				else {
					xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				var xml = M$.Webcom.source + MWebcom + "/xml.xml";
				if(M$.Webcom.ReloadXml) xml = M$.UrlVariables.SetRandomKey(xml, M$.Webcom.RandomKeyName);
				
				xmlhttp.open("GET", xml, false);
				xmlhttp.send();
				xmlDoc = xmlhttp.responseXML; 
				
				/* add css */
				$("head").prepend("<style type=\"text/css\">" + $(xmlDoc).find("css").text() + "</style>");
				/* add css */
				
				/* add dom */
				var prefix = $.trim($(xmlDoc).find("webcom prefix").text());
				eval("M$.Webcom.Cache." + MWebcom + ".prefix = prefix;");
				
				var suffix = $.trim($(xmlDoc).find("webcom suffix").text());
				eval("M$.Webcom.Cache." + MWebcom + ".suffix = suffix;");
				
				var fixdata = $.trim($(xmlDoc).find("webcom fixdata").text());
				eval("M$.Webcom.Cache." + MWebcom + ".fixdata = fixdata;");
				
				var script = $.trim($(xmlDoc).find("webcom script").text());
				eval("M$.Webcom.Cache." + MWebcom + ".script = script;");
				
				/* add dom */
			}
			/* cache */
			
			obj.addClass(MWebcom);
			array[array.length] = [$(this), getLevel($(this), 0)];
		});
		
		/* replace node */
		array = sortArray(array);
		
		$(array).each(function(index, value) {
			M$.Webcom.ReplaceNode(value[0]);
		});
		/* replace node */
	}
}
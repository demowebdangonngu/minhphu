

    get_cart();
    get_top_bar();

    function dgProduct(product_id , typeDg){
        if(MEM_ID == '' || MEM_ID == 0){ 
            $('#msg_danhgia').html(MSGDG);
            setTimeout(window.location.href = baseurl + "tai-khoan/login", 2000);
            return false;
        }else{
            $.post(baseurl + "product/productdg", { product_id: product_id, typeDg: typeDg, ajax: '1' },
            function(data){});    
        } 
    }

    function get_top_bar(){
        //$.get(baseurl + "top/top_bar", function(top_bar){ // Get the contents of the url cart/show_cart
		//	$("#topbar").html(top_bar); // Replace the information in the div #cart_content with the retrieved data
		//}); 
    }
    function get_cart(){
        $.get(baseurl + "cartorder/cart_top", function(cart){ // Get the contents of the url cart/show_cart
			$("#cart").html(cart); // Replace the information in the div #cart_content with the retrieved data
		}); 
    }
    function add_cart_item(id, idcolor, idsize , qty){
        $.post(baseurl + "cartorder/add_cart_item", { product_id: id, id_color: idcolor, id_size: idsize, quantity: qty, ajax: '1' },
        function(data){	 
            $("#notification").html(data);       
            $("html, body").animate({ scrollTop: 0 }, "slow");
         
            $.get(baseurl + "cartorder/page_cart", function(cart){ // Get the contents of the url cart/show_cart
				$("#page_cart").html(cart); // Replace the information in the div #cart_content with the retrieved data
			});
            $.get(baseurl + "cartorder/cart_top", function(cart){ // Get the contents of the url cart/show_cart
				$("#cart").html(cart); // Replace the information in the div #cart_content with the retrieved data
			});
     	 
         });
    }
    function remove_cart(id){
        $.post(baseurl + "cartorder/remove_cart", { rowid: id, ajax: '1' },
        function(data){	 
        	   $("#notification").html(data);   
               $("html, body").animate({ scrollTop: 0 }, "slow");  
        			//$.get(baseurl + "cartorder/cart_right", function(cart){ // Get the contents of the url cart/show_cart
        //          							$("#cart_block").html(cart); // Replace the information in the div #cart_content with the retrieved data
        //        						}); 		  		
                    $.get(baseurl + "cartorder/page_cart", function(cart){ // Get the contents of the url cart/show_cart
        				$("#page_cart").html(cart); // Replace the information in the div #cart_content with the retrieved data
        			});
                    $.get(baseurl + "cartorder/cart_top", function(cart){ // Get the contents of the url cart/show_cart
        				$("#cart").html(cart); // Replace the information in the div #cart_content with the retrieved data
        			});
         });
    }	
    function empty_cart(){
        $.post(baseurl + "cartorder/empty_cart", {  ajax: '1' },
        function(data){	 
        	 $("#notification").html(data);    
             $("html, body").animate({ scrollTop: 0 }, "slow"); 
        			//$.get(baseurl + "cartorder/cart_right", function(cart){ // Get the contents of the url cart/show_cart
        //          							$("#cart_block").html(cart); // Replace the information in the div #cart_content with the retrieved data
        //        						}); 	
                    
                    $.get(baseurl + "cartorder/page_cart", function(cart){ // Get the contents of the url cart/show_cart
        				$("#page_cart").html(cart); // Replace the information in the div #cart_content with the retrieved data
        			}); 
                    $.get(baseurl + "cartorder/cart_top", function(cart){ // Get the contents of the url cart/show_cart
        				$("#cart").html(cart); // Replace the information in the div #cart_content with the retrieved data
        			});	  		
        	 
         });
    }    
    
    function change_qty( qty , rowid){
        $.post(baseurl + "cartorder/update_cart", { rowid: rowid, qty: qty, ajax: '1' },
        function(data){	 
        		$("#notification").html(data);    
                $("html, body").animate({ scrollTop: 0 }, "slow"); 
                    $.get(baseurl + "cartorder/page_cart", function(cart){ // Get the contents of the url cart/show_cart
        				$("#page_cart").html(cart); // Replace the information in the div #cart_content with the retrieved data
        			});
                    
                    
                    $.get(baseurl + "cartorder/cart_top", function(cart){ // Get the contents of the url cart/show_cart
        				$("#cart").html(cart); // Replace the information in the div #cart_content with the retrieved data
        			});
                   
         });
    }
    
    /////////////////////////////////////
    
    function add_wish_list(id){
        $.post(baseurl + "wish/add_wish", { product_id: id,  ajax: '1' },
        function(data){	  
                    $("#notification").html(data);  
                    $("html, body").animate({ scrollTop: 0 }, "slow"); 
         });
    }
    function delete_wish(id){
        $.post(baseurl + "wish/delete_wish", { product_id: id,  ajax: '1' },
        function(data){	  
                    $("#notification").html(data);  
                    $("html, body").animate({ scrollTop: 0 }, "slow"); 
         });
    }
    
    function add_compare(id){
        $.post(baseurl + "compare/add_compare", { product_id: id,  ajax: '1' },
        function(data){	  
                    $("#notification").html(data);  
                    $("html, body").animate({ scrollTop: 0 }, "slow"); 
         });
    }
    function delete_compare(id){
        $.post(baseurl + "compare/delete_compare", { product_id: id,  ajax: '1' },
        function(data){	  
                    $("#notification").html(data);  
                    $("html, body").animate({ scrollTop: 0 }, "slow"); 
         });
    }
    function delete_all_compare(){
        $.post(baseurl + "compare/delete_all_compare", { ajax: '1' },
        function(data){	  
                    $("#notification").html(data);  
                    $("html, body").animate({ scrollTop: 0 }, "slow"); 
         });
    }
    
    
    ///////////////////////////////////////
    function registered_mem(){ 
        $.post(baseurl + "registered", { name: $('#registered_name').val(), email: $('#registered_email').val(), ajax: '1' },
        function(data){	 
        		 alert(data);
         });
    
    }
    function replaceUnicode(str)
    {
        var str = $.trim(str);
        str = str.toLowerCase();
        str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
        str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
        str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
        str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
        str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
        str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
        str = str.replace(/đ/g, "d");
        str = str.replace(/!|@|%|\^|\*|\(|\)|\=|\<|\>|\?|\/|,|\.|\:|\;|\'| |\"|\&|\#|\[|\]|~|$|_/g, "-");
        /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
        str = str.replace(/-+-/g, "-"); //thay thế 2- thành 1-
        str = str.replace(/^\-+|\-+$/g, "");
        return str;
    }
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
        // alert( pattern.test(emailAddress) );
        return pattern.test(emailAddress);
    }; 
    // search
    $('#keyword').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            
                $(this).val(replaceUnicode($(this).val()));
              
        } 
    }); 
    $("#icon_search").click(function () {  
        if (!isValidEmailAddress($("#keyword").val())) { 
            var val = replaceUnicode($('#keyword').val()); 
        }else{
            var val = $('#keyword').val(); 
        } 
        if (val == '') {
            $('#keyword').focus(); 
        } else {
            $("#frmSearch").submit();
        }
    }); 
    $('#frmSearch').submit(function(){   
            var $this = $(this),
                action = $this.attr('action');   
            $('#frmSearch').attr('action', action+'/'+$('#keyword').val());  
         
        
         
    });  
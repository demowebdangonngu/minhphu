main_height = function() {
	var height = $(".main_content").height();
	
	$(".main_content").css({
		"min-height": height
	});
}

box_like = function() {
	var window_height = $(window).height();
	var right_height = $(".right").height();
	
	var scrollTop = $(window).scrollTop();
	
	var space_top = $(".header").height() + $(".menu_search").height() + 26;
	var space_bottom = 20;
	
	var scrollTop_max = space_top + $(".main_content").height() - window_height + 20;
	
	var limit = space_top + (right_height - window_height) + space_bottom;
	
	if(limit > 0) {
		var top = window_height - right_height - space_bottom;
		
		if(scrollTop > limit && scrollTop < scrollTop_max) {
			$(".right").removeClass("right_bottom").addClass("right_fixed").css({
				top: top
			});
		}
		else if(scrollTop > scrollTop_max) {
			$(".right").removeClass("right_fixed").addClass("right_bottom");
		}
		else {
			$(".right").removeClass("right_bottom").removeClass("right_fixed");
		}
	}
}

/************************************************************************
Ready
************************************************************************/
jQuery(function($) {
	document.onmousedown=disableclick;
	status="Right Click Disabled";
	function disableclick(event)
	{
		if(event.button == 2) {
			console.log(status);
			return false;    
		}
	}
	
	$(window).scroll(function () {
		if ($(this).scrollTop() > 0) {
			$('#go-to-top').fadeIn();
		} else {
			$('#go-to-top').fadeOut();
		}
	});
	
	$("#go-to-top").click(function () {
		if($(window).width() > 800) {
			$("html, body").animate({ scrollTop: 0 }, "fast");
			return false;
		}
		else {
			$("html, body").animate({ scrollTop: 0 }, "slow");
			return false;
		}
	});
	
	M$.Fix.All.aHref();
	
	$(".menu_tablet").click(function() {
		$(this).find(".sub").slideToggle("fast");
	});
	
	if($(window).width() > 800) {
		$(window).scroll(function() {
			box_like();
		});
		
		$(window).resize(function() {
			main_height();
			box_like();
		});
		
		main_height();
		box_like();
	}
});
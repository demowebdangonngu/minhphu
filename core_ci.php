<?php
$system_path = 'system';
$application_folder = 'application';
if (defined('STDIN')) {
    chdir(dirname(__FILE__));
}
if (realpath($system_path) !== FALSE) {
    $system_path = realpath($system_path) . '/';
}
$system_path = rtrim($system_path, '/') . '/';
// Is the system path correct?
if (!is_dir($system_path)) {
    exit("Your system folder path does not appear to be set correctly. Please open the following file and correct this: " . pathinfo(__FILE__, PATHINFO_BASENAME));
}
// The name of THIS file
define('SELF', pathinfo(__FILE__, PATHINFO_BASENAME));

// The PHP file extension
// this global constant is deprecated.
define('EXT', '.php');

// Path to the system folder
define('BASEPATH', str_replace("\\", "/", $system_path));

// Path to the front controller (this file)
define('FCPATH', str_replace(SELF, '', __FILE__));

// Name of the "system folder"
define('SYSDIR', trim(strrchr(trim(BASEPATH, '/'), '/'), '/'));


// The path to the "application" folder
if (is_dir($application_folder)) {
    define('APPPATH', $application_folder . '/');
} else {
    if (!is_dir(BASEPATH . $application_folder . '/')) {
        exit("Your application folder path does not appear to be set correctly. Please open the following file and correct this: " . SELF);
    }

    define('APPPATH', BASEPATH . $application_folder . '/');
}

//Custom config for front-end modules changed by Bhuban <bhuban@gmail.com>
$assign_to_config['modules_locations'] = array(
    APPPATH . 'front-modules/' => '../front-modules/',
);

require(BASEPATH . 'core/Common.php');
$CFG = & load_class('Config', 'core');

// Do we have any manually set config items in the index.php file?
if (isset($assign_to_config)) {
    $CFG->_assign_to_config($assign_to_config);
}

$UNI = & load_class('Utf8', 'core');
$URI = & load_class('URI', 'core');
$RTR = & load_class('Router', 'core');
$RTR->_set_routing();
// Set any routing overrides that may exist in the main index file
if (isset($routing)) {
    $RTR->_set_overrides($routing);
}

$OUT =& load_class('Output', 'core');
$SEC = & load_class('Security', 'core');
$IN = & load_class('Input', 'core');
$LANG =& load_class('Lang', 'core');
require BASEPATH . 'core/Controller.php';

function &get_instance() {
    return CI_Controller::get_instance();
}

if (file_exists(APPPATH . 'core/' . $CFG->config['subclass_prefix'] . 'Controller.php')) {
    require APPPATH . 'core/' . $CFG->config['subclass_prefix'] . 'Controller.php';
}
if (!file_exists(APPPATH . 'controllers/' . $RTR->fetch_directory() . $RTR->fetch_class() . '.php')) {
    show_error('Unable to load your default controller. Please make sure the controller specified in your Routes.php file is valid.');
}

include(APPPATH . 'controllers/' . $RTR->fetch_directory() . $RTR->fetch_class() . '.php');
$class = $RTR->fetch_class();
$method = $RTR->fetch_method();

if (!class_exists($class) OR strncmp($method, '_', 1) == 0 OR in_array(strtolower($method), array_map('strtolower', get_class_methods('CI_Controller')))
) {
    if (!empty($RTR->routes['404_override'])) {
        $x = explode('/', $RTR->routes['404_override']);
        $class = $x[0];
        $method = (isset($x[1]) ? $x[1] : 'index');
        if (!class_exists($class)) {
            if (!file_exists(APPPATH . 'controllers/' . $class . '.php')) {
                show_404("{$class}/{$method}");
            }

            include_once(APPPATH . 'controllers/' . $class . '.php');
        }
    } else {
        show_404("{$class}/{$method}");
    }
}
$CI = new $class();
if (method_exists($CI, '_remap')) {
    $CI->_remap($method, array_slice($URI->rsegments, 2));
} else {
    // is_callable() returns TRUE on some versions of PHP 5 for private and protected
    // methods, so we'll use this workaround for consistent behavior
    if (!in_array(strtolower($method), array_map('strtolower', get_class_methods($CI)))) {
        // Check and see if we are using a 404 override and use it.
        if (!empty($RTR->routes['404_override'])) {
            $x = explode('/', $RTR->routes['404_override']);
            $class = $x[0];
            $method = (isset($x[1]) ? $x[1] : 'index');
            if (!class_exists($class)) {
                if (!file_exists(APPPATH . 'controllers/' . $class . '.php')) {
                    show_404("{$class}/{$method}");
                }

                include_once(APPPATH . 'controllers/' . $class . '.php');
                unset($CI);
                $CI = new $class();
            }
        } else {
            show_404("{$class}/{$method}");
        }
    }

    // Call the requested method.
    // Any URI segments present (besides the class/function) will be passed to the method for convenience
    call_user_func_array(array(&$CI, $method), array_slice($URI->rsegments, 2));
}
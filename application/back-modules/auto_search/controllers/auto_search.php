<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Auto_search extends MX_Controller {
	
	function __construct()	{
		parent::__construct();			
		$this->load->model('auto_search/auto_search_model','ACT',TRUE);		
		$this->module_name		= $this->uri->segment(1);
		
		$this->config->load('define',TRUE);
		$this->load->library('adminclass_model');
		//kiem tra permission 		
		/*$user_group_id		= $this->session->userdata('admin_group_id');//1;		
		$this->adminclass_model->permission_login($user_group_id,$this->module_name);	*/
	}	
	public function index(){					
	
	}
	
	
	public function search_auto() {
		
		$data 			= array();
		$key 			= addslashes($this->input->get('key', TRUE));
		$id_menu_type 	= $this->input->get('id_menu_type', TRUE);
		$type 			= '';
		$array 			= '';
		
		switch($id_menu_type ){			
			case 'actor':{
				$type 		= $this->input->get('type', TRUE);
				$array 		= $this->input->get('array', TRUE);
				if ($type 	== 0) {
					$query 	= " select a.id as idd,a.actor as name from actors a where a.actor like '%" . $key . "%'";
				} else {
					$query 	= " select a.id as idd,a.actor as name from actors a where a.actor like '%" . $key . "%' AND type = '" . $type . "' ";
				}	
				break;
			}			
			case 'tags':{
				$type 		= $this->input->get('type', TRUE);
				$query 		= " select a.id as idd,a.name as name from tags a where a.name like '%" . ucwords(str_replace("đ", "Đ", trim($key))) . "%' ";// AND publish = '1' ";
				break;
			}
			case 'country':{
				$type 		= $this->input->get('type', TRUE);
				$query 		= " select a.id as idd,a.country_name as name from country a where a.country_name like '%" . ucwords(str_replace("đ", "Đ", trim($key))) . "%' ";// AND publish = '1' ";
				break;
			}
			case 'city':{
				$type 		= $this->input->get('type', TRUE);
				$query 		= " select a.id as idd,a.cityname as name from city a where a.cityname like '%" . ucwords(str_replace("đ", "Đ", trim($key))) . "%' ";// AND publish = '1' ";
				break;
			}		
			case 'director':{
				$type 		= $this->input->get('type', TRUE);  
				$str 		= "";//$type==1?" and publish=1 ":"";
				$query 		= " select a.id as idd,a.name as name from director a where a.name like '%" .  trim($key) . "%'  ".$str;		
				break;
			}
			case 'seasons':
				$query 		= " select a.id as idd,a.name as name from season a where a.name like '%" .  trim($key) . "%'  ";
				break;		
			case 'events':{
				 $query 	= " select a.id as idd,a.name as name from event a where a.name like '%" .  trim($key) . "%'  ";
				break;
			}
			case 'logeditmodule':{
				 $query 	= " select a.id as idd,a.drescription as name from logeditmodule a where a.drescription like '%" .  trim($key) . "%'  ";
				break;
			}
			case 'subsite':{
				$type 		= $this->input->get('type', TRUE);
				$query 		= " select a.id as idd,a.name as name from subsite a where a.name like '%" . ucwords(str_replace("đ", "Đ", trim($key))) . "%' ";// AND publish = '1' ";
				break;
			}
			case 'users':
				$query 		= " select a.id as idd,a.username as name from users a where a.username like '%" .  trim($key) . "%'  ";
				break;		
			case 'clip_cat':
				 $query 	= "SELECT b.id as idd,b.name,b.name_ob as name_ext FROM clip_category b where publish=1 and (b.name like '%".$key."%' or b.name_ob like '%".$key."%' ) ";
				break;
			case 'clip':{
				 $type		= $id_menu_type;    $type = 8;        
				 $array 	= $this->input->get('array', TRUE);
				 $str 		= @$array ==1?" and publish not in (1) ":"";
				$query 		= " select fl.clip_id as idd,fl.name,fl.name_ext from clip_lang fl, clip f where f.id = fl.clip_id and fl.lang_id = 1 and f.id_menu_type = '" . $type . "' and (fl.name like '%" . $key . "%' or fl.name_ext like '%" . $key . "%') ".$str ;			
				break;
			}
			case 'clips':{
				 $type		= $id_menu_type;    $type = 8;        
				 $array 	= $this->input->get('array', TRUE);
				 $str 		= @$array ==1?" and publish not in (1) ":"";
				$query 		= " select fl.clip_id as idd,fl.name,fl.name_ext from clip_lang fl, clip f where f.id = fl.clip_id and fl.lang_id = 1 and f.id_menu_type = '" . $type . "' and (fl.name like '%" . $key . "%' or fl.name_ext like '%" . $key . "%') ".$str ;			
				break;
			}
			case 'news_cat':{
				 $query 	= "SELECT b.id as idd,b.name FROM category_news b where publish=1 and b.name like '%".$key."%' ";
				break;
			}
			case 'news':{
				  $type		= $id_menu_type;$type = 8;			  
				  $str 		= ""; 
				  $array 	= $this->input->get('array', TRUE);
				 $query 	= " select fl.news_id as idd,fl.title name,'News-event' as name_ext from news_lang fl, news f where f.id = fl.news_id and fl.lang_id = 1  and (fl.title like '%" . $key . "%' ) ".$str ;
				break;
			}
			case 'redeem':{
				  $type		= $id_menu_type; $str = ""; 	
				 $query 	= " select fl.id as idd,fl.serial name,code as name_ext from redeem fl where (fl.serial like '%" . $key . "%' ) ".$str ." group by serial" ;
				break;
			}
			case 'pagename':{
				$type 		= $this->input->get('type', TRUE);
				$array 		= $this->input->get('array', TRUE);   	
				$query 		= " select a.id as idd,a.name as name from pagename a where a.name like '%" . $key . "%' ";				
				break;
			}
			case 'livetv_cat':{
				  $type 	= $this->input->get('type', TRUE);
				 $query 	= "SELECT b.id as idd,b.name FROM livetv_cat b where publish=1 and b.name like '%".$key."%' ";
				break;
			}
			case 'livetv':{
				 $type 	= $this->input->get('type', TRUE);
				$query 		= " select la.id as idd,la.name from livetv la where la.name like '%" . $key . "%'";
				break;
			}
			case 'livetv_subsite':{
				 $type 	= $this->input->get('type', TRUE);
				$query 		= " select la.id as idd,la.name from livetv_subsite la where la.name like '%" . $key . "%'";
				break;
			}
			case 'show_cat':{
				 $query 	= "SELECT b.id as idd,b.name FROM show_cat b where publish=1 and b.name like '%".$key."%' ";
				break;
			}
			case 'show':{
				 $array 	= $this->input->get('array', TRUE);
				 $str 		= @$array ==1?"":"";// and publish not in (1) ";
				$query 		= " select fl.id as idd,fl.name,fl.name_ext from show fl where (fl.name like '%" . $key . "%' or fl.name_ext like '%" . $key . "%') ".$str ;
				break;
			}
			case 'shows':{
				 $array 	= $this->input->get('array', TRUE);
				 $str 		= "";
				$query 		= " select fl.id as idd,fl.name,fl.name_ext from show fl where (fl.name like '%" . $key . "%' or fl.name_ext like '%" . $key . "%') ".$str ;
				break;
			}
			case 'show_trailer':{
				$type 		= $id_menu_type;$type = 7;
				 $array 	= $this->input->get('array', TRUE);
				 $str 		= @$array ==1?"":"";
				//$key = addslashes($key);
				$query 		= " select f.id as idd,fl.name,fl.name_ext from show fl, show_trailer f where f.show_id = fl.id and (fl.name like '%" . $key . "%' or fl.name_ext like '%" . $key . "%') ".$str ;
				break;
			}
			case 'plan_show':{
				 $type	 	= $this->input->get('array', TRUE);
				 $str 		= @$type ==1?"":"";
				$query 		= " select fl.id as idd,fl.name from plan_show fl where (fl.name like '%" . $key . "%' ) ".$str ;
				break;
			}			
			case 'tvod_cat':{
				$type 		= $this->input->get('type', TRUE);$str	= "";
				$query 		= "SELECT b.id as idd,b.name FROM tvod_cat b where publish=1 and b.name like '%".$key."%' ".$str;
				break;
			}
			case 'tvod':{
				 $array 	= $this->input->get('array', TRUE);
				 $str 		= @$array ==1?"":" and publish not in (1) ";
				$query 		= " select fl.id as idd,fl.name,fl.name_ext from tvod fl where (fl.name like '%" . $key . "%' or fl.name_ext like '%" . $key . "%') ".$str ;
				break;
			}
			case 'tvods':{
				 $array 	= $this->input->get('array', TRUE);
				 $str 		= "";
				$query 		= " select fl.id as idd,fl.name,fl.name_ext from tvod fl where (fl.name like '%" . $key . "%' or fl.name_ext like '%" . $key . "%') ".$str ;
				break;
			}	
			case 'tvod_trailer':{
				$type 		= $id_menu_type;$type = 7;
				 $array 	= $this->input->get('array', TRUE);
				 $str 		= @$array ==1?"":"";
				//$key = addslashes($key);
				$query 		= " select f.id as idd,fl.name,fl.name_ext from tvod fl, tvod_trailer f where f.tvod_id = fl.id and (fl.name like '%" . $key . "%' or fl.name_ext like '%" . $key . "%') ".$str ;
				break;
			}	
			case 'plan_tvod':{
				 $type	 	= $this->input->get('array', TRUE);
				 $str 		= @$type ==1?"":"";
				$query 		= " select fl.id as idd,fl.name from plan_tvod fl where (fl.name like '%" . $key . "%' ) ".$str ;
				break;
			}			
			case 'film_cat':{
				$type 		= $this->input->get('type', TRUE);$str	= "";
				$query 		= "SELECT b.id as idd,b.name FROM film_cat b where publish=1 and b.name like '%".$key."%' ".$str;
				break;
			}
			case 'film_trailer':{
				$type 		= $id_menu_type;$type = 7;
				 $array 	= $this->input->get('array', TRUE);
				 $str 		= @$array ==1?"":"";
				$query 		= " select f.id as idd,fl.name,fl.name_ext from film fl, film_trailer f where f.film_id = fl.id and (fl.name like '%" . $key . "%' or fl.name_ext like '%" . $key . "%') ".$str ;
				break;
			}
			case 'plan_film':{
				 $type	 	= $this->input->get('array', TRUE);
				 $str 		= @$type ==1?"":"";
				$query 		= " select fl.id as idd,fl.name from plan_film fl where (fl.name like '%" . $key . "%' ) ".$str ;
				break;
			}
			default:{
				 $type 		= $id_menu_type;
				 if ($id_menu_type == 'film') {	$type 	= 1; }
				 $array 	= $this->input->get('array', TRUE);
				 $str 		= @$array ==1?" and publish not in (1) ":"";
				//if($_SESSION['countries_id'] == 9)$str .= " and f.countries_id=9 ";
				$query 		= " select fl.id as idd,fl.name,fl.name_ext from film fl where (fl.name like '%" . $key . "%' or fl.name_ext like '%" . $key . "%') ".$str ;
			}
		}		
	
		if ($id_menu_type != 'actor' && $id_menu_type != 'tags' && $id_menu_type != 'film' && $id_menu_type != 'seasons'&& $id_menu_type != 'events'&& $id_menu_type != 'director' && $id_menu_type != 'show' && $id_menu_type != 'shows' && $id_menu_type != 'tvod'&& $id_menu_type != 'tvods' && $id_menu_type != 'clip' && $id_menu_type != 'news'|| $id_menu_type != 'redeem'|| $id_menu_type != 'film_trailer'|| $id_menu_type != 'show_trailer'|| $id_menu_type != 'tvod_trailer') {
			if (@$this->session->userdata('Status') == "All") {
				
			} elseif (@$this->session->userdata('Status') == 1) {
				$query .= " and publish = 1";
			} elseif (@$this->session->userdata('Status') == 0) {

				$query .= " and publish = 0";
			}
		}
	//print $query;exit;
	
		$rows		= $this->ACT->get_query(10,0,$query," order by id desc");
		$key 		= 0;
	  
		foreach ($rows as  $row) {$key++;
			if ($id_menu_type == 'actor') {
				if ($type == 0) {
					if ($array == 1) {
						$data[] = array(
							"count" => $key,
							"id" => $row['idd'],
							"name" => $row['name'] . ':' . $row['idd'] . ';'
						);
					} else {
						$data[] = array(
							"count" => $key,
							"id" => $row['idd'],
							"name" => $row['name']
						);
					}
				} else {
					$data[] = array(
						"count" => $key,
						"id" => $row['idd'],
						"name" => $row['name'] . ':' . $row['idd'] . ';'
					);
				}
			} else if ($id_menu_type == 'film' ||  $id_menu_type == 'shows' || $id_menu_type == 'clips' || $id_menu_type == 'tvod' || $id_menu_type == 'catclip' || $id_menu_type == 'news'   ) {
				$data[] = array(
					"count" => $key,
					"id" => $row['idd'],
					"name" => $row['name'] . " # " . $row['name_ext'] . ':' . $row['idd'] . ';'
				);
			}else if ($id_menu_type == 'film_cat' || $id_menu_type == 'tvod_cat' || $id_menu_type == 'show_cat' || $id_menu_type == 'livetv_cat' || $id_menu_type == 'livetv_subsite'|| $id_menu_type == 'livetv'|| $id_menu_type == 'clip_cat'|| $id_menu_type == 'news_cat') {
				if($type==0){
						$data[] = array(
								"count" => $key,
								"id" => $row['idd'],
								"name" => $row['name'] 
							);
					}else{
							$data[] = array(
								"count" => $key,
								"id" => $row['idd'],
								"name" => $row['name'] . ':' . $row['idd'] . ';'
							);
					}
			}else if ($id_menu_type == 'tags' || $id_menu_type == 'city' || $id_menu_type == 'country' || $id_menu_type == 'director'  || $id_menu_type == 'subsite'  || $id_menu_type == 'plan_film' || $id_menu_type == 'plan_tvod' || $id_menu_type == 'plan_show') {
					if($type==1){
						$data[] = array(
								"count" => $key,
								"id" => $row['idd'],
								"name" => $row['name'] 
							);
					}else{
							$data[] = array(
								"count" => $key,
								"id" => $row['idd'],
								"name" => $row['name'] . ':' . $row['idd'] . ';'
							);
					}
			}else if ($id_menu_type == 'seasons' || $id_menu_type == 'events' || $id_menu_type == 'redeem' || $id_menu_type == 'pagename' || $id_menu_type == 'users' || $id_menu_type == 'logeditmodule') {
				$data[] = array(
					"count" => $key,
					"id" => $row['idd'],
					"name" => $row['name'] 
				);
			} else {
				$data[] = array(
					"count" => $key,
					"id" => $row['idd'],
					"name" => $row['name'] . " # " . $row['name_ext']
				);
			}
		}
		echo json_encode($data); exit;
	}
	public function return_add_auto($table,$field,$key){
		$sql			= "select id as idd, ".$field." as name from ".$table." where ".$field. " like '%".$key."%' ";
		$rows			= $this->ACT->get_query(1,0,$sql," order by id desc ");
		$i				=0 ;$data	= NULL;
		foreach($rows as $row){$i++; 
			$data = $row['name'] . ':' . $row['idd'] . ';';
		}
		echo ($data); exit;
	}
	public function add_auto() {
		/*//kiem tra permission 		
		$user_group_id		= $this->session->userdata('admin_group_id');//1;		
		$this->adminclass_model->permission_login($user_group_id,$this->module_name);*/
		
		$data 			= array();
		$key 			= addslashes($this->input->get('key', TRUE));
		$id_menu_type 	= $this->input->get('id_menu_type', TRUE);
		$type 			= '';
		$array 			= '';
		
		switch($id_menu_type ){	
			case 'tags':{
				$data["name"]	= $key;$table = "tags";$field = "name";
				break;		
			}
			case 'actor':{
				$data["actor"]	= $key;$table = "actors";$field = "actor";
				break;		
			}
			case 'director':{
				$data["name"]	= $key;$table = "director";$field = "name";
				break;		
			}
		}		
		$kq						= $this->ACT->insert($table,$data);
		/*if(($kq >0) ){
		// add log khong duoc li do : module name ko co trong modules
				$action			= "add";
				$this->add_log($kq,$table,$field,$action); 
		// en log
		}*/
		$this->return_add_auto($table,$field,$key);
		exit;
	}
		
	public function add_log($idobj=0,$table,$field,$action="add"){
		// lay thong tin log
		$description			= $this->ACT->getStrLog($idobj,$table,$field);		
		$data					= array();
		$data['idobj']			= $idobj;		
		$data['description']	= $description;
		$data['action']			= $action;//var_dump($data);
		$this->adminclass_model->add_logmodule($data);
	}
	public function load_param(){
		$data					= array();
		$data["key"]			= addslashes($this->input->post('key',TRUE));
		$data["id_menu_type"]	= ($this->input->post('id_menu_type',TRUE));
		$data["array"]		 	= $this->input->post('array',TRUE);
		return $data;
	}
}
 ?>
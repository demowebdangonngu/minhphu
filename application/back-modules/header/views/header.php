<!DOCTYPE html>
<html lang="vi" class="app">
    <head>
        <meta charset="utf-8" />
        <title>Manager</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
         <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/css/bxh.css">
        <link rel="stylesheet" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/css/bootstrap.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/css/animate.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/css/font-awesome.min.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/css/font.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/js/select2/select2.css" type="text/css" />
		<link rel="stylesheet" href="<?php echo $this->config->item('admin_url'); ?>themes/admincp/fancybox/jquery.fancybox.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/js/select2/theme.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/js/nestable/nestable.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/js/datepicker/datepicker.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/css/app.css" type="text/css" />
        <link rel="stylesheet" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/css/specifications.css" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/css/tooltip.css">
        <link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/css/yt.css">


        <script src="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/js/tooltip.js"></script>
        <!--[if lt IE 9]>
          <script src="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/js/ie/html5shiv.js"></script>
          <script src="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/js/ie/respond.min.js"></script>
          <script src="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/js/ie/excanvas.js"></script>
        <![endif]-->
        <style>
            .fileinput-button
            {
                position:relative;
            }
            .fileinput-button input
            { 
                position: absolute;
                top: 0;
                right: 0;
                margin: 0;
                opacity: 0;
                -ms-filter: 'alpha(opacity=0)';
                width:200px;
                height:40px;
                direction: ltr;
                cursor: pointer;
            }
        </style>
        <script src="<?php echo $this->config->item('admin_url'); ?>/themes/admincp/js/jquery.min.js"></script>
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        
    </head>
    <body class="">
        <?php if (!$_GET['iframe']) { ?>
            <section class="vbox">
                <header class="bg-dark dk header navbar navbar-fixed-top-xs">
                    <div class="navbar-header aside-md">

                        <a class="btn btn-link visible-xs">
                            <i class="fa fa-bars"></i>
                        </a>
                        <?php 
                        $logo = $this->load->model('attach/attach_model');
                        $logo->where(array('nameModel'=>'logo','status'=>1));
                        $logo->order_by('sort_order');
                        $r_logo = current($logo->get()->result());

                        ?>
                        <a href="<?php echo base_url();?>admincp/" class="navbar-brand"><img src="<?php echo base_url().'../upload/images/images/'.$r_logo->location; ?>" class="m-r-sm"></a>
                        <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user">
                            <i class="fa fa-cog"></i>
                        </a>
                    </div>
                    <ul class="nav navbar-nav navbar-right m-n hidden-xs nav-user">
                    <?php 
                    $order = $this->load->model('order/order_model');
                    $order->where('status',0);
                    $order->order_by('order_date', 'DESC');
                    $total = $order->get()->num_rows();
                    $order->select('order.*,member.name as m_name');
                    $order->join('member','member.id = order.mem_id');
                    $order->where('order.status',0);
                    $order->order_by('order_date', 'DESC');
                    $orders = $order->get()->result();

                    ?>
                    <li class="hidden-xs">
                      <a href="#" class="dropdown-toggle dk" data-toggle="dropdown">
                        <i class="fa fa-bell"></i>
                        <span class="badge badge-sm up bg-danger m-l-n-sm count" style="display: inline-block;"><?php echo $total;?></span>
                      </a>
                      <section class="dropdown-menu aside-xl">
                        <section class="panel bg-white">
                          <header class="panel-heading b-light bg-light">
                            <strong>Bạn có <span class="count" style="display: inline;"><?php echo $total;?></span> Đơn hàng mới</strong>
                          </header>
                          <div class="list-group list-group-alt animated fadeInRight">
                          <?php 
                            foreach($orders as $row){
                          ?>
                            <a href="<?php echo base_url();?>order/addedit/<?php echo $row->id;?>" class="media list-group-item">
                              <span class="pull-left thumb-sm">
                                <span class="pull-left thumb-sm text-center"><i class="fa fa-envelope-o fa-2x text-success"></i></span>
                              </span>
                              <span class="media-body block m-b-none">
                                <?php echo $row->m_name;?><br>
                                <small class="text-muted"><?php echo $row->order_date;?></small>
                              </span>
                            </a>
                            <?php }?>
                          </div>
                          <footer class="panel-footer text-sm">
                            <a href="#" class="pull-right"><i class="fa fa-cog"></i></a>
                            <a href="#notes" data-toggle="class:show animated fadeInRight">See all the notifications</a>
                          </footer>
                        </section>
                      </section>
                    </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                Xin chào <b><?php echo $this->session->userdata('admin_name'); ?></b> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu animated fadeInRight">
                                <span class="arrow top"></span>
                                <li>
                                    <a href="<?php echo base_url() . 'user/changepassword'; ?>">Thay đổi thông tin</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() . 'user/logout'; ?>">Đăng xuất</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </header>
                <section>
                    <section class="hbox stretch">
                        <!-- .aside -->
                        <aside class="bg-dark lter aside-md hidden-print hidden-xs" id="nav">          
                            <section class="vbox">
                                <section class="w-f scrollable">
                                    <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">

                                        <!-- nav -->
                                        <nav class="nav-primary hidden-xs">
                                            <ul class="nav">
											
												<li>
													<a href="<?php echo base_url() . 'managerfile'; ?>">
														<i class="fa fa-bars icon"><b class="bg-warning"></b></i>
														<span>Thư viện</span>
													</a>
												</li>
                                                <?php //if (check_permision("posts_management_access")) { ?>
                                                    <li <?php if ($this->uri->segment(1) == 'posts') echo 'class="active"'; ?>>
                                                        <a href="<?php echo base_url() . 'posts'; ?>">
                                                            <i class="fa fa-file-text icon"><b class="bg-warning"></b></i>
                                                            <span class="pull-right">
                                                                <i class="fa fa-angle-down text"></i>
                                                                <i class="fa fa-angle-up text-active"></i>
                                                            </span>
                                                            <span>Bài viết</span>
                                                        </a>
                                                        <ul class="nav lt">
                                                            <li >
                                                                <a href="<?php echo base_url() . 'posts/addedit'; ?>" >                                                        
                                                                    <i class="fa fa-angle-right"></i>
                                                                    <span>Thêm mới</span>
                                                                </a>
                                                            </li>
                                                            <li >
                                                                <a href="<?php echo base_url() . 'posts'; ?>" >                                                        
                                                                    <i class="fa fa-angle-right"></i>
                                                                    <span>Danh sách bài viết</span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                <?php //} ?>
                                                <?php //if (check_permision("product_management_access")) { ?>
                                                    <li <?php if ($this->uri->segment(1) == 'product' || $this->uri->segment(1) == 'specifications' || $this->uri->segment(1) == 'specifications_detail' || $this->uri->segment(1) == 'specifications_category') echo 'class="active"'; ?>>
                                                        <a href="<?php echo base_url() . 'product'; ?>">
                                                            <i class="fa fa-file-text icon"><b class="bg-warning"></b></i>
                                                            <span class="pull-right">
                                                                <i class="fa fa-angle-down text"></i>
                                                                <i class="fa fa-angle-up text-active"></i>
                                                            </span>
                                                            <span>Sản Phẩm</span>
                                                        </a>
                                                        <ul class="nav lt">
                                                            <li >
                                                                <a href="<?php echo base_url() . 'product/addedit'; ?>" >                                                        
                                                                    <i class="fa fa-angle-right"></i>
                                                                    <span>Thêm mới</span>
                                                                </a>
                                                            </li>
                                                            <li >
                                                                <a href="<?php echo base_url() . 'product'; ?>" >                                                        
                                                                    <i class="fa fa-angle-right"></i>
                                                                    <span>Danh sách sản phẩm</span>
                                                                </a>
                                                            </li>
															<li >
                                                                <a href="<?php echo base_url() . 'color'; ?>" >                                                        
                                                                    <i class="fa fa-angle-right"></i>
                                                                    <span>Danh sách màu sắc</span>
                                                                </a>
                                                            </li>
                                                            <!--
                                                            <li <?php if ($this->uri->segment(1) == 'specifications' || $this->uri->segment(1) == 'specifications_detail' || $this->uri->segment(1) == 'specifications_category' || $this->uri->segment(1) == 'attribute') echo 'class="active"'; ?>> 
                                                                <a> <i class="fa fa-angle-right"></i> <span>Thông Số Kỹ Thuật</span> </a>
                                                                <ul class="nav navsub" style="">
                                                                    <li <?php if ($this->uri->segment(1) == 'specifications'){?> class="active" <?php }?> >
                                                                        <a href="<?php echo base_url() . 'specifications'; ?>">
                                                                            <i class="fa fa-paste"></i><span>Danh sách TS kỹ thuật </span>
                                                                        </a>
                                                                    </li>
                                                                    <li <?php if ($this->uri->segment(1) == 'specifications_detail'){?> class="active" <?php }?>>
                                                                        <a href="<?php echo base_url() . 'specifications_detail'; ?>">
                                                                            <i class="fa fa-file"></i><span>Danh sách chi tiết TS kỹ thuật</span>
                                                                        </a>
                                                                    </li>
                                                                    <li <?php if ($this->uri->segment(1) == 'specifications_category'){?> class="active" <?php }?>><a href="<?php echo base_url() . 'specifications_category'; ?>">
                                                                            <i class="fa fa-list-alt"></i><span>Danh sách tổng quát TSKT</span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            -->
                                                        </ul>
                                                    </li>
                                                <?php //} ?>
                                       
                                                <?php //if (check_permision("order_access")) { ?>
                                                    <li <?php if ($this->uri->segment(1) == 'member') echo 'class="active"'; ?>>
                                                        <a href="<?php echo base_url() . 'member'; ?>" >
                                                            <i class="fa fa-users"><b class="bg-warning"></b></i>
                                                            <span>Thành viên</span>
                                                        </a>
                                                    </li>
                                                <?php //} ?>
                                                <?php //if (check_permision("order_access")) { ?>
                                                    <li <?php if ($this->uri->segment(1) == 'order') echo 'class="active"'; ?>>
                                                        <a href="<?php echo base_url() . 'order'; ?>" >
                                                            <i class="fa fa-building-o"><b class="bg-warning"></b></i>
                                                            <span>Hóa đơn đặt hàng</span>
                                                        </a>
                                                    </li>
                                                <?php //} ?>
                                                <?php //if (check_permision("categories_access")) { ?>
                                                    <li <?php if ($this->uri->segment(1) == 'categories') echo 'class="active"'; ?>>
                                                        <a href="<?php echo base_url() . 'categories'; ?>" >
                                                            <i class="fa fa-tasks"><b class="bg-warning"></b></i>
                                                            <span>Dòng sản phẩm</span>
                                                        </a>
                                                    </li>
                                                <?php //} ?>
                                                    <li <?php if ($this->uri->segment(1) == 'categoriesp') echo 'class="active"'; ?>>
                                                        <a href="<?php echo base_url() . 'categoriesp'; ?>" >
                                                            <i class="fa fa-tasks"><b class="bg-warning"></b></i>
                                                            <span>Dòng bài viết</span>
                                                        </a>
                                                    </li>
                                                <?php //if (check_permision("menus_access")) { ?>
                                                    <!-- end shortcut left -->
                                                    <li <?php if ($this->uri->segment(1) == 'menus') echo 'class="active"'; ?>>
                                                        <a  href="<?php echo base_url() . 'menus'; ?>" >
                                                            <i class="fa fa-bars icon"><b class="bg-warning"></b></i>
                                                            <span>Menu</span>
                                                        </a>
                                                    </li>
                                                <?php //} ?>
                                                <?php //if (check_permision("provider_access")) { ?>
                                                <?php //} ?>
                                                <?php //if (check_permision("attach_access")) { ?>
                                                    <!-- end shortcut left -->
                                                    <li <?php if ($this->uri->segment(1) == 'attach') echo 'class="active"'; ?>>
                                                        <a  href="<?php echo base_url() . 'attach'; ?>" >
                                                            <i class="fa fa-picture-o"><b class="bg-warning"></b></i>
                                                            <span>Logo & Banner</span>
                                                        </a>
                                                    </li>
                                                <?php //} ?>
                                                <?php //if (check_permision("quangcao_access")) { ?>
                                                    <!-- end shortcut left 
                                                    <li <?php if ($this->uri->segment(1) == 'quangcao') echo 'class="active"'; ?>>
                                                        <a  href="<?php echo base_url() . 'quangcao'; ?>" >
                                                            <i class="fa fa-picture-o"><b class="bg-warning"></b></i>
                                                            <span>Quảng Cáo</span>
                                                        </a>
                                                    </li>-->
                                                <?php //} ?>
                                                <?php //if (check_permision("contact_access")) { ?>
                                                    <!-- end shortcut left -->
                                                    <!--<li <?php if ($this->uri->segment(1) == 'contact') echo 'class="active"'; ?>>
                                                        <a  href="<?php echo base_url() . 'contact'; ?>" >
                                                            <i class="fa fa-envelope-o"><b class="bg-warning"></b></i>
                                                            <span>Liên hệ</span>
                                                        </a>
                                                    </li>-->
                                                <?php //} ?>
												<?php //if (check_permision("pages_access")) { ?>
                                                    <li <?php if ($this->uri->segment(1) == 'pages') echo 'class="active"'; ?>>
                                                        <a href="<?php echo base_url() . 'pages'; ?>" >
                                                            <i class="fa fa-tasks"><b class="bg-warning"></b></i>
                                                            <span>Pages</span>
                                                        </a>
                                                    </li>
                                                <?php //} ?>
												<?php //if (check_permision("urlalias_access")) { ?>
                                                    <li <?php if ($this->uri->segment(1) == 'urlalias') echo 'class="active"'; ?>>
                                                        <a href="<?php echo base_url() . 'urlalias'; ?>" >
                                                            <i class="fa fa-external-link"><b class="bg-warning"></b></i>
                                                            <span>Urlalias</span>
                                                        </a>
                                                    </li>
                                                <?php //} ?>
												<li <?php if ($this->uri->segment(1) == 'infomation/addedit/1') echo 'class="active"'; ?>>
													<a href="<?php echo base_url() . 'infomation/addedit/1'; ?>" >
														<i class="fa fa-home"><b class="bg-warning"></b></i>
														<span>Thông tin web</span>
													</a>
												</li>
                                                <!--<?php if (check_permision("votes")) { ?>
                                                                <li <?php if ($this->uri->segment(1) == 'votes') echo 'class="active"'; ?>>
                                                                    <a href="#posts">
                                                                        <i class="fa fa-tag icon"><b class="bg-warning"></b></i>
                                                                        <span class="pull-right">
                                                                            <i class="fa fa-angle-down text"></i>
                                                                            <i class="fa fa-angle-up text-active"></i>
                                                                        </span>
                                                                        <span>Câu hỏi Thăm dò</span>
                                                                    </a>
                                                                    <ul class="nav lt">
                                                                        <li>
                                                                            <a href="<?php echo base_url() . 'votes/addedit'; ?>" >                                                        
                                                                                <i class="fa fa-angle-right"></i>
                                                                                <span>Thêm thăm dò mới</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="<?php echo base_url() . 'votes'; ?>" >                                                        
                                                                                <i class="fa fa-angle-right"></i>
                                                                                <span>Danh sách thăm dò</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                <?php } ?>
                                                <?php if (check_permision("tags_access")) { ?>
                                                                <li <?php if ($this->uri->segment(1) == 'tags') echo 'class="active"'; ?>>
                                                                    <a href="#posts">
                                                                        <i class="fa fa-tag icon"><b class="bg-warning"></b></i>
                                                                        <span class="pull-right">
                                                                            <i class="fa fa-angle-down text"></i>
                                                                            <i class="fa fa-angle-up text-active"></i>
                                                                        </span>
                                                                        <span>Từ Khoá</span>
                                                                    </a>
                                                                    <ul class="nav lt">
                                                                        <li>
                                                                            <a href="<?php echo base_url() . 'tags/addedit'; ?>" >                                                        
                                                                                <i class="fa fa-angle-right"></i>
                                                                                <span>Thêm từ khoá mới</span>
                                                                            </a>
                                                                        </li>
                                                                        <li>
                                                                            <a href="<?php echo base_url() . 'tags'; ?>" >                                                        
                                                                                <i class="fa fa-angle-right"></i>
                                                                                <span>Danh sách từ khoá</span>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </li>
                                                <?php } ?>-->

                                                <li <?php if ($this->uri->segment(1) == 'user') echo 'class="active"'; ?>>
                                                    <a href="#posts">
                                                        <i class="fa fa-users icon"><b class="bg-warning"></b></i>
                                                        <span class="pull-right">
                                                            <i class="fa fa-angle-down text"></i>
                                                            <i class="fa fa-angle-up text-active"></i>
                                                        </span>
                                                        <span>Tải khoản</span>
                                                    </a>
                                                    <ul class="nav lt">
                                                        <?php //if (check_permision("users_access")) { ?>
                                                            <li>
                                                                <a href="<?php echo base_url() . 'user/addedit'; ?>" >                                                        
                                                                    <i class="fa fa-angle-right"></i>
                                                                    <span>Thêm tài khoản</span>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="<?php echo base_url() . 'user'; ?>" >                                                        
                                                                    <i class="fa fa-angle-right"></i>
                                                                    <span>Quản lý tài khoản</span>
                                                                </a>
                                                            </li>
                                                            <!--<li>
                                                              <a href="#" onclick="alert('Chức năng này đang cập nhật!');return false;" >                                                        
                                                                <i class="fa fa-angle-right"></i>
                                                                <span>Phân quyền</span>
                                                              </a>
                                                            </li>-->
                                                        <?php //} ?>
                                                        <li>
                                                            <a href="<?php echo base_url() . 'user/changepassword'; ?>" >                                                        
                                                                <i class="fa fa-angle-right"></i>
                                                                <span>Đổi mật khẩu</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <?php if (check_permision("roles_access") && check_permision("users_access")) { ?>
            <!--                                                    <li <?php if ($this->uri->segment(1) == 'roles') echo 'class="active"'; ?>>
                                                                <a href="#roles">
                                                                    <i class="fa fa-users icon"><b class="bg-warning"></b></i>
                                                                    <span class="pull-right">
                                                                        <i class="fa fa-angle-down text"></i>
                                                                        <i class="fa fa-angle-up text-active"></i>
                                                                    </span>
                                                                    <span>Nhóm quyền</span>
                                                                </a>
                                                                <ul class="nav lt">
                                                                    <li>
                                                                        <a href="<?php echo base_url() . 'roles'; ?>" >                                                        
                                                                            <i class="fa fa-angle-right"></i>
                                                                            <span>Quản lý nhóm quyền</span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>-->
                                                <?php } ?>
                                                <?php if (check_permision("experts_access")) { ?>
            <!--                                                    <li <?php if ($this->uri->segment(1) == 'expert') echo 'class="active"'; ?>>
                                                                <a href="#posts">
                                                                    <i class="fa fa-users icon"><b class="bg-warning"></b></i>
                                                                    <span class="pull-right">
                                                                        <i class="fa fa-angle-down text"></i>
                                                                        <i class="fa fa-angle-up text-active"></i>
                                                                    </span>
                                                                    <span>Chuyên gia</span>
                                                                </a>
                                                                <ul class="nav lt">
                                                                    <li>
                                                                        <a href="<?php echo base_url() . 'expert/addedit'; ?>" >                                                        
                                                                            <i class="fa fa-angle-right"></i>
                                                                            <span>Thêm chuyên gia</span>
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="<?php echo base_url() . 'expert'; ?>" >                                                        
                                                                            <i class="fa fa-angle-right"></i>
                                                                            <span>Quản lý chuyên gia</span>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </li>-->
                                                <?php } ?>
                                                <?php //if (check_permision("logs_access")) { ?>
                                                    <li <?php if ($this->uri->segment(1) == 'logs') echo 'class="active"'; ?>>
                                                        <a href="<?php echo base_url() . 'logs'; ?>"   >
                                                            <i class="fa fa-users icon"><b class="bg-warning"></b></i>
                                                            <span class="pull-right">
                                                                <i class="fa fa-angle-down text"></i>
                                                                <i class="fa fa-angle-up text-active"></i>
                                                            </span>
                                                            <span>Logs</span>
                                                        </a>
                                                    </li>
                                                <?php //} ?>

                                            </ul>
                                        </nav>
                                        <!-- / nav -->
                                    </div>
                                </section>

                                <footer class="footer lt hidden-xs b-t b-dark">
                                    <a href="#nav"  id="btn_menu_toggle" class="pull-right btn btn-sm btn-dark btn-icon">
                                        <i class="fa fa-angle-left text"></i>
                                        <i class="fa fa-angle-right text-active"></i>
                                    </a>
                                    <script>
                                        if (getCookie("menu_active") == 1) {
                                            $("#nav").addClass("nav-xs");
                                            $("#btn_menu_toggle").addClass("active");
                                        }
                                        $("#btn_menu_toggle").click(function () {
                                            //alert($(this).attr("class"));
                                            //data-toggle="class:nav-xs"
                                            if (!$(this).hasClass("active")) {
                                                $("#nav").addClass("nav-xs");
                                                $(this).addClass("active");
                                                setCookie("menu_active", 1, 30);
                                            } else {
                                                $("#nav").removeClass("nav-xs");
                                                $(this).removeClass("active");
                                                setCookie("menu_active", 0, 30);
                                            }
                                        });
                                        function setCookie(cname, cvalue, exdays) {
                                            var d = new Date();
                                            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                                            var expires = "expires=" + d.toUTCString();
                                            document.cookie = cname + "=" + cvalue + "; " + expires;
                                        }

                                        function getCookie(cname) {
                                            var name = cname + "=";
                                            var ca = document.cookie.split(';');
                                            for (var i = 0; i < ca.length; i++) {
                                                var c = ca[i];
                                                while (c.charAt(0) == ' ')
                                                    c = c.substring(1);
                                                if (c.indexOf(name) != -1)
                                                    return c.substring(name.length, c.length);
                                            }
                                            return "";
                                        }
                                    </script>
                                </footer>
                            </section>
                        </aside>
                        <!-- /.aside -->
                    <?php } ?>
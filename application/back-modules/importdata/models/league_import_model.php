<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	#echo 111111111111111111;
require_once APPPATH."third_party/YT/AModel.php";
class league_import_model extends YT_AModel {
       function __construct() {
            parent::__construct();
            
         $this->fields=array (
  'id' => 'PRI',
  'name' => 1,
  'short_name' => 1,
  'uname' => 1,
  'image' => 1,
  'content' => 1,
  'create_date' => 1,
  'modify_date' => 1,
  'show_ranking' => 1,
  'status' => 1,
  'scope' => 1,
  'ref_id' => 1,
  'ref_source' => 1,
             'current_season' => 1,
);
         $this->fields_lang=array(
            
        );
         $this->table_name='league';
         $this->id_field='id';
         $this->status_field='status';
         
        }
//        function rebuild_date(){
//            $this->getDB()->select("hash_key");
//            $this->getDB()->from($this->table_name);
//            $this->getDB()->where(array('time'=>null));
//            $list=$this->getDB()->get()->result();
//            foreach ($list as $value) {
//                $hash=  explode("_", $value->hash_key);
//                $number=$hash[count($hash)-1];
//                if($number){
//                    $date=intval($number/10000)."-".intval($number%10000/100)."-".intval($number%100);
//                    $this->getDB()->update($this->table_name,array('time'=>$date),array("hash_key"=>$value->hash_key));
//                }
//            }
//            echo 'finish';
//            
//            
//        }
       function convert_soccerxml($array){
           $store=array();
           foreach($array as $index =>$value){
               if($this->fields[strtolower($index)])
               $store[strtolower($index)]=$value;
           }
           if($store['date']){
               $store['date']=  date("Y-m-d H:i",strtotime($store['date']));
           }
           $store['uname']= $store['name'];
           $store['ref_id']=$store['id'];
           $store['ref_source']="socerxml";
           unset($store['id']);
           $this->getDB()->from($this->table_name);
           $this->getDB()->where(array("ref_id"=>$store['ref_id']));
           $arr=$this->getDB()->get()->result();
           $arr=  @current($arr);
           if($arr->id){
               $store['id']=$arr->id;
               unset($store['name']);
               
           }
           $this->store_data($store);
                   
       }
	function on_data_change(){
           return;
        }
	
}
?>
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	#echo 111111111111111111;
require_once APPPATH."third_party/YT/AModel.php";
class matchs_importer_model extends YT_AModel {
       function __construct() {
            parent::__construct();
         $this->fields=array (
  'id' => 'pri',
  'name' => 1,
  'short_name' => 1,
  'home' => 1,
  'away' => 1,
  'home_sheme' => 1,
  'away_sheme' => 1,
  'home_scheme_image' => 1,
  'away_scheme_image' => 1,
  'substitute_list_home' => 1,
  'main_list_home' => 1,
  'substitute_list_away' => 1,
  'main_list_away' => 1,
  'league_id' => 1,
  'league_name' => 1,
  'season_id' => 1,
  'season_code' => 1,
  'time' => 1,
  'round' => 1,
  'home_goal_1st' => 1,
  'away_goal_1st' => 1,
  'home_goal' => 1,
  'homegoaldetails' => 1,
  'away_goal' => 1,
  'awaygoaldetails' => 1,
  'create_date' => 1,
  'modify_date' => 1,
  'arbitration' => 1,
  'stadium' => 1,
  'stadium_id' => 1,
  'link' => 1,
  'state' => 1,
  'status' => 1,
  'home_name' => 1,
  'away_name' => 1,
  'hash_key' => 1,
  'league_code' => 1,
  'ref_id' => 1,
  'ref_source' => 1,
  'uname' => 1,
  'home_image' => 1,
  'away_image' => 1,
  'uleague' => 1,
  'uhome_name' => 1,
  'uaway_name' => 1,
  'ref_home_team_id' => 1,
  'ref_away_team_id' => 1,
  'ustadium' => 1,
  'hometeamyellowcarddetails' => 1,
  'awayteamyellowcarddetails' => 1,
  'hometeamredcarddetails' => 1,
  'awayteamredcarddetails' => 1,
  'spectators' => 1,
  'homelineupgoalkeeper' => 1,
  'homelineupdefense' => 1,
  'homelineupmidfield' => 1,
  'homelineupforward' => 1,
  'homelineupsubstitutes' => 1,
  'homelineupcoach' => 1,
  'awaylineupgoalkeeper' => 1,
  'awaylineupdefense' => 1,
  'awaylineupmidfield' => 1,
  'awaylineupforward' => 1,
  'awaylineupsubstitutes' => 1,
  'awaylineupcoach' => 1,
  'homesubdetails' => 1,
  'awaysubdetails' => 1,
             'ref_fixture_id'=>1,
			 'homeshots' => 1,
			 'awayshots' => 1,
			 'homeshotsontarget' => 1,
			 'awayshotsontarget' => 1,
			 'homecorners' => 1,
			 'awaycorners' => 1,
			 'homefouls' => 1,
			 'awayfouls' => 1,
);
         $this->fields_lang=array(
            
        );
         $this->table_name='match';
         $this->id_field='id';
         $this->status_field='status';
         
        }
       function convert_soccerxml($array){
           $store=array();
           foreach($array as $index =>$value){
               //if($this->fields[strtolower($index)])
               $store[strtolower($index)]=$value;
           }
           if($store['date']){
               $store['time']=  date("Y-m-d H:i",strtotime($store['date']));
           }
           //$store['match_id']=$store['id'];
           $store['ref_id']=$store['id'];
           $store['ref_fixture_id']=$store['fixturematch_id'];
           unset($store['id']);
           $store['ref_home_team_id']=$store['hometeam_id'];
           $store['ref_away_team_id']=$store['awayteam_id'];
           $store['ref_away_team_id']=$store['awayteam_id'];
           $store['home_sheme']=$store['hometeamformation'];
           $store['away_sheme']=$store['awayteamformation'];
           $store['away_goal']=$store['awaygoals'];
           $store['home_goal']=$store['homegoals'];
		   /***********them*************/
		   /*
			$store['homeshots']=$store['homeshots'];
			$store['awayshots']=$store['awayshots'];
			$store['homeshotsontarget']=$store['homeshotsontarget'];
			$store['awayshotsontarget']=$store['awayshotsontarget'];
			$store['homecorners']=$store['homecorners'];
			$store['awaycorners']=$store['awaycorners'];
			$store['homefouls']=$store['homefouls'];
			$store['awayfouls']=$store['awayfouls'];
			$store['hometeamyellowcarddetails']=$store['hometeamyellowcarddetails'];
			$store['awayteamyellowcarddetails']=$store['awayteamyellowcarddetails'];
			$store['hometeamredcarddetails']=$store['hometeamredcarddetails'];
			$store['awayteamredcarddetails']=$store['awayteamredcarddetails'];
		   */
		   /***********8*****************/
           $store['uleague']=$store['league'];
		   if(isset($store['halftimehomegoals'])||isset($store['halftimeawaygoals'])) {
				$store['home_goal_1st']=$store['halftimehomegoals'];
				$store['away_goal_1st']=$store['halftimeawaygoals'];
		   }
           
           //$store['ref_league_id']=$store['league_id'];
           $this->getDB()->from($this->table_name);
           $this->getDB()->or_where(array("ref_fixture_id"=>$store['fixturematch_id'],'ref_id'=>$store['ref_id']));
           $arr=$this->getDB()->get()->result();
           $arr=  @current($arr);
           if($arr->id){
               $store['id']=$arr->id;
           }
           $this->store_data($store);
                   
       }
	function on_data_change(){
           return;
        }
	
}
?>
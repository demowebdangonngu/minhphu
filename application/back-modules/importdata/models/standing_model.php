<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	#echo 111111111111111111;
require_once APPPATH."third_party/YT/AModel.php";
class standing_model extends YT_AModel {
       function __construct() {
            parent::__construct();
            
         $this->fields=array (
  'id' => 'PRI',
  'team' => 1,
  'team_id' => 1,
  'playedathome' => 1,
  'playedaway' => 1,
  'won' => 1,
  'draw' => 1,
  'lost' => 1,
  'goals_for' => 1,
  'goals_against' => 1,
  'goal_difference' => 1,
  'points' => 1,
  'create_date' => 1,
  'modify_date' => 1,
  'status' => 1,
  'league_id' => 1,
  'season' => 1,
  'ref_team_id' => 1,
  'ref_team_name' => 1,
);
         $this->fields_lang=array(
            
        );
         $this->table_name='standing';
         $this->id_field='id';
         $this->status_field='status';
         
        }
//        function rebuild_date(){
//            $this->getDB()->select("hash_key");
//            $this->getDB()->from($this->table_name);
//            $this->getDB()->where(array('time'=>null));
//            $list=$this->getDB()->get()->result();
//            foreach ($list as $value) {
//                $hash=  explode("_", $value->hash_key);
//                $number=$hash[count($hash)-1];
//                if($number){
//                    $date=intval($number/10000)."-".intval($number%10000/100)."-".intval($number%100);
//                    $this->getDB()->update($this->table_name,array('time'=>$date),array("hash_key"=>$value->hash_key));
//                }
//            }
//            echo 'finish';
//            
//            
//        }
       function convert_soccerxml($array,$league_id,$season){
           $store=array();
           foreach($array as $index =>$value){
               //if($this->fields[strtolower($index)])
               $store[strtolower($index)]=$value;
           }
           if($store['date']){
               $store['date']=  date("Y-m-d H:i",strtotime($store['date']));
           }
           $store['league_id']= $league_id;
           $store['season']=$season;
           $store['ref_team_id']=$store['team_id'];
           $store['ref_team_name']=$store['team'];
           unset($store['team']);
           unset($store['team_id']);
           
           //$store['ref_source']="socerxml";
           unset($store['id']);
           $this->getDB()->from($this->table_name);
           $this->getDB()->where(array("ref_team_id"=>$store['ref_team_id'],'league_id'=>$league_id,'season'=>$season));
           $arr=$this->getDB()->get()->result();
           $arr=  @current($arr);
           if($arr->id){
               $store['id']=$arr->id;
               unset($store['team']);
               
           }
           $this->store_data($store);
                   
       }
	function on_data_change(){
           return;
        }
	
}
?>
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	#echo 111111111111111111;
require_once APPPATH."third_party/YT/AModel.php";
class live_score_model extends YT_AModel {
       function __construct() {
            parent::__construct();
            
         $this->fields=array (
  'id' => 'PRI',
  'date' => 1,
  'uleague' => 1,
  'league_id' => 1,
  'season' => 1,
  'round' => 1,
  'home_name' => 1,
  'home_id' => 1,
  'home_image' => 1,
  'away_name' => 1,
  'away_id' => 1,
  'away_image' => 1,
  'time' => 1,
  'homegoals' => 1,
  'awaygoals' => 1,
  'homegoaldetails' => 1,
  'awaygoaldetails' => 1,
  'homelineupgoalkeeper' => 1,
  'awaylineupgoalkeeper' => 1,
  'homelineupdefense' => 1,
  'awaylineupdefense' => 1,
  'homelineupmidfield' => 1,
  'awaylineupmidfield' => 1,
  'homelineupforward' => 1,
  'awaylineupforward' => 1,
  'homelineupsubstitutes' => 1,
  'awaylineupsubstitutes' => 1,
  'homelineupcoach' => 1,
  'awaylineupcoach' => 1,
  'homesubdetails' => 1,
  'awaysubdetails' => 1,
  'hometeamformation' => 1,
  'awayteamformation' => 1,
  'location' => 1,
  'stadium' => 1,
  'hometeamyellowcarddetails' => 1,
  'awayteamyellowcarddetails' => 1,
  'hometeamredcarddetails' => 1,
  'awayteamredcarddetails' => 1,
  'create_date' => 1,
  'modify_date' => 1,
  'match_id' => 1,
  'status' => 1,
  'league_name' => 1,
  'ref_home_id' => 1,
  'ref_away_id' => 1,
  'ref_match_id' => 1,
);
         $this->fields_lang=array(
            
        );
         $this->table_name='livescore';
         $this->id_field='id';
         $this->status_field='status';
         
        }
//        function rebuild_date(){
//            $this->getDB()->select("hash_key");
//            $this->getDB()->from($this->table_name);
//            $this->getDB()->where(array('time'=>null));
//            $list=$this->getDB()->get()->result();
//            foreach ($list as $value) {
//                $hash=  explode("_", $value->hash_key);
//                $number=$hash[count($hash)-1];
//                if($number){
//                    $date=intval($number/10000)."-".intval($number%10000/100)."-".intval($number%100);
//                    $this->getDB()->update($this->table_name,array('time'=>$date),array("hash_key"=>$value->hash_key));
//                }
//            }
//            echo 'finish';
//            
//            
//        }
       function convert_soccerxml($array){
           $store=array();
           foreach($array as $index =>$value){
               //if($this->fields[strtolower($index)])
               $store[strtolower($index)]=$value;
           }
           if($store['date']){
               $store['date']=  date("Y-m-d H:i",strtotime($store['date']));
           }
           //$store['match_id']=$store['id'];
           $store['ref_match_id']=$store['match_id'];
           unset($store['match_id']);
           $store['ref_home_id']=$store['hometeam_id'];
           $store['ref_away_id']=$store['awayteam_id'];
           $store['ref_match_id']=$store['id'];
           unset($store['id']);
           //$store['ref_league_id']=$store['league_id'];
           $this->getDB()->from($this->table_name);
           $this->getDB()->where(array("ref_match_id"=>$store['ref_match_id']));
           $arr=$this->getDB()->get()->result();
           $arr=  @current($arr);
           if($arr->id){
               $store['id']=$arr->id;
           }
           $this->store_data($store);
                   
       }
       function on_data_change(){
           return;
        }
	
}
?>
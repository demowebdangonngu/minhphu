<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	#echo 111111111111111111;
require_once APPPATH."third_party/YT/AModel.php";
class players_importer_model extends YT_AModel {
       function __construct() {
            parent::__construct();
            
        $this->fields=array (
  'id' => 'pri',
  'name' => 1,
  'team_id' => 1,
  'national' => 1,
  'position' => 1,
  'number' => 1,
  'year' => 1,
  'hight' => 1,
  'weight' => 1,
  'image' => 1,
  'type' => 1,
  'create_date' => 1,
  'modify_date' => 1,
  'status' => 1,
  'play_status' => 1,
  'uname' => 1,
  'current_team' => 1,
  'ref_id' => 1,
  'ref_team_id' => 1,
  'dateofbirth' => 1,
  'dateofsigning' => 1,
  'signing' => 1,
);
         $this->fields_lang=array(
            
        );
         $this->table_name='players';
         $this->id_field='id';
         $this->status_field='status';
         
        }
//        function rebuild_date(){
//            $this->getDB()->select("hash_key");
//            $this->getDB()->from($this->table_name);
//            $this->getDB()->where(array('time'=>null));
//            $list=$this->getDB()->get()->result();
//            foreach ($list as $value) {
//                $hash=  explode("_", $value->hash_key);
//                $number=$hash[count($hash)-1];
//                if($number){
//                    $date=intval($number/10000)."-".intval($number%10000/100)."-".intval($number%100);
//                    $this->getDB()->update($this->table_name,array('time'=>$date),array("hash_key"=>$value->hash_key));
//                }
//            }
//            echo 'finish';
//            
//            
//        }
       function convert_soccerxml($array){
           $store=array();
           foreach($array as $index =>$value){
               //if($this->fields[strtolower($index)])
               $store[strtolower($index)]=$value;
           }
           if($store['dateofbirth']){
               $store['dateofbirth']=  date("Y-m-d H:i",strtotime($store['dateofbirth']));
           }
           if($store['dateofsigning']){
               $store['dateofsigning']=  date("Y-m-d H:i",strtotime($store['dateofsigning']));
           }
           //signing
           if($store['playernumber'])
           $store['number']=$store['playernumber'];
           $store['ref_id']=$store['id'];
          // $store['ref_team_id']=$store['team_id'];
           //unset($store['team_id']);
           unset($store['id']);
           $store['uname']=$store['name'];
           $this->getDB()->from($this->table_name);
           $this->getDB()->where(array("ref_id"=>$store['ref_id']));
           $arr=$this->getDB()->get()->result();
           $arr=  @current($arr);
           if($arr->id){
               $store['id']=$arr->id;
               unset($store['name']);
           }else{
               $store['short_name']=$store['name'];
               //$store['slug']=  remove_accent( $store['name']);
                $store['status']= 1;
           }
           $this->store_data($store);
                   
       }
	function on_data_change(){
           return;
        }
	
}
?>
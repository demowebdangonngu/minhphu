<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	#echo 111111111111111111;
require_once APPPATH."third_party/YT/AModel.php";
class import_model extends YT_AModel {
       function __construct() {
            parent::__construct();
            
         $this->fields=array (
  'id' => 'pri',
  'name' => 1,
  'short_name' => 1,
  'home' => 1,
  'away' => 1,
  'home_sheme' => 1,
  'away_sheme' => 1,
  'home_scheme_image' => 1,
  'away_scheme_image' => 1,
  'substitute_list_home' => 1,
  'main_list_home' => 1,
  'substitute_list_away' => 1,
  'main_list_away' => 1,
  'league_id' => 1,
  'league_name' => 1,
  'season_id' => 1,
  'season_code' => 1,
  'time' => 1,
  'round' => 1,
  'home_goal_1st' => 1,
  'away_goal_1st' => 1,
  'home_goal' => 1,
  'homegoaldetails' => 1,
  'away_goal' => 1,
  'awaygoaldetails' => 1,
  'create_date' => 1,
  'modify_date' => 1,
  'arbitration' => 1,
  'stadium' => 1,
  'stadium_id' => 1,
  'link' => 1,
  'state' => 1,
  'status' => 1,
  'home_name' => 1,
  'away_name' => 1,
  'hash_key' => 1,
  'league_code' => 1,
  'ref_id' => 1,
  'ref_source' => 1,
  'uname' => 1,
  'home_image' => 1,
  'away_image' => 1,
  'uleague' => 1,
  'uhome_name' => 1,
  'uaway_name' => 1,
  'ref_home_team_id' => 1,
  'ref_away_team_id' => 1,
  'ustadium' => 1,
  'hometeamyellowcarddetails' => 1,
  'awayteamyellowcarddetails' => 1,
  'hometeamredcarddetails' => 1,
  'awayteamredcarddetails' => 1,
  'spectators' => 1,
  'homelineupgoalkeeper' => 1,
  'homelineupdefense' => 1,
  'homelineupmidfield' => 1,
  'homelineupforward' => 1,
  'homelineupsubstitutes' => 1,
  'homelineupcoach' => 1,
  'awaylineupgoalkeeper' => 1,
  'awaylineupdefense' => 1,
  'awaylineupmidfield' => 1,
  'awaylineupforward' => 1,
  'awaylineupsubstitutes' => 1,
  'awaylineupcoach' => 1,
  'homesubdetails' => 1,
  'awaysubdetails' => 1,
             'ref_fixture_id' => 1,
);
         $this->fields_lang=array(
            
        );
         $this->table_name='match';
         $this->id_field='id';
         $this->status_field='status';
         
        }
function convert_soccerxml($array,$season){
           $store=array();
           foreach($array as $index =>$value){
               //if($this->fields[strtolower($index)])
               $store[strtolower($index)]=$value;
           }
           if($store['date']){
               $store['time']=  date("Y-m-d H:i",strtotime($store['date']));
           }
           //$store['match_id']=$store['id'];
           $store['ref_fixture_id']=$store['id'];
           unset($store['id']);
           $store['ref_home_team_id']=$store['hometeam_id'];
           $store['ref_away_team_id']=$store['awayteam_id'];
           $store['ref_away_team_id']=$store['awayteam_id'];
           $store['home_sheme']=$store['hometeamformation'];
           $store['away_sheme']=$store['awayteamformation'];
           $store['uleague']=$store['league'];
           $store['season_code']=$season;
           
           //$store['ref_league_id']=$store['league_id'];
           $this->getDB()->from($this->table_name);
           $this->getDB()->where(array("ref_fixture_id"=>$store['ref_fixture_id']));
           $arr=$this->getDB()->get()->result();
           $arr=  @current($arr);
           if($arr->id){
               $store['id']=$arr->id;
           }
           $this->store_data($store);
                   
       }
        function update_record1($row,$season_code,$state='pass'){
            $convert=$this->map1($row,$season_code,$state);
            $this->getDB()->from($this->table_name);
            $this->getDB()->where(array('hash_key'=>$convert['hash_key']));
            if($this->getDB()->count_all_results()>0){
                $this->getDB()->update($this->table_name,$convert,array('hash_key'=>$convert['hash_key']));
            }else{
                $this->getDB()->insert($this->table_name,$convert);
            }
            unset($convert);
        }
        //for football-data
        //$season_code: 1314,0809
        function map1($array,$season_code,$state='pass'){
            
            $datearr=  explode("/", $array['Date']);
            if(intval(@$datearr[2])<100&&intval(@$datearr[2])>=20){
                $datearr[2]="19".@$datearr[2];
            } 
            if(@intval(@$datearr[2])<100&&@intval(@$datearr[2])<=20){
                $datearr[2]="20".@$datearr[2];
            }
                $map=array(
            'name' => $array['HomeTeam']."-".$array['AwayTeam'],
            'short_name' => @$array['HomeTeam']."-".@$array['AwayTeam'],
            'season_code' => @$season_code,
            'home_goal_1st' => @$array['HTHG'],
            'away_goal_1st' => @$array['HTAG'],
            'home_goal' => @$array['FTHG'],
            'away_goal' => @$array['FTAG'],
            'state' => $state,
            'home_name' => @$array['HomeTeam'],
            'away_name' => @$array['AwayTeam'],
            'hash_key' => @$array['HomeTeam']."_".@$array['AwayTeam']."_".@$datearr[2].@$datearr[1].@$datearr[0],
            'league_code' => @$array['Div'],
                    
                );
                return $map;
        }
        //for soccer only fixtures
        function map2($array,$season_code){
            
            $time=strtotime($array['DATE']);
            
            $array['DATE']=date("Y-m-d H:i",$time);//giờ việt nam
            $datearr=  explode("/", date("d/m/Y",$time));
                $map=array(
                    //'league_name'=>$array['LEAGUE'],
            'name' => $array['HOMETEAM']."-".$array['AWAYTEAM'],
            'short_name' => @$array['HOMETEAM']."-".@$array['AWAYTEAM'],
            'season_code' => @$season_code,
            //'home_goal_1st' => @$array['HTHG'],
            //'away_goal_1st' => @$array['HTAG'],
            //'home_goal' => @$array['FTHG'],
            //'away_goal' => @$array['FTAG'],
            'uhome_name' => @$array['HOMETEAM'],
                    'time' => @$array['DATE'],
            'round' => @$array['ROUND'],
            'ustadium'=>@$array['LOCATION'],
            'uaway_name' => @$array['AWAYTEAM'],
            'hash_key' => @$array['HOMETEAM']."_".@$array['AWAYTEAM']."_".@$datearr[2].@$datearr[1].@$datearr[0],
                    'ref_id' => @$array['ID'],
                    'ref_source' => 'xmlsoccer',
                    'uleague' => @$array['LEAGUE'],
                    'ref_home_team_id' => @$array['HOMETEAM_ID'],
                    'ref_away_team_id' => @$array['AWAYTEAM_ID'],
                    
            //'league_code' => @$array['Div'],
                    
                );
                if($time<time()-90*60){
                $map['state']="pass";
                }
                if($time>time()+90*60){
                    $map['state']="next";
                }
                return $map;
        }
        function update_record2($row,$season_code){
            $convert=$this->map2($row,$season_code);
            $this->getDB()->from($this->table_name);
            $this->getDB()->where(array('hash_key'=>$convert['hash_key']));
            if($this->getDB()->count_all_results()>0){
                $this->getDB()->update($this->table_name,$convert,array('hash_key'=>$convert['hash_key']));
            }else{
                $this->getDB()->insert($this->table_name,$convert);
            }
            unset($convert);
        }
	function on_data_change(){
           return;
        }
	
}
?>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/ControllerAdmin.php";

class importdata extends YT_ControllerAdmin {
    function teams_request() {//echo date("h:i d/m/Y",strtotime('2014-07-25T05:18:28.993+07:00'));exit;
        ini_set('memory_limit','1024M');
        //$streamer = new SimpleXmlStreamer("./test.xml");
        //$file = "./test.xml";
/* @var $importer team_importer_model */
        $importer = $this->load->model('team_importer_model');
        
        $link = "http://www.xmlsoccer.com/FootballData.asmx/GetAllTeams?ApiKey={$this->api_key}";
        $filename="{$this->data_dir}/teams_".date("d-m-Y",time()).".xml";
        //echo $filename;exit;
        
        if (!file_exists($filename)) {
            echo "Download: ".$link . "\n";
            $content = file_get_contents($link);
            file_put_contents($filename, $content);
        }else echo "read file: ".$filename . "\n";
       $array = $this->XMLtoArray(file_get_contents($filename));
                /* @var $importer standing_model */
                //$standings = $this->load->model('standing_model');
        foreach ($array['XMLSOCCER.COM']['TEAM'] as $value) {
            $importer->convert_soccerxml($value);
        }
        echo "import " . count($array['XMLSOCCER.COM']['TEAM']) . " team!<br/>\n";
        $importer->getDB()->query("update standing join team on team.id = standing.team_id
set team_image=team.image");
        unset($content);
        unset($parsed);
         /* @var $caches MY_Caches */
           $caches=$this->load->library('Caches');
           $caches->on_livescore_update();
    }
    function import_standing() {
        //error_reporting(E_ALL);
        /* @var $importer league_import_model */
        $importer = $this->load->model('league_import_model');
        $leagues = $importer->get_list();
        foreach ($leagues as $leag) {
            if ($leag->ref_id) {
                echo "Request: {$leag->uname}\n";
                $link = "http://www.xmlsoccer.com/FootballData.asmx/GetLeagueStandingsBySeason?ApiKey={$this->api_key}&league={$leag->uname}&seasonDateString={$leag->current_season}";
                $filename = "./{$this->data_dir}/standing_{$leag->uname}" . date("dmY", time()) . ".xml";
                if (!file_exists($filename)) {
                    echo "download:" . $link . "\n";
                    //if (!file_exists($filename)) {
                    $content = $this->get_data($link);
                    file_put_contents($filename, $content);
                    //}
                }
                $array = $this->XMLtoArray(file_get_contents($filename));
                /* @var $importer standing_model */
                $standings = $this->load->model('standing_model');
                foreach ($array['XMLSOCCER.COM']['TEAMLEAGUESTANDING'] as $value) {
                    $standings->convert_soccerxml($value,$leag->id,$leag->current_season);
                }
                echo "import " . count($array['XMLSOCCER.COM']['TEAMLEAGUESTANDING']) . " team!<br/>\n";
                $standings->getDB()->query("UPDATE standing JOIN team ON team.`id`=standing.ref_team_id
                                            SET standing.team_id=team.id,standing.team=team.`name`,standing.team_image=team.`image`
                                            WHERE standing.team_id IS NULL");
                

            }
        }
        /* @var $caches MY_Caches */
           $caches=$this->load->library('Caches');
           $caches->on_livescore_update();
    }

    function import_league(){
        $link = "http://www.xmlsoccer.com/FootballData.asmx/GetAllLeagues?ApiKey={$this->api_key}";
        $filename="./{$this->data_dir}/leagues_".date("dmY",time()).".xml";
        if(!file_exists($filename)){
        echo "download:".$link . "\n";
        //if (!file_exists($filename)) {
            $content = file_get_contents($link);
            file_put_contents($filename, $content);
        //}
        }
         $array=$this->XMLtoArray(file_get_contents($filename));
         /* @var $importer league_import_model */
        $importer = $this->load->model('league_import_model');
         foreach ($array['XMLSOCCER.COM']['LEAGUE'] as $value) {
             $importer->convert_soccerxml($value);
         }
         
         echo "import ".count($array['XMLSOCCER.COM']['LEAGUE'])." match!";
          /* @var $caches MY_Caches */
           $caches=$this->load->library('Caches');
           $caches->on_livescore_update();
    }
    var $api_key="LRYQFUEHQVXKSMGFVVJQEHUOFNJHOCSQUQQQCDVSDNRTHLAHNQ";
    var $data_dir="./data";
    function request_oods(){
		$running=@file_get_contents($this->data_dir."/runodds");
		if($running==1) { echo "current running";exit;}
		file_put_contents($this->data_dir."/runodds", 1);
        /* @var $match matchs_model */
        $matchs=$this->load->model("matchs/matchs_model");
        $matchs->getDB()->from('match');
        $matchs->getDB()->where(array('time >'=>date('Y-m-d H:i:00',time()-90*60)));
        $matchs->getDB()->order_by("last_odd");
        $matchs->getDB()->limit(5);
        $matchlist=$matchs->getDB()->get()->result();
        $count=0;
        foreach ($matchlist as $match) {
            $link = "http://www.xmlsoccer.com/FootballData.asmx/GetOddsByFixtureMatchId?ApiKey={$this->api_key}&fixtureMatch_Id={$match->ref_id}";
            if(!is_dir("{$this->data_dir}/oods/")) mkdir ("{$this->data_dir}/oods/");
            $filename="{$this->data_dir}/oods/last_oods_{$match->ref_id}.xml";
            echo $link . "\n";
                $content = $this->get_data($link);
                if(!$content) continue;
                file_put_contents($filename, $content);
                $array=$this->XMLtoArray(file_get_contents($filename));
                if($array['XMLSOCCER.COM']['ODDS']){
                        $odd=current($array['XMLSOCCER.COM']['ODDS']);
                        if($odd['BET365_HOME']){
                            $item=array();
                            $item['type']='1x2';
                            $item['match_id']=$match->id;
                            $item['home']=$odd['BET365_HOME'];
                            $item['away']=$odd['BET365_AWAY'];
                            $item['draw']=$odd['BET365_DRAW'];
                            $item['draw']=$odd['BET365_DRAW'];
                            $item['update']=date('Y-m-d H:i:s',time());
                            /* @var $odd_model odds_model */
                            $odd_model=$this->load->model("odds/odds_model");
                            $odd_model->where=array('match_id'=>$match->id,'type'=>'1x2');
                            $obj=$odd_model->get_object_by_where();
                            if($obj->id){
                                $item['id']=$obj->id;
                            }
                            $odd_model->store_data($item);
                            $count++;
                        }
                        
                }
//                if($array['XMLSOCCER.COM']['ODDS']['BET365_HOME']){
//                    
//                }
                $matchs->getDB()->update("match",array('last_odd'=>date('Y-m-d H:i:00',time())),array('id'=>$match->id));
        }
//        
//        
//         
//         /* @var $importer live_score_model */
//        $importer = $this->load->model('live_score_model');
//         foreach ($array['XMLSOCCER.COM']['MATCH'] as $value) {
//             $importer->convert_soccerxml($value);
//         }
//         
         echo "import ".$count." odds!\n";
		 file_put_contents($this->data_dir."/runodds", 0);
         /* @var $caches MY_Caches */
           $caches=$this->load->library('Caches');
           $caches->on_livescore_update();
		exit;		   
         
    }
    function request_livescore(){
		$running=@file_get_contents($this->data_dir."/runlivescores");
		if($running==1) { echo "current running";exit;}
		file_put_contents($this->data_dir."/runlivescores",1);
        $link = "http://www.xmlsoccer.com/FootballData.asmx/GetLiveScore?ApiKey=$this->api_key";
        $filename="{$this->data_dir}/livescore_last.xml";
        echo $link . "\n";
        //if (!file_exists($filename)) {
            $content = $this->get_data($link);
            file_put_contents($filename, $content);
        //}
         $array=$this->XMLtoArray(file_get_contents($filename));
         /* @var $importer live_score_model */
        $importer = $this->load->model('live_score_model');
         foreach ($array['XMLSOCCER.COM']['MATCH'] as $value) {
             $importer->convert_soccerxml($value);
         }
         echo "import ".count($array['XMLSOCCER.COM']['MATCH'])." match!";
         if(count(count($array['XMLSOCCER.COM']['MATCH']))){
         $this->update_league_livescore();
		 //unlink($filename);
		 file_put_contents($this->data_dir."/runlivescores",0);
          /* @var $caches MY_Caches */
           $caches=$this->load->library('Caches');
           $caches->on_livescore_update();
         }
    }
    function update_league_livescore(){
        $livescore=$this->load->model('livescores/livescores_model');
        $livescore->getDB()->query("UPDATE livescore
INNER JOIN league ON livescore.uleague = league.uname
SET livescore.league_id = league.id,livescore.league_name = league.name
WHERE livescore.league_id = 0");
        $livescore->getDB()->query("UPDATE livescore
INNER JOIN team ON livescore.ref_home_id = team.id
SET livescore.home_id = team.id,livescore.home_name = team.name,livescore.home_image = team.image
WHERE livescore.home_id = 0");
        $livescore->getDB()->query("UPDATE livescore
INNER JOIN team ON livescore.ref_away_id = team.id
SET livescore.away_id = team.id,livescore.away_name = team.name,livescore.away_image = team.image
WHERE livescore.away_id = 0");
        $livescore->getDB()->query("UPDATE livescore
INNER JOIN `match` ON livescore.ref_match_id = `match`.ref_id
SET livescore.match_id = `match`.id
WHERE livescore.match_id = 0");
        


        

        /*$livescore=$this->load->model('livescores/livescores_model');
        $leagues=$this->load->model('leagues/leagues_model');
        $livescore->where=array('league_id'=>0);
        $lives=$livescore->get_list();
        foreach($lives as $live){
            $leagues->where['uname']=$live->league;
            $league=$leagues->get_object_by_where();
            if($league->id)
            //$live->league_id=$league->id;
            $livescore->getDB()->update($livescore->table_name,array('league_id'=>$league->id),array('id'=>$live->id));
        }*/
    }
    function get_data($url) {
	$ch = curl_init();
	$timeout = 5;
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
	$data = curl_exec($ch);
	curl_close($ch);
	return $data;
}
    function request_result(){
        //error_reporting(E_ERROR  | E_PARSE | E_COMPILE_ERROR | E_CORE_ERROR);
                        //ini_set('display_errors', 1);
        /* @var $importer league_import_model */
        $lea = $this->load->model('league_import_model');
        $lea->getDB()->from("league");
        $lea->getDB()->where(array('status'=>1));
        $lea->getDB()->order_by("last_result_request");
        $lea->getDB()->limit(1);
        $leag=current($lea->getDB()->get()->result());
            if ($leag->ref_id) {
                echo "Request: {$leag->uname}\n";
                $from=date("Y-m-d",time()-86400*30*2);
                $to=date("Y-m-d",time()+86400*2);
                $unameescap=urlencode($leag->uname);
        $link = "http://www.xmlsoccer.com/FootballData.asmx/GetHistoricMatchesByLeagueAndDateInterval?ApiKey={$this->api_key}&league={$unameescap}&startDateString=$from&endDateString=$to";
        $name=  remove_accent($leag->uname,"_");
        $filename="./{$this->data_dir}/result_{$name}_".date("h_i_d_m_Y").".xml";
        //$filename="./{$this->data_dir}/result_Allsvenskan_01_45_04_08_2014.xml";
        //echo $filename;exit;
        
        if (!file_exists($filename)) {
            echo "Download ".$link . "\n";
            $content = $this->get_data($link);
            echo "put to: $filename\n";
            file_put_contents($filename, $content);
        }else{
            echo "read ".$filename . "\n";
        }
         $array=$this->XMLtoArray(file_get_contents($filename));
//         echo "<pre>";
//         print_r($array);
//         echo "</pre>";
//         exit;
         /* @var $importer matchs_importer_model */
        $importer = $this->load->model('matchs_importer_model');
         foreach ($array['XMLSOCCER.COM']['MATCH'] as $value) {
             $importer->convert_soccerxml($value);
         }
         $lea->getDB()->update("league",array('last_result_request'=>date("Y-m-d H:i:0")),array('id'=>$leag->id));
        
       }
         echo "import ".count($array['XMLSOCCER.COM']['MATCH'])." match!";
         $importer->getDB()->query("UPDATE `match`
JOIN team 
ON `match`.ref_home_team_id=team.`ref_id`
SET home=team.id,home_name=team.`name`,match.home_image=team.image
WHERE home IS NULL AND `match`.ref_home_team_id >0");
         $importer->getDB()->query("UPDATE `match`
JOIN team 
ON `match`.ref_away_team_id=team.`ref_id`
SET away=team.id,away_name=team.`name`,match.away_image=team.image
WHERE home IS NULL AND `match`.ref_home_team_id >0");
         $importer->getDB()->query("UPDATE `match` JOIN league ON `match`.`uleague`=`league`.`uname`
SET `match`.`league_id`=`league`.`id`
WHERE `match`.league_id IS NULL");
         $importer->getDB()->query("update `match` set name=concat(home_name,'-',away_name) where name is null");
         //$this->update_league_livescore();
         echo "Clear cache\n";
          /* @var $caches MY_Caches */
           $caches=$this->load->library('Caches');
           $caches->on_livescore_update();
           exit;
    }
    function football_data_download() {
        /* @var $importer import_model */
        $importer = $this->load->model('import_model');
        //echo 1;exit;
        $league = array(
            'E0' => 'Premier League',
            'E1' => 'Championship',
            'E2' => 'League 1',
            'E3' => 'League 2',
            'EC' => 'Conference',
        );
        for ($i = 1993; $i < 2014; $i++) {
            echo $i.":\n";
            foreach ($league as $code => $name) {
                $season = substr($i, -2, 2) . substr($i + 1, -2, 2);
                $link = "http://www.football-data.co.uk/mmz4281/$season/$code.csv";
                echo $link . "\n";
                if (!file_exists("{$this->data_dir}/{$season}_{$code}.csv")) {
                $content = file_get_contents($link);
                    file_put_contents("{$this->data_dir}/{$season}_{$code}.csv", $content);
                    unset($content);
                }
            }
        }
    }
    function football_data() {//echo date("h:i d/m/Y",strtotime('2014-07-25T05:18:28.993+07:00'));exit;
        /* @var $importer import_model */
        $importer = $this->load->model('import_model');
        $importer->rebuild_date();
        exit;
        //echo 1;exit;
        $league = array(
            'E0' => 'Premier League',
            'E1' => 'Championship',
            'E2' => 'League 1',
            'E3' => 'League 2',
            'EC' => 'Conference',
        );
        #error_reporting(E_ALL);
        for ($i = 2013; $i < 2014; $i++) {
            echo $i.":\n";
            foreach ($league as $code => $name) {
                $season = substr($i, -2, 2) . substr($i + 1, -2, 2);
                $link = "http://www.football-data.co.uk/mmz4281/$season/$code.csv";
                echo $link . "\n";
                if (!file_exists("{$this->data_dir}/{$season}_{$code}.csv")) {
                $content = file_get_contents($link);
                    file_put_contents("{$this->data_dir}/{$season}_{$code}.csv", $content);}
                    $parsed = $this->csv_to_array("{$season}_{$code}.csv");
                    foreach ($parsed as $value) {
                        $importer->update_record1($value, $season, $state = "pass");
                    }
                    unset($content);
                    unset($parsed);
                //}
            }
        }
    }
    function fixture_request() {//echo date("h:i d/m/Y",strtotime('2014-07-25T05:18:28.993+07:00'));exit;
        ini_set('memory_limit','1024M');
        //$streamer = new SimpleXmlStreamer("./test.xml");
        //$file = "./test.xml";
/* @var $importer import_model */
        $importer = $this->load->model('import_model');
        $from = date("Y-m-d", time() - 86400);
        $to = date("Y-m-d", time() + 86400*30);//data for a month
        $season = substr(date("Y",time()), -2, 2) ;
        
        $link = "http://www.xmlsoccer.com/FootballData.asmx/GetFixturesByDateInterval?ApiKey=$this->api_key&startDateString=$from&endDateString=$to";
        $filename="{$this->data_dir}/soccer_{$from}_{$to}.xml";
        //echo $filename;exit;
        
        if (!file_exists($filename)) {
            echo "Download: ".$link . "\n";
            $content = file_get_contents($link);
            file_put_contents($filename, $content);
        }else echo "read file: ".$filename . "\n";
       $array=$this->XMLtoArray(file_get_contents($filename));
        foreach ($array['XMLSOCCER.COM']['MATCH'] as $row) {
             $importer->convert_soccerxml($row, $season);
        }
        echo "\nProcess ".count($array['XMLSOCCER.COM']['MATCH'])."match(s)";
         $importer->getDB()->query("UPDATE `match`
JOIN team 
ON `match`.ref_away_team_id=team.`ref_id`
SET away=team.id,away_name=team.`name`,match.away_image=team.image
WHERE away IS NULL AND `match`.ref_away_team_id >0");
        $importer->getDB()->query("UPDATE `match`
JOIN team 
ON `match`.ref_home_team_id=team.`ref_id`
SET home=team.id,home_name=team.`name`,match.home_image=team.image
WHERE home IS NULL AND `match`.ref_home_team_id >0");
        $importer->getDB()->query("UPDATE `match` JOIN league ON `match`.`uleague`=`league`.`uname`
SET `match`.`league_id`=`league`.`id`
WHERE `match`.league_id IS NULL");
        
        unset($content);
        unset($parsed);
         /* @var $caches MY_Caches */
           $caches=$this->load->library('Caches');
           $caches->on_livescore_update();
    }

function XMLtoArray($XML)
{
    $xml_parser = xml_parser_create();
    xml_parse_into_struct($xml_parser, $XML, $vals);
    xml_parser_free($xml_parser);
    // wyznaczamy tablice z powtarzajacymi sie tagami na tym samym poziomie
    $_tmp='';
    foreach ($vals as $xml_elem) {
        $x_tag=$xml_elem['tag'];
        $x_level=$xml_elem['level'];
        $x_type=$xml_elem['type'];
        if ($x_level!=1 && $x_type == 'close') {
            if (isset($multi_key[$x_tag][$x_level]))
                $multi_key[$x_tag][$x_level]=1;
            else
                $multi_key[$x_tag][$x_level]=0;
        }
        if ($x_level!=1 && $x_type == 'complete') {
            if ($_tmp==$x_tag)
                $multi_key[$x_tag][$x_level]=1;
            $_tmp=$x_tag;
        }
    }
    // jedziemy po tablicy
    foreach ($vals as $xml_elem) {
        $x_tag=$xml_elem['tag'];
        $x_level=$xml_elem['level'];
        $x_type=$xml_elem['type'];
        if ($x_type == 'open')
            $level[$x_level] = $x_tag;
        $start_level = 1;
        $php_stmt = '$xml_array';
        if ($x_type=='close' && $x_level!=1)
            $multi_key[$x_tag][$x_level]++;
        while ($start_level < $x_level) {
            $php_stmt .= '[$level['.$start_level.']]';
            if (isset($multi_key[$level[$start_level]][$start_level]) && $multi_key[$level[$start_level]][$start_level])
                $php_stmt .= '['.($multi_key[$level[$start_level]][$start_level]-1).']';
            $start_level++;
        }
        $add='';
        if (isset($multi_key[$x_tag][$x_level]) && $multi_key[$x_tag][$x_level] && ($x_type=='open' || $x_type=='complete')) {
            if (!isset($multi_key2[$x_tag][$x_level]))
                $multi_key2[$x_tag][$x_level]=0;
            else
                $multi_key2[$x_tag][$x_level]++;
            $add='['.$multi_key2[$x_tag][$x_level].']';
        }
        if (isset($xml_elem['value']) && trim($xml_elem['value'])!='' && !array_key_exists('attributes', $xml_elem)) {
            if ($x_type == 'open')
                $php_stmt_main=$php_stmt.'[$x_type]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
            else
                $php_stmt_main=$php_stmt.'[$x_tag]'.$add.' = $xml_elem[\'value\'];';
            eval($php_stmt_main);
        }
        if (array_key_exists('attributes', $xml_elem)) {
            if (isset($xml_elem['value'])) {
                $php_stmt_main=$php_stmt.'[$x_tag]'.$add.'[\'content\'] = $xml_elem[\'value\'];';
                eval($php_stmt_main);
            }
            foreach ($xml_elem['attributes'] as $key=>$value) {
                $php_stmt_att=$php_stmt.'[$x_tag]'.$add.'[$key] = $value;';
                eval($php_stmt_att);
            }
        }
    }
    return $xml_array;
}
    private function csv_to_array($filename = '', $delimiter = ',') {
        if (!file_exists($filename) || !is_readable($filename))
            return FALSE;

        $header = NULL;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                if (!$header)
                    $header = $row;
                else
                    $data[] = @array_combine($header, $row);
            }
            fclose($handle);
        }
        return $data;
    }

    function show_column($table) {
        $whitelist = array(
            '127.0.0.1',
            '192.168.1.125',
            '192.168.2.1',
            '::1'
        );

        if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
            echo "your IP: :)";die();
        }
        /* @var $importer import_model */
        $importer = $this->load->model('import_model');
        $query = $importer->getDB()->query("SHOW COLUMNS FROM `$table`;");
        $fields = array();
        foreach ($query->result() as $row) {
            $fields[$row->Field] = 1;
            if ($row->Key == 'PRI')
                $fields[$row->Field] = 'PRI';
        }
        echo "<pre>".strtolower(var_export($fields,true))."</pre>";exit;
    }
    /**
     *
     * @var Test_model 
     */
    public $model;
    
    function __construct() {
        $class = str_replace(CI::$APP->config->item('controller_suffix'), '', get_class($this));
        log_message('debug', $class . " MX_Controller Initialized");
        Modules::$registry[strtolower($class)] = $this;

        /* copy a loader instance and initialize */
        $this->load = clone load_class('Loader');
        $this->load->initialize($this);

        /* autoload module items */
        $this->load->_autoloader($this->autoload);
        $this->load->library('adminclass_model');
        // if(!$_SESSION['admin_id']&&($this->uri->segment(1)!='user'&&$this->uri->segment(2)!='login')) redirect (base_url()."user/login");
        //$user_group_id		= $this->session->userdata('admin_group_id');//1;
        //$this->lang->load( 'global', $this->config->item('language_admin'));
        $this->load->library('form_validation');
        $this->module_name = $this->uri->segment(1);
        //$this->config->load('define',TRUE);
        //$this->model=$this->load->model('test_model');
        //$this->data['return_url']=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:base_url().$this->uri->segment(1); 
        //$this->data['controller']=$this;
        //$this->data['model']=$this->model;
        $this->config->item('time_zone') ? date_default_timezone_set($this->config->item('time_zone')) : "";
        if(!is_dir($this->data_dir)){
            mkdir($this->data_dir);
            chmod($this->data_dir,0777);
        }
        //echo date("H:i d/m/Y",  strtotime("2014-08-16T03:45:00-08:00"));exit;
    }

}





?>
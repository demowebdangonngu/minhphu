<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class home_category_model extends YT_AModel {

    function __construct() {
        parent::__construct();

        $this->fields = array(
            'id' => 'pri',
            'cat_id' => 1,
            'title' => 1,
            'sort_order' => 1,
        );
        $this->table_name = 'home_category';
        $this->id_field = 'id';
        $this->status_field = 'status';
    }

    function on_data_change() {
        /* @var $caches MY_Caches */
        $caches = $this->load->library('Caches');
        $caches->on_post_update();
        //parent::on_data_change();
    }

}

?>
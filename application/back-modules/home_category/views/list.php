<script>
    var module = '<?php echo $this->uri->segment(1); ?>';
    var base_url = '<?php echo base_url(); ?>';
</script>
<section id="content">

    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">

                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>"><?php
                        if ($module_title) {
                            echo $module_title;
                        } else {
                            echo $this->uri->segment(1);
                        };
                        ?></a></li>
                <!--<li class="active">Static table</li>-->
            </ul>
            <div class="row">
                <div class="col-sm-6">
                    <input type="hidden" name="menu_order" id="menu-top-order" value="">
                    <section class="panel panel-default">
                        <header class="panel-heading">
                            <span class="h4">Danh sách home cate</span>
                        </header>
                        <div class="panel-body">
                            <section class="vbox" style="height: 500px;">
                            <section class="scrollable wrapper">
                            <div class="catalog" data-output="">
                                <ol class="dd-list" id="top">
                                    <?php
                                    /* @var $catemodel Categories_model */
                                    $catemodel = $this->load->model("categories/Categories_model");
                                    isset($_GET['trash']) ? $status_array = array(0, 1, 2) : $status_array = array(0, 1);
                                    $categories = $catemodel->get_tree($status_array);
                                    if (isset($categories)) {

                                        foreach ($categories as $cat) {
                                            ?>
                                            <li class="dd-item parent_category <?php echo "status_" . $cat->status; ?>">
                                                <div class="dd-handle"><?php echo $cat->name; ?></div>
                                                <input value="<?php echo $cat->id ?>" hidden=""/>
                                            </li>
                                            <?php
                                            if (count($cat->children)) {
                                                ?>
                                                <ol class="dd-list">
                                                    <?php
                                                    foreach ($cat->children as $cat2) {
                                                        ?>
                                                        <li class="dd-item <?php echo "status_" . $cat2->status; ?>">
                                                            <div class="dd-handle"><?php echo $cat2->name; ?></div>
                                                            <input value="<?php echo $cat2->id ?>" hidden=""/>
                                                        </li>
                                                        <?php
                                                        if (count($cat2->children)) {
                                                            ?>
                                                            <ol class="dd-list">
                                                                <?php
                                                                foreach ($cat2->children as $cat3) {
                                                                    ?>
                                                                    <li class="dd-item <?php echo "status_" . $cat3->status; ?>">
                                                                        <div class="dd-handle"><?php echo $cat3->name; ?></div>
                                                                        <input value="<?php echo $cat3->id ?>" hidden=""/>
                                                                    </li>
                                                                    <?php
                                                                }
                                                                ?>

                                                            </ol>  
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </ol>
                                                <?php
                                            }
                                            ?>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </ol>
                            </div>
                            </section>
                            </section>
                        </div>
                    </section>
                </div>
                <div class="col-sm-6">
                    <form  method="post" id='add-insert-menu-frm' action="<?php echo base_url() . $this->uri->segment(1); ?>/addedit_process/" onsubmit="update_form($(this), update_form_callback);
                            return false;">
                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <span class="h4">Thêm sửa danh mục</span>
                            </header>
                            <div class="panel-body">
                                <div id="msg_error_msg" style="color:#f00;"></div>
                                <style>
                                    #sortable {
                                        border: 1px solid #eee;
                                        width: 510px;
                                        min-height: 20px;
                                        list-style-type: none;
                                        margin: 0;
                                        padding: 5px 0 0 0;
                                        float: left;
                                        margin-right: 10px;
                                    }
                                    #sortable li {
                                        margin: 0 5px 5px 5px;
                                        padding: 5px;
                                        font-size: 1.2em;
                                        width: 480px;
                                        margin-left: -40px;
                                    }
                                </style>
                                <div class="form-group" id="sortable">
                                    <ol id="sortable_ol">
                                        <?php
                                        $home_cat = $this->load->model("home_category_model");
                                        $home_cat->join('categories',"categories.id=home_category.cat_id");
                                        $home_cat->order_by("sort_order", "ASC");
                                        $list_home_cat = $home_cat->get()->result();
                                        if ($list_home_cat) {
                                            $count = 0;
                                            foreach ($list_home_cat as $row) {
                                                $count++;
                                                ?>
                                                <li class='dd-item parent_category'>
                                                    <input value='<?php echo $row->cat_id; ?>' name='data[<?php echo $count; ?>][cat_id]' hidden=''/>
                                                    <input type='text' style="float: left;width: 30%;" class='form-control' value='<?php echo $row->name; ?>' disabled="">
                                                    <input type='text' style="width: 70%;" name='data[<?php echo $count; ?>][name]' class='form-control' value='<?php echo $row->title; ?>'>
                                                    <a href='#' onclick='$(this).parents("li").remove();' ><i style='float: right;margin-right: -30px;margin-top: -25px;' class='fa fa-times icon-muted fa-fw'></i></a>
                                                </li>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <div class="placeholder">Thêm danh mục</div>
                                        <?php } ?>
                                    </ol>
                                </div>

                            </div>
                            <div class="form-group">
                            </div>
                            <div class="clear"></div>
                            <footer class="text-right">
                                <input type="button" onclick="update_form($('#add-insert-menu-frm'), update_form_callback);
                                        return false;" class="btn btn-success btn-s-xs" value="Cập nhật"/>
                            </footer>
                        </section>
                    </form>
                </div>


            </div>
            <?php ?>
            </div>
        </section>
    </section>
    <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
</section>
<script>
    $(document).ready(function ()
    {
        var stt = <?php if($count){echo $count;}else{echo 0;} ?>;
        $(function () {

            $(".catalog li").draggable({
                appendTo: "body",
                helper: "clone"
            });
            $("#sortable ol").droppable({
                accept: ":not(.ui-sortable-helper)",
                drop: function (event, ui) {
                    stt++;
                    var cat_id = $(ui.draggable[0]).find('input').val();
                    $(this).find(".placeholder").remove();
                    $("<li class='dd-item parent_category'><input type='text' style='float: left;width: 30%;' class='form-control' id='name-categories' value='' disabled=''><input value='' id='cat_id' name='' hidden=''/><input style='width: 70%;' type='text' name='' id='cat_name' class='form-control' value=''><a href='#' onclick='$(this).parents(\"li\").remove();' ><i style='float: right;margin-right: -30px;margin-top: -25px;' class='fa fa-times icon-muted fa-fw'></i></a></li>").appendTo(this);

                    $('#cat_name').attr('name', 'data[' + stt + '][name]');
                    $('#cat_name').attr('value', ui.draggable.text().trim());
                    $('#cat_name').attr('id', "name-cat-" + stt);
                    $('#name-categories').attr('value', ui.draggable.text().trim());
                    $('#name-categories').attr('id', "name-categories-" + stt);
                    $('#cat_id').attr('name', 'data[' + stt + '][cat_id]');
                    $('#cat_id').attr('value', cat_id);
                    $('#cat_id').attr('id', "cat-id-" + stt);
                }
            }).sortable();
        });
    });
    function update_form_callback(data) {
        if (data.status)
            window.location = base_url + module;
    }
</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require_once APPPATH."third_party/YT/ControllerAdmin.php";
class home_category extends YT_ControllerAdmin {
	/**
     *
     * @var Test_model 
     */
	public $model;
	function __construct()	{
		parent::__construct();	
		$this->model=$this->load->model('home_category_model');
                if($this->uri->segment(1)!='home_category') redirect (base_url()."home_category");
                 $this->check_access('home_category');
	}
        function addedit_process() {
            $count = 0;
            foreach ($_POST['data'] as $row) {
                $cat_id[] = $row['cat_id'];
                $this->model->insert_or_dup_update("home_category", array('cat_id'=>$row['cat_id'],'title'=>$row['name'],'sort_order'=>++$count));
            }
            $this->model->getDB()->where_not_in('cat_id', $cat_id);
            $this->model->getDB()->delete('home_category');
            $result['status']=0;
            echo json_encode($result);
            exit;
        }
}
 ?>
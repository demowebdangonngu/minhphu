<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class votes_option_model extends YT_AModel {

    function __construct() {
        parent::__construct();

        $this->fields = array(
            'id' => 'pri',
            'vote_id' => 1,
            'name' => 1,
            'type' => 1,
            'count' => 1,
            'sort_order' => 1,
        );
        $this->table_name = 'vote_option';
        $this->table_name_lang='';
         $this->id_field='id';
         $this->status_field='status';
         $this->order_field='sort_order';
         $this->image_field=array('image'=>'');
    }

    function on_data_change() {
        /* @var $caches MY_Caches */
        $caches = $this->load->library('Caches');
        $caches->on_post_update();
        //parent::on_data_change();
    }

}

?>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class Posts_model extends YT_AModel {

    function __construct() {
        parent::__construct();



        $this->fields = array(
            'id' => 'pri',
            'cat_id' => 1,
            'admin_id' => 1,
            'title' => 1,
            'content' => 1,
            'description' => 1,
            'image' => 1,
            'public_date' => 1,
            'status' => 1,
        );
        $this->table_name = 'posts';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->id_field_lang = '';
        $this->status_field = 'posts.status';
        $this->cat_field = '';
        $this->order_field = '';
        $this->image_field = array('image' => '');
        $this->id_lang_rel = $this->id_field;
    }

    function on_data_change() {
        /* @var $caches MY_Caches */
        $caches = $this->load->library('Caches');
        $caches->on_post_update();
        //parent::on_data_change();
    }

    function getTopAuthor($type='likes'){
        
        //$this->getDB()->start_cache();
        $this->getDB()->select('users.*,posts_count.post_id,sum(posts_count.count) as count,NOW() - INTERVAL 7 DAY');
        $this->getDB()->from('users');
        $this->getDB()->join('posts','posts.uid=users.id','left');
        $this->getDB()->join('posts_count','`posts.id=posts_count.post_id`','left');
        $this->getDB()->group_by('users.id');
        $this->getDB()->where('posts_count.type',$type);
        
         
        $this->getDB()->limit(50);
        $this->getDB()->order_by('count',"DESC");
        $this->getDB()->where('posts_count.time >= NOW() - INTERVAL 7 DAY');
        //$this->getDB()->stop_cache();
        
        
        if($type=='count'){
            //pr($this->getDB());die;
            $this->getDB()->ar_select=array();
            $this->getDB()->ar_where=array();
            //$this->getDB()->ar_orderby=array();
            $this->getDB()->select('users.*,posts_count.post_id,count(posts.id) as count ');
            $this->getDB()->where('posts_count.type','views');
            $this->getDB()->where('posts.publish_date >= NOW() - INTERVAL 7 DAY');
           // $this->getDB()->order_by('count(id) as count',"DESC");
        }
        
		
        $ls=$this->getDB()->get()->result();
        
        //pr($this->getDB()->last_query());die;
        return $ls;
        //pr($ls);die;  
        
    }
    function delete_id($id){
        $this->getDB()->where('id',$id);
        $this->getDB()->delete($this->table_name);
    }
    
}

?>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/ControllerAdmin.php";

class Posts extends YT_ControllerAdmin {

    /**
     *
     * @var Test_model 
     */
    public $model;

    function __construct() {
        // pr($this->uri);die;
        parent::__construct();
        $this->model = $this->load->model('Posts_model');
        $this->lang->load('posts', $this->config->item('language_admin'));
        //if($this->uri->segment(1)!='posts') redirect (base_url()."posts");
        $this->check_access('posts_management_access');
    }

    public function search($array_fillter) {

        if (is_numeric($array_fillter['key'])) {
            $this->model->where = array($this->model->table_name . "." . $this->model->id_field => $array_fillter['key']);
        } else {


            if ($array_fillter['from']) {
                $date = explode("/", $array_fillter['from']);
                $fromdate = intval($date[2]) . "-" . intval($date[1]) . "-" . intval($date[0]) . " 00:00:00";
                $this->model->where['posts.create_date >='] = $fromdate;
            }
            if ($array_fillter['to']) {
                $date = explode("/", $array_fillter['to']);
                $fromto = intval($date[2]) . "-" . intval($date[1]) . "-" . intval($date[0]) . " 23:59:59";
                $this->model->where['posts.create_date <='] = $fromto;
            }
            if ($array_fillter['key']) {
                $this->model->like = array('posts.title', '' . $array_fillter['key'] . '');
            }
            if ($array_fillter['status']) {
                if ($array_fillter['status'] == 'schedule') {
                    $this->model->where['status'] = 'publish';
                    $this->model->where['isschedule'] = 1;
                } else
                    $this->model->where['status'] = $array_fillter['status'];
            }
            if ($array_fillter['position'] == 'hot' || $array_fillter['position'] == 'feature') {

                $this->model->where[$array_fillter['position']] = 1;
            }
            if ($array_fillter['hot_position']) {

                $this->model->where['hot_position'] = intval($array_fillter['hot_position']);
                $this->model->where['hot'] = 1;
            }
            if ($array_fillter['admin']) {

                $this->model->where['admin_id'] = intval($array_fillter['admin']);
                //$this->model->where['hot']=1;
            }
        }
        //return $this->index();
    }

    function link_search($callback = "link_callback") {
        if ($this->input->get('key')) {
            $this->search($_GET);
        }
        $input = $this->input->get();
        if ($this->model->fields[$this->model->status_field]) {
            if (!$input['trash']) {
                $this->model->where[$this->model->status_field . " !="] = 2;
            } else {
                $this->model->where[$this->model->status_field . " ="] = 2;
            }
        }
        $this->model->single_table_mode = false;
        $this->data['list_item'] = $this->model->get_list_language(100);
        $this->data['callback'] = $callback;
        $this->data['model'] = $this->model;
        $this->data['controller'] = $this;
        $this->template->build('link_search', $this->data);
    }

    public function index() {
        if ($this->input->get('search')) {
            $this->search($this->input->get('search'));
        }
        $input = $this->input->get();
        if ($input['order']) {
            $pattern = explode(" ", $input['order']);
            if ($this->model->fields[$pattern[0]]) {
                $this->model->order = $this->model->table_name . "." . $pattern[0] . " " . (in_array(strtolower($pattern[1]), array('desc', 'asc')) ? $pattern[1] : '');
            } else
            if ($this->model->fields_lang[$pattern[0]]) {
                $this->model->order = $this->model->table_name_lang . "." . $pattern[0] . " " . (in_array(strtolower($pattern[1]), array('desc', 'asc')) ? $pattern[1] : '');
            }
        } else {
            $this->model->order = $this->model->table_name . "." . $this->model->id_field . " desc";
        }
        if ($this->model->fields[$this->model->status_field]) {
            if (!$input['trash']) {
                $this->model->where[$this->model->status_field . " !="] = 0;
            } else {
                $this->model->where[$this->model->status_field . " ="] = 0;
            }
            $this->model->where["(" . $this->model->status_field . " !=0 or admin_id=" . $this->session->userdata('admin_id') . ")"] = null;
        }
        $this->model->single_table_mode = false;
        $this->model->select = "posts.*,categories.name as cat_name, users.username as u_name";
        $this->model->join[] = array('users', "posts.admin_id=users.id");
        $this->model->join[] = array('categories', "categories.id=posts.cat_id");
        $this->data['content'] = $this->model->get_page_list("{$this->uri->segment(1)}/index", 30, 3);
        foreach ($this->data['content']['pageList'] as $value) {
            $roption['row'] = $value;
            $roption['controller'] = $this;
            $roption['model'] = $this->model;
            $value->row_option = $this->load->view('row_option', $roption, true);
        }

        $this->data['controller'] = $this;
        $this->data['model'] = $this->model;
        $this->data['form_search'] = $this->load->view('form_search', $this->data, true);
//                $this->data['option']['quick_trash']=1;
//                $this->data['option']['quick_delete']=1;
        $this->template->build('list', $this->data);
    }

    function quick_status($action) {
        $return = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $this->uri->segment(1);
        $ids = $this->input->get('ids');
        $ids = explode(",", $ids);
        foreach ($ids as $index => $id) {
            if (!is_numeric($id)) {
                unset($ids[$index]);
            }
        }
        if (count($ids)) {
            $this->model->where_in[] = array($this->model->id_field, $ids);
            $this->model->update(array($this->model->status_field => $action));
            $this->model->on_data_change();
            logs($action . " posts id: " . implode(",", $ids));
        }
        redirect($return);
    }

    function addedit($id = 0) {
        $id = $this->uri->segment(3);
        $this->data['cat'] = $this->load->model('categories/Categories_model');
        $this->data['users'] = $this->load->model('user/user_model');
        // if (isset($_FILES['img']['name']) && $_FILES['img']['name'] != "") {
            // move_uploaded_file($_FILES['img']['tmp_name'], '../upload/images/images/' . time() . "_" . $_FILES['img']['name']);
            // $_POST['data']['image'] = time() . "_" . $_FILES['img']['name'];
        // }
		
        $tags=$this->load->model('tags/tags_model');
        $this->data['tags']=$tags->get_tag_by_object($_POST['data']['id']);
        $toptags=$tags->get_top_tag();
        $s=array();
        foreach ($toptags as $t) {
            $s[]="'".  str_replace("'","\\'",$t->name)."'";
        }
        $this->data['toptags']=  implode(",", $s);
        $data_post = $this->input->post();
		// print_r($data_post);exit;
        if($data_post){
            $tags=$this->load->model('tags/tags_model');
            $tags->insert_tags_for_object($_POST['tags'], $id);
            //print_r($_POST['tags']);exit;
			$_POST['data']['admin_id'] = $_SESSION['admin_id'];
			$_POST['data']['public_date'] = date('Y-m-d H:i');
			if(!$_POST['data']['status']){
				$_POST['data']['status']=0;
			}
            $id = $this->model->store_data($this->input->post("data"));
            if ($id) {
                
                $this->on_after_addedit_process($id);
            }
            
            redirect("posts", 'refresh');
        }
        
        parent::addedit($id);
    }
	
	function delete_image(){
		if($_POST['image'] == 1){
			$data = array(
				'image' => '',
			);
			$this->model->getDB()->where('id', $_POST['id']);
			$this->model->getDB()->update('posts', $data); 
		}
	}
	
    function on_after_addedit_process($pid) {
        
        $result = array();
       
        $date = date('Y-m-d',time());
        $admin_id = $this->session->userdata('admin_id');
        $data = array(
            'admin_id' => $admin_id,
            'public_date' => $date,
        );
        $this->model->where('id',$pid);
        $this->model->update($data);
        return array_merge($result, (array) parent::on_after_addedit_process($id));
    }

    public function quick_trash() {
        $return = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $this->uri->segment(1);
        $ids = $this->input->get('ids');
//        $ids = explode(",", $ids);
//        foreach ($ids as $index => $id) {
//            if (!is_numeric($id)) {
//                unset($ids[$index]);
//            }
//        }
        $this->model->delete_id($ids);
        $this->model->on_data_change();
        if ($ids)
            logs("Xóa: " . implode(",", $ids));
        redirect($return);
    }

    function refuse_post($id) {
        if (!check_permision("posts_refuse"))
            return die();
        $this->model->where(array('id' => $id));
        $obj = current($this->model->get()->result());
        if ($obj->id && $obj->status == 'pending') {
            $this->model->getDB()->update($this->model->table_name, array('status' => 'draft', 'is_refuse' => 1, 'refuse_by' => $this->session->userdata('admin_id')), array('id' => $id));
            logs("Trả bài: " . $obj->title);
        }
        redirect(base_url() . $this->uri->segment(1));
    }

    function topAuthor($type) {
        // echo 123123;die;
        $this->data['top'] = $this->model->getTopAuthor($type);
        $this->data['type'] = $type;
        if ($type == 'count')
            $this->data['type'] = 'bài';

        $this->load->view('topAuthor', $this->data);
    }

}

?>
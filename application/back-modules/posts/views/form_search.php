<div class="row wrapper">
    <div class="col-sm-12">
        <style>
            .frm-posts-search>div{
                margin:1px 0px;
                padding:0px;
            }

        </style>
        <form class="col-sm-12 frm-posts-search" action="<?php echo base_url() . $this->uri->segment(1) . "/index"; ?>">
            <?php if ($_GET['trash']) { ?><input type="hidden" name="trash" value="1"/><?php } ?>
            <div class="col-sm-3">
                <div class="">
                    <input type="text" style="width:100%;" placeholder="Từ khóa" value="<?php echo $_GET['search']['key']; ?>" name="search[key]" class="input-sm form-control">
                </div>
            </div>
            <?php
            $users = $this->load->model("user/user_model");
            $ulist = $users->get_list();
            ?>
            <div class="col-sm-3">
                <div class="input-group">
                    <span class="input-group-addon">Tìm theo tác giả</span>
                    <select  name="search[admin]" class='input-s-sm input-s select-input form-control'>
                        <option <?php if (!$_GET['search']['admin']) echo "selected=selected"; ?> value="">Tất cả</option>
                        <?php foreach ($ulist as $user) { ?>
                            <option <?php if ($_GET['search']['admin'] == $user->id) echo "selected=selected"; ?> value="<?php echo $user->id; ?>"><?php echo $user->name . "({$user->username})"; ?></option>
                        <?php } ?>
                    </select>

                </div>
            </div>
            <div class="col-sm-3">
                <div class="input-group">
                    <span class="input-group-btn ">
                        <input type="submit" class="btn btn-primary" value="Tìm"/>
                    </span>
                </div>
            </div>

        </form>
    </div>
</div>
<!--
              <form action="<?php echo base_url() . $this->uri->segment(1) . "/index"; ?>">
              <div class="search fr" style="">
                  <input type="text" placeholder="Nhập từ khóa" name="search[key]" value="<?php echo $_GET['search']['key']; ?>" /> 
<?php if ($model->fields['create_date']) { ?>
                              <input type="text" placeholder="Từ ngày" name="search[from]" class="short yt-datetime" value="<?php echo $_GET['search']['from']; ?>"/> 
                              <input type="text" placeholder="Đến ngày" name="search[to]" class="short yt-datetime" value="<?php echo $_GET['search']['to']; ?>"/> 
                              <script>
                                  $(".yt-datetime").datepicker({dateFormat:'dd/mm/yy'});
                              </script>
<?php } ?>
                  <input type="submit" value="" class="bntS" />
              </div> 
              </form>
-->          
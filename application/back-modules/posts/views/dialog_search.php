<?php
//base_url();
//$this->uri->segment(1);
$returnurl = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : base_url() . $this->uri->segment(1);
$id_field = $model->id_field;
?>
<?php if (!isset($_REQUEST['search'])) { ?>
    <div>
        <div id="search-panel">
            <form action="<?php echo $this->uri->segment(1); ?>/dialog_search/<?php echo $callback; ?>" onsubmit="ajax_submit_form(this, 'search-results');
                        return false;">
                <div class="link-search-wrapper">
                    <label>
                        <span class="search-label">Tìm</span>

                        <input type="search" id="search-field" name="key" class="link-search-field" autocomplete="off">
                        <input type="submit" value="Tìm"/>
                        <input type="hidden" name="search" value="1"/> 
                        <span class="spinner"></span>
                    </label>
                </div>
            </form>

            <div id="search-results" class="query-results">
            <?php } ?>
            <ul>
                <?php
                foreach ($list_item as $item) {
                    $name = $item->title_short ? $item->title_short : $item->title;
                    $str = str_replace( array('"',"'") , '', $item->title );
                    ?>
                    <li class="alternate">
                        <input type="hidden" class="item-permalink" value="http://tuyenbui.com/chuyen-mon-cong-nghe-thong-tin/seo-sem-marketing-online/tang-luong-truy-cap-cho-trang-web-62">
                        <span class="item-title"><?php echo cut_string($name, 35); ?></span>
                        <!--<span class="item-info"><?php echo $item->create_date ? date('H:i d/m/Y', strtotime($item->create_date)) : ""; ?></span>-->
                        <input class="select_btn" type="button" onclick="<?php echo $callback ?>(<?php echo $item->$id_field, ",'" . $str . "'"; ?>)" value="Chọn"/>
                    </li>
                <?php }
                ?>
                <?php
                if (!$list_item)
                    echo "<p>Không tìm thấy kết quả nào!</p>";
                ?>
            </ul>
            <div class="river-waiting" style="display: none;">
                <span class="spinner"></span>
            </div>
            <?php if (!isset($_REQUEST['search'])) { ?>
            </div>

        </div>

    </div>
<?php } ?>
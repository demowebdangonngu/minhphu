<table class="table-au" border="1" >
   
    
    <?php foreach($top as $key=>$value){ ?>
    <tr>
        <td width="50%" ><?php if($value->avatar){ ?><img src="<?php echo $this->config->item('img_path'); ?>30x30<?php echo $value->avatar; ?>"><?php } ?><?php echo $value->name; ?></td>
       
        <td style="text-align:right;"><b><?php echo number_format($value->count); ?></b> <?php echo $type; ?></td>
    </tr>
    <?php } ?>
    
</table>

<style>
    .table-au td{
        padding: 5px 20px;
    }
    .table-au td img{
        padding-right:   5px;
    }
    .table-au{
        width:  100%;
    }
    #top-author-ajax{
        overflow:auto;
    }
    
</style>
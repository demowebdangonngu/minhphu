<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class User_model extends YT_AModel {
	public function __construct(){  
		parent::__construct();  
//id 	username 	email 	password 	salt 	gid 	name 	lastlogin 	ip_connection 	joindate 	avatar 	slug 	permit 	publish

                $this->fields=array (
  'id' => 'PRI',
  'username' => 1,
  'email' => 1,
  'password' => 1,
  'salt' => 1,
  'gid' => 1,
  'name' => 1,
  'lastlogin' => 1,
  'ip_connection' => 1,
  'avatar' => 1,
  'slug' => 1,
  'publish' => 1,
   'avatar' => 1,
   'type' => 1,
   'phone' => 1,
  'create_date' => 1,
  'modify_date' => 1,
  'yahoo' => 1,
  'skype'=>1,
);
         $this->fields_lang=array();
         $this->table_name='users';
         $this->table_name_lang='';
         $this->id_field='id';
         $this->id_field_lang='conlg_id';
         $this->status_field='publish';
         $this->cat_field='';
         $this->order_field='';
         $this->image_field=array('avatar'=>'1');
         $this->id_lang_rel=$this->id_field;
	}
	//public $table_name = "users";
	
	
//	public function get_list($limit, $start,$where=" where 1=1 ", $order=" order by id desc"){   
//		$sql			= "select * from ".$this->table_name." where 1=1 ".$where.$order." limit ".$start.",".$limit;//print $sql;
//		$query			= $this->db_slave->query($sql);		
//		return $query->result_array();
//	}
	public function check_name($data){
		$this->db_slave->select('id');
		$this->db_slave->like('username', $data, 'none'); 
		$query 			= $this->db_slave->get($this->table_name);	
		return $query->num_rows();
	}
	public function insert($data)	{
		$kq				= $this->getDB()->insert($this->table_name, $data); 	
		return $kq== TRUE?$this->getDB()->insert_id():0;
		
	}
	public function edit($id)	{
		$this->db_slave->select('*');
		$this->db_slave->where('id',$id);		
		$query 			= $this->db_slave->get($this->table_name);			
		if($query->num_rows()>0){
			$data 		= $query->row_array();				
			return $data;
		}
		return 0;
	}
	/*
	public function update($id,$data)	{    
	
	$sql				= "select id from ".$this->table_name." where username = '".$data['username']."' and id <> '".$id."' limit 0,1";
	$query				= $this->getDB()->query($sql);
	$check				= $query->num_rows();
		if($check 		== TRUE)	return 4;			
		else{
			$this->getDB()->where('id', $id);
			$kq	 		=  $this->getDB()->update($this->table_name, $data); 
			return $kq 	== TRUE ?1:2;			
		}
	} 
	*/
	public function delete($id)	{
		$this->getDB()->where('id', $id);
		return $this->getDB()->delete($this->table_name); 			
	}
	
	public function publish($id, $publish)	{
		$data['publish'] 	= ($publish=='publish') ? 0 : 1;
		$data['id'] 		= $id;			
		$this->getDB()->where('id', $id);
		return $this->getDB()->update($this->table_name, $data); 			
	}
	
	public function delete_all($items){
		foreach($items as $value){
			$this->getDB()->where('id', $value);
			$this->getDB()->delete($this->table_name);
		}
		return true;			
	}
	public function field_all($items,$data){
		foreach($items as $value){
			$this->getDB()->where('id', $value);
			$this->getDB()->update($this->table_name,$data);
		}
		return true;			
	}
	public function getStrLog($id)	{
		$description		= "";
		$this->db_slave->select('*');
		$this->db_slave->where('id',$id);		
		$query 				= $this->db_slave->get($this->table_name);			
		if($query->num_rows()>0){
			$row 			= $query->row_array();	
			$publish		= $row['publish']==1?"publish":"unpublish";	
			$description 	= '
			Tên : ' . $row['username'] . ' <br>id = '.$row['id']. ' <br>status = '.$publish;		
			return $description;
		}
		return $description;
	}
	public function check_login($username,$password){
		$sql	= 'Select * From users Where username = "'.addslashes($username) .'" And publish = 1';
		$query 	= $this->getDB()->query($sql);
		$row 	= $query->row_array();
		return $row;		
	}
	
	public function add_loglogin($data = array()){
		//update account
		$ip_connection= $_SERVER['REMOTE_ADDR'];
		if(@$data['id']>0){
			$update		= array('ip_connection'=> "".$ip_connection."");	
			$this->getDB()->where('id', $data['id']);
			$this->getDB()->update('users',$update); 
		}
		
		// save log login
		$insert	= array('user_name'=> "".$data['username']."",'user_id'=> "".$data['id']."");	
		$res 	= $this->getDB()->insert('user_loglogin', $insert); 
		return $res ? true : false;
	}
	public function get_group(){
		$sql	= 'Select * From user_group Where 1 And publish = 1';
		$query 	= $this->getDB()->query($sql);
		$row 	= $query->result_array();
		return $row;
	}
        function load_permision($user_id=0){
            $this->join("users_role_rel","users_role_rel.user_id=users.id");
            $this->join("users_role","users_role_rel.role_id=users_role.id");
            $this->where(array("users_role_rel.user_id"=>$user_id));
            $list=$this->get()->result();
            $permision=array();
            foreach ($list as $v) {
                $pl=explode(",", $v->permision);
                foreach ($pl as $p) {
                    if($p)
                    $permision[$p]=$p;
                }
                
            }
            return $permision;  
                    
        }
        function get_role_list($user_id=0){
            $this->join("users_role_rel","users_role_rel.user_id=users.id");
            $this->join("users_role","users_role_rel.role_id=users_role.id");
            $this->where(array("users_role_rel.user_id"=>$user_id));
            return $this->get()->result();
        }
        function get_role_id_list($user_id=0){
            $ids=array();
            $list=$this->get_role_list($user_id);
            foreach($list as $role){
                $ids[$role->id]=$role->id;
            }
            return $ids;
        }
	
	//$sql = $this->db->last_query();	
}
?>
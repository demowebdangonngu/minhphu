<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/ControllerAdmin.php";

class User extends YT_ControllerAdmin {

    function __construct() {
        parent::__construct();
        $this->module_name = $this->uri->segment(1);
        $this->table_name = "users";

        $this->config->load('define', TRUE);
        $this->load->library('adminclass_model');
        $this->model = $this->load->model('user/User_model', 'ACT', TRUE);
    }

    function addedit($id = 0) {
        if (!$_POST['data']['password'])
            unset($_POST['data']['password']);
        else {
            $salt = substr(md5(rand(0, 9999999)), 0, 5);
            $pass = trim($_POST['data']['password']);
            $_POST['data']['password'] = md5(md5($pass) . $salt);
            $_POST['data']['salt'] = $salt; //echo $salt;
        }
        if (isset($_FILES['img']['name']) && $_FILES['img']['name'] != "") {
            move_uploaded_file($_FILES['img']['tmp_name'], '../upload/images/images/' . time() . "_" . $_FILES['img']['name']);
            $_POST['data']['avatar'] = time() . "_" . $_FILES['img']['name'];
        }
        $data_post = $this->input->post(); 
        if($data_post){ 
            $id = $this->model->store_data($this->input->post("data"));
            if ($id) {
                $this->on_after_addedit_process($id);
            }
            redirect("user", 'refresh');
        }
        parent::addedit($id);
    }

    function addedit_process() {
        if (!$_POST['data']['password'])
            unset($_POST['data']['password']);
        else {
            $salt = substr(md5(rand(0, 9999999)), 0, 5);
            $pass = trim($_POST['data']['password']);
            $_POST['data']['password'] = md5(md5($pass) . $salt);
            $_POST['data']['salt'] = $salt; //echo $salt;
        }

        if (isset($_FILES['img']['name']) && $_FILES['img']['name'] != "") {
            move_uploaded_file($_FILES['img']['tmp_name'], '../upload/images/images/' . time() . "_" . $_FILES['img']['name']);
            $_POST['data']['avatar'] = time() . "_" . $_FILES['img']['name'];
        }
        logs("Add/edit user: " . $_POST['data']['name']);
        parent::addedit_process();
		
    }

    public function index() {
        // if (!check_permision("users_access")) {
            // redirect(base_url() . "user/changepassword");
        // }
        return parent::index();
    }

    function changepassword() {
        $this->model->where(array('id' => $this->session->userdata('admin_id')));
        $this->data['obj'] = current($this->model->get()->result());
        $this->data['model'] = $this->model;

        $this->template->build("changepassword", $this->data);
    }

    function changepassword_process() {
        $array_allow = array("name" => 1,
            "email" => 1,
            "password" => 1,
            "phone" => 1,
            "avatar" => 1);
        foreach ($array_allow as $index => $value) {
            if ($_POST['data'][$index])
                $array_allow[$index] = $_POST['data'][$index];
            else
                unset($array_allow[$index]);
        }
        if ($array_allow['password']) {
            if (strlen($array_allow['password']) < 6) {
                $this->show_error("Mật khẩu quá ngắn!");
            }
            $salt = substr(md5(rand(0, 9999999)), 0, 5);
            $pass = trim($array_allow['password']);
            $array_allow['password'] = md5(md5($pass) . $salt);
            $array_allow['salt'] = $salt; //echo $salt;
        }
        $array_allow['id'] = $this->session->userdata('admin_id');
        if ($array_allow['name'] && strlen($array_allow['name']) < 4) {
            $this->show_error("Tên quá ngắn!");
        }
        $this->model->store_data($array_allow);
        logs("Đổi mật khẩu");
        echo json_encode(array('status' => 0, 'id' => $array_allow['id']));
        die;
    }

    private function show_error($message) {
        $result = array('status' => 0, 'message' => $message);
        echo json_encode($result);
        die;
    }

    public function login() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $row = $this->ACT->check_login($_POST['username'], $_POST['password']);
            //echo md5(sha1(addslashes($_POST['password'])));exit;
            if ($row) {

//					if($row['partners_id']	==	'1'){	
                if (md5(md5(trim($_POST['password'])) . $row['salt']) == $row['password']) {
                    $permision = $this->ACT->load_permision($row['id']);
                    $roles = $this->ACT->get_role_id_list($row['id']);
                    $userdata = array('user' => $row['username'],
                        'admin_id' => $row['id'],
                        'admin_user' => $row['username'],
                        'admin_name' => $row['name'],
                        'admin_email' => $row['email'],
                        'admin_group_id' => $row['gid'],
                        // 'roles' => $roles,
                        'last_ip' => $_SERVER['REMOTE_ADDR'],
                        'permision' => $permision,
                        'isLoggedIn' => TRUE);
                    $this->session->set_userdata($userdata);
                    logs("Login");
                    $this->add_login($row);
                    redirect(PAGE_DEFAULT_ADMIN, 'refresh');
                } else {//password						
                    $this->session->set_flashdata('error_admin', LANG_USER_INCORRECT);
                    redirect(PAGE_DEFAULT_LOGIN, 'refresh');
                }
//					}else{//partners_id
//						$this->session->set_flashdata('error_admin',LANG_USER_INCORRECT.'yy');			
//							redirect(PAGE_DEFAULT_LOGIN, 'refresh');
//					}
            } else {//row
                $this->session->set_flashdata('error_admin', LANG_USER_INCORRECT);
                redirect(PAGE_DEFAULT_LOGIN, 'refresh');
            }
        } else { //REQUEST_METHOD
            if ($this->session->userdata('isLoggedIn')) {
                redirect(PAGE_DEFAULT_ADMIN, 'refresh');
            }

            $data["title"] = "Administrator Login";
            $this->template->build('user_login', $data);
        }
    }

    public function logout() {
        logs("Logout");
        $userdata = array('user' => '',
            'admin_id' => '',
            'admin_user' => '',
            'admin_name' => '',
            'admin_email' => '',
            'admin_group_id' => '',
            'last_ip' => '',
            'isLoggedIn' => FALSE);
        $this->session->unset_userdata($userdata);

        redirect('user/login', 'refresh');
    }

    public function add_login($data = array()) {
        $res = $this->ACT->add_loglogin($data);
        //$res 	= User_model::add_loglogin($data);
    }

    function on_after_addedit_process($id) {
        // $roles = $this->load->model("roles/roles_model");
        // if (isset($_POST['roles'])) {
            // $vids = array();
            // if ($_POST['roles']) {
                // foreach ($_POST['roles'] as $vid) {
                    // if ($vid) {
                        // $roles->insert_or_dup_update("users_role_rel", array('user_id' => $id, 'role_id' => $vid));

                        // $vids[] = $vid;
                    // }
                // }
            // }


            // if ($vids) {
                // $roles->getDB()->delete("users_role_rel", "user_id ='$id' and role_id not in (" . implode(",", $vids) . ")");
            // }
        // }
		
        parent::on_after_addedit_process($id);
    }

}

?>
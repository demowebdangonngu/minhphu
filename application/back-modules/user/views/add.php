﻿<script src="<?php echo $this->config->item('admin_url');?>themes/admincp/js/admin_js/script_admin.js"></script> 
<div class="container-fluid">
	<ul class="breadcrumb">
		<li><a href="javascript:void(0)">Home</a><span class="divider">&raquo;</span></li>
		<li><a href="<?php echo site_url('user')?>">User</a><span class="divider">&raquo;</span></li>
		<li class="active">New User</li>
	</ul>
	<div class="row-fluid">
		<div class="span12">
			<div class="nonboxy-widget">
				<div class="widget-head">
					<h5><i class="black-icons blocks_images"></i> Form New User</h5>
				</div>
                <div id="error_admin" class="error" style="color:red;"><?php echo @$this->session->flashdata('error_admin');?>c</div>
				<div class="widget-content">
					<div class="widget-box">
						<div class="form-horizontal well">
                        <form id="myform" name="myform" action="<?php echo $mod_form;?>"  enctype="multipart/form-data" method="post">
							<fieldset>
								<div class="control-group">
									<label class="control-label">User Name </label>
									<div class="controls">                                    	
                                    	<input id="txtName" name="username" type="text" class="span6" value="<?php echo @$username;?>"/>
									</div>
								</div>
                                 <div class="control-group">
									<label class="control-label">Password</label>
									<div class="controls">
                                    	<input type="password" placeholder="Password" class="login-input user-pass" id="password" name="password" value="<?php echo @$password?>">
									</div>
								</div>
                                <div class="control-group">
									<label class="control-label">Full Name </label>
									<div class="controls">                                    	
                                    	<input id="txtFullName" name="fullname" type="text" class="span6" value="<?php echo @$fullname;?>"/>
									</div>
								</div>
                               	<div class="control-group">
									<label class="control-label">Group Name</label>
									<div class="controls">
                                    <input type="hidden" value="<?php echo @$id;?>" name="id"/>
										<select name="user_group_id" id="user_group_id">
                                        <?php if (empty($GroupUser)) {
									} else {
										foreach ($GroupUser as $row) {
											$id 	= $row['id'];
											$name 	= $row['name'];
											?>
					<option value="<?php echo $id; ?>" <?php if (@$user_group_id == $id) echo "selected='selected'" ?>><?php echo $name; ?></option>							
											<?php
										}
									}
									?> 
                                        </select>
									</div>
								</div>	
                                
                                 <div class="control-group">
									<label class="control-label">Email</label>
									<div class="controls">
                                    	<input id="email" name="email" type="text" class="span6" value="<?php echo @$email;?>"/>
									</div>
								</div>
								<div class="control-group">
									<label class="control-label">Publish </label>
									<div class="controls">
										<label class="radio inline">
										<input class="radio-b" type="radio" checked="" value="1" name="publish" <?php echo (@$publish==1) ? "checked='checked'" : "";?>>
										Publish</label>
										<label class="radio inline">
										<input class="radio-b" type="radio" value="0" name="publish" <?php echo (@$publish==0) ? "checked='checked'" : "";?>>
										UnPublish</label>
									</div>
								</div>	
                                
							</fieldset>
							<div class="form-actions">
								<button class="btn btn-primary" onclick="document.myform.submit();">Lưu lại</button>
                                
							</div>
                            </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>		
</div>	

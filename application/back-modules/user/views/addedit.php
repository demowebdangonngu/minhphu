<script>
   var module = '<?php echo $this->uri->segment(1);  ?>';
   var base_url ='<?php echo base_url();  ?>';
   var img_path = '<?php echo $this->config->item('img_path')  ?>';
   <?php 
   $returnurl=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:base_url().$this->uri->segment(1); 
   $id_field=$model->id_field;
   $order_field=$model->order_field;
   $status_field=$model->status_field;
   ?>
</script>
<script type="text/javascript">
    $(document).ready(function (e) {
        $('.aut_img').each(function (index, element) {
            $(this).error(function () {
                $(this).hide();
            });
        });
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result).show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="<?php echo base_url();  ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url().$this->uri->segment(1);  ?>"><?php if($module_title){echo $module_title;}else{echo $this->uri->segment(1);};?></a> </li>
                <li class="active">Thêm / Sửa user</li>
              </ul>
              <div class="row">
                <div class="col-sm-12">
                    <form id="form" name="form" method="post" action="" class="form-horizontal" enctype="multipart/form-data">
                        <input type="hidden" name="data[gid]" value="1"/>
                        <input  name="data[<?php echo $id_field; ?>]" type="hidden" id="txt_primary_id" value="<?php echo $obj->$id_field;?>"/>
                  <section class="panel panel-default">
                    <header class="control-fixed panel-heading font-bold" data-top="49" >
                        <label style="line-height: 33px;">Thêm / Sửa nội dung</label>
                        <label class="pull-right">
                              <input type="submit" class="btn btn-primary" value="Lưu lại"/>
                              <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');return false;">Đóng</button>
                            </label>
                    </header>
                    <div class="panel-body">
                        <div id="post_form" class="col-sm-8 left">
                            <style>
                                
                                .panel-heading.control-fixed {
                                    height: 43px;
                                    padding: 4px 17px;
                                  }
                            </style>
                            <script>
                                $(".scrollable").scroll(function(){
                                var elem = $('.control-fixed');//alert("sdfsdf");
                                if (!elem.attr('data-top')) {
                                    if (elem.hasClass('navbar-fixed-top'))
                                        return;
                                     var offset = elem.offset()
                                    elem.attr('data-top', offset.top);
                                }
                                if (elem.attr('data-top') <= $(this).scrollTop() )
                                    elem.addClass('navbar-fixed-top');
                                else
                                    elem.removeClass('navbar-fixed-top');
                                });
                            
                            </script>
                            <label id="msg_error_msg" style="color:#f00"></label>
                        <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Tên</label>
                            <div class="col-sm-10">
                              <input  required minlength="3" type="text" name="data[name]" class="form-control" value="<?php echo $obj->name;?>">
                            </div>
                          </div>
                            <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Tên đăng nhập</label>
                            <div class="col-sm-10">
                              <input  required minlength="3" type="text" name="data[username]" class="form-control" value="<?php echo $obj->username;?>">
                            </div>
                          </div>
                      <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Mật khẩu</label>
                            <div class="col-sm-10">
                                <input placeholder="" type="text" name="data[password]" class="form-control" value="">
                            </div>
                          </div>
                          <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input required minlength="3" type="text" name="data[email]" class="form-control" value="<?php echo $obj->email;?>">
                            </div>
                          </div>  
                          <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-10">
                                <input required minlength="3" type="text" name="data[phone]" class="form-control" value="<?php echo $obj->phone;?>">
                            </div>
                          </div>
                          <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Name yahoo</label>
                            <div class="col-sm-10">
                                <input required minlength="3" type="text" name="data[yahoo]" class="form-control" value="<?php echo $obj->yahoo;?>">
                            </div>
                          </div>
                          <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Name skype</label>
                            <div class="col-sm-10">
                                <input required minlength="3" type="text" name="data[skype]" class="form-control" value="<?php echo $obj->skype;?>">
                            </div>
                          </div>
                          <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Loại user</label>
                            <div class="col-sm-10">
                                <select name="data[type]" id="menus_type" class="form-control">
                                    <option value="1" <?php
                                    if ($obj->type == '1') {
                                        echo "selected";
                                    };
                                    ?>>Admin</option>
                                    <option value="2" <?php
                                    if ($obj->type == '2') {
                                        echo "selected";
                                    };
                                    ?>>Kinh doanh</option>
                                    <option value="3" <?php
                                    if ($obj->type == '3') {
                                        echo "selected";
                                    };
                                    ?>>Kỹ thuật</option>
                                </select>
                            </div>
                          </div>  
                          <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Avatar</label>
                            <div class="col-sm-10">
                                <a title="<div>Kích thước chuẩn:<div> <div>960x800 - .jpg .png .gif</div>Dung lượng tối đa: 2Mb ">
                                    <input type="file" onChange="readURL(this);" name="img" id="img" > <br />
                                </a>
                                <?php
                                if ($obj->avatar) {
                                    ?><label for="hinhanh">Hình Cũ</label>
                                    <img src="<?php echo $this->config->item("img_path") . $obj->avatar; ?>" width="100%" height="100%" />
                                    <br>
                                    <label for="hinhanh">Hình Mới</label>
                                    <?php
                                }
                                ?><img class="aut_img" id='blah' src='' style=" max-width: 100%; max-height: 100%;" />
                            </div>
                          </div>  
                        </div>
                        
                        <div class="col-sm-4">
                             <div class="form-group">
                                <label class="col-sm-6 control-label">Trạng thái</label>
                                <div class="col-sm-6">
                                  <label class="switch">
                                      <input name="data[<?php echo $status_field;  ?>]"  value="1" <?php if($obj->$status_field==1) echo 'checked=checked';?>  type="checkbox">
                                    <span></span>
                                  </label>
                                </div>
                              </div>

                        </div>
                        
                        <div id="" class="col-sm-12">
                            <div class="line line-dashed line-lg pull-in"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                               <input type="submit" class="btn btn-primary" value="Lưu lại"/>
                              <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');return false;">Đóng</button>
                            </div>
                          </div>
                      </div>
                        </div>
                      
                    </div>
                  </section>
                        </form>
                </div>
                
              </div>
              
            </section>
          </section>
          <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
        </section>

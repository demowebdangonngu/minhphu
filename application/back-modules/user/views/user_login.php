<!DOCTYPE html>
<html lang="en" class="bg-dark">
<head>
  <meta charset="utf-8" />
  <title>Đăng nhập</title>
  <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" /> 
  <link rel="stylesheet" href="<?php echo $this->config->item('static_path');?>themes/admincp/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->config->item('static_path');?>themes/admincp/css/animate.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->config->item('static_path');?>themes/admincp/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->config->item('static_path');?>themes/admincp/css/font.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->config->item('static_path');?>themes/admincp/css/app.css" type="text/css" />
  <!--[if lt IE 9]>
    <script src="<?php echo $this->config->item('static_path');?>themes/admincp/js/ie/html5shiv.js"></script>
    <script src="<?php echo $this->config->item('static_path');?>themes/admincp/js/ie/respond.min.js"></script>
    <script src="<?php echo $this->config->item('static_path');?>themes/admincp/js/ie/excanvas.js"></script>
  <![endif]-->
</head>
<body class="">
  <section id="content" class="m-t-lg wrapper-md animated fadeInUp">    
    <div class="container aside-xxl">
      <section class="panel panel-default bg-white m-t-lg">
            <header class="panel-heading text-center">
              <strong>Đăng nhập</strong>
            </header>
                        <form class="panel-body wrapper-lg" method="post" action="<?php echo site_url("user/login");?>">
                            <div id="error_admin" class="error" style="color:red;"><?php $error = $this->session->all_userdata(); echo @$error['flash:new:error_admin']; ?></div>
              <div class="form-group">
                <label class="control-label">Tên đăng nhập</label>
                <input type="text" name="username" placeholder="" class="form-control input-lg">
              </div>
              <div class="form-group">
                <label class="control-label">Mật khẩu</label>
                <input type="password" name="password" placeholder="Password" class="form-control input-lg">
              </div>
              <button type="submit" class="btn btn-primary">Đăng nhập</button>
            </form>
    </section>
    </div>
  </section>
  <!-- footer -->
  <!-- / footer -->
  <script src="<?php echo $this->config->item('static_path');?>themes/admincp/js/jquery.min.js"></script>
  <!-- Bootstrap -->
  <script src="<?php echo $this->config->item('static_path');?>themes/admincp/js/bootstrap.js"></script>
  <!-- App -->
  <script src="<?php echo $this->config->item('static_path');?>themes/admincp/js/app.js"></script> 
  <script src="<?php echo $this->config->item('static_path');?>themes/admincp/js/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo $this->config->item('static_path');?>themes/admincp/js/app.plugin.js"></script>
</body>
</html>
<!--
 <link href="<?php echo $this->config->item('admin_url');?>themes/admincp/css/init.css" rel="stylesheet" type="text/css"  media="screen" />

<script type="text/javascript" src="<?php echo $this->config->item('admin_url');?>themes/admincp/scripts/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('admin_url');?>themes/admincp/scripts/lib/scriptbreaker-multiple.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('admin_url');?>themes/admincpscripts/lib/jquery.sticky.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('admin_url');?>themes/admincpscripts/lib/jquery.tooltipster.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('admin_url');?>themes/admincpscripts/lib/library.js"></script>

 
<div class="siteAD"><div class="site">

	
    <div class="loginPage">
    	<a href="#" class="logoLogin"></a>
       <div class="boxLogin">
       		<h2>Login</h2>
           <div class="formLogin fixed">
           <form id="form" action="<?php echo site_url("user/login");?>" method="post">
           		<div id="error_admin" class="error" style="color:red;"><?php $error = $this->session->all_userdata(); echo @$error['flash:new:error_admin']; ?></div><br />
                
           		<label class="user"></label> 
                <input type="text" placeholder="Username or Email" id="username" name="username"><br />
            	<label class="pass"></label> 
                <input type="password" placeholder="Password" id="password" name="password"><br />
              	
                <input type="submit" value="Login" /><br /> 
                
           </form> 
           </div> 
       </div> 
    </div>
    
    <div class="footerM">
    	Copyright 2012 - 2013 <span>ONASIA</span>. Allright services
    </div>

</div></div>-->
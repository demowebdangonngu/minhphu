<script>
   var module = '<?php echo $this->uri->segment(1);  ?>';
   var base_url ='<?php echo base_url();  ?>';
   var img_path = '<?php echo $this->config->item('img_path')  ?>';
   <?php 
   $return_url=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:base_url()."home"; 
   $id_field=$model->id_field;
   $order_field=$model->order_field;
   $status_field=$model->status_field;

   ?>
</script>
<section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="<?php echo base_url();  ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url().$this->uri->segment(1);  ?>"><?php if($module_title){echo $module_title;}else{echo $this->uri->segment(1);};?></a> </li>
                <li class="active">Đổi thông tin cá nhân</li>
              </ul>
              <div class="row">
                <div class="col-sm-12">
                    <form class="form-horizontal" id="frm_add_edit" method="post" action="<?php echo base_url().$this->uri->segment(1); ?>/changepassword_process/" onsubmit="submit_form($(this),'<?php echo $return_url; ?>');return false;"  >
                        <input type="hidden" name="data[gid]" value="1"/>
                        <input  name="data[<?php echo $id_field; ?>]" type="hidden" id="txt_primary_id" value="<?php echo $obj->$id_field;?>"/>
                  <section class="panel panel-default">
                    <header class="control-fixed panel-heading font-bold" data-top="49" >
                        <label style="line-height: 33px;">Đổi thông tin cá nhân</label>
                        <label class="pull-right">
                              <button class="btn btn-primary" type="button" onclick="update_form($('#frm_add_edit'));" class="bntAll">Lưu lại</button>
                              <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');return false;">Đóng</button>
                            </label>
                    </header>
                    <div class="panel-body">
                        <div id="post_form" class="col-sm-8 left">
                            <style>
                                
                                .panel-heading.control-fixed {
                                    height: 43px;
                                    padding: 4px 17px;
                                  }
                            </style>
                            <script>
                                $(".scrollable").scroll(function(){
                                var elem = $('.control-fixed');//alert("sdfsdf");
                                if (!elem.attr('data-top')) {
                                    if (elem.hasClass('navbar-fixed-top'))
                                        return;
                                     var offset = elem.offset()
                                    elem.attr('data-top', offset.top);
                                }
                                if (elem.attr('data-top') <= $(this).scrollTop() )
                                    elem.addClass('navbar-fixed-top');
                                else
                                    elem.removeClass('navbar-fixed-top');
                                });
                            
                            </script>
                            <label id="msg_error_msg" style="color:#f00"></label>
                        <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Tên</label>
                            <div class="col-sm-10">
                              <input id="txt_name"  required minlength="3" type="text" name="data[name]" class="form-control" value="<?php echo $obj->name;?>">
                            </div>
                          </div>
                            <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Tên đăng nhập</label>
                            <div class="col-sm-10">
                                <input  disabled="" type="text" name="" class="form-control" value="<?php echo $obj->username;?>">
                            </div>
                          </div>
                      <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Mật khẩu</label>
                            <div class="col-sm-10">
                                <input autocomplete="off" id="txt_password" placeholder="" type="password" name="data[password]" class="form-control" value="">
                            </div>
                          </div>
                       <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Nhập lại Mật khẩu</label>
                            <div class="col-sm-10">
                                <input autocomplete="off"  id="txt_password_repeat" placeholder="" type="password" name="data[password_repeat]" class="form-control" value="">
                            </div>
                          </div>
                          <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input required minlength="3" type="text" name="data[email]" class="form-control" value="<?php echo $obj->email;?>">
                            </div>
                          </div>  
                        </div>
                        
                        <div class="col-sm-4">
                            <div style="">
                                                <div class="form-group" style="margin-bottom: 0px;">
                                                    <label class="control-label"><strong>Hình đại diện</strong></label>
                                                    
                                                </div>
                                                <div class="form-group image-img-upload">	
                                    <!--<input width="200" type="file" class="filestyle" name="files[]" data-icon="false" data-classButton="btn btn-default" id="thumb-img-file-upload" data-classInput="form-control inline input-s" data-url="<?php echo base_url() ?>uploadfile/multi"  multiple="multiple"/>-->
                                                    <span class="btn btn-s-md btn-default fileinput-button">
                                                        <span> Chọn ảnh  760x520 </span>
                                                        <a title="<div>Kích thước chuẩn:<div> <div>128x128  - .png .gif</div>Dung lượng tối đa: 2Mb <b>Nền trong suốt</b>" >
                                                            <input id="img-file-upload" type="file" name="files[]" data-url="<?php echo base_url() ?>uploadfile/multi"/></a>
                                                    </span>
                                                    <style>
                                                        .img-editor img{
                                                            width:100%;
                                                            max-height: 180px;
                                                        }
                                                        
                                                    </style>
                                                    <div class="progress progress-sm m-t-sm">
                                                        <div class="progress-bar progress-bar-info" style="width: 0%"></div>
                                                    </div>
                                                    <div id="img-thumb-panel" class="img-thumb-panel">
                                                        <div class="col-sm-12 img-editor" style="text-align:center; margin-top: 5px;">
                                                            
                                                        <?php if ($obj->avatar) { ?>
                                                            
                                                               <img src="<?php echo $this->config->item("img_path")."wxhxt".$obj->avatar;?>"/>
                                                            
                                                        <?php } ?>
                                                        </div>
                                                        <input  class="input-file-path" type="hidden" name="data[avatar]" checked="checked" value="<?php echo $obj->image; ?>"/>
                                                    </div>
                                                    <script>
                                                        //*************upload thumb*******************************/
                                                         $(document).ready(function(e) {
                                                             $('.image-img-upload  #img-file-upload').fileupload({
                                                                 dataType: 'json',
                                                                 done: function(e, data) {
                                                                     if(data.result.status=='0'){
                                                                         alert(data.result.error);
                                                                     }else{
                                                                     $.each(data.result.files, function(index, file) {
                                                                         //countimg++;
                                                                         if($('.image-img-upload  .img-editor img').length>0)
                                                                         $('.image-img-upload   .img-editor img').attr("src",img_path+"wxhxt"+file.url);
                                                                        else $(".image-img-upload   .img-editor").append($("<img />").attr("src",img_path+"wxhxt"+file.url));
                                                                         $(".image-img-upload .input-file-path").val(file.url);
                                                                     });
                                                                     }
                                                                     $('.image-img-upload .progress .progress-bar').css('width', '0%');
                                                                     $('.image-img-upload  .progress').hide();
                                                                     

                                                                 },
                                                                 progressall: function(e, data) {
                                                                     var progress = parseInt(data.loaded / data.total * 100, 10);
                                                                     $('.image-img-upload .progress').show();
                                                                     $('.image-img-upload  .progress .progress-bar').css('width', progress + '%');
                                                                 }
                                                             });
                                                         });
                                                      </script>
                                                </div>
                                                    </div>

                        </div>
                        
                        <div id="" class="col-sm-12">
                            <div class="line line-dashed line-lg pull-in"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                               <input onclick="update_form($('#frm_add_edit'));" class="btn btn-primary" type="button" value="Lưu lại"/>
                              <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');return false;">Đóng</button>
                            </div>
                          </div>
                      </div>
                        </div>
                      
                    </div>
                  </section>
                        </form>
                </div>
                
              </div>
              
            </section>
          </section>
          <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
        </section>
<script type='text/javascript'>
    function on_form_submit(form){
        
        if($("#txt_name").val().length<4){
            alert("Mật khẩu quá ngắn (ít nhất 4 ký tự)");
             return false;
        }
        if($("#txt_password").val()!=$("#txt_password_repeat").val()){
            alert("Mật khẩu bạn nhập không giống nhau");
             return false;
        }
        
        if($("#txt_password").val()!=""&&$("#txt_password").val().length<6){
            alert("Mật khẩu quá ngắn (ít nhất 6 ký tự)");
             return false;
        }
    };
	//CKEDITOR.replace('content_here');
        
        /**global javascript**/
        $(function() {
    
            $( ".input-sortable" ).sortable({
              placeholder: "ui-state-highlight"
            });

          });

</script>		
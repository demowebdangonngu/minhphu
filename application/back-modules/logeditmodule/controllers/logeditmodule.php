<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Logeditmodule extends MX_Controller {
	
	function __construct()	{
		parent::__construct();			
		$this->load->model('logeditmodule/logeditmodule_model','ACT',TRUE);		
		$this->module_name		= $this->uri->segment(1);
		$this->table_name 		= "logeditmodule";	
		
		$this->config->load('define',TRUE);
		$this->load->library('adminclass_model');
		//kiem tra permission 		
		$user_group_id		= $this->session->userdata('admin_group_id');//1;		
		$this->adminclass_model->permission_login($user_group_id,$this->module_name);
	}	
	public function index(){
					
		//pagin	
		$base_url 				= base_url().'logeditmodule/index';        
		$per_page				= PER_PAGE;		
		$page 					= ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) * $per_page :0;		
		if($this->session->userdata('Status')=='' )$this->session->set_userdata('Status','All');
		//search form
		$where					= "  ";
		$where1					= " ";$order	= " order by id desc";
		if(isset($_POST['submit_form'])) {
			$this->session->set_userdata("name",addslashes($this->input->post('name', TRUE))) ;
			$this->session->set_userdata("user_id",($this->input->post('user_id', TRUE))) ;
			$this->session->set_userdata("idmod",($this->input->post('idmod', TRUE))) ;
			$this->session->set_userdata("action",($this->input->post('action', TRUE))) ;
			$this->session->set_userdata("create_date",($this->input->post('create_date', TRUE))) ;
		}
		if($this->session->userdata("create_date")){
			$from				= $this->session->userdata("create_date");
			$to					= $this->session->userdata("create_date")." 23:59:59 ";
			$where 				.= " and d.create_date between '".$from."' and '".$to."'";	
			$where1				.= " and d.create_date between '".$from."' and '".$to."'";			
		}
		if($this->session->userdata("user_id")){
			$where 				.= " and d.id_user = '".$this->session->userdata("user_id")."'";	
			$where1				.= " and d.id_user = '".$this->session->userdata("user_id")."'";			
		}
		if($this->session->userdata("idmod")){
			$where 				.= " and d.idmod = '".$this->session->userdata("idmod")."'";	
			$where1				.= " and d.idmod = '".$this->session->userdata("idmod")."'";			
		}
		if($this->session->userdata("action")){
			$where 				.= " and d.action = '".$this->session->userdata("action")."'";	
			$where1				.= " and d.action = '".$this->session->userdata("action")."'";			
		}
		
		if($this->session->userdata("name")){
			$where 				.= " and d.description like '%".$this->session->userdata("name")."%'";	
			$where1				.= " and d.description like '%".$this->session->userdata("name")."%'";			
		}
		
		$select					= " d . * , c.name, u.username ";
		$total_rows 			= $this->adminclass_model->count_Items($where,$this->table_name." d LEFT JOIN modules c ON d.`idmod` = c.id
LEFT JOIN users u ON d.`id_user` = u.id "," count(d.id) " );			
		$data['Modules']		= $this->ACT->get_modules();
		$data['Users']			= $this->ACT->get_users();
		$data['Actions']		= $this->get_action();
		$data["Rows"] 			= $this->adminclass_model->get_list($per_page, $page,$where,$order,$this->table_name." d LEFT JOIN modules c ON d.`idmod` = c.id
LEFT JOIN users u ON d.`id_user` = u.id ",$select);	
		$data['pagination'] 	= $this->adminclass_model->loadIndex($base_url,$total_rows,"");		
		$this->template->build('list', $data);
	}
	

	
	function get_action(){
		$data					= array(array("id"=>"add","name"=>"add"),
										array("id"=>"edit","name"=>"edit"),
										array("id"=>"update","name"=>"update"),
										array("id"=>"publish","name"=>"publish"),
										array("id"=>"delete","name"=>"delete"),
										array("id"=>"delete_all","name"=>"delete_all"),
										array("id"=>"publish_all","name"=>"publish_all"),
										array("id"=>"hot_all","name"=>"hot_all"));
		return $data;
	}
}
 ?>
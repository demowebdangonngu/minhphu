﻿<div class="container-fluid">
    <div class="toolbar-action">
		<div class="toolbar-list" id="toolbar">
				
		</div>
		<div class="pagetitle icon-48-article"><h4>Log Edit Module Manager</h4></div>
    </div>
     <div id="error_admin" class="error" style="color:red;"><?php echo @$this->session->flashdata('error_admin');?></div>
     <br/>
     <div id="form_div">
     	<form name="myform_search" method="post" action="<?php echo base_url();?>logeditmodule/index" enctype="multipart/form-data" >
     	<input type="text" placeholder="Name" id="search_logeditmodule" name="name" value="<?php echo $this->session->userdata('name')?>" >
        <input type="text" placeholder="date" id="create_date" name="create_date" value="<?php echo $this->session->userdata('create_date')?>" >
          <select name="user_id">
                           <option value="0">-All-</option>
                         <?php if( empty($Users) ) { 
							  ?>
                              <option value="0">-All-</option>
                              <?php } else {
								 
								  foreach($Users as $row) {
									   
									  $id = $row['id'];									 														
									  $name = stripslashes($row['username']);							   	  
						  ?>
							<option value="<?php echo $id;?>" <?php if($this->session->userdata('user_id')==$id) echo "selected='selected'"?>><?php echo $name;?></option>							
                           <?php }}?> 
                    </select>
            <select name="idmod">
                           <option value="0">-All-</option>
                         <?php if( empty($Modules) ) { 
							  ?>
                              <option value="0">-All-</option>
                              <?php } else {
								 
								  foreach($Modules as $row) {
									   
									  $id = $row['id'];									 														
									  $name = stripslashes($row['name']);							   	  
						  ?>
							<option value="<?php echo $id;?>" <?php if($this->session->userdata('idmod')==$id) echo "selected='selected'"?>><?php echo $name;?></option>							
                           <?php }}?> 
                    </select>
              <select name="action">
                           <option value="0">-All-</option>
                         <?php if( empty($Actions) ) { 
							  ?>
                              <option value="0">-All-</option>
                              <?php } else {
								 
								  foreach($Actions as $row) {
									   
									  $id = $row['id'];									 														
									  $name = stripslashes($row['name']);							   	  
						  ?>
							<option value="<?php echo $id;?>" <?php if($this->session->userdata('action')==$id) echo "selected='selected'"?>><?php echo $name;?></option>							
                           <?php }}?> 
                    </select>
        <input type="submit" name="submit_form" id="submit_form" value="Search"/>
        </form>
      
    <br>
     </div>
     <br/>
    <div class="row-fluid">
      <div class="span12">
        <div class="nonboxy-widget">          
          <table class="data-tbl-striped table table-striped table-bordered">
            <thead>
              <tr>
                <th class="center"> <input name="checkbox" type="checkbox" value="" class="checkall-task">
                </th>
                <th class="center"> # </th>
                 <th> Id </th>
                <th> UserName </th>
                <th class="center"> Module</th>
                <th class="center"> Description </th>                
                <th class="center"> Action </th>
              </tr>
            </thead>
            <tbody>
				<?php 
					if(!empty($Rows)){ 	$i=1;					
						foreach($Rows as $row){
							$actname	= ($row['action'] );
							$username	= ($row['username'] );
							$modname	= ($row['name'] );
							$create_date= ($row['create_date'] );
							$description= ($row['description'] );
				?>
				<tr>
					<td class="center tr-task-check"><input class='checkboxClass' name="checkbox" type="checkbox" value="<?php echo $row['id'];?>"></td>
					<td class="center"><a href="javascript:void(0)"><?php echo $i++; ?></a></td>
                    <td><?php echo $row['id'];?></td>
					<td><?php echo $username;?>
					<td class="center"><?php echo $modname;?>  </td>
					<td ><?php echo $description;?> </td>										
					<td><?php echo $actname.'<br/> create_date : '.$create_date;?> </td>
					 
				  </tr>	
				  
				<?php }}?>	
            </tbody>
            <tfoot>
              <tr>
                <th class="center" colspan="6"> 
					<?php echo $pagination;?>	
                </th>
              </tr>
            </tfoot>			
          </table>
        </div>
      </div>
    </div>
</div>

<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
class Logeditmodule_model extends MY_Model {
	public function __construct(){  
		parent::__construct();  
	}
	private $table_name = "logeditmodule";
	
	
	public function get_list($limit, $start,$where=" where 1=1 ", $order=" order by id desc"){   
		$sql			= "select * from ".$this->table_name." where 1=1 ".$where.$order." limit ".$start.",".$limit;//print $sql;
		$query			= $this->db_slave->query($sql);		
		return $query->result_array();
	}
	
	public function get_modules(){
		$sql	= 'Select * From modules Where 1 And publish = 1';
		$query 	= $this->db_master->query($sql);
		$row 	= $query->result_array();
		return $row;
	}
	public function get_users(){
		$sql	= 'Select * From users Where 1 ';
		$query 	= $this->db_master->query($sql);
		$row 	= $query->result_array();
		return $row;
	}
	//$sql = $this->db->last_query();	
}
?>
<link rel="stylesheet" href="<?php echo $this->config->item('static_path'); ?>themes/admincp/js/select2/select2.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->config->item('static_path'); ?>themes/admincp/js/select2/theme.css" type="text/css" />
<section class="vbox">
    <section class="scrollable padder">

        <div class="m-b-md">
            <h3 class="m-b-none">Chi Tiết Liên Hệ</h3>
        </div>


        <section class="panel panel-default">
            <header class="panel-heading font-bold">
                Bảng Chi Tiết Liên Hệ
            </header>
            <div class="panel-body">
                <form id="form" name="form" method="post" action="" class="form-horizontal">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tiêu Đề</label>
                        <div class="col-sm-10">
                            <input disabled="" maxlength="30" type="text" id="title" name="title" value="<?php echo $detail[0]->title ?>"  class="form-control" placeholder="Nhập Tên Tài Khoản">
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Họ Tên</label>
                        <div class="col-sm-10">
                            <input disabled="" maxlength="30" type="text" id="name" name="name" value="<?php echo $detail[0]->name ?>"  class="form-control" placeholder="Nhập Mật Khẩu">
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Số Điện Thoại</label>
                        <div class="col-sm-10">
                            <input disabled="" maxlength="30" type="text" id="phone" name="phone" value="<?php echo $detail[0]->phone ?>"  class="form-control" placeholder="Nhập Mật Khẩu">
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input disabled="" maxlength="30" type="text" id="email" name="email" value="<?php echo $detail[0]->email ?>"  class="form-control" placeholder="Nhập Mật Khẩu">
                        </div>
                    </div>
                    <div class="line line-dashed line-lg pull-in"></div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nội Dung</label>
                        <div class="col-sm-10">
                            <div class="m-b">
                                <textarea style="width:893px; height: 200px" disabled="" id="content" name="content"><?php echo $detail[0]->content ?></textarea>
                            </div>

                        </div>
                    </div>

                    <div class="m-b">
                        <input  maxlength="30" type="text" id="status" name="status" value="1" hidden="">
                    </div>



                    <div class="line line-dashed line-lg pull-in"></div>

                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">Xác Nhận</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </section>
</section>
<script src="<?php echo $this->config->item('static_path'); ?>themes/admincp/js/select2/select2.min.js"></script>

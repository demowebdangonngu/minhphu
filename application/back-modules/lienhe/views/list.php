<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-user"></i> Bảng Xem Liên Hệ</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">

                <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tiêu Đề</th>
                            <th>Họ Tên</th>
                            <th>SĐT</th>
                            <th>Email</th>
                            <th>Status</th>
                            <th>Hành Động</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $stt = 1;
                        foreach ($data as $item) {
                            ?>
                            <tr>
                                <td><?php echo $stt++ ?></td>
                                <td><?php echo $item->title ?></td>
                                <td><?php echo $item->name ?></td>
                                <td><?php echo $item->phone ?></td>
                                <td><?php echo $item->email ?></td>
                                <td><?php echo $item->status == 1 ? "Đã Xem" : "Chưa Xem" ?></td>
                                <td class="center">
                                    <a title="Chi Tiết" href="<?php echo base_url() . 'lienhe/detail/' . $item->id_lienhe; ?>"><input type="image" src="<?php echo $this->config->item('static_path'); ?>themes/admincp/images/new-24-256.png" height="30px" width="30px"></a>---
                                    <a title="Xóa" onClick="return confirm('Chắc chắn bạn muốn xóa?');" href="<?php echo base_url() . 'lienhe/delete?id=' . $item->id_lienhe; ?>"><input type="image" src="<?php echo $this->config->item('static_path'); ?>themes/admincp/images/editing-delete-icon.png" height="30px" width="30px"></a>
                                </td>
                            </tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!--/span-->

</div><!--/row-->
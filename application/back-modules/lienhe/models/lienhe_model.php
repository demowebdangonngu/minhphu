<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class Lienhe_model extends YT_AModel {
	public function __construct(){  
		parent::__construct();  
		$this->table_name="lienhe";
	}
	public function getList(){
		$this->getDB()->select("*");
		$this->getDB()->from($this->table_name);
		return $this->getDB()->get()->result();
	}
	function getone1($id){
            $this->getDB()->select('*');
            $this->getDB()->from($this->table_name);
            $this->getDB()->where('id_lienhe', $id);
            return $this->getDB()->get()->result();
        }
	public function insert_database($data){
	 
		 $this->getDB()->insert($this->table_name, $data); 
		 
	}
	
	public function update_database($data , $where){
	       $this->getDB()->where($where);
            $this->getDB()->update($this->table_name,$data);
	}
        public function getOne($where){
		$this->getDB()->select("*");
		$this->getDB()->from($this->table_name);
        $this->getDB()->where($where);
		//$this->getDB()->get()->result();
		return $this->getDB()->get()->result();
	}
	
	function delete($id){
            $this->getDB()->where('id',$id);
            $this->getDB()->delete($this->table_name);
        }

	
	//$sql = $this->db->last_query();	
}
?>
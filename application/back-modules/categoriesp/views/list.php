hư<script>
    var module = '<?php echo $this->uri->segment(1); ?>';
    var base_url = '<?php echo base_url(); ?>';
</script>
<script type="text/javascript">
$(document).ready(function (e) {
    $('.aut_img').each(function (index, element) {
        $(this).error(function () {
            $(this).hide();
        });
    });
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result).show();
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function readURL1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah1').attr('src', e.target.result).show();
        }
        reader.readAsDataURL(input.files[0]);
    }
}
</script>
<section id="content" style="width: 100%;">


    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">

                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>"><?php
                        if ($module_title) {
                            echo $module_title;
                        } else {
                            echo $this->uri->segment(1);
                        };
                        ?></a></li>
                <!--<li class="active">Static table</li>-->
            </ul>
            <div class="row">
                <div class="col-sm-6">
                    <form id="menu-sorter-form" data-validate="parsley" method="post" action="<?php echo base_url().$this->uri->segment(1); ?>">
                        <input type="hidden" name="cat_order" id="cat_order" value="">
                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <span class="h4">Danh sách thể loại bài viết</span>
                                <ul class="nav nav-tabs pull-right">
                                    <li <?php echo (!$this->input->get("trash") ? "class='active'" : ""); ?>><a href="<?php echo base_url() . $this->uri->segment(1); ?>" ><i class="fa fa-file-text text-default"></i>&nbsp; Danh mục</a></li>
                                    <li <?php
                                    echo ($this->input->get("trash") ? "class='active'" : "");
                                    ;
                                    ?>><a href="<?php echo base_url() . $this->uri->segment(1) . "?trash=1"; ?>" ><i class="fa fa-trash-o text-default"></i>&nbsp; Hiện danh mục đã xóa</a></li>
                                </ul>
                            </header>
                            <div class="panel-body">
                                <div class="dd" id="categories" data-output="">
                                    <ol class="dd-list">
                                        <?php
                                        /* @var $catemodel Categories_model */
                                        $catemodel = $this->load->model("Categoriesp_model");
                                        isset($_GET['trash']) ? $status_array = array(0, 1, 2) : $status_array = array(0, 1);
                                        $categories = $catemodel->get_tree($status_array);

                                        if (isset($categories)) {
                                            foreach ($categories as $cat) {
                                                if($cat->type == 1){
                                                ?>
                                                <li class="dd-item parent_category <?php echo "status_" . $cat->status; ?>" data-id="<?php echo $cat->id; ?>">
                                                    <div class="dd-handle"><?php echo $cat->name; ?></div>
                                                    <span class="pull-right" style="position: absolute;right: 0px;top: 6px;">
                                                        <?php ?>
                                                        <a title="Chỉnh sửa danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '?id=' . $cat->id; ?>"><i class="fa fa-pencil icon-muted fa-fw m-r-xs"></i></a>
                                                        <?php ?>
                                                        <?php if (!count($cat->children)) { ?>
                                                            <?php if ($cat->status != 2) { ?>
                                                                <a href="<?php echo base_url() . $this->uri->segment(1) . '/quick_trash?ids=' . $cat->id; ?>"><i class="fa fa-times icon-muted fa-fw"></i></a>
                                                            <?php } else { ?>
                                                                <a title="Hiện danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '/update_status/' . $cat->id . '?status=1&return=1' ?>"><i class="fa icon-muted  fa-fw  fa-check-circle-o"></i></a>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </span>
                                                    <?php
                                                    if (count($cat->children)) {
                                                        ?>
                                                        <ol class="dd-list">
                                                            <?php
                                                            foreach ($cat->children as $cat2) {
                                                                ?>
                                                                <li class="dd-item <?php echo "status_" . $cat2->status; ?>" data-id="<?php echo $cat2->id; ?>">
                                                                    <div class="dd-handle"><?php echo $cat2->name; ?></div>
                                                                    <span class="pull-right" style="position: absolute;right: 0px;top: 6px;">

                                                                        <a title="Chỉnh sửa thể loại" href="<?php echo base_url() . $this->uri->segment(1) . '?id=' . $cat2->id; ?>"><i class="fa fa-pencil icon-muted fa-fw m-r-xs"></i></a>
                                                                        <?php if ($cat2->status != 2) { ?>
                                                                            <a href="<?php echo base_url() . $this->uri->segment(1) . '/quick_trash?ids=' . $cat2->id; ?>"><i class="fa fa-times icon-muted fa-fw"></i></a>
                                                                        <?php } else { ?>
                                                                            <a title="Hiện danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '/update_status/' . $cat2->id . '?status=1&return=1' ?>"><i class="fa icon-muted  fa-fw  fa-check-circle-o"></i></a>

                                                                        <?php } ?>
                                                                        <?php ?>
                                                                    </span>
                                                                </li>
                                                                <?php
                                                                if (count($cat2->children)) {
                                                                    ?>
                                                                    <ol class="dd-list">
                                                                        <?php
                                                                        foreach ($cat2->children as $cat3) {
                                                                            ?>
                                                                            <li class="dd-item <?php echo "status_" . $cat3->status; ?>" data-id="<?php echo $cat3->id; ?>">
                                                                                <div class="dd-handle"><?php echo $cat3->name; ?></div>
                                                                                <span class="pull-right" style="position: absolute;right: 0px;top: 6px;">

                                                                                    <a title="Chỉnh sửa thể loại" href="<?php echo base_url() . $this->uri->segment(1) . '?id=' . $cat3->id; ?>"><i class="fa fa-pencil icon-muted fa-fw m-r-xs"></i></a>
                                                                                    <?php if ($cat3->status != 2) { ?>
                                                                                        <a href="<?php echo base_url() . $this->uri->segment(1) . '/quick_trash?ids=' . $cat3->id; ?>"><i class="fa fa-times icon-muted fa-fw"></i></a>
                                                                                    <?php } else { ?>
                                                                                        <a title="Hiện danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '/update_status/' . $cat3->id . '?status=1&return=1' ?>"><i class="fa icon-muted  fa-fw  fa-check-circle-o"></i></a>

                                                                                    <?php } ?>
                                                                                    <?php ?>
                                                                                </span>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </ol>  
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ol>
                                                        <?php
                                                    }
                                                    ?>
                                                </li>
                                                <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                            <footer class="panel-footer text-right bg-light lter">
                                <?php ?>
                                <input type="button" onclick="update_form($('#menu-sorter-form'), update_menu_callback);
                                        return false;" class="btn btn-success btn-s-xs" value="Cập nhật thứ tự"/>
                                       <?php ?>
                            </footer>
                        </section>
                        <style>
                            .dd li.status_0 .dd-handle{
                                color: #bbb;
                                background: #F7F7F7;
                            }
                            .dd li.status_2 .dd-handle{
                                color: #bbb;
                                background: #FF6666;
                            }


                        </style>
                    </form>
                </div>
                <?php ?>
                <div class="col-sm-6">
                    <form id="add-insert-menu-frm" method="post" action="<?php echo base_url() . $this->uri->segment(1); ?>/addedit_process/" enctype="multipart/form-data">
                        <input type="hidden" name="data[id]" value="<?php echo $obj->id; ?>">
                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <span class="h4"><?php if ($obj->id) { ?>Sửa<?php } else { ?>Thêm<?php } ?> danh mục</span>
                            </header>
                            <div class="panel-body">
                                <div id="msg_error_msg" style="color:#f00;"></div>
                                <div class="form-group">
                                    <label>Chọn danh mục cha</label>
                                    <select name="data[parent]" id="categories_parent" class="form-control">
                                        <option value="0" <?php
                                        if (!$obj->parent) {
                                            echo "selected";
                                        };
                                        ?>>Không có danh mục cha</option>
                                                <?php
                                                foreach ($categories as $cat) {
                                                    if($cat->type == 1){
                                                    ?>
                                            <option value="<?php echo $cat->id; ?>" <?php if ($obj->parent == $cat->id) { ?>selected<?php } ?>> <?php echo $cat->name; ?></option>

                                            <?php
                                            if (count($cat->children)) {
                                                foreach ($cat->children as $cat2) {
                                                    ?>
                                                    <option value="<?php echo $cat2->id; ?>" <?php if ($obj->parent == $cat2->id) { ?>selected<?php } ?>>&nbsp;|-- <?php echo $cat2->name; ?></option>
                                                    <?php
                                                    }
                                                }
                                            }
                                            ?>

                                        <?php }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Tên danh mục</label>
                                    <input required minlength="3" vname="Tên danh mục" type="text" name="data[name]" class="form-control" value="<?php echo $obj->name; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Đường dẫn</label>
                                    <input maxlength="1024" type="text" name="data[slug]" class="form-control" value="<?php echo $obj->slug; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Giới thiệu - description</label>
                                    <textarea class="form-control" name="data[description]"><?php echo $obj->description; ?></textarea> 
                                </div>
                                <div class="form-group">
                                    <label>Icon</label>
                                    <div class="radio">
                                        <?php 
                                        $icon = list_icon_bv();
                                        $stt=0;foreach($icon as $row){$stt++;
                                        ?>
                                        <label class="radio-custom" style="padding-left: 50px;">
                                            <input <?php if($obj->icon == $stt){?>checked="checked"<?php }?> type="radio" name="data[icon]" value="<?php echo $stt;?>">
                                            <img width="20" height="20" src="<?php echo $row;?>">
                                        </label>
                                        <?php }?>

                                    </div>
                                </div>
                                <!--<div class="form-group">
                                    <label>Số bài viết show theo hàng</label>
                                    <div class="radio">
                                        <select name="data[num_row_product_show_home]" id="categories_parent" class="form-control">
                                        <?php 
                                        $num = list_num_row_product_show_home();
                                        $stt=0;foreach($num as $row){$stt++;
                                        ?>
                                        <option <?php if($obj->num_row_product_show_home == $stt){?>selected="selected"<?php }?> value="<?php echo $stt;?>"><?php echo $row;?></option>
                                        <?php }?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Số sản phẩm show</label>
                                    <div class="radio">
                                        <select name="data[num_product_show_home]" id="categories_parent" class="form-control">
                                        <?php 
                                        $num = list_num_row_product_show_home();
                                        $stt=0;foreach($num as $row){$stt++;
                                        ?>
                                        <option <?php if($obj->num_product_show_home == $stt){?>selected="selected"<?php }?> value="<?php echo $stt;?>"><?php echo $row;?></option>
                                        <?php }?>
                                        </select>
                                    </div>
                                </div>-->
                                <!--
                              <div class="form-group">
                                <label class="col-sm-5 control-label" style="margin-left: -13px;">Hiện trang chủ</label>
                                <div class="col-sm-7">
                                  <label class="switch">
                                    <input type="checkbox" value="1" name="data[show]" <?php if ($obj->show) { ?>checked<?php } ?>>
                                    <span></span>
                                  </label>
                                </div>
                                <div class="clear"></div>
                              </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="margin-left: -13px;">Chạy slider</label>
                                    <div class="col-sm-7">
                                        <label class="switch">
                                            <input type="checkbox" value="1" name="data[have_slide]" <?php if ($obj->have_slide) { ?>checked<?php } ?>>
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="clear"></div>
                                </div>-->
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="margin-left: -13px;">Hoạt động</label>
                                    <div class="col-sm-7">
                                        <label class="switch">
                                            <input type="checkbox" value="1" name="data[status]" <?php if ($obj->status) { ?>checked<?php } ?>>
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <footer class="text-right bg-light lter">
                                    <input type="submit" class="btn btn-primary" value="<?php if (isset($obj->id)) { ?>Cập nhật<?php } else { ?>Thêm<?php } ?>"/>
                                </footer>
                            </div>
                        </section>
                    </form>
                </div>
                <?php ?>
            </div>
        </section>
    </section>
    <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
</section>
<script>
    $(document).ready(function ()
    {
        // activate Nestable for list 1
        if ($('#categories').length)
        {
            $('#categories').nestable({
                group: 0,
                maxDepth: 3
            }).on('change', updateOutput);
        }
        if ($('#menus').length)
        {
            $('#menus').nestable({
                group: 0,
                maxDepth: 1
            }).on('change', updateOutput);
        }
    });
    var change = false;
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target);
        //alert(window.JSON.stringify(list.nestable('serialize')));
        change = true;
        $("#cat_order").val(window.JSON.stringify(list.nestable('serialize')));
    };
    function update_menu_callback(data) {
        alert('đã cập nhật thành công!');
        change = false;
    }
    function update_form_callback(data) {
        if (data.status)
            window.location = base_url + module;
    }
    $(document).ready(function (e) {
    });
    window.onbeforeunload = function () {
        if (change)
            return 'Bạn vừa thay đổi vị trí của các danh mục bên trái, nếu thoát trang thì các vị trí sẽ không được lưu lại!';
    };
</script>
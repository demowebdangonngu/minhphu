<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class Categoriesp_model extends YT_AModel {

    function __construct() {
        parent::__construct();
        /*
          id
          name
          slug
          parent
          ORDER
          SHOW
          STATUS
          TYPE
          forwc

         */
        $this->fields = array(
            'id' => '1',
            'name' => '1',
            'slug' => '1',
            'parent' => 1,
            'sort_order' => 1,
            'show' => 1,
            'type' => 1,
            'forwc' => 1,
            'create_date' => '1',
            'modify_date' => '1',
            'order' => 1,
            'league_id' => 1,
            'status' => '1',
            'image_home' => 1,
            'image_page' => 1,
            'description' => 1,
            'icon' => 1,
            'num_row_product_show_home' => 1,
            'have_slide' => 1,
            'num_product_show_home' => 1,
        );
        $this->fields_lang = array();
        $this->table_name = 'categories';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->id_field_lang = '';
        $this->status_field = 'status';
        $this->cat_field = '';
        $this->order_field = 'sort_order';
        $this->image_field = array();
        ///$this->id_lang_rel=$this->id_field;
    }

    public $obj_array = array();
    //public $tree_array=array();
    public $tree = array();

    function get_tree($status = array(0, 1, 2)) {
        sort($status);
        $key = implode("", $status);
        if (count($this->tree[$key]))
            return $this->tree[$key];
        $this->getDB()->select("*");
        $this->getDB()->from($this->table_name);
        $this->getDB()->where_in('status', $status);
        $this->getDB()->order_by('order');
        $list = $this->getDB()->get()->result();
        foreach ($list as $cat) {
            $this->obj_array[$key][$cat->id] = $cat;
            //$this->tree_array[$cat->id]=$cat;
            $this->obj_array[$key][$cat->id]->children = array();
            if ($cat->parent == 0) {//root node
                $this->tree[$key][$cat->id] = $cat;
            }
        }
        foreach ($this->obj_array[$key] as $keys => $obj) {
            if ($this->obj_array[$key][$obj->parent]) {
                $this->obj_array[$key][$obj->parent]->children[$obj->id] = $obj;
            }
        }
        return $this->tree[$key];
    }

    function get_node($id, $status = array(0, 1, 2)) {
        sort($status);
        $key = implode("", $status);
        if (!isset($this->tree[$key]))
            $this->get_tree($status);
        if ($this->obj_array[$key][$id]) {
            return $this->obj_array[$key][$id];
        }
        return null;
    }

    function store_tree($tree_array, $parent_id = 0) {
        $sort = 1;
        foreach ($tree_array as $cate) {
            $this->getDB()->update($this->table_name, array('parent' => $parent_id, 'order' => $sort++), array('id' => $cate->id));
            if (count($cate->children)) {
                $this->store_tree($cate->children, $cate->id);
            }
        }
    }

    function get_children_ids($id, $status = array(0, 1, 2)) {
        sort($status);
        $key = implode("", $status);
        if (!isset($this->tree[$key]))
            $this->get_tree($status);
        $ids = array();
        if ($this->obj_array[$key][$id]) {
            $ids[$id] = $id;
            foreach ($this->obj_array[$key][$id]->children as $c) {
                if ($c->id)
                    $ids = array_merge($ids, $this->get_children_ids($c->id, $status));
            }
        }
        return $ids;
    }

    function get_cat_id_for_post($post_id) {
        $this->getDB()->from("posts_category");
        $this->getDB()->where(array('post_id' => $post_id));
        $l = $this->getDB()->get()->result();
        $result = array();
        foreach ($l as $row) {
            $result[$row->cat_id] = $row->cat_id;
        }
        return $result;
    }

    function on_data_change() {
        /* @var $caches MY_Caches */
        $caches = $this->load->library('Caches');
        $caches->on_categories_update();
        //parent::on_data_change();
    }

}

?>
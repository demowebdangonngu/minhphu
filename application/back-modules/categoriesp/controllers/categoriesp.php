<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require_once APPPATH."third_party/YT/ControllerAdmin.php";
class Categoriesp extends YT_ControllerAdmin {
	/**
     *
     * @var Test_model 
     */
	public $model;
        /**
         *
         * @var Genre_model
         */
        public $genre;
	function __construct()	{
		parent::__construct();	
		$this->model=$this->load->model('categoriesp_model');	
	}
        function addedit_process() {
            // if($_POST['data']['slug']) $_POST['data']['slug']=  remove_accent ($_POST['data']['slug']);
            // if (isset($_FILES['img']['name']) && $_FILES['img']['name'] != "") {
                // move_uploaded_file($_FILES['img']['tmp_name'], '../upload/images/images/' . time() . "_" . $_FILES['img']['name']);
                // $_POST['data']['image_home'] = time() . "_" . $_FILES['img']['name'];
            // }
            // if (isset($_FILES['img1']['name']) && $_FILES['img1']['name'] != "") {
                // move_uploaded_file($_FILES['img1']['tmp_name'], '../upload/images/images/' . time() . "_" . $_FILES['img1']['name']);
                // $_POST['data']['image_page'] = time() . "_" . $_FILES['img1']['name'];
            // }
			$_POST['data']['type'] = 1;
            if($_POST['data']['id'] == ''){
                $_POST['data']['create_date'] = date('Y-m-d H:i');
				$_POST['data']['modify_date'] = date('Y-m-d H:i');
				unset($_POST['data']['id']);
            }else{
				$_POST['data']['modify_date'] = date('Y-m-d H:i');
			}
            parent::addedit_process();
        }
        function on_after_addedit_process($id) {
            redirect("categoriesp", 'refresh');
            //if(!$_POST['data']['show']) $_POST['data']['show']=0;
            //if(!$_POST['data']['status']) $_POST['data']['status']=0;
            parent::on_after_addedit_process($id);
            
        }
        function index() {
            if(isset($_POST['cat_order'])){
//                echo "<pre>";
//                print_r(json_decode($_POST['cat_order']));
//                echo "</pre>";
//                exit;
                $array=json_decode($_POST['cat_order']);
                if(count($array)){
                    $this->model->store_tree($array);
                    $this->model->on_data_change();
                }
                echo json_encode(array('status'=>1));exit;
            }
            if(intval($_GET['id'])){
                $this->model->where['id']=intval($_GET['id']);
                $this->data['obj']=$this->model->get_object_by_where();
            }
            parent::index();
        }
}
 ?>
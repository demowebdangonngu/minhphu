<?php
$product_type = $this->load->model("categories/Categories_model");
isset($_GET['trash']) ? $status_array = array(0, 1, 2) : $status_array = array(0, 1);
$product_t = $product_type->get_tree($status_array);
?>
<script>
    var module = '<?php echo $this->uri->segment(1); ?>';
    var base_url = '<?php echo base_url(); ?>';
</script>
<link rel="stylesheet" href="<?php echo $this->config->item('admin_url') ?>themes/admincp/css/specifications.css" type="text/css" />
<script>
    sttbt1 = 0;
    sttbt2 = 0;
    $(document).ready(function () {
        
        $("#menus_type").change(function () {
            $.ajax({
                url:"specifications_category/listsp?ch="+this.value,
                success:function(result){
                    $("#txtResult").html(result);
                }});
            $.ajax({
                url:"specifications_category/addedit?ch="+this.value,
                success:function(result){
                    $("#result").html(result);
                }});
           
        });
        
        $('#btn1').click(function () {
            sttbt1++;
            //========================
            classL = 'dd-list-' + sttbt1;
            $idNum = 'addNumber-' + sttbt1;
            idnum = $idNum + ' btn btn-success btn-s-xs';
            $select = 'select-' + sttbt1;
            $classC = "addbt2('." + classL + "')";
            $num = 'number-' + sttbt1;
            $namesp_id = 'data[group][' + sttbt1 + '][sp_id]';
            $name_group = 'data[group][' + sttbt1 + '][sort_group]';
            //========================
            $('#select1').attr('name', $namesp_id);
            $('#select1').attr('id', $select);
            $('.dd-list1').attr('class', classL);
            $('.addNumber1').attr('class', idnum);
            $('.' + $idNum).attr('onclick', $classC);
            $('.number1').attr('class', $num);
            $('.' + $num).attr('name', $name_group);
            //========================
            $("#" + $select).select2().change(function () {
            });
        });
    });
    //==bt1====================
    function addbt2(id) {
        sttbt2++;
        $html = "<div class='dd-item content' id='group'>\n\
    <div class='col-left'>\n\
    <select name='' style='width:100%;' id='select11'>\n\
<?php
$specifications_detail = $this->load->model('specifications_detail/Specifications_detail_model');
$datad = $specifications_detail->listAll();
?><?php foreach ($datad as $row) { ?><?php if ($row->status == 1) { ?>\n\                                                                                                                                                                                  <option <?php if ($row->id) echo 'selected=selected'; ?> value='<?php echo $row->id; ?>'>&nbsp;<?php echo $row->title; ?></option>\n\
        <?php
    }
}
?></select></div>\n\
    <div class='col-right'><input style='width: 100%;text-align: center;margin-top: -3px;float: left;' class='sort-order1' name='' value='0'></div>\n\
    <input type='button' onclick='' class='close' id='close' value='X' /><br/><br/></div>\n\
    ";
        $(id).append($html);
        $('.close').click(function () {
            $(this).parent().remove();
        });

        //var a = document.getElementById(this).className;
        var this_class = $(id).attr('class');
        var group = this_class.substr(8, 4);

        //=======================
        $select = 'select-a-' + sttbt2;
        $sort_order = 'sort-order-1' + sttbt2;
        $namesort_order = 'data[group][' + group + '][group_1][' + sttbt2 + '][sort_order]';
        $namesp_did = 'data[group][' + group + '][group_1][' + sttbt2 + '][sp_did]';
        //=======================
        $('#select11').attr('name', $namesp_did);
        $('#select11').attr('id', $select);
        $('.sort-order1').attr('class', $sort_order);
        $('.' + $sort_order).attr('name', $namesort_order);
        //=======================
        $("#" + $select).select2().change(function () {
        });
    }
    ;
//==bt2======================
    function addbt1(id) {
        $html = "\
<div class='myBox' style='position: relative;'>\n\
    <div class='boxHeader'>\n\
        <div class='col-left'><div>\n\
        <select name='' style='width:100%;' id='select1'>\n\
<?php
$specifications = $this->load->model('specifications/specifications_model');
$data = $specifications->listAll();
?><?php foreach ($data as $row) { ?><?php if ($row->status == 1) { ?>\n\
                                                                                                                                                                                                                                                                        <option <?php if ($row->id) echo 'selected=selected'; ?> value='<?php echo $row->id; ?>'>&nbsp;<?php echo $row->title; ?></option>\n\
    <?php } ?><?php } ?></select>\n\
    </div>\n\
</div>\n\
<div class='col-right'>\n\
    <input style='width: 90%;margin-top: 10px;margin-left: 3px;text-align: center;' class='number1' name='' value='0'>\n\
</div>\n\
</div>\n\
    <div class='dd boxBody' id='menus-top' data-output=''>\n\
        <div class='boxButton'>\n\
            <div class='boxButton'><input id='addNumber' type='button' onclick='' class='addNumber1 btn btn-success btn-s-xs' style='background: url(<?php echo $this->config->item('admin_url') ?>/themes/admincp/images/addspemini.png);background-repeat: no-repeat;min-width: 30px;height:29px'></div>\n\
        <div class='dd-list1'>\n\
        </div>\n\
        </div>\n\
    </div><input style='position: absolute;top: 0px;right: -30px;width: 25px;height: 25px;' type='button' onclick='$(this).parent().remove();' class='close' id='close' value='X' />\n\
    ";
        $(id).append($html);
    }
</script>

<section id="content">

    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>"><?php
                        if ($module_title) {
                            echo $module_title;
                        } else {
                            echo $this->uri->segment(1);
                        }
                        ?></a></li>
                <!--<li class="active">Static table</li>-->
            </ul>
            <div class="row">
                <div class="col-sm-6">
                    <input type="hidden" name="cat_order" id="cat_order" value="">
                    <section class="panel panel-default">
                        <header class="panel-heading">
                            <span class="h4">Danh sách</span>
                            <ul class="nav nav-tabs pull-right">
                                <li <?php echo (!$this->input->get("trash") ? "class='active'" : ""); ?>><a href="<?php echo base_url() . $this->uri->segment(1); ?>" ><i class="fa fa-file-text text-default"></i>&nbsp; Danh mục</a></li>
                                <li <?php echo ($this->input->get("trash") ? "class='active'" : ""); ?>><a href="<?php echo base_url() . $this->uri->segment(1) . "?trash=1"; ?>" ><i class="fa fa-trash-o text-default"></i>&nbsp; Hiện danh sách đã xóa</a></li>
                            </ul>
                        </header>
                        <div id="txtResult" class="panel-body">

                        </div>

                        
                    </section>
                    <style>
                        .dd li.status_0 .dd-handle{
                            color: #bbb;
                            background: #F7F7F7;
                        }
                        .dd li.status_2 .dd-handle{
                            color: #bbb;
                            background: #FF6666;
                        }


                    </style>

                </div>
                <?php ?>
                <div class="col-sm-6">
                    <form  method="post" id='add-insert-menu-frm' action="<?php echo base_url() . $this->uri->segment(1); ?>/addedit_process/" onsubmit="update_form($(this), update_menu_callback);
                            return false;">
                        <input type="hidden" name="data[id]" value="<?php echo $obj->id; ?>">
                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <span class="h4"><?php if ($obj->id) { ?>Sửa<?php } else { ?>Thêm<?php } ?> danh sách TSKT</span>
                            </header>
                            <div class="panel-body">
                                <div id="msg_error_msg" style="color:#f00;"></div>
                                <div>
                                    <label>Chọn Loại Sản Phẩm</label>

                                    <select name="data[product_tid]" id="menus_type" class="form-control">
                                        <option value="0"> Lựa chọn loại sản phẩm</option>
                                        <?php
                                                foreach ($product_t as $cat) {
                                                    if($cat->type == 2){
                                                    ?>
                                            <option value="<?php echo $cat->id; ?>" <?php if ($obj->parent == $cat->id) { ?>selected<?php } ?>> <?php echo $cat->name; ?></option>

                                            <?php
                                            if (count($cat->children)) {
                                                foreach ($cat->children as $cat2) {
                                                    ?>
                                                    <option value="<?php echo $cat2->id; ?>" <?php if ($obj->parent == $cat2->id) { ?>selected<?php } ?>>&nbsp;|-- <?php echo $cat2->name; ?></option>
                                                    <?php
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <br>
                                <div class="form-group" style="overflow: hidden;">
                                    <label class="col-sm-5 control-label" style="margin-left: -13px;">Hoạt động</label>
                                    <div class="col-sm-7">
                                        <label class="switch">
                                            <input type="checkbox" value="1" name="data[status]" <?php if ($obj->status || !$obj->id) { ?>checked<?php } ?>>
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <div id="result">

                                </div>
                                <p class="add"></p>
                                <input type="button" onclick="addbt1('.add')" class="btn btn-success btn-s-xs" id="btn1" style=" background: url(<?php echo $this->config->item('admin_url') ?>/themes/admincp/images/addspe.png);min-width: 38px; min-height:38px;margin-top: -10px;" /><br/><br/>
                                <div class="bao" id="bao"></div>
                                <footer class="panel-footer text-right bg-light lter">
                                    <input type="button" onclick="update_form($('#add-insert-menu-frm'), update_form_callback);
                                            return false;" class="btn btn-success btn-s-xs" value="<?php if (isset($obj->id)) { ?>Cập nhật<?php } else { ?>Thêm<?php } ?>"/>
                                </footer>
                            </div>
                        </section>
                    </form>
                </div>
                <?php ?>
            </div>
        </section>
    </section>
    <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
</section>
<script>
    function update_form_callback(data) {
        if (data.status)
            window.location = base_url + module;
    }
</script>
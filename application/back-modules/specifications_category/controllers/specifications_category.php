<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/ControllerAdmin.php";

class Specifications_category extends YT_ControllerAdmin {
    public $model;
    function __construct() {
        parent::__construct();
        $this->model = $this->load->model('specifications_category_model');
        $this->check_access('specifications_category_access');
    }

    public function index() {
        if (isset($_POST['sort_order'])) {
//                echo "<pre>";
//                print_r(json_decode($_POST['cat_order']));
//                echo "</pre>";
//                exit;
            $array = json_decode($_POST['sort_order']);
            if (count($array)) {
                $this->model->store_tree($array);
                $this->model->on_data_change();
            }
            echo json_encode(array('status' => 1));
            exit;
        }
        if (intval($_GET['id'])) {
            $this->model->where['id'] = intval($_GET['id']);
            $this->data['obj'] = $this->model->get_object_by_where();
        }
        parent::index();
    }

    function addedit_process() {
        $result = array('status' => 1, 'id' => 0);
        $input = $this->input->post("data");
        $id = $this->model->sp_category($this->input->post("data"));
        if ($id) {
            $afterpc = $this->on_after_addedit_process($id);
            if (is_array($afterpc)) {
                $result = array_merge($result, $afterpc);
            }
            $result['id'] = $id;
        } else {
            $result['status'] = 0;
        }
        echo json_encode($result);
        exit;
    }

    function on_after_addedit_process($id) {
        parent::on_after_addedit_process($id);
    }

    function addedit() {
        $this->load->view('addedit');
    }

    function listsp() {
        $this->load->view('listsp');
    }
    

}

?>
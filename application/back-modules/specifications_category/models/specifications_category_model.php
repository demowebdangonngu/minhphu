<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class Specifications_category_model extends YT_AModel {

    function __construct() {
        parent::__construct();



        $this->fields = array(
            'id' => 'pri',
            'status' => 1,
            'sort_order' => 1,
            'group' => 1,
            'sp_id' => 1,
            'sp_did' => 1,
            'product_tid' => 1,
            'sort_group' => 1,
                //'order'=>1,
        );
        $this->table_name = 'specifications_category';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->id_field_lang = '';
        $this->status_field = 'status';
        $this->id_lang_rel = $this->id_field;
        $this->order_field = 'sort_order';
        $this->group_field = 'sort_group';
    }

    function on_data_change() {
        /* @var $caches MY_Caches */
        $caches = $this->load->library('Caches');
        $caches->on_post_update();
        //parent::on_data_change();
    }

    function listAll($where, $sort) {
        $this->getDB()->select('specifications_category.*, specifications.title as name_sp, specifications_detail.title as name_spd');
        $this->getDB()->from($this->table_name);
        $this->getDB()->join("categories", "categories.id=specifications_category.product_tid", "left");
        $this->getDB()->join("specifications", "specifications.id=specifications_category.sp_id", "left");
        $this->getDB()->join("specifications_detail", "specifications_detail.id=specifications_category.sp_did", "left");
        $this->getDB()->where($where);
        $this->getDB()->order_by($sort);
        return $this->getDB()->get()->result();
    }
    function getcount($select, $where){
        $this->getDB()->select($select);
        $this->getDB()->from($this->table_name);
        $this->getDB()->where($where);
        return $this->getDB()->get()->result();
    }

    function sp_category($array) {
        $data = array();
        foreach ($this->fields as $key => $value) {
            isset($array[$key]) ? $data[$key] = $array[$key] : "";
        }
        $returnid = 0;
        //print_r($data);exit;
        foreach ($data['group'] as $row) {
            if ($row['sp_id'] == Null && $row['sort_group'] == Null) {
                unset($data['group']);
                unset($data['sp_did']);
                unset($data['id']);
                $this->getDB()->where(array('product_tid' => $data['product_tid'], 'sort_group' => $row['sort']));
                $this->getDB()->delete($this->table_name);
                unset($row['sort']);
            } else {
                $data['sort_group'] = $row['sort_group'];
                $data['sp_id'] = $row['sp_id'];
                if ($row['group_1']) {
                    foreach ($row['group_1'] as $row_sp) {
                        if (!$row_sp['id']) {
                            $sql = 'SELECT COUNT(id) as stt FROM specifications_category WHERE sort_group="' . $data['sort_group'] . '" and sp_id="' . $data['sp_id'] . '" and product_tid="' . $data['product_tid'] . '"';
                            $count = $this->getDB()->query($sql)->result();
                            if ($count[0]->stt == 1 && $row['id']) {
                                $data['sp_did'] = $row_sp['sp_did'];
                                $data['sort_order'] = $row_sp['sort_order'];
                                unset($data['id']);
                                unset($data['group']);
                                $this->getDB()->where(array('id' => $row['id']));
                                $this->getDB()->update($this->table_name, $data);
                                unset($row['id']);
                            }else{
                                $data['sp_did'] = $row_sp['sp_did'];
                                $data['sort_order'] = $row_sp['sort_order'];
                                unset($data['id']);
                                unset($data['group']);
                                $this->getDB()->insert($this->table_name, $data);
                                $returnid = $this->getDB()->insert_id();
                            }
                        } else {
                            $data['sp_did'] = $row_sp['sp_did'];
                            $data['sort_order'] = $row_sp['sort_order'];
                            if ($data['sort_order'] == Null && $data['sp_did'] == Null) {
                                $sql = 'SELECT COUNT(id) as stt FROM specifications_category WHERE sort_group="' . $data['sort_group'] . '" and sp_id="' . $data['sp_id'] . '" and product_tid="' . $data['product_tid'] . '"';
                                $count = $this->getDB()->query($sql)->result();
                                if ($count[0]->stt == 1) {
                                    $data['id'] = $row_sp['id'];
                                    unset($data['group']);
                                    $this->getDB()->where(array('id' => $data['id']));
                                    $this->getDB()->update($this->table_name, $data);
                                    unset($data['id']);
                                } else {
                                    $data['id'] = $row_sp['id'];
                                    unset($data['group']);
                                    $this->getDB()->where(array('id' => $data['id']));
                                    $this->getDB()->delete($this->table_name);
                                    unset($data['id']);
                                }
                            } else {
                                $data['id'] = $row_sp['id'];
                                unset($data['group']);
                                $this->getDB()->where(array('id' => $data['id']));
                                $this->getDB()->update($this->table_name, $data);
                                unset($data['id']);
                            }
                        }
                    }
                } else {
                    if (!$row['id']) {
                        unset($data['id']);
                        unset($data['group']);
                        unset($data['sp_did']);
                        $this->getDB()->insert($this->table_name, $data);
                        $returnid = $this->getDB()->insert_id();
                    } else {
                        unset($data['group']);
                        unset($data['sp_did']);
                        $data['id'] = $row['id'];
                        $this->getDB()->where(array('id' => $data['id']));
                        $this->getDB()->update($this->table_name, $data);
                    }
                }
            }
        }

        if ($array['_mlang'] && $this->table_name_lang) {
            foreach ($array['_mlang'] as $idx => $lang) {
                $lang['lang_id'] = $idx;
                $this->store_data_lang($returnid, $lang);
            }
        }
        $this->on_data_change($returnid);
        return $returnid;
    }

}

?>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/ControllerAdmin.php";

class votes extends YT_ControllerAdmin {

    /**
     *
     * @var Test_model 
     */
    public $model;

    function __construct() {
        parent::__construct();
        $this->model = $this->load->model('votes_model');
        if ($this->uri->segment(1) != 'votes')
            redirect(base_url() . "votes");
        $this->check_access('votes');
    }

    function addedit($id = 0) {
        parent::addedit($id);
    }

    function addedit_process() {

        $id = $this->model->store_data($this->input->post("data"));
        if ($id) {
            $afterpc = $this->on_after_addedit_process($id);
            if (is_array($afterpc)) {
                $result = array_merge($result, $afterpc);
            }
            $result['id'] = $id;
        } else {
            $result['status'] = 0;
        }

        echo json_encode($result);
        exit;
    }

    function on_after_addedit_process($pid) {
        $result = array();
        $count = 0;
//            print_r($_POST['option']);exit;
        foreach ($_POST['option'] as $row) {
            
            $this->model->insert_or_dup_update("vote_option", array('vote_id' => $pid, 'name' => $row['name'], 'sort_order' => ++$count));
            $order[] = $count;
        }
        $this->model->getDB()->where(array('vote_id' => $pid));
        $this->model->getDB()->where_not_in('sort_order',$order);
        $this->model->getDB()->delete('vote_option');
//            print_r($this->model->getDB());
        return array_merge($result, (array) parent::on_after_addedit_process($id));
    }

}

?>
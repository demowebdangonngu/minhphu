<script>
    var module = '<?php echo $this->uri->segment(1); ?>';
    var base_url = '<?php echo base_url(); ?>';
    var img_path = '<?php echo $this->config->item('img_path') ?>';
<?php
$returnurl = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : base_url() . $this->uri->segment(1);
$id_field = $model->id_field;
$order_field = $model->order_field;
$status_field = $model->status_field;
?>
</script>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>"><?php
                        if ($module_title) {
                            echo $module_title;
                        } else {
                            echo $this->uri->segment(1);
                        };
                        ?></a> </li>
                <li class="active">Thêm / Sửa bài viết</li>
            </ul>
            <div class="row">
                <div class="col-sm-12">
                    <form class="form-horizontal" id="frm_add_edit" method="post" action="<?php echo base_url() . $this->uri->segment(1); ?>/addedit_process/" onsubmit="submit_form($(this), '<?php echo $return_url; ?>');
                            return false;"  >
                        <input  name="data[<?php echo $id_field; ?>]" type="hidden" id="txt_primary_id" value="<?php echo $obj->$id_field; ?>"/>
                        <section class="panel panel-default">
                            <header class="control-fixed panel-heading font-bold" data-top="49" >
                                <label style="line-height: 33px;">Thêm / Sửa nội dung</label>
                                <label class="pull-right">
                                    <button class="btn btn-primary" type="button" onclick="update_form($('#frm_add_edit'));" class="bntAll">Lưu lại</button>
                                    <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');
                                            return false;">Đóng</button>
                                </label>
                            </header>
                            <div class="panel-body">
                                <div id="post_form" class="col-sm-8 left">
                                    <div class="row">
                                        <div class="form-group has-success">
                                            <label class="col-sm-2 control-label">Tiêu đề</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="data[title]" class="form-control" value="<?php echo $obj->title; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row-content">
                                        <label class="control-label"><strong>Nội dung</strong></label>
                                        <textarea rows="8" name="data[content]" class="content form-control"><?php echo $obj->content; ?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-4" style="padding:0 45px;">
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label">Trạng thái</label>
                                        <div class="col-sm-6">
                                            <label class="switch">
                                                <input name="data[status]"  value="1" <?php if ($obj->$status_field == 1) echo 'checked=checked'; ?>  type="checkbox">
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <?php
                                    $countries = $this->load->model("countries/countries_model");
                                    $listcountries = $countries->get_countries();
                                    ?>
                                    <div class="form-group">
                                        <section class="panel panel-default">
                                            <header class="panel-heading font-bold">Thời Gian</header>

                                            <div class="input-group">
                                                <span class="input-group-addon" style="width: 100px;">Từ Ngày:</span>
                                                <?php
                                                $datefield = "from_date";
                                                $date = strtotime($obj->$datefield);
                                                if ($date <= 0)
                                                    $date = time();
                                                ?>

                                                <div id="date-time-panel-<?php echo $datefield; ?>">
                                                    <input type="text" value="<?php echo date("d/m/Y", $date); ?>" placeholder="dd/mm/yyyy" size="10" style="float: left; padding: 6px 3px; width: 82px; margin-right: 6px;" class="input-s-sm input-s dateimtepk form-control txt-yt-date">
                                                    <input type="number" min=1 max="23" value="<?php echo date("H", $date); ?>" placeholder="Giờ" class="form-control txt-yt-hour"  style="float: left; padding: 6px 3px; width: 40px;">
                                                    <span style="float: left; min-width: 0px; width: 10px ! important; padding: 6px 2px;" class="btn btn-s-md btn-default "> : </span>
                                                    <input type="number" min="0" max="59" value="<?php echo date("i", $date); ?>" placeholder="Phút" class="form-control txt-yt-min" name="timesche_min" style="float: left; padding: 6px 3px; width: 44px;">
                                                    <input type="hidden" class="txt-date-time-value" name="data[<?php echo $datefield; ?>]" value="<?php echo $obj->$datefield; ?>"/>
                                                </div>                                              
                                                <script>
                                                    $(document).ready(function () {
                                                        var datepk = $("#date-time-panel-<?php echo $datefield; ?> .dateimtepk");
                                                        datepk.datepicker({dateFormat: "d/m/yy"});
                                                        $("#date-time-panel-<?php echo $datefield; ?> .txt-yt-date,#date-time-panel-<?php echo $datefield; ?> .txt-yt-hour,#date-time-panel-<?php echo $datefield; ?> .txt-yt-min").change(function () {
                                                            var currentdate = datepk.datepicker("getDate");
                                                            $("#date-time-panel-<?php echo $datefield; ?> .txt-date-time-value").val(
                                                                    currentdate.getFullYear() + "-"
                                                                    + (currentdate.getMonth() + 1) + "-"
                                                                    + currentdate.getDate() + " "
                                                                    + $("#date-time-panel-<?php echo $datefield; ?> .txt-yt-hour").val() + ":"
                                                                    + $("#date-time-panel-<?php echo $datefield; ?> .txt-yt-min").val());
                                                        });
                                                    });
                                                </script>    
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon" style="width: 100px;">Đến Ngày:</span>
                                                <?php
                                                $datefield = "to_date";
                                                $date = strtotime($obj->$datefield);
                                                if ($date <= 0)
                                                    $date = time();
                                                ?>

                                                <div id="date-time-panel-<?php echo $datefield; ?>">
                                                    <input type="text" value="<?php echo date("d/m/Y", $date); ?>" placeholder="dd/mm/yyyy" size="10" style="float: left; padding: 6px 3px; width: 82px; margin-right: 6px;" class="input-s-sm input-s dateimtepk form-control txt-yt-date">
                                                    <input type="number" min=1 max="23" value="<?php echo date("H", $date); ?>" placeholder="Giờ" class="form-control txt-yt-hour"  style="float: left; padding: 6px 3px; width: 40px;">
                                                    <span style="float: left; min-width: 0px; width: 10px ! important; padding: 6px 2px;" class="btn btn-s-md btn-default "> : </span>
                                                    <input type="number" min="0" max="59" value="<?php echo date("i", $date); ?>" placeholder="Phút" class="form-control txt-yt-min" name="timesche_min" style="float: left; padding: 6px 3px; width: 44px;">
                                                    <input type="hidden" class="txt-date-time-value" name="data[<?php echo $datefield; ?>]" value="<?php echo $obj->$datefield; ?>"/>
                                                </div>                                              
                                                <script>
                                                    $(document).ready(function () {
                                                        var datepk = $("#date-time-panel-<?php echo $datefield; ?> .dateimtepk");
                                                        datepk.datepicker({dateFormat: "d/m/yy"});
                                                        $("#date-time-panel-<?php echo $datefield; ?> .txt-yt-date,#date-time-panel-<?php echo $datefield; ?> .txt-yt-hour,#date-time-panel-<?php echo $datefield; ?> .txt-yt-min").change(function () {
                                                            var currentdate = datepk.datepicker("getDate");
                                                            $("#date-time-panel-<?php echo $datefield; ?> .txt-date-time-value").val(
                                                                    currentdate.getFullYear() + "-"
                                                                    + (currentdate.getMonth() + 1) + "-"
                                                                    + currentdate.getDate() + " "
                                                                    + $("#date-time-panel-<?php echo $datefield; ?> .txt-yt-hour").val() + ":"
                                                                    + $("#date-time-panel-<?php echo $datefield; ?> .txt-yt-min").val());
                                                        });
                                                    });
                                                </script>    
                                            </div>

                                        </section>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label">Tổng thăm dò</label>
                                        <div class="col-sm-6">
                                            <input type="text" name="data[total]" class="form-control" value="<?php echo $obj->total; ?>" disabled="">
                                        </div>
                                    </div>
                                </div>
                                <div class="line line-dashed line-lg pull-in"></div>
                            </div>
                            <div class="form-group panel panel-default" style="width: 75%;margin-left: 1%;">
                                <header class="panel-heading font-bold">Option thăm dò</header>
                                <style>
                                    #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
                                    #sortable li { width: 165%;margin: 0 3px 3px 3px; padding-left: 1.5em; font-size: 1.4em;}
                                    #sortable li span { position: absolute; margin-left: -1.3em; }
                                </style>

                                <input type="button" onclick="addbt('#sortable')" class="btn btn-success btn-s-xs" id="btn" style=" background: url(<?php echo $this->config->item('admin_url') ?>/themes/admincp/images/addspe.png);min-width: 36px; min-height:36px;margin-top: 5px;margin-bottom: 5px;" />

                                <ul id="sortable">
                                    <?php
                                    $model->from('vote_option');
                                    $model->where(array("vote_id" => $obj->id));
                                    $model->order_by("sort_order");
                                    $vote_option = $model->get()->result();
                                    $count = 0;
                                    foreach ($vote_option as $row_voteop) {
                                        $count++;
                                        ?>
                                        <li class='list-group-item'>
                                            <div>
                                                <span class='pull-right' style='margin-left: 93%;margin-top: 5px;' >
                                                    <a href='#' onclick="$(this).parents('li').remove();">
                                                        <i class='fa fa-times icon-muted fa-fw'></i>
                                                    </a>
                                                </span>
                                                <span class='pull-left media-xs'>
                                                    <i class='fa fa-sort text-muted fa m-r-sm' style="margin-top: 8px;" ></i>
                                                </span>
                                                <div class='clear'>
                                                    <input type="text" class="form-control" style="width: 75%;float:left" name="option[<?php echo $row_voteop->sort_order; ?>][name]" value='<?php echo $row_voteop->name ?>'>
                                                    <span style='margin-left: 15px;margin-top: 5px;'>Lượt vote: <?php echo $row_voteop->count; ?></span>
                                                </div>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <script>
                                    $(function () {
                                        $("#sortable").sortable();
                                        $("#sortable").disableSelection();
                                    });
                                    var stt = <?php
                                    if ($vote_option) {echo $row_voteop->sort_order;} else {echo 0;}
                                    ?>;
                                    function addbt(id) {
                                        stt++;
                                        $html = "<li class='list-group-item'>\n\
                                <div>\n\
                                    <span class='pull-right' style='margin-left: 93%;margin-top: 8px;' >\n\
                                        <a href='#' onclick='$(this).parents(\"li\").remove();'><i class='fa fa-times icon-muted fa-fw'  ></i></a>\n\
                                    </span>\n\
                                    <span class='pull-left media-xs'><i class='fa fa-sort text-muted fa m-r-sm' style='margin-top: 8px;'></i></span>\n\
                                    <div class='clear'><input type='text' class='form-control' id='vote-option' style='width: 75%;float:left' name='' value=''>\n\
                                    <span style='margin-left: 15px;margin-top: 5px;'>Lượt vote: 0</span></div>\n\
                                </div>\n\
                                 </li>";
                                        $(id).append($html);
                                        //=======================
                                        $('#vote-option').attr('name', 'option[' + stt + '][name]');
                                        $('#vote-option').attr('id', 'vote-option-' + stt);
                                    }
                                </script>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-2">
                                    <input onclick="update_form($('#frm_add_edit'));" class="btn btn-primary" type="button" value="Lưu lại"/>
                                    <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');
                                            return false;">Đóng</button>
                                </div>
                            </div>

                        </section>
                    </form>
                </div>

            </div>

        </section>
    </section>
    <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
</section>
<script type='text/javascript'>
    function update_form_callback(data) {
        if (data.s
                tatus)
            window.location = base_url + module;
    }

</script>		
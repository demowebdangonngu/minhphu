<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/ControllerAdmin.php";

class contact extends YT_ControllerAdmin {

    /**
     *
     * @var Test_model 
     */
    public $model;

    function __construct() {
        // pr($this->uri);die;
        parent::__construct();
        $this->model = $this->load->model('contact_model');
        $this->check_access('contact_access');
    }

    function addedit($id = 0) {
        $data_post = $this->input->post();
        if($data_post){
            $id = $this->model->store_data($this->input->post("data"));
            if ($id) {
                $this->on_after_addedit_process($id);
            }
            redirect("contact", 'refresh');
        }
        parent::addedit($id);
    }
    public function index() {
        return parent::index();
    }
    public function quick_trash() {
        $return = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $this->uri->segment(1);
        $ids = $this->input->get('ids');
        $this->model->delete_id($ids);
        $this->model->on_data_change();
//        if ($ids)
//            logs("Xóa: " . implode(",", $ids));
        redirect($return);
    }

//    function on_after_addedit_process($pid) {
//        $date = date('Y-m-d',time());
//        $result = array();
//        $admin_id = $this->session->userdata('admin_id');
//        $data = array(
//            'admin_id' => $admin_id,
//            'public_date' => $date,
//        );
//        $this->model->where('id',$pid);
//        $this->model->update($data);
//        return array_merge($result, (array) parent::on_after_addedit_process($pid));
//    }
}

?>
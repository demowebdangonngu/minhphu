<section id="content">
    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>"><?php
                        if ($module_title) {
                            echo $module_title;
                        } else {
                            echo $this->uri->segment(1);
                        };
                        ?></a> </li>
                <li class="active">Xem liên hệ</li>
            </ul>
            <div class="row">
                <div class="col-sm-12">
                    <form id="form" name="form" method="post" action="" class="form-horizontal" enctype="multipart/form-data">
                        <section class="panel panel-default">
                            <header class="control-fixed panel-heading font-bold" data-top="49" >
                                <label style="line-height: 33px;">Xem liên hệ</label>
                                <label style="line-height: 33px;" class="message_alert"></label>
                                <label class="pull-right">
                                    <input type="submit" class="btn btn-primary" value="Lưu lại"/>
                                    <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');
                                            return false;">Đóng</button>
                                </label>
                            </header>
                            <div class="panel-body">
                                <div id="post_form" class="col-sm-8 left">
                                    <input type="hidden" type="text" name="data[status]" class="form-control" value="1" hidden="">
                                    <style>

                                        .panel-heading.control-fixed {
                                            height: 43px;
                                            padding: 4px 17px;
                                        }
                                    </style>
                                    <script>
                                        $(".scrollable").scroll(function () {
                                            var elem = $('.control-fixed');//alert("sdfsdf");
                                            if (!elem.attr('data-top')) {
                                                if (elem.hasClass('navbar-fixed-top'))
                                                    return;
                                                var offset = elem.offset()
                                                elem.attr('data-top', offset.top);
                                            }
                                            if (elem.attr('data-top') <= $(this).scrollTop())
                                                elem.addClass('navbar-fixed-top');
                                            else
                                                elem.removeClass('navbar-fixed-top');
                                        });

                                    </script>
                                    <label id="msg_error_msg" style="color:#f00"></label>
                                    <div class="form-group has-success">
                                        <label class="col-sm-2 control-label">Tên</label>
                                        <div class="col-sm-10">
                                            <input  required minlength="3" type="text" name="data[name]" class="form-control" value="<?php echo $obj->name; ?>" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <label class="col-sm-2 control-label">Số điện thoại</label>
                                        <div class="col-sm-10">
                                            <input required minlength="3" type="text" name="data[phone]" class="form-control" value="<?php echo $obj->phone; ?>" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="data[email]" class="form-control" value="<?php echo $obj->email; ?>" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group has-success">
                                        <label class="col-sm-2 control-label">Tiêu đề</label>
                                        <div class="col-sm-10">
                                            <input type="text" name="data[title]" class="form-control" value="<?php echo $obj->title; ?>" disabled="">
                                        </div>
                                    </div> 
                                    <div class="form-group has-success">
                                        <label class="col-sm-2 control-label">Nội dung</label>
                                        <div class="col-sm-10">
                                            <textarea type="text" name="data[content]" class="form-control" disabled=""><?php echo $obj->content; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="line line-dashed line-lg pull-in"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <input type="submit" class="btn btn-primary" value="Lưu lại"/>
                                            <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');
                                                    return false;">Đóng</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>




                    </form>
                </div>
            </div>  
        </section>


    </section>
</section>
<input id="old-data-form" type="hidden" value=""/>

<script>
    function on_form_submit() {
        var form = $("#old-data-form");
        if ($("select.data-form-status").val() == 'publish') {

            if ($("#title").val().length < 3) {
                alert("Tên bài viết quá ngắn");
                return false;
            }
            if ($("#summary_short").val().length < 20) {
                alert("Tóm tắt ngắn, ngắn hơn 20 ký tự");
                return false;
            }
            if ($("select.data-form-category").val() == null) {
                alert("Vui lòng chọn danh mục");
                return false;
            }
            //alert($("input.chose_img_radio:checked").val());
            if ($("input.chose_img_radio:checked").val() == null) {
                alert("Vui lòng chọn hình đại diện");
                return false;
                ;
            }
            /*if ($("input.form-data-hot:checked").val() == 1 && $("input.form-data-hot-image").val().length == 0) {
             alert("Vui lòng chọn ảnh của tin hot");
             return false;
             ;
             }
             if ($("input.form-data-banner:checked").val() == 1 && $("input.form-data-banner-image").val().length == 0) {
             alert("Vui lòng chọn ảnh đăng banner");
             return false;
             ;
             }*/

        }
    }
    $(".scrollable").scroll(function () {
        var elem = $('.control-fixed');//alert("sdfsdf");
        if (!elem.attr('data-top')) {
            if (elem.hasClass('navbar-fixed-top'))
                return;
            var offset = elem.offset()
            elem.attr('data-top', offset.top);
        }
        if (elem.attr('data-top') <= $(this).scrollTop())
            elem.addClass('navbar-fixed-top');
        else
            elem.removeClass('navbar-fixed-top');
    });
    function oncount($idPut, $idCount) {
        if ($($idPut).val().length >= $($idCount).data('count'))
        {
            //alert("Bạn đã nhập quá ký tự cho phép");
            $($idPut).val($($idPut).val().substring(0, $($idCount).data('count')));
        }
        if ($idCount != $idPut) {
            $($idCount).val($($idCount).data('count') - $($idPut).val().length);
        }
    }

    //*************end of upload thumb*******************************/

</script>
<script>
    $(document).ready(function ()
    {
        // activate Nestable for list 1
        if ($('#menus-top').length)
        {
            $('#menus-top').nestable({
                group: 0,
                maxDepth: 2
            }).on('change', updateOutputTop);
        }
    });
</script>
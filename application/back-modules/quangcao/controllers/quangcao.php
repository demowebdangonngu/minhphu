<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require_once APPPATH."third_party/YT/ControllerAdmin.php";
class quangcao extends YT_ControllerAdmin {
	/**
     *
     * @var Test_model 
     */
	public $model;
        /**
         *
         * @var Genre_model
         */
        public $genre;
	function __construct()	{
		parent::__construct();	
		$this->model=$this->load->model('quangcao_model');	
	}
        function addedit_process() {
            if($_POST['data']['link']) $_POST['data']['link']=  remove_accent ($_POST['data']['link']);
            if(!$_POST['data']['id']){
                $this->model->getDB()->select("max(sort_order) as order_max");
                $this->model->getDB()->from($this->model->table_name);
                $obj=current($this->model->getDB()->get()->result());
                $_POST['data']['sort_order']=intval($obj->order_max)+1;
                
            }
            if (isset($_FILES['img']['name']) && $_FILES['img']['name'] != "") {
                move_uploaded_file($_FILES['img']['tmp_name'], '../upload/images/images/' . time() . "_" . $_FILES['img']['name']);
                $_POST['data']['image'] = time() . "_" . $_FILES['img']['name'];
            }
//            $_POST['data']['idAdmin'] = $this->session->userdata('admin_id');
            parent::addedit_process();
        }
        function on_after_addedit_process($id) {
            redirect("quangcao", 'refresh');
            //if(!$_POST['data']['show']) $_POST['data']['show']=0;
            //if(!$_POST['data']['status']) $_POST['data']['status']=0;
            parent::on_after_addedit_process($id);
            
        }
        function index() {
            if(isset($_POST['menu_order'])){
//                echo "<pre>";
//                print_r(json_decode($_POST['cat_order']));
//                echo "</pre>";
//                exit;
                $array=json_decode($_POST['menu_order']);
                if(count($array)){
                    $this->model->store_tree($array);
                    $this->model->on_data_change();
                }
                echo json_encode(array('status'=>1));exit;
            }
            if(intval($_GET['id'])){
                $this->model->where['id']=intval($_GET['id']);
                $this->data['obj']=$this->model->get_object_by_where();
            }
            $this->model->order_by('sort_order');
            $this->data['list'] = $this->model->get()->result();
//            $this->addedit_process())
            parent::index();
        }
}
 ?>
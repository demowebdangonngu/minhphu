<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class urlalias_model extends YT_AModel {
       function __construct() {
            parent::__construct();
            
         $this->fields=array(
            'id'=>'1',
            'realurl'=>'1',
            'aliasurl'=>'1',
            'title'=>'1',
            'description'=>'1',
            'status'=>'1',
            'language'=>'1',
            'create_date'=>'1',
            'modify_date'=>'1',
             'keyword'=>'1',
        );
         $this->table_name='urlalias';
         $this->id_field='id';
         $this->status_field='status';
         
        }
        function on_data_change() {
           /* @var $caches MY_Caches */
           $caches=$this->load->library('Caches');
           $hkey=array("html_cache","urlalias","posts");
            $caches->flush_cache($hkey);
           //parent::on_data_change();
       } 
	
}
?>
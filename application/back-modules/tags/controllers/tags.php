<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require_once APPPATH."third_party/YT/ControllerAdmin.php";
class Tags extends YT_ControllerAdmin {
	/**
     *
     * @var Test_model 
     */
	public $model;
        /**
         *
         * @var Tags_model
         */
        public $genre;
	function __construct()	{
		parent::__construct();	
		$this->model=$this->load->model('Tags_model');	
	}
        function addedit_process() {
            $_POST['data']['slug']=  remove_accent($_POST['data']['name']);
            parent::addedit_process();
        }
        function on_after_addedit_process($id) {
            
            parent::on_after_addedit_process($id);
            
        }
        function list_keyword(){
            $list= $this->model->search($_GET['q'],20);
            $result=array();
            foreach ($list as $tag){
                $result[]=$tag->name;
            }
            echo json_encode(array('list'=>$result));exit;
        }
}
 ?>
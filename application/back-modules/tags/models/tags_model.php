<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class Tags_model extends YT_AModel {
    var $max_lengh=80;
       function __construct() {
            parent::__construct();
            /*
  Full texts 	id 	name 	slug 	count 	create_date 	modify_date 	status  
             
             */
         $this->fields=array(
             'id'=>'1',
            'name'=>'1',
             'slug'=>1,
             'count'=>1,
             'create_date'=>1,
             'modify_date'=>1,
             'status'=>1,
        );
         $this->fields_lang=array();
         $this->table_name='tags';
         $this->table_name_lang='';
         $this->id_field='id';
         $this->id_field_lang='';
         $this->status_field='status';
         $this->cat_field='';
         $this->order_field='';
         $this->image_field=array();
         ///$this->id_lang_rel=$this->id_field;
         
        }
        function get_tag_by_object($obj_id=0,$module='posts'){
            $this->where(array('obj_id'=>$obj_id,'module'=>$module));
            $this->join('tags_content',$this->table_name."."."id=tags_content.tag_id");
            return $this->get()->result();
        }
        function get_top_tag($module='posts'){
            //$this->getDB()->where(array('modify_date'>date("Y-m-d H:i",time()-86400*30)));
            $this->getDB()->from($this->table_name);
            $this->getDB()->where(array('status !='=>2));
            $this->getDb()->order_by("modify_date","desc");
            $this->getDb()->limit(40);
            return $this->getDb()->get()->result();
        }
		
		function get_top_tag_table($module='tags'){
            //$this->getDB()->where(array('modify_date'>date("Y-m-d H:i",time()-86400*30)));
            $this->getDB()->from($module);
            $this->getDB()->where(array('status !='=>2));
            $this->getDb()->order_by("modify_date","desc");
            $this->getDb()->limit(40);
            return $this->getDb()->get()->result();
        }
		
        function insert_tags_for_object($tabstring,$obj_id=0,$module='posts',$separe=","){
            if($obj_id){
                $this->getDB()->delete('tags_content',array('obj_id'=>$obj_id,'module'=>$module));
            }
            $str=explode($separe, preg_replace("/\s+/"," ",$tabstring));
			$stt =0;
            foreach ($str as $st){
				$stt++;
                if(strlen($st)<=$this->max_lengh){
                    $id=$this->insert_tag($st);
                    if($obj_id&&$id){
                        $this->getDB()->insert('tags_content',array('tag_id'=>$id,'module'=>$module,'obj_id'=>$obj_id,'sort_order'=>$stt));
                    }
                }
            }
            
        }
        function insert_tag($tagname){
            
            $st=trim($tagname);
                $item=array();
                $item['slug']=remove_accent($st);

                if(!$item['slug']) return false;
                $item['name']=$st;
            $this->where['slug']=$item['slug'];
            $obj=$this->get_object_by_where();
            if($obj->id) {
                $this->store_dataa(array('id'=>$obj->id));
                return $obj->id;
            }
            $item['create_date']=date("Y-m-d H:i",time());
            $item['modify_date']=date("Y-m-d H:i",time());
            $item['status']=1;
            return $this->store_dataa($item);
        }
        public function search($key,$limit=20){
            $search=remove_accent(preg_replace("/\s+/"," ",$key));
            $this->getDB()->from($this->table_name);
            $this->getDB()->like('slug', $search);
            $this->getDB()->limit($limit);
            return $this->getDB()->get()->result();
        }
        function store_dataa($array){
            $data=array();
            foreach ($this->fields as $key=>$value){
                isset($array[$key])?$data[$key]=$array[$key]:"";
            }
            $returnid=0;
           // print_r($data);exit;
            if(!$array[$this->id_field]){
                if($this->fields['create_date'])  {
                    //$data['modify_date']=  'CURRENT_TIMESTAMP';
                    $this->getDB()->set('create_date',date("Y-m-d H:i:s",time()));
                }
                 if($this->fields['modify_date'])  {
                    $this->getDB()->set('modify_date',date("Y-m-d H:i:s",time()));
                }
                $this->getDB()->insert($this->table_name, $data);
                
                $returnid = $this->getDB()->insert_id();
            }else{
                $this->getDB()->where(array($this->id_field=>$array[$this->id_field]));
                if($this->fields['modify_date'])  {
                    //$data['modify_date']=  'CURRENT_TIMESTAMP';
                    $this->getDB()->set('modify_date',date("Y-m-d H:i:s",time()));
                }
                $this->getDB()->update($this->table_name,$data);
                $returnid=$array[$this->id_field];
            }
            if($array['_mlang']&&$this->table_name_lang){
                foreach ($array['_mlang'] as $idx=> $lang){
                    $lang['lang_id']=$idx;
                    $this->store_data_lang($returnid,$lang);
                }
            }
            $this->on_data_change($returnid);
            return $returnid;
                
        }
}
?>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class banner_model extends YT_AModel {

    function __construct() {
        parent::__construct();
        $this->fields = array(
            'id' => 'PRI',
            'name' => 1,
            'image' => 1,
            'sort_order' => 1,
            'status' => 1,
        );
        $this->fields_lang = array();
        $this->table_name = 'attach';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->id_field_lang = '';
        $this->status_field = 'status';
        $this->cat_field = '';
        $this->order_field = 'sort_order';
        $this->image_field = array();
        ///$this->id_lang_rel=$this->id_field;
    }

    function store_tree($tree_array) {
        $sort = 1;
        foreach ($tree_array as $cate) {
            $this->getDB()->update($this->table_name, array('sort_order' => $sort++), array('id' => $cate->id));
        }
    }

}

?>
<script>
    var module = '<?php echo $this->uri->segment(1); ?>';
    var base_url = '<?php echo base_url(); ?>';
</script>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>"><?php
                        if ($module_title) {
                            echo $module_title;
                        } else {
                            echo $this->uri->segment(1);
                        };
                        ?></a></li>
                <!--<li class="active">Static table</li>-->
            </ul>
            <div class="row">
                <div class="col-sm-6">
                    <?php
                    /* @var $menusmodel Menus_model */
                    $menusmodel = $this->load->model("Menus_model");
                    isset($_GET['trash']) ? $status_array = array(0, 1, 2) : $status_array = array(0, 1);
                    $menus = $menusmodel->get_tree($status_array);
					// print_r($menus);exit;
                    ?>
                    <form id="menu-sorter-form-top" data-validate="parsley" method="post" action="<?php echo base_url() . $this->uri->segment(1); ?>" >
                        <input type="hidden" name="menu_order" id="menu-top-order" value="">
                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <span class="h4">Danh sách thể loại</span>
                                <ul class="nav nav-tabs pull-right">
                                    <li <?php echo (!$this->input->get("trash") ? "class='active'" : ""); ?>><a href="<?php echo base_url() . $this->uri->segment(1); ?>" ><i class="fa fa-file-text text-default"></i>&nbsp; Menus</a></li>
                                    <li <?php
                                    echo ($this->input->get("trash") ? "class='active'" : "");
                                    ;
                                    ?>><a href="<?php echo base_url() . $this->uri->segment(1) . "?trash=1"; ?>" ><i class="fa fa-trash-o text-default"></i>&nbsp; Hiện menus đã xóa</a></li>
                                </ul>
                            </header>
                            <header class="panel-heading">
                                <span class="h4">Menu top</span>
                            </header>
                            <div class="panel-body">
                                <div class="dd" id="menus-top" data-output="">
                                    <ol class="dd-list">
                                        <?php
										
                                        if (isset($menus['top'])) {
//                                            print_r($menus['top']);exit;
                                            foreach ($menus['top'] as $cat) {
                                                ?>
                                                <li class="dd-item parent_category <?php echo "status_" . $cat->status; ?>" data-id="<?php echo $cat->id; ?>">
                                                    <div class="dd-handle"><?php echo $cat->title?$cat->title:$cat->name; ?></div>
                                                    <span class="pull-right" style="position: absolute;right: 0px;top: 6px;">
                                                        <?php ?>
                                                        <a title="Chỉnh sửa danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '?id=' . $cat->id; ?>"><i class="fa fa-pencil icon-muted fa-fw m-r-xs"></i></a>
                                                        <?php ?>
                                                        <?php if (!count($cat->children)) { ?>
                                                            <?php if ($cat->status != 2) { ?>
                                                                <a href="<?php echo base_url() . $this->uri->segment(1) . '/quick_trash?ids=' . $cat->id; ?>"><i class="fa fa-times icon-muted fa-fw"></i></a>
                                                            <?php } else { ?>
                                                                <a title="Hiện danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '/update_status/' . $cat->id . '?status=1&return=1' ?>"><i class="fa icon-muted  fa-fw  fa-check-circle-o"></i></a>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </span>
                                                    <?php
                                                    if (count($cat->children)) {
                                                        ?>
                                                        <ol class="dd-list">
                                                            <?php
                                                            foreach ($cat->children as $cat2) {
                                                                ?>
                                                                <li class="dd-item <?php echo "status_" . $cat2->status; ?>" data-id="<?php echo $cat2->id; ?>">
                                                                    <div class="dd-handle"><?php echo $cat2->title?$cat2->title:$cat2->name; ?></div>
                                                                    <span class="pull-right" style="position: absolute;right: 0px;top: 6px;">

                                                                        <a title="Chỉnh sửa thể loại" href="<?php echo base_url() . $this->uri->segment(1) . '?id=' . $cat2->id; ?>"><i class="fa fa-pencil icon-muted fa-fw m-r-xs"></i></a>
                                                                        <?php if ($cat2->status != 2) { ?>
                                                                            <a href="<?php echo base_url() . $this->uri->segment(1) . '/quick_trash?ids=' . $cat2->id; ?>"><i class="fa fa-times icon-muted fa-fw"></i></a>
                                                                        <?php } else { ?>
                                                                            <a title="Hiện danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '/update_status/' . $cat2->id . '?status=1&return=1' ?>"><i class="fa icon-muted  fa-fw  fa-check-circle-o"></i></a>

                                                                        <?php } ?>
                                                                        <?php ?>
                                                                    </span>
                                                                </li>
                                                                <?php
                                                                if (count($cat2->children)) {
                                                                    ?>
                                                                    <ol class="dd-list">
                                                                        <?php
                                                                        foreach ($cat2->children as $cat3) {
                                                                            ?>
                                                                            <li class="dd-item <?php echo "status_" . $cat3->status; ?>" data-id="<?php echo $cat3->id; ?>">
                                                                                <div class="dd-handle"><?php echo $cat3->title?$cat3->title:$cat3->name; ?></div>
                                                                                <span class="pull-right" style="position: absolute;right: 0px;top: 6px;">

                                                                                    <a title="Chỉnh sửa thể loại" href="<?php echo base_url() . $this->uri->segment(1) . '?id=' . $cat3->id; ?>"><i class="fa fa-pencil icon-muted fa-fw m-r-xs"></i></a>
                                                                                    <?php if ($cat3->status != 2) { ?>
                                                                                        <a href="<?php echo base_url() . $this->uri->segment(1) . '/quick_trash?ids=' . $cat3->id; ?>"><i class="fa fa-times icon-muted fa-fw"></i></a>
                                                                                    <?php } else { ?>
                                                                                        <a title="Hiện danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '/update_status/' . $cat3->id . '?status=1&return=1' ?>"><i class="fa icon-muted  fa-fw  fa-check-circle-o"></i></a>

                                                                                    <?php } ?>
                                                                                    <?php ?>
                                                                                </span>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </ol>  
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ol>
                                                        <?php
                                                    }
                                                    ?>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ol>
                                </div>

                            </div>
                            <footer class="panel-footer text-right bg-light lter">
                                <?php ?>
                                <input type="button" onclick="update_form($('#menu-sorter-form-top'), update_menu_top_callback);
                                        return false;" class="btn btn-success btn-s-xs" value="Cập nhật thứ tự"/>
                                       <?php ?>
                            </footer>
                        </section>
                        <style>
                            .dd li.status_0 .dd-handle{
                                color: #bbb;
                                background: #F7F7F7;
                            }
                            .dd li.status_2 .dd-handle{
                                color: #bbb;
                                background: #FF6666;
                            }


                        </style>
                    </form>
                    <form id="menu-sorter-form-bottom" data-validate="parsley" method="post" action="<?php echo base_url() . $this->uri->segment(1); ?>" >
                        <input type="hidden" name="menu_order" id="menu-top-bottom" value="">
                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <span class="h4">Menu bottom</span>
                            </header>
                            <div class="panel-body">
                                <div class="dd" id="menus-bottom" data-output="">
                                    <ol class="dd-list">
                                        <?php
                                        if (isset($menus['bottom'])) {

                                            foreach ($menus['bottom'] as $cat) {
                                                ?>
                                                <li class="dd-item parent_category <?php echo "status_" . $cat->status; ?>" data-id="<?php echo $cat->id; ?>">
                                                    <div class="dd-handle"><?php echo $cat->title?$cat->title:$cat->name; ?></div>
                                                    <span class="pull-right" style="position: absolute;right: 0px;top: 6px;">
                                                        <?php ?>
                                                        <a title="Chỉnh sửa danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '?id=' . $cat->id; ?>"><i class="fa fa-pencil icon-muted fa-fw m-r-xs"></i></a>
                                                        <?php ?>
                                                        <?php if (!count($cat->children)) { ?>
                                                            <?php if ($cat->status != 2) { ?>
                                                                <a href="<?php echo base_url() . $this->uri->segment(1) . '/quick_trash?ids=' . $cat->id; ?>"><i class="fa fa-times icon-muted fa-fw"></i></a>
                                                            <?php } else { ?>
                                                                <a title="Hiện danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '/update_status/' . $cat->id . '?status=1&return=1' ?>"><i class="fa icon-muted  fa-fw  fa-check-circle-o"></i></a>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </span>
                                                    <?php
                                                    if (count($cat->children)) {
                                                        ?>
                                                        <ol class="dd-list">
                                                            <?php
                                                            foreach ($cat->children as $cat2) {
                                                                ?>
                                                                <li class="dd-item <?php echo "status_" . $cat2->status; ?>" data-id="<?php echo $cat2->id; ?>">
                                                                    <div class="dd-handle"><?php echo $cat2->title?$cat2->title:$cat2->name; ?></div>
                                                                    <span class="pull-right" style="position: absolute;right: 0px;top: 6px;">

                                                                        <a title="Chỉnh sửa thể loại" href="<?php echo base_url() . $this->uri->segment(1) . '?id=' . $cat2->id; ?>"><i class="fa fa-pencil icon-muted fa-fw m-r-xs"></i></a>
                                                                        <?php if ($cat2->status != 2) { ?>
                                                                            <a href="<?php echo base_url() . $this->uri->segment(1) . '/quick_trash?ids=' . $cat2->id; ?>"><i class="fa fa-times icon-muted fa-fw"></i></a>
                                                                        <?php } else { ?>
                                                                            <a title="Hiện danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '/update_status/' . $cat2->id . '?status=1&return=1' ?>"><i class="fa icon-muted  fa-fw  fa-check-circle-o"></i></a>

                                                                        <?php } ?>
                                                                        <?php ?>
                                                                    </span>
                                                                </li>
                                                                <?php
                                                                if (count($cat2->children)) {
                                                                    ?>
                                                                    <ol class="dd-list">
                                                                        <?php
                                                                        foreach ($cat2->children as $cat3) {
                                                                            ?>
                                                                            <li class="dd-item <?php echo "status_" . $cat3->status; ?>" data-id="<?php echo $cat3->id; ?>">
                                                                                <div class="dd-handle"><?php echo $cat3->title?$cat3->title:$cat3->name; ?></div>
                                                                                <span class="pull-right" style="position: absolute;right: 0px;top: 6px;">

                                                                                    <a title="Chỉnh sửa thể loại" href="<?php echo base_url() . $this->uri->segment(1) . '?id=' . $cat3->id; ?>"><i class="fa fa-pencil icon-muted fa-fw m-r-xs"></i></a>
                                                                                    <?php if ($cat3->status != 2) { ?>
                                                                                        <a href="<?php echo base_url() . $this->uri->segment(1) . '/quick_trash?ids=' . $cat3->id; ?>"><i class="fa fa-times icon-muted fa-fw"></i></a>
                                                                                    <?php } else { ?>
                                                                                        <a title="Hiện danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '/update_status/' . $cat3->id . '?status=1&return=1' ?>"><i class="fa icon-muted  fa-fw  fa-check-circle-o"></i></a>

                                                                                    <?php } ?>
                                                                                    <?php ?>
                                                                                </span>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                    </ol>  
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ol>
                                                        <?php
                                                    }
                                                    ?>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ol>
                                </div>

                            </div>
                            <footer class="panel-footer text-right bg-light lter">
                                <?php ?>
                                <input type="button" onclick="update_form($('#menu-sorter-form-bottom'), update_menu_bottom_callback);
                                        return false;" class="btn btn-success btn-s-xs" value="Cập nhật thứ tự"/>
                                       <?php ?>
                            </footer>
                        </section>
                        <style>
                            .dd li.status_0 .dd-handle{
                                color: #bbb;
                                background: #F7F7F7;
                            }
                            .dd li.status_2 .dd-handle{
                                color: #bbb;
                                background: #FF6666;
                            }


                        </style>
                    </form>

                </div>
                <?php ?>
                <div class="col-sm-6">
                    <form  method="post" id='add-insert-menu-frm' action="<?php echo base_url() . $this->uri->segment(1); ?>/addedit_process/" onsubmit="update_form($(this), update_menu_callback);
                            return false;">
                        <input type="hidden" name="data[id]" value="<?php echo $obj->id; ?>">
                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <span class="h4"><?php if ($obj->id) { ?>Sửa<?php } else { ?>Thêm<?php } ?> danh mục</span>
                            </header>
                            <div class="panel-body">
                                <div id="msg_error_msg" style="color:#f00;"></div>
                                <div class="form-group">
                                    <label>Chọn vị trí</label>
                                    <select name="data[position]" id="menus_parent_position" class="form-control">
                                        <option value="top" <?php
                                        if ($obj->position == 'top') {
                                            echo "selected";
                                        };
                                        ?>>Top</option>
                                        <option value="bottom" <?php
                                        if ($obj->position == 'bottom') {
                                            echo "selected";
                                        };
                                        ?>>bottom</option>
                                    </select>
                                </div>
                                <div class="form-group parent-select-category"  id="menus_parent_top_panel">
                                    <label>Menu cha</label>
                                    <select name="data[parent]" id="menus_parent_top" class="form-control  ">
                                        <option value="0" <?php
                                        if (!$obj->parent) {
                                            echo "selected";
                                        };
                                        ?>>Gốc</option>
                                                <?php
                                                foreach ($menus['top'] as $cat) {
                                                    ?>
                                            <option value="<?php echo $cat->id; ?>" <?php if ($obj->parent == $cat->id) { ?>selected<?php } ?>> <?php echo $cat->title?$cat->title:$cat->name; ?></option>

                                            <?php
                                            if (count($cat->children)) {
                                                foreach ($cat->children as $cat2) {
                                                    ?>
                                                    <option value="<?php echo $cat2->id; ?>" <?php if ($obj->parent == $cat2->id) { ?>selected<?php } ?>>&nbsp;|-- <?php echo $cat2->title?$cat2->title:$cat2->name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>

                                        <?php }
                                        ?>
                                    </select>
                                </div>
                                <script>
                                    // $("#menus_parent_position").change(function () {
                                        // var current = this.value;

                                        // $(".parent-select-category").hide();
                                        // $(".parent-select-category select").attr("name", "");
                                        // $("#menus_parent_" + current + "_panel").show();
                                        // $("#menus_parent_" + current).attr("name", "data[parent]");
                                    // }).change();
                                </script>
                                <div class="form-group">
                                    <label>Chọn kiểu</label>
                                    <select name="data[type]" id="menus_type" class="form-control">
                                        <option value="category" <?php
                                        if ($obj->type == 'category') {
                                            echo "selected";
                                        };
                                        ?>>Danh mục</option>
                                        <option value="link" <?php
                                        if ($obj->type == 'link') {
                                            echo "selected";
                                        };
                                        ?>>Liên kết</option>


                                    </select>
                                </div>
                                <div class="form-group is_category_row" id="menu-category-select">
                                    <label>Danh mục</label>
                                    <select name="data[value]" id="menus_parent_bottom" class="form-control">
                                        <?php
                                        /* @var $catemodel Categories_model */
                                        $catemodel = $this->load->model("categories/categories_model");
                                        isset($_GET['trash']) ? $status_array = array(0, 1, 2) : $status_array = array(0, 1);
                                        $categories = $catemodel->get_tree($status_array);
                                        ?>
                                        <option disabled="disabled" value="" style="font-size: 19px;color: rgb(124, 0, 255);">Sản phẩm</option>
                                        <?php
                                        foreach ($categories as $cat) {
                                            if($cat->type == 2){
                                            ?>
                                            <option value="<?php echo $cat->id; ?>" <?php if ($obj->value == $cat->id) { ?>selected<?php } ?>> <?php echo $cat->name; ?></option>

                                            <?php
                                            if (count($cat->children)) {
                                                foreach ($cat->children as $cat2) {
                                                    ?>
                                                    <option value="<?php echo $cat2->id; ?>" <?php if ($obj->value == $cat2->id) { ?>selected<?php } ?>>&nbsp;|-- <?php echo $cat2->name; ?></option>
                                                    <?php
                                                    }
                                                }
                                            }
                                        }?>
                                        <option disabled="disabled" value="" style="font-size: 19px;color: rgb(124, 0, 255);">Bài viết</option>
                                        <?php
                                        foreach ($categories as $cat) {
                                            if($cat->type == 1){
                                            ?>
                                            
                                            <option value="<?php echo $cat->id; ?>" <?php if ($obj->value == $cat->id) { ?>selected<?php } ?>> <?php echo $cat->name; ?></option>

                                            <?php
                                            if (count($cat->children)) {
                                                foreach ($cat->children as $cat2) {
                                                    ?>
                                                    <option value="<?php echo $cat2->id; ?>" <?php if ($obj->value == $cat2->id) { ?>selected<?php } ?>>&nbsp;|-- <?php echo $cat2->name; ?></option>
                                                    <?php
                                                    }
                                                }
                                            }  
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group is_link_row">
                                    <label>Đường dẫn</label>
                                    <input maxlength="1024" type="text" name="data[link]" class="form-control" value="<?php echo $obj->link; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Kiểu click</label>
                                    <select name="data[target]" id="menus_type" class="form-control">
                                        <option value="_self" <?php
                                        if ($obj->target == '_self') {
                                            echo "selected";
                                        };
                                        ?>>Tại trang</option>
                                        <option value="_blank" <?php
                                        if ($obj->target == '_blank') {
                                            echo "selected";
                                        };
                                        ?>>Mở tab mới</option>
                                    </select>
                                </div>
                                <script>
                                    // $("#menus_type").change(function () {
                                        // if (this.value == 'category')
                                        // {
                                            // $(".is_category_row").show();
                                            // $(".is_link_row").hide();
                                            // $(".is_videocategory_row").hide();
                                        // }
                                        // else
                                        // {
                                            // if (this.value == 'link')
                                            // {
                                                // $(".is_category_row").hide();
                                                // $(".is_link_row").show();
                                                // $(".is_videocategory_row").hide();
                                            // }
                                        // }
                                    // }).change();
                                </script>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="margin-left: -13px;">Hoạt động</label>
                                    <div class="col-sm-7">
                                        <label class="switch">
                                            <input type="checkbox" value="1" name="data[status]" <?php if ($obj->status || !$obj->id) { ?>checked<?php } ?>>
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <footer class="text-right bg-light lter">
                                    <input type="button" onclick="update_form($('#add-insert-menu-frm'), update_form_callback);
                                            return false;" class="btn btn-success btn-s-xs" value="<?php if (isset($obj->id)) { ?>Cập nhật<?php } else { ?>Thêm<?php } ?>"/>
                                </footer>
                            </div>
                        </section>
                    </form>
                </div>
            </div>
        </section>
    </section>
    <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
</section>
<script>
    $(document).ready(function ()
    {
        // activate Nestable for list 1
        if ($('#menus-top').length)
        {
            $('#menus-top').nestable({
                group: 0,
                maxDepth: 2
            }).on('change', updateOutputTop);
        }
        if ($('#menus-bottom').length)
        {
            $('#menus-bottom').nestable({
                group: 0,
                maxDepth: 1
            }).on('change', updateOutputBottom);
        }
    });
    var topchange = false;
    var bottomchange = false;
    var updateOutputTop = function (e) {
        var list = e.length ? e : $(e.target);
        //alert(window.JSON.stringify(list.nestable('serialize')));
        topchange = true;
        $("#menu-top-order").val(window.JSON.stringify(list.nestable('serialize')));
    };
    var updateOutputBottom = function (e) {
        var list = e.length ? e : $(e.target);
        //alert(window.JSON.stringify(list.nestable('serialize')));
        bottomchange = true;
        $("#menu-top-bottom").val(window.JSON.stringify(list.nestable('serialize')));
    };
    function update_menu_top_callback(data) {
        alert('đã cập nhật thành công!');
        topchange = false;
    }
    function update_menu_bottom_callback(data) {
        alert('đã cập nhật thành công!');
        bottomchange = false;
    }
    function update_form_callback(data) {
        if (data.status)
            window.location = base_url + module;
    }
    window.onbeforeunload = function () {
        if (topchange || bottomchange)
            return 'Bạn vừa thay đổi vị trí của các danh mục bên trái, nếu thoát trang thì các vị trí sẽ không được lưu lại!';
    };
</script>
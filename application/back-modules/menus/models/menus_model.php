<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class Menus_model extends YT_AModel {

    function __construct() {
        parent::__construct();
        /*
          id
          name
          slug
          parent
          ORDER
          SHOW
          STATUS
          TYPE
          forwc

         */
        $this->fields = array(
            'id' => 'PRI',
            'type' => 1,
            'value' => 1,
            'title' => 1,
            'link' => 1,
            'target' => 1,
            'sort_order' => 1,
            'parent' => 1,
            'position' => 1,
            'status' => 1,
            'of_value' => 1,
        );
        $this->fields_lang = array();
        $this->table_name = 'menu';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->id_field_lang = '';
        $this->status_field = 'status';
        $this->cat_field = '';
        $this->order_field = 'sort_order';
        $this->image_field = array();
        ///$this->id_lang_rel=$this->id_field;
    }

    public $obj_array = array();
    //public $tree_array=array();
    public $tree = array();

    function get_tree($status = array(0, 1, 2)) {
        sort($status);
        $key = implode("", $status);
        if (count($this->tree[$key]))
            return $this->tree[$key];
        $this->getDB()->select("menu.*,categories.name as name,categories.slug ", false);
        //$this->getDB()->select("menu.*,videocategories.name as vname,categories.name  AS name  ,categories.slug ");
        $this->getDB()->from($this->table_name);
        $this->getDB()->join("categories","categories.id=menu.value","left");
        $this->getDB()->where_in('menu.status', $status);
        $this->getDB()->order_by('menu.sort_order');
        $list = $this->getDB()->get()->result();
        // print_r($list);exit;
        foreach ($list as $mn) {
            $mn->type == 'category' ? $mn->title : $mn->name;
            $mn->type == 'link' ? $mn->title : $mn->name;


            $this->obj_array[$key][$mn->position][$mn->id] = $mn;
            //$this->obj_array[$key][$mn->type][$mn->id]=$mn;
            //$this->tree_array[$mn->id]=$mn;
            $this->obj_array[$key][$mn->position][$mn->id]->children = array();
            if ($mn->parent == 0) {//root node
                $this->tree[$key][$mn->position][$mn->id] = $mn;
            }
            $mn->type == 'category' ? $mn->title : $mn->name ;
            $mn->type == 'link' ? $mn->title : $mn->name ;
        }
        foreach ($this->obj_array[$key] as $position => $plist) {
            foreach ($plist as $obj) {
                if ($this->obj_array[$key][$position][$obj->parent]) {
                    $this->obj_array[$key][$position][$obj->parent]->children[$obj->id] = $obj;
                }
            }
        }
		// print_r($this->obj_array);exit;
        return $this->tree[$key];
    }

    function store_tree($tree_array, $parent_id = 0) {
        $sort = 1;
		// print_r($parent_id);exit;
        foreach ($tree_array as $cate) {
			$this->getDB()->from($this->table_name);
			$this->getDB()->where('id',$parent_id);
			$item = current($this->getDB()->get()->result());
			$of_value = $item->value == 0?0:$item->value;
            $this->getDB()->update($this->table_name, array('parent' => $parent_id, 'sort_order' => $sort++, 'of_value'=>$of_value), array('id' => $cate->id));
            if (count($cate->children)) {
                $this->store_tree($cate->children, $cate->id);
            }
        }
    }

}

?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require_once APPPATH."third_party/YT/ControllerAdmin.php";
class Menus extends YT_ControllerAdmin {
	/**
     *
     * @var Test_model 
     */
	public $model;
        /**
         *
         * @var Genre_model
         */
        public $genre;
	function __construct()	{
		parent::__construct();	
		$this->model=$this->load->model('Menus_model');	
	}
        function addedit_process() {
			// print_r($_POST);exit;
            if($_POST['data']['slug']) $_POST['data']['slug']=  remove_accent ($_POST['data']['slug']);
            if(!$_POST['data']['id']){
                $this->model->getDB()->select("max(sort_order) as order_max");
                $this->model->getDB()->from($this->model->table_name);
                $obj=current($this->model->getDB()->get()->result());
                $_POST['data']['sort_order']=intval($obj->order_max)+1;
				unset($_POST['data']['id']);
                
            }
			if($_POST['data']['type'] == 'link'){
				$_POST['data']['value'] = '';
			}
            parent::addedit_process();
        }
        function on_after_addedit_process($id) {
            //if(!$_POST['data']['show']) $_POST['data']['show']=0;
            //if(!$_POST['data']['status']) $_POST['data']['status']=0;
            parent::on_after_addedit_process($id);
            
        }
        function index() {
            if(isset($_POST['menu_order'])){
				
               // echo "<pre>";
               // print_r(json_decode($_POST['menu_order']));
               // echo "</pre>";
               // exit;
                $array=json_decode($_POST['menu_order']);
				
                if(count($array)){
					
                    $this->model->store_tree($array);
                    $this->model->on_data_change();
                }
                echo json_encode(array('status'=>1));exit;
            }
            if(intval($_GET['id'])){
                $this->model->where['id']=intval($_GET['id']);
                $this->data['obj']=$this->model->get_object_by_where();
            }
            parent::index();
        }
}
 ?>
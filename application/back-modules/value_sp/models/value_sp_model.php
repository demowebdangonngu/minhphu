<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class value_sp_model extends YT_AModel {

    function __construct() {
        parent::__construct();
//id 	name 	link Link chi tiết của Tin vắn	status 	post_id ID của bài viết nếu trong bài viết có tùy chọn Tin vắn	type
        $this->fields = array(
            'id' => 'pri',
            'sp_did' => 1,
            'sp_id' => 1,
            'product_id' => 1,
            'cat_id' => 1,
            'value' => '1',
            'status' => 1,
            'score' => 1,
            'group' => 1,
        );
        $this->table_name = 'value_sp';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->id_field_lang = '';
        $this->status_field = 'status';
        $this->id_lang_rel = $this->id_field;
        $this->group_field = 'group';
    }

    function on_data_change() {
        /* @var $caches MY_Caches */
        $caches = $this->load->library('Caches');
        $caches->on_post_update();
        //parent::on_data_change();
    }

    public function listAll($where) {
        $this->getDB()->select('*');
        $this->getDB()->from($this->table_name);
        $this->getDB()->where($where);
        return $this->getDB()->get()->result();
    }

    function value_sp($array) {
        $data = array();
        foreach ($this->fields as $key => $value) {
            isset($array[$key]) ? $data[$key] = $array[$key] : "";
        }
        $returnid = 0;
        //print_r($data);
        foreach ($data['group'] as $row) {
            $data['sp_id'] = $row['sp_id'];
            if ($row['product_id']) {
                $data['product_id'] = $row['product_id'];
                foreach ($row['group_1'] as $row_group) {
                    $data['sp_did'] = $row_group['sp_did'];
                    $data['cat_id'] = $row_group['cat_id'];
                    $data['score'] = $row_group['score'];
                    $data['value'] = $row_group['value_product'];
                    $data['attr_id'] = $row_group['value_attr'];
                    unset($data['group']);
                    unset($data['group_1']);
                    $sql = 'SELECT * FROM value_sp WHERE product_id="' . $data['product_id'] . '" and cat_id="' . $data['cat_id'] . '"';
                    $list = $this->getDB()->query($sql)->result();
                    foreach ($list as $row_list) {}
                    if ($row_list) {
                        $this->getDB()->where(array('id' => $row_list->id));
                        $this->getDB()->update($this->table_name, $data);
                    } else {
                        $this->getDB()->insert($this->table_name, $data);
                        $returnid = $this->getDB()->insert_id();
                    }
                }
            } else {
                foreach ($row['group_1'] as $row_group) {
                    $data['sp_did'] = $row_group['sp_did'];
                    $data['cat_id'] = $row_group['cat_id'];
                    $data['score'] = $row_group['score'];
                    $data['value'] = $row_group['value_product'];
                    $data['attr_id'] = $row_group['value_attr'];
                    unset($data['group']);
                    unset($data['group_1']);

                    $sql_product = 'SELECT * FROM products';
                    $get_product = $this->getDB()->query($sql_product)->result();
                    foreach ($get_product as $get_p) {}
                    $data['product_id'] = $get_p->id;
                    $this->getDB()->insert($this->table_name, $data);
                    $returnid = $this->getDB()->insert_id();
                }
            }
        }

        if ($array['_mlang'] && $this->table_name_lang) {
            foreach ($array['_mlang'] as $idx => $lang) {
                $lang['lang_id'] = $idx;
                $this->store_data_lang($returnid, $lang);
            }
        }
        $this->on_data_change($returnid);
        return $returnid;
    }

}

?>
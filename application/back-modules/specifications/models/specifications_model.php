<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class Specifications_model extends YT_AModel {
       function __construct() {
            parent::__construct();
            	


          $this->fields=array (
  'id' => 'pri',
  'title' => 1,
  'status' => 1,
);
         $this->table_name='specifications';
         $this->table_name_lang='';
         $this->id_field='id';
         $this->id_field_lang='';
         $this->status_field='status';
         $this->id_lang_rel=$this->id_field;
         
        }
       function on_data_change() {
           /* @var $caches MY_Caches */
           $caches=$this->load->library('Caches');
           $caches->on_post_update();
           //parent::on_data_change();
       }
       
	function listAll($status=array(0,1,2)){
           
           $this->getDB()->select('*');
           $this->getDB()->from($this->table_name);
           $this->getDB()->order_by('id','DESC');
           $this->getDB()->where_in('status',$status);
           return $this->getDB()->get()->result();
       }
}
?>
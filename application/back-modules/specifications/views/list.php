<script>
   var module = '<?php echo $this->uri->segment(1);  ?>';
   var base_url ='<?php echo base_url();  ?>';
</script>

<section id="content">
    
    
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                  
                <li><a href="<?php echo base_url();  ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url().$this->uri->segment(1);  ?>"><?php if($module_title){echo $module_title;}else{echo $this->uri->segment(1);};?></a></li>
                <!--<li class="active">Static table</li>-->
              </ul>
              <div class="row">
              	<div class="col-sm-6">
                    <form id="menu-sorter-form" data-validate="parsley" method="post" action="<?php echo base_url().$this->uri->segment(1); ?>" >
                  	<input type="hidden" name="sort_order" id="sort_order" value="">
                    <section class="panel panel-default">
                      <header class="panel-heading">
                        <span class="h4">Danh sách TSKT</span>
                        <ul class="nav nav-tabs pull-right">
                            <li <?php echo (!$this->input->get("trash") ?"class='active'":"");?>><a href="<?php echo base_url().$this->uri->segment(1); ?>" ><i class="fa fa-file-text text-default"></i>&nbsp; TSKT</a></li>
                            <li <?php echo ($this->input->get("trash") ?"class='active'":"");;?>><a href="<?php echo base_url().$this->uri->segment(1)."?trash=1"; ?>" ><i class="fa fa-trash-o text-default"></i>&nbsp; Hiện TSKT đã xóa</a></li>
                        </ul>
                      </header>
                      <div class="panel-body">
                        <div class="dd" id="specifications_detail" data-output="">
						<ol class="dd-list">
						<?php
                                                /* @var $catemodel specifications_model */
                                                $specifications=$this->load->model("specifications_model");
                                                isset($_GET['trash'])?$status_array=array(0,1,2):$status_array=array(0,1);
                                                $data=$specifications->listAll($status_array);
                                                if(isset($data)) {
                                                foreach($data as $cat){
                                                ?>
						
						  <li class="dd-item parent_category <?php echo "status_".$cat->status;?>" data-id="<?php echo $cat->id;?>">
							  <div class="dd-handle"><?php echo $cat->title;?></div>
                              <span class="pull-right" style="position: absolute;right: 0px;top: 6px;">
                                    <?php  ?>
                                        <a title="Chỉnh sửa danh mục" href="<?php echo base_url().$this->uri->segment(1).'?id='.$cat->id;?>"><i class="fa fa-pencil icon-muted fa-fw m-r-xs"></i></a>
                                        <?php ?>
					<?php if($cat->status!=2){ ?>
                                    <a href="<?php echo base_url().$this->uri->segment(1).'/quick_trash?ids='.$cat->id;?>"><i class="fa fa-times icon-muted fa-fw"></i></a>
                                        <?php }else{ ?>
                                    <a title="Hiện danh mục" href="<?php echo base_url().$this->uri->segment(1).'/update_status/'.$cat->id.'?status=1&return=1'?>"><i class="fa icon-muted  fa-fw  fa-check-circle-o"></i></a>
                                        <?php } ?>					
                                </span>
                                                
						  </li>
                                                <?php }}?>
					  </ol>
					  </div>
                      </div>
                      
                    </section>
                        <style>
                            .dd li.status_0 .dd-handle{
                                color: #bbb;
                                background: #F7F7F7;
                            }
                            .dd li.status_2 .dd-handle{
                                color: #bbb;
                                background: #FF6666;
                            }
                            
                            
                        </style>
                  </form>
                </div>
                <?php 
		?>
                <div class="col-sm-6">
                  <form  method="post" id='add-insert-menu-frm' action="<?php echo base_url().$this->uri->segment(1); ?>/addedit_process/" onsubmit="update_form($(this),update_menu_callback);return false;">
                      <input type="hidden" name="data[id]" value="<?php echo $obj->id;?>">
                    <section class="panel panel-default">
                      <header class="panel-heading">
                        <span class="h4"><?php if($obj->id) {?>Sửa<?php }else{?>Thêm<?php }?> TSKT</span>
                      </header>
                      <div class="panel-body">
                          <div id="msg_error_msg" style="color:#f00;"></div>
                        
                        <div class="form-group">
                          	<label>Tên TSKT</label>
                            <input required minlength="3" vname="Tên chi tiết TSKT" type="text" name="data[title]" class="form-control" value="<?php echo $obj->title;?>">
                        </div>
                        
                        <div class="form-group">
                          <label class="col-sm-5 control-label" style="margin-left: -13px;">Hoạt động</label>
                          <div class="col-sm-7">
                            <label class="switch">
                              <input type="checkbox" value="1" name="data[status]" <?php if ($obj->status){?>checked<?php }?>>
                              <span></span>
                            </label>
                          </div>
                          <div class="clear"></div>
                        </div>
						<footer class="text-right bg-light lter">
                                                    <input type="button" onclick="update_form($('#add-insert-menu-frm'),update_form_callback);return false;" class="btn btn-success btn-s-xs" value="<?php if(isset($obj->id)) {?>Cập nhật<?php }else{?>Thêm<?php }?>"/>
						</footer>
                      </div>
                    </section>
                  </form>
                </div>
                <?php ?>
              </div>
            </section>
          </section>
          <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
        </section>
<script>

function update_form_callback(data){
    if(data.status)
   window.location=base_url+module;
}
$(document).ready(function(e) {
});
window.onbeforeunload = function(){
    if(change)
        return 'Bạn vừa thay đổi vị trí của các danh mục bên trái, nếu thoát trang thì các vị trí sẽ không được lưu lại!';
};
</script>
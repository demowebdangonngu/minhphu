<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class order_detail_model extends YT_AModel {

    function __construct() {
        parent::__construct();



        $this->fields = array(
            'odd_id' => 'pri',
            'order_id' => 1,
            'product_id' => 1,
            'phone' => 1,
            'quantily' => 1,
            'price' => 1,
        );
        $this->table_name = 'order_detail';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->id_field_lang = '';
        $this->status_field = 'order_detail.status';
        $this->cat_field = '';
        $this->order_field = '';
        $this->image_field = array('image' => '');
        $this->id_lang_rel = $this->id_field;
    }

    function on_data_change() {
        /* @var $caches MY_Caches */
        $caches = $this->load->library('Caches');
        $caches->on_post_update();
        //parent::on_data_change();
    }
    function delete_id($id){
        $this->getDB()->where('id',$id);
        $this->getDB()->delete($this->table_name);
    }

}

?>
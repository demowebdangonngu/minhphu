<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class order_model extends YT_AModel {

    function __construct() {
        parent::__construct();



        $this->fields = array(
            'order_id' => 'pri',
            'name' => 1,
            'email' => 1,
            'phone' => 1,
            'address' => 1,
            'order_date' => 1,
            'total_price' => 1,
            'status' => 1,
        );
        $this->table_name = 'order';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->id_field_lang = '';
        $this->status_field = 'order.status';
        $this->cat_field = '';
        $this->order_field = '';
        $this->image_field = array('image' => '');
        $this->id_lang_rel = $this->id_field;
    }

    function on_data_change() {
        /* @var $caches MY_Caches */
        $caches = $this->load->library('Caches');
        $caches->on_post_update();
        //parent::on_data_change();
    }
    function delete_id($id){
        $this->getDB()->where('id',$id);
        $this->getDB()->delete($this->table_name);
    }
    public function getOne($where) {
        $this->getDB()->select("*, product.name as sp_name, product.price_sale as sp_sale, product.price as sp_price, product.image as sp_image,order.name as hd_name, order.phone as hd_phone, order.email as hd_email, order.address as hd_address, order.order_date as hd_date, order.id as hd_id, order.status as order_status");
        $this->getDB()->from('order_detail');
        $this->getDB()->join("order", "order.id = order_detail.order_id");
        $this->getDB()->join("product", "product.id = order_detail.product_id");
        $this->getDB()->where($where);
        //$this->getDB()->get()->result();
        return $this->getDB()->get()->result();
    }

    public function getMember($where){
        $this->getDB()->select("order.*, member.name as m_name, member.email as m_email, member.phone as m_phone, member.address as m_address");
        $this->getDB()->from('order');
        $this->getDB()->join("member", "member.id = order.mem_id");
        $this->getDB()->where($where);
        //$this->getDB()->get()->result();
        return $this->getDB()->get()->result();
    }
}

?>
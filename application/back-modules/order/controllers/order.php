<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/ControllerAdmin.php";

class order extends YT_ControllerAdmin {

    /**
     *
     * @var Test_model 
     */
    public $model;

    function __construct() {
        // pr($this->uri);die;
        parent::__construct();
        $this->model = $this->load->model('order_model');
        $this->check_access('order_access');
    }

    public function search($array_fillter){
            
            if(is_numeric($array_fillter['key'])){
                $this->model->where=array($this->model->table_name.".".$this->model->id_field=>$array_fillter['key']);
            }else{
                
                
                if($array_fillter['from']){
                    $date=  explode("/", $array_fillter['from']);
                    $fromdate=intval($date[2])."-".intval($date[1])."-".intval($date[0])." 00:00:00";
                    $this->model->where['order.order_date >=']=$fromdate;
                }
                if($array_fillter['to']){
                    $date=  explode("/", $array_fillter['to']);
                    $fromto=intval($date[2])."-".intval($date[1])."-".intval($date[0])." 23:59:59";
                    $this->model->where['order.order_date <=']=  $fromto;
                }
                if($array_fillter['key']){
                    $this->model->like=array('order.email',''.$array_fillter['key'].'');
                }
                if($array_fillter['status']){
                    $this->model->where['status']=$array_fillter['status'];
                }

                //pr($this->model->where);die;
            }
            //return $this->index();
    }

    function addedit($id = 0) {
        $this->data['id'] = $this->uri->segment(3);
        // EDIT
        if ($this->data['id']) {
            $this->data['detail'] = $this->model->getOne("order_detail.order_id = " . $this->data['id']);
            $this->data['member'] = $this->model->getMember("order.id = " . $this->data['id']);
            //print_r($this->data['member']);exit;
        }
        //print_r($this->data['detail']);exit;
        $data_post = $this->input->post();
        if($data_post){
            $id = $this->model->store_data($this->input->post("data"));
            if ($id) {
                $this->on_after_addedit_process($id);
            }
            redirect("order", 'refresh');
        }
        parent::addedit($id);
    }
    public function index() {
        if ($this->input->get('search')) {
            $this->search($this->input->get('search'));
        }
        $input = $this->input->get();
        if ($input['order']) {
            $pattern = explode(" ", $input['order']);
            if ($this->model->fields[$pattern[0]]) {
                $this->model->order = $this->model->table_name . "." . $pattern[0] . " " . (in_array(strtolower($pattern[1]), array('desc', 'asc')) ? $pattern[1] : '');
            } else
            if ($this->model->fields_lang[$pattern[0]]) {
                $this->model->order = $this->model->table_name_lang . "." . $pattern[0] . " " . (in_array(strtolower($pattern[1]), array('desc', 'asc')) ? $pattern[1] : '');
            }
        } else {
            $this->model->order = $this->model->table_name . "." . $this->model->id_field . " desc";
        }
        if ($this->model->fields[$this->model->status_field]) {
            if (!$input['trash']) {
                $this->model->where[$this->model->status_field . " !="] = 0;
            } else {
                $this->model->where[$this->model->status_field . " ="] = 0;
            }
            $this->model->where["(" . $this->model->status_field . " !=0 or admin_id=" . $this->session->userdata('admin_id') . ")"] = null;
        }
        $this->model->single_table_mode = false;
        $this->model->select = "order.*,member.name as m_name, member.email as m_email, member.phone as m_phone";
        $this->model->join[] = array('member', "member.id=order.mem_id");
        $this->data['content'] = $this->model->get_page_list("{$this->uri->segment(1)}/index", 30, 3);
        foreach ($this->data['content']['pageList'] as $value) {
            $roption['row'] = $value;
            $roption['controller'] = $this;
            $roption['model'] = $this->model;
            $value->row_option = $this->load->view('row_option', $roption, true);
        }

        $this->data['controller'] = $this;
        $this->data['model'] = $this->model;
        $this->data['form_search'] = $this->load->view('form_search', $this->data, true);
//                $this->data['option']['quick_trash']=1;
//                $this->data['option']['quick_delete']=1;
        $this->template->build('list', $this->data);
    }
    public function quick_trash() {
        $return = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $this->uri->segment(1);
        $ids = $this->input->get('ids');
        $this->model->delete_id($ids);
        $this->model->on_data_change();
        if ($ids)
            logs("Xóa: " . implode(",", $ids));
        redirect($return);
    }

//    function on_after_addedit_process($pid) {
//        $date = date('Y-m-d',time());
//        $result = array();
//        $admin_id = $this->session->userdata('admin_id');
//        $data = array(
//            'admin_id' => $admin_id,
//            'public_date' => $date,
//        );
//        $this->model->where('id',$pid);
//        $this->model->update($data);
//        return array_merge($result, (array) parent::on_after_addedit_process($pid));
//    }
}

?>
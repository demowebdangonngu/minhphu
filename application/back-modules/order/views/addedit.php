<?php 
$logo = $this->load->model('attach/attach_model');
$logo->where(array('nameModel'=>'logo','status'=>1));
$logo->order_by('sort_order');
$r_logo = current($logo->get()->result());

?>
<section id="content">
   <section class="vbox bg-white">
   
		<header class="header b-b b-light hidden-print">
			<label style="line-height: 33px;" class="message_alert"></label>
			<label class="pull-right" style="margin-top: 8px;">
      <button href="#" class="btn btn-sm btn-info pull-right" onclick="window.print();" style="height:34px;">Print</button>
			<button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');
			   return false;">Đóng</button>
			</label>
		</header>
      <section class="scrollable wrapper">
         <div class="row">
            <div class="col-sm-12">
               
                    <form form id="form" name="form" method="post" action="" class="form-horizontal" enctype="multipart/form-data">
                     <section class="scrollable wrapper">
                        <img src="<?php echo base_url().'../upload/images/images/'.$r_logo->location; ?>"/>
                        <div class="row">
                           <div class="col-xs-6">
                              <h4>Camerano1</h4>
                              <p><a href="http://camerano1.com/">camerano1.com</a></p>
                              <p>Địa Chỉ: 6/1/16 DT743 ,Đồng an 3 ,Bình Hòa ,Thuận An, Bình Dương. </p>
                              <p>Telephone:  0933.149.990 - 0168.258.6825<br>
							  </p>
                           </div>
                        </div>
                        <div class="well m-t">
                           <div class="row">
                              <div class="col-xs-6">
                                 <strong>TO:</strong>                    
                                 <h4><?php echo $member[0]->m_name ?></h4>
                                 <p><?php echo $member[0]->m_address ?><br>                      Phone: <?php echo $member[0]->m_phone ?><br>                      Email: <?php echo $member[0]->m_email ?><br>                    </p>
                              </div>
                              <div class="col-xs-6">
                                 <strong>SHIP TO:</strong>                    
                                 <h4><?php echo $member[0]->m_name ?></h4>
                                 <p><?php if($detail[0]->hd_address){echo $detail[0]->hd_address;}else{echo $member[0]->m_address;} ?><br>                      Phone: <?php echo $member[0]->m_phone ?><br>                      Email: <?php echo $member[0]->m_email ?><br>                    </p>
                              </div>
                           </div>
                        </div>
                        <p class="m-t m-b">Order date: <strong><?php echo $detail[0]->hd_date;?></strong><br>Order ID: <strong>#<?php echo $detail[0]->hd_id;?></strong><br>Trạng thái: <strong>
                      <select  name="data[status]" class='input-s-sm input-s select-input form-control'>
                          <option <?php if($detail[0]->order_status =='0') echo "selected=selected"; ?> value="0">Chưa xác nhận</option>
                          <option <?php if($detail[0]->order_status =='1') echo "selected=selected"; ?> value="1">Xác nhận đơn hàng</option>
                          <option <?php if($detail[0]->order_status =='2') echo "selected=selected"; ?> value="2">Đang giao hàng</option>
                          <option <?php if($detail[0]->order_status =='3') echo "selected=selected"; ?> value="3">Đã thanh toán</option>
                          <option <?php if($detail[0]->order_status =='4') echo "selected=selected"; ?> value="4">Hoàn tất đơn hàng</option>
						  <option <?php if($detail[0]->order_status =='5') echo "selected=selected"; ?> value="5">Hủy đơn hàng</option>
                      </select>
                        </strong>              </p>
                        <div class="line"></div>
                        <table class="table">
                           <thead>
                              <tr>
                                 <th width="60">STT</th>
                                 <th>Sản Phẩm</th>
								 <th></th>
                                 <th width="90">Giá</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php				
							  $stt = 0;				
							  foreach ($detail as $row) {					
							  $stt++;	
								if($row->sp_sale >0){
									$total += $row->sp_sale;
									$price = $row->sp_sale;
								}else{
									$total += $row->sp_price;
									$price = $row->sp_price;
								}			
							  ?>                  
                              <tr>
                                 <td><?php echo $stt;?></td>
                                 <td><?php echo $row->sp_name ?></td>
								 <td></td>
                                 <td><?php echo number_format($price); ?> VNĐ</td>
                              </tr>
                              <?php }?>                                    
                              <tr>
                                 <td colspan="3" class="text-right no-border"><strong>Total</strong></td>
                                 <td><strong><?php echo number_format($total); ?> VNĐ</strong></td>
                              </tr>
                           </tbody>
                        </table>
                        <header class="header b-b b-light hidden-print">
                        <label style="line-height: 33px;" class="message_alert"></label>
                        <label class="pull-right" style="margin-top: 8px;">
                        <input type="submit" class="btn btn-primary" value="Lưu lại"/>
                        <button href="#" class="btn btn-sm btn-info pull-right" onclick="window.print();" style="height:34px;">Print</button>
                        <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');
                           return false;">Đóng</button>
                        </label>
                      </header>
                     </section>
                     </form>
            </div>
            
         </div>
         </div>  
      </section>
      
   </section>
</section>

<div class="row wrapper">
    <div class="col-sm-12">
        <style>
            .frm-posts-search>div{
                margin:1px 0px;
                padding:0px;
            }
            
        </style>
        <form class="col-sm-12 frm-posts-search" action="<?php echo base_url().  $this->uri->segment(1)."/index"; ?>">
            <?php if($_GET['trash']) { ?><input type="hidden" name="trash" value="1"/><?php } ?>
                    <div class="col-sm-3">
                        <div class="">
                          <input type="text" style="width:100%;" placeholder="Từ khóa" value="<?php echo $_GET['search']['key'];?>" name="search[key]" class="input-sm form-control">
                        </div>
                    </div>
                    <div class="col-sm-4" style="width: 28%;">
                          <div class="input-group">
                            <span class="input-group-addon">Thời gian:</span>
                            <input type="text" data-date-format="dd/mm/yyyy" placeholder="Từ ngày ..." value="<?php echo $_GET['search']['from']; ?>" size="16" style="width:100px;" class="input-s-sm input-s yt-datetime form-control" id="date_sort_start" name="search[from]">
                            <input type="text" data-date-format="dd/mm/yyyy" placeholder="đến ngày ..." value="<?php echo $_GET['search']['to']; ?>" size="16" style="width:100px;" class="input-s-sm input-s yt-datetime form-control" id="date_sort_end" name="search[to]">
                            <script>
                                $(document).ready(function(){
                                    $(".yt-datetime").datepicker({dateFormat:'dd/mm/yy'});
                                });
                      
                  </script>
                        </div>
                      </div>
                      <div class="col-sm-3">
                        <div class="input-group">
                            <span class="input-group-addon">Trạng thái</span>
                            <select  name="search[status]" class='input-s-sm input-s select-input form-control'>
                                <option <?php if(!$_GET['search']['status']) echo "selected=selected"; ?> value="">Tất cả</option>
                                <option <?php if($_GET['search']['status']=='0') echo "selected=selected"; ?> value="0">Chưa xác nhận</option>
                                <option <?php if($_GET['search']['status']=='1') echo "selected=selected"; ?> value="1">Xác nhận đơn hàng</option>
                                <option <?php if($_GET['search']['status']=='2') echo "selected=selected"; ?> value="2">Đang giao hàng</option>
                                <option <?php if($_GET['search']['status']=='3') echo "selected=selected"; ?> value="3">Đã thanh toán</option>
                                <option <?php if($_GET['search']['status']=='4') echo "selected=selected"; ?> value="4">Hoàn tất đơn hàng</option>
								<option <?php if($_GET['search']['status']=='5') echo "selected=selected"; ?> value="5">Hủy đơn hàng</option>
                            </select>
                            
                        </div>
                      </div>
                   
                      </div>
                      
            <div class="col-sm-3 banner-positino-control" style="<?php if($_GET['search']['position']!='hot') echo "display:none"; ?>">
               <?php 
                            $a_position=array(1=> "317x200","317x200","317x132","317x132","317x132","317x269","317x132",
                                                                                    "317x200","317x200","317x132","317x132","317x132","317x269","317x132",
                                                                                    "317x200","317x200","317x132","317x132","317x132","317x269","317x132",);
                                                                                    ?>
                        <div class="input-group " >
                            <span class="input-group-addon">Vị trí banner</span>
                            <select  name="search[hot_position]" class='input-s-sm input-s select-input form-control'>
                                <option <?php if(!$_GET['search']['hot_position']) echo "selected=selected"; ?> value="">Tất cả banner</option>
                                <?php foreach ($a_position as $_ps=>$_text) {                                                                                                                                
                                                                    $_size=explode("x", $_text);
                                                                ?>
                                  <option <?php if($_GET['search']['hot_position']==$_ps) echo "selected=selected"; ?> value="<?php echo $_ps;?>" w="<?php echo $_size[0];?>" h="<?php echo $_size[1];?>">Vị trí <?php echo $_ps." (" .$_text.") ";?> </option>
                                <?php } ?>
                            </select>
                            
                        </div>

                 </div>
                    <div class="col-sm-3">
                        <div class="input-group">
                          <span class="input-group-btn ">
                            <input type="submit" class="btn btn-primary" value="Tìm"/>
                          </span>
                        </div>
                    </div>
                      
            </form>
    </div>
</div>
<!--
              <form action="<?php echo base_url().  $this->uri->segment(1)."/index"; ?>">
              <div class="search fr" style="">
                  <input type="text" placeholder="Nhập từ khóa" name="search[key]" value="<?php echo $_GET['search']['key'];?>" /> 
                  <?php if($model->fields['create_date']){?>
                  <input type="text" placeholder="Từ ngày" name="search[from]" class="short yt-datetime" value="<?php echo $_GET['search']['from'];?>"/> 
                  <input type="text" placeholder="Đến ngày" name="search[to]" class="short yt-datetime" value="<?php echo $_GET['search']['to'];?>"/> 
                  <script>
                      $(".yt-datetime").datepicker({dateFormat:'dd/mm/yy'});
                  </script>
                  <?php } ?>
                  <input type="submit" value="" class="bntS" />
              </div> 
              </form>
    -->          
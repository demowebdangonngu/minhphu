<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class pages_model extends YT_AModel {
       function __construct() {
            parent::__construct();
            
         $this->fields=array (
  'id' => 'pri',
  'name' => 1,
  'slug' => 1,
  'summary' => 1,
  'content' => 1,
  'status' => 1,
  'image' => 1,
  'created_by' => 1,
  'admin_note' => 1,
  'create_date' => 1,
  'modify_date' => 1,
  'modify_by' => 1,
  'mtitle' => 1,
  'mdescription' => 1,
  'mkeyword' => 1,
);
         
         $this->table_name='pages';
         $this->id_field='id';
         $this->status_field='status';
         $this->image_field=array('image'=>'');
         
        }
	
}
?>
<script type="text/javascript">
    $(document).ready(function (e) {
        $('.aut_img').each(function (index, element) {
            $(this).error(function () {
                $(this).hide();
            });
        });
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result).show();
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
<script language="javascript">
    var site_root_domain = "<?php echo $this->config->item('static_path'); ?>";
</script>
<script type="text/javascript" src="<?php echo $this->config->item('static_path'); ?>themes/admincp/scripts/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('static_path'); ?>themes/admincp/scripts/js_editor.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('static_path'); ?>themes/admincp/scripts/jquery.tinymce.js"></script>
<section id="content">
    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>"><?php
                        if ($module_title) {
                            echo $module_title;
                        } else {
                            echo $this->uri->segment(1);
                        };
                        ?></a> </li>
                <li class="active">Thêm/Sửa bài viết</li>
            </ul>
            <div class="row">
                <div class="col-sm-12">
                    <form id="form" name="form" method="post" action="" class="form-horizontal" enctype="multipart/form-data">
                        <section class="panel panel-default">
                            <header class="control-fixed panel-heading font-bold" data-top="49" >
                                <label style="line-height: 33px;">Thêm / Sửa nội dung</label>
                                <label style="line-height: 33px;" class="message_alert"></label>
                                <label class="pull-right">
                                    <input type="submit" class="btn btn-primary" value="Lưu lại"/>
                                    <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');
                                            return false;">Đóng</button>
                                </label>
                            </header>
                            <div class="panel-body">
                                <style>

                                    .panel-heading.control-fixed {
                                        height: 43px;
                                        padding: 4px 17px;
                                    }
                                </style>
                                <div class="col-sm-8 left" id="post_form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" style="text-align: left; padding-left: 0px; padding-right: 0px; width: 75px;">Tiêu đề</label>
                                        <div class="col-sm-7">
                                            <input name="data[title]"  id="title" type="text" onKeyUp="oncount('#title', '#count_title')" onChange="oncount('#title', '#count_title')" class="form-control" data-required="true" value="<?php echo htmlspecialchars($obj->title); ?>" placeholder="Tiêu đề">
                                        </div>
                                        <label class="col-sm-2 control-label" style="text-align: left; padding-left: 0px; padding-right: 0px; width: 45px;">Còn lại</label>
                                        <div class="col-sm-2">
                                            <input name="count_title" data-count=200 disabled id="count_title" type="text" class="form-control" style="width: 104px;text-align: center;" value="137" placeholder="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-10" style="padding-left:0px">
                                            <label class="control-label" style="margin-bottom: 5px;"><strong>Tóm tắt</strong></label>
                                        </div>
                                        <div class="col-sm-10" style="padding-left:0px">
                                            <textarea rows="5" name="data[description]" id="summary" class="form-control" onKeyUp="oncount('#summary', '#count_summary')" onChange="oncount('#summary', '#count_summary')"><?php echo $obj->description; ?></textarea>
                                        </div>
                                        <div class="col-sm-2  text-center">
                                            <span>Còn lại</span>
                                            <span><input id="count_summary" data-count=300 disabled type="text" class="form-control" style="width: 104px;
                                                         text-align: center;" value="42" placeholder=""></span>
                                        </div>
                                    </div>

                                    <script>
                                        $(document).ready(function () {
                                            oncount('#title', '#count_title');
                                            oncount('#summary', '#count_summary');
                                            oncount('#summary_short', '#count_summary_short');
                                            oncount('#title_short', '#count_title_short');
                                        });
                                    </script>
                                    <div class="form-group row-content">
                                        <label class="control-label"><strong>Nội Dung</strong></label>
                                        <textarea class="input_textarea" name="data[content]" style="width: 100%;" rows="5" id="aaa"><?php echo $obj->content;?></textarea>
                                    </div>
                                </div>

                                <div class="col-sm-4">   
                                    <section class="panel panel-default">
                                        <header class="panel-heading font-bold">Trạng thái</header>
                                        <div class="panel-body">     
                                            <div class="col-sm-12"> 
                                                <label>Trạng Thái Sử Dụng</label>
                                                <label class="switch">
                                                    <input type="checkbox" value="1" name="data[status]" <?php if ($obj->status) { ?>checked<?php } ?>>
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="panel panel-default">
                                        <header class="panel-heading font-bold">Danh mục</header>

                                        <div class="panel-body">     
                                            <div class="col-sm-12">  
                                                <div class="form-group">
                                                    <label class="control-label">Danh mục chính</label>
                                                    <select name="data[cat_id]" id="menus_parent_video" class="form-control">
                                                        <?php
                                                        $catemodel = $this->load->model("categories/Categories_model");
                                                        isset($_GET['trash']) ? $status_array = array(0, 1, 2) : $status_array = array(0, 1);
                                                        $categories = $catemodel->get_tree($status_array);
                                                        foreach ($categories as $cat) {
                                                            ?>
                                                            <option value="<?php echo $cat->id; ?>" <?php if ($obj->cat_id == $cat->id) { ?>selected<?php } ?>> <?php echo $cat->name; ?></option>

                                                            <?php
                                                            if (count($cat->children)) {
                                                                foreach ($cat->children as $cat2) {
                                                                    ?>
                                                                    <option value="<?php echo $cat2->id; ?>" <?php if ($obj->cat_id == $cat2->id) { ?>selected<?php } ?>>&nbsp;|-- <?php echo $cat2->name; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section class="panel panel-default">
                                        <header class="panel-heading font-bold">Hình ảnh và vị trí</header>
                                        <div class="panel-body">     
                                            <div class="col-sm-12">  
                                                <div class="form-group">
                                                    <a title="<div>Kích thước chuẩn:<div> <div>960x800 - .jpg .png .gif</div>Dung lượng tối đa: 2Mb ">
                                                        <input type="file" onChange="readURL(this);" name="img" id="img" > <br />
                                                    </a>
                                                        <?php
                                                        if ($obj->image) {
                                                            ?><label for="hinhanh">Hình Cũ</label>
                                                            <img src="<?php echo $this->config->item("img_path") . $obj->image; ?>" width="100%" height="100%" />
                                                            <br>
                                                            <label for="hinhanh">Hình Mới</label>
                                                            <?php
                                                        }
                                                        ?><img class="aut_img" id='blah' src='' style=" max-width: 100%; max-height: 100%;" />
                                                </div>	
                                            </div>
                                        </div>
                                    </section>
                                </div>   
                                <div class="col-sm-12">
                                    <div class="line line-dashed line-lg pull-in"></div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <input type="submit" class="btn btn-primary" value="Lưu lại"/>
                                            <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');
                                                    return false;">Đóng</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </form>
                </div>
            </div>  
        </section>


    </section>
</section>
<input id="old-data-form" type="hidden" value=""/>

<script>

    function on_form_submit() {
        var form = $("#old-data-form");
        if ($("select.data-form-status").val() == 'publish') {

            if ($("#title").val().length < 3) {
                alert("Tên bài viết quá ngắn");
                return false;
            }
            if ($("#summary_short").val().length < 20) {
                alert("Tóm tắt ngắn, ngắn hơn 20 ký tự");
                return false;
            }
            if ($("select.data-form-category").val() == null) {
                alert("Vui lòng chọn danh mục");
                return false;
            }
            //alert($("input.chose_img_radio:checked").val());
            if ($("input.chose_img_radio:checked").val() == null) {
                alert("Vui lòng chọn hình đại diện");
                return false;
                ;
            }
        }
    }
    $(".scrollable").scroll(function () {
        var elem = $('.control-fixed');//alert("sdfsdf");
        if (!elem.attr('data-top')) {
            if (elem.hasClass('navbar-fixed-top'))
                return;
            var offset = elem.offset()
            elem.attr('data-top', offset.top);
        }
        if (elem.attr('data-top') <= $(this).scrollTop())
            elem.addClass('navbar-fixed-top');
        else
            elem.removeClass('navbar-fixed-top');
    });
    function oncount($idPut, $idCount) {
        if ($($idPut).val().length >= $($idCount).data('count'))
        {
            //alert("Bạn đã nhập quá ký tự cho phép");
            $($idPut).val($($idPut).val().substring(0, $($idCount).data('count')));
        }
        if ($idCount != $idPut) {
            $($idCount).val($($idCount).data('count') - $($idPut).val().length);
        }
    }

    //*************end of upload thumb*******************************/

</script>
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class Posts_image extends YT_AModel {
       function __construct() {
            parent::__construct();
            	
 //Full texts 	id 	path 	description 	name 	sort_order 	posts_id


         $this->fields=array(
            'id'=>1,	
            'path'=>1,
            'description'=>1,
            'name'=>1,
            'sort_order'=>1,
            'posts_id'=>1,
             'create_date'=>1,
        );
         $this->table_name='posts_image';
         $this->table_name_lang='';
         $this->id_field='id';
         $this->id_field_lang='';
         $this->status_field='status';
         $this->cat_field='';
         $this->order_field='sort_order';
         $this->image_field=array('path'=>'');
         $this->id_lang_rel=$this->id_field;
         
        }
        function getImageByPost($post_id){
            $this->where['posts_id']=$post_id;
            $this->order="sort_order asc";
            return $this->get_list();
        }
	
}
?>
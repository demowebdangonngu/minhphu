<!-- main left -->
<div class="mainLeft">
    
    <!-- categories -->
    <div class="blogCategories">
      <a href="#" class="homepage"><?php echo $this->lang->line('menu_home');?></a>
        <ul class="navL">
            <li><a href="#"><i class="icoN1"></i> <?php echo $this->lang->line('menu_items');?> </a>
            <ul>
            	 <li><a href="<?php echo site_url("items"); ?>">
				 	<?php echo $this->lang->line('menu_manager');?></a></li>
                    
                 <li><a href="<?php echo site_url("items/addvod"); ?>">
				 	<?php echo $this->lang->line('menu_items_add1');?></a></li>
                    
                <li style="display:none" class="active"><a href="#"><?php echo $this->lang->line('menu_add');?></a>
                             <ul>
                            <li><a href="<?php echo site_url("items/addvod"); ?>">
								<?php echo $this->lang->line('menu_items_add1');?></a></li>
                             <li><a href="<?php echo site_url("items/addradio"); ?>">
							 	<?php echo $this->lang->line('menu_items_add2');?></a></li>
                             <li><a href="<?php echo site_url("items/guide"); ?>">
							 <?php echo $this->lang->line('menu_guide');?></a></li> 
                             
                          </ul> 
                  </li>
                <li  ><a href="#"><?php echo $this->lang->line('menu_items_timeline');?></a>
                             <ul>
                            <li><a href="<?php echo site_url("items/addtimeline"); ?>">
								<?php echo $this->lang->line('menu_add');?></a></li>
                             <li><a href="<?php echo site_url("items/timeline"); ?>">
							 	<?php echo $this->lang->line('menu_manager');?></a></li>
                             <li><a href="<?php echo site_url("items/guidetimeline"); ?>">
							 	<?php echo $this->lang->line('menu_guide');?></a></li> 
                             
                          </ul> 
                  </li>
                  
                  <li><a href="<?php echo site_url("items/recommend"); ?>">
				 	<?php echo $this->lang->line('menu_items_recom');?></a></li>
                    
                    
                  
                  
                 <li style="display:none"><a href="#"><?php echo $this->lang->line('menu_delete');?></a></li>
                  
              </ul>  
          </li> 
          <li><a href="#"><i class="icoN2"></i> <?php echo $this->lang->line('menu_banner');?></a>
             <ul>
                <li><a href="#"><?php echo $this->lang->line('menu_add');?></a></li>
                <li><a href="#"><?php echo $this->lang->line('menu_manager');?></a></li>
                <li><a href="#"><?php echo $this->lang->line('menu_guide');?></a></li> 
                 <li><a href="#"><?php echo $this->lang->line('menu_delete');?></a></li>
              </ul> 
          </li> 
          <li><a href="#"><i class="icoN3"></i> <?php echo $this->lang->line('menu_provider');?></a>
             <ul>
                <li><a href="#"><?php echo $this->lang->line('menu_add');?></a></li>
                <li><a href="#"><?php echo $this->lang->line('menu_manager');?></a></li>
                <li><a href="#"><?php echo $this->lang->line('menu_guide');?></a></li> 
                 <li><a href="#"><?php echo $this->lang->line('menu_delete');?></a></li>
              </ul>
          </li> 
          <li><a href="#"><i class="icoN11"></i><?php echo $this->lang->line('menu_genre');?></a>
            <ul>
                <li><a href="#">Đại lý mới</a></li>
                 <li><a href="#"><?php echo $this->lang->line('menu_add');?></a></li>
                <li><a href="#"><?php echo $this->lang->line('menu_manager');?></a></li>
                <li><a href="#"><?php echo $this->lang->line('menu_guide');?></a></li> 
                 <li><a href="#"><?php echo $this->lang->line('menu_delete');?></a></li>
              </ul>
          </li> 
          
      </ul>
      <!--<a href="#" class="min"><i class="icoN10"></i> Less</a>-->
    </div>
    <!-- en categories -->
    
</div> 
<!-- en main left --> 
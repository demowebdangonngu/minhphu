<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require_once APPPATH."third_party/YT/ControllerAdmin.php";
class Vsection extends YT_ControllerAdmin {
	/**
     *
     * @var Test_model 
     */
	public $model;
	function __construct()	{
		parent::__construct();	
		$this->model=$this->load->model('vsection_model');	
	}
        function on_after_addedit_process($id) {
            if(isset($_POST['video'])){
                $vids=array();
                if($_POST['video']){
                    foreach ($_POST['video'] as $vid) {
                        if($vid){
                        $vids[]=$vid;
                        $this->model->insert_if_not_exist("vsection_video", array('section_id'=>$id,'video_id'=>$vid));
                       $this->model->getDB()->update("vsection_video",array('show_home'=>1),array('section_id'=>$id,'video_id'=>$vid));
                        }
                    }
                }
                if($vids)
                $this->model->getDB()->query("update vsection_video set show_home=0  where  section_id='$id' and video_id not in (".  implode(",", $vids).")");
            }
            /* @var $tags tags_model */
            $tags=$this->load->model('tags/tags_model');
            $tags->insert_tags_for_object($_POST['tags'], $id,"vsection");
            parent::on_after_addedit_process($id);
        }
}
 ?>
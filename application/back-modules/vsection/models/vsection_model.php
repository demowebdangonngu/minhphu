<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class Vsection_model extends YT_AModel {
       function __construct() {
            parent::__construct();
            
         $this->fields=array (
  'id' => 'pri',
  'name' => 1,
  'status' => 1,
  'type' => 1,
  'show_home' => 1,
  'home_limit' => 1,
  'sort_order' => 1,
  'create_date' => 1,
  'modify_date' => 1,
  'image' => 1,
             'template' => 1,
);
         $this->table_name='vsection';
         $this->id_field='id';
         $this->status_field='status';
         $this->order_field='sort_order';
         $this->image_field=array('image'=>'');
         
        }
        function get_list_video($video_id){
            $this->select($this->table_name.".*");
            //$this->from("vsection_video");
            $this->join("vsection_video","vsection_video.section_id=vsection.id");
            $this->where(array("vsection_video.video_id"=>$video_id));
            $list= $this->get()->result();
            $return=array();
            foreach ($list as $sec) {
                $return[$sec->id]=$sec;
            }
            unset($list);
            return $return;
        }
	
}
?>
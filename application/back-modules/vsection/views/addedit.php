<script>
   var module = '<?php echo $this->uri->segment(1);  ?>';
   var base_url ='<?php echo base_url();  ?>';
   var img_path = '<?php echo $this->config->item('img_path')  ?>';
   <?php 
   $returnurl=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:base_url().$this->uri->segment(1); 
   $id_field=$model->id_field;
   $order_field=$model->order_field;
   $status_field=$model->status_field;
   ?>
</script>
<section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="<?php echo base_url();  ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url().$this->uri->segment(1);  ?>"><?php if($module_title){echo $module_title;}else{echo $this->uri->segment(1);};?></a> </li>
                <li class="active">Thêm / Sửa bài viết</li>
              </ul>
              <div class="row">
                <div class="col-sm-12">
                    <form class="form-horizontal" id="frm_add_edit" method="post" action="<?php echo base_url().$this->uri->segment(1); ?>/addedit_process/" onsubmit="submit_form($(this),'<?php echo $return_url; ?>');return false;"  >
                        <input  name="data[<?php echo $id_field; ?>]" type="hidden" id="txt_primary_id" value="<?php echo $obj->$id_field;?>"/>
                  <section class="panel panel-default">
                    <header class="control-fixed panel-heading font-bold" data-top="49" >
                        <label style="line-height: 33px;">Thêm / Sửa nội dung</label>
                        <label class="pull-right">
                              <button class="btn btn-primary" type="button" onclick="update_form($('#frm_add_edit'));" class="bntAll">Lưu lại</button>
                              <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');return false;">Đóng</button>
                            </label>
                    </header>
                    <div class="panel-body">
                        <div id="post_form" class="col-sm-8 left">
                            <style>
                                
                                .panel-heading.control-fixed {
                                    height: 43px;
                                    padding: 4px 17px;
                                  }
                            </style>
                            <script>
                                $(".scrollable").scroll(function(){
                                var elem = $('.control-fixed');//alert("sdfsdf");
                                if (!elem.attr('data-top')) {
                                    if (elem.hasClass('navbar-fixed-top'))
                                        return;
                                     var offset = elem.offset()
                                    elem.attr('data-top', offset.top);
                                }
                                if (elem.attr('data-top') <= $(this).scrollTop() )
                                    elem.addClass('navbar-fixed-top');
                                else
                                    elem.removeClass('navbar-fixed-top');
                                });
                            
                            </script>
                        <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Tên</label>
                            <div class="col-sm-10">
                              <input   maxlength="128"  type="text" name="data[name]" class="form-control" value="<?php echo htmlspecialchars($obj->name);?>">
                            </div>
                          </div>
                        <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Thứ tự (hiện trang chủ)</label>
                            <div class="col-sm-10">
                                <input placeholder="" type="text" name="data[sort_order]" class="form-control" value="<?php echo $obj->sort_order;?>">
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-sm-2 control-label">Hiện trang chủ</label>
                                <div class="col-sm-6">
                                  <label class="switch">
                                    <input onchange="if(this.checked) $('.select-data-type').show();else $('.select-data-type').hide();" name="data[show_home]"  value="1" <?php if($obj->show_home==1) echo 'checked=checked';?>  type="checkbox">
                                    <span></span>
                                  </label>
                                </div>
                              </div>
                        <div class="form-group has-success select-data-type"  style="<?php if(!$obj->show_home) echo "display:none;"; ?>">
                            <label class="col-sm-3 control-label">
                                <input type="radio" value="latest" <?php if($obj->type=="latest") echo "checked=checked";?> onchange="on_type_select(this.value);" name="data[type]" />Hiện theo tin mới nhất
                            </label>
                            <label class="col-sm-3 control-label">
                                <input type="radio" value="topview" <?php if($obj->type=="topview") echo "checked=checked";?> onchange="on_type_select(this.value);" name="data[type]" />Hiện theo tin xem nhiều nhất
                            </label>
                            <label class="col-sm-3 control-label">
                                <input type="radio" value="tags" <?php if($obj->type=="tags") echo "checked=checked";?> onchange="on_type_select(this.value);" name="data[type]" />Hiện video theo từ khóa
                            </label>
                            <label class="col-sm-3 control-label">
                                <input type="radio" value="optical" <?php if($obj->type=="optical") echo "checked=checked";?> onchange="on_type_select(this.value);" name="data[type]" />Chọn video
                            </label>
                            <script>
                            function on_type_select(type){
                                if(type=='optical') $("#select-data-type-item").show();
                                else $("#select-data-type-item").hide();
                                if(type=='tags') $(".tag-row-panel").show();
                                else $(".tag-row-panel").hide();
                                
                            }
                            </script>
                        </div>
                            <div class="form-group has-success" id="select-data-type-item" style="<?php if($obj->type!='optical') echo "display:none;"; ?>">
                                <?php 
                                $videos=array();
                                if($obj->id){
                                $vsection=$this->load->model("vsection_model");
                                 $vsection->getDB()->select("video.*");
                                $vsection->getDB()->from("vsection_video");
                                $vsection->getDB()->join("video","video.id=vsection_video.video_id");
                                $vsection->getDB()->where(array('section_id'=>$obj->id,'vsection_video.show_home'=>1));
                                $videos=$vsection->getDB()->get()->result();
                                }
                                $viewpatch=  str_replace("admincp/", "", base_url()) ;
                                ?>
                                <ul id="video-list-panel">
                                    <?php foreach ($videos as $v) { ?>
                                    <li id="vid-row-<?php echo $v->id;?>">
                                        <img width="50" src="<?php echo $this->config->item('img_path')."wxhxt".($v->image?$v->image:"/no_team.png");  ?>"/>
                                        <input type="hidden" name="video[]" value="<?php echo $v->id; ?>"  />
                                        <?php echo $v->name; ?>
                                        <a style="text-decoration: underline;" href="<?php echo $viewpatch."videoapi/review/".$v->code.".html" ?>" target="_blank">Xem</a>
                                        <a style="text-decoration: underline;" onclick="remove_this_row('vid-row-<?php echo $v->id;?>');return false;" href='#'>Xóa</a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <input type="button" value="+Mời bạn thêm video" onclick="show_dialog('videos/dialog_search/on_select_video?ajax=1')"/>
                                <script>
                                                            function radiochange(type){
                                                                if(type=='video') $('#select-video-panel').show();
                                                                else $('#select-video-panel').hide();
                                                            }
                                                        function on_select_video(id,code,title,image){ 
                                                            //$("#txt-id-video-code").val(code);
                                                            if($("#vid-row-"+id).length>0) return;
                                                            var html ="<li id='vid-row-"+id+"'>";
                                                                if(image)
                                                                html+="<img width=\"50\" src=\"<?php echo $this->config->item('img_path')."wxhxt";?>"+image+"\"/>";
                                                                html += "<input type=\"hidden\" name=\"video[]\" value=\""+id+"\"  />"+title;
                                                                html+="<a style=\"text-decoration: underline;\" href=\"<?php echo $viewpatch."videoapi/review/";?>"+code+".html\"  target=\"_blank\">Xem</a>";
                                                                 html+="<a style=\"text-decoration: underline;\" onclick=\"remove_this_row('vid-row-"+id+"');return false;\" href='#'>Xóa</a>";
                                                                html+="</li>";
                                                            
                                                            $("#video-list-panel").append(html);
                                                            //$(".ui-dialog-content").dialog("close");
                                                        }
                                                        function remove_this_row(id){
                                                            $("#"+id).remove();
                                                        }
                                </script>
                                <style>
                                #select-data-type-item > ul {
                                    border: 1px solid #ccc;
                                    list-style: none outside none;
                                    padding: 10px;
                                  }
                                  #select-data-type-item > ul li a{
                                      margin-left:10px;
                                  }
                                    
                                </style>
                            </div>
                            <div class="form-group tag-row-panel" style="<?php if($obj->type!='tags') echo "display:none;"; ?>">
                                <label class="col-sm-2 control-label">Từ khóa</label>
                                                    <?php 
                                                     /* @var $tagmodel tags_model */
                                                    $tagmodel=$this->load->model("tags/tags_model");
                                                    
                                                    $toptags=$tagmodel->get_top_tag();
                                                    $s=array();
                                                    foreach ($toptags as $t) {
                                                        $s[]="'".$t->name."'";
                                                    }
                                                    $toptags=  implode(",", $s);
                                                    
                                                    $this->data['toptags']=  implode(",", $s);
                                                    
                                                    $tags=$tagmodel->get_tag_by_object($obj->id,"vsection");
                                                    $tagtext=array(); 
                                                    if(count($tags)) foreach($tags as $t){
                                                        $tagtext[]=$t->name;
                                                    } ?>
                                                    <input type="hidden" id="select2-tags" class="col-sm-10" name="tags"  value="<?php echo implode(",", $tagtext); ?>"/>
                                                    <script>
                                                    $(document).ready(function() {
                                                        $("#select2-tags").select2({
                                                            tags: [<?php echo $toptags; ?>],
                                                            tokenSeparators: [","],
                                                            multiple: true,
                                                        });
                                                    });
                                                    </script>
                            </div>
                            <div class="form-group has-success select-data-type">
                                <?php $typelist=array("horizontal"=>"horizontal","list"=>"list"); ?>
                            <label class="col-sm-2 control-label">Kiểu hiện</label>
                            <div class="col-sm-3">
                                <select class="form-control" name="data[template]">
                                    <?php foreach($typelist as $t=>$v){?>
                                    <option <?php if($obj->template==$t)echo "selected=selected"; ?> value="<?php echo $t;?>"><?php echo $t;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                             <div class="form-group">
                                <label class="col-sm-6 control-label">Trạng thái</label>
                                <div class="col-sm-6">
                                  <label class="switch">
                                    <input name="data[status]"  value="1" <?php if($obj->$status_field==1) echo 'checked=checked';?>  type="checkbox">
                                    <span></span>
                                  </label>
                                </div>
                              </div>
                            <div style="float: right; clear: both; width: 217px;">
                                                <div class="form-group" style="margin-bottom: 0px;">
                                                    <label class="control-label"><strong>ICON</strong></label>
                                                    
                                                </div>
                                                <div class="form-group image-img-upload">	
                                    <!--<input width="200" type="file" class="filestyle" name="files[]" data-icon="false" data-classButton="btn btn-default" id="thumb-img-file-upload" data-classInput="form-control inline input-s" data-url="<?php echo base_url() ?>uploadfile/multi"  multiple="multiple"/>-->
                                                    <span class="btn btn-s-md btn-default fileinput-button">
                                                        <span> Chọn ảnh  36x33 </span>
                                                        <a title="<div>Kích thước chuẩn:<div> <div>36x33  - .png .gif</div>Dung lượng tối đa: 10kb <b>Nền trong suốt hoặc nền đen #2A2A2A</b>" >
                                                            <input id="img-file-upload" type="file" name="files[]" data-url="<?php echo base_url() ?>uploadfile/multi"/></a>
                                                    </span>
                                                    <style>
                                                        .img-editor img{
                                                            width:100%;
                                                            max-height: 180px;
                                                        }
                                                        
                                                    </style>
                                                    <div class="progress progress-sm m-t-sm">
                                                        <div class="progress-bar progress-bar-info" style="width: 0%"></div>
                                                    </div>
                                                    <div id="img-thumb-panel" class="img-thumb-panel">
                                                        <div class="col-sm-12 img-editor" style="text-align:center; margin-top: 5px;">
                                                        <?php if ($obj->image) { ?>
                                                            
                                                               <img src="<?php echo $this->config->item("img_path")."wxhxt".$obj->image;?>"/>
                                                            
                                                        <?php } ?>
                                                        </div>
                                                        <input  class="input-file-path" type="hidden" name="data[image]" checked="checked" value="<?php echo $obj->image; ?>"/>
                                                    </div>
                                                    <script>
                                                        //*************upload thumb*******************************/
                                                         $(document).ready(function(e) {
                                                             $('.image-img-upload  #img-file-upload').fileupload({
                                                                 dataType: 'json',
                                                                 done: function(e, data) {
                                                                     if(data.result.status=='0'){
                                                                         alert(data.result.error);
                                                                     }else{
                                                                     $.each(data.result.files, function(index, file) {
                                                                         //countimg++;
                                                                         if($('.image-img-upload  .img-editor img').length>0)
                                                                         $('.image-img-upload   .img-editor img').attr("src",img_path+"wxhxt"+file.url);
                                                                        else $(".image-img-upload   .img-editor").append($("<img />").attr("src",img_path+"wxhxt"+file.url));
                                                                         $(".image-img-upload .input-file-path").val(file.url);
                                                                     });
                                                                     }
                                                                     $('.image-img-upload .progress .progress-bar').css('width', '0%');
                                                                     $('.image-img-upload  .progress').hide();
                                                                     

                                                                 },
                                                                 progressall: function(e, data) {
                                                                     var progress = parseInt(data.loaded / data.total * 100, 10);
                                                                     $('.image-img-upload .progress').show();
                                                                     $('.image-img-upload  .progress .progress-bar').css('width', progress + '%');
                                                                 }
                                                             });
                                                         });
                                                      </script>
                                                </div>
                                                    </div>
                            <div style="clear:both;"></div>

                        </div>
                        
                        <div id="" class="col-sm-12">
                            <div class="line line-dashed line-lg pull-in"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                               <input onclick="update_form($('#frm_add_edit'));" class="btn btn-primary" type="button" value="Lưu lại"/>
                              <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');return false;">Đóng</button>
                            </div>
                          </div>
                      </div>
                        </div>
                      
                    </div>
                  </section>
                        </form>
                </div>
                
              </div>
              
            </section>
          </section>
          <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
        </section>
<script type='text/javascript'>
    function on_form_submit(form){
        //code on submit here
        //return false;
    };
	//CKEDITOR.replace('content_here');
        
        /**global javascript**/
        $(function() {
    
            $( ".input-sortable" ).sortable({
              placeholder: "ui-state-highlight"
            });

          });

</script>
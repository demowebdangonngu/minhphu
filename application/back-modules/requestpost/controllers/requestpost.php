﻿<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/ControllerAdmin.php";

class Requestpost extends YT_ControllerAdmin {
	
	function __construct() {
		error_reporting(0);
		$headers=array();
 
		//$headers[] = "Accept-Encoding: gzip, deflate, sdch" ;
		$headers[] = 'Content-Type: text/xml; charset=utf-8';
		$headers[] = "Accept-Language: vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2" ;
		$headers[] = "User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/47.0 Chrome/41.0.2272.127_coc_coc Safari/537.36" ;
		$headers[] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" ;
		$headers[] = "Referer: http://google.com.vn/" ;
		// $headers[] = "Cookie: " ;
		$headers[] = "Connection: keep-alive";
    }
	function get_data($url,$headers=array(),$type="",$data=array()) {
		 $ch = curl_init();
		 $timeout = 5;
		 curl_setopt($ch, CURLOPT_URL, $url);
		 if($type=="POST")
		 curl_setopt($ch, CURLOPT_POST, 1);
	 if($data)
		 
		 curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query($data));
		 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		 
		 //--compressed
		 curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		 //return header
		 //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:'));
		 curl_setopt($ch, CURLOPT_HEADER, 1);
		 $data = curl_exec($ch);
		 
		 $response = curl_exec($ch); 
		 $result=array();
		 list($result['header'], $result['body']) = explode("\r\n\r\n", $data, 2);
		 //print_r($header_text);exit;
		 curl_close($ch);
		 return $result;
	}
	
	function get_id_url($url,$sep="-"){
		$url=str_replace(".html", "", $url);
		$url=explode($sep, $url);
		return intval($url[count($url)-1]);
	}
	
	private function download_file($url) {
	// echo $url;exit;
        $info = pathinfo($url);
        // print_r($info);exit;

        $saveto = "/home/phut91/tmp/";
        $newpath = $saveto . $info['basename'];
        $ch = curl_init($url);
        $fp = fopen($newpath, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);
        //$newpath;
        $config = $this->config->load('upload', true);
        $this->load->library('ftp');
        $this->ftp->connect($config);

        $dir = date("/Y/m/d", time());
        $this->ftp->mkdir_recu($dir);
        $file_name = $info['filename'];
        $path = $dir . '/' . $file_name . "." . $info['extension'];
        $seg = 0;
        while ($this->ftp->file_exist($path)) {
            $seg++;
            $path = $dir . '/' . $file_name . "-$seg." . $info['extension'];
        }
		// print_r($path);exit;
        $this->ftp->upload($newpath, $path);
        if ($this->ftp->error_message) {
            echo $this->ftp->error_message;
            return 0;
        } else {
            if (function_exists("getimagesize")) {
                if (!$files) {
                    require_once APPPATH . "back-modules/uploadfile/models/files_model.php";
                    $files = new files_model();
                }
                list($width, $height, $type, $attr) = getimagesize($newpath);
                $files->store_data(array('name' => $file_name, 'ext' => $info['extension'], 'path' => $path,
                    'size' => $data['size'],
                    'w' => $width, 'h' => $height));
                @unlink($newpath);
            }
        }
        return $path;
    }
	
    function post_danviet(){
		$posts = $this->load->model("posts/Posts_model");
		$tags=$this->load->model('tags/tags_model');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$data= $this->get_data("http://danviet.vn/the-thao",$headers);
		echo "dân việt \n";
		$DOM = new DOMDocument;
		$DOM->loadHTML($data['body']);
		$link = array();
		//hot
		preg_match_all('/<div class="imgcm1">(.*?)<\/[\s]*div>/s', $data['body'], $item_hot);
		preg_match_all('/<img src="(.*?)"(.*?)/s', $item_hot[1][0], $image_hot);
		preg_match_all('/<a href="(.*?)".*?<\/[\s]*a>/s', $item_hot[1][0], $link_hot);
		$id_request[] = $this->get_id_url($link_hot[1][0]);
		$link[] = $link_hot[1][0];
		$images[] = $image_hot[1][0];
		//banner
		for($i = 0; $i<=2; $i++){
			if($i == 0){
				$preg = '/<div class="boxClipItemcm">(.*?)<\/[\s]*div>/s';
			}else{
				$preg = '/<div class="boxClipItemcm'.$i.'">(.*?)<\/[\s]*div>/s';
			}
			preg_match($preg, $data['body'], $item_banner);
			preg_match('/<a href="(.*?)".*?<\/[\s]*a>/s', $item_banner[1], $link_banner);
			preg_match('/<img src="(.*?)"(.*?)/s', $item_banner[1], $image_banner);
			$link[] = $link_banner[1];
			$id_request[] = $this->get_id_url($link_banner[1]);
			$images[] = $image_banner[1];
		}
		
		preg_match_all('/<div class="listNewscm1" >(.*?)<\/[\s]*div>/s', $data['body'], $tin_khacs);
		foreach ($tin_khacs[1] as $tinkhac) {
			preg_match('/<a href="(.*?)".*?<\/[\s]*a>/s', $tinkhac, $link_tinkhac[]);
			preg_match('/<img src="(.*?)"(.*?)/s', $tinkhac, $image_tinkhac);
			$images[] = $image_tinkhac[1];
		}
		foreach ($link_tinkhac as $value_tinkhac) {
			$link[] = $value_tinkhac[1];
			$id_request[] = $this->get_id_url($value_tinkhac[1]);
		}
		foreach($images as $i){
			$array_image[]=$this->download_file($i);
		}
		$stt = -1;
		foreach ($link as $value) {
			$stt++;
			$posts->where(array('id_request'=>$id_request[$stt]));
			$id_post = $posts->get()->result();
			if(!$id_post){
				$data1 = '';
				$html = "http://danviet.vn".$value;
				echo "|--->".$html." \n";
				$data1 = $this->get_data($html,$headers);
				$DOM = new DOMDocument;
				$DOM->loadHTML($data1['body']);
				preg_match_all('/<div class="Bigtieudebaiviet">(.*?)<\/[\s]*div>/s', $data1['body'], $title);//$title[1][0]
				preg_match_all('/<div class="sapobaiviet">(.*?)<\/[\s]*div>/s', $data1['body'], $div_title_short);
				preg_match_all('/<h3>(.*?)<\/[\s]*h3>/s', $div_title_short[1][0], $title_short);//$title_short[1][0]
				preg_match_all('/<div class="contentbaiviet">(.*?)<\/[\s]*div>/s', $data1['body'], $content);//$content[1][0]
				preg_match_all('/<div class="tukhoa">(.*?)<\/[\s]*div>/s', $data1['body'], $div_tags);
				preg_match_all('/<a href="(.*?)" title="(.*?)">(.*?)<\/[\s]*a>/s', $div_tags[1][0], $list_tags);//$tags[2]
				$array = array();
				$array = array(
					'title' => $title[1][0],
					'summary' => $title_short[1][0],
					'content' => $content[1][0],
					'uid' => 44,
					'name_request' => "danviet",
					'id_request' => $id_request[$stt],
					'image' => $array_image[$stt],
				);
				$id = '';
				$id = $posts->store_data($array);
				if($id){
					$tags->insert_tags_for_object($list_tags[3], $id);
				}
			}
			
		}
		$this->post_vnmedia();
		$this->post_sggp();
		exit;
	}
	
	function post_vnmedia(){
		$posts = $this->load->model("posts/Posts_model");
		$tags=$this->load->model('tags/tags_model');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		$data= $this->get_data("http://vnmedia.vn/HomeRss/Rssid_27.xml",$headers);
		echo "vnmedia \n";
		$DOM = new DOMDocument;
		$DOM->loadHTML($data['body']);
		preg_match_all('/<item>(.*?)<\/[\s]*item>/s', $data['body'], $items);
		foreach($items[1] as $item){
			preg_match('/<LastEditDate>(.*?)<\/[\s]*LastEditDate>/s', $item, $date);
			preg_match("/([0-9]{4})\/([0-9]{2})\/([0-9]{2})/", $date[1], $matches);
			$getdate = date('Y/m/d', strtotime('+0 days' ));
			if($matches[0] == $getdate){
				preg_match('/<link>(.*?)<\/[\s]*link>/s', $item, $link);
				$links[] = $link[1];
				$id_request[] = $this->get_id_url($link[1]);
				preg_match("/<img src='(.*?)'(.*?)/s", $item, $image);
				$images[] = $image[1];
			}
		}
		foreach($images as $i){
			$array_image[]=$this->download_file($i);
		}
		$stt = -1;
		foreach($links as $l){
			$stt++;
			$posts->where(array('id_request'=>$id_request[$stt]));
			$id_post = $posts->get()->result();
			if(!$id_post){
				$data1 = '';
				echo "|--->".$l." \n";
				$data1 = $this->get_data($l,$headers);
				$DOM = new DOMDocument;
				$DOM->loadHTML($data1['body']);
				preg_match_all('/<h1 class="title">(.*?)<\/[\s]*h1>/s', $data1['body'], $title);//$title[1][0]
				preg_match_all('/<div class="content">(.*?)<\/[\s]*div>/s', $data1['body'], $content);//$content[1][0]
				preg_match('/<FONT size=2 face=Arial>(.*?)<[\s]*BR>/s', $content[1][0], $title_short);//$title_short[1]
				preg_match('/<div class="article-tags">(.*?)<\/[\s]*div>/s', $data1['body'], $div_tag);
				preg_match_all("/<a href='(.*?)'>(.*?)<\/[\s]*a>/s", $div_tag[1], $list_tags);
				$array = array();
				$array = array(
					'title' => $title[1][0],
					'summary' => $title_short[1],
					'content' => $content[1][0],
					'uid' => 44,
					'name_request' => "vnmedia",
					'id_request' => $id_request[$stt],
					'image' => $array_image[$stt],
				);
				$id = '';
				$id = $posts->store_data($array);
				if($id){
					$tags->insert_tags_for_object($list_tags[2], $id);
				}
			}
		}
		// exit;
	}
	
	function get_name_link($i){
		$daytext = array(   
			'0'   => 'http://www.sggp.org.vn/thethao/bongdaquocte/',
			'1'   => 'http://www.sggp.org.vn/thethao/vleague/',
			'2'   => 'http://www.sggp.org.vn/tennis/',
		);
		return $daytext[$i];
	}
	
	function post_sggp(){
		echo "sggp \n";
		$posts = $this->load->model("posts/Posts_model");
		$tags=$this->load->model('tags/tags_model');
		date_default_timezone_set('Asia/Ho_Chi_Minh');
		for($a = 0; $a <=2; $a++){
			$html = $this->get_name_link($a);
			$data= $this->get_data($html,$headers);
			echo "|- ".$html." \n";
			$DOM = new DOMDocument;
			$DOM->loadHTML($data['body']);
			preg_match_all('/<div class="newscat-hotest">(.*?)<\/[\s]*div>/s', $data['body'], $item_f);
			preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $item_f[1][0], $date_f);
			$getdate = date('d/m/Y', strtotime('+0 days' ));
			if($getdate == $date_f[0]){
				preg_match('/<a href="(.*?)" class="newscat-hotest-title">(.*?)<\/[\s]*a>/s', $item_f[1][0], $link_f);
				$links[] = $link_f[1];
				$titles[] = $link_f[2];
				preg_match_all("/([0-9]{6})/", $link_f[1], $id_f);
				$id_request[] = $id_f[1][0];
				preg_match('/<FONT face=(arial||Arial)>(.*?)<\/[\s]*FONT>/s', $item_f[1][0], $title_short_f);
				$title_shorts[] = $title_short_f[2];
				preg_match('/<IMG SRC="(.*?)"(.*?)<\/[\s]*a>/s', $item_f[1][0], $image);
				$array_image[]=$this->download_file("http://www.sggp.org.vn".$image[1]);
			}
			preg_match_all('/<div class="newscat-lst">(.*?)<\/[\s]*div>/s', $data['body'], $item_m);
			foreach($item_m[1] as $item){
				preg_match("/([0-9]{2})\/([0-9]{2})\/([0-9]{4})/", $item, $date_m);
				if($getdate == $date_f[0]){
					preg_match('/<a href="(.*?)" class="newscat-hotest-title">(.*?)<\/[\s]*a>/s', $item, $link_m);
					$links[] = $link_m[1];
					$titles[] = $link_m[2];
					preg_match_all("/([0-9]{6})/", $link_m[1], $id_m);
					$id_request[] = $id_m[1][0];
					preg_match('/<FONT face=(arial||Arial)>(.*?)<\/[\s]*FONT>/s', $item, $title_short_m);
					$title_shorts[] = $title_short_m[2];
					preg_match('/<IMG SRC="(.*?)"(.*?)<\/[\s]*a>/s', $item, $image);
					$array_image[]=$this->download_file("http://www.sggp.org.vn".$image[1]);
					// http://www.sggp.org.vn//dataimages/original/2015/08/images572243_802.jpg
				}
			}
			$stt = -1;
			foreach($links as $row){
				$stt++;
				$posts->where(array('id_request'=>$id_request[$stt]));
				$id_post = $posts->get()->result();
				if(!$id_post){
					$data1 = '';
					echo "|--->http://www.sggp.org.vn".$row." \n";
					$data1 = $this->get_data("http://www.sggp.org.vn".$row,$headers);
					$DOM = new DOMDocument;
					$DOM->loadHTML($data1['body']);
					preg_match_all('/<div class="news-content">(.*?)<\/[\s]*div>/s', $data1['body'], $div_content);
					$array = array();
					$array = array(
						'title' => $titles[$stt],
						'summary' => $title_shorts[$stt],
						'content' => $div_content[1][0],
						'uid' => 44,
						'name_request' => "sggp",
						'id_request' => $id_request[$stt],
						'image' => $array_image[$stt],
					);
					$id = '';
					$id = $posts->store_data($array);
				}
			}
		}
		// exit;
	}
	
	function quay(){
		$i = 1;
		// for($i= 0; $i<= 10000; $i++){
			$rd[$i] = $this->get_random_string(8);
			$gmail = $rd[$i].$i.'@gmail.com';
			echo $gmail." \n";
			$headers1[] = 'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8';
			// $headers1[] = 'Accept-Encoding:gzip, deflate, sdch';
			$headers1[] = 'Accept-Language:vi-VN,vi;q=0.8,fr-FR;q=0.6,fr;q=0.4,en-US;q=0.2,en;q=0.2';
			// $headers1[] = 'Cache-Control:max-age=0';
			// $headers1[] = 'Connection:keep-alive';
			$headers1[] = 'Cookie:PHPSESSID=6aa33adef1e25d171f662f37b6dc1923; SITE_ORDERCOOKIES=6L0GA7T173Q89I8N25GH; SITE_VISITOR=1; TEMP_PAGESUITE_FP_TOKEN=907I12254VP96X75K0UL0; _ga=GA1.2.1704315228.1439952710; _gat=1';
			$headers1[] = 'Host:nobita.vn';
			$headers1[] = 'Referer:http://nobita.vn/';
			$headers1[] = 'Upgrade-Insecure-Requests:1';
			$headers1[] = 'User-Agent:Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/50.0.125 Chrome/44.0.2403.125 Safari/537.36';
			$data= $this->get_data("http://nobita.vn/users/register/index.html",$headers, "POST", array('email'=>$gmail,'password'=>'123123','repassword'=>'123123','gender'=>1,'fullname'=>'quay pha nobita','telephone'=>'0909696969'));
			$DOM = new DOMDocument;
			$DOM->loadHTML($data['body']);
			if($data['body']){
				echo "1 \n";
			}else{
				echo "0 \n";
			}
		// }
		print_r($data['body']);exit;
	}
	function get_random_string($length)
	{
		$valid_chars = '1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
		// start with an empty random string
		$random_string = "";

		// count the number of chars in the valid chars string so we know how many choices we have
		$num_valid_chars = strlen($valid_chars);

		// repeat the steps until we've created a string of the right length
		for ($i = 0; $i < $length; $i++)
		{
			// pick a random number from 1 up to the number of valid chars
			$random_pick = mt_rand(1, $num_valid_chars);

			// take the random character out of the string of valid chars
			// subtract 1 from $random_pick because strings are indexed starting at 0, and we started picking at 1
			$random_char = $valid_chars[$random_pick-1];

			// add the randomly-chosen char onto the end of our string so far
			$random_string .= $random_char;
		}

		// return our finished random string
		return $random_string;
	}
	
}
//http://m15.mytvnet.vn/vmplive.m3u8?c=vstv036&q=medium&deviceType=1&userId=12214&deviceId=41939&type=tv&p=0&zn=net&token=7XdoA0LK_7mnByo6Blu37A&time=1435754737&pkg=pkg3&profiles=medium&location=&from=tv.anluong.info
//http://m15.mytvnet.vn/vmplive.m3u8?c=vstv036&q=medium&deviceType=1&userId=5301&deviceId=41538&type=tv&p=0&zn=net&token=_Y5QIZ_xjJLhQz-TgtdzEw&time=1435755696&pkg=pkg3&profiles=medium&location=&from=tv.anluong.info
?>
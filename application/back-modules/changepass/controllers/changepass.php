<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Changepass extends MX_Controller {
	
	function __construct(){
		parent::__construct();	
		$this->load->model('changepass_model');			
		$this->load->helper("url");
		$this->config->load('define',TRUE);
			
		
		// kiem tra phan quyen
		$this->load->library(array('adminclass_model'));
		$this->module_name			= $this->uri->segment(1);
		$user_group_id				= $this->session->userdata('admin_group_id');//1;	
		$this->adminclass_model->permission_login($user_group_id,$this->module_name);			
	}
	
	function index(){	

		$data['password']		= $this->input->post('password', TRUE);		
		$data['re_password']	= $this->input->post('re_password', TRUE);	
		$data['id']				= $this->input->post('id', TRUE);	
			
		
		if(@$data['id']	==0) 	$data['id'] = $this->session->userdata('admin_id');
		// Update data
		if($_SERVER['REQUEST_METHOD'] 		== 'POST') {		
		
			if(  empty($data['password']) || 	empty($data['re_password'])  ) {
				$this->session->set_flashdata('error_admin','Please input values passwords');		
				$this->re_load();               
			}else if( $data['password']	 		!= $data['re_password']  ) {
				$this->session->set_flashdata('error_admin',"Password phải trùng nhau");
				$this->re_load();
               
			}else {				
				$res	= $this->changepass_model->update($data);
				$kq		= $res ? true : false;
				if( $kq ) {
					// add log
					$idobj		= $this->session->userdata('admin_id');
					$action		= "update";
					$this->add_log($idobj,$action);
					// en log
					$this->session->set_flashdata('error_admin', LANG_UPDATE_SUCCESS);
					redirect($this->module_name, 'refresh');
				}else {
					$this->session->set_flashdata('error_admin',  LANG_UPDATE_FAILED);
				}
			}
		}		
		
		$this->template->build('add', $data);

	}

	function re_load(){				
		redirect($this->module_name, 'refresh');
	}

	public function add_log($idobj=0,$action="add"){
		// lay thong tin log
		$description			= $this->changepass_model->getStrLog($idobj);		
		$data					= array();
		$data['idobj']			= $idobj;		
		$data['description']	= $description;
		$data['action']			= $action;//var_dump($data);
		$this->adminclass_model->add_logmodule($data);
	}

	
	

}
?>
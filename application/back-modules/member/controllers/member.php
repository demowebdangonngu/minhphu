<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/ControllerAdmin.php";

class Member extends YT_ControllerAdmin {

    function __construct() {
        parent::__construct();
        $this->module_name = $this->uri->segment(1);
        $this->table_name = "member";

        $this->config->load('define', TRUE);
        $this->load->library('adminclass_model');
        $this->model = $this->load->model('member/member_model', 'ACT', TRUE);
    } 
    public function index() { 
        return parent::index();
    }
 

}

?>
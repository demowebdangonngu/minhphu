<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class member_model extends YT_AModel {
	public function __construct(){  
		parent::__construct();  
//id 	username 	email 	password 	salt 	gid 	name 	lastlogin 	ip_connection 	joindate 	avatar 	slug 	permit 	publish

        $this->fields=array (
          'id' => 'PRI',
          'name' => 1,
          'email' => 1,
          'status' => '1'
        ); 
         $this->table_name='member'; 
         $this->id_field='id';     
	}
     
}
?>
<script>
    var module = '<?php echo $this->uri->segment(1); ?>';
    var base_url = '<?php echo base_url(); ?>';
</script>
<section id="content">


    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">

                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>"><?php
                        if ($module_title) {
                            echo $module_title;
                        } else {
                            echo $this->uri->segment(1);
                        };
                        ?></a></li>
                <!--<li class="active">Static table</li>-->
            </ul>
            <div class="row">
                <div class="col-sm-6">
                    <form id="menu-sorter-form" data-validate="parsley" method="post" action="<?php echo base_url().$this->uri->segment(1); ?>">
                        <input type="hidden" name="cat_order" id="cat_order" value="">
                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <span class="h4">Danh sách màu sắc</span>
                                <ul class="nav nav-tabs pull-right">
                                    <li <?php echo (!$this->input->get("trash") ? "class='active'" : ""); ?>><a href="<?php echo base_url() . $this->uri->segment(1); ?>" ><i class="fa fa-file-text text-default"></i>&nbsp; Màu sắc</a></li>
                                    <li <?php
                                    echo ($this->input->get("trash") ? "class='active'" : "");
                                    ;
                                    ?>><a href="<?php echo base_url() . $this->uri->segment(1) . "?trash=1"; ?>" ><i class="fa fa-trash-o text-default"></i>&nbsp; Hiện màu sắc đã xóa</a></li>
                                </ul>
                            </header>
                            <div class="panel-body">
                                <div class="dd" id="categories" data-output="">
                                    <ol class="dd-list">
                                        <?php
                                        /* @var $catemodel Categories_model */
                                        $catemodel = $this->load->model("color_model");
                                        isset($_GET['trash']) ? $status_array = array(0, 1, 2) : $status_array = array(0, 1);
                                        $categories = $catemodel->get_tree($status_array);
                                        if (isset($categories)) {

                                            foreach ($categories as $cat) {
                                                ?>
                                                <li class="dd-item parent_category <?php echo "status_" . $cat->status; ?>" data-id="<?php echo $cat->id; ?>">
                                                    <div class="dd-handle"><?php echo $cat->name; ?></div>
                                                    <span class="pull-right" style="position: absolute;right: 0px;top: 6px;">
                                                        <?php ?>
                                                        <a title="Chỉnh sửa danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '?id=' . $cat->id; ?>"><i class="fa fa-pencil icon-muted fa-fw m-r-xs"></i></a>
                                                        <?php ?>
                                                        <?php if (!count($cat->children)) { ?>
                                                            <?php if ($cat->status != 2) { ?>
                                                                <a href="<?php echo base_url() . $this->uri->segment(1) . '/quick_trash?ids=' . $cat->id; ?>"><i class="fa fa-times icon-muted fa-fw"></i></a>
                                                            <?php } else { ?>
                                                                <a title="Hiện danh mục" href="<?php echo base_url() . $this->uri->segment(1) . '/update_status/' . $cat->id . '?status=1&return=1' ?>"><i class="fa icon-muted  fa-fw  fa-check-circle-o"></i></a>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </span>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ol>
                                </div>
                            </div>
                            <footer class="panel-footer text-right bg-light lter">
                                <?php ?>
                                <input type="button" onclick="update_form($('#menu-sorter-form'), update_menu_callback);
                                        return false;" class="btn btn-success btn-s-xs" value="Cập nhật thứ tự"/>
                                       <?php ?>
                            </footer>
                        </section>
                        <style>
                            .dd li.status_0 .dd-handle{
                                color: #bbb;
                                background: #F7F7F7;
                            }
                            .dd li.status_2 .dd-handle{
                                color: #bbb;
                                background: #FF6666;
                            }


                        </style>
                    </form>
                </div>
                <?php ?>
                <div class="col-sm-6">
                    <form id="add-insert-menu-frm" method="post" action="<?php echo base_url() . $this->uri->segment(1); ?>/addedit_process/" enctype="multipart/form-data">
                        <input type="hidden" name="data[id]" value="<?php echo $obj->id; ?>">
                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <span class="h4"><?php if ($obj->id) { ?>Sửa<?php } else { ?>Thêm<?php } ?> màu sắc</span>
                            </header>
                            <div class="panel-body">
                                <div id="msg_error_msg" style="color:#f00;"></div>
                                
                                <div class="form-group">
                                    <label>Tên màu sắc</label>
                                    <input vname="Tên màu sắc" type="text" name="data[name]" class="form-control" value="<?php echo $obj->name; ?>">
                                </div>
                                
                                <div class="form-group is_link_row">
                                    <label>Hình ảnh</label>
									<div class="input-append">
										<input id="fieldIDbanner" onChange="readURL_banner(this);" class="image_banner" type="text" name="data[image]" value="<?php echo $obj->image?$obj->image:''; ?>" style="height:33px" hidden>
										<a style="margin-left: 40%;" href="<?php echo $this->config->item('admin_url');?>themes/admincp/js/tinymce/filemanager/dialog.php?type=1&amp;field_id=fieldIDbanner" class="btn btn-primary iframe-btn" type="button">Select</a>
									</div>
									<img id="image_old_fieldIDbanner" src="" width="100%" height="100%" />
                                </div>
                               
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="margin-left: -13px;">Hoạt động</label>
                                    <div class="col-sm-7">
                                        <label class="switch">
                                            <input type="checkbox" value="1" name="data[status]" <?php if ($obj->status) { ?>checked<?php } ?>>
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <footer class="text-right bg-light lter">
                                    <input type="submit" class="btn btn-primary" value="<?php if (isset($obj->id)) { ?>Cập nhật<?php } else { ?>Thêm<?php } ?>"/>
                                </footer>
                            </div>
                        </section>
                    </form>
                </div>
                <?php ?>
            </div>
        </section>
    </section>
    <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
</section>
<script>
function responsive_filemanager_callback(field_id){
	var url=jQuery('#'+field_id).val();
	$('#image_old_'+field_id).attr('src', url);
}
responsive_filemanager_callback('fieldIDbanner');
$(document).ready(function (e) {
	$('.aut_img_banner').each(function (index, element) {
		$(this).error(function () {
			$(this).hide();
		});
	});
});
function readURL_banner(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader(); 
		reader.onload = function (e) {
			$('#blah_banner').attr('src', e.target.result).show();
		}
		reader.readAsDataURL(input.files[0]);
	}
} 
function delete_image_row(stt) {
    var row=$(".image_old_"+stt);
    row.remove();
	$.ajax({
		type: 'POST',
		url: "<?php echo base_url();?>categories/delete_image/",
		data: "image=1&id=<?php echo $obj->id;?>&loai="+stt,
		success: function() {
		}
	});
}
    $(document).ready(function ()
    {
        // activate Nestable for list 1
        if ($('#categories').length)
        {
            $('#categories').nestable({
                group: 0,
                maxDepth: 1
            }).on('change', updateOutput);
        }
        if ($('#menus').length)
        {
            $('#menus').nestable({
                group: 0,
                maxDepth: 1
            }).on('change', updateOutput);
        }
    });
    var change = false;
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target);
        //alert(window.JSON.stringify(list.nestable('serialize')));
        change = true;
        $("#cat_order").val(window.JSON.stringify(list.nestable('serialize')));
    };
    function update_menu_callback(data) {
        alert('đã cập nhật thành công!');
        change = false;
    }
    function update_form_callback(data) {
        if (data.status)
            window.location = base_url + module;
    }
    $(document).ready(function (e) {
    });
    window.onbeforeunload = function () {
        if (change)
            return 'Bạn vừa thay đổi vị trí của các danh mục bên trái, nếu thoát trang thì các vị trí sẽ không được lưu lại!';
    };
</script>
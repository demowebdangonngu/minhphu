<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class Color_model extends YT_AModel {

    function __construct() {
        parent::__construct();
        $this->fields = array(
            'id' => '1',
            'name' => '1',
            'status' => '1',
            'image' => 1,
            'order' => 1,
        );
        $this->fields_lang = array();
        $this->table_name = 'color';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->id_field_lang = '';
        $this->status_field = 'status';
        $this->cat_field = '';
        $this->order_field = 'order';
        $this->image_field = array();
        ///$this->id_lang_rel=$this->id_field;
    }

    public $obj_array = array();
    //public $tree_array=array();
    public $tree = array();

    function get_tree($status = array(0, 1, 2)) {
        sort($status);
        $key = implode("", $status);
        if (count($this->tree[$key]))
            return $this->tree[$key];
        $this->getDB()->select("*");
        $this->getDB()->from($this->table_name);
        $this->getDB()->where_in('status', $status);
        $this->getDB()->order_by('order');
        $list = $this->getDB()->get()->result();
        foreach ($list as $cat) {
            $this->obj_array[$key][$cat->id] = $cat;
            //$this->tree_array[$cat->id]=$cat;
            $this->obj_array[$key][$cat->id]->children = array();
            if ($cat->parent == 0) {//root node
                $this->tree[$key][$cat->id] = $cat;
            }
        }
        foreach ($this->obj_array[$key] as $keys => $obj) {
            if ($this->obj_array[$key][$obj->parent]) {
                $this->obj_array[$key][$obj->parent]->children[$obj->id] = $obj;
            }
        }
        return $this->tree[$key];
    }

    function get_node($id, $status = array(0, 1, 2)) {
        sort($status);
        $key = implode("", $status);
        if (!isset($this->tree[$key]))
            $this->get_tree($status);
        if ($this->obj_array[$key][$id]) {
            return $this->obj_array[$key][$id];
        }
        return null;
    }

    function store_tree($tree_array, $parent_id = 0) {
        $sort = 1;
        foreach ($tree_array as $cate) {
            $this->getDB()->update($this->table_name, array('order' => $sort++), array('id' => $cate->id));
            if (count($cate->children)) {
                $this->store_tree($cate->children, $cate->id);
            }
        }
    }

    function get_children_ids($id, $status = array(0, 1, 2)) {
        sort($status);
        $key = implode("", $status);
        if (!isset($this->tree[$key]))
            $this->get_tree($status);
        $ids = array();
        if ($this->obj_array[$key][$id]) {
            $ids[$id] = $id;
            foreach ($this->obj_array[$key][$id]->children as $c) {
                if ($c->id)
                    $ids = array_merge($ids, $this->get_children_ids($c->id, $status));
            }
        }
        return $ids;
    }

    function on_data_change() {
        /* @var $caches MY_Caches */
        $caches = $this->load->library('Caches');
        $caches->on_categories_update();
        //parent::on_data_change();
    }

    function listAll(){
        $this->getDB()->from($this->table_name);
        $this->getDB()->where(array('status'=>1));
		$this->getDB()->order_by('order','ASC');
        return $this->getDB()->get()->result();
    }

}

?>
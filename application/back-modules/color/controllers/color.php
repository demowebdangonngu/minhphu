<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require_once APPPATH."third_party/YT/ControllerAdmin.php";
class Color extends YT_ControllerAdmin {
	/**
     *
     * @var Test_model 
     */
	public $model;
        /**
         *
         * @var Genre_model
         */
        public $genre;
	function __construct()	{
		parent::__construct();	
		$this->model=$this->load->model('color_model');	
	}
        function addedit_process() {
      
			if(!$_POST['data']['status']){
				$_POST['data']['status'] = 0;
			}
			if($_POST['data']['id'] == ''){
				unset($_POST['data']['id']);
			}
            parent::addedit_process();
        }
		function delete_image(){
			if($_POST['image'] == 1){
				if($_POST['loai'] == 1){
					$data = array(
						'image_home' => '',
					);
				}else{
					$data = array(
						'image_page' => '',
					);
				}
				
				$this->model->getDB()->where('id', $_POST['id']);
				$this->model->getDB()->update('color', $data); 
			}
		}
        function on_after_addedit_process($id) {
            redirect("color", 'refresh');
            //if(!$_POST['data']['show']) $_POST['data']['show']=0;
            //if(!$_POST['data']['status']) $_POST['data']['status']=0;
            parent::on_after_addedit_process($id);
            
        }
        function index() {
            if(isset($_POST['cat_order'])){
//                echo "<pre>";
//                print_r(json_decode($_POST['cat_order']));
//                echo "</pre>";
//                exit;
                $array=json_decode($_POST['cat_order']);
                if(count($array)){
                    $this->model->store_tree($array);
                    $this->model->on_data_change();
                }
                echo json_encode(array('status'=>1));exit;
            }
            if(intval($_GET['id'])){
                $this->model->where['id']=intval($_GET['id']);
                $this->data['obj']=$this->model->get_object_by_where();
            }
            parent::index();
        }
}
 ?>
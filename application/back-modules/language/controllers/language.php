<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require_once APPPATH."third_party/YT/ControllerAdmin.php";
class Language extends YT_ControllerAdmin {
	/**
     *
     * @var Test_model 
     */
	public $model;
	function __construct()	{
		parent::__construct();	
		$this->lang->load( 'global', $this->config->item('language_admin'));
		$this->load->library('form_validation');
		$this->model=$this->load->model('language_model');				
			
		$this->module_name		= $this->uri->segment(1);
		//$this->config->load('define',TRUE);
	}
}
 ?>
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class Language_model extends YT_AModel {
       function __construct() {
            parent::__construct();
            
         $this->fields=array(
            'lang_id'=>'1',
            'code'=>'1',
            'image'=>'1',
            'name'=>'1',
            'default'=>'1'
        );
         $this->fields_lang=array(
            'llg_id'=>'1',
             'name'=>'1',
            'lang_id'=>'1',
        );
         $this->table_name='language';
         $this->table_name_lang='language_lang';
         $this->id_field='lang_id';
         $this->id_field_lang='llg_id';
         $this->status_field='';
         $this->cat_field='';
         $this->order_field='';
         $this->image_field=array('image'=>'');
         $this->id_lang_rel=$this->id_field;
        }
	
}
?>
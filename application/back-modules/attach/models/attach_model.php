<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class Attach_model extends YT_AModel {

    function __construct() {
        parent::__construct();
        /*
          id
          name
          slug
          parent
          ORDER
          SHOW
          STATUS
          TYPE
          forwc

         */
        $this->fields = array(
            'id' => 'PRI',
            'idModel' => 1,
            'nameModel' => 1,
            'name' => 1,
            'location' => 1,
            'idAdmin' => 1,
            'bannerName' => 1,
            'logoName' => 1,
            'loai' => 1,
            'status' => 1,
            'sort_order' => 1,
            'slug' => 1,
        );
        $this->fields_lang = array();
        $this->table_name = 'attach';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->id_field_lang = '';
        $this->status_field = 'status';
        $this->cat_field = '';
        $this->order_field = 'sort_order';
        $this->image_field = array();
        ///$this->id_lang_rel=$this->id_field;
    }

    function store_tree($tree_array) {
        $sort = 1;
        foreach ($tree_array as $cate) {
            $this->getDB()->update($this->table_name, array('sort_order' => $sort++), array('id' => $cate->id));
            if (count($cate->children)) {
                $this->store_tree($cate->children, $cate->id);
            }
        }
    }
    public function getList_database($where = false, $limit = false){
		$this->getDB()->select("*");
		$this->getDB()->from($this->table_name);
        if($limit)
            $this->getDB()->limit($limit);
       
        if($where)
            $this->getDB()->where($where);
		$return  = $this->getDB()->get()->result();
     //	 echo $this->getDB()->last_query(); exit;
       
          return $return;
	}
    
    
    public function insert_database($data){
            $this->getDB()->insert($this->table_name, $data); 
		 
	}
    public function update_database($data , $where){
	       $this->getDB()->where($where);
            $this->getDB()->update($this->table_name,$data);
	}
    
    function delete_database($where){
            $this->getDB()->where($where);
            $this->getDB()->delete($this->table_name);
    }

}

?>
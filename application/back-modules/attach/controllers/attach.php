<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require_once APPPATH."third_party/YT/ControllerAdmin.php";
class Attach extends YT_ControllerAdmin {
	/**
     *
     * @var Test_model 
     */
	public $model;
        /**
         *
         * @var Genre_model
         */
        public $genre;
        public $obImg;
        public $data_get;
        public $user_id;
	function __construct()	{
		parent::__construct();	
		$this->model=$this->load->model('Attach_model');	
                $this->obImg = $this->load->library("mgallery");
            
            
                $this->data_get = $this->input->get(); 
    		$this->data_get['idModel'] = $this->data_get['idModel']?$this->data_get['idModel']:0;
                
	}
        function addedit_process() {
            if($_POST['data']['slug']) $_POST['data']['slug']=  remove_accent ($_POST['data']['slug']);
            if(!$_POST['data']['id']){
                $this->model->getDB()->select("max(sort_order) as order_max");
                $this->model->getDB()->from($this->model->table_name);
                $obj=current($this->model->getDB()->get()->result());
                $_POST['data']['sort_order']=intval($obj->order_max)+1;
                unset($_POST['data']['id']);
            }
			
            // if (isset($_FILES['img']['name']) && $_FILES['img']['name'] != "") {
                // move_uploaded_file($_FILES['img']['tmp_name'], '../upload/images/images/' . time() . "_" . $_FILES['img']['name']);
                // $_POST['data']['location'] = time() . "_" . $_FILES['img']['name'];
                // $_POST['data']['name'] = $_FILES['img']['name'];
            // }
			// print_r($_POST['data']['location']);exit;
            $_POST['data']['idAdmin'] = $this->session->userdata('admin_id');
            parent::addedit_process();
        }
        function on_after_addedit_process($id) {
            redirect("attach", 'refresh');
            //if(!$_POST['data']['show']) $_POST['data']['show']=0;
            //if(!$_POST['data']['status']) $_POST['data']['status']=0;
            parent::on_after_addedit_process($id);
            
        }
        function index() {
            if(isset($_POST['menu_order'])){
//                echo "<pre>";
//                print_r(json_decode($_POST['cat_order']));
//                echo "</pre>";
//                exit;
                $array=json_decode($_POST['menu_order']);
                if(count($array)){
                    $this->model->store_tree($array);
                    $this->model->on_data_change();
                }
                echo json_encode(array('status'=>1));exit;
            }
            if(intval($_GET['id'])){
                $this->model->where['id']=intval($_GET['id']);
                $this->data['obj']=$this->model->get_object_by_where();
            }
            $this->model->order_by('sort_order');
            $list_attach = $this->model->get()->result();
            foreach($list_attach as $row){
            	
            		if($row->nameModel == 'logo'){
	                    $this->data['cat_attach']['logo'][] = $row;
	                }
	                if($row->nameModel == 'banner'){
	                    $this->data['cat_attach']['banner'][] = $row;
	                }	
            	
                
            }
//            $this->addedit_process())
            parent::index();
        }
		function delete_image(){
			if($_POST['image'] == 1){
				$data = array(
					'name' => '',
					'location' => '',
				);
				$this->model->getDB()->where('id', $_POST['id']);
				$this->model->getDB()->update('attach', $data); 
			}
		}
        public function listview(  )
        {     
            
            $_data['idModel'] = $this->data_get['idModel'];
            $_data['nameModel'] = $this->data_get['nameModel'];
      
            $arrAttach=$this->model->getList_database($this->data_get); 
            
            
            // SHow Img
            $this->obImg->upload_foldera = "../../upload/images/images/";
            $this->obImg->upload_folder = "../../upload/images/images/";
            $_data['images'] = $this->obImg->get_images_arr($arrAttach);
            $this->load->view('listview' , $_data);
       }
       
       
       public function add(){ 
                $data_post = $this->input->post(); 
                // Upload 
                if($data_post){  
                        if( $_arrNameImage=$this->obImg->upload() ) { 
                        	foreach($_arrNameImage as $value) { 
                        		// insert Array name image for  table( attach)
                                $attachName = $value['name'];
                                $location = $value['location']; 
                                $data_post['idModel'] = $this->data_get['idModel'];
                                $data_post['nameModel'] = $this->data_get['nameModel'];
                                $data_post['location'] = $location;
                                $data_post['name'] = $attachName;
                                
                                $data_post['idAdmin'] = $this->session->userdata('admin_id');
                                unset($data_post['fsubmit']);
								// print_r($data_post);exit;
                                $this->model->insert_database($data_post); 
                        	}
                        }
                        else 	
                        	echo "Khong Thanh Cong";                
                }
                $this->listview();
        }
        
        
        public function delete(){ 
            if($this->data_get['name']){
                //Xoa hinh trong thu muc
                $this->obImg->delete($this->data_get['name']); 
                //Xoa trong database
                $this->model->delete_database($this->data_get);
                unset($this->data_get['name']);
            } 
            $this->listview();
        }
}
 ?>
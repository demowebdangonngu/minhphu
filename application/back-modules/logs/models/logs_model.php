<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class logs_model extends YT_AModel {
    function __construct() {
            parent::__construct();
            
         $this->fields=array (
  'id' => 'pri',
  'name' => 1,
  'ip' => 1,
  'user_id' => 1,
  'create_date' => 1,
  'browser' => 1,
);
        /* $this->fields_lang=array(
            'id'=>'1',
             'name'=>'1',
            'description'=>'1',
            'content'=>'1',
        );*/
         $this->table_name='logs';
         $this->id_field='id';
         $this->status_field='status';
         $this->order_field='sort_order';
         $this->image_field=array('image'=>'');
         
        }
}
?>
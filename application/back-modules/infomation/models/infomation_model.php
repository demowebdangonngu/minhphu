<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/AModel.php";
class infomation_model extends YT_AModel {
	public function __construct(){  
		parent::__construct();  
//id 	username 	email 	password 	salt 	gid 	name 	lastlogin 	ip_connection 	joindate 	avatar 	slug 	permit 	publish

                $this->fields=array (
  'id' => 'PRI',
  'username' => 1,
  'address' => 1,
  'email' => 1,
  'hotline_1' => 1,
  'hotline_2' => 1,
  'hotline_3' => 1,
  'hotline_4' => 1,
  'fax' => 1,
);
        $this->fields_lang=array();
        $this->table_name='admin';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->cat_field = '';
        $this->order_field = '';
        $this->id_lang_rel = $this->id_field;
    }

    function on_data_change() {
        /* @var $caches MY_Caches */
        $caches = $this->load->library('Caches');
        $caches->on_post_update();
        //parent::on_data_change();
    }
}
?>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 require_once APPPATH."third_party/YT/ControllerAdmin.php";
class infomation extends YT_ControllerAdmin {
	/**
     *
     * @var Test_model 
     */
	public $model;
        /**
         *
         * @var Genre_model
         */
        public $genre;
	function __construct()	{
 
		parent::__construct();	
		$this->model=$this->load->model('infomation_model');	
		}
			function addedit_process() {
				
				parent::addedit_process();
			}

        function addedit() {
			 
			$id = $this->uri->segment(3);

            if($id){
                $this->model->where['id'] = $id;
                $this->data['obj']=$this->model->get_object_by_where();
				$data_post = $this->input->post(); 
				if($data_post){
					
					$id = $this->model->store_data($this->input->post("data"));
					if ($id) {
						$this->on_after_addedit_process($id);
					}
					redirect("infomation/addedit/1", 'refresh');
				}
				// $this->addedit();
            }
            parent::index();
        }
}
 ?>
﻿<script>
    var module = '<?php echo $this->uri->segment(1); ?>';
    var base_url = '<?php echo base_url(); ?>';
</script>
<section id="content" style="width: 100%;">
    <section class="vbox">
        <section class="scrollable padder">
            <div class="row">
                <div class="col-sm-12">
                    <form id='add-insert-menu-frm' name="form" method="post"  >
							
                        <input type="hidden" name="data[id]" value="<?php echo $obj->id; ?>">
                        <section class="panel panel-default">
                            <header class="panel-heading">
                                <span class="h4">Sửa thông tin website</span>
                            </header>
                            <div class="panel-body">
                                <div id="msg_error_msg" style="color:#f00;"></div>
                                <div class="form-group">
                                    <label>Name</label>
                                    <input name="data[username]" class="form-control" value="<?php echo $obj->username;?>"/>
                                </div>
                                <div class="form-group">
                                    <label>Địa chỉ</label>
                                    <input name="data[address]" class="form-control" value="<?php echo $obj->address;?>"/>
                                </div>
								<div class="form-group">
                                    <label>Email</label>
                                    <input name="data[email]" class="form-control" value="<?php echo $obj->email;?>"/>
                                </div>
								<div class="form-group">
                                    <label>Fax</label>
                                    <input name="data[fax]" class="form-control" value="<?php echo $obj->fax;?>"/>
                                </div>
								<div class="form-group">
                                    <label>Hotline top</label>
                                    <input name="data[hotline_1]" class="form-control" value="<?php echo $obj->hotline_1;?>"/>
                                </div>
								<div class="form-group">
                                    <label>Hotline Banner</label>
                                    <input name="data[hotline_2]" class="form-control" value="<?php echo $obj->hotline_2;?>"/>
                                </div>
								<div class="form-group">
                                    <label>Hotline footer 1</label>
                                    <input name="data[hotline_3]" class="form-control" value="<?php echo $obj->hotline_3;?>"/>
                                </div>
								<div class="form-group">
                                    <label>Hotline footer 2</label>
                                    <input name="data[hotline_4]" class="form-control" value="<?php echo $obj->hotline_4;?>"/>
                                </div>
                                <footer class="text-right bg-light lter">
                                    <input type="submit" class="btn btn-success btn-s-xs" value="Cập nhật"/>
                                </footer>
                            </div>
                        </section>
                    </form>
                </div>
                <?php ?>
            </div>
        </section>
    </section>
    <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
</section>
<script>
    $(document).ready(function ()
    {
        // activate Nestable for list 1
        if ($('#menus-top').length)
        {
            $('#menus-top').nestable({
                group: 0,
                maxDepth: 2
            }).on('change', updateOutputTop);
        }
        if ($('#menus-bottom').length)
        {
            $('#menus-bottom').nestable({
                group: 0,
                maxDepth: 1
            }).on('change', updateOutputBottom);
        }
    });
</script>
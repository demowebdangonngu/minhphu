<script>
    var module = '<?php echo $this->uri->segment(1); ?>';
    var base_url = '<?php echo base_url(); ?>';
</script>
<section id="content">


    <section class="vbox">
        <section class="scrollable padder">
            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">

                <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url() . $this->uri->segment(1); ?>"><?php
                        if ($module_title) {
                            echo $module_title;
                        } else {
                            echo $this->uri->segment(1);
                        };
                        ?></a></li>
                <!--<li class="active">Static table</li>-->
                <a  href="<?php echo base_url() . $this->uri->segment(1) . "/addedit"; ?>" class="add_new_item"><button >Thêm</button></a>
            </ul>
            <section class="panel panel-default">
                <!--<header class="panel-heading">
                  Responsive Table
                </header>-->

                <?php echo $form_search; ?>
                <!---search-->

                <div class="table-responsive">
                    <table class="table table-striped b-t b-light">
                        <thead>
                            <tr>
                                <th width="20"><input type="checkbox"></th>
                                <th>
                                    <a href="<?php echo $controller->sort_link($model->id_field) ?>">ID</a>
                                </th>
                                <th class="title"><a href="<?php echo $controller->sort_link('title') ?>">Tên</a></th>
                                <!--<th class="views"><a href="<?php echo $controller->sort_link('cat_id') ?>">Loại</a></th>-->
                                <th  class="post_by"><a href="<?php echo $controller->sort_link('admin_id') ?>">Đăng bởi</a></th> 
                                <?php if($controller->sort_link('sendmail')){?><th  class="post_by"><a href="<?php echo $controller->sort_link('sendmail') ?>">SendMail</a></th><?php }?>
                                <th  class="post_by"><a href="<?php echo $controller->sort_link('status') ?>">Trạng thái</a></th>  
                                <?php if ($model->fields[$model->status_field]) { ?>
                                    <th  class="status" width="100"><a href="<?php echo $controller->sort_link($model->status_field) ?>">Tình trạng</a></th>                               
                                <?php } ?>
                                <th class=""></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($content['pageList']))
                                foreach ($content['pageList'] as $row) {
                                    $status_field = $model->status_field;
                                    $id_field = $model->id_field;
                                    ?>
                                    <tr class="<?php if ((check_permision("posts_publish") && $row->status == 'approve' ) || (!check_permision("posts_publish") && $row->status == 'pending' )) echo "highlight" ?>">

                                        <td><input type="checkbox" class="item-cbox" value="<?php echo $row->$id_field; ?>" id="cbox-<?php echo $row->$id_field; ?>"/></td> 
                                        <td><?php echo $row->$id_field; ?></td> 
                                        <td>
                                            <a class='post-title' href="<?php echo base_url() . $this->uri->segment(1) . "/addedit/" . $row->$id_field; ?>" class="" title="Sửa">
                                                <?php echo $row->name; ?>
                                            </a>
                                        </td>
                                        <!--<td><?php echo $row->cat_name; ?></td>-->
                                                                    
                                      
                                        <td><?php echo $row->u_name; ?></td>  
                                        <?php if($controller->sort_link('sendmail')){?><td><?php echo $row->sendmail?'<b style="color: blue;">Send</b>':'Not'; ?></td><?php }?>
                                        <td><?php echo $row->status?'<b style="color: blue;">Public</b>':'Unpublic'; ?></td>
                                        <?php if ($model->fields[$model->status_field]) { ?>
                                            <td>
                                                <i class="<?php $row->status; ?>">
                                                    <?php
                                                    $time = strtotime($row->timeschedule) - time() + 60; //echo $time;
                                                    $strtime = "";
                                                    $hour = intval($time / (60 * 60));
                                                    $min = intval(($time - $hour * 60 * 60) / 60);
                                                    if ($hour >= 1 && $hour < 12)
                                                        $strtime.="$hour giờ ";
                                                    if ($hour < 12)
                                                        $strtime.="$min phút nữa";
                                                    if (!$strtime)
                                                        $strtime = date("H:i d/m/Y", strtotime($row->timeschedule));
                                                    if ($row->isschedule && $row->status == 'publish')
                                                        echo "Hẹn giờ <br><i style='color:#f69;'>" . $strtime . "</i>";
                                                    else
                                                        echo $model->lang->line($row->status);
                                                    ?>
                                                </i>
                                                <?php if ($row->status) { ?><i class="status_note"><?php echo $row->status; ?></i><?php } ?>
                                            </td>                              
                                        <?php } ?>
                                        <td>
                                            <?php echo $row->row_option; ?>
                                        </td>                
                                    </tr>
                                <?php } ?>
                        </tbody>
                    </table>
                </div>
                <footer class="panel-footer">
                    <div class="row">
                        <div class="col-sm-7 wrapper text-center-xs">
                            <select class="input-sm form-control input-s-sm inline v-middle" id="sl_action">
                                <option value="">Chọn thao tác</option>
                                <?php if (!$_GET['trash']) { ?>
                                    <option value="quick_status/trash">Chuyển vào thùng rác</option>
                                <?php } ?>
                                <?php if ($_GET['trash'] && $config['permanent_delete']) { ?>
                                    <option value="delete">Xóa vĩnh viễn</option>
                                <?php } ?>
                                <option value="quick_status/draft"><?php echo $model->lang->line('draft'); ?></option>
                                <option value="quick_status/pending"><?php echo $model->lang->line('pending'); ?></option>
                                <option value="quick_status/approve"><?php echo $model->lang->line('approve'); ?></option>
                                <option value="quick_status/publish"><?php echo $model->lang->line('publish'); ?></option>
                            </select>
                            <button class="btn btn-sm btn-default"  onclick="do_sl_action()">Thực hiện</button>  
                            <script>
                                function do_sl_action() {
                                    if ($("#sl_action").val().length > 0) {
                                        var ids = "";
                                        $("input.item-cbox").each(function () {
                                            if (this.checked)
                                                ids.length > 0 ? ids += "," + this.value : ids += this.value;

                                        });
                                        if (ids.length == 0) {
                                            alert("Bạn chưa chọn tin nào!");
                                        } else {
                                            window.location = base_url + module + "/" + $("#sl_action").val() + "?ids=" + ids;
                                        }
                                    }
                                }
                            </script>
                        </div>
                        <!--
                      <div class="col-sm-4 text-center">
                        <small class="text-muted inline m-t-sm m-b-sm">showing 20-30 of 50 items</small>
                      </div>
                        -->
                        <!--<div class="col-sm-4 text-right text-center-xs">                
                          <ul class="pagination pagination-sm m-t-none m-b-none">
                            <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                          </ul>
                        </div>-->
                        <?php echo $content['paging']; ?>
                    </div>
                </footer>
            </section>
        </section>
    </section>
    <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
</section>
<style>
    tr.highlight a.post-title{
        color:#f00;
    }
</style>






























<?php 
$product = $this->load->model('product_model');
$color = $this->load->model('color/color_model');
$colors = $color->listAll();
if($obj){
	$value = $obj->id;
	$data['value'] = $value;
	// $this->data['id_product'] = $_POST['id_product'];
	// $this->data['id_color'] = $_POST['id_color'];
}

?>
<script>
var sttsize = 0;
var sttimage = 0;
function AddSize<?php echo $value;?>(type) {
	sttsize++;
	$.ajax({
		type: 'POST',
		url: "<?php echo base_url();?>product/loadaddsize/",
		success: function(html) {
			$("#addsize<?php echo $value;?>").append(html);
			var classsize = 'size_<?php echo $value;?>_'+sttsize;
			var namesize = 'product[<?php echo $value;?>][group]['+sttsize+'][size]';
			var classsoluong = 'color_<?php echo $value;?>_'+sttsize;
			var namesoluong = 'product[<?php echo $value;?>][group]['+sttsize+'][soluong]';
			$('.size').attr('class', classsize);
			$('.'+classsize).attr('name', namesize);
			$('.soluong').attr('class', classsoluong);
			$('.'+classsoluong).attr('name', namesoluong);
		}
	});
}
function AddImage<?php echo $value;?>(type) {
	sttimage++;
	$.ajax({
		type: 'POST',
		url: "<?php echo base_url();?>product/loadaddimage/",
		data: "type="+type+"&id_product=<?php echo $obj->id;?>&stt="+sttimage,
		success: function(html) {
			$("#addimage<?php echo $value;?>").append(html);
			var classsize = 'image-color-<?php echo $value;?>-'+sttimage;
			var namesize = 'product[<?php echo $value;?>][image][]';
			$('.image-color').attr('class', classsize);
			$('.'+classsize).attr('name', namesize);
		}
	});
}
</script>
<div class="myBox" style="position: relative;">
   <div class="boxHeader">
      <div class="col-left">
         <select name="<?php echo $obj?"product[".$value."][id_color]":"categories_parent";?>" id="<?php echo $obj?"categories_parent_".$obj->id:"categories_parent";?>" class="color form-control">
            <?php foreach($colors as $color){?>
            <option <?php echo $obj->id_color == $color->id?'selected':'';?> value="<?php echo $color->id;?>"> <?php echo $color->name;?></option>
            <?php }?>
         </select>
      </div>
      <div class="col-right">
         <!--<input style="width: 90%;margin-top: 10px;margin-left: 3px;text-align: center;" class="number-1" name="data_post[group][sort_group]" value="0">-->
      </div>
   </div>
   <div class="dd boxBody" id="menus-top" data-output="">
      <div class="boxButton">
         <div class="boxButton">
            <input type="button" class="btn btn-primary" onclick="AddSize<?php echo $value;?>('add')" value="Thêm size"/>
            <input type="button" class="btn btn-primary" onclick="AddImage<?php echo $value;?>('add')" value="Thêm hình"/>
         </div>
         <div class="dd-list-1">
            <div class="dd-item content" id="group">
               <table id="compare-produc-table" class="table" style="width:40%;float:left" border="0">
                  <tbody class="col-left" id="addsize<?php echo $value;?>">
                     <?php 
					 if($obj){
						 $where = array('id_pc'=>$value);
						 $sizes = $product->GetSizeColor($where);
						 if($sizes){
							 foreach($sizes as $size){
								 $data['size'] = $size;
								 $this->load->view('html_size', $data);
							}
						 }
					 }
					 ?>
                  </tbody>
               </table>
               <div class="col-right" id="addimage<?php echo $value;?>" style="width: 49%;">
					<?php 
					if($obj){
					 $where = array('id_pc'=>$value);
					 $images = $product->GetImageColor($where);
					 if($images){
						 foreach($images as $image){
							 $data['image'] = $image;
							 $this->load->view('html_image', $data);
						}
					 }
					}
					 ?>
				
               </div>
               
            </div>
         </div>
      </div>
   </div>
   <input style="position: absolute;top: 0px;right: 28px;width: 25px;height: 25px;" type="button" onclick="$(this).parent().remove();" class="close" id="close" value="X">
</div>
<script>
var sttcolor = <?php echo $value;?>;
<?php 
if($size){
?>
var sttsize = <?php echo $size->id;?>;
<?php 
}
if($image){
?>
var sttimage = <?php echo $image->id;?>;
<?php }?>
</script>
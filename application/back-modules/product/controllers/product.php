<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/ControllerAdmin.php";

class product extends YT_ControllerAdmin {

    /**
     *
     * @var Test_model 
     */
    public $model;

    function __construct() {
        // pr($this->uri);die;
        parent::__construct();
        $this->model = $this->load->model('product_model');
        
        $this->check_access('product_management_access');
    }

    public function search($array_fillter) {

        if (is_numeric($array_fillter['key'])) {
            $this->model->where = array($this->model->table_name . "." . $this->model->id_field => $array_fillter['key']);
        } else {


            if ($array_fillter['from']) {
                $date = explode("/", $array_fillter['from']);
                $fromdate = intval($date[2]) . "-" . intval($date[1]) . "-" . intval($date[0]) . " 00:00:00";
                $this->model->where['product.create_date >='] = $fromdate;
            }
            if ($array_fillter['to']) {
                $date = explode("/", $array_fillter['to']);
                $fromto = intval($date[2]) . "-" . intval($date[1]) . "-" . intval($date[0]) . " 23:59:59";
                $this->model->where['product.create_date <='] = $fromto;
            }
            if ($array_fillter['key']) {
                $this->model->like = array('product.name', '' . $array_fillter['key'] . '');
            }
            if ($array_fillter['status']) {
                if ($array_fillter['status'] == 'schedule') {
                    $this->model->where['status'] = 'publish';
                    $this->model->where['isschedule'] = 1;
                } else
                    $this->model->where['status'] = $array_fillter['status'];
            }
            if ($array_fillter['position'] == 'hot' || $array_fillter['position'] == 'feature') {

                $this->model->where[$array_fillter['position']] = 1;
            }
            if ($array_fillter['hot_position']) {

                $this->model->where['hot_position'] = intval($array_fillter['hot_position']);
                $this->model->where['hot'] = 1;
            }
            if ($array_fillter['admin']) {

                $this->model->where['admin_id'] = intval($array_fillter['admin']);
                //$this->model->where['hot']=1;
            }
        }
        //return $this->index();
    }
	function delete_image(){
		if($_POST['image'] == 1){
			$data = array(
				'image' => '',
			);
			$this->model->getDB()->where('id', $_POST['id']);
			$this->model->getDB()->update('product', $data); 
		}
	}
    function link_search($callback = "link_callback") {
        if ($this->input->get('key')) {
            $this->search($_GET);
        }
        $input = $this->input->get();
        if ($this->model->fields[$this->model->status_field]) {
            if (!$input['trash']) {
                $this->model->where[$this->model->status_field . " !="] = 2;
            } else {
                $this->model->where[$this->model->status_field . " ="] = 2;
            }
        }
        $this->model->single_table_mode = false;
        $this->data['list_item'] = $this->model->get_list_language(100);
        $this->data['callback'] = $callback;
        $this->data['model'] = $this->model;
        $this->data['controller'] = $this;
        $this->template->build('link_search', $this->data);
    }

    public function index() {
        if ($this->input->get('search')) {
            $this->search($this->input->get('search'));
        }
        $input = $this->input->get();
        if ($input['order']) {
            $pattern = explode(" ", $input['order']);
            if ($this->model->fields[$pattern[0]]) {
                $this->model->order = $this->model->table_name . "." . $pattern[0] . " " . (in_array(strtolower($pattern[1]), array('desc', 'asc')) ? $pattern[1] : '');
            } else
            if ($this->model->fields_lang[$pattern[0]]) {
                $this->model->order = $this->model->table_name_lang . "." . $pattern[0] . " " . (in_array(strtolower($pattern[1]), array('desc', 'asc')) ? $pattern[1] : '');
            }
        } else {
            $this->model->order = $this->model->table_name . "." . $this->model->id_field . " desc";
        }
        if ($this->model->fields[$this->model->status_field]) {
            if (!$input['trash']) {
                $this->model->where[$this->model->status_field . " !="] = 0;
            } else {
                $this->model->where[$this->model->status_field . " ="] = 0;
            }
            $this->model->where["(" . $this->model->status_field . " !=0 or admin_id=" . $this->session->userdata('admin_id') . ")"] = null;
        }
        $this->model->single_table_mode = false;
        $this->model->select = "product.*,categories.name as cat_name, users.username as u_name";
        $this->model->join[] = array('users', "product.admin_id=users.id");
        $this->model->join[] = array('categories', "categories.id=product.cat_id");
        $this->data['content'] = $this->model->get_page_list("{$this->uri->segment(1)}/index", 30, 3);
        foreach ($this->data['content']['pageList'] as $value) {
            $roption['row'] = $value;
            $roption['controller'] = $this;
            $roption['model'] = $this->model;
            $value->row_option = $this->load->view('row_option', $roption, true);
        }
        $this->data['member_send_mail'] = $this->member_send_mail();
        $this->data['controller'] = $this;
        $this->data['model'] = $this->model;
        $this->data['form_search'] = $this->load->view('form_search', $this->data, true);
//                $this->data['option']['quick_trash']=1;
//                $this->data['option']['quick_delete']=1;
        $this->template->build('list', $this->data);
    }

    function quick_status($action) {
        $return = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $this->uri->segment(1);
        $ids = $this->input->get('ids');
        $ids = explode(",", $ids);
        foreach ($ids as $index => $id) {
            if (!is_numeric($id)) {
                unset($ids[$index]);
            }
        }
        if (count($ids)) {
            $this->model->where_in[] = array($this->model->id_field, $ids);
            $this->model->update(array($this->model->status_field => $action));
            $this->model->on_data_change();
            logs($action . " product id: " . implode(",", $ids));
        }
        redirect($return);
    }

    function addedit($id = 0) {
        $id = $this->uri->segment(3);
        $this->data['cat'] = $this->load->model('categories/Categories_model');
        $this->data['users'] = $this->load->model('user/user_model');
        // if (isset($_FILES['img']['name']) && $_FILES['img']['name'] != "") {
            // move_uploaded_file($_FILES['img']['tmp_name'], '../upload/images/images/' . time() . "_" . $_FILES['img']['name']);
            // $_POST['data']['image'] = time() . "_" . $_FILES['img']['name'];
        // }
		
        $tags=$this->load->model('tags/tags_model');
        $this->data['tags']=$tags->get_tag_by_object($id,'product');
        $toptags=$tags->get_top_tag('product');
        $s=array();
        foreach ($toptags as $t) {
            $s[]="'".  str_replace("'","\\'",$t->name)."'";
        }
        $this->data['toptags']=  implode(",", $s);
		// print_r($this->data['toptags']);exit;
		$this->model->getDB()->from('product');
		$this->model->getDB()->where('id',$id);
        $item=current($this->model->getDB()->get()->result());
		$str=explode(",", preg_replace("/\s+/"," ",$item->cat_extra));
		foreach($str as $v){
			if($v){
				$this->model->getDB()->from('categories');
				$this->model->getDB()->where('id',$v);
				$cat = current($this->model->getDB()->get()->result());
				$s_cat[]=$cat->name;
			}
		}
		$this->data['tags_categories'] = implode(",", $s_cat);
		// print_r($this->data['tags_categories']);exit;
        $data_post = $this->input->post();
		
        if($data_post){
			
			if(!$_POST['data']['highlights']){
				$_POST['data']['highlights'] = 0;
			}
			// print_r($_POST['product']);exit;
			$_POST['data']['admin_id'] = $_SESSION['admin_id'];
            $tags->insert_tags_for_object($_POST['tags'], $id, 'product');
            $id = $this->model->store_data($this->input->post("data"));
            if ($id) {
                $this->on_after_addedit_process($id);
            }
            //$input_value = $this->input->post("data_value");
            $value_sp = $this->load->model("value_sp/value_sp_model");
            $id_value = $value_sp->value_sp($this->input->post("data_value"));
            redirect("product", 'refresh');
        }
        parent::addedit($id);
    }

    function on_after_addedit_process($pid) {
        $date = date('Y-m-d',time());
		
		if($_POST['categories']){
			
			$str_categories = ',';
			$str=explode(",", preg_replace("/\s+/"," ",$_POST['categories']));
			foreach($str as $v){
				$this->model->getDB()->from('categories');
				$this->model->getDB()->where('name',$v);
				$item_cat = current($this->model->getDB()->get()->result());
				$str_categories .= $item_cat->id.',';
			}
			$this->model->getDB()->where(array('id'=>$pid));
			$this->model->getDB()->update('product', array('cat_extra'=>'')); 
			$array_categories = array(
				'cat_extra'=>$str_categories,
			);
			$this->model->getDB()->where(array('id'=>$pid));
			$this->model->getDB()->update('product',$array_categories);
		}
		//dữ liệu color, size, sl, image liên quan
		if($_POST['product']){
			$listpc = $this->model->GetColor(array('id_product'=>$pid));
			foreach($listpc as $item){
				$this->model->getDB()->where(array('id_pc'=>$item->id));
				$this->model->getDB()->delete('product_size'); 
				
				$this->model->getDB()->where(array('id_pc'=>$item->id));
				$this->model->getDB()->delete('product_image'); 
			}
			$this->model->getDB()->where(array('id_product'=>$pid));
			$this->model->getDB()->delete('product_color'); 
			
			foreach($_POST['product'] as $product_color){
				$array_insert_color = array(
					'id_product' => $pid,
					'id_color' => $product_color['id_color']
				);
				$this->model->getDB()->insert('product_color',$array_insert_color);
				$idpc = $this->model->getDB()->insert_id();
				if($idpc){
					foreach($product_color['group'] as $sizesls){
						$array_insert_sizesl = array(
							'size' => $sizesls['size'],
							'soluong' => $sizesls['soluong'],
							'id_pc' => $idpc
						);
						$this->model->getDB()->insert('product_size',$array_insert_sizesl);
					}
					foreach($product_color['image'] as $images){
						$array_insert_image = array(
							'image' => $images,
							'id_pc' => $idpc
						);
						$this->model->getDB()->insert('product_image',$array_insert_image);
					}
				}
			}
		}
		
        $result = array();
        $admin_id = $this->session->userdata('admin_id');
        $data = array(
            'admin_id' => $admin_id,
            'public_date' => $date,
        );
        $this->model->where('id',$pid);
        $this->model->update($data);
        return array_merge($result, (array) parent::on_after_addedit_process($pid));
    }

    public function quick_trash() {
        $return = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : $this->uri->segment(1);
        $ids = $this->input->get('ids');
        $this->model->delete_id($ids);
        $this->model->on_data_change();
        if ($ids)
            logs("Xóa: " . implode(",", $ids));
        redirect($return);
    }

    function refuse_post($id) {
        if (!check_permision("product_refuse"))
            return die();
        $this->model->where(array('id' => $id));
        $obj = current($this->model->get()->result());
        if ($obj->id && $obj->status == 'pending') {
            $this->model->getDB()->update($this->model->table_name, array('status' => 'draft', 'is_refuse' => 1, 'refuse_by' => $this->session->userdata('admin_id')), array('id' => $id));
            logs("Trả bài: " . $obj->title);
        }
        redirect(base_url() . $this->uri->segment(1));
    }

    function topAuthor($type) {
        // echo 123123;die;
        $this->data['top'] = $this->model->getTopAuthor($type);
        $this->data['type'] = $type;
        if ($type == 'count')
            $this->data['type'] = 'bài';
        $this->load->view('topAuthor', $this->data);
    }
    
    
    
    public function sendmail($vonght = false , $test = true){
        if(!$test){
            header( "refresh:2;url=".base_url() . $this->uri->segment(1) );
            echo 'Đang cap nhat !! <br/>';
            echo 'Return after 2s.'; exit;
        }

        $where['sendmail'] = 1;
        // get SP Check SEND Mail  
        $this->model->where(array('sendmail' => 1));
        $this->model->limit(6);
        $data['list_sp'] = $this->model->get()->result(); 

        $this->model->select('email');
        $this->model->where(array('status' => 1,'set_email'=>1)); 
        $this->model->from('member');
        $this->model->limit(20); 
        $this->model->order_by('sendmail_date' , 'ASC');
        $data['list_member'] = $this->model->get()->result(); 

        $this->model->select('sendmail_date , sendmail_vong');
        $this->model->where(array('status' => 1,'set_email'=>1)); 
        $this->model->from('member');
        $this->model->order_by('sendmail_date' , 'DESC');
        $data['member'] = $this->model->get()->result(); 
 
        $message = $this->load->view('mail_html2' , $data , true);
        //echo $message;
        $subject = 'Sản phẩm mới tại CAMERANO1.COM';
        foreach($data['list_member'] as $k=>$v){ 
            if( $this->validateEmail($v->email) ){
                $arrMember[] = $v->email;
            }
        } 
        //$this->send_mail( implode(',' , $arrMember) , $message , $subject);
        $mailsend = $this->send_mail(  implode(',' , $arrMember) , $message , $subject);
        $mailsend = true;
        if($mailsend){
        	foreach($data['list_member'] as $k=>$v){ 
                if( $this->validateEmail($v->email) ){
                    $where = array();
                    $dataUpdate = array();
                    $where['email'] = $v->email;
                    $dataUpdate['sendmail_date'] = date('Y-m-d H:i:s');
                    $dataUpdate['sendmail_vong'] = $vonght; 
                    $this->model->update_member($where , $dataUpdate); 
                    
                }
            } 
            header( "refresh:2;url=".base_url() . $this->uri->segment(1) );
            echo 'Send mail success !! <br/>';
            echo 'Return after 2s.';
        }else{
            header( "refresh:6;url=".base_url() . $this->uri->segment(1) );
            echo 'Send mail error !! <br/>';
            echo 'Return after 6s.';
        } 
    }




    private function validateEmail($email) {
        $pattern = "/^[a-zA-Z0-9-_\.]+@{1}[a-zA-Z0-9-\.]+\.{1}[a-zA-Z]{2,4}$/";
        if (preg_match($pattern, $email))
            return true;
        else
            return false;
    }
     
    public function send_mail_php($emailTo, $message, $subject){
    	$to = $emailTo;  
    	$this->config->load('host_email', TRUE);
        $config = $this->config->config['host_email']; 
        //echo '<pre>'; print_r($config); exit;   
		$headers = "From: " .$config['name_email'] .'<'. $config['smtp_user'] . ">\r\n";  
		$headers .= "Reply-To: ".$config['smtp_user'] .  "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n"; 
  		$headers .= "X-Mailer: PHP". phpversion() ."\r\n" ;

		mail($to, $subject, $message, $headers);
		return true;
    }
    public function member_send_mail(){ 
        $this->model->select('email');
        $this->model->where(array('status' => 1,'set_email'=>1)); 
        $this->model->from('member');  
        $tong_member = count($this->model->get()->result());  
        $this->model->select('sendmail_date , sendmail_vong');
        $this->model->where(array('status' => 1,'set_email'=>1)); 
        $this->model->from('member');
        $this->model->order_by('sendmail_date' , 'DESC');
        $this->model->limit(1); 
        $member = $this->model->get()->result();
        
        $this->model->select('email');
        $this->model->where(array('status' => 1,'set_email'=> 1, 'sendmail_vong < '=>$member[0]->sendmail_vong)); 
        $this->model->from('member'); 
        $this->model->order_by('sendmail_date' , 'ASC');
        $so_member_chua_send_mail_cua_vong = count($this->model->get()->result()); 
      
        $result['sendmail_vong'] = $member[0]->sendmail_vong;
        $result['sendmail_date'] = $member[0]->sendmail_date;
        $result['total_member'] = $tong_member;
        $result['sended_member'] = $so_member_chua_send_mail_cua_vong; 
        if($result['sended_member'] == 0){
            $result['sended_member'] =  0;
            $result['sendmail_vong'] = $result['sendmail_vong'] + 1;
        }  
        return $result; 
    }




    public function sendmail_demo(){
        $where['sendmail'] = 1;
        // get SP Check SEND Mail  
        $this->model->where(array('sendmail' => 1));
        $this->model->limit(6);
        $data['list_sp'] = $this->model->get()->result(); 
        $this->model->select('email');
        $this->model->where(array('status' => 1)); 
        $this->model->from('member');
        $data['list_member'] = $this->model->get()->result(); 
        $message = $this->load->view('mail_html2' , $data , true);
        //echo $message;
        $subject = 'Sản phẩm mới';
        foreach($data['list_member'] as $k=>$v){ 
            if( $this->validateEmail($v->email) ){
                $arrMember[] = $v->email;
            }
        } 
        //$this->send_mail( implode(',' , $arrMember) , $message , $subject);
        $mailsend = $this->send_mail(  'minhtiencreater@gmail.com' , $message , $subject);
        if($mailsend){
            //header( "refresh:2;url=".base_url() . $this->uri->segment(1) );
            echo 'Send mail success !! <br/>';
            echo 'Return after 2s.';
        }else{
            //header( "refresh:6;url=".base_url() . $this->uri->segment(1) );
            echo 'Send mail error !! <br/>';
            echo 'Return after 6s.';
        } 
    }
    
    private function send_mail($emailTo, $message, $subject) {
        $this->config->load('host_email', TRUE);
        $config = $this->config->config['host_email'];  
		
        $this->load->library('email', $config);
        $this->email->clear(); 
        $this->email->set_newline("\r\n");

        $this->email->from($config['smtp_user'] , $config['name_email'] );
        $this->email->to($emailTo);
        $this->email->reply_to($config['smtp_user'] , $config['name_email'] );
        $this->email->subject($subject);
        $this->email->message($message);  
         // echo '<pre>'; print_r($this->email); exit;
        if ($this->email->send()) {
            //exit;
            $result = true;
        } else { 
            $result = FALSE;
            show_error($this->email->print_debugger());
        }
        return $result;
    }//

    private function send_mail_demo($emailTo = false, $message = false, $subject = false) {
        
         
      require_once(APPPATH.'libraries/email.class.php');
      $email = new Emailer ();
      $email->setTo("minhtiencreater@gmail.com");
      //$this->email->addBCC($bw->input['tour']['Email']); 
      $email->setFrom ('ly-son-travel@lysontravel.org');
      $email->setSubject ($subject );
       
      $email->setBody ($message);
      $email->sendMail (); 
      echo 123213;die; 
    }

    function listsp() {
        $this->load->view('listsp');
    }
	
	function addcolor(){
		if($_POST){
			$stt = $_POST['stt']+1;
			$this->data['value'] = $stt;
		}
		$this->load->view('listcolor', $this->data);
	}
	
	function LoadAddSize(){
		if($_POST['type'] == 'load'){
			$where = array('id_pc'=>$_POST['id_pc'],'id'=>$_POST['id']);
			$this->data['size'] = current($this->model->GetSizeColor($where));
		}
		$this->load->view('html_size', $this->data);
	}
	
	function LoadAddImage(){
		if($_POST['type'] == 'add'){
			$stt = $_POST['stt']+1;
			$this->data['idbanner'] = 'fieldIDbanner'.$stt;
		}
		if($_POST['type'] == 'load'){
			$stt = $_POST['stt']+1;
			$this->data['idbanner'] = 'fieldIDbanner'.$stt;
			$where = array('id_pc'=>$_POST['id_pc'],'id'=>$_POST['id']);
			$this->data['size'] = current($this->model->GetImageColor($where));
		}
		$this->load->view('html_image', $this->data);
	}
	
	function loadsql(){
		$this->model->getDB()->from('product');
		$a = $this->model->getDB()->get()->result();
		foreach($a as $item){
			$array[] = $item->id;
			
		}
		
		print_r($array);exit;
		echo 'xong';
	}
}

?>
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/AModel.php";

class product_model extends YT_AModel {

    function __construct() {
        parent::__construct();



        $this->fields = array(
            'id' => 'pri',
            'cat_id' => 1,
            'admin_id' => 1,
            'pro_id' => 1,
            'name' => 1,
            'content' => 1,
            'price' => 1,
            'price_sale' => 1,
            'image' => 1,
            'description' => 1,
            'sendmail' => 1,
            'public_date' => 1,
            'status' => 1,
            'specification' => 1,
			'highlights' => 1,
			'code_product' => 1,
			'cat_extra' => 1,
        );
        $this->table_name = 'product';
        $this->table_name_lang = '';
        $this->id_field = 'id';
        $this->id_field_lang = '';
        $this->status_field = 'product.status';
        $this->cat_field = '';
        $this->order_field = '';
        $this->image_field = array('image' => '');
        $this->id_lang_rel = $this->id_field;
    }

    function on_data_change() {
        /* @var $caches MY_Caches */
        $caches = $this->load->library('Caches');
        $caches->on_post_update();
        //parent::on_data_change();
    }
    function get_row($where) {
        $this->getDB()->select('*');
        $this->getDB()->from($this->table_name);
        $this->getDB()->where($where);
        return $this->getDB()->get()->result();
    }
    function delete_id($id){
        $this->getDB()->where('id',$id);
        $this->getDB()->delete($this->table_name);
    }
    function update_member($where , $data){
        $this->getDB()->where($where);
        $this->getDB()->update('member' , $data);
    }
	
	function GetColor($where){
		$this->getDB()->from('product_color');
		if($where)
			$this->getDB()->where($where);
		// $this->getDB()->order_by('id','ASC');
		return $this->getDB()->get()->result();
	}
	
	function GetImageColor($where){
		$this->getDB()->from('product_image');
		if($where)
			$this->getDB()->where($where);
		// $this->getDB()->order_by('id','ASC');
		return $this->getDB()->get()->result();
	}
	
	function GetSizeColor($where){
		$this->getDB()->from('product_size');
		if($where)
			$this->getDB()->where($where);
		// $this->getDB()->order_by('id','ASC');
		return $this->getDB()->get()->result();
	}

}

?>
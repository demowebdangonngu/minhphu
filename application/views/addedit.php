<script>
   var module = '<?php echo $this->uri->segment(1);  ?>';
   var base_url ='<?php echo base_url();  ?>';
   var img_path = '<?php echo $this->config->item('img_path')  ?>';
   <?php 
   $returnurl=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:base_url().$this->uri->segment(1); 
   $id_field=$model->id_field;
   $order_field=$model->order_field;
   $status_field=$model->status_field;
   ?>
</script>
<section id="content">
          <section class="vbox">
            <section class="scrollable padder">
              <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                <li><a href="<?php echo base_url();  ?>"><i class="fa fa-home"></i> Home</a></li>
                <li><a href="<?php echo base_url().$this->uri->segment(1);  ?>"><?php if($module_title){echo $module_title;}else{echo $this->uri->segment(1);};?></a> </li>
                <li class="active">Thêm / Sửa bài viết</li>
              </ul>
              <div class="row">
                <div class="col-sm-12">
                    <form class="form-horizontal" id="frm_add_edit" method="post" action="<?php echo base_url().$this->uri->segment(1); ?>/addedit_process/" onsubmit="submit_form($(this),'<?php echo $return_url; ?>');return false;"  >
                        <input  name="data[<?php echo $id_field; ?>]" type="hidden" id="txt_primary_id" value="<?php echo $obj->$id_field;?>"/>
                  <section class="panel panel-default">
                    <header class="control-fixed panel-heading font-bold" data-top="49" >
                        <label style="line-height: 33px;">Thêm / Sửa nội dung</label>
                        <label class="pull-right">
                              <button class="btn btn-primary" type="button" onclick="update_form($('#frm_add_edit'));" class="bntAll">Lưu lại</button>
                              <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');return false;">Đóng</button>
                            </label>
                    </header>
                    <div class="panel-body">
                        <div id="post_form" class="col-sm-8 left">
                            <style>
                                
                                .panel-heading.control-fixed {
                                    height: 43px;
                                    padding: 4px 17px;
                                  }
                            </style>
                            <script>
                                $(".scrollable").scroll(function(){
                                var elem = $('.control-fixed');//alert("sdfsdf");
                                if (!elem.attr('data-top')) {
                                    if (elem.hasClass('navbar-fixed-top'))
                                        return;
                                     var offset = elem.offset()
                                    elem.attr('data-top', offset.top);
                                }
                                if (elem.attr('data-top') <= $(this).scrollTop() )
                                    elem.addClass('navbar-fixed-top');
                                else
                                    elem.removeClass('navbar-fixed-top');
                                });
                            
                            </script>
                        <div class="form-group has-success">
                            <label class="col-sm-2 control-label">Tên</label>
                            <div class="col-sm-10">
                              <input type="text" name="data[name]" class="form-control" value="<?php echo htmlspecialchars($obj->name);?>">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-4">
                             <div class="form-group">
                                <label class="col-sm-6 control-label">Trạng thái</label>
                                <div class="col-sm-6">
                                  <label class="switch">
                                    <input name="data[status]"  value="1" <?php if($obj->$status_field==1) echo 'checked=checked';?>  type="checkbox">
                                    <span></span>
                                  </label>
                                </div>
                              </div>

                        </div>
                        
                        <div id="" class="col-sm-12">
                            <div class="line line-dashed line-lg pull-in"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                               <input onclick="update_form($('#frm_add_edit'));" class="btn btn-primary" type="button" value="Lưu lại"/>
                              <button class="btn btn-default" type="button" onclick="exit_form('<?php echo $return_url; ?>');return false;">Đóng</button>
                            </div>
                          </div>
                      </div>
                        </div>
                      
                    </div>
                  </section>
                        </form>
                </div>
                
              </div>
              
            </section>
          </section>
          <a data-target="#nav" data-toggle="class:nav-off-screen" class="hide nav-off-screen-block" href="#"></a>
        </section>
<script type='text/javascript'>
    function on_form_submit(form){
        //code on submit here
        //return false;
    };
	//CKEDITOR.replace('content_here');
        
        /**global javascript**/
        $(function() {
    
            $( ".input-sortable" ).sortable({
              placeholder: "ui-state-highlight"
            });

          });

</script>		
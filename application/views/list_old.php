

<!-- main right -->
   <div class="mainRight">
       <div class="boxAll blogSearch">
              <div class="fl">
                  <h2><a href="<?php echo base_url();  ?>">Home</a> > <a href="<?php echo base_url().$this->uri->segment(1);  ?>"><?php if($module_title){echo $module_title;}else{echo $this->uri->segment(1);};?></a> <a  href="<?php echo base_url().$this->uri->segment(1)."/addedit";  ?>" class="add_new_item"><button >Thêm</button></a></h2>
                  
              </div> 
                <?php echo $form_search;?>
              <div class="clr"></div>
                
        </div>
        <div class="boxAll blogTable">
        
            
            
            <table cellpadding="0" cellspacing="0" border="0" class="tableS2">
                    <tr class="first">
                        <th  class="cbox"><input type="checkbox" class="cbox-all item-cbox" value="" onclick="$('.item-cbox').attr('checked',this.checked);"/>
                        <th  class="id"><a href="<?php echo $controller->sort_link($model->id_field)?>">ID</a></th>    
                         <?php if($model->fields['name']||$model->fields_lang['name']){?>
                         <th class="title"><a href="<?php echo $controller->sort_link('name')?>">Tên</a></th>                               
                         <?php }?>
                         <?php if($model->fields['create_date']){?>
                         <th  class="date"><a href="<?php echo $controller->sort_link('create_date')?>">Ngày đăng</a></th>                               
                         <?php }?>
                          <?php if($model->fields[$model->status_field]){?>
                         <th  class="status"><a href="<?php echo $controller->sort_link($model->status_field)?>">Tình trạng</a></th>                               
                          <?php } ?>
                         <th></th>  
                     </tr> 
                     
                     <?php 
                     
                     if(!empty($content['pageList']))
					 foreach ($content['pageList'] as $row) {  
					 	/*$this->load->model('categoy/category_model','CATE',TRUE);	 
 						$detailCate 		= $this->CATE->detail($row->cat_id);
						
						$this->load->model('providers/providers_model','PRO',TRUE);	 
 						$deatilPro 		= $this->PRO->detail($row->pro_id);*/
                         $status_field=$model->status_field;
                          $id_field=$model->id_field;    
					 ?>
                     <tr>
                         
                         <td><input type="checkbox" class="item-cbox" value="<?php echo $row->$id_field; ?>" id="cbox-<?php echo $row->$id_field; ?>"/></td> 
                         <td><?php echo $row->$id_field; ?></td> 
                            <?php if($model->fields['name']||$model->fields_lang['name']){?>
                         <td>
                             <a href="<?php echo base_url().$this->uri->segment(1)."/addedit/".$row->$id_field;?>" class="" title="Sửa">
                                 <?php if( $model->fields['name'])echo $row->name;else foreach ($row->_mlang as $v) {echo "<div>".$v->name."</div>";}  ?>
                             </a>
                         </td>
                            <?php }?>
                            <?php if($model->fields['create_date']){?>
                         <td><?php echo date('H:i d/m/Y',strtotime($row->create_date)); ?></td>                             
                            <?php }?>
                            <?php if($model->fields[$model->status_field]){?>
                         <td>
                             <?php if($row->$status_field==1){ ?>
                             <a title="Click đổi trạng thái" class="ico_activeOn tooltip" item-id="<?php echo $row->$id_field;?>" value="<?php echo $row->$status_field;?>" onClick="update_status(this)" ></a>
                             <?php }else{ ?>
                             <a title="<?php if($row->$status_field!=2){ ?>Click đổi trạng thái<?php } ?>" class="ico_activeOff tooltip" item-id="<?php echo $row->$id_field;?>" value="<?php echo $row->$status_field;?>"  <?php if($row->$status_field!=2){ ?>onClick="update_status(this)"<?php } ?> ></a>
                             <?php } ?>
                         </td>                              
                            <?php } ?>
                         <td>
                             <?php echo $row->row_option;?>
                         </td>                
                     </tr>
                     
                     <?php } ?>
                     
           </table>          
        </div>
        <!-- table -->
        <div class="boxAll pageNum" style="">
            <div>
                <label>
                    <select class="sl_action" id="sl_action">
                        <option value="">Chọn thao thác</option>
                        <?php if(!$_GET['trash']){?>
                        <option value="quick_trash">Chuyển vào thùng rác</option>
                        <?php } ?>
                        <?php if($_GET['trash']){?>
                        <option value="delete">Xóa vĩnh viễn</option>
                        <?php } ?>
                        <option value="hidden">Ẩn</option>
                        <option value="show">Hiện</option>
                    </select>
                    <button onclick="do_sl_action()">Thực hiện</button>
                    <script>
                        function do_sl_action() {
                            if($("#sl_action").val().length>0){
                                var ids = "";
                                $("input.item-cbox").each(function(){
                                    if(this.checked) ids.length>0?ids+=","+this.value:ids+=this.value;
                                    
                                });
                                if(ids.length==0){
                                    alert("Bạn chưa chọn tin nào!");
                                }else{
                                    window.location=base_url+module+"/"+$("#sl_action").val()+"?ids="+ids;
                                }
                            }
                        }
                    </script>
                        
                </label>
            </div>
        </div>
        <!-- page num -->
        <div class="boxAll pageNum" style="">
                <!--<div class="page">Trang   <a href="#" class="active">01</a>   <a href="#">02</a>    <a href="#">03</a>    <a href="#">04</a>    <a href="#">05</a> <a href="#" class="preP"></a>  <a href="#" class="nextP"></a></div>-->
                <div class="page">
                <?php echo $content['paging'];?>
                </div>
                <!--
              <div class="jumbP">
                Tới trang <input type="text" value="" class="shortx" /> Số dòng <input type="text" value="" class="shortx" /> <a href="#" class="bntAll">Ok</a>
              </div>
                -->
        </div>
        
        
   </div> 
<!-- en main right -->
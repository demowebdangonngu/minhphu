<script>
   var module = '<?php echo $this->uri->segment(1);  ?>';
   var base_url ='<?php echo base_url();  ?>';
   <?php 
   $returnurl=$_SERVER['HTTP_REFERER']?$_SERVER['HTTP_REFERER']:base_url().$this->uri->segment(1); 
   $id_field=$model->id_field;
   ?>
</script>
<div class="mainRight">
       		<form id="frm_add_edit" method="post" action="<?php echo base_url().$this->uri->segment(1); ?>/addedit_process/" onsubmit="submit_form($(this),'<?php echo $return_url; ?>');return false;"  >
            <!-- search -->
            <div class="boxAll blogSearch">
                
                  <div class="fl"><h2>Thêm bài viết</h2></div> 
                  
                  <div style="" class="search fr">
                  <input type="button" onclick="update_form($('#frm_add_edit'));" class="bntAll" value="Lưu lại">
                  <input type="button" onclick="exit_form('<?php echo $return_url; ?>')" class="bntAll btnExit" value="Thoát">
                  </div>
                  <div class="clr"></div>
                    
            </div>
            <!-- en search -->
            
          
            <!-- info user -->
            <div class="boxAll blogAddContent">
                
                    <div id="msg_error_msg">
                          
                    </div>
                    <input  name="data[<?php echo $id_field; ?>]" type="hidden" value="<?php echo $obj->$id_field;?>"/>
                     <div class="fromContent rowC2">
                         <h1>Trạng thái:</h1>
                        <label>Trạng thái: </label>
                      <label><input type="radio" <?php if($obj->status==1) echo 'checked=checked';?> name="data[status]"  value="0"/>Ẩn</label>
                      <label><input type="radio" <?php if($obj->status==1) echo 'checked=checked';?> name="data[status]"  value="1"/>Hiện</label>
                      <div>&nbsp;</div>
                    </div>
                <?php if($model->table_name_lang){?>
                     <div class="fromContent rowC2">
                    <h1>Nội dung đa ngôn ngữ:</h1>
            	<div id="lang-tabs">
  <ul>
    <?php foreach($langlist as $lg){ ?>  
    <li><a href="<?php echo $this->uri->uri_string;?>#lang-tabs-<?php echo $lg->lang_id;?>"><?php echo $lg->name;?></a></li>
    <?php } ?>
  </ul>
  <?php foreach($langlist as $lg){ ?>  
  <div id="lang-tabs-<?php echo $lg->lang_id;?>">
    <?php if($model->fields_lang['name']){ ?>
            	  <h1>Tiêu đề và tóm tắt (<?php echo $lg->name;?>)</h1>
            
            	  <div class="fromContent rowC1">
                  	<label>Tiêu đề</label> 
                        <input <?php if($lg->lang_id==1){ ?> required vname="Tiêu đề (<?php echo $lg->name;?>)" <?php } ?> type="text" name="data[_mlang][<?php echo $lg->lang_id;?>][name]" placeholder="" value="<?php echo $mlang[$lg->lang_id]->name;?>" />
                        <div class="error"></div>
                        <br />
                        <?php if($model->fields_lang['description']){ ?>
                    <label class="blockC">Tóm tắt <span class="red">(Tối đa 150 ký tự)</span></label> 
                    <textarea <?php if($lg->lang_id==1){ ?> required maxlength="150" vname="Tóm tắt (<?php echo $lg->name;?>)" <?php } ?> name="data[_mlang][<?php echo $lg->lang_id;?>][description]"><?php echo $mlang[$lg->lang_id]->description;?></textarea><br />
                        <?php } ?>
                  </div>
  <?php } ?>  
      
  </div>
  <?php } ?>
</div>	
                <style>
             .ui-helper-clearfix:after {
  clear: none;
  height: 26px;
}
#lang-tabs ul li  {
  height: 24px;
  line-height: 12px;
}
#lang-tabs{
    margin-bottom:10px;
}
             </style>   
                <script>
  $(function() {
    $( "#lang-tabs" ).tabs();
  });
  </script>
                     </div>
                <?php } ?>
  <?php if($model->fields['name']){ ?>
            	  <h1>Tiêu đề và tóm tắt</h1>
            
            	  <div class="fromContent rowC1">
                  	<label>Tiêu đề</label> <input name="data[name]" type="text" placeholder="" /><div class="error"></div><br />
                    <label class="blockC">Tóm tắt <span class="red">(Tối đa 150 ký tự)</span></label> 
                    <textarea></textarea><br />
                  </div>
  <?php } ?>        
                  <h1>Thông tin khác</h1>
            
            	  <div class="fromContent rowC2">
                  		<div class="col">
            			<label>Title 1</label> <input type="text" placeholder="" /><div class="error"></div><br />
                	    <label>Title 2</label> <select><option>Vietnam</option></select> <div class="error"></div><br /> 
                      </div>
                      <div class="col">
                           <label>Title 3</label> <select><option>Vietnam</option></select> <div class="error"></div><br />
                          <label>Title 4</label> <input type="text" placeholder="" /><div class="error"></div><br />  
                      </div>
                      <div class="clr"></div>
                  </div>
                  
                  <h1>Hình ảnh cho thiết bị và web</h1>
            
            	  <div class="fromContent rowC1">
                  	
                    	<fieldset>
                            <legend>Hình cho web:</legend>
                            <label>Hình 1</label> <input type="file" />  <img src="images/logo.png" /><div class="error"></div><br /> 
                        </fieldset>
                    
                  </div>
                  
                  <h1>Thêm ca sĩ, diễn viên và tag</h1>
            
            	  <div class="fromContent rowC3">
                  		<div class="col">
            			<label>Chọn diễn viên</label> <input type="text" placeholder="" /> <input type="submit" class="bntAll" value="Thêm diễn viên"><br />
                        <div class="boxAdds"><span class="tagC">Dien vien 1 <a href="#" class="closeTag">X</a></span></div>
                	    
                      </div>
                      <div class="col">
                          <label>Chọn tag</label> <input type="text" placeholder="" /> <input type="submit" class="bntAll" value="Thêm tag"><br /> 
                          <div class="boxAdds"><span class="tagC">Dien vien 1 <a href="#" class="closeTag">X</a></span></div> 
                      </div>
                      <div class="clr"></div>
                  </div>
                  
                  <h1>Mô tả</h1>
            
            	  <div class="fromContent rowC1">
                  	
                    	<div class="boxAdds">
                            <textarea name="name[content]" id="content_here"></textarea>
                        </div>
                    
                  </div>
                  
                  <div class="alc"><input type="button" onclick="submit_form($('#frm_add_edit'),'<?php echo $returnurl; ?>');" class="bntAll" value="Lưu lại và đóng"></div>
                
            </div>
            <!-- en info user -->
            
          </form>   
       </div> 
<script type='text/javascript'>
	CKEDITOR.replace('content_here');
        
        /**global javascript**/
        $(function() {
    
            $( ".input-sortable" ).sortable({
              placeholder: "ui-state-highlight"
            });

          });

</script>		
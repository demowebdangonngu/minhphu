<div class="tn-boxlivesco matchs-statis">
          <table class="table table-nobordered">
              <?php if($live->homegoaldetails||$live->awaygoaldetails){ ?>
            <tr>
              <td class="boxlivesco-tdl text-right">
                  <?php echo $live->homegoaldetails;?>
              </td>
              <td class="text-center"><img src="<?php echo $this->config->item('static_path')."themes/front/images/";?>graphics_ico/lsc-01.png" width="20" height="20" alt="Goal"></td>
              <td class="boxlivesco-tdr">
                  <?php echo $live->homegoaldetails;?>
              </td>
            </tr>
            <?php } ?>
            <?php if($live->homeyellowcarddetails||$live->awayyellowcarddetails){ ?>
            <tr>
              <td class="boxlivesco-tdl text-right">
              <?php echo $live->homeyellowcarddetails; ?>
              </td>
              <td class="text-center"><img src="<?php echo $this->config->item('static_path')."themes/front/images/";?>graphics_ico/lsc-03.png" width="20" height="20" alt="Yellow card"></td>
              <td class="boxlivesco-tdr">
              <?php echo $live->awayyellowcarddetails; ?>
              </td>
            </tr>
            <?php } ?>
            <?php if($live->homeredcarddetails||$live->awayredcarddetails){ ?>
            <tr>
              <td class="boxlivesco-tdl text-right">
              <?php echo $live->homeredcarddetails; ?>
              </td>
              <td class="text-center"><img src="<?php echo $this->config->item('static_path')."themes/front/images/";?>graphics_ico/lsc-04.png" width="20" height="20" alt="Red card"></td>
              <td class="boxlivesco-tdr">
              <?php echo $live->awayredcarddetails; ?>
              </td>
            </tr>
            <?php } ?>
            <?php if($live->homesubstitutions||$live->awaysubstitutions){ ?>
            <tr>
              <td class="boxlivesco-tdl text-right">
              <?php echo $live->homesubstitutions; ?>
              </td>
              <td class="text-center"><img src="<?php echo $this->config->item('static_path')."themes/front/images/";?>graphics_ico/lsc-05.png" width="20" height="20" alt="in out"></td>
              <td class="boxlivesco-tdr">
              <?php echo $live->awaysubstitutions; ?>
              </td>
            </tr>
            <?php } ?>
          </table>
        </div>
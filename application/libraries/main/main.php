<?php

class Main extends MX_Controller {

    private $model;

    public function __construct() {
        $this->model = $this->model_home;
    }

    public function banner($pro_id, $cat_id) {
        $this->model->db_slave->select('name, image, link, target');
        $this->model->db_slave->from('tbl_banner_slider');
        $arr = array(
            'status' => 1,
            'pro_id' => $pro_id,
            'cat_id' => $cat_id
        );
        $this->model->db_slave->where($arr);
        $this->model->db_slave->order_by('sort_order', "asc");
        $data['banner'] = $this->model->db_slave->get()->result();
        $result = $this->load->view('home/banner', $data, true);
        $this->redis->set('banner-' . $pro_id . '-' . $cat_id, $result, CACHE5);
        return $result;
    }

    function get_id_url($url, $sep = "-") {
        $url = str_replace(".html", "", $url);
        $url = explode($sep, $url);
        $id = $url[count($url) - 1];
        return is_numeric($id) ? $id : 0;
    }

    public function check_ip_client() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $_SERVER['REMOTE_ADDR'] = $ip;
        return $ip;
    }

    //convert s -> H:i:s
    public function convertSecondToTime($s) {
        $h = intval($s / 3600);
        $i = intval(($s - (3600 * $h)) / 60);
        $s = ($s - ((3600 * $h) + (60 * $i)));
        return $h . ':' . $i . ':' . $s;
    }

    // convert s -> i:s
    public function convertSecondToMinute($s, $returnS = false) {
        $i = intval($s / 60);
        if ($returnS) {
            $s = $s - ($i * 60);
            return $i . ':' . $s;
        }
        return $i;
    }

    public function breadcrumb(array $data, $name = '') {
        
        $name = ucfirst(strtolower($name));
        
        
        $html = '<ul class="breadcrumb">';
        $html .= '<li itemtype = "http://data-vocabulary.org/Breadcrumb" itemscope><a href = "'.base_url().'" itemprop = "url"><span itemprop = "title">Trang chủ</span></a></li>';
        if (!empty($data)) {
            $count = count($data); 
            for ($i = 0; $i < $count; $i++) {
                $data[$i]['name'] = ucfirst(strtolower($data[$i]['name']));
                $html .= '<li itemtype = "http://data-vocabulary.org/Breadcrumb" itemscope><a href = "' . base_url($data[$i]['url']) . '" itemprop = "url"><span itemprop = "title">' . $data[$i]['name'] . '</span></a></li>';
            }
        }
        if (!empty($name)) {
            $html .= '<li itemtype="http://data-vocabulary.org/Breadcrumb" class="active" ><span itemprop="title">' . $name . '</span></li>';
        }
        $html .= '</ul>';
        return $html;
    }

    public function getModuleFromCat($cat_id) {
        switch ($cat_id) {
            case 1:
                $rs['module'] = 'phimle';
                $rs['func'] = 'theloai';
                break;
            case 2:
                $rs['module'] = 'phimbo';
                $rs['func'] = 'quocgia';
                break;
            case 3:
                $rs['module'] = 'show';
                $rs['func'] = 'quocgia';
                break;
            case 4:
                $rs['module'] = 'clip';
                $rs['func'] = 'theloai';
                break;
        }
        return $rs;
    }

    public function tooltip($data, $height) {
        $SEO = $this->load->library('SEO'); 
        $result = ''; 
		$link = trim(substr($data->image,  url_image(), 1000));
        if (!empty($data)) {
      
			$result = '<li class="item" itemtype="http://schema.org/Product" itemscope="" >';
				$result.= '<div class="item-inner">';
					$result.= '<div class="thumbs-wrap">';
						$result.= '<a class="entry-thumbs" itemprop="url" title="'.$data->name.'" href="'.$SEO->build_link($data,"sanpham").'">';
							$result.= '<img itemprop="image" src="'.base_url().'themes/upload/'.$height.'/'.$link.'" title="'.$data->name.'" alt="'.$data->name.'" class="img-responsive">';
						$result.= '</a>'; 
					$result.= '<div class="area-favorite" onclick="add_wish_list(' ."'".$data->id."'" .');" ><i class="glyphicon glyphicon-heart-empty"></i>Yêu thích</div>';
				$result.= '</div>';
				
				$result.= '<div class="meta">'; 
					$result.= '<div class="entry-price" itemtype="http://schema.org/Offer" itemscope="" itemprop="offers"   >';
						if($data->price == 0){
                            $result.= '<span class="price-sale">Liên hệ</span>';
                        }else{
							if($data->price >= $data->price_sale && $data->price_sale != 0){
                                $result.= '<span class="price-sale">'.number_format($data->price_sale).'đ</span>';
                                $result.= '<span class="price-sale-off">'.number_format($data->price).'đ</span>';
                                $result.= '<meta content="'.number_format($data->price_sale/1000).'" itemprop="price">';
                            }else{ 
                                $result.= '<span class="price-sale">'.number_format($data->price).'đ</span>';
                                $result.= '<meta content="'.number_format($data->price/1000).'" itemprop="price">';
                            } 
						} 
					$result.= '<meta content="" itemprop="priceCurrency">';
					$result.= '</div>';
					$result.= '<div class="entry-name"><a href="'.$SEO->build_link($data,"sanpham").'" title="" itemprop="name">'.$data->name.'</a></div>'; 
					$result.= '<div class="entry-code">Mã: <span>'.$data->code_product.'</span></div>';
				$result.= '</div>';
				
			 
				if($data->price >= $data->price_sale && $data->price_sale != 0){
					$result.= '<span class="stick-label">-'.ceil(100 - ($data->price_sale/$data->price *100)).'%</span>';
				}   
			 
				
			$result.= '</li>';     
        }
        return $result;
    }

    public function checkLinkYoutube($link) {
        $pos = strpos($link, 'youtube');
        return ($pos === false) ? false : true;
    }

    public function paging($base_url, $total, $per_page, $uri_segment, $num_links = 3, $cur_page = 0, $prefix = '') {
        $this->load->library('pagination');
        $config['base_url'] = $base_url;
        $config['total_rows'] = $total;
        $config['per_page'] = $per_page;
        $config['num_links'] = $num_links;
        $config['uri_segment'] = $uri_segment;
        $config['first_tag_open'] = '<li class="first">';
        $config['first_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li class="next">';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="last">';
        $config['last_tag_close'] = '</li>';
        if ($cur_page !== 0 && is_numeric($cur_page)) {
            $config['cur_page'] = $cur_page;
        }
        if (!empty($prefix)) {
            $config['prefix'] = 'trang-';
        }
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    public function getCurrentUrlWithQuery($md5 = 1, $query = 1) {
        $currentUrl = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . str_replace('\\', '/', $_SERVER['REQUEST_URI']);
        $queryUrl = http_build_query($_GET, '', "&");
        if ($query === 1 && $queryUrl !== '') {
            $currentUrl .= '?' . $queryUrl;
        }
        $currentUrl = rtrim(trim($currentUrl), '/');
        if ($md5) {
            $currentUrl = md5($currentUrl);
        }
        return $currentUrl;
    }

    public function getNameContent($str) {
        $arr = explode('(', $str);
        $result['name_main'] = (isset($arr[1])) ? substr($str, 0, strpos($str, "(")) . substr($str, strpos($str, ')') + 1, strlen($str)) : $str;
        $result['name_ext'] = (isset($arr[1])) ? substr($str, strpos($str, "(") + 1, strpos($str, ")") - strpos($str, "(") - 1) : '';

        $result['name_main'] = trim($result['name_main']);
        $result['name_ext'] = trim($result['name_ext']);
        return $result;
    }

    public function getNameFullTextSearch($str) {

        $str = trim($str);
        $arr = array();
        $arr = explode('-', $str);
        foreach ($arr as $k => $v) {
            if (strlen($v) == 1)
                $arr[$k] = $v . '00';
            if (strlen($v) == 2)
                $arr[$k] = $v . '0';
        }
        $str = implode('-', $arr);
        return $str;
    }

    public function btn_facebook($url) {
        echo '<div class="widget-social"><div class="fanpage">';
        echo '<div class="fb-like" data-href="' . $url . '" data-layout="button_count" data-action="like" data-width="180" data-show-faces="true" data-share="true"></div>';
        echo '<div class="g-plus" data-action="share" data-href="' . $url . '" data-annotation="none"></div>';
        echo '</div></div>';
    }

    public function info_img($type, $obj) {
        $name = "";
        $type2 = 'Phim';
        if (isset($obj->event_name)) {
            $name = $obj->event_name;
            $type2 = 'Event';
        }
        if ($type == 'Avatar')
            $type2 = '';
        if (isset($obj->name)) {
            $name = $obj->name;
            if (isset($obj->cat_id)) {
                switch ($obj->cat_id) {
                    case 3:
                        $type2 = 'Show';
                        break;
                    case 4:
                        $type2 = 'Clip';
                        break;
                }
            }
        }
        $name = htmlspecialchars($name, ENT_QUOTES);
        return "alt='$type $type2 $name'";
    }

    public function link_html_title($obj) {
        $type2 = 'Phim';
        if (isset($obj->name)) {
            $name = $obj->name;
            if (isset($obj->cat_id)) {
                switch ($obj->cat_id) {
                    case 3:
                        $type2 = 'Show';
                        break;
                    case 4:
                        $type2 = 'Clip';
                        break;
                }
            }
        }
        $name = htmlspecialchars($name, ENT_QUOTES);
        return " title='$type2 $name' ";
    }

    public function rewriteUrl($content , $event = false) {
        if($event){
            $url = 'event/index/'.$content->event_id; 
        }else{
            switch ($content->cat_id) {
                case 1:
                case 2:
                    $prefixSeg2 = 'xem-phim-';
                    $posfixSeg1 = 'phim-' . remove_accent($content->genre_name);
                    break;
                case 3:
                    $prefixSeg2 = 'xem-show-';
                    $posfixSeg1 = 'show-' . remove_accent($content->genre_name);
                    break;
                case 4:
                    $prefixSeg2 = 'xem-clip-';
                    $posfixSeg1 = 'clip-' . remove_accent($content->genre_name);
                    break;
                default :
                    $prefixSeg2 = 'xem-phim-';
                    $posfixSeg1 = 'phim-' . remove_accent($content->genre_name);
                    break;
            } 
            $url = $posfixSeg1 . '/' . $prefixSeg2 . remove_accent($content->name) . '-' . $content->content_id . '.html';
        }
        
        $cacheAliasUrl = $this->redis->hGet(ALIASURL, $url);
        if ($cacheAliasUrl !== FALSE) {
            $cacheAliasUrl = json_decode($cacheAliasUrl);
            $url = $cacheAliasUrl->aliasurl; 
        }
        return base_url($url);
    }

    public function rewriteUrlCate($module, $name = '') {
        if (!empty($name)) {
            switch ($module) {
                case 'phim-le':
                case 'phimle':
                    $url = 'phim-le/phim-' . remove_accent($name);
                    break;
                case 'phim-bo':
                case 'phimbo':
                    $url = 'phim-bo/phim-' . remove_accent($name);
                    break;
                case 'show':
                    $url = 'show/show-' . remove_accent($name);
                    break;
                case 'clip':
                    $url = 'clip/clip-' . remove_accent($name);
                    break;
            }
        } else {
            switch ($module) {
                case 'phim-le':
                case 'phimle':
                    $url = 'phim-le';
                    break;
                case 'phim-bo':
                case 'phimbo':
                    $url = 'phim-bo';
                    break;
                case 'show':
                    $url = 'show';
                    break;
                case 'clip':
                    $url = 'clip';
                    break;
            }
        }

        $cacheAliasUrl = $this->redis->hGet(ALIASURL, $url);
        if ($cacheAliasUrl !== FALSE) {
            $cacheAliasUrl = json_decode($cacheAliasUrl);
            $url = $cacheAliasUrl->aliasurl;
        }
        return $url;
    }

    public function rewriteUrlTag($module, $name = '') {
        $url = base_url();
        if (!empty($name)) {
            switch ($module) {
                case 'dienvien':
                    $url = 'dien-vien/' . ($name);
                    break;
                case 'daodien':
                    $url = 'dao-dien/' . ($name);
                    break;
                case 'tag':
                    $url = 't/' . ($name);
                    break;
                case 'timkiem':
                    $url = 'tim-kiem/' . ($name);
                    break;
            }
        } else {
            switch ($module) {
                case 'dienvien':
                    $url = 'dien-vien';
                    break;
                case 'daodien':
                    $url = 'dao-dien';
                    break;
                case 'tag':
                    $url = 't';
                    break;
            }
        }

        $cacheAliasUrl = $this->redis->hGet(ALIASURL, $url);
        if ($cacheAliasUrl !== FALSE) {
            $cacheAliasUrl = json_decode($cacheAliasUrl);
            $url = $cacheAliasUrl->aliasurl;
        }
        return $url;
    }

    public function fanpage() {
        echo '<div class="box sb-box fb-page">';
        echo '<div class="fb-page" data-href="https://www.facebook.com/VNmegabox?ref=ts&amp;amp;fref=ts" data-width="300" data-height="520" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/VNmegabox?ref=ts&amp;amp%3Bfref=ts"><a href="https://www.facebook.com/VNmegabox?ref=ts&amp;amp%3Bfref=ts">Megabox.vn</a></blockquote></div></div>';
        echo '</div>';
    }

    public function tab_block() {
        $this->load->model('tag/model_tag', 'tag');
        $tag = $this->tag->getHomeTags();
        if (!empty($tag)) {
            echo '<div class="tagCloud">';
            echo '<div class="tagCloud-title">TAG</div>';
            echo '<div class="tagCloud-container">';
            foreach ($tag as $v) {
                echo "<a href='" . base_url('t/' . remove_accent($v->name) . '-' . $v->tag_id) . "'><span class='bg-tag-left'><span class='bg-tag-body'>" . $v->name . "</span></span></a>";
            }
            echo '</div></div>';
        }
    } 
    public function formatView($num) {
        return number_format($num, 0, ',', '.');
    } 

}

?>
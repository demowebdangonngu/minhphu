<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
if(APP_TYPE=='admin'){
$route['default_controller'] = "home";
$route['404_override'] = '';
$route['pages'] = 'pages';

//$route['(:num)-(:any).html'] = "providers/index/pro_id/$1";
}else{
    
    
$route['default_controller'] = "home";
$route['404_override'] = 'category/detailNoFound';



$route['tim-kiem/(:any)'] = "search/index/$1"; 
$route['tim-kiem-tin/(:any)'] = "search/tintuc/$1"; 
$route['product/(:any)-(:num).html'] = "product/detail/$2";
$route['tin-tuc/(:any)-(:num).html'] = "posts/detail/$2";
$route['tin-tuc'] = "posts/index";

// $route['cat-(:any)/trang-(:num)'] = "category/detail/$1/$2";
// $route['cat-(:any)'] = "category/detail/$1";
// $route['cat-(:any)'] = "category/detail/$1";
$route['pages/(:any)'] = "pages/index/$1";
 
$route['cartorder/(:any)'] = "cartorder/$1";
$route['cartorder'] = "cartorder/index";

$route['cartorder'] = "cartorder/index";

 

//$route['index.html'] = "home";
/*$route['tim-kiem/(:any)'] = "search/index/$1"; 
$route['tin-tuc'] = "baiviet/index";
$route['tin-tuc.html'] = "baiviet/index";
$route['tin-tuc.html/(:any)'] = "baiviet/index/$1";

$route['registered'] = "registered/index";


$route['huong-dan-mua-hang.html'] = "pages/huong-dan-mua-hang.html";
$route['gioi-thieu.html'] = "pages/gioi-thieu.html";
$route['lien-he.html'] = "pages/lien-he.html";

$route['sanpham/(:any)-(:num).html'] = "sanpham/detail/$2";
$route['baiviet/(:any)-(:num).html'] = "baiviet/detail/$2";

$route['pages/(:any)'] = "pages/index/$1";
$route['order/(:any)'] = "cartorder/email_order/$1";
*/


}
//$route['providers/([a-z]+)/(\d+)'] = "$1/id_$2";products/shirts/123


/* End of file routes.php */
/* Location: ./application/config/routes.php */
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
 


define('FB_ID', '846327672104446'); 
define('FB_ID_ADMIN', '123123'); 
define('NAME_SITE', 'CAMERA NO1'); 





define('PER_PAGE_MAIN', 16); 
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
define("BASE_URL", 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . str_replace('//', '/', dirname($_SERVER['SCRIPT_NAME']) . '/'));
define("BACKEND", "admincp");
define("HAYHAYTV", "hayhaytv");
define("KEYFP", "hayhaytv2014");


define("SDT", "0902 414 565 - 0934 143 565");

//image
define('UPLOAD_LINK_IMAGE','http://img.hayhaytv.com.vn/upload/');
define("LINK_IMAGE", "http://img.hayhaytv.com.vn:81/");
// thumb : http://img.hayhaytv.com.vn:81/150x150xt/
//crop   : http://img.hayhaytv.com.vn:81/150x150/ 
define('UPLOAD_NEWS','news/');
define('UPLOAD_LOGO','logo/');
define('UPLOAD_LIVETV','livetv/');
define('UPLOAD_FILM','film/');
define('UPLOAD_contents','news/');
define('UPLOAD_LANGUAGES','languages/');
define('UPLOAD_POSTER','poster/');
define('UPLOAD_HOMEPAGE','homepage/'); 
define('UPLOAD_TRAILER','trailer/');
define('UPLOAD_WHATHOT','whathot/');
define('UPLOAD_EVENT','event/');
define('UPLOAD_SEASON','season/');
define('UPLOAD_SUBTITLE','subtitle/');
define('UPLOAD_ACTOR','actor/');
define('UPLOAD_DIRECTOR','director/');
define('UPLOAD_ADS','ads/');
define('UPLOAD_ADSBANNER','adsbanner/');
define('UPLOAD_ADSCLIP','ads_clip/');
define('UPLOAD_SHOW','show/');
define('UPLOAD_CLIP','clip/');
define('UPLOAD_POPUP','popup/');

define('BANNER_DEFAULT','default/banner_default.jpg');
define('POSTER_DEFAULT','default/poster_default.jpg');

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

// Mongodb
define('USERNAME_MONGO', 'hayhay_v2');
define('PASSWORD_MONGO', 'fssfsdczxcf');
define('DATABASE_MONGO','hayhay_v2');
define('HOST_MONGO','113.164.15.102:27017');
// Mongodb test
//define('USERNAME_MONGO','hayhaytv_mgo');
//define('PASSWORD_MONGO','dfpqnchfksfe$');
//define('DATABASE_MONGO','hayhaytv');
//define('HOST_MONGO','127.0.0.1:27017');

/* End of file constants.php */
/* Location: ./application/config/constants.php */
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SEO
 *
 * @author Administrator
 */
class SEO {
    /**
     * 
     * @param type $obj object
     * @param string $type category,post,video
     */
    function build_link($obj,$type='post',$data=array()){
        switch ($type) {
            case "post":
                $CI=  get_instance();
                $categories=$CI->load->model("categories/public_categories_model");
                $catebase="uncategory";
                if($category=$categories->get_node($obj->cid)){
                    $catebase=$category->slug?$category->slug:  remove_accent($category->name);
                }
                $slug= $obj->slug?$obj->slug:  remove_accent($obj->title);
                return base_url()."content/detail/".$slug."-".$obj->id.".html";

                break;
            case "mathang23123":
                $CI=  get_instance();
                $code= $code->slug?$code->slug:  remove_accent($obj->name);
                return base_url()."mathang/".$code."-".$obj->id_mathang.".html";

                break;
            case "sanpham":
               return base_url()."sanpham/xem/".remove_accent($obj->mhname)."-".($obj->id_mathang).".html";
            case "menu":
                 if($node->type='category'){
                     return base_url().$obj->slug;
                 }
                 return base_url().$obj->link;
               
                break;
            case "post_tag":
                     return base_url()."tag/".$obj->slug;
                break;
              case "tag":
                     return base_url()."tags/".$obj->slug."-".$obj->id; 
                break;    
             case "content":
                     return main_url()."contents/detail/".remove_accent($obj->title)."-".$obj->id.".html";
               
                break;   
           case "video":
                     if($obj->cid)
                        return main_url()."contents/detail/".remove_accent($obj->ctitle?$obj->ctitle:$obj->name)."-".($obj->cid).".html";
                     else return main_url()."videos/xem/".remove_accent($obj->name)."-".($obj->id).".html";
               break;
           case "video_section":
                     return base_url()."videos/section/".remove_accent($obj->name)."-".$obj->id;
                break;
            default:
                return base_url().$obj->slug;
                break;
        }
    }
    public $title;
    public $description;
    public $keywords;
    function build_meta($obj,$type='post'){
        global $CFG;
        switch ($type) {
            case 'post':
               $this->title=$obj->title?$obj->title:$obj->title_short;
                $this->description=$obj->decription?$obj->decription:$obj->decription_short;
                //$this->keywords=$obj->keywords;
                $this->ogimage=$CFG->config['img_path']."470x320xt".$obj->image;
                $this->ogwidth=470;
                $this->ogheight=   320;     
                $this->ogtitle=$obj->title_short?$obj->title_short:$obj->title;
                $this->obdescription=$obj->summary_short?$obj->summary_short:$obj->summary;
                break;

            default:
                $this->title=$obj->title?$obj->title:$obj->name;
                $this->description=$obj->decription?$obj->decription:$obj->decription_short;
                $this->ogtitle=$this->title;
                
                break;
        }
    }
}

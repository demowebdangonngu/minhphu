<div id="UpdatePanel1">
   <div class="main">
      <div class="container detail">
         <div class="row">
            <div class="col-md-12">
             
                <?php
             if(!empty($listOrder)){
                ?>
                
                <div id="pnl">
                  <div class="span11 r-form"> 
                     Hóa đơn đặt hàng của email: <b><?php echo $listOrder[0]->email;  ?></b> <br />
                     <b style="color:#FF1414"> 
                     Hỗ trợ: <?php echo SDT; ?></b><br> 
                     <hr>
                  </div> 
               </div>
               <table class="table table-bordered">
                  <thead>
                     <tr class="row1">
                        <th class="title">ID</th>
                        <th class="title">Tên</th>
                        <th class="title" width="30%">Địa chỉ</th>
                        <th class="title" width="10%">Phone</th>
                        <th class="title" width="10%">Ngày đặt</th>
                        <th class="title" width="10%">Tổng tiền</th>
                        <th class="title" width="10%">Tình trạng</th>
                     </tr>
                  </thead>
                  <tbody> 
                    <?php  
                    foreach($listOrder as $k=>$v){  
                        ?>
                  
                     <tr class="row0">
                        <td align="center">   
                           <span id="lblPrice" class="price-cart"><?php echo $v->id; ?></span>                          
                        </td>
                        <td align="center">
                           <span id="lblPName">
                                <a href="<?php echo base_url().'order/'.urlencode($listOrder[0]->email).'/'.$v->id; ?>"><?php echo $v->name; ?></a> 
                           </span>
                        </td> 
                        <td align="center">   
                           <span id="lblPrice" class="price-cart"><?php echo $v->address; ?></span>                          
                        </td>
                        <td align="center">   
                           <span id="lblPrice" class="price-cart"><?php echo $v->phone; ?></span>                          
                        </td>
                        <td align="center">   
                           <span id="lblPrice" class="price-cart"><?php echo $v->order_date; ?></span>                          
                        </td> 
                        <td align="center">   
                           <span id="lblPrice" class="price-cart"><?php echo number_format($v->total_price); ?></span>    VND                          
                        </td>
                        <td align="center">   
                           <span id="lblPrice" class="price-cart"><?php echo $v->status==1?'<b style="color:blue;">Đã xác nhận</b>':($v->status==2?'<b>Đã giao hàng</b>':'<b style="color: red;">Đợi duyệt</b>'); ?></span>                          
                        </td> 
                         
                     </tr>
                     <?php                  
                    } 
                    ?> 
                  </tbody>
               </table> 
               <div class="row">
                  <div class="update-cart"> 
                     <a style="color: #FFFFFF;" href="<?php echo base_url(); ?>" class="submit">Mua sản phẩm khác</a> 
                  </div>
               </div> 
               
               <?php
               }else{ 
                    ?><p id="emptyCartWarning" class="warning">Không có hóa đơn nào !! </p><?php
               }
               ?>
            </div>
         </div>
      </div>
   </div>
</div> 
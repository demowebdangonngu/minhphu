  
<div class="container">
   <?php echo $breadcumb; ?>
   <div class="row">
      <section id="sidebar-main" class="col-md-12">
         <div id="content" class="wrapper clearfix">
            <h1>Giỏ Hàng              </h1>
            <!--<p>Giỏ hàng của bạn đang trống!</p>-->
                
               <?php
                if($count){
                ?>
                        
                       <div class="table-responsive">
                          <table class="table table-bordered">
                             <thead>
                                <tr>
                                   <td class="text-center">Hình ảnh</td>
                                   <td class="text-left">Tên sản phẩm</td>
                                   <td class="text-left">Mã hàng</td>
                                   <td class="text-left">Số lượng</td>
                                   <td class="text-right">Đơn Giá</td>
                                   <td class="text-right">Tổng cộng</td>
                                </tr>
                             </thead>
                             <tbody> 
                                <?php 
                                $sp = $this->load->model('product/model_product');
                                foreach($cart_content as $k=>$v){
                                    $where['product.id'] = $v['name'];
                                    $rsp = $sp->getList($where);
                                    ?>
                                    <tr>
                                       <td class="text-center">                  
                                            <a href="<?php echo $SEO->build_link($rsp[0],"sanpham")?>">
                                                <img src="<?php echo $this->config->item("img_path")  . $rsp[0]->image; ?>" alt="<?php echo $rsp[0]->name; ?>" title="<?php echo $rsp[0]->name; ?>" class="img-thumbnail">
                                            </a>
                                       </td>
                                       <td class="text-left">
                                            <a href="<?php echo $SEO->build_link($rsp[0],"sanpham")?>"><?php echo $rsp[0]->name; ?></a>
                                       </td>
                                       <td class="text-left"><?php echo $rsp[0]->name; ?></td>
                                       <td class="text-left">
                                          <div class="input-group btn-block" style="max-width: 200px;">
                                             <input  type="text" value="<?php echo $v['qty']; ?>" class="form-control" id="quantity" onchange="change_qty(this.value , '<?php echo $k; ?>');" /> 
                                             <span class="input-group-btn">
                                             <button type="button" data-toggle="tooltip" title="" class="btn btn-inverse" onclick="remove_cart('<?php echo $k; ?>');" data-original-title="Loại bỏ"><i class="fa fa-times-circle"></i></button></span>
                                          </div>
                                       </td>
                                       <td class="text-right"><?php echo number_format($v['price']); ?>VNĐ</td>
                                       <td class="text-right"><?php echo number_format($v['subtotal']); ?>VNĐ</td>
                                    </tr>
                              
                               
                                 <?php                  
                                } 
                                ?> 
                         
                             </tbody>
                          </table>
                       </div>
             
                  
                    <div class="row">
                       <div class="col-sm-4 col-sm-offset-8">
                          <table class="table table-bordered">
                             <tbody>
                                <tr>
                                   <td class="text-right"><strong>Thành tiền::</strong></td>
                                   <td class="text-right"><?php echo $total_price; ?>VNĐ</td>
                                </tr>
                                <tr>
                                   <td class="text-right"><strong>Tổng cộng ::</strong></td>
                                   <td class="text-right"><?php echo $total_price; ?>VNĐ</td>
                                </tr>
                             </tbody>
                          </table>
                       </div>
                    </div>
                    <div class="buttons">
                       <div class="pull-left"><a href="<?php echo base_url(); ?>" class="btn btn-default">Tiếp tục mua hàng</a></div>
                       <div class="pull-right"><a href="<?php echo base_url().'checkout'; ?>" class="btn btn-outline">Thanh toán</a></div>
                    </div>

               <?php
               }else{ 
                    if($ordered){
                        ?>
                        <script>$(document).ready(function (e) {
                          swal("Đặt hàng thành công... Chúng tôi sẽ liên lạc lại ngay sau khi nhận được đơn hàng. Xin chân thành cảm ơn! ", "", "success");
                        });</script>
                        <p id="emptyCartWarning" class="warning">Đặt hàng thành công... Chúng tôi sẽ liên lạc lại ngay sau khi nhận được đơn hàng. Xin chân thành cảm ơn! </p>
                        <?php
                    }else{
                        ?>
                        <script>$(document).ready(function (e) {
                          swal("Giỏ hàng rỗng.", "Error!","error");
                           
                        });</script>
                        <p id="emptyCartWarning" class="warning">Giỏ hàng rỗng.</p>
                        <?php
                    } 
               }
               ?> 
                
                
         </div>
      </section>
   </div>
   <footer id="footer">
                <?php echo Modules::run('footer'); ?>
           </footer> 
</div>





 
<script> 

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    // alert( pattern.test(emailAddress) );
    return pattern.test(emailAddress);
}; 
 
function submit_cart(){
    var error = false;
    var message_error = ''; 
    
    
    if (!isValidEmailAddress($("#email").val())) {
        error = true;
        message_error += "Email.\n";
    }
    if ($("#name").val() == '') {
        error = true;
        message_error += "Tên.\n";
    }
    if ($("#phone").val() == '') {
        error = true;
        message_error += "Số điện thoại.\n";
    }
    
    if ($("#address").val() == '') {
        error = true;
        message_error += "Địa chỉ.\n";
    }
    

    // end check
    if (error == true) {
        swal("Vui lòng nhập: "+message_error, "");
        return false;

    } else {
        return true;
    }
    
    
    
}
</script>
 
 
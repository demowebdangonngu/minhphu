<div id="UpdatePanel1">
   <div class="main">
      <div class="container detail">
         <div class="row">
            <div class="col-md-12">
             
                <?php
             if(!empty($listOrderCT)){
                ?>
                
                <div id="pnl">
                  <div class="span11 r-form"> 
                     Hóa đơn đặt hàng của email: <b><?php echo $listOrderCT[0]->email;  ?></b> <br />
                     ID Hóa đơn: <?php echo $listOrderCT[0]->order_id;  ?> <br /> 
                     <b style="color:#FF1414"> 
                     Hỗ trợ: <?php echo SDT; ?></b><br> 
                     <hr>
                  </div> 
               </div>
               <table class="table table-bordered">
                  <thead>
                     <tr class="row1">
                        <th class="title">STT</th>
                        <th class="title">Tên sản phẩm</th>
                        <th class="title" width="10%">Số lượng</th>
                        <th class="title" width="15%">Đơn giá</th>
                        <th class="title" width="15%">Thành tiền</th>  
                     </tr>
                  </thead>
                  <tbody> 
                    <?php $no = 1;
                    foreach($listOrderCT as $k=>$v){  
                        ?>
                  
                     <tr class="row0">
                        <td align="center">   
                           <span id="lblPrice" class="price-cart"><?php echo $no++; ?></span>                          
                        </td>
                        <td align="center">
                           <span id="lblPName">
                                <a href="<?php echo $SEO->build_link($v,"sanpham2")?>"><?php echo $v->name_sp; ?></a>
                                <br><br> 
                                <img height="120" border="0" alt="<?php echo $v->name_sp; ?>" src="<?php echo $this->config->item("img_path")  . $v->image; ?>">
                           </span>
                        </td>
                        
                        <td align="center">  
                            <span id="lblPrice" class="price-cart"><?php echo $v->quantity; ?></span> 
                        </td>
                        <td align="center">   
                           <span id="lblPrice" class="price-cart"><?php echo number_format($v->price); ?></span>    VND                          
                        </td>
                        <td align="center">  
                           <span id="lblTotal" class="price-cart"><?php echo number_format($v->price * $v->quantity); ?></span>     VND 
                        </td>
                         
                        
                         
                         
                     </tr>
                     <?php                  
                    } 
                    ?> 
                  </tbody>
               </table> 
               <div class="row">
                  <div class="update-cart"> 
                     <a style="color: #FFFFFF;" href="<?php echo base_url(); ?>" class="submit">Mua sản phẩm khác</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <span id="Label1">Tổng tiền:  <b><?php echo number_format($v->total_price); ?> VND</b></span>
                  </div>
               </div> 
               
               <?php
               }else{ 
                    ?><p id="emptyCartWarning" class="warning">Không có mặt hàng nào !! </p><?php
               }
               ?>
            </div>
         </div>
      </div>
   </div>
</div> 
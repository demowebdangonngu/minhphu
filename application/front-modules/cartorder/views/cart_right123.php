 
            <h4>
                <a href="<?php echo base_url()."cartorder"; ?>">Cart</a>
                <span id="block_cart_expand" class="hidden">&nbsp;</span>
                <span id="block_cart_collapse" >&nbsp;</span>
            </h4>
            <div class="block_content">
                <!-- block summary -->
                
                <div id="cart_block_summary" class="collapsed">
                    
                    <?php
                    
                    if(is_array($cart_content)){
                        ?>
                        <span class="ajax_cart_quantity"  ><?php echo $count; ?></span>
                        <span class="ajax_cart_product_txt_s"  >products</span>
                        <span class="ajax_cart_total"  ><?php echo $total_price?$total_price:0 ?> VND</span>
                        
                        
                        <?php
                    }else{ 
                        ?> 
                        <span class="ajax_cart_product_txt" >product</span> 
                        <span class="ajax_cart_no_product" >(empty)</span>
                        <?php
                    }
                    ?> 
                </div>
                <!-- block list of products -->
                <div id="cart_block_list" class="expanded">
                    <?php
                    
                    if(is_array($cart_content)){
                        ?>
                        <dl class="products">
                         <?php 
                         
                         $sp = $this->load->model('sanpham/model_sanpham');
                         
                         foreach($cart_content as $k=>$v){
                            
                            $where['product.id'] = $v['name'];
                            $rsp = $sp->getList($where);
                            
                            ?>
                            <dt id="cart_block_product_11" class="first_item">
                				<span class="quantity-formated"><span class="quantity"><?php echo $v['qty']; ?></span>x</span>
                				<a <?php echo seo_a($v); ?> href="<?php echo $SEO->build_link($rsp[0],"sanpham")?>" class="cart_block_product_name"   >
                				<?php 
                                $arr = explode(' ' , $rsp[0]->name);
                                echo $arr[0];
                                if($arr[1])   echo ' '.$arr[1];
                                    
                                ?>,...</a>
                				<span class="remove_link"><a onclick="remove_cart('<?php echo $k; ?>');" class="ajax_cart_block_remove_link"  href="javascript:void(0)" title="Xóa khỏi giỏ hàng"> </a></span>
                				<span class="price"><?php echo number_format($v['subtotal']); ?> VND</span>
                			</dt>
                            
                            <?php
                         }
                         
                         
                         ?>
                            
                        </dl>
                        <?php
                    }
                    
                    ?>
                
                    
                            
                            
						<!-- Customizable datas -->
						
                    
                    <div class="cart-prices"> 
                        <div class="cart-prices-block" style="height: 48px;">
                            <span>Total</span>
                            <br />
                            <span id="cart_block_total" class="price ajax_block_cart_total"><?php echo $total_price?$total_price:0 ?> VND</span>
                        </div>
                    </div>
                    
                    <p id="cart-buttons">
                        <a href="<?php echo base_url()."cartorder"; ?>" class="button_small" title="Cart">Xem</a>			<a href="javascript:void(0)" onclick="empty_cart();" id="button_order_cart" class="button" title="Check out">Xóa Giỏ hàng</a>
                    </p>
                </div>
            </div>
        
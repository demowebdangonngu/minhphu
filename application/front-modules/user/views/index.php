<form name="counter" style="display:none"><input type="text" size="8" name="d2"></form> 

<div class="container">
      <?php echo $breadcrumb;?>
   <div class="row">
      <section id="sidebar-main" class="col-md-9">
         <div id="content" class="wrapper clearfix">
            <?php echo $page_user;?>
         </div>
      </section>
      <aside id="sidebar-right" class="col-md-3">
         <column id="column-right" class="hidden-xs sidebar">
            <div class="box box-normal theme account">
               <div class="box-heading"><span>Tài khoản</span></div>
               <div class="box-content">
               <?php if($this->session->userdata('member_name')){?>
                  <div class="list-group">
                     <a href="<?php echo base_url(); ?>user/account" class="list-group-item">Tài Khoản Của Tôi</a>
                     <a href="<?php echo base_url(); ?>user/changeuser" class="list-group-item">Thay đổi tài khoản</a>
                     <a href="<?php echo base_url(); ?>user/changepass" class="list-group-item">Thay đổi mật khẩu</a>
                     <a href="<?php echo base_url(); ?>user/wishlist" class="list-group-item">Giỏ hàng</a> 
                     <a href="<?php echo base_url(); ?>user/order" class="list-group-item">Lịch sử đơn hàng</a>
                     <a href="<?php echo base_url(); ?>user/newsletter" class="list-group-item">Thư thông báo</a>
                     <a href="<?php echo base_url(); ?>user/logout" class="list-group-item">Đăng xuất</a>
                  </div>
                <?php }else{?>
                   <div class="list-group">
                     <a href="<?php echo base_url(); ?>tai-khoan/login" class="list-group-item">Đăng nhập</a> <a href="<?php echo base_url(); ?>tai-khoan/register" class="list-group-item">Đăng kí</a> <a href="<?php echo base_url(); ?>tai-khoan/forgotten" class="list-group-item">Quên Mật Khẩu</a>
                  </div>
                <?php }?>
               </div>
            </div>
         </column>
      </aside>
   </div>
</div>
<footer id="footer">
   <?php echo Modules::run('footer'); ?>
</footer>
<?php if($success == 1){ ?>
<script>$(document).ready(function () {
  swal("Tạo tài khoản thành công! Chúc bạn mua sắm vui vẻ. ", "", "success");
});</script>
<?php }else{if($success == 2){?>
<script>$(document).ready(function () {
  swal("Không thể đăng ký! Vui lòng liên hệ với bộ phận kỹ thuật.", "", "error");
});</script>
<?php }}?>
<script>
  var root = '<?php echo base_url(); ?>';
</script>
<?php 
unset($success);
unset($breadcrumb);
?>
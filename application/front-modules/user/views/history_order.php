<h1>Lịch Sử Đơn Hàng</h1>
<div class="table-responsive">
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <td class="text-right">Mã ID đơn hàng</td>
              <td class="text-left">Tình trạng</td>
              <td class="text-left">Ngày tạo</td> 
              <td class="text-left">Khách hàng</td>
              <td class="text-right">Tổng Cộng</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
              <?php  
              foreach($order_history_list['pageList'] as $k=>$v){
                  ?>
                  <tr>
                    <td class="text-right">#<?php echo $v->id; ?></td>
                    <td class="text-left"><?php echo OrderTypeU($v->status); ?></td>
                    <td class="text-left"><?php echo $v->order_date; ?></td> 
                    <td class="text-left"><?php echo $v->mname; ?></td>
                    <td class="text-right"><?php echo number_format($v->total_price); ?>VNĐ</td>
                    <td class="text-right"><a href="<?php echo base_url('tai-khoan/order_detail/'.$v->id); ?>" data-toggle="tooltip" title="" class="btn btn-info" data-original-title="Xem"><i class="fa fa-eye"></i></a></td>
                  </tr> 
                  <?php
              } 
              ?> 
           </tbody>
        </table>
</div>
 
<?php  echo $order_history_list['paging']; ?>
<div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo base_url(); ?>" class="btn btn-outline">Tiếp tục</a></div>
</div>
  
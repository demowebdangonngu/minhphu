<h2>Tài khoản của tôi</h2>
<ul class="list-unstyled">
   <li><a href="<?php echo base_url(); ?>tai-khoan/changeuser">Thay đổi thông tin tài khoản</a></li>
   <li><a href="<?php echo base_url(); ?>tai-khoan/changepass">Thay đổi mật khẩu</a></li>
   <li><a href="<?php echo base_url(); ?>tai-khoan/wishlist">Danh sách yêu thích</a></li>
</ul>
<h2>Đơn hàng của tôi</h2>
<ul class="list-unstyled">
   <li><a href="<?php echo base_url(); ?>tai-khoan/register">Xem lịch sử đơn hàng</a></li>
</ul>
<h2>Thư thông báo</h2>
<ul class="list-unstyled">
   <li><a href="<?php echo base_url(); ?>tai-khoan/newsletter">Đăng kí / hủy đăng kí thông báo</a></li>
</ul>
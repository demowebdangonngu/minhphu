<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";
class user extends YT_ControllerPublic {
 
    public function __construct() {
        parent::__construct();
        $this->model = $this->load->model('user_model');
        $this->load->model('wish/model_wish' , 'WISH' , TRUE);
        $this->load->library('main/main', 'main');

    }
    
    

    function account(){
    	if($this->session->userdata('member_name')){
    		
    		
    		$root = 'http://'.$_SERVER['HTTP_HOST'];
			$this->data['breadcrumb'] = <<<EOF
	<ul class="breadcrumb">
      <li><a href="{$root}">Trang chủ</a></li>
      <li><a href="{$root}/tai-khoan/account">Tài khoản</a></li>
   </ul>
EOF;
			$this->data['page_user'] = $this->load->view('account', $this->data , true); 
	    	$this->template->build('index', $this->data);
	    	
		}else{
			redirect('user/login', 'refresh');
		}
    }

    function newsletter(){
    	if($this->session->userdata('member_name')){
    		if($_POST){
    			//print_r($_POST);exit;
    			$array= array(
	    			'set_email' => $_POST['set_email'],
	    		);
    			$kq = $this->model->edit($this->session->userdata('member_id'),$array);
	    		echo $kq;
    		}else{
    		
    		$root = 'http://'.$_SERVER['HTTP_HOST'];
			$this->data['breadcrumb'] = <<<EOF
	<ul class="breadcrumb">
      <li><a href="{$root}">Trang chủ</a></li>
      <li><a href="{$root}/tai-khoan/newsletter">Đăng ký nhận thông báo</a></li>
   </ul>
EOF;
			$this->data['page_user'] = $this->load->view('newsletter', $this->data , true); 
	    	$this->template->build('index', $this->data);
	    	}
		}else{
			redirect('user/login', 'refresh');
		}
    }
	function random_pass($length = 6){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	function forgotten(){
    	if(!$this->session->userdata('member_name')){
    		if($_POST){
    			// print_r($_POST);exit;
    			$pass_new = $this->random_pass();
				$array = array(
	    			'password' => md5(md5(trim($pass_new))),
	    		);
    			$id = $this->model->edit($_POST['id'],$array);
				$data['id'] = $id;
				$data['pass'] = $pass_new;
	    		echo json_encode($data);
    		}else{
    		
    		$root = 'http://'.$_SERVER['HTTP_HOST'];
			$this->data['breadcrumb'] = <<<EOF
	<ul class="breadcrumb">
      <li><a href="{$root}">Trang chủ</a></li>
      <li><a href="{$root}/tai-khoan/forgotten">Quên mật khẩu</a></li>
   </ul>
EOF;
			$this->data['page_user'] = $this->load->view('forgotten', $this->data , true); 
	    	$this->template->build('index', $this->data);
	    	}
		}else{
			redirect('user/account', 'refresh');
		}
    }
	
    function changeuser(){
    	if($this->session->userdata('member_name')){
	    	if($_POST){
	    		$array= array(
	    			'name' => $_POST['name'],
	    			'email' => $_POST['email'],
	    			'phone' => $_POST['phone'],
	    			'address' => $_POST['address'],
	    		);
	    		$kq = $this->model->edit($this->session->userdata('member_id'),$array);
	    		echo $kq;
	    	}else{
	    		$this->model->from('member');
	    		$this->model->where('id',$this->session->userdata('member_id'));
	    		$this->data['obj'] = current($this->model->get()->result());
	    		$root = 'http://'.$_SERVER['HTTP_HOST'];
			$this->data['breadcrumb'] = <<<EOF
	<ul class="breadcrumb">
      <li><a href="{$root}">Trang chủ</a></li>
      <li><a href="{$root}/tai-khoan/account">Tài khoản</a></li>
      <li><a href="{$root}/tai-khoan/account">Sửa thông tin tài khoản</a></li>
   </ul>
EOF;
				$this->data['page_user'] = $this->load->view('changeuser', $this->data , true); 
		    	$this->template->build('index', $this->data);
	    	}
	    }else{
			redirect('user/login', 'refresh');
		}
    }

    function changepass(){
    	if($this->session->userdata('member_name')){
	    	if($_POST){
	    		$array= array(
	    			'password' => md5(md5(trim($_POST['pass_1']))),
	    		);
	    		$kq = $this->model->edit($this->session->userdata('member_id'),$array);
	    		echo $kq;
	    	}else{
	    		$this->model->from('member');
	    		$this->model->where('id',$this->session->userdata('member_id'));
	    		$this->data['obj'] = current($this->model->get()->result());
	    		$root = 'http://'.$_SERVER['HTTP_HOST'];
			$this->data['breadcrumb'] = <<<EOF
	<ul class="breadcrumb">
      <li><a href="{$root}">Trang chủ</a></li>
      <li><a href="{$root}/tai-khoan/account">Tài khoản</a></li>
      <li><a href="{$root}/tai-khoan/changepass">Sửa mật khẩu</a></li>
   </ul>
EOF;
				$this->data['page_user'] = $this->load->view('changepass', $this->data , true); 
		    	$this->template->build('index', $this->data);
	    	}
	    }else{
			redirect('user/login', 'refresh');
		}
    }

    public function login(){
    	if($_POST){
    		$this->model->from('member');
    		$this->model->where(array('email'=>$_POST['email'],'password'=>md5(md5(trim($_POST['password']))),'status'=>1));
    		$user = current($this->model->get()->result());
    		if($user){
    			$str=$_POST['email'];
    			$userdata = array('user' => $user->email,
                    'member_id' => $user->id,
                    'member_name' => $user->name,
                    'member_address' => $user->address,
                    'last_ip' => $_SERVER['REMOTE_ADDR'],
                    'isLoggedInFront' => TRUE);
                $this->session->set_userdata($userdata);
    			$data[2] = explode("@",$str);
    			$data[1] = 1;
    		}else{
    			$data[1] = 0;
    		}
    		echo json_encode($data);
    	}else{
    		$root = 'http://'.$_SERVER['HTTP_HOST'];
			$this->data['breadcrumb'] = <<<EOF
	<ul class="breadcrumb">
      <li><a href="{$root}">Trang chủ</a></li>
      <li><a href="{$root}/tai-khoan/account">Tài khoản</a></li>
      <li><a href="{$root}/tai-khoan/login">Đăng nhập</a></li>
   </ul>
EOF;
    	$this->data['page_user'] = $this->load->view('login', $this->data , true); 
    	$this->template->build('index', $this->data);
    	}
    }

	public function register()
	{
		if($_POST){
			if ($_SESSION['captcha'] == $this->input->post('captcha' , true)) {
				if($_POST['data']['password'] == $_POST['data']['password_nl']){
					//$salt = substr(md5(rand(0, 9999999)), 0, 5);
		            $pass = trim($_POST['data']['password']);
		            $_POST['data']['password'] = md5(md5(trim($pass)));
					$array = array(
						'name' => $_POST['data']['name'],
						'email' => $_POST['data']['email'],
						'phone' => $_POST['data']['phone'],
						'address' => $_POST['data']['address'],
						'password' => $_POST['data']['password'],
					);

					if($this->model->insert_member($array)){
						$success = 1;
					}else{
						$success = 2;
					}
					echo $success;
				}
			}
		}else{
			$root = 'http://'.$_SERVER['HTTP_HOST'];
			$this->data['breadcrumb'] = <<<EOF
	<ul class="breadcrumb">
      <li><a href="{$root}">Trang chủ</a></li>
      <li><a href="{$root}/tai-khoan/account">Tài khoản</a></li>
      <li><a href="{$root}/tai-khoan/register">Đăng Kí</a></li>
   </ul>
EOF;
			$this->data['page_user'] = $this->load->view('register', $this->data , true); 
    		$this->template->build('index', $this->data);
		}
       
	}

	function check_all(){
		if(!$this->session->userdata('member_name')){
			$mail = $_POST['email'];
			$phone = $_POST['phone'];
			//Resi
			$this->model->from('member');
			$this->model->where(array('email'=>$mail,'phone'=>$phone));
			$id = current($this->model->get()->result());
			//forgotten
			$this->model->from('member');
			$this->model->where(array('email'=>$mail,'phone'=>$phone));
			$forgotten = current($this->model->get()->result());
			if($forgotten){
				$data['forgotten'] = 1;
				$data['id'] = $forgotten->id;
			}else{
				$data['forgotten'] = 0;
			}
			if($id){
				$data[1] = 1;
			}else{
				$data[1] = 0;
			}
			if ($_SESSION['captcha'] == $_POST['captcha']) {
				$data[2] = 1;
			}else{
				$data[2] = 0;
			}
			if($_POST['pass1'] == $_POST['pass2']){
				$data[3] = 1;
			}else{
				$data[3] = 0;
			}
		}else{
			$mail = $this->session->userdata('user');
			$this->model->from('member');
			$this->model->where(array('email'=>$mail));
			$id = current($this->model->get()->result());
			if(md5(md5(trim($_POST['pass_cu']))) == $id->password){
				$data['pass_cu'] = 1;
			}else{
				$data['pass_cu'] = 0;
			}
			if($_POST['pass_1'] == $_POST['pass_2']){
				$data['pass_new'] = 1;
			}else{
				$data['pass_new'] = 0;
			}
			
		}
		
		
		echo json_encode($data);
	}

	public function logout() {
        $userdata = array('user' => '',
            'member_id' => '',
            'member_user' => '',
            'member_name' => '',
            'member_address' => '', 
            'member_email' => '',
            'isLoggedInFront' => FALSE);
        $this->session->unset_userdata($userdata);

        redirect('tai-khoan/login', 'refresh');
    }
    function wishlist(){
    	if($this->session->userdata('member_name')){ 
    		$base_url = 'tai-khoan/wishlist';
    		$data_get = $this->input->get('del' , TRUE);
    		if(intval($data_get) && $data_get > 0){
    			// Delete DS Yeu Thich
    			$whereDeleteWish['id_product'] = $data_get;
    			$whereDeleteWish['id_member'] = $this->session->userdata('member_id');
    			$this->WISH->delete_list_wish($whereDeleteWish);
    			redirect($base_url);
    		}  
    		// breadcumb
	        $breadcumb = array( 
	        	array(
	                'url' => 'tai-khoan/account',
	                'name' => 'Tài khoản'
	            ),
	            array(
	                'url' => $base_url,
	                'name' => 'Danh sách Yêu Thích'
	            )
	        ); 
	        $this->data['breadcrumb'] = $this->main->breadcrumb($breadcumb);  
            $base_url = base_url('tai-khoan/wishlist'); 
            $slug = $this->uri->segment(1).'/'.$this->uri->segment(2); 
            $where['id_member'] = $this->session->userdata('member_id'); 
            $this->data['content_wish_list'] = $this->WISH->get_list_product_pages(PER_PAGE_MAIN,array('uri_segment'=>3,'base'=>$slug),$where);
			$this->data['page_user'] = $this->load->view('wishlist', $this->data , true); 
	    	$this->template->build('index', $this->data); 
		}else{
			redirect('user/login', 'refresh');
		}
    }

    function order(){
    	if($this->session->userdata('member_name')){  
	        $breadcumb = array( 
	        	array(
	                'url' => 'tai-khoan/account',
	                'name' => 'Tài khoản'
	            ),
	            array(
	                'url' => 'tai-khoan/order',
	                'name' => 'Lịch Sử Đơn Hàng'
	            )
	        ); 
	        $this->data['breadcrumb'] = $this->main->breadcrumb($breadcumb);   
            $slug = $this->uri->segment(1).'/'.$this->uri->segment(2); 
            $where['mem_id'] = $this->session->userdata('member_id'); 
            $this->data['order_history_list'] = $this->model->get_list_order_pages(PER_PAGE_MAIN,array('uri_segment'=>3,'base'=>$slug),$where);
			$this->data['page_user'] = $this->load->view('history_order', $this->data , true); 
	    	$this->template->build('index', $this->data); 
		}else{
			redirect('user/login', 'refresh');
		}
    }
    function order_detail(){
    	$idHD = $this->uri->segment(3);
    	if($this->session->userdata('member_name') && intval($idHD)){  
	        $breadcumb = array( 
	        	array(
	                'url' => 'tai-khoan/account',
	                'name' => 'Tài khoản'
	            ),
	            array(
	                'url' => 'tai-khoan/order',
	                'name' => 'Lịch Sử Đơn Hàng'
	            )
	            ,
	            array(
	                'url' => 'tai-khoan/order_detail/'.$idHD,
	                'name' => 'Thông tin đơn hàng'
	            )
	        ); 
	        $this->data['breadcrumb'] = $this->main->breadcrumb($breadcumb);   
    
            $where['mem_id'] = $this->session->userdata('member_id'); 
            $where['order.id'] = $idHD;  
            $this->data['detail'] = $this->model->get_detail_order($where);



            $where = array();
            $where['order_detail.order_id'] = $idHD; 
            $this->data['list_product_detail'] = $this->model->get_detail_order2($where);
 
			$this->data['page_user'] = $this->load->view('history_order_detail', $this->data , true); 
	    	$this->template->build('index', $this->data); 
		}else{
			redirect('user/login', 'refresh');
		}
    }
    
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
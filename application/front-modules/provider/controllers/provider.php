<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";
class provider extends YT_ControllerPublic {
 
    public function __construct() {
        parent::__construct();
        $this->load->model('product/model_product', 'SP' , true);
        $this->load->model('provider/model_provider' , 'PRO' , true);  
        $this->load->library('main/main', 'main');
    }
  
    public function detail($slug = '',$pro_id = 0, $page = 1) { 
        if (!is_numeric($page)) {
            $page = 1;
        }
        if ($slug == '' || $pro_id == 0) {
            redirect();
        }   
    
        $this->PRO->where(array('status'=>1,'id'=>$pro_id));
        $provider_detail = current($this->PRO->get()->result());
 
        $base_url = $slug; 
        $slug = $this->uri->segment(2); 
        $this->data['product']=$this->SP->get_product_provider_pages(PER_PAGE_MAIN,array('uri_segment'=>3,'base'=>$slug),$pro_id); 
        // breadcumb
        $breadcumb = array( 
            array(
                'url' => $base_url,
                'name' => $provider_detail->name
            )
        ); 
        $this->data['title'] = $provider_detail->name;  
        $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);  

        $this->data['SEO']->build_meta($provider_detail,'provider');
        //echo '<pre>'; print_r($this->data['product']); exit;
        $this->data['data_sanpham'] = $this->load->view('data_sanpham' , $this->data , TRUE); 
        $this->template->build('index_sanpham', $this->data);  







    }
 
    
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
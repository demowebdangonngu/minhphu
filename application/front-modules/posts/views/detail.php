<div class="container">
   <?php  echo $breadcumb; ?>
   <div class="row">
      <aside id="sidebar-left" class="col-md-3"> 
         <column id="column-left" class="hidden-xs sidebar">
            <?php echo Modules::run('left/menuleft'); ?> 
            <?php echo Modules::run('left/tintuc_noibat'); ?> 
         </column> 
      </aside>
      <section class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
           <div id="content">
              <section id="blog-info" class="pav-blog wrapper clearfix clearfix blog-wrapper clearfix clearfix">
                 <figure class="image">
                    <?php
                    if($tintuc[0]->image != ""){
                      ?><img src="<?php echo $this->config->item("img_path") . $tintuc[0]->image;?>" title="<?php echo $tintuc[0]->name; ?>" alt="<?php echo $tintuc[0]->name; ?>" class="img-responsive"><?php
                    }
                    ?>
                    
                 </figure>
                 <header>
                    <h1 class="blog-title"><?php echo $tintuc[0]->name; ?></h1>
                 </header>
                 <section class="content">
                    <section class="blog-meta">
                       <span class="author">
                       <span><i class="fa fa-pencil"></i>Viết bởi: </span>
                       <span class="t-color"><?php echo $tintuc[0]->admin_name; ?></span>
                       </span>
                       <span class="publishin">
                       <span><i class="fa fa-thumb-tack"></i>Trong danh mục: </span>
                       <a  class="t-color" title="<?php echo $tintuc[0]->cat_name; ?>"><?php echo $tintuc[0]->cat_name; ?></a>
                       </span>
                       <span class="created"><span><i class="fa fa-calendar"></i>Ngày viết:  <?php $public_date = explode(' ',$tintuc[0]->public_date); echo $public_date[0]; ?></span></span>
                       <span class="hits">
                       <!--
                       <span><i class="fa fa-insert-template"></i>Số lần xem: </span>
                       <span class="t-color"><?php echo $tintuc[0]->views?$tintuc[0]->views:0; ?></span>
                       -->
                       </span> 
                    </section>
                    <section class="description">
                       <p><?php echo $tintuc[0]->description; ?></p>
                    </section>
                    <section class="blog-content">
                       <div class="content-wrap clearfix">
                          <?php echo $tintuc[0]->content; ?>
                       </div>
                    </section>
                 </section>
                 <footer> 
                    <section class="blog-social clearfix">
                       <div class="social-wrap">
                          <div class="social-heading"><b>Like This:</b></div>
                  			<div class="fb-like" data-href="<?php echo current_url(); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
	                   
                       </div>
                    </section>
                    <section class="blog-bottom row">
                       <div class="col-lg-6 col-sm-6 col-xs-12">
                          <h4>Bài viết liên quan</h4>
                          <ul class="sample-blog-category">
                             <?php  
                             foreach($related as $k=>$v){
                                  echo '<li><a href="'.$SEO->build_link($v,"posts").'">'.$v->title.'</a></li>';
                             }
                             ?> 
                          </ul>
                       </div>
                    </section> 
                 </footer>
              </section>
           </div>
           <!-- End Div Content -->
        </section>
   </div>
</div>
<footer id="footer">
    <?php echo Modules::run('footer'); ?>
</footer> 
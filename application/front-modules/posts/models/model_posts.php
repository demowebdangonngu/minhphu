<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/Model.php";
class Model_posts extends YT_Model {
	public function __construct(){  
		parent::__construct();
                $this->table_name= "posts";   
	} 
    public function getList($where = false , $limit = array('per_page' => 8, 'start' => 0) , $like = false , $strIn = false , $orderDate = false , $where_in_id = false){
		$this->getDB()->select("posts.* , categories.name as cat_name, categories.slug as cat_slug ");
		$this->getDB()->from($this->table_name); 
        $this->getDB()->join('categories' , 'categories.id = posts.cat_id' , 'left'); 
        $this->getDB()->where("posts.status = 1");   
        if($where)
            $this->getDB()->where($where);   
        if($like)
            $this->getDB()->like($like);   
             
        if($where_in_id){
            $this->getDB()->where_in('posts.id', $where_in_id);
        }
          
        if($orderDate){
            $this->getDB()->order_by("public_date", "DESC");
        } 
        if($limit)
            $this->getDB()->limit($limit['per_page'], $limit['start']); 
		$result  = $this->getDB()->get()->result();  
       
        return $result;
	}
    function get_posts_pages($limit=10,$option=array(),$cat_id=array(),$not_id=array()){
        $return=array();
        $condition=array('posts.status'=>1,'categories.status'=>1);
		$this->getDB()->select("posts.*,categories.name as cat_name, categories.id as cat_id ,users.name as admin_name, categories.slug as cat_slug");
        $this->getDB()->from($this->table_name);
        $this->getDB()->join ("users", "users.id=posts.admin_id" , 'LEFT');
       if($cat_id){
           $this->getDB()->join ("categories", "categories.id=posts.cat_id" , 'LEFT');
           $this->getDB()->where_in('posts.cat_id',$cat_id);
       }
        
        $this->getDB()->where($condition);
        
        $pagination=$this->load->library("Pagination");
        $result['total']=$config['total_rows'] = $this->getDB()->count_all_results();;
		$config['per_page'] = $limit;
         $option['base']?$option['base']=trim($option['base'],"/")."/":"latest";
        $config['base_url']=base_url().$option['base'];
        $config['use_page_numbers']=true;
        $config['uri_segment']=$option['uri_segment']?$option['uri_segment']:2;
        $pagination->initialize($config);
        $result['paging']=$pagination->create_links();
        
        
        $this->getDB()->select("posts.*,categories.name as cat_name, categories.id as cat_id ,users.name as admin_name, categories.slug as cat_slug");
        $this->getDB()->from($this->table_name);
        $this->getDB()->join ("users", "users.id=posts.admin_id" , 'LEFT');
        if($cat_id){
           $this->getDB()->join ("categories", "categories.id=posts.cat_id" , 'LEFT');
           $this->getDB()->where_in('posts.cat_id',$cat_id);
        }
        $this->getDB()->where($condition);
        $this->getDB()->order_by("posts.public_date","DESC");
		// if($not_id)
		// $this->getDB()->where_not_in('id',$not_id);
        $this->getDB()->limit($limit,$pagination->cur_page>1?($pagination->cur_page-1)*$limit:0);
        $result['cur_page']=$pagination->cur_page;
        $plist=$this->getDB()->get()->result();
        $result['pageList']=array();
        $pids=array();
        foreach ($plist as $p){
            $result['pageList'][$p->id]=$p;
             $pids[]=$p->id;
        }
		// print_r($result);exit; 
        return $result;
        
    }
     
	 
}
?>
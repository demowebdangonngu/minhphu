<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";

class product extends YT_ControllerPublic {
 
    public function __construct() {
 
        parent::__construct();
        $this->model = $this->load->model('Model_product');
        $this->load->library('main/main');

        
    }
	public function index()
	{ 
           $this->load->view('product_layout'); 
           //$this->template->build('welcome_message'); 
	}
    


    public function detail($id) {  
        $this->model->where(array('id' => $id));
        $obj = current($this->model->get()->result()); 
        if ($obj->id) { 
            $id = $obj->id;
            $this->data['obj'] = $obj; 
            $this->data['SEO']->build_meta($obj, "sanpham"); 
            $this->model->select("product.*,categories.name as cat_name, categories.id as cat_id ,users.name as admin_name,categories.slug as cat_slug");
            $this->model->join("users", "users.id=product.admin_id" , 'left');
            $this->model->join("categories", "categories.id=product.cat_id" , 'left'); 
            $this->model->where(array("product.id"=>$id));
            $this->data['sanpham'] = $this->model->get()->result();  
            $this->model->limit(8 , 0);
            $this->model->where(array("cat_id"=>$this->data['sanpham'][0]->cat_id));
			$this->model->order_by('public_date','DESC');
            $this->data['related'] = $this->model->get()->result();  
            $this->data['rating'] = $this->model->getRatingSP($id);  
            $breadcumb = array(  
                array(
                    'url' => 'loai/'.$this->data['sanpham'][0]->cat_slug,
                    'name' => $this->data['sanpham'][0]->cat_name
                ),
                array(
                    'url' => $this->data['SEO']->build_link($this->data['sanpham'][0],"sanpham",false , true),
                    'name' => $this->data['sanpham'][0]->name
                )
            );  
            $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);  
            $this->template->build('detail', $this->data);
        }else{
            redirect(base_url());
        }
    }
    
    public function product_hot_home(){
        $this->data['product_hot_home'] = $this->model->getList(array('highlights'=>1) , array('per_page' => 4, 'start' => 0) , false, false , true);
        $this->load->view('product_hot_home' , $this->data); 
    }



    public function productdg(){
        $data_post = $this->input->post(); 
        if($data_post['ajax'] != 1){
            redirect(base_url()); exit;
        } 
        if(!$this->session->userdata('member_name')){
            exit;
        } 
        if($data_post['typeDg'] == 1){
            $dataUpdate['point'] = 1;
        }else{
            $dataUpdate['point'] = -1;
        } 
        $where['product_id'] = $data_post['product_id'];
        $where['mem_id'] = $this->session->userdata('member_id'); 
        $point = $this->model->getPoint_DanhGiaSP($where);
        if($point){  // update
            if($dataUpdate['point'] != $point){
                $this->model->updateDanhGiaSP($where , $dataUpdate);
                echo 'Update';
            } 
        }else{  // Insert
            $dataInsert['product_id'] = $data_post['product_id'];
            $dataInsert['mem_id'] = $this->session->userdata('member_id');
            $dataInsert['point'] = $dataUpdate['point'];
            $this->model->insertDanhGiaSP($dataInsert); 
            echo 'Insert';
        }
        exit; 
    }
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
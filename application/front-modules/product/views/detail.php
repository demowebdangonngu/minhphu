<div class="container">
   <?php  echo $breadcumb; ?>
   <div class="row">
      <aside id="sidebar-left" class="col-md-3">
         <column id="column-left" class="hidden-xs sidebar">
            
            <?php echo Modules::run('left/menuleft'); ?> 
             
            <?php echo Modules::run('left/sanpham_noibat'); ?> 
         </column>
      </aside>
      <section id="sidebar-main" class="col-md-9">
         <div id="contents" class="wrappers clearfix">
            <div class="product-info">
               <div class="row">
                  <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 image-container">
                     <div class="image">
                        <span class="product-label exist">
                        <span class="product-label-special">Sale</span>
                        </span>
                        <a href="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>" title="<?php echo $sanpham[0]->name; ?>" class="imagezoom">
                        <img itemprop="image" src="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>" title="<?php echo $sanpham[0]->name; ?>" alt="<?php echo $sanpham[0]->name; ?>" id="image" data-zoom-image="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>" class="product-image-zoom img-responsive">
                        </a>
                     </div>
                     <div class="thumbs-preview">
                        <div class="image-additional slide carousel " id="image-additional">
                           <div id="image-additional-carousel" class="carousel-inner">
                              <div class="item clearfix active">
                                 <a href="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>" title="<?php echo $sanpham[0]->name; ?>" class="imagezoom" data-zoom-image="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>" data-image="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>">
                                 <img src="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>" style="max-width:75px" title="<?php echo $sanpham[0]->name; ?>" alt="<?php echo $sanpham[0]->name; ?>" data-zoom-image="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>" class="product-image-zoom img-responsive">
                                 </a>
                                 <a href="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>" title="<?php echo $sanpham[0]->name; ?>" class="imagezoom" data-zoom-image="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>" data-image="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>">
                                 <img src="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>" style="max-width:75px" title="<?php echo $sanpham[0]->name; ?>" alt="<?php echo $sanpham[0]->name; ?>" data-zoom-image="<?php echo $this->config->item("img_path") . $sanpham[0]->image;?>" class="product-image-zoom img-responsive">
                                 </a>
                              </div>
                           </div>
                           <!-- Controls -->
                        </div>
                        <script type="text/javascript">
                           $('#image-additional .item:first').addClass('active');
                           $('#image-additional').carousel({interval:false})
                        </script>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 product-view">
                     <h1 class="name"><?php echo $sanpham[0]->name; ?></h1>
                     <div class="rating">
                        <p>
                           <?php 
                              for($i = 1 ; $i <= 5 ; $i++){
                                 $class = 'fa-star-o';
                                 if($i<=$rating){
                                    $class = 'fa-star';
                                 }
                                 ?><span class="fa fa-stack"><i class="fa <?php echo $class; ?> fa-stack-1x"></i></span><?php
                              }
                           ?> 
                           <a href="#review-form" class="popup-with-form" onclick="dgProduct('<?php echo $sanpham[0]->id; ?>','1'); $('a[href=\'#tab-review\']').trigger('click');  return false;">Sản phẩm tốt</a> / <a href="#review-form" class="popup-with-form" onclick=" dgProduct('<?php echo $sanpham[0]->id; ?>','0'); $('a[href=\'#tab-review\']').trigger('click');   return false;">Cần cải thiện</a>
                        </p>
                        <hr>
                     </div>
                     <div class="product-extra">
                        <ul class="list-unstyled description no-padding">
                           <li><i class="fa fa-chevron-down"></i><b>Mã sản phẩm:</b> <?php echo $sanpham[0]->code; ?></li>   
                           <li><i class="fa fa-chevron-down"></i><b>Tình trạng:</b> <?php echo "Còn hàng"; ?></li>
                        </ul>
                     </div>
                     <div class="price">
                        <ul class="list-unstyled">
                           <li>
                              <?php
                              
                              if($sanpham[0]->price >= $sanpham[0]->price_sale){
								  if($sanpham[0]->price == 0){?>
								  <span class="price-new">
                                    Liên hệ</span>
									<?php 
									}else{
									if($sanpham[0]->price_sale == 0){ 
                                    ?>
									<span class="price-new">
										<?php echo number_format($sanpham[0]->price).'VNÐ'; $gia = number_format($sanpham[0]->price);?>
									</span>
									<?php }else{?>
										<span class="price-new">
										<?php echo number_format($sanpham[0]->price_sale).'VNÐ'; $gia = number_format($sanpham[0]->price_sale);?></span>
										<span style="text-decoration: line-through;">
										<?php echo number_format($sanpham[0]->price).'VNÐ' ;?></span>
                                    <?php }}
                                }else{ 
                                    ?>
                                    <span class="price-new">
                                    <?php echo number_format($sanpham[0]->price).'VNÐ';  $gia = number_format($sanpham[0]->price);?></span> 
                                    <?php
                                }  
                              ?> 
                           </li>
                           <?php 
                           if($sanpham[0]->isVAT == 1){
                                echo '<li class="other-price price-tax">Giá chưa có VAT: '.$gia.'VNĐ</li>';
                           } 
                           ?>
                           
                        </ul>
                     </div>
                     <div id="product">
                        <div class="product-extra">  
                           <div class="action pull-left">
                              <div class="cart">
                                 <button onclick="add_cart_item('<?php echo $sanpham[0]->id; ?>' , 1);" type="button" id="button-cart" data-loading-text="Loading..." class="btn btn-inverse btn-variant">
                                 <span>Mua hàng</span>
                                 </button>
                              </div>
                              <div class="wishlist">
                                 <button type="button" data-toggle="tooltip" class="wishlist btn btn-outline-inverse btn-variant" title="" onclick="add_wish_list('<?php echo $sanpham[0]->id; ?>');" data-original-title="Thêm vào DS yêu thích">
                                 <i class="fa fa-heart"></i>
                                 <span>Thêm vào DS yêu thích</span>
                                 </button>
                              </div>
                              <!--
                              <div class="compare">
                                 <button type="button" data-toggle="tooltip" class="compare btn btn-outline-inverse btn-variant" title="" onclick="add_compare('<?php echo $sanpham[0]->id; ?>');" data-original-title="So sánh sản phẩm">
                                 <i class="fa fa-refresh"></i>
                                 <span>So sánh sản phẩm</span>
                                 </button>
                              </div>-->
                           </div>
                        </div>
                     </div>
                     <hr>
                     <!-- AddThis Button BEGIN -->
                     <div class="addthis_toolbox addthis_default_style">
                        <div class="fb-like" data-href="<?php echo current_url(); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                     </div>
                    
                     <!-- AddThis Button END -->
                  </div>
               </div>
            </div>
            <div class="tabs-group box">
               <ul class="nav nav-tabs clearfix">
                  <li class="active"><a href="#tab-description" data-toggle="tab">Mô tả</a></li>
                  <li><a href="#tab-review" data-toggle="tab" style="display: none;">Bình luận</a></li>
                  <li><a href="#spec-review" data-toggle="tab">Thông số kỹ thuật</a></li>
               </ul>
               <div class="tab-content">
               
                  <div class="tab-pane active" id="tab-description" style="  color: black;">
                     	<?php echo $sanpham[0]->content; ?>
                  </div>
                  <div class="tab-pane" id="tab-review" >
                  <!--
                     <div id="review">
                        <p>Không có đánh giá cho sản phẩm này.</p>
                     </div>
                     <p> <a href="#review-form" class="popup-with-form btn btn-sm btn-inverse" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">Viết đánh giá</a></p>
              		-->



                     <div class="hide">
                        <div id="review-form" class="panel">
                           <div class="panel-body"  style="  text-align: center;">
                              <form class="form-horizontal ">
                                 <h2 id='msg_danhgia'>Cảm ơn đánh giá của bạn</h2>
                                 <!--
                                 <div class="form-group required">
                                    <div class="col-sm-12">
                                       <label class="control-label" for="input-name">Tên bạn:</label>
                                       <input type="text" name="name" value="" id="input-name" class="form-control">
                                    </div>
                                 </div>
                                 <div class="form-group required">
                                    <div class="col-sm-12">
                                       <label class="control-label" for="input-review">Đánh giá của bạn:</label>
                                       <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>
                                       <div class="help-block"><span style="color: #FF0000;">Lưu ý:</span> không hỗ trợ HTML!</div>
                                    </div>
                                 </div>
                                 <div class="form-group required">
                                    <div class="col-sm-12">
                                       <label class="control-label">Bình chọn:</label>
                                       &nbsp;&nbsp;&nbsp; Xấu&nbsp;
                                       <input type="radio" name="rating" value="1">
                                       &nbsp;
                                       <input type="radio" name="rating" value="2">
                                       &nbsp;
                                       <input type="radio" name="rating" value="3">
                                       &nbsp;
                                       <input type="radio" name="rating" value="4">
                                       &nbsp;
                                       <input type="radio" name="rating" value="5">
                                       &nbsp;Tốt
                                    </div>
                                 </div>
                                 <div class="form-group required">
                                    <div class="col-sm-12">
                                       <label class="control-label" for="input-captcha">Nhập mã kiểm tra vào ô bên dưới:</label>
                                       <input type="text" name="captcha" value="" id="input-captcha" class="form-control">
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-sm-12"> <img src="index.php?route=tool/captcha" alt="" id="captcha"> </div>
                                 </div>
                                 <div class="buttons">
                                    <div class="pull-right">
                                       <button type="button" id="button-review" data-loading-text="Loading..." class="btn btn-outline">Tiếp tục</button>
                                    </div>
                                 </div>
                                 -->
                              </form>
                           </div>
                        </div>
                     </div> 
                  </div>
                  <div class="tab-pane" id="spec-review" style="color: black;">
                  	<?php echo $sanpham[0]->specification; ?>
                  </div>
               </div>
               	<?php 
				$tags=$this->load->model('tags/tags_model');
				$list_tags=$tags->get_tag_by_object($obj->id, $module = 'product');
				if(count($list_tags)){
			  	?>
               	<div class="clearfix post-footer" style="background: rgb(241, 241, 241);margin-bottom: 10px;border: 1px solid #e1e2e4;">
					<div class="entry-tags" style="padding: 3px;"> 
						<img width="20px" style="VERTICAL-ALIGN: middle;" src="<?php echo THEME_FRONT; ?>/image/Tags-icon.png">
						<?php foreach($list_tags as $t){?>
						<a style="padding: 10px;" target="_blank" href="<?php echo $this->config->item('base_url') ?>tim-kiem/<?php echo $t->slug;?>" title="<?php echo $t->name;?>"><?php echo $t->name;?></a> 
						<?php }?>
					</div>
				</div>
				<?php }?>
            </div> 

            <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-numposts="5"></div>

            <div class="box featured">
               <div class="box-heading">
                  <span>Sản phẩm liên quan</span>
                  <em class="line"></em>
               </div>
               <div class="box-content">
                  <div class="row products-row">
                     <?php
                     foreach($related as $k=>$v){
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 product-col">
                        <?php echo $this->main->tooltip($v); ?>
                         </div>
                        <?php
                     } 
                     ?> 
                  </div>
               </div>
            </div>
         </div>
      </section>
   </div>
</div>
<footer id="footer">
    <?php echo Modules::run('footer'); ?>
</footer> 
<script>
	var MEM_ID = "<?php echo @$this->session->userdata('member_id'); ?>";
	var MSGDG  = 'Bạn cần đăng nhập trước !!!';
</script>
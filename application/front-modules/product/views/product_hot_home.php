<div class="box producttabs box-center box-center box-variant ">
    <ul class="nav nav-pills box-heading" id="producttabs1017883159">
        <li class="active"><a href="#tab-special1017883159" data-toggle="tab">Sản phẩm HOT</a></li>
    </ul>
    <div class="tab-content box-content">
        <div class="tab-pane box-products  tabcarousel1017883159 slide active" id="tab-special1017883159">
            <div class="carousel-inner products-block">
                <div class="item active products-block">
                    <div class="row products-row last"> 
                        <?php
                        foreach($product_hot_home as $k=>$v){
                            ?>
                            <div class="col-lg-3 col-sm-3 col-xs-12  product-col">
                                <?php
                                echo $this->main->tooltip($v);
                                ?> 
                            </div>
                            <?php    
                        }
                        ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
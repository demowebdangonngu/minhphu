<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/Model.php";
class Model_product extends YT_Model {
	public function __construct(){  
		parent::__construct();
                $this->table_name= "product";   
	} 
    public function getList($where = false , $limit = array('per_page' => 8, 'start' => 0) , $like = false , $strIn = false , $orderDate = false , $where_in_id = false){
		$this->getDB()->select("product.* , categories.name as cat_name , provider.name as pro_name ");
		$this->getDB()->from($this->table_name); 
        $this->getDB()->join('categories' , 'categories.id = product.cat_id' , 'left');
        $this->getDB()->join('provider' , 'provider.id = product.pro_id' , 'left');
        $this->getDB()->where("product.status = 1");   
        if($where)
            $this->getDB()->where($where);   
        if($like)
            $this->getDB()->like($like);   
             
        if($where_in_id){
            $this->getDB()->where_in('product.id', $where_in_id);
        }
         
        if($strIn){
            $this->getDB()->where($strIn);
           	$this->getDB()->where("price > price_sale"); 
            $this->getDB()->order_by("price_sale", "DESC");
        }  
        if($orderDate){
            $this->getDB()->order_by("public_date", "DESC");
        } 
        if($limit)
            $this->getDB()->limit($limit['per_page'], $limit['start']); 
		$result  = $this->getDB()->get()->result();  
       
        return $result;
	}
    
    function get_product_pages($limit=10,$option=array(),$cat_id=array(),$not_id=array() , $where = false){
        $return=array();
        $condition=array('product.status'=>1,'categories.status'=>1);
		$this->getDB()->select("product.*,categories.name as cat_name, categories.id as cat_id ");
        $this->getDB()->from($this->table_name);
        $this->getDB()->join ("categories", "categories.id=product.cat_id");
         
        if($where){ 
           $this->getDB()->where($where);
        }
        if($cat_id){ 
           $this->getDB()->where_in('product.cat_id',$cat_id);
        }
        
        $this->getDB()->where($condition);
        
        $pagination=$this->load->library("Pagination");
        $result['total']=$config['total_rows'] = $this->getDB()->count_all_results();;
		$config['per_page'] = $limit;
         $option['base']?$option['base']=trim($option['base'],"/")."/":"latest";
        $config['base_url']=base_url().$option['base'];
        $config['use_page_numbers']=true;
        $config['uri_segment']=$option['uri_segment']?$option['uri_segment']:2;
        $pagination->initialize($config);
        $result['paging']=$pagination->create_links();
        
        
        $this->getDB()->select("product.*,categories.name as cat_name, categories.id as cat_id ");
        $this->getDB()->from($this->table_name);
        $this->getDB()->join ("categories", "categories.id=product.cat_id");
        if($where){ 
           $this->getDB()->where($where);
        }
        if($cat_id){ 
           $this->getDB()->where_in('product.cat_id',$cat_id);
        }
        $this->getDB()->where($condition);
        $this->getDB()->order_by("product.public_date","DESC");
		// if($not_id)
		// $this->getDB()->where_not_in('id',$not_id);
        $this->getDB()->limit($limit,$pagination->cur_page>1?($pagination->cur_page-1)*$limit:0);
        $result['cur_page']=$pagination->cur_page;
        $plist=$this->getDB()->get()->result();
        $result['pageList']=array();
        $pids=array();
        foreach ($plist as $p){
            $result['pageList'][$p->id]=$p;
             $pids[]=$p->id;
        }
		// print_r($result);exit;

        return $result;
        
    }
    function get_product_provider_pages($limit=10,$option=array(),$pro_id=array()){
        $return=array();
        $condition=array('product.status'=>1,'provider.status'=>1,'product.pro_id'=>$pro_id);
        $this->getDB()->select("product.*,provider.name as pro_name, provider.id as pro_id ");
        $this->getDB()->from($this->table_name);
        $this->getDB()->join ("provider", "provider.id=product.pro_id"); 
        $this->getDB()->where($condition);
        
        $pagination=$this->load->library("Pagination");
        $result['total']=$config['total_rows'] = $this->getDB()->count_all_results();;
        $config['per_page'] = $limit;
         $option['base']?$option['base']=trim($option['base'],"/")."/":"latest";
        $config['base_url']=base_url().$option['base'];
        $config['use_page_numbers']=true;
        $config['uri_segment']=$option['uri_segment']?$option['uri_segment']:2;
        $pagination->initialize($config);
        $result['paging']=$pagination->create_links();
        
        
        $this->getDB()->select("product.*,provider.name as pro_name, provider.id as pro_id ");
        $this->getDB()->from($this->table_name);
        $this->getDB()->join ("provider", "provider.id=product.pro_id"); 
        $this->getDB()->where($condition);
        $this->getDB()->order_by("product.public_date","DESC");
        // if($not_id)
        // $this->getDB()->where_not_in('id',$not_id);
        $this->getDB()->limit($limit,$pagination->cur_page>1?($pagination->cur_page-1)*$limit:0);
        $result['cur_page']=$pagination->cur_page;
        $plist=$this->getDB()->get()->result();
        $result['pageList']=array();
        $pids=array();
        foreach ($plist as $p){
            $result['pageList'][$p->id]=$p;
             $pids[]=$p->id;
        }
        // print_r($result);exit;

        return $result;
        
    }

    
     
    
	public function list_sale($limit = 8){
		$this->getDB()->from($this->table_name); 
		$this->getDB()->where("status = 1 AND price > price_sale");
		$this->getDB()->order_by("price_sale", "DESC");
		$this->getDB()->limit($limit);
		$result  = $this->getDB()->get()->result();
        return $result;
	}
	
    public function getList_sale($limit = array('per_page' => 3, 'start' => 0)){
		$this->getDB()->select("*");
		$this->getDB()->from($this->table_name); 
        $this->getDB()->where("status = 1 AND price > price_sale");  
        $this->getDB()->order_by('RAND()');
  
        if($limit)
            $this->getDB()->limit($limit['per_page'], $limit['start']);
		$result  = $this->getDB()->get()->result();
        return $result;
	}
    
    public function getList_order($limit = array('per_page' => 4, 'start' => 0)){
 
        return $result;
	}
    
    
    public function getList_image($where,  $limit = array('per_page' => 4, 'start' => 0) ){
		$this->getDB()->select("location");
		$this->getDB()->from('attach');  
        $this->getDB()->where($where);
        $this->getDB()->where("status = 1 AND idAdmin != 0 AND nameModel ='sanpham'");   
        if($limit)
            $this->getDB()->limit($limit['per_page'], $limit['start']);
		$result  = $this->getDB()->get()->result(); 
        return $result;
	}


    public function getRatingSP($product_id){
        $this->getDB()->select("count(*) as jcount");
        $this->getDB()->from('judge');  
        $this->getDB()->where('product_id' , $product_id);   
        $result  = $this->getDB()->get()->result();
        $totalDG  = $result[0]->jcount; 

        $this->getDB()->select("count(*) as jcount");
        $this->getDB()->from('judge');  
        $this->getDB()->where('product_id' , $product_id);   
        $this->getDB()->where('point' , 1); 
        $result  = $this->getDB()->get()->result();
        $likeDG  = $result[0]->jcount;   
        return ceil(($likeDG/$totalDG)*5);
    }
    public function getPoint_DanhGiaSP($where){
        $this->getDB()->select("point");
        $this->getDB()->from('judge');  
        $this->getDB()->where($where);   
        $result  = $this->getDB()->get()->result(); 
        return $result[0]->point;
    }
    public function insertDanhGiaSP($data){
        $this->getDB()->insert('judge' , $data);
    }
	public function updateDanhGiaSP($where , $data){
        $this->getDB()->where($where);
        $this->getDB()->update('judge' , $data);
    }
}
?>
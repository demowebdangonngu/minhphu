<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";
class menu extends YT_ControllerPublic { 
 
    public function __construct() {
        parent::__construct(); 
        $this->model=$this->load->model('model_menu');
    }
	public function index()
	{
            
			$this->data['menu'] = $this->model->get_tree(); 
           $this->load->view(__CLASS__, $this->data); 
           //$this->template->build('welcome_message');
           
           
	}
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

                  <section id="pav-masshead" class="pav-masshead aligned-right">
                     <div class="container">
                     <div class="inner row clearfix">
                     <div id="pav-mainnav">
                     <div class="container">
                     <div class="pav-megamenu">
                        <div class="navbar navbar-mega" role="navigation">
                           <div id="mainmenutop" class="megamenu" role="navigation">
                              <div class="show-mobile hidden-lg hidden-md">
                                 <div class="quick-access pull-left">
                                    <button data-toggle="offcanvas" class="btn btn-outline visible-xs visible-sm" type="button"><span class="fa fa-bars"></span></button>
                                 </div>
                              </div>
                              <div class="collapse navbar-collapse" id="bs-megamenu">
                                 <ul class="nav navbar-nav megamenu">
                                    <li class="home">
                                       <a href="<?php echo base_url(); ?>"><span class="menu-title">TRANG CHỦ</span></a>
                                    </li>
                                     <?php
                              
                                     foreach($menu['top'] as $k=>$v){
                                       if(!empty($v->children)){
                                        ?>
                                       <li class="parent dropdown " >
                                       <a class="dropdown-toggle" data-toggle="dropdown" target="<?php echo $v->target; ?>" href="<?php echo $v->link; ?>"><span class="menu-title"><?php echo $v->cname; ?> </span><span class="menu-desc"><?php echo $v->cname; ?> </span><b class="caret"></b></a>
                                       <div class="dropdown-menu level1"  >
                                          <div class="dropdown-menu-inner">
                                             <div class="row">
                                                <div class="col-sm-12 mega-col" data-colwidth="12" data-type="menu" >
                                                   <div class="mega-col-inner">
                                                      <ul>
                                                      <?php
                                                      foreach($v->children as $k1=>$v1){ 
                                                      ?>
                                                         <li class=""><a target="<?php echo $v1->target; ?>" href="<?php echo $v1->link; ?>" ><span class="menu-title"><?php echo $v1->cname; ?></span></a></li>
                                                      <?php }?>
                                                      </ul>
                                                   </div>
                                                </div>
                                             </div>
                                       </li>
                                        <?php }else{?>
                                       <li class="">
                                          <a target="<?php echo $v->target; ?>" href="<?php echo $v->link; ?>"><span class="menu-title"><?php echo $v->name; ?></span></a>
                                       </li>
                                        <?php }}?>
                                 </ul>
                                 </div>
                                 </div>
                                 </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
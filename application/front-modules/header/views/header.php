
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
 
<?php 
$SEO = $this->load->library("SEO");
$SEO->init_meta_site();
$page_title = ($SEO->mtitle ? ($SEO->mtitle . " |") : "") . "  Camerano1";
?>
<title><?php echo $page_title; ?></title>
<meta name="title" content="<?php echo $page_title; ?>" />
<meta name="description" content="<?php echo htmlspecialchars($SEO->mdescription); ?>" />
<?php if ($SEO->mkeyword) { ?>
    <meta name="keyword" content="<?php echo htmlspecialchars($SEO->mkeyword); ?>" />
<?php }else{
	?>
	<meta name="keyword" content="<?php echo NAME_SITE; ?>" />
	<?php
} ?>
<meta name="distribution" content="Global" />  
<meta http-equiv="audience" content="General" />
<meta name="RATING" content="GENERAL" />

<meta property="fb:app_id" content="<?php echo FB_ID; ?>"/> 
<meta property="fb:admins" content="<?php echo FB_ID_ADMIN; ?>"/> 
<meta property="og:url" content="<?php echo current_url(); ?>" /> 
<meta property="og:title" content="<?php echo (htmlspecialchars(str_replace("\"", "", $SEO->ogtitle ? $SEO->ogtitle : $SEO->mtitle))=='')?NAME_SITE:(htmlspecialchars(str_replace("\"", "", $SEO->ogtitle ? $SEO->ogtitle : $SEO->mtitle))); ?>" />
<meta property="og:type" content="website"/> 
<meta property="og:site_name" content="<?php echo NAME_SITE; ?>" />
<meta property="og:description" content="<?php echo htmlspecialchars($SEO->obdescription ? $SEO->obdescription : NAME_SITE); ?>" />
<meta property="og:image" content="<?php echo $SEO->ogimage ? $this->config->item('img_path') .$SEO->ogimage : THEME_FRONT.'catalog/view/theme/megashop/image/logo.png'; ?>" />
 
<meta property="og:image:type" content="image/jpg" /> 
<meta property="og:image:width" content="380" /> 
<meta property="og:image:height" content="285" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link href="<?php echo THEME_FRONT; ?>image/catalog/icon_camera.png" rel="icon" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/stylesheet/bootstrap.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/stylesheet/stylesheet.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/stylesheet/font.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/javascript/jquery/magnific/magnific-popup.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/stylesheet/homebuilder.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/default/stylesheet/pavverticalmenu/style.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/stylesheet/sliderlayer/css/typo.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/stylesheet/pavproducttabs.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/stylesheet/pavverticalcategorytabs.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/stylesheet/bootstrap-tabs-x.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/stylesheet/pavcategorytabs.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/stylesheet/pavcarousel.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/stylesheet/pavblog.css" rel="stylesheet" />
<link href="<?php echo THEME_FRONT; ?>css/jquery.bxslider.css" rel="stylesheet" />


<script type="text/javascript" src="<?php echo THEME_FRONT; ?>catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php echo THEME_FRONT; ?>catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>
<script type="text/javascript" src="<?php echo THEME_FRONT; ?>catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo THEME_FRONT; ?>catalog/view/javascript/common.js"></script>
<script type="text/javascript" src="<?php echo THEME_FRONT; ?>catalog/view/theme/megashop/javascript/common.js"></script>
<script type="text/javascript" src="<?php echo THEME_FRONT; ?>catalog/view/javascript/layerslider/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo THEME_FRONT; ?>catalog/view/javascript/layerslider/jquery.themepunch.revolution.min.js"></script>
<link href="<?php echo $this->config->item('static_path') ?>themes/sweetalert-master/lib/sweet-alert.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $this->config->item('static_path') ?>themes/sweetalert-master/lib/sweet-alert.js"></script>
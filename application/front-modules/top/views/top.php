<section id="header-top" >
                  <div id="topbar">
                     <div class="container">
                        <div class="show-desktop">
                           <div class="quick-access pull-left  hidden-sm hidden-xs">
                              <div class="login links link-active">
                              <?php if(!$this->session->userdata('member_name')){?>
                                 <a href="<?php echo base_url(); ?>tai-khoan/register">Đăng ký</a> or
                                 <a href="<?php echo base_url(); ?>tai-khoan/login">Đăng nhập</a>
                              <?php }else{?>
                                 <a href="<?php echo base_url(); ?>tai-khoan/logout">Đăng xuất</a>
                              <?php }?>
                              </div>
                           </div>
                           <!--Button -->
                           <div class="quick-top-link  links pull-right">
                              <!-- language -->
                              <div class="btn-group box-language">
                              </div>
                              <!-- currency -->
                              <!-- user-->
                              <?php if($this->session->userdata('member_name')){?>
                              <div class="btn-group box-user">
                                 <div data-toggle="dropdown">
                                    <span>Tài khoản cá nhân</span>
                                    <i class="fa fa-angle-down "></i>
                                 </div>
                                 <ul class="dropdown-menu setting-menu">
                                    <li id="wishlist">
                                       <a href="<?php echo base_url(); ?>tai-khoan/wishlist" id="wishlist-total"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Yêu thích</a>
                                    </li>
                                    <li class="acount">
                                       <a href="<?php echo base_url(); ?>tai-khoan/account"><i class="fa fa-user"></i>&nbsp;&nbsp;Tài khoản cá nhân</a>
                                    </li>
                                    <li class="shopping-cart">
                                       <a href="<?php echo base_url(); ?>tai-khoan/order"><i class="fa fa-bookmark"></i>&nbsp;&nbsp;Giỏ hàng</a>
                                    </li>
                                 </ul>
                              </div>
                              <?php }?>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- header -->
                  <header id="header">
                     <div id="header-main" class="hasmainmenu">
                        <div class="container">
                           <div class="row">
                              <div class="logo inner  col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                 <div id="logo-theme" class="logo-store pull-left">
                                    <a href="<?php echo base_url(); ?>">
                                    <span>CAMERA QUAN SÁT -- CAMERA AN NINH -- THIẾT BỊ VĂN PHÒNG</span>
                                    </a>
                                 </div>
                              </div>
                              <div class="inner col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class=" search_block">
                                  
                                    <form id="frmSearch" method="post" action="<?php echo base_url().'tim-kiem'; ?>" class="input-group search-box">
                                      
                                       <div id="search0" class="search-input-wrapper clearfix clearfixs input-group">
                                          <input type="text" id="keyword" name="txtSearch" value="" size="70" autocomplete="off" placeholder="Tìm sản phẩm.." name="search" class="form-control input-lg input-search">
                                          <span class="input-group-addon input-group-search">
                                          <span class="fa fa-search"></span>
                                           
                                          <span class="button-search ">
                                          Tìm sản phẩm..                    </span>
                                          </span>
                                       </div> 
                                    </form>
                                    <div class="clear clr"></div>
                                 </div>
                                        
                              </div>
                              <div id="hidemenu" class=" inner col-lg-3 col-md-3 col-sm-3 hidden-xs">
                                 <div class="cart-top">
                                    <div id="cart" class="btn-group btn-block">
                                       <div type="button" data-toggle="dropdown" data-loading-text="Loading..." class="heading dropdown-toggle">
                                          <div class="cart-inner pull-right">
                                             <div class="icon-cart"></div>
                                             <h4>Xem chi tiết giỏ hàng</h4>
                                             <a><span id="cart-total">0 sản phẩm - Liên hệ<i class="fa fa-angle-down"></i></span></a>
                                          </div>
                                       </div>
                                       <ul class="dropdown-menu pull-right content">
                                          <li>
                                             <p class="text-center empty">Không có sản phẩm trong giỏ hàng!</p>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </header>
                  
                  
                  <?php echo Modules::run('menu'); ?>
                  <!-- /header -->
               </section>
               
               
               
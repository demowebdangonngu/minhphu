<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
require_once APPPATH."third_party/YT/ControllerPublic.php";
class wish extends YT_ControllerPublic { 
    function __construct()	{ 
    		parent::__construct();		 
    		$this->model=$this->load->model('Model_wish');
            $this->load->model('product/model_product' , 'SP' , true); 
   	}
     
   public function add_wish(){ 
 
        $content_id = $this->input->post('product_id', TRUE);  
        $ajax = $this->input->post('ajax', TRUE);  
        $where['product.id'] = $content_id;  
        $where['product.status'] = 1;  
        $result = $this->SP->getList($where); 
       
        if($this->session->userdata('member_id')){
            if(isset($result[0]->id)){ 
                    
                    $dataInsert['id_product'] = $content_id;
                    $dataInsert['id_member'] = $this->session->userdata('member_id'); 
                    $arrList = $this->model->get_list_wish($dataInsert); 
              
                    if(empty($arrList)){
                        $idNew = $this->model->insert_list_wish($dataInsert);
                        ?>
                        <div class="alert alert-success">
                            <i class="fa fa-check-circle"></i> 
                            Thành công: Thêm vào <a href="<?php echo base_url().'tai-khoan/wishlist'; ?>">DS Yêu Thích</a> thành công!<button type="button" class="close" data-dismiss="alert">×</button>
                        </div>
                        <?php
                    }else{
                        ?>
                        <div class="alert alert-danger">
                            <i class="fa fa-exclamation-circle"></i> 
                            Xem <a href="<?php echo base_url().'tai-khoan/wishlist'; ?>">DS Yêu Thích</a><button type="button" class="close" data-dismiss="alert">×</button>
                        </div>
                        <?php
                    } 
                    exit; 
            }else{
                ?>
                <div class="alert alert-danger">
                    <i class="fa fa-exclamation-circle"></i> 
                    Sản phẩm không có trong kho!    <button type="button" class="close" data-dismiss="alert">×</button>
                </div>
                <?php
                exit;
            }
        }else{
            ?> 
            <div class="alert alert-info">
                <i class="fa fa-info-circle"></i> Bạn phải <a href="<?php echo base_url().'tai-khoan/login'; ?>">đăng nhập</a> hoặc <a href="<?php echo base_url().'tai-khoan/register'; ?>">tạo tài khoản</a> để lưu vào <a href="<?php echo base_url().'tai-khoan/wishlist'; ?>"> danh sách yêu thích</a>!
            <button type="button" class="close" data-dismiss="alert">×</button></div>
            <?php
            exit;
        } 
    }   
    public function delete_compare(){ 
        $id = $this->input->post('product_id', TRUE); // Assign posted product_id to $id
        $ajax = $this->input->post('ajax', TRUE); // Assign posted quantity to $cty   
        if($this->session->userdata('member_id') && $ajax == 1){
            if(isset($id)){  
                    unset($_SESSION['compare_products']['products'][$id]);
                    if(count($_SESSION['compare_products']['products']) == 0 ){
                        unset($_SESSION['compare_products']);
                    } 
                    echo 'Xoa thanh cong'; exit;  
            }else{
                echo 'Khong co content nay'; exit;
            }
        }else{
            echo 'Phai dang nhap';  exit;
        } 
    }
    public function delete_all_compare(){  
        $ajax = $this->input->post('ajax', TRUE); // Assign posted quantity to $cty   
        if($this->session->userdata('member_id') && $ajax == 1){ 
            unset($_SESSION['compare_products']);
            echo 'Xoa tat ca so sanh thanh cong'; exit;   
        }else{
            echo 'Phai dang nhap';  exit;
        } 
    }
    
    public function index(){
        $base_url = base_url('tai-khoan/wishlist'); 
        $slug = $this->uri->segment(1); 
        
        $where['id_member'] = $this->session->userdata('member_id');
		$this->data['product']=$this->model->get_list_product_pages(PER_PAGE_MAIN,array('uri_segment'=>1,'base'=>$slug),$where); 
        
        // breadcumb
        $breadcumb = array( 
            array(
                'url' => $base_url,
                'name' => 'Danh sách Yêu Thích'
            )
        ); 
        $this->data['title'] = $detail_name;
        $this->data['image'] = $catObj[0]->image_page;
        $this->data['description'] = $catObj[0]->description;
        $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);  
        //echo '<pre>'; print_r($this->data['product']); exit;
        $this->data['data_sanpham'] = $this->load->view('data_sanpham' , $this->data , TRUE); 
        $this->template->build('index_sanpham', $this->data);   
        
    }
   
}
 ?>
<div class="container">
   <?php  echo $breadcumb; ?>
   <div class="row">
      <aside id="sidebar-left" class="col-md-3">
         <column id="column-left" class="hidden-xs sidebar">
            <?php echo Modules::run('left/menuleft'); ?> 
            <?php echo Modules::run('left/tintuc_noibat'); ?> 
         </column>
      </aside>
      <section class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
         <div id="content">
            <h1 class="title-blogs"><?php echo $title; ?></h1>
            <a class="rss-wrapper clearfix pull-right" href="category/rssab5e.html?id=40"><span class="fa fa-feed"></span></a>
            <?php echo $data_tintuc_child;  ?>
         </div>
      </section>
   </div>
</div>
<footer id="footer">
    <?php echo Modules::run('footer'); ?>
</footer> 
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";
class category extends YT_ControllerPublic {
 
    public function __construct() {
        parent::__construct();
        $this->load->model('product/model_product', 'SP' , true);
        $this->load->model('category/model_category' , 'CAT' , true); 
        $this->load->model('posts/model_posts', 'POSTS' , true);
        
        
        $this->load->library('main/main', 'main');
    }
  
    public function detail($slug = '', $page = 1) { 
        if (!is_numeric($page)) {
            $page = 1;
        }
        if ($slug == '') {
            redirect();
        } 
        $list['category'] = $this->CAT->get_list(array('status' => 1 , 'slug' => $slug)); 
    
        $cat_id = 0;
        $pro_id = 0;
        $module = 'detail_main_cat'; 
        if(!empty($list['category'])){
            $cat_id = $list['category'][0]->id;
            $detail_name = $list['category'][0]->name;   
            if($list['category'][0]->type == 2){    // San Pham
                $module = 'detail_main_cat';
            }else{                                  // Tin tuc
                $module = 'detail_main_post';
            } 
        } 
        if ($pro_id == 0 && $cat_id == 0) {
            redirect();
        } 
        $slug = $slug;
        $this->$module($slug, $detail_name, $cat_id, $pro_id,$page , $list['category']);
    }
 
    private function detail_main_cat($slug, $detail_name ,$cat_id = 0,$pro_id = 0,$page = 1 , $catObj) { 

		$this->CAT->where(array('status'=>1,'id'=>$cat_id));
		$parent = current($this->CAT->get()->result());
        $ids[] = $cat_id;
		if($parent->parent == 0){
			$this->CAT->where(array('status'=>1,'parent'=>$cat_id));
			$list_parent = $this->CAT->get()->result(); 
			foreach($list_parent as $row){
				$ids[] = $row->id;
			}
		}else{
			$ids[] = $parent->id;
		}
		 //print_r($ids);exit; 
        $total = count($this->data['product']);
        $base_url = 'loai/'.$slug; 
        $slug = 'loai/'.$slug; 
		$this->data['product']=$this->SP->get_product_pages(PER_PAGE_MAIN,array('uri_segment'=>4,'base'=>$slug),$ids); 
        // breadcumb
        $breadcumb = array( 
            array(
                'url' => $base_url,
                'name' => $detail_name
            )
        ); 
        $this->data['title'] = $detail_name;
        $this->data['image'] = $catObj[0]->image_page;
        $this->data['description'] = $catObj[0]->description;
        $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);  

        $this->data['SEO']->build_meta($catObj[0],'category_product');
        //echo '<pre>'; print_r($this->data['product']); exit;
        $this->data['data_sanpham'] = $this->load->view('data_sanpham' , $this->data , TRUE); 
        $this->template->build('index_sanpham', $this->data);    
    }
    
    private function detail_main_post($slug, $detail_name ,$cat_id = 0,$pro_id = 0,$page = 1 , $catObj) { 
		$this->CAT->where(array('status'=>1,'id'=>$cat_id));
		$parent = current($this->CAT->get()->result());
        
        $this->CAT->where(array('status'=>1,'parent'=>$cat_id));
        $hasChild = current($this->CAT->get()->result());

		if($parent->parent == 0 && isset($hasChild->id) ){
			$this->CAT->where(array('status'=>1,'parent'=>$cat_id)); 
		    $this->data['list_cate_post'] = $this->CAT->get()->result(); 
            $viewData = 'data_tintuc_parent';
            $viewIndex = 'index_tintuc_parent';
		}else{
		  
            $ids[] = $cat_id;
    	 
          
		    $total = count($this->data['product']);
            $base_url = 'loai/'.$slug;
            $slug = $this->uri->segment(1); 
    		$this->data['posts']=$this->POSTS->get_posts_pages(2,array('uri_segment'=>2,'base'=>$slug),$ids); 
            $viewData = 'data_tintuc_child';
            $viewIndex = 'index_tintuc_child';
		} 
 	    
        // breadcumb
        $breadcumb = array( 
            array(
                'url' => $base_url,
                'name' => $detail_name
            )
        ); 
        $this->data['title'] = $detail_name;
        $this->data['image'] = $catObj[0]->image_page;
        $this->data['description'] = $catObj[0]->description; 
        $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);  
        $this->data['SEO']->build_meta($catObj[0],'category_post');

        $this->data[$viewData] = $this->load->view($viewData , $this->data , TRUE); 
        $this->template->build($viewIndex, $this->data);    
    }
    
    
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
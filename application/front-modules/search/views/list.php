<div class="container">
   <?php echo $breadcumb; ?>
   <div class="row">
      <section id="sidebar-main" class="col-md-12">
         <div id="content" class="wrapper clearfix">
            <h2>Tìm Kiếm - <?php echo $key; ?></h2> 
            <div class="product-filter ">
               <div class="inner clearfix">
                  <div class="display">
                     <div class="btn-group group-switch">
                        <button type="button" id="list-view" class="btn btn-switch list" data-toggle="tooltip" title="" data-original-title="Danh sách">
                        <i class="fa fa-th-list"></i>
                        </button>
                        <button type="button" id="grid-view" class="btn btn-switch grid active" data-toggle="tooltip" title="" data-original-title="Lưới sản phẩm">
                        <i class="fa fa-th-large"></i>
                        </button>
                     </div>
                  </div>
                  <!--
                  <div class="filter-right">
                     <div class="product-compare pull-right"><a href="http://camerano1.com/product/compare.html" class="btn btn-default" id="compare-total">So sánh sản phẩm</a></div>
                     
                  </div>-->
               </div>
            </div>
            <div id="products" class="product-grid">
               <div class="products-block">
                  <div class="row products-row">
                     
                     <?php
                     foreach($product_search['pageList'] as $k=>$v){
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 product-col col-fullwidth">
                        <?php echo $this->main->tooltip($v); ?>
                        </div>
                        <?php
                     }
                     
                     ?> 
                  </div>
               </div>
            </div>
            <div class="paginations paging clearfix">
                <?php echo $product_search['paging'];  ?>
               
            </div>
         </div>
      </section>
   </div>
</div>
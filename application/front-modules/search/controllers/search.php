<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once APPPATH."third_party/YT/ControllerPublic.php";
class search extends YT_ControllerPublic {  
 
    function __construct()	{
    		parent::__construct();		 
    	 $this->load->library('main/main', 'main');
  	$this->load->model('product/model_product' , 'SP' , true); 
   	}
    
   public function index($text) {   
        $text = trim($text); 
        if($text == '' || !isset($text)){
            redirect(base_url());
        } 
        $text1 = str_replace('-',' ',$text);
        $text = str_replace('-','%',$text);
       
        $like['product.name'] = str_replace('-' , ' ' , $text); 
        $this->data['sanpham'] = $this->SP->getList(false , false , $like);
        
         
        
         //print_r($ids);exit; 
        $total = count($this->data['product']);
        $base_url = 'tim-kiem/'.$this->uri->segment(2); 
        $slug = 'tim-kiem/'.$this->uri->segment(2); 
        $where = "product.name LIKE '%$text%'";
		$this->data['product_search']=$this->SP->get_product_pages(PER_PAGE_MAIN,array('uri_segment'=>3,'base'=>$slug),array(),array(),$where); 
         
        // breadcumb
        $breadcumb = array( 
            array(
                'url' => $base_url,
                'name' => 'Tìm kiếm'
            ),
            array(
                'url' => $base_url,
                'name' => $text1
            )
        );  
        
        $this->data['key'] = $text1;
        $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);    
        $this->template->build('list', $this->data); 
        
         
        
   }
     
   
}
 ?>
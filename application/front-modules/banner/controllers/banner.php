<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";
class banner extends YT_ControllerPublic {
 
    public function __construct() {
        parent::__construct();
        $this->model=$this->load->model('model_banner');
    }
	public function index()
	{
        $this->data['banner_main'] = $this->model->getList(array("loai" => 2,'status' => 1)); 
        $this->data['banner_right'] = $this->model->getList(array("loai" => 3,'status' => 1));  
        $this->load->view(__CLASS__,$this->data);  
	}
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
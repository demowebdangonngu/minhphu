<div class="pav-inner">
    <div class="row row-level-1 ">
        <div class="row-inner  clearfix">
            <div class="pav-col col-lg-3 col-md-4 col-sm-6 col-xs-12   ">
                <div class="col-inner">
                    <?php echo Modules::run('left/menuleft'); ?> 
                </div>
            </div>
            <div class="pav-col col-lg-9 col-md-8 col-sm-6 col-xs-12   ">
                <div class="col-inner" style="overflow:hidden">
                    <?php
                    if(!empty($banner_right)){
                        ?>
                        <div class="layerslider_right" style="float:right;margin-left: 2px">  
                            <a href="<?php echo base_url($banner_mid[0]->slug); ?>">
                                <img  width='250' height='355' src="<?php echo $this->config->item("img_path") .  $banner_right[0]->location; ?>" alt="<?php echo $banner_right[0]->name; ?>" /> 
                            </a>
                        </div>
                        <?php
                    }
                    ?> 
                    <div class="layerslider" style="overflow:hidden"> 
                    	<script type="text/javascript" src="<?php echo THEME_FRONT; ?>js/jquery.bxslider.js"></script>
                    	<ul id="bxslider">
                    		<?php 
                            foreach($banner_main as $k=>$v){
                                $str = $k==0?' active':'';
                                if($v->location != ''){
                                  ?>    
	                                <li >
	                                  <img  width='100%' height='355' src="<?php echo $this->config->item("img_path") .  $v->location; ?>"  alt="<?php echo $v->name; ?>"/> 
	                                </li>	 
                                  <?php
                                } 
                            } 
                            ?>  
						</ul>
						<script>
    						$( document ).ready(function() { 
    						    $('#bxslider').bxSlider({
    					            mode: 'fade',
    					            auto: true,
    					            autoHover: true,
    					            autoStart: true,
    					            infiniteLoop: true,
    					            pagerCustom: '#bx-pager',
    					            hideControlOnEnd: true
    					        }); 
    						}); 
						</script> 
                    </div>


                    <?php echo Modules::run('product/product_hot_home'); ?>
                     
                </div>
            </div>
        </div>
    </div>
</div>
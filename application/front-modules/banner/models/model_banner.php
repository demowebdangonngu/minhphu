<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
require_once APPPATH."third_party/YT/Model.php";
class Model_banner extends YT_Model {

    public function __construct() {
        $this->table_name="attach";
        parent::__construct(); 
    }
    public function getList($where = false ){
		$this->getDB()->select("*");
		$this->getDB()->from($this->table_name); 
        $this->getDB()->where("status = 1  AND nameModel = 'banner'");    
        if($where)
            $this->getDB()->where($where);   
 
		$result  = $this->getDB()->get()->result();
        return $result;
	}  
}
<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";
class Left extends YT_ControllerPublic { 

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
         * 
	 */
    public function __construct() {
        parent::__construct();
        $this->load->model('category/model_category' , 'CAT' , true); 
        $this->load->model('product/model_product','PRODUCT' , TRUE);
        $this->load->model('posts/model_posts','POSTS' , TRUE);
        $this->load->library('main/main', 'main');
    }
	public function index()
	{
            
           $this->load->view(__CLASS__); 
           //$this->template->build('welcome_message'); 
	}
    public function menuleft()
	{  
	   	$where['categories.type'] = 2;
		$this->data['category'] = $this->CAT->get_tree(array(0, 1),$where);
        $this->load->view('menuleft', $this->data);  
	}
    public function menuleft_posts()
	{  
	   $where['categories.type'] = 2;
		$this->data['category'] = $this->CAT->get_tree(array(0, 1, 2),$where);
        $this->load->view('menuleft_posts', $this->data);  
	}
    
    
    
    
    public function menuleft_side()
	{ 
           $this->load->view('menuleft_side');  
	}
    public function sanpham_noibat()
	{ 
           
           $this->data['product_hot_home'] = $this->PRODUCT->getList(false , array('per_page' => 4, 'start' => 0) , false, false , true);
           $this->load->view('sanpham_noibat' , $this->data); 
	}
    public function tintuc_noibat()
	{ 
	        
           $this->data['post_new'] = $this->POSTS->getList(false , array('per_page' => 4, 'start' => 0) , false, false , true);
           $this->load->view('tintuc_noibat' , $this->data);  
	}
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<div id="pav-verticalmenu" class="pav-verticalmenu hasicon box box-highlighted hidden-xs">
   <div class="box-heading">
      <span>DANH MỤC BÀI VIẾT</span>
   </div>
   <div class="box-content">
      <div id="verticalmenu" class="verticalmenus" role="navigation">
         <div class="navbar navbar-vertical">
            <a href="javascript:;" data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle">
            <span class="icon-bar"></span>
            </a>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
               <ul class="nav navbar-nav verticalmenu">
                  
                  <?php 
                     foreach($category as $v){
                        if(!empty($v->children)){
                  ?>
                  <li class="parent dropdown">
                     <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo $SEO->build_link($v,"menu");?>"><span class="menu-icon" style="background:url('image/catalog/icon_camera.png') no-repeat;"><span class="menu-title"><?php echo $v->name;?></span><b class="caret"></b></span></a>
                     <div class="dropdown-menu level1">
                        <div class="dropdown-menu-inner">
                           <div class="row">
                              <div class="mega-col col-md-12" data-type="menu">
                                 <div class="mega-col-inner">
                                    <ul>
                                       <?php foreach($v->children as $k1=>$v1){  ?>
                                       <li <?php if(!empty($v1->children)){?>class="parent dropdown-submenu"<?php }?>>
                                          <a class="dropdown-toggle" data-toggle="dropdown" href="<?php echo $SEO->build_link($v1,"menu");?>"><span class="menu-title"><?php echo $v1->name;?></span><b class="caret"></b></a>
                                          <?php if(!empty($v1->children)){?>
                                          <div class="dropdown-menu level2">
                                             <div class="dropdown-menu-inner">
                                                <div class="row">
                                                   <div class="col-sm-12 mega-col" data-colwidth="12" data-type="menu">
                                                      <div class="mega-col-inner">
                                                         <ul>
                                                            <?php 
                                                            foreach($v1->children as $k2=>$v2){ 
                                                            ?>
                                                            <li class=" "><a href="<?php echo $SEO->build_link($v2,"menu");?>"><span class="menu-title"><?php echo $v2->name;?></span></a></li>
                                                            <?php }?>
                                                         </ul>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <?php }?>
                                       </li>
                                       <?php }?>
                                    </ul>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <?php }else{?> 
                  <li class="">
                     <a href="<?php echo $SEO->build_link($v,"menu");?>"><span class="menu-icon" style="background:url('image/catalog/icon_dauthu_05.png') no-repeat;"><span class="menu-title"><?php echo $v->name;?></span></span></a>
                  </li>
                  <?php }}?>
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
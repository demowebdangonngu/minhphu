<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";
class Home extends YT_ControllerPublic {
    public function __construct() {
        parent::__construct();
        $this->load->library('main/main', 'main');
        $this->load->model('product/model_product', 'SP' , true);
        $this->load->model('category/model_category' , 'CAT' , true); 
        $this->load->model('banner/model_banner' , 'BANNER' , true); 
    }
    public function index() { 
        
        
        //$this->data['list_new'] = $this->SP->getList(false ,array('per_page' => 8, 'start' => 0) , false , false , true); 
        //$this->data['list_sale'] = $this->SP->getList(false ,array('per_page' => 8, 'start' => 0) , false , 'product.status = 1' , true);
        
          
        $this->data['banner_mid'] = $this->BANNER->getList(array("loai" => 1,'status' => 1 )); 


         
        $category = $this->CAT->get_list(array('status' => 1 , 'type'=>2 , 'show'=>1));   
        
        $category_parent = array();
        foreach($category as $k=>$v){
            if($v->parent == 0){
                $category_parent[] = $category[$k];
            } 
        } 
        foreach($category_parent as $k=>$v){  
            foreach($category as $k1=>$v1){ 
                if($v1->parent == $v->id){
                    $category_parent[$k]->list_child[] = $category[$k1];
                } 
            } 
        } 
        
        
        /*
        foreach($category_parent as $k=>$v){  
            $str_where = '( parent = '. $v->id . '  OR product.cat_id =  '.$v->id.' )'; 
            $category_parent[$k]->list_product = $this->SP->getList($str_where ,array('per_page' => 8, 'start' => 0) , false , false , true);
        } 
        */
        foreach($category_parent as $k=>$v){ 
			if($v->image_home != ''){
				foreach($v->list_child  as $k1=>$v1){
					$str_where = '( product.cat_id = '. $v->id . '  OR product.cat_id =  '.$v1->id.' )';   
					$category_parent[$k]->list_child[$k1]->list_product = $this->SP->getList($str_where ,array('per_page' => 8, 'start' => 0) , false , false , true);               
				}
			}else{
				foreach($v->list_child  as $k1=>$v1){
					$str_where = '( product.cat_id = '. $v->id . '  OR product.cat_id =  '.$v1->id.' )';   
					$category_parent[$k]->list_child[$k1]->list_product = $this->SP->getList($str_where ,array('per_page' => 12, 'start' => 0) , false , false , true);               
				}
			}	
        } 
		
        $this->data['list_category'] = $category_parent;  
        foreach($category_parent as $k=>$v){   
             
            if(!empty($v->list_child)){
                $this->data['category_product_home'] = $v; 
                if($v->image_home != ''){
                    $this->data['cat_html_home_have_image'][] = $this->load->view('cat_html_home_have_image' , $this->data , TRUE);
                }else{
                    $this->data['cat_html_home_not_image'][] = $this->load->view('cat_html_home_not_image' , $this->data , TRUE);    
                } 
            } 
        }
        
        
        //
//        $this->load->model('baiviet/model_baiviet' , 'POSTS', true);  
//        $limit = array('per_page' => 2, 'start' => $start); 
//        $this->data['posts'] = $this->POSTS->getList($where, $limit); 
//        
       // $meta=new stdClass();  
      //  $this->data['SEO']->build_meta($meta,'home');
        return $this->template->build('home', $this->data);
        
    }


    function showHome(){ 
         
        
 
        
        return $this->template->build('index', $this->data);
        $this->output->_display( $html_cache);
    }

    
    
    
   
   

}
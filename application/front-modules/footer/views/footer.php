<section class="footer-top " id="pavo-footer-top">
               <div class="container">
               <div class="inner">
               <div class="row">
               <div class="col-md-3 col-sm-12">
               <div class="box-content">                                            <div class="address">                                                <p><span style="color: #ffa200;">HỖ TRỢ KHÁCH HÀNG</span></p>                                                <div class="box-addres">                                                    <div class="link-address icon">                                                       <span class="fa fa-phone">&nbsp;</span><span>Phone: 0933.149.990</span></div>                                                    <div class="link-mobile icon">                                                        <span class="fa fa-phone">&nbsp;</span><span>Phone: 0168.258.6825</span>                                                    </div>                                                    <div class="link-mail icon">                                                        <span class="fa fa-envelope">&nbsp;</span>                                                        <span>Email: ledinhhung.ktp@gmail.com</span>                                                    </div>                                                </div>                                            </div>                                        </div>                </div>
               <div class="col-md-2 col-sm-4">
               <h5 class="box-heading"><span>Chăm sóc khách hàng</span></h5>
               <ul class="list-unstyled">
                   <li><a href="<?php echo base_url(); ?>pages/gioi-thieu.html">Giới thiệu</a></li> 
                    <li><a href="<?php echo base_url(); ?>pages/lien-he.html">Liên hệ</a></li> 
               </ul>
               </div>
               <div class="col-md-2 col-sm-4">
                    <h5 class="box-heading"><span>Nhà cung cấp</span></h5>
                    <ul class="list-unstyled">
                         <?php 
                       foreach($provider as $k=>$v){
                          ?> 
                          <li>
                             <a  title="<?php echo $v->name; ?>" href="<?php echo $SEO->build_link($v,"provider"); ?>" ><?php echo $v->name; ?></a> 
                          </li> 
                          <?PHP 
                       }
                       ?>
                    </ul> 
               </div>
               <div class="col-md-2 col-sm-4">
               <h5 class="box-heading"><span>Tài khoản của tôi</span></h5>
               <ul class="list-unstyled">
               <li><a href="<?php echo base_url(); ?>user/account">Tài khoản của tôi</a></li>
               <li><a href="<?php echo base_url(); ?>user/order">Lịch sử đơn hàng</a></li>
               <li><a href="<?php echo base_url(); ?>user/wishlist">Danh sách yêu thích</a></li>
               <li><a href="<?php echo base_url(); ?>user/newsletter">Thư thông báo</a></li>
               </ul>
               </div>
               <div class="col-md-3 col-sm-12">
               <div class="box pav-custom  ">                                        <div class="box-heading">                                            <span>LIÊN HỆ</span>                                        </div>                                        <div class="box-content">                                            <div class="address">                                                <p><span style="color: #ffa200;">TRUNG TÂM THIẾT BỊ AN NINH VÀ GIÁM SÁT CÔNG NGHỆ CAO TH </span></p>                                                <div class="box-addres">                                                    <div class="link-address icon">                                                        <span class="fa fa-map-marker">&nbsp;</span><span>6/1/16 DT743 ,Đồng an 3 ,Bình Hòa ,Thuận An, Bình Dương.</span>                                                    </div>                                                    <div class="link-mobile icon">                                                        <span class="fa fa-phone">&nbsp;</span><span>Phone: 0168.258.6825</span>                                                    </div>                                                    <div class="link-mail icon">                                                        <span class="fa fa-envelope">&nbsp;</span>                                                        <span>Email: support@gmail.com</span>                                                    </div>                                                </div>                                            </div>                                        </div>                                    </div>                </div>
               </div>
               </div>
               </div>
               </section>
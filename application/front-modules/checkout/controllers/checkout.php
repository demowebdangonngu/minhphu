<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
require_once APPPATH."third_party/YT/ControllerPublic.php";
class checkout extends YT_ControllerPublic { 
    function __construct()	{ 
    		parent::__construct();		 
    		$this->model=$this->load->model('Model_checkout');
            $this->load->model('product/model_product', 'SP' , true);
            $this->load->library('main/main');
   	}
     
   function index(){
        $cart_content = $this->cart->contents();
        if(is_array($cart_content)){ 
            $this->data['count'] = count($cart_content);
            $this->data['cart_content'] = $cart_content;
            $this->data['total_price'] = $this->cart->format_number($this->cart->total()); 

            $breadcumb = array(   
                array(
                    'url' => 'cartorder',
                    'name' => 'Giỏ hàng'
                ),
                array(
                    'url' => 'checkout',
                    'name' => 'Thanh toán'
                )
            );  
            $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);    

            

            if(!$this->session->userdata('member_name')){
                $this->template->build('thanhtoan1', $this->data);  
            }else{

                $this->data['data_address'] = $this->input->post('address' , TRUE);
                if($this->data['data_address'] != ''){ 
                    $userdata = array( 'member_address' => $this->data['data_address']);
                    $this->session->set_userdata($userdata); 
                    $this->template->build('thanhtoan2', $this->data); 
                }else{

                    $this->data['data_address'] = $this->session->userdata('member_address');

                    $this->template->build('thanhtoan_address', $this->data); 
                } 
            } 
        }else{
            redirect(base_url());
        } 
        
        
   }
     
   
}
 ?>
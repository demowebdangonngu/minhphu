
<div class="container">
    
    <?php echo $breadcumb; ?>
    <div class="row">
       <section id="sidebar-main" class="col-md-12">
          <div id="content" class="wrapper clearfix">
             <h1>Thanh Toán</h1>
             <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                   <div class="panel-heading">
                      <h4 class="panel-title">Bước 1: Đăng nhập tài khoản</h4>
                   </div>
                   <div class="panel-collapse collapse" id="collapse-checkout-option">
                      <div class="panel-body"></div>
                   </div>
                </div> 
                

                <div class="panel panel-default">
                   <div class="panel-heading">
                      <h4 class="panel-title"><a href="#collapse-checkout-confirm" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Bước 2: Địa Chỉ Giao Hàng <i class="fa fa-caret-down"></i></a></h4>
                   </div>
                   <div class="panel-collapse collapse in" id="collapse-checkout-confirm" style="height: auto;">
                      <div class="panel-body">
                      	<form action="" method="POST">
	                         <div class="table-responsive">
	                         	<h2>Địa chỉ giao hàng: </h2>
	                           	<textarea name="address" class="form-control"><?php echo $data_address; ?></textarea>
	                           	<br />
	                         </div>
	                         <div class="buttons">
	                            <div class="pull-right"> 
	                                <input type="submit" value="Xác nhận Địa Chỉ" id="button-confirm" class="btn btn-outline"> 
	                            </div>
	                         </div> 
                         </form>
                      </div>
                   </div>
                </div>

                <div class="panel panel-default">
                   <div class="panel-heading">
                      <h4 class="panel-title">Bước 3: Xác Nhận Đơn Hàng</h4>
                   </div>
                   <div class="panel-collapse collapse" id="collapse-checkout-option">
                      <div class="panel-body"></div>
                   </div>
                </div>
  
             </div>
          </div>
       </section>
       <footer id="footer">
             <?php echo Modules::run('footer'); ?>
       </footer> 
    </div>
</div>


<div class="container">
    
    <?php echo $breadcumb; ?>
    <div class="row">
       <section id="sidebar-main" class="col-md-12">
          <div id="content" class="wrapper clearfix">
             <h1>Thanh Toán</h1>
             <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                   <div class="panel-heading">
                      <h4 class="panel-title">Bước 1: Đăng nhập tài khoản</h4>
                   </div>
                   <div class="panel-collapse collapse" id="collapse-checkout-option">
                      <div class="panel-body"></div>
                   </div>
                </div> 
                <div class="panel panel-default">
                   <div class="panel-heading">
                      <h4 class="panel-title">Bước 2: Địa Chỉ Giao Hàng (F5 để nhập lại địa chỉ)</h4>
                   </div>
                   <div class="panel-collapse collapse" id="collapse-checkout-option">
                      <div class="panel-body"></div>
                   </div>
                </div> 
                <div class="panel panel-default">
                   <div class="panel-heading">
                      <h4 class="panel-title"><a href="#collapse-checkout-confirm" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Bước 2: Xác Nhận Đơn Hàng <i class="fa fa-caret-down"></i></a></h4>
                   </div>
                   <div class="panel-collapse collapse in" id="collapse-checkout-confirm" style="height: auto;">
                      <div class="panel-body">
                         <div class="table-responsive">
                            <table class="table table-bordered table-hover">
                               <thead>
                                  <tr>
                                     <td class="text-left">Tên sản phẩm</td>
                                     <td class="text-left">Mã hàng</td>
                                     <td class="text-right">Số lượng</td>
                                     <td class="text-right">Giá</td>
                                     <td class="text-right">Tổng Cộng</td>
                                  </tr>
                               </thead>
                               <tbody>
                                  <?php 
                                    $sp = $this->load->model('product/model_product');
                                    foreach($cart_content as $k=>$v){
                                        $where['product.id'] = $v['name'];
                                        $rsp = $sp->getList($where);
                                        ?>
                                        
                                            
                                      <tr>
                                         <td class="text-left"><a href="<?php echo $SEO->build_link($rsp[0],"sanpham")?>"><?php echo $rsp[0]->name; ?></a>
                                         </td>
                                         <td class="text-left"><?php echo $rsp[0]->name; ?></td>
                                         <td class="text-right"><?php echo $v['qty']; ?></td>
                                         <td class="text-right"><?php echo number_format($v['price']); ?>VNĐ</td>
                                         <td class="text-right"><?php echo number_format($v['subtotal']); ?>VNĐ</td>
                                      </tr>
                                          
                                     <?php                  
                                    } 
                                    ?> 
                                
                               </tbody>
                               <tfoot>
                                  <tr>
                                     <td colspan="4" class="text-right"><strong>Thành tiền:</strong></td>
                                     <td class="text-right"><?php echo $total_price; ?>VNĐ</td>
                                  </tr> 
                                  <tr>
                                     <td colspan="4" class="text-right"><strong>Tổng cộng :</strong></td>
                                     <td class="text-right"><?php echo $total_price; ?>VNĐ</td>
                                  </tr>
                               </tfoot>
                            </table>
                         </div>
                         <div class="buttons">
                            <div class="pull-right">
                            <form action="<?php echo base_url().'cartorder/order'; ?>">
                                <input type="submit" value="Xác nhận đơn hàng" id="button-confirm" class="btn btn-outline">
                            </form>
                              
                            </div>
                         </div> 
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </section>
       <footer id="footer">
             <?php echo Modules::run('footer'); ?>
       </footer> 
    </div>
</div>

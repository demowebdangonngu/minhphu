<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/Model.php";
class Model_checkout extends YT_Model {
	public function __construct(){  
		parent::__construct();
        $this->table_name= "order";   
	} 
    public function insert_hd($data){ 
        $this->getDB()->insert($this->table_name, $data); 
        return  $this->getDB()->insert_id();   
	} 
    public function insert_cthd($data){ 
        $this->getDB()->insert('order_detail', $data); 
        return  $this->getDB()->insert_id();   
	}
    public function get_member($where){
        $this->getDB()->select('*');
        $this->getDB()->from('member');
        $this->getDB()->where($where); 
        return  $this->getDB()->get()->result(); 
    }
    public function insert_member($data){ 
        $this->getDB()->insert('member', $data); 
        return  $this->getDB()->insert_id();   
	}
    public function update_member($data , $where){  
        $this->getDB()->where($where);
        $this->getDB()->update('member', $data);    
	}
    
    public function get_list_cthd($where){
        $this->getDB()->select('order_detail.* , email , order.status , order.total_price , product.name as name_sp , image');
        $this->getDB()->from('order_detail');
        $this->getDB()->join('order' , 'order.id = order_detail.order_id');
        $this->getDB()->join('product' , 'product.id = order_detail.product_id');
        $this->getDB()->where($where); 
        return  $this->getDB()->get()->result(); 
    }
	 
}
?>
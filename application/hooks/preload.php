<?php

class Preload extends MX_Controller {

    public $redis;
    public $client;

    public function index() {
        //$this->loadElasticsearch();
        //$this->loadRedis();
    }

    private function loadElasticsearch() {
        $params = array();
        $this->config->load('elasticsearch');
        if (is_array($this->config->config['elasticsearch'])) {
            $configElasticsearch = $this->config->config['elasticsearch'];
        } else {
            $configElasticsearch['IP_Port'] = '127.0.0.1:9200';
        }
        $params['hosts'] = array(
            $configElasticsearch['IP_Port'], // IP + Port
        );
        require_once '/../libraries/elasticsearch/vendor/autoload.php';
        $CI = & get_instance();
        $CI->client = new Elasticsearch\Client($params);
    }

    private function loadRedis() {
        $this->config->load('redis');
        if (is_array($this->config->config['redis'])) {
            $configRedis = $this->config->config['redis'];
        } else {
            $configRedis = array(
                'IP' => '127.0.0.1',
                'Port' => 6379
            );
        }
        $CI = & get_instance();
        $CI->redis = new Redis();
        $CI->redis->pconnect($configRedis['IP'], $configRedis['Port']);
    }

}

?>

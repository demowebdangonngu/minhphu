<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
require_once APPPATH."third_party/YT/Model.php";
class user_model extends YT_Model {

    public function __construct() {
        $this->table_name="users";
        parent::__construct(); 
    }
    
 	function insert_member($data){
 		$this->getDB()->insert('member', $data);
 		return  $this->getDB()->insert_id();
 	}
   
   public function edit($id, $data)	{
		$this->getDB()->where('id', $id);
		if($this->getDB()->update('member', $data)){
			return 1;
		}else{
			return 0;
		}
	}

	function get_detail_order($where = false){
        $return=array();  
        $this->getDB()->select("order.id as oid,   total_price,   order_date,order.status as ostatus,order.address as oaddress   ,  member.email as memail, member.name as mname, member.phone as mphone, member.address as maddress ");
        $this->getDB()->from('order'); 
        $this->getDB()->join ("member", "order.mem_id=member.id");  
        if($where){ 
           $this->getDB()->where($where);
        }   
        $result=$this->getDB()->get()->result();  
        return $result; 
    }
    function get_detail_order2($where = false){
        $return=array();  
        $this->getDB()->select("product.id,product.name,code, order_detail.*");
        $this->getDB()->from('order_detail'); 
        $this->getDB()->join ("product", "product.id=order_detail.product_id"); 
        if($where){ 
           $this->getDB()->where($where);
        }   
        $result=$this->getDB()->get()->result();  
        return $result; 
    }



	function get_list_order_pages($limit=10,$option=array(),  $where = false){
        $return=array(); 
		$this->getDB()->select("order.*,member.email as memail, member.name as mname, member.phone as mphone, member.address as maddress ");
        $this->getDB()->from('order');
        $this->getDB()->join ("member", "order.mem_id=member.id"); 
         
        if($where){ 
           $this->getDB()->where($where);
        } 
        
        
        $pagination=$this->load->library("Pagination");
        $result['total']=$config['total_rows'] = $this->getDB()->count_all_results();;
		$config['per_page'] = $limit;
         $option['base']?$option['base']=trim($option['base'],"/")."/":"latest";
        $config['base_url']=base_url().$option['base'];
        $config['use_page_numbers']=true;
        $config['uri_segment']=$option['uri_segment']?$option['uri_segment']:2;
        $pagination->initialize($config);
        $result['paging']=$pagination->create_links();
        
        
        $this->getDB()->select("order.*,member.email as memail, member.name as mname, member.phone as mphone, member.address as maddress ");
        $this->getDB()->from('order');
        $this->getDB()->join ("member", "order.mem_id=member.id"); 
        if($where){ 
           $this->getDB()->where($where);
        }  
        $this->getDB()->order_by("order.order_date","DESC"); 
        $this->getDB()->limit($limit,$pagination->cur_page>1?($pagination->cur_page-1)*$limit:0);
        $result['cur_page']=$pagination->cur_page;
        $plist=$this->getDB()->get()->result();
        $result['pageList']=array();
        $pids=array();
        foreach ($plist as $p){
            $result['pageList'][$p->id]=$p;
             $pids[]=$p->id;
        }
		// print_r($result);exit;

        return $result;
        
    }
   
       
}
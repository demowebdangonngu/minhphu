<h1>Thông Tin Tài Khoản</h1>
<form action="" method="post" id="changeuser" enctype="multipart/form-data" class="form-horizontal">
  <fieldset>
    <legend>Thông tin cá nhân</legend>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-firstname">Họ và tên: </label>
      <div class="col-sm-10">
        <input type="text" name="name" value="<?php echo $obj->name;?>" placeholder="Tên:" id="input-firstname" class="form-control">
                    </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-email">E-Mail:</label>
      <div class="col-sm-10">
        <input type="email" name="email" value="<?php echo $obj->email;?>" placeholder="E-Mail:" id="input-email" class="form-control">
                    </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-telephone">Điện thoại:</label>
      <div class="col-sm-10">
        <input onkeypress='validate(event)' type="tel" name="phone" value="<?php echo $obj->phone;?>" placeholder="Điện thoại:" id="input-telephone" class="form-control">
                    </div>
    </div>
    <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-address">Địa chỉ:</label>
      <div class="col-sm-10">
        <input type="tel" name="address" value="<?php echo $obj->address;?>" placeholder="Địa chỉ:" id="input-address" class="form-control">
                    </div>
    </div>
  </fieldset>
  <div class="buttons clearfix">
    <div class="pull-left"><a href="<?php echo base_url(); ?>tai-khoan/account" class="btn btn-default">Quay lại</a></div>
    <div class="pull-right">
      <input type="button" onclick="submit_edit()" value="Tiếp tục" class="btn btn-primary">
    </div>
  </div>
</form>
<script>
function Redirect(user) {
   window.location = root + 'tai-khoan/account';
}

function submit_edit() {
  $.ajax({
    type: 'POST',
    url: root + 'user/changeuser',
    data: $('#changeuser').serialize(),
    success: function (value) {
      if (value == 1) {
        swal("Sửa thành công! Bạn vui lòng chờ giây lát ","", "success");
        setTimeout('Redirect()', 2000);
      } else {
        swal("Không sửa được! vui lòng liên hệ tới bộ phận kỹ thuật.", "", "error");
      }
    }
  });
}

function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

</script>
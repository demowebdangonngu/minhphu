<div>Bạn muốn nhận thông báo các sản phẩm mới từ email của chúng tôi! hãy chọn đồng ý để luôn cập nhật thông tin mới nhất từ Elara.vn</div>
<a href="#" onclick="submit_huy()" class="btn btn-default">Hủy nhận thông báo</a><input type="button" onclick="submit_nhan()" value="Đồng ý nhận thông báo" class="btn btn-primary">
<script>
function submit_huy(){
  $.ajax({
      type: 'POST',
      url: root + 'user/newsletter',
      data: 'set_email=0',
      success: function (value) {
          if (value == 1) {
              swal('Đã hủy nhận thông báo',"", "success");
          } else {
            swal('Không thể thay đổi! vui lòng liên hệ với bộ phận kỹ thuật.',"", "success");
             return false;
          }
      }
  });
}
function submit_nhan(){
  $.ajax({
      type: 'POST',
      url: root + 'user/newsletter',
      data: 'set_email=1',
      success: function (value) {
          if (value == 1) {
              swal('Đã đồng ý nhận thông báo',"", "success");
          } else {
             swal('Không thể thay đổi! vui lòng liên hệ với bộ phận kỹ thuật.',"", "success");
             return false;
          }
      }
  });
}
</script>
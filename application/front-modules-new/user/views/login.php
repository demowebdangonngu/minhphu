<div class="row">
   <div class="col-sm-6">
      <div class="well">
         <h2>Khách hàng mới</h2>
         <p><strong>Đăng kí tài khoản</strong></p>
         <p>Bằng cách tạo tài khoản bạn sẽ có thể mua sắm nhanh hơn, cập nhật tình trạng đơn hàng, theo dõi những đơn hàng đã đặt.</p>
         <a href="<?php echo base_url(); ?>tai-khoan/register" class="btn btn-outline">Tiếp tục</a>
      </div>
   </div>
   <div class="col-sm-6">
      <div class="well">
         <h2>Khách hàng cũ</h2>
         <p><strong>Tôi là khách hàng cũ</strong></p>
         <form action="" method="post" id="frm_login" enctype="multipart/form-data" class="form-horizontal">
            <div class="form-group">
               <label class="control-label" for="input-email">Địa chỉ E-Mail:</label>
               <input type="text" name="email" value="" placeholder="Địa chỉ E-Mail:" id="input-email" class="form-control">
            </div>
            <div class="form-group">
               <label class="control-label" for="input-password">Mật khẩu:</label>
               <input type="password" name="password" value="" placeholder="Mật khẩu:" id="input-password" class="form-control">
               <!--<a href="#">Quên mật khẩu</a>-->
            </div>
            <input type="button" onclick="submit_login()" value="Đăng nhập" class="btn btn-outline">
         </form>
      </div>
   </div>
</div>
<script>
function Redirect(user) {
   window.location = root + 'tai-khoan/account';
}

function submit_login() {
  $.ajax({
    type: 'POST',
    url: root + 'user/login',
    data: $('#frm_login').serialize(),
    success: function (value) {
      var obj = JSON.parse(value);
      if (obj[1] == 1) {
        swal("Đăng nhập thành công! Bạn vui lòng chờ giây lát ","", "success");
        setTimeout('Redirect()', 2000);
      } else {
        swal("Mật khẩu hoặc tài khoản nhập sai! vui lòng thử lại.", "", "error");
      }
    }
  });
}
</script>
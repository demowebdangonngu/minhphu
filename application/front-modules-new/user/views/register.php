
<h1>Đăng Kí Tài Khoản</h1>
<p>Nếu bạn đã đăng kí tài khoản, vui lòng đăng nhập tại <a href="<?php echo base_url(); ?>tai-khoan/login">đây</a>.</p>
<form action="" method="post" id="frm_add_edit" enctype="multipart/form-data" class="form-horizontal">
   <fieldset id="account">
  <legend>Thông tin cá nhân</legend>
  <div class="form-group required" style="display: none;">
    <label class="col-sm-2 control-label">Nhóm khách hàng</label>
    <div class="col-sm-10">
                                  <div class="radio">
        <label>
          <input type="radio" name="customer_group_id" value="1" checked="checked">
          Default</label>
      </div>
                                </div>
  </div>
  <div class="form-group required">
    <label class="col-sm-2 control-label" for="input-lastname">Họ và tên:</label>
    <div class="col-sm-10">
      <input type="text" name="data[name]" value="<?php echo $_POST['data']['name'];?>" placeholder="Họ và tên:" id="name" class="form-control">
                  </div>
  </div>
  <div class="form-group required">
    <label class="col-sm-2 control-label" for="input-email">Địa chỉ E-Mail:</label>
    <div class="col-sm-10">
      <input type="email" name="data[email]" value="<?php echo $_POST['data']['email'];?>" placeholder="Địa chỉ E-Mail:" id="email" class="form-control">
                  </div>
  </div>
  <div class="form-group required">
    <label class="col-sm-2 control-label" for="input-telephone">Điện Thoại:</label>
    <div class="col-sm-10">
      <input onkeypress='validate(event)' type="tel" name="data[phone]" value="<?php echo $_POST['data']['phone'];?>" placeholder="Điện Thoại:" id="phone" class="form-control">
                  </div>
  </div>
      </fieldset>
   <fieldset id="address">
      <legend>Địa chỉ của bạn</legend>
      <div class="form-group">
         <label class="col-sm-2 control-label" for="input-address-1">Địa chỉ:</label>
         <div class="col-sm-10">
            <input type="text" name="data[address]" value="<?php echo $_POST['data']['address'];?>" placeholder="Địa chỉ:" id="address1" class="form-control">
            <div class="text">Địa chỉ phải từ 3 đến 128 kí tự!</div>
         </div>
      </div>
      
   </fieldset>
   <fieldset>
      <legend>Mật khẩu</legend>
      <div class="form-group">
         <label class="col-sm-2 control-label" for="input-password">Mật Khẩu:</label>
         <div class="col-sm-10">
            <input type="password" name="data[password]" value="" placeholder="Mật Khẩu:" id="password1" class="form-control">
            <div class="text">Mật khẩu phải từ 6 đến 20 kí tự!</div>
         </div>
      </div>
      <div class="form-group">
         <label class="col-sm-2 control-label" for="input-confirm">Nhập lại Mật Khẩu:</label>
         <div class="col-sm-10">
            <input type="password" name="data[password_nl]" value="" placeholder="Nhập lại Mật Khẩu:" id="password2" class="form-control">
         </div>
      </div>
   </fieldset>
   <!--<fieldset>
      <legend>Thư thông báo</legend>
      <div class="form-group">
         <label class="col-sm-2 control-label">Đăng kí thông báo:</label>
         <div class="col-sm-10">
            <label class="radio-inline">
            <input type="radio" name="newsletter" value="1">
            Có</label>
            <label class="radio-inline">
            <input type="radio" name="newsletter" value="0" checked="checked">
            Không</label>
         </div>
      </div>
   </fieldset>-->
   <img style="float:left" src="../captcha/captcha.php" id="captcha" /><br/>
                  <input type="text" style="font-size: 13px;width: 10%;" onclick="
    document.getElementById('captcha').src='<?php echo $this->config->item('static_path') ?>captcha/captcha.php?'+Math.random();
    document.getElementById('captcha-form').focus();"
    id="btn-send" class="submit btn btn-outline" value="Đổi hình." readonly=""/><br><br/>

  Captcha
  <input type="text" name="captcha" id="captcha-form" autocomplete="off" /><br/>
   <div class="buttons">
      <div class="pull-right"><!---Tôi đã đọc và đồng ý với điều khoản <a class="fancybox" href="http://localhost/camerano1/information/information/agree.html?information_id=3" alt="Privacy Policy"><b>Privacy Policy</b></a>                        <input type="checkbox" name="agree" value="1">
         &nbsp;-->
         <input type="button" onclick="check_register()" value="Tiếp tục" id="submit_form" class="btn btn-outline">
      </div>
   </div>
</form>
<script> 

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    // alert( pattern.test(emailAddress) );
    return pattern.test(emailAddress);
}; 
var data = '';
var milisec=0 
var seconds=3 
document.counter.d2.value='3' 
function check_all(email, captcha, pass1, pass2){
  $.ajax({
      type: 'POST',
      url: root + 'user/check_all',
      data: 'email=' + email + '&captcha=' + captcha + '&pass1=' + pass1 + '&pass2=' + pass2,
      success: function (value) {
        var message_error = '';
        var error = false;
        var obj = JSON.parse(value);
          if(obj[1] == 1){
            error = true;
            message_error += "- Email này đã được sử dụng.\n";
          }
          if(obj[2] == 0){
            error = true;
            message_error += "- Mã bảo vệ sai.\n";
          }
          if(obj[3] == 0){
            error = true;
            message_error += "- Mật khẩu nhập lại sai.\n";
          }
          if (error == true) {
              swal(message_error,"", "error");
              return false;
          } else {
            submit_register();
          }
      }
  });
  
}

function check_register(){
    var error = false;
    var message_error = ''; 
    
    if (!isValidEmailAddress($("#email").val())) {
        error = true;
        message_error += "- Email hợp lệ.\n";
    }
    if ($("#name").val().length < 4) {
        error = true;
        message_error += "- Tên (>4 ký tự).\n";
    }
    if ($("#phone").val().length < 8) {
        error = true;
        message_error += "- Số điện thoại (>8 ký tự).\n";
    }
    
    if ($("#address1").val().length < 5) {
        error = true;
        message_error += "- Địa chỉ (>5 ký tự).\n";
    }
    if ($("#password1").val().length < 5) {
        error = true;
        message_error += "- Mật khẩu (>5 ký tự).\n";
    }
    if ($("#password2").val().length < 5) {
        error = true;
        message_error += "- Nhập lại mật khẩu (>5 ký tự).\n";
    }
    
    // end check
    if (error == true) {
        swal("Vui lòng nhập: \n"+message_error, "");
        
        return false;
    } else {
        check_all($("#email").val(),$("#captcha-form").val(),$("#password1").val(),$("#password2").val());
    }
    
}

function display(){ 
 /*if (milisec<=0){ 
    milisec=9 
    seconds-=1 
 } */
 if (seconds<=-1){ 
    milisec=0 
    seconds+=1 
 } 
 else 
    //milisec-=1 
    var a=seconds;
    //+"."+milisec 
    setTimeout("display()",100);
    return a;
} 

function Redirect() {
   window.location = root + 'tai-khoan/login';
}
            
function submit_register() {
  $.ajax({
    type: 'POST',
    url: root + 'user/register',
    data: $('#frm_add_edit').serialize(),
    success: function (value) {
      if (value == 1) {
        swal("Tạo tài khoản thành công! Chúc bạn mua sắm vui vẻ ","", "success");
        setTimeout('Redirect()', 3000);
      } else {
        swal("Không thể tạo tài khoản! Vui lòng liên hệ với bộ phận kỹ thuật", "", "error");
      }
    }
  });
}

function validate(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}



</script>
<h2>Danh sách yêu thích</h2>
<table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td class="text-center">Hình ảnh</td>
            <td class="text-left">Tên sản phẩm</td>  
            <td class="text-right">Đơn giá</td>
            <td class="text-right">Thao tác</td>
          </tr>
        </thead>
        <tbody>
        
            <?php 
            foreach($content_wish_list['pageList'] as $k=>$v){
                ?>
                <tr>
                    <td class="text-center">              
                        <a href="<?php echo $SEO->build_link($v,"sanpham")?>"><img width="100px" height="100px" src="<?php echo $v->image;?>" alt="<?php echo $v->name?>" title="<?php echo $v->name?>"/></a>
                    </td>
                    <td class="text-left"><a href="<?php echo $SEO->build_link($v,"sanpham")?>"><?php echo $v->name?></a></td>
                    <td class="text-right">               
                        <div class="price">
                            <?php
                            if($v->price >= $v->price_sale){
                                ?>
                                <b><?php echo number_format($v->price); ?>VNĐ</b> 
                                <?php
                            }else{ 
                                ?>
                                <b><?php echo number_format($v->price_sale); ?>VNĐ</b> 
                                <s><?php echo number_format($v->price); ?>VNĐ</s>
                                <?php
                            } 
                            ?> 
                        </div>
                    </td>
                    <td class="text-right"> 
                      <a href="<?php echo base_url('tai-khoan/wishlist?del='.$v->id); ?>" data-toggle="tooltip" onclick="delete_wish('');" title="" class="btn btn-danger" data-original-title="Loại bỏ"><i class="fa fa-times">Loại bỏ</i></a>
                    </td>
                </tr>
                <?php
            } 
            ?>
            
          
           
        </tbody>
</table>
<?php  echo $content_wish_list['paging']; ?>
<div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo base_url(); ?>" class="btn btn-outline">Tiếp tục</a></div>
</div>
  
﻿<h1>Quên mật khẩu</h1>
<form action="" method="post" id="forgotten" enctype="multipart/form-data" class="form-horizontal">
   <fieldset>
      <legend>Số điện thoại đã đăng ký</legend>
      <div class="form-group required">
         <label class="col-sm-2 control-label" for="input-password">Email:</label>
         <div class="col-sm-10">
            <input type="text" name="email" id="email" value="" placeholder="Email:" class="form-control">
         </div>
      </div>
	  <legend>Số điện thoại đã đăng ký</legend>
      <div class="form-group required">
         <label class="col-sm-2 control-label" for="input-password">Số điện thoại:</label>
         <div class="col-sm-10">
            <input type="text" name="phone" id="phone" value="" placeholder="Số điện thoại:" class="form-control">
         </div>
      </div>
   </fieldset>
   <div class="buttons clearfix">
      <div class="pull-left"><a href="<?php echo base_url(); ?>tai-khoan/account" class="btn btn-default">Quay lại</a></div>
      <div class="pull-right">
         <input type="button" onclick="check_input()" value="Tiếp tục" class="btn btn-outline">
      </div>
   </div>
</form>
<script>
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    // alert( pattern.test(emailAddress) );
    return pattern.test(emailAddress);
}; 
function check_all(email, phone){
  $.ajax({
      type: 'POST',
      url: root + 'user/check_all',
      data: 'email=' + email + '&phone=' + phone,
      success: function (value) {
        var message_error = '';
        var error = false;
        var obj = JSON.parse(value);
          if(obj['forgotten'] == 0){
            error = true;
            message_error += "- Bạn đã nhập sai Email hoặc Số điện thoại.\n";
          }
          if (error == true) {
              swal(message_error,"", "error");
              return false;
          } else {
            submit_edit(email,phone,obj['id']);
          }
      }
  });
}

function check_input(){
    var error = false;
    var message_error = ''; 
    
    if (!isValidEmailAddress($("#email").val())) {
        error = true;
        message_error += "- Email hợp lệ.\n";
    }
    if ($("#phone").val().length < 6) {
        error = true;
		message_error += "- Số điện thoại (>8 ký tự).\n";
    }
    // end check
    if (error == true) {
        swal(message_error, "");
        
        return false;
    } else {
        check_all($("#email").val(),$("#phone").val());
    }
    
}

function submit_edit(email,phone,id) {
   $.ajax({
    type: 'POST',
    url: root + 'user/forgotten',
    data: 'email=' + email + '&phone=' + phone + '&id=' + id,
    success: function (value) {
	  var obj = JSON.parse(value);
      if (obj['id'] == 1) {
        swal("Mật khẩu lấy lại thành công. Mật khẩu mới của bạn là:\n"+obj['pass'],"", "success");
      } else {
        swal("Không thực hiện được bạn vui lòng liên hệ với bộ phận kỹ thuật !!!.", "", "error");
      }
    }
  });
}
</script>
<h1>Thay đổi mật khẩu</h1>
<form action="" method="post" id="changepass" enctype="multipart/form-data" class="form-horizontal">
   <fieldset>
      <legend>Mật khẩu</legend>
      <div class="form-group required">
         <label class="col-sm-2 control-label" for="input-password">Mật Khẩu cũ:</label>
         <div class="col-sm-10">
            <input type="password" name="pass_cu" id="pass_cu" value="" placeholder="Mật Khẩu cũ:" id="input-password" class="form-control">
         </div>
      </div>
      <div class="form-group required">
         <label class="col-sm-2 control-label" for="input-password">Mật Khẩu mới:</label>
         <div class="col-sm-10">
            <input type="password" name="pass_1" id="pass_1" value="" placeholder="Mật Khẩu mới:" id="input-password" class="form-control">
         </div>
      </div>
      <div class="form-group required">
         <label class="col-sm-2 control-label" for="input-confirm">Nhập Lại Mật Khẩu:</label>
         <div class="col-sm-10">
            <input type="password" name="pass_2" id="pass_2" value="" placeholder="Nhập Lại Mật Khẩu:" id="input-confirm" class="form-control">
         </div>
      </div>
   </fieldset>
   <div class="buttons clearfix">
      <div class="pull-left"><a href="<?php echo base_url(); ?>tai-khoan/account" class="btn btn-default">Quay lại</a></div>
      <div class="pull-right">
         <input type="button" onclick="check_input()" value="Tiếp tục" class="btn btn-outline">
      </div>
   </div>
</form>
<script>
function Redirect(user) {
   window.location = root + 'tai-khoan/account';
}

function check_all(pass_cu, pass_1, pass_2){
  $.ajax({
      type: 'POST',
      url: root + 'user/check_all',
      data: 'pass_cu=' + pass_cu + '&pass_1=' + pass_1 + '&pass_2=' + pass_2,
      success: function (value) {
        var message_error = '';
        var error = false;
        var obj = JSON.parse(value);
          if(obj['pass_cu'] == 0){
            error = true;
            message_error += "- Mật khẩu cũ nhập sai.\n";
          }
          if(obj['pass_new'] == 0){
            error = true;
            message_error += "- Mật khẩu nhập lại sai.\n";
          }
          if (error == true) {
              swal(message_error,"", "error");
              return false;
          } else {
            submit_edit();
          }
      }
  });
}

function check_input(){
    var error = false;
    var message_error = '- Vui lòng nhập mật khẩu hơn 6 ký tự'; 
    
    if ($("#pass_cu").val().length < 6) {
        error = true;
    }
    if ($("#pass_1").val().length < 6) {
        error = true;
    }
    
    if ($("#pass_2").val().length < 6) {
        error = true;
    }
    
    // end check
    if (error == true) {
        swal(message_error, "");
        
        return false;
    } else {
        check_all($("#pass_cu").val(),$("#pass_1").val(),$("#pass_2").val());
    }
    
}

function submit_edit() {
   check_all();
   $.ajax({
    type: 'POST',
    url: root + 'user/changepass',
    data: $('#changepass').serialize(),
    success: function (value) {
      if (value == 1) {
        swal("Sửa thành công! Bạn vui lòng chờ giây lát ","", "success");
        setTimeout('Redirect()', 2000);
      } else {
        swal("Không sửa được! vui lòng liên hệ tới bộ phận kỹ thuật.", "", "error");
      }
    }
  });
}
</script>
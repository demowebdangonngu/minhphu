 
 
   <h2>Thông tin đơn hàng</h2>
   <table class="table table-bordered table-hover">
      <thead>
         <tr>
            <td class="text-left" colspan="2">Chi tiết đơn hàng</td>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td class="text-left">              
               <b>Mã đơn hàng:</b> #<?php echo $detail[0]->oid; ?><br>
               <b>Ngày tạo:</b> <?php echo $detail[0]->order_date; ?>
            </td>
            <td class="text-left">              
               <b>Phương thức thanh toán:</b> Thu tiền khi giao hàng<br>
               <b>Phương thức vận chuyển:</b> Phí vận chuyển cố định              
            </td>
         </tr>
      </tbody>
   </table>
   <table class="table table-bordered table-hover">
      <thead>
         <tr>
            <td class="text-left">Địa chỉ thanh toán</td>
            <td class="text-left">Địa chỉ giao hàng</td>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td class="text-left"><?php echo $detail[0]->mname; ?><br><?php echo $detail[0]->maddress; ?></td>
            <td class="text-left"><?php echo $detail[0]->mname; ?><br><?php echo $detail[0]->address; ?></td>
         </tr>
      </tbody>
   </table>
   <div class="table-responsive">
      <table class="table table-bordered table-hover">
         <thead>
            <tr>
               <td class="text-left">Tên sản phẩm</td>
               <td class="text-left">Mã hàng</td>
               <td class="text-right">Số lượng</td>
               <td class="text-right">Đơn Giá</td>
               <td class="text-right">Tổng Cộng</td>
               <td style="width: 20px;"></td>
            </tr>
         </thead>
         <tbody>

            <?php
            foreach($list_product_detail as $k=>$v){
                  ?>
                     <tr>
                        <td class="text-left"><?php echo $v->name; ?></td>
                        <td class="text-left"><?php echo $v->code; ?></td>
                        <td class="text-right"><?php echo $v->quantity; ?></td>
                        <td class="text-right"><?php echo number_format($v->price); ?>VNĐ</td>
                        <td class="text-right"><?php echo number_format($v->price*$v->quantity); ?>VNĐ</td>
                        <td class="text-right" style="white-space: nowrap;">                
                           <a href="javascript:void(0)" onclick="add_cart_item('<?php echo $v->id; ?>' , 1);" data-toggle="tooltip" title="" class="btn btn-outline" data-original-title="Đặt hàng lại"><i class="fa fa-shopping-cart"></i></a>
                           
                        </td>
                     </tr>
                  <?php
            }
            ?> 
         </tbody>
         <tfoot>
            <tr>
               <td colspan="3"></td>
               <td class="text-right"><b>Thành tiền:</b></td>
               <td class="text-right"><?php echo number_format($detail[0]->total_price); ?>VNĐ</td>
               <td></td>
            </tr> 
            <tr>
               <td colspan="3"></td>
               <td class="text-right"><b>Tổng cộng :</b></td>
               <td class="text-right"><?php echo number_format($detail[0]->total_price); ?>VNĐ</td>
               <td></td>
            </tr>
         </tfoot>
      </table>
   </div>
   <h3>Lịch sử đơn hàng</h3>
   <table class="table table-bordered table-hover">
      <thead>
         <tr>
            <td class="text-left">Ngày tạo</td>
            <td class="text-left">Tình trạng</td> 
         </tr>
      </thead>
      <tbody>
         <tr>
            <td class="text-left"><?php echo $detail[0]->order_date; ?></td>
            <td class="text-left"><?php echo OrderTypeU($detail[0]->ostatus); ?></td> 
         </tr>
      </tbody>
   </table>
<div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo base_url(); ?>" class="btn btn-outline">Tiếp tục</a></div>
</div>
 

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  
require_once APPPATH."third_party/YT/ControllerPublic.php";
class cartorder extends YT_ControllerPublic { 
    function __construct()	{ 
    		parent::__construct();		 
    		$this->model=$this->load->model('Model_cart');
            $this->load->model('product/model_product', 'SP' , true);
            $this->load->library('main/main');
   	}
    
   function order(){ 
        $cart_content = $this->cart->contents();
		// print_r($this->input->post('captcha' , true));exit;
		//if ($_SESSION['captcha'] == $this->input->post('captcha' , true)) {
		  if($this->session->userdata('member_id') &&  $this->session->userdata('member_id') > 0){
			echo "<script>swal('Đặt hàng thành công... Chúng tôi sẽ liên lạc lại ngay sau khi nhận được đơn hàng. Xin chân thành cảm ơn!', 'success');</script>";
			if(is_array($cart_content)){ 
				$data_post['mem_id'] = $this->session->userdata('member_id');
				$data_post['phone'] = '';
                $data_post['email'] = '';
				$data_post['name'] = '';
				$data_post['address'] = $this->session->userdata('member_address');
				$data_post['order_date'] = date("Y-m-d h:i:s" , time());
				$data_post['total_price'] = $this->cart->total(); 
				$id_hd = $this->model->insert_hd($data_post);
				if($id_hd){
					
				  
					foreach($cart_content as $k=>$v){
						$data_insert = array();
						$data_insert['order_id'] = $id_hd;
						$data_insert['product_id'] = $v['id'];
						$data_insert['quantity'] = $v['qty'];
						$data_insert['price'] = $v['price']; 
						$data_insert['color'] = $v['id_color']; 
						$data_insert['size'] = $v['id_size']; 
						$this->model->insert_cthd($data_insert); 
					} 
					$this->cart->destroy(); 
					$this->data['ordered'] = true; 
				} 
				$this->data['page_cart'] = $this->load->view('page_cart', $this->data , true); 
				$this->template->build('cartorder', $this->data);  
			}else{
				redirect(base_url());
			} 
		}else{
			redirect(base_url());
			//echo '<script language="javascript"> alert("Bạn đã nhập sai mã bảo vệ!") </script>';
    		//echo '<script>window.location.href="javascript:history.go(-1)"</script>';
			// echo '<script type="text/javascript">alert();</script>';
			// redirect("cartorder", 'refresh');
			// exit;
		}
		$this->template->build('cartorder', $this->data);
   }
     
   function index(){
        $cart_content = $this->cart->contents();
		// print_r($cart_content);exit;
        if(is_array($cart_content)){ 
            $this->data['count'] = count($cart_content);
            $this->data['cart_content'] = $cart_content;
            $this->data['total_price'] = $this->cart->format_number($this->cart->total());
        }
        
        $breadcumb = array(   
            array(
                'url' => 'cartorder',
                'name' => 'Giỏ hàng'
            )
        );  
        $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);  
        //echo '<pre>'; print_r($this->data['breadcumb']); exit;
        $this->data['page_cart'] = $this->load->view('page_cart', $this->data , true);
        $this->template->build('cartorder', $this->data); 
   }
    
   function page_cart(){
        $cart_content = $this->cart->contents();
        if(is_array($cart_content)){ 
            $this->data['count'] = count($cart_content);
            $this->data['cart_content'] = $cart_content;
            $this->data['total_price'] = $this->cart->format_number($this->cart->total()); 
        } 
        $this->load->view('page_cart', $this->data);
   } 
    
    
   function validate_add_cart_item(){ 
        $id = $this->input->post('product_id'); // Assign posted product_id to $id
        $cty = $this->input->post('quantity'); // Assign posted quantity to $cty  
		$id_color = $this->input->post('id_color');
		$id_size = $this->input->post('id_size');
		
        $where['product.id'] = $id;  
        $where['product.status'] = 1;  
        $result = $this->SP->getList($where);    
        $cart_content = $this->cart->contents(); 
        if(isset($result[0]->id)){ 
				$rowid = md5(md5($result[0]->id).md5($id_color).md5($id_size));
                if($cart_content[$rowid]){
					$data = array(
					   'rowid' => $rowid,
					   'qty'   => $cart_content[$rowid]['qty']+1
					); 
					// Update the cart with the new information
					$this->cart->update($data);
                    //upload
                    
                }else{ 
                    // Insert
                    $data = array(
                        'id'      => $result[0]->id,
                        'qty'     => 1,
                        'price'   => $result[0]->price_sale==0?$result[0]->price:$result[0]->price_sale,
                        'name'    => $result[0]->id,
						'id_color' => $id_color,
						'id_size' => $id_size,
						'code' => $result[0]->code_product,
                    );  
                    $this->cart->insert($data); 
                }  
                return TRUE; // Finally return TRUE 
        }else{
            // Nothing found! Return FALSE! 
            return FALSE;
        }
    }  
    function add_cart_item(){ 
   
        if($this->validate_add_cart_item() == TRUE){ 
            if($this->input->post('ajax') != '1'){
                //redirect('cart'); // If javascript is not enabled, reload the page with new data
            }else{
                ?>
                <div class="alert alert-success">
                    <i class="fa fa-check-circle"></i> 
                    Thành công: Đã thêm vào <a href="<?php echo base_url().'cartorder'; ?>">giỏ hàng</a>!<button type="button" class="close" data-dismiss="alert">×</button>
                </div>
                <?php 
            }
        }else{
            ?>
            <div class="alert alert-danger">
                <i class="fa fa-exclamation-circle"></i> 
                Sản phẩm không có trong kho!    <button type="button" class="close" data-dismiss="alert">×</button>
            </div> 
            <?php 
        } 
    } 
   function update_cart(){
        // Get the total number of items in cart
        $total = $this->cart->total_items(); 
        $item = $this->input->post('rowid');
        $qty = $this->input->post('qty'); 
        $cart_content = $this->cart->contents(); 
        if($cart_content[$item]){ 
            $data = array(
                   'rowid' => $item,
                   'qty'   => $qty
                ); 
            // Update the cart with the new information
            $this->cart->update($data);
            ?>
            <div class="alert alert-success">
                <i class="fa fa-check-circle"></i> 
                Thành công: Cập nhật thành công!<button type="button" class="close" data-dismiss="alert">×</button>
            </div>
            <?php 
        }else{
            ?>
            <div class="alert alert-danger">
                <i class="fa fa-exclamation-circle"></i> 
                Giỏ hàng rỗng!    <button type="button" class="close" data-dismiss="alert">×</button>
            </div>
            <?php
        } 
    }  
    function remove_cart(){
        $rowid = $this->input->post('rowid');
        $data = array(
               'rowid' => $rowid,
               'qty'   => 0
            ); 
            // Update the cart with the new information
            $this->cart->update($data); 
        ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle"></i> 
            Thành công: Xóa thành công!<button type="button" class="close" data-dismiss="alert">×</button>
        </div>
        <?php 
    } 
    
    
   function empty_cart(){
        $this->cart->destroy(); // Destroy all cart data 
        ?>
        <div class="alert alert-success">
            <i class="fa fa-check-circle"></i> 
            Thành công: Xóa giỏ hàng thành công!<button type="button" class="close" data-dismiss="alert">×</button>
        </div>
        <?php
    } 
    
    
   public function cart_right() {    
        $cart_content = $this->cart->contents(); 
        if(is_array($cart_content) > 0){ 
            $this->data['count'] = count($cart_content);
            $this->data['cart_content'] = $cart_content;
            $this->data['total_price'] = $this->cart->format_number($this->cart->total()); 
        } 
        $this->load->view('cart_right123', $this->data);
   } 
   public function cart_modal() {    
        $id = $this->input->post('product_id'); // Assign posted product_id to $id  
        $where['product.id'] = $id;  
        $where['product.status'] = 1;  
        $data['result'] = $this->SP->getList($where); 
     
        $this->load->view('cart_modal', $data);
   } 
   
   
    public function cart_top() {   
        $cart_content = $this->cart->contents();   
        
        if(is_array($cart_content) > 0){  
            $this->data['cart_content'] = $cart_content;
            $this->data['count'] = count($cart_content);  
            $this->data['total_price'] = $this->cart->format_number($this->cart->total()); 
        }  
        $this->load->view('cart_top', $this->data);
   } 
   
   
   public function email_order($email , $idCT){ 
        $this->load->helper('email'); 
        $email = urldecode($email);
    
        if (valid_email($email)){ 
            unset($where);
            if(intval($idCT) && $idCT > 0){
                $where['email'] = $email;
                $where['product.status'] = 1;
                $where['order_detail.order_id'] = $idCT;
                $this->data['listOrderCT'] = $this->model->get_list_cthd($where); 
                $this->template->build('list_order_ct', $this->data);
            }else{
                $where['email'] = $email;
                $this->data['listOrder'] = $this->model->get_list($where);
                $this->template->build('list_order', $this->data);
            } 
        }else{
            redirect(base_url());
        }
   }
   
}
 ?>
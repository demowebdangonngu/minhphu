
<div class="h25"></div>
<div class="row"> 
		
		<div class="col-lg-9">
			<div class="td-block-featured td-shopping-cart">
				<div class="block-featured-header">
					<h2 class="block-title">Giỏ hàng</h2>
				</div>
				<div class="td-block-container">
					<div class="block-cart">
					<?php
					if($count){
					?>
						<div class="box-cart">
						 <div class="product_in_order">
							<div class="title">
							   <div class="sp">Sản phẩm</div>
							   <div class="sl">Số lượng</div>
							   <div class="gt">Giá thành</div>
							   <div class="opt"></div>
							</div>
							<div class="prods-list">
							
								<?php 
                                $sp = $this->load->model('product/model_product');
                                foreach($cart_content as $k=>$v){
                                    $where['product.id'] = $v['name'];
                                    $rsp = $sp->getList($where);
									
									//get color
									$sp->getDB()->select('color.*, product_color.id as id_pc');
									$sp->getDB()->from('product_color');
									$sp->getDB()->join('color','color.id = product_color.id_color');
									$sp->getDB()->join('product','product.id = product_color.id_product');
									$sp->getDB()->where(array('id_color'=>$v['id_color'], 'id_product'=>$v['id']));
									$color = current($sp->getDB()->get()->result());
									//get size
									$sp->getDB()->from('product_size');
									$sp->getDB()->where('id',$v['id_size']);
									$size = current($sp->getDB()->get()->result());
									//get image
									$sp->getDB()->from('product_image');
									$sp->getDB()->where('id_pc',$color->id_pc);
									$image = current($sp->getDB()->get()->result());
                                    ?>
									
									
									<div class="product_box clearfix"> 
										  <div class="img_product sp">
											<div class="img"><a href="<?php echo $SEO->build_link($rsp[0],"sanpham")?>" title="<?php echo $rsp[0]->name; ?>" class="product-image"><img width="60" height="60" src="<?php echo   $image->image; ?>"></a>
											</div>
											<div class="name_attr">
											   <div class="name">
												  <h2 class="product-name"><a href="<?php echo $SEO->build_link($rsp[0],"sanpham")?>"><?php echo $rsp[0]->name; ?></a></h2>
											   </div>
												<div class="attr">
													<dl>
														 <dt>Màu sắc</dt>
														 <dd class="box-choose color">
															<span class="attri attr-color" style="padding: 0px;">
																<img src="<?php echo $color->image;?>" style="width: 26px;height: 26px;">
															</span>
														 </dd>
													</dl>
													<dl>
													<dt>Kích thước</dt>
													<dd class="box-choose color">
														<span class="attri attr-color"><?php echo $size->size;?></span>
													</dd>
												  </dl>
												</div>
											</div>
										 </div>
										 <div class="sl">
										 
								
										 
											<input maxlength="12" type="text" class="input-text cart-qty qty" title="Số lượng" id="quantity" size="4" value="<?php echo $v['qty']; ?>" name="qty"  onchange="change_qty(this.value , '<?php echo $k; ?>');">
											
										</div>
										<div class="gt"> <span class="price"><b><?php echo number_format($v['subtotal']); ?>&nbsp;đ</b></span> </div>
										 <div class="opt box-btn">
											<button type="submit" name="delete_cart_action" value="delete_product" title="Xoá" class="delete-prod btn btn-outline"  onclick="remove_cart('<?php echo $k; ?>');" ><span>Xoá</span></button>
										 </div> 
									</div> 
                                 <?php                  
                                } 
                                ?> 
							  
							 <!--product_box-->
							   <div class="cart-payment clearfix" id="cart-payment">
								  <ul class="cart-total">
									 <li>
										<div class="label basegrandtotal ">Tổng tiền:</div>
										<div class="price basegrandtotal red"><b><?php echo $total_price; ?>&nbsp;đ</b></div>
									 </li>
									  <li class="cart-total-btn">
										<a href="<?php echo base_url().'checkout'; ?>"  class="btn-pay" title="Thanh toán" type="button">Đặt hàng</a>
									  </li>
								  </ul>
							   </div>
							</div>
						 </div>
					  </div>
					  <!--box-cart-->   
					<?php
					   }else{ 
							if($ordered){
								?>
								<script>$(document).ready(function (e) {
								  swal("Đặt hàng thành công... Chúng tôi sẽ liên lạc lại ngay sau khi nhận được đơn hàng. Xin chân thành cảm ơn! ", "", "success");
								});</script>
								<p id="emptyCartWarning" class="warning">Đặt hàng thành công... Chúng tôi sẽ liên lạc lại ngay sau khi nhận được đơn hàng. Xin chân thành cảm ơn! </p>
								<?php
							}else{
								?>
								<script>$(document).ready(function (e) {
								  swal("Giỏ hàng rỗng.", "Error!","error");
								   
								});</script>
								<p id="emptyCartWarning" class="warning">Giỏ hàng rỗng.</p>
								<?php
							} 
					   }
					   ?>  
					</div>
	
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<?php echo Modules::run('right/sanpham_khuyenmai'); ?>
			<?php echo Modules::run('right/sanpham_banchay'); ?>
		</div>
	</div>
	
	
<script> 

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    // alert( pattern.test(emailAddress) );
    return pattern.test(emailAddress);
}; 
 
function submit_cart(){
    var error = false;
    var message_error = ''; 
    
    
    if (!isValidEmailAddress($("#email").val())) {
        error = true;
        message_error += "Email.\n";
    }
    if ($("#name").val() == '') {
        error = true;
        message_error += "Tên.\n";
    }
    if ($("#phone").val() == '') {
        error = true;
        message_error += "Số điện thoại.\n";
    }
    
    if ($("#address").val() == '') {
        error = true;
        message_error += "Địa chỉ.\n";
    }
    

    // end check
    if (error == true) {
        swal("Vui lòng nhập: "+message_error, "");
        return false;

    } else {
        return true;
    }
    
    
    
}
</script>
 
    <div type="button" data-toggle="dropdown" data-loading-text="Loading..." class="heading dropdown-toggle">
      <div class="cart-inner pull-right">
			<i class="icon icon-shopping-cart glyphicon glyphicon-shopping-cart"></i>
			<span class="add-cart-number">Giỏ hàng<span>(<i><?php echo $count?$count:0; ?></i>)</span></span> 
      </div>
   </div>
   <ul class="dropdown-menu pull-right content">
  
        <?php 
        if($count){
            ?>   
       
             
               <li>
                  <table class="table table-striped">
                     <tbody>
                     
                        <?php 
                        $sp = $this->load->model('product/model_product');
                        foreach($cart_content as $k=>$v){
                            $where['product.id'] = $v['name'];
                            $rsp = $sp->getList($where);
                            ?>
                            <tr>
                               <td class="text-center">                        
                                    <a href="<?php echo $SEO->build_link($rsp[0],"sanpham")?>">
                                        <img src="<?php echo $this->config->item("img_path")  . $rsp[0]->image; ?>" alt="<?php echo $rsp[0]->name; ?>" title="<?php echo $rsp[0]->name; ?>" class="img-thumbnail">
                                    </a>
                               </td>
                               <td class="text-left">
                                    <a href="<?php echo $SEO->build_link($rsp[0],"sanpham")?>"><?php echo $rsp[0]->name; ?></a>
                               </td>
                               <td class="text-right">x <?php echo $v['qty']; ?></td>
                               <td class="text-right"><?php echo number_format($v['price']); ?>VNĐ</td>
                               <td class="text-center"><button type="button" onclick="remove_cart('<?php echo $k; ?>');" title="Loại bỏ" class="btn btn-link btn-xs"><i class="fa fa-times"></i></button></td>
                            </tr>
                            
                             
                         <?php                  
                        } 
                        ?>  
                     </tbody>
                  </table>
               </li>
               <li>
                  <div>
                     <table class="table table-bordered">
                        <tbody>
                           <tr>
                              <td class="text-right"><strong>Thành tiền:</strong></td>
                              <td class="text-right"><?php echo $total_price; ?>VNĐ</td>
                           </tr>
                           <tr>
                              <td class="text-right"><strong>Tổng cộng :</strong></td>
                              <td class="text-right"><?php echo $total_price; ?>VNĐ</td>
                           </tr>
                        </tbody>
                     </table>
                     <p class="text-right">
                        <a href="<?php echo base_url()."cartorder"; ?>" class="btn btn-inverse">
                        Xem chi tiết giỏ hàng                    </a>&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo base_url()."cartorder"; ?>" class="btn btn-outline">
                        Thanh toán                    </a>
                     </p>
                  </div>
               </li> 
            <?php
        }else{
            ?>  
            <li>
            <p class="text-center empty">Không có sản phẩm trong giỏ hàng!</p>
            </li>
            <?php
        } 
        ?>
         
      
   </ul>
    
    
    
    
    
    
    
    
    
    
    
    
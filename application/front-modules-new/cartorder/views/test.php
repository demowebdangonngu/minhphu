﻿<script src="<?php echo $this->config->item('static_path') ?>themes/front_new/js/global.js" type="text/javascript"></script>
<script src="<?php echo $this->config->item('static_path') ?>themes/front_new/js/vi.js" type="text/javascript"></script>
<script type="text/javascript">
function sendcontact( num )
{
	var sendname = document.getElementById( 'name' );
	var sendemail = document.getElementById( 'email' );
	var sendcontent = document.getElementById( 'content' );
	var bt = document.getElementById( 'btn-send' );
	var fcon = document.getElementById( 'fcontact' );
	bt.disabled = true;
	
	if( sendname.value.length < 2 )
	{
		swal( nv_fullname );
		bt.disabled = false;
		return false;
	}
	
	if( ! nv_mailfilter.test( sendemail.value ) )
	{
		swal( nv_email );
		bt.disabled = false;
		return false;
	}
	
	if( sendcontent.value.length < 2 )
	{
		swal( nv_content );
		bt.disabled = false;
		return false;
	}
	
   return true;
}</script>
<div id="UpdatePanel1">
   <div class="main">
      <div class="container detail">
         <div class="row">
            <div class="col-md-12">
             
                <?php
             if($count){
                ?>
               <table class="table table-bordered">
                  <thead>
                     <tr class="row1">
                        <th class="title">Tên sản phẩm</th>
                        <th class="title" width="10%">Số lượng</th>
                        <th class="title" width="15%">Đơn giá</th>
                        <th class="title" width="15%">Thành tiền</th>
                        <th class="title" width="15%">Xóa sản phẩm</th>
                     </tr>
                  </thead>
                  <tbody> 
                    <?php 
                    $sp = $this->load->model('sanpham/model_sanpham');
                    foreach($cart_content as $k=>$v){
                        $where['product.id'] = $v['name'];
                        $rsp = $sp->getList($where);
                        ?>
                  
                     <tr class="row0">
                        <td align="center">
                           <span id="lblPName">
                                <a href="<?php echo $SEO->build_link($rsp[0],"sanpham")?>"><?php echo $rsp[0]->name; ?></a>
                                <br><br> 
                                <img height="120" border="0" alt="<?php echo $rsp[0]->name; ?>" src="<?php echo $this->config->item("img_path")  . $rsp[0]->image; ?>">
                           </span>
                        </td>
                        <td align="center">  
                           <input  type="text" value="<?php echo $v['qty']; ?>" id="quantity" onchange="change_qty(this.value , '<?php echo $k; ?>');" /> 
                        </td>
                        <td align="center">   
                           <span id="lblPrice" class="price-cart"><?php echo number_format($v['price']); ?></span>    VND                          
                        </td>
                        <td align="center">  
                           <span id="lblTotal" class="price-cart"><?php echo number_format($v['subtotal']); ?></span>     VND 
                        </td>
                        <td align="center">  
                           <a href="javascript:void(0)">    
                                <img   onclick="remove_cart('<?php echo $k; ?>');" src="<?php echo THEME_FRONT; ?>FrontEnd/img/publish_r.png" style="height:16px;width:16px;border-width:0px;">
                           </a>      
                        </td>
                     </tr>
                     <?php                  
                    } 
                    ?> 
                  </tbody>
               </table> 
               <div class="row">
                  <div class="update-cart"> 
                     <a style="color: #FFFFFF;" href="<?php echo base_url(); ?>" class="submit">Mua sản phẩm khác</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <span id="Label1">Tổng tiền:  <b><?php echo $total_price; ?> VND</b></span>
                     <ins><input type="button" onclick="remove_cart('<?php echo $k; ?>');" name="ctl00$ContentPlaceHolder1$ctl00$btnUpdate" value="Xóa giỏ hàng"  class="submit"></ins>
                  </div>
               </div> 
               <div id="pnl">
                  <div class="span11 r-form">
                     <b style="color:#FF1414">Nếu bạn ngại đặt hàng! Hãy gọi tới CSKH để được giúp đỡ.<br>
                     Hỗ trợ: <?php echo SDT; ?></b><br><br>
                     <b>Để mua hàng:</b> Quý khách vui lòng điền đầy đủ thông tin: <b>TÊN, SỐ ĐIỆN THOẠI, ĐỊA CHỈ, EMAIL</b> vào form đặt hàng dưới đây và kích vào nút đặt hàng, chúng tôi sẽ liên lạc lại ngay sau khi nhận được đơn hàng. Xin chân thành cảm ơn! <br>
                     <hr>
                  </div>
                  <?php
                  if(!empty($cart_content)){
                    ?>
                    <div class="cart-form">
                         <div class="span4 l-form">
                            <form action="<?php echo base_url()."cartorder/order"; ?>" method="post" onsubmit="return submit_cart();" onload="document.getElementById('captcha-form').focus()" id="fcontact" onsubmit="return sendcontact('6');"id="voucher">
                                <div class="booking-left">Họ và tên (*)</div>
                                <div class="booking-right">       
                                   <input name="name" type="text" id="name" class="form-control"> 
                                </div>
                                <div class="booking-left">Điện thoại (*)</div>
                                <div class="booking-right">
                                   <input name="phone" type="text" id="phone" class="form-control"> 
                                </div>
                                <div class="booking-left">Địa chỉ</div>
                                <div class="booking-right">              
                                   <input name="address" type="text" id="address" class="form-control">
                                </div>
                                <div class="booking-left">Email</div>
                                <div class="booking-right">      
                                   <input name="email" type="text" id="email" class="form-control"> 
                                </div>
								<img style="float:left" src="../captcha/captcha.php" id="captcha" /><br/>
                                <input type="text" style="font-size: 13px;width: 7%;" onclick="
									document.getElementById('captcha').src='<?php echo $this->config->item('static_path') ?>captcha/captcha.php?'+Math.random();
									document.getElementById('captcha-form').focus();"
									id="btn-send" class="submit" value="Đổi hình." readonly=""/><br><br/>

								Captcha
								<input type="text" name="captcha" id="captcha-form" autocomplete="off" /><br/>
                                <br>    
                                <div class="booking-submit">    
                                   <input type="submit"  value="Đặt hàng" id="btnSend" class="submit">            
                                </div> 
                            </form>
                         </div>
                      </div>
                    <?php
                  }
                  ?> 
               </div>
               <?php
               }else{ 
                    if($ordered){
                        ?>
                        <p id="emptyCartWarning" class="warning">Đặt hàng thành công... Chúng tôi sẽ liên lạc lại ngay sau khi nhận được đơn hàng. Xin chân thành cảm ơn! </p>
                        <?php
                    }else{
                        ?>
                        <p id="emptyCartWarning" class="warning">Giỏ hàng rỗng.</p>
                        <?php
                    } 
               }
               ?>
            </div>
         </div>
      </div>
   </div>
</div>
<script> 
function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    // alert( pattern.test(emailAddress) );
    return pattern.test(emailAddress);
}; 
 
function submit_cart(){
    var error = false;
    var message_error = ''; 
    
    
    if (!isValidEmailAddress($("#email").val())) {
        error = true;
        message_error += "Nhập Email.\n";
    }
    if ($("#name").val() == '') {
        error = true;
        message_error += "Nhập tên.\n";
    }
    if ($("#phone").val() == '') {
        error = true;
        message_error += "Nhập số điện thoại.\n";
    }
    
    if ($("#address").val() == '') {
        error = true;
        message_error += "Nhập địa chỉ.\n";
    }
    

    // end check
    if (error == true) {
        alert(message_error); 
        return false;

    } else {
        return true;
    }
    
    
    
}
</script>
 
 
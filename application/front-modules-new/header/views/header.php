<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="vi-VN" xml:lang="vi-VN">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<title>Trang chủ - Minh Phú Auto</title>
		
		<meta name="keywords" content="DVD xe hơi Winstone, phụ kiện xe hơi cao cấp Uncle, nệm ghế da xe hơi Wilson, phim kính cách nhiệt xe hơi ASWF, cách âm chống ồn xe hơi Vaber" />
		<meta name="description" content="Trung Tâm Phụ Kiện Ô Tô Chất Lượng Cao Minh Phú cung cấp sản phẩm Phụ Kiện xe hơi cao cấp và dịch vụ chu đáo - chuyên nghiệp" />
		
		<link rel="shortcut icon" href="<?php echo THEME_FRONT; ?>icon/favicon.ico" />
		<link rel="image_src" href="<?php echo THEME_FRONT; ?>icon/favicon.ico" />
		
		<link rel="apple-touch-icon" href="<?php echo THEME_FRONT; ?>icon/touch-icon-iphone.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo THEME_FRONT; ?>icon/touch-icon-ipad.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo THEME_FRONT; ?>icon/touch-icon-iphone4.png" />
		
		<meta http-equiv="content-script-type" content="text/javascript"/>
		
		<meta name="robots" content="noodp, noindex, nofollow" />
		
		<link href='http://fonts.googleapis.com/css?family=Roboto:500,300,700,400&subset=latin,vietnamese' rel='stylesheet' type='text/css'>
		
		<link type="text/css" rel="stylesheet" href="<?php echo THEME_FRONT; ?>css/font.css" />
		<link type="text/css" rel="stylesheet" href="<?php echo THEME_FRONT; ?>css/icon.css" />
		<link type="text/css" rel="stylesheet" href="<?php echo THEME_FRONT; ?>css/com.css" />
		<link type="text/css" rel="stylesheet" href="<?php echo THEME_FRONT; ?>css/tem.css" />
		<link type="text/css" rel="stylesheet" href="<?php echo THEME_FRONT; ?>css/css.css" />
		<link type="text/css" rel="stylesheet" href="<?php echo THEME_FRONT; ?>css/responsive.css" />
		
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<link rel="schema.DC" href="http://purl.org/dc/elements/1.1/">
		<meta name="DC.title" content="Minh Phú Auto">
		<meta name="DC.identifier" content="http://minhphuauto.com/">
		<meta name="DC.description" content="Trung Tâm Phụ Kiện Ô Tô Chất Lượng Cao Minh Phú cung cấp sản phẩm Phụ Kiện xe hơi cao cấp và dịch vụ chu đáo - chuyên nghiệp">
		<meta name="DC.subject" content="Trung Tâm Phụ Kiện Ô Tô Chất Lượng Cao Minh Phú cung cấp sản phẩm Phụ Kiện xe hơi cao cấp và dịch vụ chu đáo - chuyên nghiệp">
		<meta name="DC.language" scheme="UTF-8" content="vi">
		
		<meta name="geo.region" content="VN" />
		<meta name="geo.placename" content="Hồ Chí Minh" />
		<meta name="geo.position" content="10.775214;106.671678" />
		<meta name="ICBM" content="10.775214, 106.671678" />
	</head>
	<body class="home_page" oncontextmenu="return false;">
		
		<!-- begin HEADER -->
		<div class="header"><div class="header_content">
			<div class="logo">
				<h1>
					<a class="link" href="1-home.html" title="Trang chủ">
						<img class="img" src="<?php echo THEME_FRONT; ?>images/logo.jpg" alt="Minh Phú Auto" />
					</a>
				</h1>
			</div>
			
			<div class="search_tablet">
				<input class="input" type="text" placeholder="TÌM KIẾM" />
				<a class="button">
					<img class="img" src="<?php echo THEME_FRONT; ?>images/search_button.gif" />
				</a>
			</div>
			
			<div class="menu_tablet">
				TRANG CHỦ
				<div class="sub">
					<a class="link link_active" href="1-home.html">
						TRANG CHỦ
					</a>
					<a class="link" href="2-about.html">
						GIỚI THIỆU
					</a>
					<a class="link" href="3-product.html">
						SẢN PHẨM
					</a>
					<a class="link" href="4-video.html">
						VIDEO
					</a>
					<a class="link" href="5-advisory.html">
						GÓC TƯ VẤN
					</a>
					<a class="link" href="6-news.html">
						TIN TỨC
					</a>
					<a class="link" href="7-agency.html">
						ĐẠI LÝ
					</a>
					<a class="link" href="8-contact.html">
						LIÊN HỆ
					</a>
				</div>
			</div>
			
			<div class="slogan_hotline">
				<div class="slogan">
					CHUYÊN PHỤ KIỆN ÔTÔ CHẤT LƯỢNG CAO
				</div>
				<div class="hotline">
					Hotline: 0946.350.968
				</div>
			</div>
			
			<div class="clear"></div>
		</div></div>
		<!-- end HEADER -->
		
		<!-- begin MENU_SEARCH -->
		<div class="menu_search"><div class="menu_search_content">
			
			<div class="menu">
			
				<div class="each">
					<a class="link link_active" href="1-home.html">
						TRANG CHỦ
					</a>
				</div>
				<div class="each">
					<a class="link" href="2-about.html">
						GIỚI THIỆU
					</a>
				</div>
				<div class="each">
					<a class="link" href="3-product.html">
						SẢN PHẨM
					</a>
				</div>
				<div class="each">
					<a class="link" href="4-video.html">
						VIDEO
					</a>
				</div>
				<div class="each">
					<a class="link" href="5-advisory.html">
						GÓC TƯ VẤN
					</a>
				</div>
				<div class="each">
					<a class="link" href="6-news.html">
						TIN TỨC
					</a>
				</div>
				<div class="each">
					<a class="link" href="7-agency.html">
						ĐẠI LÝ
					</a>
				</div>
				<div class="each">
					<a class="link" href="8-contact.html">
						LIÊN HỆ
					</a>
				</div>
				
			</div>
			
			<div class="search">
				<input class="input" type="text" placeholder="TÌM KIẾM" />
				<a class="button">
					<img class="img" src="<?php echo THEME_FRONT; ?>images/search_button.gif" />
				</a>
			</div>
			
			<div class="clear"></div>
			
		</div></div>
		
		<!-- end MENU_SEARCH -->
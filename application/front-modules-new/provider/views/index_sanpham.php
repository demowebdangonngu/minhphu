<div class="container">
 
   <?php  echo $breadcumb; ?>
   
   <div class="row">
      <aside id="sidebar-left" class="col-md-3">
         <column id="column-left" class="hidden-xs sidebar">
            <?php echo Modules::run('left/menuleft'); ?> 
             
            <?php echo Modules::run('left/sanpham_noibat'); ?> 
         </column>
      </aside>
      <section id="sidebar-main" class="col-md-9">
         <div id="content">
            <h1 class="heading-category"><?php echo $title; ?></h1>
 
            <!--
            <div class="box refine-search clearfix">
               <div class="box-heading">
                  <span>Lọc tìm kiếm</span>
               </div>
               <div class="category-list box-content clearfix ">
                  <div class="row">
                     <div class="col-sm-3">
                        <ul>
                           <li><a href="Camera-quan-sat/Vantech-c67c68.html">Vantech (13)</a></li>
                           <li><a href="Camera-quan-sat/Questek-c67c69.html">Questek (0)</a></li>
                        </ul>
                     </div>
                     <div class="col-sm-3">
                        <ul>
                           <li><a href="Camera-quan-sat/Vdtech-c67c70.html"> Vdtech (8)</a></li>
                           <li><a href="Camera-quan-sat/HDtech-c67c94.html"> HDtech (0)</a></li>
                        </ul>
                     </div>
                     <div class="col-sm-3">
                        <ul>
                           <li><a href="Camera-quan-sat/Sony-c67c71.html">Sony (9)</a></li>
                           <li><a href="Camera-quan-sat/samsung-c67c72.html">samsung (0)</a></li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            -->
            <div class="product-filter ">
               <div class="inner clearfix">
                  <div class="display">
                     <div class="btn-group group-switch">
                        <button type="button" id="list-view" class="btn btn-switch list" data-toggle="tooltip" title="" data-original-title="Danh sách">
                        <i class="fa fa-th-list"></i>
                        </button>
                        <button type="button" id="grid-view" class="btn btn-switch grid active" data-toggle="tooltip" title="" data-original-title="Lưới sản phẩm">
                        <i class="fa fa-th-large"></i>
                        </button>
                     </div>
                  </div>
                  <!--
                  <div class="filter-right">
                     <div class="product-compare pull-right"><a href="<?php echo base_url().'compare.html'; ?>" class="btn btn-default" id="compare-total">So sánh sản phẩm</a></div>
                   
                  </div>-->
               </div>
            </div>
            <?php echo $data_sanpham; ?>
            
         </div>
         
      </section>
      
   </div>
   
</div>
<footer id="footer">
    <?php echo Modules::run('footer'); ?>
</footer> 

<div class="row">
    <div class="col-lg-12">
      <div class="td-block-news">
        <div class="td-block-header">
          <h2 class="block-title">Tin tức</h2>
        </div>
          <div class="td-block-container">
            <div class="news-list owl-news">
			
				<?php
				foreach($category_layout4 as $k=>$v){					
					$link = trim(substr($v->image,  url_image(), 1000));
					?>
					<div class="item-news">
					<div class="thumbs-wrap">
					  <a href="<?php echo $SEO->build_link($v,"posts"); ?>" title="<?php echo $v->title; ?>">
						<img src="<?php echo base_url().'themes/upload/208x245/'.$link; ?>" class="img-responsive">
					  </a>
					</div>
					<h3><a href="<?php echo $SEO->build_link($v,"posts"); ?>"><?php echo $v->title; ?></a></h3>
				  </div> 
					<?php 
				}
				?> 
            </div>
        </div>
      </div>
    </div>
  </div>
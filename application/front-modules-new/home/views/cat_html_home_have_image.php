 

<div class="box pav-verticalcategorytabs clearfix box-purple">
      <div class="box-wapper box-purple clearfix">
          <div class="tab-nav tabs-x tabs-left">
              <div class="banner-image hidden-md hidden-sm hidden-xs">
                  <a class="img-banner" href="<?php echo $SEO->build_link($category_product_home,"category")?>" data-toggle="tab">
                  <img class="img-responsive" width="249" height="627" src="<?php echo $this->config->item("img_path") . $category_product_home->image_home; ?>" alt="<?php echo $category_product_home->name; ?>"/>					</a>
              </div>
              <ul class="nav nav-tabs " id="pav-categorytabs3" role="tablist">
                  <div class="nav-header box-heading">
                    <a style="color: white" href="<?php echo $SEO->build_link($category_product_home,"category")?>"><span><?php echo $category_product_home->name; ?></span></a>
                  </div><!-- end div.box-heading --> 
                  <?php
                  foreach($category_product_home->list_child  as $k=>$v){
                        $dem = $k==0?'active':'';
                        echo '<li class="'.$dem.'"><a href="#tab3-cat'.$v->id.'" role="tab" data-toggle="tab">'.$v->name.'</a></li>';  
                  } 
                  ?> 
                  
              </ul>
          </div><!-- end div.tab-nav -->
          <div class="tab-content">
          
                <?php
              foreach($category_product_home->list_child  as $k=>$v){
                    ?>
                    <div class="tab-pane <?php echo $k==0?'active':''; ?> " id="tab3-cat<?php echo $v->id; ?>">
                      <div class="carousel-inner carousel3 slide" id="list3-cat<?php echo $v->id; ?>">
                          <div class="item active">
                              <div class="row products-row "> 
                                    <?php
                                    foreach($v->list_product as $k1=>$v1){
                                        echo '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 product-col">';
                                        echo $this->main->tooltip($v1);
                                        echo '</div>'; 
                                    }
                                    ?>    
                              </div>																			
                          </div>
                      </div><!-- end div.carousel-content-->
                  </div><!-- end div.tab-panel-->
                   
                    <?php  
              } 
              ?>  
           
               
          </div><!-- end div.tab-content -->
      </div><!-- end div.box-wapper -->
  </div><!-- end div.box -->
 
          <div class="box pav-product-tab  tabs-group  clearfix">
              <div class="module-desc">
              </div>
              <a class="banner effect  nopadding out-variant" href="<?php echo $SEO->build_link($category_product_home,"category")?>" data-toggle="tab">
              </a>
              <div class="tab-heading box-heading clearfix">
                  <div class="tab-header nopadding clearfix">
                      <div class="nav-header navbar-brand"><a style="color: white" href="<?php echo $SEO->build_link($category_product_home,"category")?>"><?php echo $category_product_home->name; ?></div></a>
                      <ul class="nav nav-pills right" id="pav-categorytabs3">
                          <?php
						  
                          foreach($category_product_home->list_child  as $k=>$v){
                                echo '<li><a href="#tab3-cat'.$v->id.'" role="tab" data-toggle="tab">'.$v->name.'</a></li>';  
                          } 
                          ?> 
                      </ul>
                  </div><!-- end div.tab-heading -->
              </div><!-- end div.box-heading -->
              <a class="banner effect nopadding in-variant" href="#" data-toggle="tab">
              </a>
              <!-- end a.banner -->
              <div class="box-content tab-content nopadding">
                  <div class="tab-content ">
                  
                      <?php
					  
                      foreach($category_product_home->list_child  as $k=>$v){
                            ?>
							
                            <div class="tab-pane <?php echo $k==0?'active':''; ?> box-products clearfix" id="tab3-cat<?php echo $v->id; ?>">
                                  <div class="carousel-inner carousel3 slide" id="list3-cat<?php echo $v->id; ?>">
                                      <div class="item active">
                                          <div class="row products-row">     
                                                <?php
												
                                                foreach($v->list_product as $k1=>$v1){
                                                    echo '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 product-col">';
                                                    echo $this->main->tooltip($v1);
                                                    echo '</div>'; 
                                                }
                                                ?>            
                                          </div> 
                                      </div><!-- end div.carousel-content-->
                                  </div><!-- end div.tab-panel-->
                              </div><!-- end div.tab-content-inner -->
                            
                            <?php  
                      }
                      ?>  
                  </div><!-- end div.tab-content -->
              </div><!-- end div.box -->
              <script>
                 $(function() {
                 	$('.carousel3').carousel({interval:99999999999999,auto:false,pause:'hover'});
                 	$('#pav-categorytabs3 a:first').tab('show');
                 });
              </script>
              
              
        
              <script type="text/javascript"><!--
                 $('#pavcarousel2').carousel({interval:3000});
                 --></script>
          </div>
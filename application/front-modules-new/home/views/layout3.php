<?php 
$banner1 = array_shift($banner_mid);
$banner2 = array_shift($banner_mid);
?>
<div class="row">
    <div class="col-lg-3">
      <div class="td-block-ads ads-left"><a href="<?php echo $banner1->slug;?>"><img src="<?php echo $banner1->location;?>"/></a></div>
    </div>
    <div class="col-lg-6">
      <div class="td-block-products clearfix">
        <div class="td-block-header">
          <h2 class="block-title"></h2>
          <ul class="nav nav-tabs" role="tablist">
		  
		    <?php
			$c = 1;
			foreach($category_layout3 as $k=>$v){ 
				?>
				
				<li role="presentation"  <?php echo $c==1?'class="active"':''; ?>   ><a href="#tab-m-<?php echo $k; ?>" aria-controls="profile" role="tab" data-toggle="tab"><?php echo $v->name; ?></a></li>
				<?php 
				$c++;
			}
			?>
		  
          </ul>
        </div>
		
		
        <div class="td-block-container">
          <div class="tab-content">
		  
		    <?php
			$c = 1;
			foreach($category_layout3 as $k=>$v){ 
				?>
				<div role="tabpanel" class="tab-pane <?php echo $c==1?'active':''; ?>" id="tab-m-<?php echo $k; ?>">
				  <ul class="products-list products-list-2"> 
					<?php  
					foreach($v->list_product as $k1=>$v1){ 
						echo $this->main->tooltip($v1,'173x204'); 
					}
					?> 
				  </ul>
				</div> 
				<?php 
				$c++;
			}
			?>
		  
		   
           
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-3">
      <div class="td-block-ads ads-right"><a href="<?php echo $banner2->slug;?>"><img src="<?php echo $banner2->location;?>"/></a></div>
    </div>
  </div>
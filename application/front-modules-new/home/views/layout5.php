<!-- begin MAIN -->
		<div class="main"><div class="main_content">
			
			<div class="left">
				
				<!-- begin SLIDE -->
				<div class="slide" id="slider1_container">
					
					<!-- Slides Container -->
					<div class="slides" u="slides">
						<div><a href="3b-product-detail.html"><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/slide1.jpg" /></a></div>
						<div><a href="3b-product-detail.html"><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/slide2.jpg" /></a></div>
						<div><a href="3b-product-detail.html"><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/slide1.jpg" /></a></div>
						<div><a href="3b-product-detail.html"><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/slide2.jpg" /></a></div>
						<div><a href="3b-product-detail.html"><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/slide1.jpg" /></a></div>
						<div><a href="3b-product-detail.html"><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/slide2.jpg" /></a></div>
					</div>
					
					<!-- bullet navigator container -->
					<div u="navigator" class="jssorb10_m">
						<!-- bullet navigator item prototype -->
						<div u="prototype"></div>
					</div>
					<!--#endregion Bullet Navigator Skin End -->

					<!--#region Arrow Navigator Skin Begin -->
					<!-- Arrow Left -->
					<span u="arrowleft" class="jssora05l">
					</span>
					<!-- Arrow Right -->
					<span u="arrowright" class="jssora05r">
					</span>
					
				</div>
				<!-- end SLIDE -->
				
				<!-- begin LIST -->
				<div class="list mt-17">
					<div class="title">
						<a class="link" href="3a-product-list.html">
							DVD CAO CẤP THEO XE
						</a>
					</div>
					<div class="content">
						
						<!-- each -->
						<div class="each each_odd">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/OduTKR4v.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla Đầu DVD theo xe Toyota Corolla Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each2 each_even">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/product1.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each3 each_odd">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/product1.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each_even">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/product1.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each2 each_odd">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/product1.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each3 each_even">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/product1.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<div class="clear"></div>
					</div>
				</div>
				<!-- end LIST -->
				
				<!-- begin LIST -->
				<div class="list mt-17">
					<div class="title">
						<a class="link" href="3a-product-list.html">
							PHỤ KIỆN CAO CẤP THEO XE
						</a>
					</div>
					<div class="content">
						
						<!-- each -->
						<div class="each each_odd">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/product1.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each2 each_even">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/product1.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each3 each_odd">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/product1.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each_even">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/product1.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each2 each_odd">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/product1.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each3 each_even">
							<div class="image">
								<a class="link" href="3b-product-detail.html">
									<img class="img" src="<?php echo THEME_FRONT; ?>images/temp/product1.jpg" />
								</a>
							</div>
							<div class="new">
								<div class="label">MỚI</div>
							</div>
							<div class="each_title">
								<h2>
									<a class="link" href="3b-product-detail.html">Đầu DVD theo xe Toyota Corolla</a>
								</h2>
							</div>
							<div class="code">
								Mã sản phẩm: <span class="hl">Corolla</span>
							</div>
							<a class="more" href="3b-product-detail.html">
								Chi tiết
							</a>
						</div>
						<!-- each -->
						
						<div class="clear"></div>
					</div>
				</div>
				<!-- end LIST -->
				
			</div>
			
			<div class="right">
				
				<div class="block">
					<div class="title title_category">
						DANH MỤC
					</div>
					<div class="content content_category">
						
						<!-- each -->
						<div class="each">
							<h2>
								<a class="parent" href="3a-product-list.html">
									DVD CAO CẤP THEO XE
								</a>
							</h2>
							
							<div class="child_wrap">
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Honda
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link link_last" href="3a-product-list.html">
											Toyota
										</a>
									</h3>
								</div>
								<!-- each -->
								
							</div>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each">
							<h2>
								<a class="parent" href="3a-product-list.html">
									PHỤ KIỆN CAO CẤP THEO XE
								</a>
							</h2>
							
							<div class="child_wrap">
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Honda City
										</a>
									</h3>
									
									<div class="sub"><div class="sub_content">
										
										<!-- each -->
										<h3>
											<a class="sub_each" href="3a-product-list.html">
												Honda City 2014
											</a>
										</h3>
										<!-- each -->
										
										<!-- each -->
										<h3>
											<a class="sub_each" href="3a-product-list.html">
												Honda City 2015
											</a>
										</h3>
										<!-- each -->
										
										<!-- each -->
										<h3>
											<a class="sub_each sub_each_last" href="3a-product-list.html">
												Honda City 2016
											</a>
										</h3>
										<!-- each -->
										
									</div></div>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Honda Civic
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Honda Accord
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Toyota Altis
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Toyota Camry
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Toyota Vios
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Toyota Land Cruiser
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Toyota Land Prado
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Toyota Fortuner
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Toyota Fortuner
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Hyundai Santafe
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Kia Morning
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Ford Ecosport
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link link_last" href="3a-product-list.html">
											Mazda CX5
										</a>
									</h3>
								</div>
								<!-- each -->
								
							</div>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each">
							<h2>
								<a class="parent" href="3a-product-list.html">
									PHIM CÁCH NHIỆT MỸ
								</a>
							</h2>
							
							<div class="child_wrap">
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link" href="3a-product-list.html">
											Phim cách nhiệt nhà kính
										</a>
									</h3>
								</div>
								<!-- each -->
								
								<!-- each -->
								<div class="child">
									<h3>
										<a class="link link_last" href="3a-product-list.html">
											Phim cách nhiệt xe hơi
										</a>
									</h3>
								</div>
								<!-- each -->
								
							</div>
						</div>
						<!-- each -->
						
					</div>
				</div>
				
				<div class="block">
					<div class="title">
						HỖ TRỢ TRỰC TUYẾN
					</div>
					<div class="content content_online_support">
						
						<!-- each -->
						<div class="each each_chat">
							<div class="wrapclear">
								<div class="name">
									Tư vấn kỹ thuật
								</div>
								
								<div class="nick">
									
									<!-- each -->
									<a class="child">
										<img class="img" src="<?php echo THEME_FRONT; ?>images/main_right_skype.gif" />
									</a>
									<!-- each -->
									
									<!-- each -->
									<a class="child child_last">
										<img class="img" src="<?php echo THEME_FRONT; ?>images/main_right_yahoo.gif" />
									</a>
									<!-- each -->
									
								</div>
							</div>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each_hotline">
							<div class="wrapclear">
								<div class="name">
									Hotline
								</div>
								
								<div class="tel">
									0946.350.968
								</div>
							</div>
						</div>
						<!-- each -->
						
					</div>
				</div>
				
				<div class="block">
					<div class="title">
						KẾT NỐI VỚI CHÚNG TÔI
					</div>
					<div class="content content_social">
						
						Box facebook
						
					</div>
				</div>
				
				<div class="block block_last">
					<div class="title">
						LƯỢT TRUY CẬP
					</div>
					<div class="content content_statistic">
						
						<!-- each -->
						<div class="each each_user_online">
							<div class="wrapclear">
								<div class="name">
									Khách online
								</div>
								
								<div class="number">
									6
								</div>
							</div>
						</div>
						<!-- each -->
						
						<!-- each -->
						<div class="each each_history">
							<div class="wrapclear">
								<div class="name">
									Lượt truy cập
								</div>
								
								<div class="number">
									686868
								</div>
							</div>
						</div>
						<!-- each -->
						
					</div>
				</div>
				
			</div>
			
			<div class="clear"></div>	
		</div></div>
		<!-- end MAIN -->
		
		<!-- begin PARTNER -->
		<div class="partner"><div class="partner_content">
			<div class="slide" id="slider2_container">
				<div class="slides" u="slides">
					<div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner001.gif" /></div>
					<div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner002.gif" /></div>
					<div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner003.gif" /></div>
					<div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner004.gif" /></div>
					<div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner005.gif" /></div>
					<div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner001.gif" /></div>
					<div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner002.gif" /></div>
					<div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner003.gif" /></div>
					<div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner004.gif" /></div>
					<div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner005.gif" /></div>
				</div>
				
				<!--#region Arrow Navigator Skin Begin -->
				<!-- Arrow Left -->
				<span u="arrowleft" class="jssora09l">
				</span>
				<!-- Arrow Right -->
				<span u="arrowright" class="jssora09r">
				</span>
				<!--#endregion Arrow Navigator Skin End -->
			</div>
		</div></div>
		<!-- end PARTNER -->
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";
class Widget extends YT_ControllerPublic{
  public  $data=array();
  function contact(){
    $users = $this->load->model('user/user_model');
    $users->where(array('publish'=>1,'type !='=>1));
    $users->limit(6);
    $users->order_by('type');
    $this->data['users'] = $users->get()->result();
    $this->load->view("contact",$this->data);
  }

}

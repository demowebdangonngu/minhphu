<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/Model.php";

class model_pages extends YT_Model {

    public function __construct() {
        $this->table_name = "pages";
        parent::__construct();
    }

}

﻿<div class="container">
   <?php  echo $breadcumb; ?>
   <div class="row">
      <aside id="sidebar-left" class="col-md-3">
         <column id="column-left" class="hidden-xs sidebar">
            
            <?php echo Modules::run('left/menuleft'); ?> 
             
            <?php echo Modules::run('left/sanpham_noibat'); ?> 
         </column>
      </aside>

      <section class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
               <div id="content">
                  <h1 class="title-blogs"><?php echo $obj->name;?></h1>
                  <a class="rss-wrapper clearfix pull-right" href="category/rssab5e.html?id=40"><span class="fa fa-feed"></span></a>
                  <section class="pav-category wrapper clearfix clearfix blog-wrapper clearfix clearfix">
                     <div class="pav-blogs">
                        <div class="leading-blogs row">
                           <div class="col-lg-12 col-sm-12 col-xs-12">
                              <article class="blog-item clearfix">
                    
                                     <section class="description">
                                       <?php echo $obj->content;?>
                                    </section>
                                   
                                    <div class="addthis_toolbox addthis_default_style">
                                       <div class="fb-like" data-href="<?php echo current_url(); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                                    </div> 
                              </article>
                                       </div>
                        </div> 
                     </div>
                  </section>  

               </div>
      </section>

   </div>
</div>
<footer id="footer">
    <?php echo Modules::run('footer'); ?>
</footer> 
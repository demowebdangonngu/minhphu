<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH . "third_party/YT/ControllerPublic.php";

class Pages extends YT_ControllerPublic {

    public function __construct() {
        parent::__construct();
		$this->load->library('main/main', 'main');
    }

    function index() {
		
		$slug = $this->uri->segment(2);
		// print_r($slug);exit;
		 
		$pages = $this->load->model("model_pages");
		$pages->where(array("status"=>1,"slug"=>$slug));
		$this->data['obj'] = current($pages->get()->result()); 
		if($this->data['obj']){ 
	        $breadcumb = array( 
	            array(
	                'url' => $slug,
	                'name' => $this->data['obj']->name
	            )
	        ); 
	        $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);  
			$this->template->build('index', $this->data);
		}else{
			show_404();
		}
    }

}

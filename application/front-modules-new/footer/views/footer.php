<!-- begin FOOTER -->
    <div class="footer"><div class="footer_content">
      
      <div class="information">
        <div class="title">
          TRUNG TÂM PHỤ KIỆN Ô TÔ CHẤT LƯỢNG CAO MINH PHÚ
        </div>
        <div class="content">
          <div class="address">
            402 - 404 Cao Thắng Nối Dài, Phường 12, Quận 10, Tp. HCM
          </div>
          <div class="tel">
            0946 35 09 68
          </div>
          <div class="website">
            <a class="link" href="1-home.html">www.minhphuauto.com</a>
          </div>
          <div class="mail">
            <a class="link" href="mailto:khale@minhphuauto.com">khale@minhphuauto.com</a>
          </div>
        </div>
      </div>
      
      <div class="newsletter">
        <div class="title">
          ĐĂNG KÝ NHẬN THÔNG TIN
        </div>
        <div class="content">
          <input class="input" type="text" placeholder="Nhập địa chỉ email" />
          <a class="button">
            ĐĂNG KÝ
          </a>
          <div class="clear"></div>
        </div>
        <div class="social">
          <a class="each each_facebook"></a>
          <a class="each each_twitter"></a>
          <a class="each each_skype"></a>
          <a class="each each_google_plus"></a>
          <a class="each each_youtube"></a>
        </div>
      </div>
      
      <div class="map">
        <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=vi&amp;geocode=&amp;q=404+Cao+Th%E1%BA%AFng,+ph%C6%B0%E1%BB%9Dng+12,+H%E1%BB%93+Ch%C3%AD+Minh,+Vi%E1%BB%87t+Nam&amp;aq=0&amp;oq=404+Cao+Th%E1%BA%AFng+&amp;sll=37.0625,-95.677068&amp;sspn=48.240201,107.138672&amp;ie=UTF8&amp;hq=&amp;hnear=404+Cao+Th%E1%BA%AFng,+12,+H%E1%BB%93+Ch%C3%AD+Minh,+Vi%E1%BB%87t+Nam&amp;t=m&amp;ll=10.779011,106.673727&amp;spn=0.017496,0.02193&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>
      </div>
      
      <div class="clear"></div>
      
    </div></div>
    <!-- end FOOTER -->
    
    <!-- begin COPYRIGHT -->
    <div class="copyright">
      Copyright © 2010-2015 <span class="hl">Minh Phú Auto</span>, thiết kế và phát triển bởi <a class="link" title="Méo Services" href="http://meo.com.vn/" target="_blank">Méo Services</a>
    </div>
    <!-- begin COPYRIGHT -->
    
    <script src="<?php echo THEME_FRONT; ?>library/jquery/jquery-1.10.2.min.js"></script>
    
    <script src="<?php echo THEME_FRONT; ?>library/jquery-ui-1.10.4.custom/js/jquery-ui-1.10.4.custom.min.js"></script>
    
    <script src="<?php echo THEME_FRONT; ?>library/jquery/jquery.MTool.1.0.8.js"></script>
    <script src="<?php echo THEME_FRONT; ?>library/jquery/jquery.MTool.1.0.8.Language.vi.js"></script>
    
    <script src="<?php echo THEME_FRONT; ?>library/Jssor.Slider.FullPack/js/jssor.slider.mini.js"></script>
    
    <script src="<?php echo THEME_FRONT; ?>js/script.js"></script>
    
    <script>
      jQuery(document).ready(function ($) {
        var _SlideshowTransitions = [
          {$Duration:1800,x:1,y:0.2,$Delay:30,$Cols:10,$Rows:5,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Reverse:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2050,$Easing:{$Left:$JssorEasing$.$EaseInOutSine,$Top:$JssorEasing$.$EaseOutWave,$Clip:$JssorEasing$.$EaseInOutQuad},$Outside:true,$Round:{$Top:1.3}},
          {$Duration:1800,x:1,y:0.2,$Delay:30,$Cols:10,$Rows:5,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$Reverse:true,$Formation:$JssorSlideshowFormations$.$FormationSwirl,$Assembly:2050,$Easing:{$Left:$JssorEasing$.$EaseInOutSine,$Top:$JssorEasing$.$EaseOutWave,$Clip:$JssorEasing$.$EaseInOutQuad},$Outside:true,$Round:{$Top:1.3}},
          {$Duration:1800,x:1,y:0.2,$Delay:30,$Cols:10,$Rows:5,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$Reverse:true,$Formation:$JssorSlideshowFormations$.$FormationStraightStairs,$Assembly:2050,$Easing:{$Left:$JssorEasing$.$EaseInOutSine,$Top:$JssorEasing$.$EaseOutWave,$Clip:$JssorEasing$.$EaseInOutQuad},$Outside:true,$Round:{$Top:1.3}},
          {$Duration:1800,x:1,y:0.2,$Delay:30,$Cols:10,$Rows:5,$Clip:15,$During:{$Left:[0.3,0.7],$Top:[0.3,0.7]},$SlideOut:true,$Reverse:true,$Formation:$JssorSlideshowFormations$.$FormationSwirl,$Assembly:2050,$Easing:{$Left:$JssorEasing$.$EaseInOutSine,$Top:$JssorEasing$.$EaseOutWave,$Clip:$JssorEasing$.$EaseInOutQuad},$Outside:true,$Round:{$Top:1.3}}
        ];

        var options = {
          $AutoPlay: true,
          
          $ArrowKeyNavigation: true,
          
          $DragOrientation: 3,
          
          $SlideshowOptions: {
            $Class: $JssorSlideshowRunner$,
            $Transitions: _SlideshowTransitions,
            $TransitionsOrder: 0,
            $ShowLink: true
          },

          
          $BulletNavigatorOptions: {
            $Class: $JssorBulletNavigator$,
            $ChanceToShow: 2,
            $AutoCenter: 1,
            $SpacingX: 7
          },
          
          $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$,
            $ChanceToShow: 2
          }
        };
        var jssor_slider1 = new $JssorSlider$('slider1_container', options);
        
        var options = {
          $FillMode: 5,
          
          $AutoPlay: true,
          $AutoPlaySteps: 1,
          
          $ArrowKeyNavigation: true,
          
          $SlideWidth: 146,
          $SlideHeight: 134,
          $DisplayPieces: 8,
          
          $DragOrientation: 1,
          
          $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$,
            $ChanceToShow: 1,
            $AutoCenter: 2
          }
        };
        var jssor_slider2 = new $JssorSlider$('slider2_container', options);
        
        //responsive code begin
        //you can remove responsive code if you don't want the slider scales while window resizes
        function ScaleSlider() {
          var bodyWidth = $(".main .left").width();
          if (bodyWidth)
            jssor_slider1.$ScaleWidth(Math.min(bodyWidth, 1920));
          else
            window.setTimeout(ScaleSlider, 30);
            
          var bodyWidth = $(".partner_content").width();
          if (bodyWidth < 500) {
            /*
            jssor_slider2.$Pause();
            $("#slider2_container").html('<div class="slides" u="slides"><div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner001.gif" /></div><div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner002.gif" /></div><div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner001.gif" /></div><div><img u="image" src="<?php echo THEME_FRONT; ?>images/temp/partner002.gif" /></div></div>');
            
            options = {
              $FillMode: 5,
              
              $AutoPlay: true,
              $AutoPlaySteps: 1,
              
              $ArrowKeyNavigation: true,
              
              $SlideWidth: 146,
              $SlideHeight: 134,
              $DisplayPieces: 3,
              
              $DragOrientation: 1,
              
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$,
                $ChanceToShow: 1,
                $AutoCenter: 2
              }
            };
            jssor_slider2 = new $JssorSlider$('slider2_container', options);
            */
            jssor_slider2.$ScaleWidth(Math.min(bodyWidth, 1920));
          }
          else if (bodyWidth) {
            jssor_slider2.$ScaleWidth(Math.min(bodyWidth, 1920));
          }
          else
            window.setTimeout(ScaleSlider, 30);
        }
        ScaleSlider();

        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        //responsive code end
      });
    </script>
  </body>
</html>
<?php 
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";
class footer extends YT_ControllerPublic {  

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
         * 
	 */
    public function __construct() {
        parent::__construct();
        
    }
	public function index()
	{
            

           $this->model=$this->load->model('menu/model_menu'); 
  			 $this->data['provider'] = $this->model->provider_list();   
           $this->load->view(__CLASS__, $this->data); 
           //$this->template->build('welcome_message');
           
           
	}
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
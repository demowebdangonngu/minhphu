<?php echo Modules::run('menu/index2'); ?>
   <?php  echo $breadcumb; ?> 
	<div class="row">
		<div class="col-lg-9">
			<div class="post-news">
				<h1 class="post-news-title"><?php echo $obj->title;?></h1>
				<div class="post-news-body">
					<?php echo $obj->content;?>
				</div>
				<?php 
					$tags=$this->load->model('tags/tags_model');
					$list_tags=$tags->get_tag_by_object($obj->id);
					if(count($list_tags)){
				?>
				<div class="post-news-tags">Tags: 
					<?php foreach($list_tags as $t){?>
					<a href="<?php echo $this->config->item('base_url') ?>tim-kiem-tin/<?php echo $t->slug;?>" title="<?php echo $t->name;?>"><?php echo $t->name;?></a>
					<?php }?>
				</div>
				<?php }?>
			</div>
			<div class="td-block-featured">
				<div class="block-featured-header">
					<h2 class="block-title">Tin tức liên quan</h2>
				</div>
				<div class="td-block-container">
					<ul class="products-list">
						<?php  
						 foreach($related as $k=>$v){
							$link = trim(substr($v->image,  url_image(), 1000));
						?> 
						<li class="item">
							<div class="item-inner">
							<div class="thumbs-wrap">
								<a class="entry-thumbs" href="<?php echo $SEO->build_link($v,"posts");?>">
									<img src="<?php echo base_url().'themes/upload/199x235/'.$link; ?>" class="img-responsive">
								</a>
							</div>
							<div class="meta">
								<div class="entry-name"><a href="<?php echo $SEO->build_link($v,"posts");?>" title="<?php echo $v->title;?>"><?php echo cut_string($v->title,100);?></a></div>
							</div>
							</div>
						</li>
						<?php }?>
					</ul>
				</div>
			</div>
		</div>
	
		<div class="col-lg-3">
			<?php echo Modules::run('right/sanpham_khuyenmai'); ?>
			<?php echo Modules::run('right/sanpham_banchay'); ?> 
		</div>
	</div>
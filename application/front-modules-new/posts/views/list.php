<?php echo Modules::run('menu/index2'); ?>
   <?php  echo $breadcumb; ?> 
<div class="row">
	<div class="col-lg-9">
		<div class="td-block-news">
			<div class="td-block-header">
				<h2 class="block-title">Tin tức</h2>
			</div>
			<div class="td-block-container clearfix">
			<?php 
			if($posts){
				$item1 = array_shift($posts);
			?>
				<div class="box-feartures">
					<div class="thumbs-wrap">
						<a href="<?php echo $SEO->build_link($item1,"posts");?>"><img style="width:415px;height:289px" src="<?php echo $item1->image;?>"></a>
					</div>
					<h2 class="entry-title"><a href="<?php echo $SEO->build_link($item1,"posts");?>" title="<?php echo $item1->title;?>"><?php echo $item1->title;?></a></h2>
					<p class="entry-des"><?php echo $item1->description;?></p>
				</div>
				<div class="box-aside">
					<ul class="aside-list">
					<?php foreach($posts as $item){?>
						<li class="item">
							<div class="thumbs-wrap">
								<a href="<?php echo $SEO->build_link($item,"posts");?>"><img style="width:90px;height:106px" src="<?php echo $item->image;?>" class="img-responsive mCS_img_loaded"></a>
							</div>
							<h3 class="entry-title"><a href="<?php echo $SEO->build_link($item,"posts");?>" title="<?php echo $item->title;?>"><?php echo cut_string($item->title,180);?></a></h3>
						</li>
					<?php }?>
					</ul>
				</div>
			<?php }?>
			</div>
		</div>
		
		<div class="row">
			<?php 
			$this->model = $this->load->model('model_posts');
			foreach($categories as $cat){
				$post_cat = $this->model->getList(array('posts.status'=>1,'cat_id'=>$cat->id), array('per_page' => 4, 'start' => 0));
				if($post_cat){
			?>
			<div class="col-lg-6">
				<div class="td-block-news td-block-news-small">
					<div class="td-block-header">
						<h2 class="block-title"><?php echo $cat->name;?></h2>
					</div>
					<div class="td-block-container clearfix">
						<div class="box-aside">
							<ul class="aside-list">
							<?php 
							
							foreach($post_cat as $itemcat){
							?>
								<li class="item">
									<div class="thumbs-wrap">
										<a href="<?php echo $SEO->build_link($itemcat,"posts");?>"><img style="width:90px;height:106px" src="<?php echo $itemcat->image;?>" class="img-responsive mCS_img_loaded"></a>
									</div>
									<h3 class="entry-title"><a href="<?php echo $SEO->build_link($itemcat,"posts");?>" title="<?php echo $itemcat->title;?>"><?php echo cut_string($itemcat->title,38);?></a></h3>
									<p  class="entry-des"><?php echo cut_string($itemcat->description,120);?></p>
								</li>
							<?php }?>
							</ul>
						</div>
					</div>
				</div>	
			</div>
			<?php }}?>
		</div>
	</div>

	<div class="col-lg-3">
		<?php echo Modules::run('right/sanpham_khuyenmai'); ?>
		<?php echo Modules::run('right/sanpham_banchay'); ?> 
	</div>
</div>
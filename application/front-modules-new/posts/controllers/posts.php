<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";

class posts extends YT_ControllerPublic {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
         * 
	 */
    public function __construct() {
        parent::__construct();
        $this->model = $this->load->model('Model_posts');
        $this->load->library('main/main');
    }
	
	function index(){
		$this->data['posts'] = $this->model->getList(false,array('per_page' => 5, 'start' => 0));
		$this->model->getDB()->from('categories');
		$this->model->getDB()->where(array('status'=>1,'type'=>1));
		$this->data['categories'] = $this->model->getDB()->get()->result();
		
		$this->template->build('list', $this->data);
	}
	
    public function detail($id) {  
        $this->model->where(array('id' => $id));
        $obj = current($this->model->get()->result());
        if ($obj->id) { 
            $id = $obj->id;
            $this->data['obj'] = $obj; 
            $this->data['SEO']->build_meta($obj, "posts");
            
            $this->model->select("posts.*,categories.name as cat_name, categories.id as cat_id ,users.name as admin_name,categories.slug as cat_slug");
            $this->model->where(array("posts.id"=>$id));
            $this->model->join("users", "users.id=posts.admin_id" , 'LEFT');
            $this->model->join("categories", "categories.id=posts.cat_id" , 'LEFT');
            $this->data['tintuc'] = $this->model->get()->result(); 
            
             
             $breadcumb = array(  
                array(
                    'url' => 'loai/'.$this->data['tintuc'][0]->cat_slug,
                    'name' => $this->data['tintuc'][0]->cat_name
                ),
                array(
                    'url' => $this->data['SEO']->build_link($this->data['tintuc'][0],"posts",false,true),
                    'name' => $this->data['tintuc'][0]->title
                )
            );  
            $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);  
             
            
            $this->model->limit(6 , 0);
            $this->model->where(array("cat_id"=>$this->data['tintuc'][0]->cat_id));
            $this->data['related'] = $this->model->get()->result(); 
             
           // echo '<pre>'; print_r($this->data['tintuc']); exit;
            $this->template->build('detail', $this->data);
        }else{
            redirect(base_url());
        }
    }
    
    public function product_hot_home(){
        $this->data['product_hot_home'] = $this->model->getList(false , array('per_page' => 4, 'start' => 0) , false, false , true);
        $this->load->view('product_hot_home' , $this->data); 
    }
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
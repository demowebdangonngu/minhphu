<?php echo Modules::run('menu/index2'); ?>  
<div class="h25"></div>
<?php echo $breadcumb; ?>

    <div class="row">
       <section id="sidebar-main" class="col-md-12">
          <div id="content" class="wrapper clearfix">
             <h1>Thanh Toán</h1>
             <div class="panel-group" id="accordion">
                
                <div class="panel panel-default">
                   <div class="panel-heading">
                      <h4 class="panel-title"><a href="#collapse-checkout-confirm" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Bước 1: Đăng nhập tài khoản<i class="fa fa-caret-down"></i></a></h4>
                   </div>
                   <div class="panel-collapse collapse in" id="collapse-checkout-option" style="height: auto;">
                       <div class="panel-body">
                          <div class="row">
                             <div class="col-sm-6">
                                <h2>Khách Hàng Mới</h2>
                                <p>Tùy Chọn Thanh Toán:</p>
                                <div class="radio">
                                   <label>
                                   <input type="radio" name="account" value="register" checked="checked">
                                   Đăng kí tài khoản</label>
                                </div>
                                <p>Bằng việc tạo tài khoản bạn có thể mua sắm nhanh hơn, theo dõi trạng thái đơn hàng, và theo dõi đơn hàng mà bạn đã đặt.</p>
                                <a href="<?php echo base_url().'tai-khoan/register'; ?>"  id="button-account"  class="btn btn-primary">Tiếp tục</a>
                             </div>
                             <div class="col-sm-6">
                                <h2>Khách hàng cũ</h2>
                                 <p><strong>Tôi là khách hàng cũ</strong></p>
                                 <form action="" method="post" id="frm_login" enctype="multipart/form-data" class="form-horizontal">
                                    <div class="form-group">
                                       <label class="control-label" for="input-email">Địa chỉ E-Mail:</label>
                                       <input type="text" name="email" value="" placeholder="Địa chỉ E-Mail:" id="input-email" class="form-control">
                                    </div>
                                    <div class="form-group">
                                       <label class="control-label" for="input-password">Mật khẩu:</label>
                                       <input type="password" name="password" value="" placeholder="Mật khẩu:" id="input-password" class="form-control">
                                       <a href="<?php echo base_url("tai-khoan/forgotten"); ?>">Quên mật khẩu</a>
                                    </div>
                                    <input type="button" onclick="submit_login()" value="Đăng nhập" class="btn btn-outline">
                                 </form>
                             </div>
                          </div>
                       </div>
                    </div>
                </div>
                <div class="panel panel-default">
                   <div class="panel-heading">
                      <h4 class="panel-title">Bước 2: Địa Chỉ Giao Hàng </h4>
                   </div>
                   <div class="panel-collapse collapse" id="collapse-checkout-option">
                      <div class="panel-body"></div>
                   </div>
                </div> 
                <div class="panel panel-default">
                   <div class="panel-heading">
                      <h4 class="panel-title">Bước 3: Xác Nhận Đơn Hàng </h4>
                   </div>
                   <div class="panel-collapse collapse" id="collapse-checkout-option">
                      <div class="panel-body"></div>
                   </div>
                </div> 
                
                
             </div>
          </div>
       </section> 
    </div>

<script>

  var root = '<?php echo base_url(); ?>';

function Redirect(user) {
   window.location = root + 'checkout';
}

function submit_login() {
  $.ajax({
    type: 'POST',
    url: root + 'user/login',
    data: $('#frm_login').serialize(),
    success: function (value) {
      var obj = JSON.parse(value);
      if (obj[1] == 1) {
        swal("Đăng nhập thành công! Bạn vui lòng chờ giây lát ","", "success");
        setTimeout('Redirect()', 2000);
      } else {
        swal("Mật khẩu hoặc tài khoản nhập sai! vui lòng thử lại.", "", "error");
      }
    }
  });
}
</script>

<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
require_once APPPATH."third_party/YT/Model.php";
class Model_category extends YT_Model {

    public function __construct() {
        $this->table_name="categories";
        parent::__construct(); 
    }
    public $obj_array=array();
        //public $tree_array=array();
        public  $tree=array();
    
    
    function get_tree($status = array(0, 1, 2) , $where = false) {
        sort($status);
        $key = implode("", $status);
        if (count($this->tree[$key]))
            return $this->tree[$key];
        $this->getDB()->select("*");
        $this->getDB()->from($this->table_name);
        $this->getDB()->where_in('status', $status);
        if($where){
        	$this->getDB()->where($where);
        }


        $this->getDB()->order_by('order');
        $list = $this->getDB()->get()->result();
        
        foreach ($list as $cat) {
            $this->obj_array[$key][$cat->id] = $cat;
            //$this->tree_array[$cat->id]=$cat;
            $this->obj_array[$key][$cat->id]->children = array();
            if ($cat->parent == 0) {//root node
                $this->tree[$key][$cat->id] = $cat;
            }
        }
        
        foreach ($this->obj_array[$key] as $keys => $obj) {
            if ($this->obj_array[$key][$obj->parent]) {
                $this->obj_array[$key][$obj->parent]->children[$obj->id] = $obj;
            }
        }
         
        return $this->tree[$key]; 
    }
 
       
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";
class category extends YT_ControllerPublic {
 
    public function __construct() {
        parent::__construct();
        $this->load->model('product/model_product', 'SP' , true);
        $this->load->model('category/model_category' , 'CAT' , true); 
        $this->load->model('posts/model_posts', 'POSTS' , true);
		$this->load->model('menu/model_menu', 'MENU' , true);
        
        
        $this->load->library('main/main', 'main');
    }
	
	
	
	public function detailNoFound(){
		
		$currentUrl = current_url();
		$base_url = base_url();
		$arrUrl = explode($base_url , $currentUrl);
		 
		 
		// get page 
		$arrUrlSub = explode('/' , $arrUrl[1]); 
		$page = 1;
		if(is_numeric(end($arrUrlSub))){
			$page = end($arrUrlSub);
			$slug = substr($arrUrl[1],0,strrpos($arrUrl[1] , '/')); 
		}else{
			// Get slug
			$slug = $arrUrl[1]; 
		}
  
		$this->detail($slug , $page);
	}
	
	
	function getTrueSlug($slug){
		$arrUrlSub = explode('/' , $slug); 
		$order = "";
		switch (end($arrUrlSub)){
			case 'ban-chay': 
				$order = 'ban-chay';
				$arr = explode('/'.$order,$slug);
				$slug = $arr[0]; 
				break;
			case 'khuyen-mai': 
				$order = 'khuyen-mai';
				$arr = explode('/'.$order,$slug);
				$slug = $arr[0]; 
				break;
			case 'gia-tot':  
				$order = 'gia-tot';
				$arr = explode('/'.$order,$slug);
				$slug = $arr[0];
				break;
			case 'xem-nhieu': 
				$order = 'xem-nhieu';
				$arr = explode('/'.$order,$slug);
				$slug = $arr[0]; 
				break; 
		} 
		
		return array('slug'=>$slug , 'order'=>$order);
	}
  
    public function detail($slug = '', $page = 1) {  
        if (!is_numeric($page)) {
            $page = 1;
        }
        if ($slug == '') {
            redirect();
        }  
		$arrSlug = $this->getTrueSlug($slug);
		$slugTrue = $arrSlug['slug'];
		$order = $arrSlug['order'];
		
        $menu = $this->MENU->get_list(array('status' => 1 , 'link' => $slugTrue)); 
    
        $arrCatID = array();
        $pro_id = 0;
        $module = 'detail_main_cat'; 
        if(!empty($menu)){
            $arrCatID[] = $menu[0]->value; 
			$arrayName = array();  
			// Get category info
			if($menu[0]->of_value != 0 && $menu[0]->of_value != ''){
				$arrCatID[] = $menu[0]->of_value;
				$categorySub = $this->CAT->get_list(array('status' => 1 , 'id' => $menu[0]->of_value)); 
				if(empty($categorySub)){
					redirect();
				} 
				$arrayName[] = $categorySub[0]->name; 
			}
			  
			$categoryCurrent = $this->CAT->get_list(array('status' => 1 , 'id' => $menu[0]->value)); 
			if(empty($categoryCurrent)){
				redirect();
			} 
            $arrayName[] = $categoryCurrent[0]->name;  

			 
            if($categoryCurrent[0]->type == 2){    // San Pham
                $module = 'detail_main_cat';
            }else{                                  // Tin tuc
                $module = 'detail_main_post';
            } 
        } 
        if (empty($arrCatID)) {
            redirect();
        }  
        $this->$module($slug, $arrayName, $arrCatID, $pro_id,$page , $categoryCurrent);
    }
 
    private function detail_main_cat($slug, $arrayName ,$arrCatID = array(),$pro_id = 0,$page = 1 , $catObj) { 

		$this->CAT->where(array('status'=>1,'id'=>$cat_id));
		$parent = current($this->CAT->get()->result());
         
		
		 
		$arrSlug = $this->getTrueSlug($slug);
		$slugTrue = $arrSlug['slug'];
		$order = $arrSlug['order']; 
		 
		 
        $total = count($this->data['product']);
        $base_url = $slugTrue; 
      
		
		$where = array();
		$order_by = array(); 
		switch ($order){
			case 'ban-chay':
				$order_by["product.public_date"] = "DESC";
				break;
			case 'khuyen-mai':
				$order_by["product.price_sale"] = "DESC"; 
				$where['price_sale !='] = 0; 
				break;
			case 'gia-tot':
				$order_by["product.price"] = "DESC";
				break;
			case 'xem-nhieu':
				$order_by["product.public_date"] = "ASC";
				break;
			default:
				$order_by["product.public_date"] = "DESC"; 
		}
		$this->data['order'] = $order;
	 
		$uri_segment = 2;
		$lenSlug = explode('/' , $slug); 
		if ($lenSlug[2]){
			$uri_segment = 4;
		}elseif ($lenSlug[1]){
			$uri_segment = 3;
		}
		 
		$this->data['product']=$this->SP->get_product_pages(40,array('uri_segment'=>$uri_segment,'base'=>$slug),$arrCatID , false , $where, $order_by); 
        // breadcumb
		$breadcumb = array(); 
		foreach($arrayName as $k=>$v){
			$breadcumb[] = array(
						'url' => remove_accent($v),
						'name' => $v
					); 
		}
		
		
        
        $this->data['title'] = $arrayName[1]?$arrayName[1]:$arrayName[0];
        $this->data['image'] = $catObj[0]->image_page;
        $this->data['description'] = $catObj[0]->description;
        $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);  
		
		$this->data['base_url']  =  base_url($base_url);
		
		
        $this->data['SEO']->build_meta($catObj[0],'category_product');
        
        $this->data['data_sanpham'] = $this->load->view('data_sanpham' , $this->data , TRUE); 
        $this->template->build('index_sanpham', $this->data);    
    }
    
	
	/*
    private function detail_main_post($slug, $arrayName ,$cat_id = 0,$pro_id = 0,$page = 1 , $catObj) { 
		$this->CAT->where(array('status'=>1,'id'=>$cat_id));
		$parent = current($this->CAT->get()->result());
        
        $this->CAT->where(array('status'=>1,'parent'=>$cat_id));
        $hasChild = current($this->CAT->get()->result());

		if($parent->parent == 0 && isset($hasChild->id) ){
			$this->CAT->where(array('status'=>1,'parent'=>$cat_id)); 
		    $this->data['list_cate_post'] = $this->CAT->get()->result(); 
            $viewData = 'data_tintuc_parent';
            $viewIndex = 'index_tintuc_parent';
		}else{
		  
            $ids[] = $cat_id;
    	 
          
		    $total = count($this->data['product']);
            $base_url = $slug;
            $slug = $slug; 
			
			$uri_segment = 2;
			$lenSlug = explode('/' , $slug); 
			if ($lenSlug[1]){
				$uri_segment = 3;
			}
			
			
    		$this->data['posts']=$this->POSTS->get_posts_pages(2,array('uri_segment'=>$uri_segment,'base'=>$slug),$ids); 
            $viewData = 'data_tintuc_child';
            $viewIndex = 'index_tintuc_child';
		} 
 	    
        // breadcumb
		$breadcumb = array(); 
		foreach($arrayName as $k=>$v){
			$breadcumb[] = array(
						'url' => remove_accent($v),
						'name' => $v
					); 
		}
		
		
        $this->data['title'] = $arrayName[1]?$arrayName[1]:$arrayName[0];
        $this->data['image'] = $catObj[0]->image_page;
        $this->data['description'] = $catObj[0]->description; 
        $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);  
        $this->data['SEO']->build_meta($catObj[0],'category_post');

        $this->data[$viewData] = $this->load->view($viewData , $this->data , TRUE); 
        $this->template->build($viewIndex, $this->data);    
    }
    */
    
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<section class="pav-category wrapper clearfix clearfix blog-wrapper clearfix clearfix">
   <div class="pav-blogs">
      <div class="leading-blogs row">
         <div class="col-lg-12 col-sm-12 col-xs-12">
            <article class="blog-item clearfix">
                <section class="description">
                  <p><?php echo $description; ?></p>
               </section>
                <?php
                foreach($posts['pageList'] as $k=>$v ){
                    ?>
                    <div class=" pavcol2 info">
                      <header class="blog-header clearfix">
                        
                         
                         <h4 class="blog-title"><a href="<?php echo $SEO->build_link($v,"posts"); ?>" title="<?php echo $v->title; ?>"><?php echo $v->title; ?></a></h4>
                      </header>
                      <footer>
                         <section class="blog-meta">
                            <span class="author">
                            <span><i class="fa fa-pencil"></i>Viết bởi: </span> 
                            <span class="t-color"><?php echo $v->admin_name; ?></span>
                            </span>
                            <span class="publishin">
                            <span><i class="fa fa-thumb-tack"></i>Trong danh mục: </span>
                            <a   class="t-color" title="<?php echo $v->cat_name; ?>"><?php echo $v->cat_name; ?></a>
                            </span>
                            <span class="created"><span><i class="fa fa-calendar"></i>Ngày viết:  <?php $adad = explode(' ',$v->public_date); echo $adad[0]; ?></span></span>
                        
                         </section>
                         <section class="description">
                            <p><?php echo $v->description; ?></p>
                         </section>
                         <section class="btn-more-link pull-right">
                            <a href="<?php echo $SEO->build_link($v,"posts"); ?>" class="readmore btn btn-outline">Xem chi tiết</a>
                         </section>
                      </footer>
                   </div>
                   <div class="pagination clearfix"></div>
                    <?php
                } 
                ?> 
                
            </article>
            <?php echo $posts['paging'];  ?>
         </div>
      </div> 
   </div>
</section>
 
	<div class="row">
		<div class="col-lg-12">
			<div class="td-filter-cate">
				<ul>
					<li <?php echo $order==""?'class="active"':''; ?>><a href="<?php echo $base_url ?>"><span <?php echo $order==""?'style="color:#FFFFFF"':''; ?>>Mới nhất</span><i class="glyphicon glyphicon-triangle-bottom"></i></a></li>
					<li <?php echo $order=="ban-chay"?'class="active"':''; ?>><a href="<?php echo $base_url."/ban-chay"; ?>"><span <?php echo $order=="ban-chay"?'style="color:#FFFFFF"':''; ?>>Bán chạy</span><i class="glyphicon glyphicon-triangle-bottom"></i></a></li>
					<li <?php echo $order=="khuyen-mai"?'class="active"':''; ?>><a href="<?php echo $base_url."/khuyen-mai"; ?>"><span <?php echo $order=="khuyen-mai"?'style="color:#FFFFFF"':''; ?>>Khuyến mãi</span><i class="glyphicon glyphicon-triangle-bottom"></i></a></li>
					<li <?php echo $order=="gia-tot"?'class="active"':''; ?>><a href="<?php echo $base_url."/gia-tot"; ?>"><span <?php echo $order=="gia-tot"?'style="color:#FFFFFF"':''; ?>>Giá</span><i class="glyphicon glyphicon-triangle-bottom"></i></a></li>
					<li <?php echo $order=="xem-nhieu"?'class="active"':''; ?>><a href="<?php echo $base_url."/xem-nhieu"; ?>"><span <?php echo $order=="xem-nhieu"?'style="color:#FFFFFF"':''; ?>>Lượt xem</span><i class="glyphicon glyphicon-triangle-bottom"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="col-lg-9">
			<div class="td-block-featured">
				<div class="block-featured-header">
					<h2 class="block-title">Sản phẩm dành cho bạn</h2>
				</div>
				<div class="td-block-container">
					<ul class="products-list">
						
						<?php 
						foreach($product['pageList'] as $k=>$v){ 
							echo $this->main->tooltip($v, '198x235');  
						} 
						?> 
						
					 
					</ul>
				</div>
				
				<?php echo $product['paging'];  ?>
			</div>
		</div>
		<div class="col-lg-3">
			<?php echo Modules::run('right/sanpham_khuyenmai'); ?>
			<?php echo Modules::run('right/sanpham_banchay'); ?>
		</div>
	</div>
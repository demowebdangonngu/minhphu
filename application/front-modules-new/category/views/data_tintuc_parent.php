<section class="pav-category wrapper clearfix clearfix blog-wrapper clearfix clearfix">
   <div class="pav-children">
      <div class="children-wrap row">
         <?php 
         
         foreach($list_cate_post as $k=>$v){
            ?>
            <div class="col-lg-4 col-sm-4 col-xs-12">
                <div class="children-inner">
                   <h4><a href="<?php echo $SEO->build_link($v,"category"); ?>" title="<?php echo $v->name; ?>"><?php echo $v->name; ?></a></h4>
                   <div class="sub-description">
                      <?php echo $v->description; ?>
                   </div>
                </div>
             </div>
            <?php
         }
         ?> 
      </div>
   </div>
   <div class="pav-blogs">
   </div>
</section>
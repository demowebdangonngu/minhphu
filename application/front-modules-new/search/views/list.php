<?php echo Modules::run('menu/index2'); ?> 
<?php echo $breadcumb; ?>

	<div class="row">
		<div class="col-lg-12">
			<div class="td-filter-cate">
				<ul>
					<li <?php echo $order==""?'class="active"':''; ?>><a href="<?php echo $base_url ?>"><span>Mới nhất</span><i class="glyphicon glyphicon-triangle-bottom"></i></a></li>
					<li <?php echo $order=="buy"?'class="active"':''; ?>><a href="<?php echo $base_url."?order=buy"; ?>"><span>Bán chạy</span><i class="glyphicon glyphicon-triangle-bottom"></i></a></li>
					<li <?php echo $order=="sale"?'class="active"':''; ?>><a href="<?php echo $base_url."?order=sale"; ?>"><span>Khuyến mãi</span><i class="glyphicon glyphicon-triangle-bottom"></i></a></li>
					<li <?php echo $order=="price"?'class="active"':''; ?>><a href="<?php echo $base_url."?order=price"; ?>"><span>Giá</span><i class="glyphicon glyphicon-triangle-bottom"></i></a></li>
					<li <?php echo $order=="view"?'class="active"':''; ?>><a href="<?php echo $base_url."?order=view"; ?>"><span>Lượt xem</span><i class="glyphicon glyphicon-triangle-bottom"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="col-lg-12">
			<div class="td-block-featured td-search-result">
				<div class="block-featured-header">
					<div class="block-title"><h1>Tìm Kiếm - <?php echo $key; ?></h1><span class="txt-amount-products"><!--( <strong><?php echo $total; ?></strong> sản phẩm )--></span></div>
					
				</div>
				<div class="td-block-container">
					<ul class="products-list"> 
						<?php
						 foreach($product_search['pageList'] as $k=>$v){
							echo $this->main->tooltip($v); 
						 } 
						 ?>  
					</ul>
					<?php echo $product_search['paging'];  ?>
				</div>
				
			</div>
		</div>
	</div>
<?php echo Modules::run('menu/index2'); ?> 
<?php echo $breadcumb;$afterposts= $data['pageList']; ?>

	<div class="row">
		<div class="col-lg-12">
			<div class="td-block-featured td-search-result">
				<div class="block-featured-header">
					<div class="block-title"><h1>Tìm Kiếm - <?php echo $tag_obj->name;?></h1><span class="txt-amount-products"><!--( <strong><?php echo $total; ?></strong> sản phẩm )--></span></div>
					
				</div>
				<div class="td-block-container">
					<ul class="products-list"> 
						 <?php $_count;
						foreach($afterposts as $v) {
						 $_count++; 
						 ?>  
						 <li class="item">
							<div class="item-inner">
							<div class="thumbs-wrap">
								<a class="entry-thumbs" href="<?php echo $SEO->build_link($v,"posts");?>">
									<img style="width:199px;height:235px;" src="<?php echo $v->image;?>" class="img-responsive">
								</a>
							</div>
							<div class="meta">
								<div class="entry-name"><a href="<?php echo $SEO->build_link($v,"posts");?>" title=""><?php echo $v->title;?></a></div>
							</div>
							</div>
						</li>
					 <?php }?>
					</ul>
					<div class="page-nav">
						<?php echo $data['paging'];  ?>
					</div>
				</div>
				
			</div>
		</div>
	</div>
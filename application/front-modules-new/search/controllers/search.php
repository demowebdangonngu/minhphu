<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
require_once APPPATH."third_party/YT/ControllerPublic.php";
class search extends YT_ControllerPublic {  
 
    function __construct()	{
    		parent::__construct();		 
    	 $this->load->library('main/main', 'main');
		$this->load->model('product/model_product' , 'SP' , true); 
		$this->load->model('posts/model_posts' , 'BV' , true); 
   	}
    
   public function index($text) {   
	 
        $text = trim($text); 
        if($text == '' || !isset($text)){
            redirect(base_url());
        } 
        $text1 = str_replace('-',' ',$text);
        $text  = str_replace('-','%',$text);
       
        $like['product.name'] = str_replace('-' , ' ' , $text); 
        $this->data['sanpham'] = $this->SP->getList(false , false , $like);
        
         
        
         //print_r($ids);exit; 
        $total = count($this->data['product']);
        $base_url = 'tim-kiem/'.$this->uri->segment(2); 
        $slug = 'tim-kiem/'.$this->uri->segment(2); 
        $where = "product.name LIKE '%$text%'";
		
		
		
		 
		$order_by = array(); 
		$data_get = $this->input->get("order" , true); 
		switch ($data_get){
			case 'buy':
				$order_by["product.public_date"] = "DESC";
				break;
			case 'sale':
				$order_by["product.price_sale"] = "DESC"; 
				$where .= ' AND price_sale != 0'; 
				break;
			case 'price':
				$order_by["product.price"] = "DESC";
				break;
			case 'view':
				$order_by["product.public_date"] = "ASC";
				break;
			default:
				$order_by["product.public_date"] = "DESC";
		}
		$this->data['order'] = $data_get;
		
		
		$this->data['product_search']=$this->SP->get_product_pages(PER_PAGE_MAIN,array('uri_segment'=>3,'base'=>$slug),array(),array(),$where,$order_by); 
         
        // breadcumb
        $breadcumb = array( 
            array(
                'url' => $base_url,
                'name' => 'Tìm kiếm'
            ),
            array(
                'url' => $base_url,
                'name' => $text1
            )
        );  
        $this->data['base_url']  =  base_url($base_url);
        $this->data['key'] = $text1;
        $this->data['breadcumb'] = $this->main->breadcrumb($breadcumb);    
        $this->template->build('list', $this->data); 
        
         
        
   }
   
	function tintuc(){
		$tags=$this->load->model("tags/tags_model");
		$tags->where['slug']=$this->uri->segment(2);
		$tags->where['status']=1;
		$this->data['tag_obj']=$tags->get_object_by_where();
		if($this->data['tag_obj']){
			$posts=$this->load->model("posts/model_posts");
			$this->data['looppage']=1000;
			$this->data['data']=$posts->get_content_tag($this->data['tag_obj']->id,8,array('tag'=>1,'uri_segment'=>3,'base'=>"tim-kiem-tin/".$this->uri->segment(2)));
			$this->data['base_path']="tim-kiem-tin/".$this->uri->segment(2);

			$ct=new stdClass();
			$ct->name="tim-kiem-tin/".$this->uri->segment(2);
			$ct->slug="tim-kiem-tin/".$this->uri->segment(2);
			$this->data['breadcrumb'][]=$ct;
			$SEO=$this->load->library("SEO");
			if($this->uri->segment(3)){
				$trang = $this->uri->segment(3);
			}else{
				$trang = 1;
			}
			
			$SEO->title=$this->data['tag_obj']->name." - Trang ".$trang;
			$SEO->description=$this->data['tag_obj']->name." - Trang ".$trang;
			$this->template->build('listposts', $this->data);
		}else{
			return show_404();
		}
	}
     
   
}
 ?>
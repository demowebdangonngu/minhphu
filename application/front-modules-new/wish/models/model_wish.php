<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');	
require_once APPPATH."third_party/YT/Model.php";
class Model_wish extends YT_Model {
	public function __construct(){  
		parent::__construct();
        $this->table_name= "product_like";   
	} 
    public function get_list_wish($where){ 
        $this->getDB()->select('*');
        $this->getDB()->from($this->table_name);
        $this->getDB()->where($where); 
        return  $this->getDB()->get()->result();  
	}
    public function insert_list_wish($data){ 
        $this->getDB()->insert($this->table_name, $data); 
        return  $this->getDB()->insert_id();   
	}   
    public function delete_list_wish($where){  
        $this->getDB()->where($where);
        $this->getDB()->delete($this->table_name);    
	}
    
    public function get_list_cthd($where){
        $this->getDB()->select('order_detail.* , email , order.status , order.total_price , product.name as name_sp , image');
        $this->getDB()->from('order_detail');
        $this->getDB()->join('order' , 'order.id = order_detail.order_id');
        $this->getDB()->join('product' , 'product.id = order_detail.product_id');
        $this->getDB()->where($where); 
        return  $this->getDB()->get()->result(); 
    }
    
    
    function get_list_product_pages($limit=10,$option=array(),  $where = false){
        $return=array();
        $condition=array('product.status'=>1);
		$this->getDB()->select("product.*,categories.name as cat_name, categories.id as cat_id ");
        $this->getDB()->from($this->table_name);
        $this->getDB()->join ("product", "product_like.id_product=product.id");
        $this->getDB()->join ("categories", "categories.id=product.cat_id");
         
        if($where){ 
           $this->getDB()->where($where);
        } 
        $this->getDB()->where($condition);
        
        $pagination=$this->load->library("Pagination");
        $result['total']=$config['total_rows'] = $this->getDB()->count_all_results();;
		$config['per_page'] = $limit;
         $option['base']?$option['base']=trim($option['base'],"/")."/":"latest";
        $config['base_url']=base_url().$option['base'];
        $config['use_page_numbers']=true;
        $config['uri_segment']=$option['uri_segment']?$option['uri_segment']:2;
        $pagination->initialize($config);
        $result['paging']=$pagination->create_links();
        
        
        $this->getDB()->select("product.*,categories.name as cat_name, categories.id as cat_id ");
        $this->getDB()->from($this->table_name);
        $this->getDB()->join ("product", "product_like.id_product=product.id");
        $this->getDB()->join ("categories", "categories.id=product.cat_id"); 
        if($where){ 
           $this->getDB()->where($where);
        } 
        $this->getDB()->where($condition);
        $this->getDB()->order_by("product.public_date","DESC"); 
        $this->getDB()->limit($limit,$pagination->cur_page>1?($pagination->cur_page-1)*$limit:0);
        $result['cur_page']=$pagination->cur_page;
        $plist=$this->getDB()->get()->result();
        $result['pageList']=array();
        $pids=array();
        foreach ($plist as $p){
            $result['pageList'][$p->id]=$p;
             $pids[]=$p->id;
        }
		// print_r($result);exit;

        return $result;
        
    }
	 
}
?>
<header id="header">
   <div id="header-top">
      <div class="container">
         <ul class="toplist pull-right">
            <li class="item"><a href="#" title="">Hotline: <b>19001999</b></a></li> 
            <?php if(!$this->session->userdata('member_name')){?> 
                  <li class="item"><a href="<?php echo base_url(); ?>tai-khoan/login" title="Đăng Nhập"  ><i class="icon icon-user glyphicon glyphicon-user"></i><span>Đăng nhập</span></a></li>
				<li class="item"><a href="<?php echo base_url(); ?>tai-khoan/register" title="Đăng Ký"  ><i class="icon icon-user glyphicon glyphicon-edit"></i><span>Đăng ký</span></a></li>
            <?php }else{?>
                  <li class="item"><a href="<?php echo base_url(); ?>tai-khoan/logout" title="Logout"><i class="icon icon-user glyphicon glyphicon-edit"></i><span>Đăng xuất</span></a></li>
                           
            <?php }?> 
         </ul>
      </div>
   </div>
   <div id="header-main" class="container">
      <div class="header-main-inner clearfix">
      <div id="logo" class="pull-left">
		<?php 
		$attach = $this->load->model('banner/Model_banner');
		$attach->getDB()->from('attach');
		$attach->getDB()->where(array('nameModel'=>'logo','status'=>1));
		$logo = current($attach->getDB()->get()->result());
		?>
        <a title="<?php echo $logo->name;?>" href="<?php echo $logo->slug==''?base_url():$logo->slug;?>"><img src="<?php echo $logo->location;?>" alt="<?php echo $logo->name;?>" height="50"/></a>
      </div>
      <div class="pull-right head-user">
		 <a href="<?php echo base_url().'cartorder';?>">
			 <div class="btn-shoppingcart" id="cart" style="height: 36px;">
				
			 </div>
		 </a>
		 <?php if($this->session->userdata('member_name')){?> 
		 <div class="btn-favorite">
		 <a href='<?php echo base_url('tai-khoan/wishlist'); ?>'>
            <i class="icon icon-favorite glyphicon glyphicon-heart"></i>
            <span>Yêu Thích</span>
		</a>
         </div>
		 <?php } ?>
         
      </div>
      <div class="pull-right quick-search">
         <form class="form-inline" id="frmSearch" method="post" action="<?php echo base_url("tim-kiem"); ?>" >
            <div class="input-group">
               <input type="text" id="keyword" name="txtSearch" class="form-control" placeholder="Bạn cần tìm sản phẩm gì?">
               <span style="display:table-cell;vertical-align:middle;width:38px;">
               <button class="btn btn-default btn-quick-search" type="submit" >
                  <i class="icon icon-search glyphicon glyphicon-search"></i>
               </button>
               </span>
            </div>
            
         </form>
		 
  
		 
      </div>
      </div>
   </div>
</header> 

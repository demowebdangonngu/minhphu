<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
require_once APPPATH."third_party/YT/Model.php";
class Tags_model extends YT_Model {

    public function __construct() {
        $this->table_name="tags";
        parent::__construct();
       
        
    }
	function get_tag_by_object($obj_id=0,$module='posts'){
		$this->getDB()->from($this->table_name);
		$this->getDB()->join('tags_content',$this->table_name."."."id=tags_content.tag_id");
		$this->getDB()->where(array('tags_content.obj_id'=>$obj_id,'tags_content.module'=>$module));
		
		$this->getDB()->order_by("tags_content.tag_id","DESC");
		return $this->getDB()->get()->result();
    }
	
	function get_tag_by_module($obj_id=0,$limit=10,$option=array(),$module='posts'){
		$return=array();
		$this->getDB()->from($this->table_name);
		$this->getDB()->join('tags_content',$this->table_name."."."id=tags_content.tag_id");
		$this->getDB()->where(array('tags_content.tag_id'=>$obj_id,'tags_content.module'=>$module));
		
		$pagination=$this->load->library("Pagination");
        $result['total']=$config['total_rows'] = $this->getDB()->count_all_results();;
		$config['per_page'] = $limit;
         $option['base']?$option['base']=trim($option['base'],"/")."/":"latest";
        $config['base_url']=base_url().$option['base'];
        $config['use_page_numbers']=true;
        $config['uri_segment']=$option['uri_segment']?$option['uri_segment']:2;
        $pagination->initialize($config);
        $result['paging']=$pagination->create_links();
		
		$this->getDB()->select("*");
		$this->getDB()->from($this->table_name);
		$this->getDB()->join('tags_content',$this->table_name."."."id=tags_content.tag_id");
		$this->getDB()->where(array('tags_content.tag_id'=>$obj_id,'tags_content.module'=>$module));
		$this->getDB()->order_by("tags_content.tag_id","DESC");
		
		$this->getDB()->limit($limit,$pagination->cur_page>1?($pagination->cur_page-1)*$limit:0);
        $result['cur_page']=$pagination->cur_page;
        $plist=$this->getDB()->get()->result();
        $result['pageList']=array();
        $pids=array();
        foreach ($plist as $p){
            $result['pageList'][$p->id]=$p;
             $pids[]=$p->id;
        }
		return $result;
    }
	
    function get_tag_by_content($ids=array(),$module="posts"){
        $this->getDB()->select("*");
        $this->getDB()->from($this->table_name);
        $this->getDB()->join("tags_content","tags.id=tags_content.tag_id","right");
        $this->getDB()->where(array('module'=>$module));
        $this->getDB()->where_in('obj_id',$ids);
        return $this->getDB()->get()->result();
    }
}
<?php echo Modules::run('menu/index2'); ?>
   <?php  echo $breadcumb; ?> 
<div class="row">
	<div class="col-lg-9">
		<div class="post">
			<div class="post-container">
				<div class="post-info">
					<div class="post-info-img">
						
					</div>
					<div class="post-info-container">
						<h1 class="entry-title"><?php echo $obj->name;?></h1>
						<div class="price-code">Mã SP:<span><?php echo $obj->code_product;?></span></div>
						<div class="entry-price">
							<?php if($obj->price_sale != 0){?>
							<span class="price-sale"><?php echo number_format($obj->price_sale,0,',', '.');?></span>
							<span class="price-sale-off">Giá cũ: <?php echo number_format($obj->price,0,',', '.');?></span>
							<?php }else{?>
							<span class="price-sale"><?php echo number_format($obj->price,0,',','.');?></span>
							<?php }?>
						</div>
						<div class="box-attributes">
							<div class="box-attr">
								<dl class="color attrs">
									<dt class="lb">Màu sắc</dt>    
									<ul class="color-list">
										<?php 
										$stt = 0;
										foreach($colors as $color){
											$stt++;
										?>
										<li class="item-color <?php echo $stt==1?'selected':'';?>" onclick="GetSizeImage('<?php echo $color->id;?>','<?php echo $obj->id;?>')">
											<span class="name-color"><?php echo $color->name;?></span>
											<span class="display-color">
											<img src="<?php echo $color->image;?>" style="width: 35px;height: 31px;" />
												<span class="triangle-bottom"></span><i class="icon icon-buynow glyphicon glyphicon-ok"></i>
											</span>
										</li>
										<?php }?>
									</ul>
								</dl>                                              
								<dl class="size attrs">
									<dt class="lb">Kích thước</dt>
									<ul class="size-list">
										
									</ul>
								</dl>
								<dl class="quality quantity">
									<dt class="lb">Số lượng: </dt>
									<dd class="sl">
										<input name="qty" id="qtysl" autofocus="autofocus" autocomplete="off" type="number" min="1" max="9999" class="inpt-qty" required value="1">
										<span class="add-qty" title="Thêm">+</span>
										<span class="sub-qty" title="Bớt">-</span>                                  
									</dd>
									
								</dl>
							</div>
							<input type="text" class="id_color" value="" hidden />
							<input type="text" class="id_size" value="" hidden />
						</div>
						<div>
							<a onclick="add_cart();" class="btn btn-default checkout-btn add-to-cart">
								<i class="icon icon-shopping-cart glyphicon glyphicon-shopping-cart"></i> Thêm vào giỏ hàng
							</a>
							<a  onclick="muangay();" class="btn btn-default checkout-btn btn-buynow">
								<i class="icon icon-buynow glyphicon glyphicon-ok"></i> Mua ngay
							</a>
							<a onclick="add_wish_list('<?php echo $obj->id;?>');" class="btn btn-default btn-likes">
								<i class="icon icon-favorite glyphicon glyphicon-heart"></i>
								<span>Yêu Thích</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--
	<div class="col-lg-3">
		<div class="td-block-sale td-widget-sale">
			<h2 class="block-title">Liên hệ với chúng tôi</h2>
			<div class="td-block-container">
				<p>Hotline:</p>
				<p>Địa chỉ: 329 Nguyễn trãi,P.7, Q.5, HCM</p>
				<ul>
					<li><a href="#">Chính sách đổi trả</a></li>
					<li><a href="#">Chính sách bảo hành</a></li>
					<li><a href="#">Phương thức vận chuyển</a></li>
				</ul>
			</div>
		</div>
	</div>
	-->
	<div class="col-lg-9">
		<div class="post-content">
			<div class="post-content-header">
				<h2 class="block-title"></h2>
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#tab-m-1" aria-controls="tab-1" role="tab" data-toggle="tab">Mô tả sản phẩm</a></li>
					<li role="presentation"><a href="#tab-m-2" aria-controls="profile" role="tab" data-toggle="tab">Thông tin sản phẩm</a></li>
				</ul>
			</div>
			<div class="post-content-container">
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="tab-m-1">
						<?php echo $obj->content;?>
					</div>
					<div role="tabpanel" class="tab-pane" id="tab-m-2">
						<?php echo $obj->specification;?>
					</div>
				</div>
			</div>
		</div>
		<div class="td-block-featured">
			<div class="block-featured-header">
				<h2 class="block-title">Sản phẩm liên quan</h2>
			</div>
			<div class="td-block-container">
				<ul class="products-list">
					<li class="item">
						<div class="item-inner">
						<div class="thumbs-wrap">
							<a class="entry-thumbs" href="#">
								<img src="http://elara.vn/image/171-giay-vai-nam-thoi-trang-220x260.jpg" class="img-responsive">
							</a>
							<div class="area-favorite"><i class="glyphicon glyphicon-heart-empty"></i>Yêu thích</div>
						</div>
						<div class="meta">
							<div class="entry-price">
								<span class="price-sale">987,000 đ</span>
								<span class="price-sale-off">1,430,000 đ</span>
							</div>
							<div class="entry-name"><a href="#" title="">Giày de mo</a></div>
							<div class="entry-code">Mã: <span>T25B</span></div>
						</div>
						<span class="stick-label">-46%</span>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-lg-3">
		<?php echo Modules::run('right/sanpham_khuyenmai'); ?>
		<?php echo Modules::run('right/sanpham_banchay'); ?> 
	</div>
</div>
<script>
<?php $item_color = array_shift($colors);?>
GetSizeImage('<?php echo $item_color->id;?>','<?php echo $obj->id;?>');
var idcolor = <?php echo $item_color->id;?>;
var idsize = 0;
function GetSizeImage(id_color, id_product){
	$('.id_color').val(id_color);
	idcolor = id_color;
	$.ajax({
		type: 'POST',
		url: '<?php echo base_url();?>product/getsize',
		data: 'id_color='+id_color+'&id_product='+id_product,
		success: function (data) {
			$('.size-list').html(data);
		}
	});
	$.ajax({
		type: 'POST',
		url: '<?php echo base_url();?>product/getimage',
		data: 'id_color='+id_color+'&id_product='+id_product,
		success: function (data) {
			$('.post-info-img').html(data);
			var sync1 = $("#sync1");
  var sync2 = $("#sync2");
 
  sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    navigation: false,
    pagination:false,
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
  });
 
  sync2.owlCarousel({
    items : 4,
    itemsDesktop      : [1199,10],
    itemsDesktopSmall     : [979,10],
    itemsTablet       : [768,8],
    itemsMobile       : [479,4],
    pagination:false,
	 navigation: false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
      el.find(".owl-item").eq(0).addClass("synced");
    }
  });
 
  function syncPosition(el){
    var current = this.currentItem;
    $("#sync2")
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if($("#sync2").data("owlCarousel") !== undefined){
      center(current)
    }
  }
 
  $("#sync2").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
  });
 
  function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }
 
    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }
    
  }
 $('.box-attr ul li').click(function(){
		if(!$(this).hasClass("selected")){
			$(this).siblings().removeClass("selected");
			$(this).addClass("selected");
		}	
			
	});
		}
	});
}

function change_size(id_size){
	$('.id_size').val(id_size);
	idsize = id_size;
}

function muangay(){
	add_cart(); 
	setTimeout(function(){ window.location.replace(baseurl + "cartorder/"); }, 1000);
}

function add_cart(){
	if(idsize == 0){
		alert('Vui lòng chọn kích thước size');
	}else{ 
		add_cart_item('<?php echo $obj->id; ?>', idcolor, idsize, $("#qtysl").val());
	}
	
}


</script>
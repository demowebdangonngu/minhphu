<section id="header-top" >
                  <div id="topbar">
                     <div class="container">
                        <div class="show-desktop">
                           <div class="quick-access pull-left  hidden-sm hidden-xs">
                              <div class="login links link-active">
                                 <a href="account/register.html">Đăng ký</a> or
                                 <a href="account/login.html">Đăng nhập</a>
                              </div>
                           </div>
                           <!--Button -->
                           <div class="quick-top-link  links pull-right">
                              <!-- language -->
                              <div class="btn-group box-language">
                              </div>
                              <!-- currency -->
                              <div class="btn-group box-currency">
                                 <div class="quick-access">
                                    <form action="http://camerano1.com/common/currency/currency.html" method="post" enctype="multipart/form-data" id="currency">
                                       <div class="btn-groups">
                                          <div type="button" class="dropdown-toggle" data-toggle="dropdown">
                                             <span>VNĐ</span>
                                             <span>Tiền tệ </span>
                                             <i class="fa fa-angle-down"></i>
                                          </div>
                                          <div class="dropdown-menu ">
                                             <div class="box-currency inner">
                                                <a class="currency-select" type="button" name="EUR">
                                                €                  </a>
                                                <a class="currency-select" type="button" name="GBP">
                                                VNĐ                  </a>
                                                <a class="currency-select" type="button" name="USD">
                                                $                  </a>
                                             </div>
                                          </div>
                                       </div>
                                       <input type="hidden" name="code" value="" />
                                       <input type="hidden" name="redirect" value="http://camerano1.com" />
                                    </form>
                                 </div>
                              </div>
                              <!-- user-->
                              <div class="btn-group box-user">
                                 <div data-toggle="dropdown">
                                    <span>Tài khoản cá nhân</span>
                                    <i class="fa fa-angle-down "></i>
                                 </div>
                                 <ul class="dropdown-menu setting-menu">
                                    <li id="wishlist">
                                       <a href="account/login.html" id="wishlist-total"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Yêu thích (0)</a>
                                    </li>
                                    <li class="acount">
                                       <a href="account/login.html"><i class="fa fa-user"></i>&nbsp;&nbsp;Tài khoản cá nhân</a>
                                    </li>
                                    <li class="shopping-cart">
                                       <a href="checkout/cart.html"><i class="fa fa-bookmark"></i>&nbsp;&nbsp;Giỏ hàng</a>
                                    </li>
                                    <li class="checkout">
                                       <a class="last" href="checkout/cart.html"><i class="fa fa-share"></i>&nbsp;&nbsp;Thanh toán</a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- header -->
                  <header id="header">
                     <div id="header-main" class="hasmainmenu">
                        <div class="container">
                           <div class="row">
                              <div class="logo inner  col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                 <div id="logo-theme" class="logo-store pull-left">
                                    <a href="index.html">
                                    <span>CAMERA QUAN SÁT -- CAMERA AN NINH -- THIẾT BỊ VĂN PHÒNG</span>
                                    </a>
                                 </div>
                              </div>
                              <div class="inner col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                 <div class=" search_block">
                                    <form method="GET" action="http://camerano1.com/index.php" class="input-group search-box">
                                       <div class="filter_type category_filter input-group-addon group-addon-select no-padding icon-select">
                                          <select name="category_id" class="no-border">
                                             <option value="0">Tất cả sản phẩm</option>
                                             <option value="73">Camera  IP</option>
                                             <option value="74">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera  IP AVtech</option>
                                             <option value="77">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera  IP HDtech</option>
                                             <option value="75">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera  IP Questek</option>
                                             <option value="76">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera  IP Vantech</option>
                                             <option value="67">Camera quan sát</option>
                                             <option value="68">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vantech</option>
                                             <option value="102">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera AHD Vantech</option>
                                             <option value="100">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - CVI Vantech</option>
                                             <option value="101">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - SDI Vantech</option>
                                             <option value="103">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - TVI Vantech</option>
                                             <option value="69">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Questek</option>
                                             <option value="95">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera AHD Questek</option>
                                             <option value="96">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera Analog Questek</option>
                                             <option value="97">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - CVI Questek</option>
                                             <option value="98">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - SDI Questek</option>
                                             <option value="99">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - TVI Questek</option>
                                             <option value="70">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Vdtech</option>
                                             <option value="94">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HDtech</option>
                                             <option value="71">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sony</option>
                                             <option value="72">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;samsung</option>
                                             <option value="119">Linh kiện Camera</option>
                                             <option value="83">Máy chấm công</option>
                                             <option value="87">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Linh kiện máy chấm công</option>
                                             <option value="85">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy chấm công thẻ giấy</option>
                                             <option value="86">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy chấm công thẻ từ</option>
                                             <option value="84">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy chấm công vân tay</option>
                                             <option value="120">phần mềm </option>
                                             <option value="118">thiết bị báo trộm</option>
                                             <option value="78">Đầu ghi hình</option>
                                             <option value="81">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vantech</option>
                                             <option value="110">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình AHD Vantech</option>
                                             <option value="114">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình Analog Vantech</option>
                                             <option value="111">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình HD-CVI Vantech</option>
                                             <option value="113">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình HD-SDI Vantech</option>
                                             <option value="82">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;questek</option>
                                             <option value="105">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ahd Questek</option>
                                             <option value="106">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Analog Questek</option>
                                             <option value="107">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HD-CVI Questek</option>
                                             <option value="112">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình HD-SDI Questek</option>
                                             <option value="79">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Samsung</option>
                                             <option value="104">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HDtech</option>
                                             <option value="80">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VDtech</option>
                                             <option value="115">Đầu ghi hình IP</option>
                                             <option value="116">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình IP Questek</option>
                                             <option value="117">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình IP Vantech</option>
                                             <option value="20">Tổng đài điện thoại</option>
                                             <option value="59">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ghi âm điện thoại</option>
                                             <option value="60">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Panasonic</option>
                                             <option value="26">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Linh kiện điện thoại</option>
                                             <option value="27">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Điện thoại bàn</option>
                                             <option value="18">Thiết bị phòng game</option>
                                             <option value="62">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy tính Intel giá rẽ</option>
                                             <option value="61">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;máy server bootrom cao cấp</option>
                                             <option value="45">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bàn gế phòng nét</option>
                                             <option value="46">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy chủ game mainboard Intel</option>
                                             <option value="122">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy tính Intel cao cấp</option>
                                             <option value="123">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy tính Intel VIP</option>
                                             <option value="121">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy tính Intel tầm trung</option>
                                             <option value="57">Thiết bị mạng</option>
                                             <option value="17">Linh kiện máy tính</option>
                                             <option value="24">thiết bị văn phòng</option>
                                             <option value="33">Trọn bộ Camera quan sát </option>
                                             <option value="25">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera quan sát Gói tiết kiệm</option>
                                             <option value="29">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 camera</option>
                                             <option value="28">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 camera</option>
                                             <option value="30">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8 camera</option>
                                             <option value="31">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;16 camera</option>
                                             <option value="32">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Camera quan sát gói Cao cấp</option>
                                             <option value="88">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 camera</option>
                                             <option value="89">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 camera</option>
                                             <option value="90">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8 camera</option>
                                             <option value="91">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;16 camera</option>
                                          </select>
                                       </div>
                                       <div id="search0" class="search-input-wrapper clearfix clearfixs input-group">
                                          <input type="text" value="" size="35" autocomplete="off" placeholder="Tìm sản phẩm.." name="search" class="form-control input-lg input-search">
                                          <span class="input-group-addon input-group-search">
                                          <span class="fa fa-search"></span>
                                          <span class="button-search ">
                                          Tìm sản phẩm..                    </span>
                                          </span>
                                       </div>
                                       <input type="hidden" name="sub_category" value="1" id="sub_category"/>
                                       <input type="hidden" name="route" value="product/search"/>
                                       <input type="hidden" name="sub_category" value="true" id="sub_category"/>
                                       <input type="hidden" name="description" value="true" id="description"/>
                                    </form>
                                    <div class="clear clr"></div>
                                 </div>
                                 <script type="text/javascript">
                                    /* Autocomplete */
                                    (function($) {
                                    	function Autocomplete1(element, options) {
                                    		this.element = element;
                                    		this.options = options;
                                    		this.timer = null;
                                    		this.items = new Array();
                                    
                                    		$(element).attr('autocomplete', 'off');
                                    		$(element).on('focus', $.proxy(this.focus, this));
                                    		$(element).on('blur', $.proxy(this.blur, this));
                                    		$(element).on('keydown', $.proxy(this.keydown, this));
                                    
                                    		$(element).after('<ul class="dropdown-menu autosearch"></ul>');
                                    		$(element).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));
                                    	}
                                    
                                    	Autocomplete1.prototype = {
                                    		focus: function() {
                                    			this.request();
                                    		},
                                    		blur: function() {
                                    			setTimeout(function(object) {
                                    				object.hide();
                                    			}, 200, this);
                                    		},
                                    		click: function(event) {
                                    			event.preventDefault();
                                    			value = $(event.target).parent().attr("href");
                                    			if (value) {
                                    				window.location = value.replace(/&amp;/gi,'&');
                                    			}
                                    		},
                                    		keydown: function(event) {
                                    			switch(event.keyCode) {
                                    				case 27: // escape
                                    					this.hide();
                                    					break;
                                    				default:
                                    					this.request();
                                    					break;
                                    			}
                                    		},
                                    		show: function() {
                                    			var pos = $(this.element).position();
                                    
                                    			$(this.element).siblings('ul.dropdown-menu').css({
                                    				top: pos.top + $(this.element).outerHeight(),
                                    				left: pos.left
                                    			});
                                    
                                    			$(this.element).siblings('ul.dropdown-menu').show();
                                    		},
                                    		hide: function() {
                                    			$(this.element).siblings('ul.dropdown-menu').hide();
                                    		},
                                    		request: function() {
                                    			clearTimeout(this.timer);
                                    
                                    			this.timer = setTimeout(function(object) {
                                    				object.options.source($(object.element).val(), $.proxy(object.response, object));
                                    			}, 200, this);
                                    		},
                                    		response: function(json) {
                                    			console.log(json);
                                    			html = '';
                                    
                                    			if (json.length) {
                                    				for (i = 0; i < json.length; i++) {
                                    					this.items[json[i]['value']] = json[i];
                                    				}
                                    
                                    				for (i = 0; i < json.length; i++) {
                                    					if (!json[i]['category']) {
                                    						html += '<li class="media" data-value="' + json[i]['value'] + '">';
                                    						if(json[i]['simage']) {
                                    							html += '	<a class="media-left" href="' + json[i]['link'] + '"><img  src="' + json[i]['image'] + '"></a>';	
                                    						}
                                    						html += '<div class="media-body">';
                                    						html += '	<a  href="' + json[i]['link'] + '"><span>' + json[i]['label'] + '</span></a>';
                                    						if(json[i]['sprice']){
                                    							html += '	<div>';
                                    							if (!json[i]['special']) {
                                    								html += json[i]['price'];
                                    							} else {
                                    								html += '<span class="price-old">' + json[i]['price'] + '</span><span class="price-new">' + json[i]['special'] + '</span>';
                                    							}
                                    							if (json[i]['tax']) {
                                    								html += '<br />';
                                    								html += '<span class="price-tax">text_tax : ' + json[i]['tax'] + '</span>';
                                    							}
                                    							html += '	</div> </div>';
                                    						}
                                    						html += '</li>';
                                    					}
                                    				}
                                    				//html += '<li><a href="index.php?route=product/search&search='+g.term+'&category_id='+category_id+'&sub_category=true&description=true" onclick="window.location=this.href">'+text_view_all+'</a></li>';
                                    
                                    				// Get all the ones with a categories
                                    				var category = new Array();
                                    				for (i = 0; i < json.length; i++) {
                                    					if (json[i]['category']) {
                                    						if (!category[json[i]['category']]) {
                                    							category[json[i]['category']] = new Array();
                                    							category[json[i]['category']]['name'] = json[i]['category'];
                                    							category[json[i]['category']]['item'] = new Array();
                                    						}
                                    						category[json[i]['category']]['item'].push(json[i]);
                                    					}
                                    				}
                                    				for (i in category) {
                                    					html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';
                                    					for (j = 0; j < category[i]['item'].length; j++) {
                                    						html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                                    					}
                                    				}
                                    			}
                                    			if (html) {
                                    				this.show();
                                    			} else {
                                    				this.hide();
                                    			}
                                    			$(this.element).siblings('ul.dropdown-menu').html(html);
                                    		}
                                    	};
                                    
                                    	$.fn.autocomplete1 = function(option) {
                                    		return this.each(function() {
                                    			var data = $(this).data('autocomplete');
                                    			if (!data) {
                                    				data = new Autocomplete1(this, option);
                                    				$(this).data('autocomplete', data);
                                    			}
                                    		});
                                    	}
                                    })(window.jQuery);
                                    $(document).ready(function() {
                                    	var selector = '#search0';
                                    	var total = 0;
                                    	var show_image = true;
                                    	var show_price = true;
                                    	var search_sub_category = true;
                                    	var search_description = true;
                                    	var width = 102;
                                    	var height = 102;
                                    
                                    	$(selector).find('input[name=\'search\']').autocomplete1({
                                    		delay: 500,
                                    		source: function(request, response) {
                                    			var category_id = $(".category_filter select[name=\"category_id\"]").first().val();
                                    			if(typeof(category_id) == 'undefined')
                                    				category_id = 0;
                                    			var limit = 5;
                                    			var search_sub_category = search_sub_category?'&sub_category=true':'';
                                    			var search_description = search_description?'&description=true':'';
                                    			$.ajax({
                                    				url: 'index.php?route=module/pavautosearch/autocomplete&filter_category_id='+category_id+'&width='+width+'&height='+height+'&limit='+limit+search_sub_category+search_description+'&filter_name='+encodeURIComponent(request),
                                    				dataType: 'json',
                                    				success: function(json) {		
                                    					response($.map(json, function(item) {
                                    						if($('.pavautosearch_result')){
                                    							$('.pavautosearch_result').first().html("");
                                    						}
                                    						total = 0;
                                    						if(item.total){
                                    							total = item.total;
                                    						}
                                    						return {
                                    							price:   item.price,
                                    							speical: item.special,
                                    							tax:     item.tax,
                                    							label:   item.name,
                                    							image:   item.image,
                                    							link:    item.link,
                                    							value:   item.product_id,
                                    							sprice:  show_price,
                                    							simage:  show_image,
                                    						}
                                    					}));
                                    				}
                                    			});
                                    		},
                                    	}); // End Autocomplete 
                                    
                                    });// End document.ready
                                    
                                 </script>                
                              </div>
                              <div id="hidemenu" class=" inner col-lg-3 col-md-3 col-sm-3 hidden-xs">
                                 <div class="cart-top">
                                    <div id="cart" class="btn-group btn-block">
                                       <div type="button" data-toggle="dropdown" data-loading-text="Loading..." class="heading dropdown-toggle">
                                          <div class="cart-inner pull-right">
                                             <div class="icon-cart"></div>
                                             <h4>Xem chi tiết giỏ hàng</h4>
                                             <a><span id="cart-total">0 sản phẩm - Liên hệ<i class="fa fa-angle-down"></i></span></a>
                                          </div>
                                       </div>
                                       <ul class="dropdown-menu pull-right content">
                                          <li>
                                             <p class="text-center empty">Không có sản phẩm trong giỏ hàng!</p>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </header>
                  
                  
                  <?php echo Modules::run('menu'); ?>
                  <!-- /header -->
               </section>
               
               
               
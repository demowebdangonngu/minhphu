<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');
require_once APPPATH."third_party/YT/Model.php";
class Model_menu extends YT_Model {

    public function __construct() {
        $this->table_name="menu";
        parent::__construct(); 
    }
    public $obj_array=array();
        //public $tree_array=array();
        public  $tree=array();
    


    function provider_list(){
         $this->getDB()->select("*");
            $this->getDB()->from('provider');
            $this->getDB()->where('status',1);
            $this->getDB()->order_by('sort_order','asc'); 
            return $this->getDB()->get()->result();
    }
    
    function get_tree($where = false){
            if(count($this->tree)) return $this->tree;

            $this->getDB()->select("menu.*,categories.name as cname,categories.slug as cslug , categories.icon");
            $this->getDB()->from($this->table_name);
            $this->getDB()->join("categories","categories.id=menu.value","left");
            if($where){
                $this->getDB()->where($where);
            }
            $this->getDB()->where(array('menu.status'=>1));
            $this->getDB()->order_by('menu.sort_order','asc');
            $list=$this->getDB()->get()->result();
            foreach ($list as $cat){
                $this->build_node_name($cat);
                $cat->position?$cat->position:$cat->position="unposition";
                $this->obj_array[$cat->position][$cat->id]=$cat;
                //$this->tree_array[$cat->id]=$cat;
                $this->obj_array[$cat->position][$cat->id]->children=array();
                if($cat->parent==0){//root node
                    $this->tree[$cat->position][$cat->id]=$cat;
                    
                }
            }

            foreach ($this->obj_array as $pos => $positionlist) {
                foreach ($positionlist as $key => $obj) {
                     if($this->obj_array[$pos][$obj->parent]){
                            $this->obj_array[$pos][$obj->parent]->children[$obj->id]=$obj;
                        }
                }
               
            }
            
            return $this->tree;
        }
        function build_node_name($node){
            if($node->type=='category'){ 
                $SEO=$this->load->library("SEO");
                $node->title?$node->name=$node->title:$node->name=$node->cname;
                $node->slug?$node->slug:$node->slug=$node->cslug;
                $node->link?$node->link:$node->link=$SEO->build_link($node,'menu'); 
            }else{
                if(strpos($node->link,"http://")===FALSE ){
                    $node->link=base_url().$node->link;
					
                }
                $node->name=$node->title;
            }
        }
         
}
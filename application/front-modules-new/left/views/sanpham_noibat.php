<div class="box featured">
   <div class="box-heading">
      <span>Sản phẩm nổi bật</span>
      <em class="line"></em>
   </div>
   <div class="box-content">
      <div class="row products-row">
      
         <?php
         foreach($product_hot_home  as $k=>$v){
            ?>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 product-col">
                 <?php  echo $this->main->tooltip($v);   ?>
             </div> 
            <?php 
         } 
         ?> 
      </div>
   </div>
</div>
<div class="sidebar-offcanvas  visible-xs visible-sm">
  <div class="offcanvas-inner panel panel-offcanvas">
     <div class="offcanvas-heading">
        <button type="button" class="btn btn-outline btn-close " data-toggle="offcanvas"> <span class="fa fa-times"></span></button>
     </div>
     <div class="offcanvas-body panel-body">
        <div class="category box box-highlights theme">
           <div class="box-heading"><span>Danh Mục</span></div>
           <div class="box-content tree-menu">
              <ul id="278969669accordion" class="box-category box-panel">
                 <li class="accordion-group clearfix">
                    <a href="Camera-IP-c73.html">Camera  IP (0)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="Camera-quan-sat-c67.html">Camera quan sát (30)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="Linh-ki-n-Camera-c119.html">Linh kiện Camera (0)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="May-ch-m-cong-c83.html">Máy chấm công (0)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="ph-n-m-m-c120.html">phần mềm  (0)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="thi-t-b-bao-tr-m-c118.html">thiết bị báo trộm (0)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="D-u-ghi-hinh-c78.html">Đầu ghi hình (6)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="D-u-ghi-hinh-IP-c115.html">Đầu ghi hình IP (0)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="T-ng-dai-di-n-tho-i-c20.html">Tổng đài điện thoại (0)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="Thi-t-b-phong-game-c18.html">Thiết bị phòng game (22)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="Thi-t-b-m-ng-c57.html">Thiết bị mạng (0)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="Linh-ki-n-may-tinh-c17.html">Linh kiện máy tính (0)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="thi-t-b-van-phong-c24.html">thiết bị văn phòng (0)</a>
                 </li>
                 <li class="accordion-group clearfix">
                    <a href="Tr-n-b-Camera-quan-sat-c33.html">Trọn bộ Camera quan sát  (12)</a>
                 </li>
              </ul>
           </div>
        </div>
        <script type="text/javascript">
           $(document).ready(function(){
               var active = $('.collapse.in').attr('id');
               $('span[data-target=#'+active+']').html("-");
           
               $('.collapse').on('show.bs.collapse', function () {
                   $('span[data-target=#'+$(this).attr('id')+']').html("-");
               });
               $('.collapse').on('hide.bs.collapse', function () {
                   $('span[data-target=#'+$(this).attr('id')+']').html("+");
               });
           });
        </script>
     </div>
     <div class="offcanvas-footer panel-footer">
        <div class="input-group" id="offcanvas-search">
           <input type="text" class="form-control" placeholder="Search" value="" name="search">
           <span class="input-group-btn">
           <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
           </span>
        </div>
     </div>
  </div>
</div>
<div class="box blog latest_blog">
   <div class="box-heading ">
      <span>Bài viết mới</span>
   </div>
   <div class="box-content no-padding">
      <div class="pavblog-latest row">
      
         
         <?php
         foreach($post_new as $k=>$v){
            ?>
            <div class="col-lg-12 col-md-12 col-sm-6 col-xs-12">
                <div class="blog-item">
                   <div class="blog-body">
                      <?php
                      if($v->image!= ""){
                           ?>

                           <div class="image">
                             <img src="<?php echo $this->config->item("img_path") . $v->image; ?>" title="<?php echo $v->title  ?>" alt="<?php echo $v->title  ?>" class="img-responsive">
                          </div>

                           
                           <?php
                      }
                      ?>
                      
                      <div class="create-info">
                         <div class="inner">
                            <div class="blog-header">
                               <h4 class="blog-title">
                                  <a href="<?php echo $SEO->build_link($v,"posts"); ?>" title="<?php echo $v->title  ?>"><?php echo $v->title;  ?></a>
                               </h4>
                            </div>
                            <div class="create-date"> 
                               <div class="created">
                                  Ngày viết:  <?php $datePublic = explode(' ',$v->public_date); echo $datePublic[0];?>
                               </div>
                            </div>
                            
                            
                            <div class="description">
                               <?php echo $v->description;  ?>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
            <?php
         } 
         ?>
         
         
          
         
           
      </div>
   </div>
</div>

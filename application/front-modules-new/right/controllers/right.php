<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once APPPATH."third_party/YT/ControllerPublic.php";
class Right extends YT_ControllerPublic { 
 
    public function __construct() {
        parent::__construct(); 
        $this->load->model('product/model_product','PRODUCT' , TRUE); 
        $this->load->library('main/main', 'main');
    }
	public function index()
	{
            
           $this->load->view(__CLASS__);  
	}
   
    public function sanpham_banchay()
	{  
           $this->data['sanpham_banchay'] = $this->PRODUCT->getList(false , array('per_page' => 4, 'start' => 0) , false, false , true);
           $this->load->view('sanphambanchay' , $this->data); 
	}
	
	public function sanpham_khuyenmai()
	{  
		$strIn = array();
		$strIn['price_sale !='] = 0; 
		$this->data['sanpham_khuyenmai'] = $this->PRODUCT->getList(false , array('per_page' => 10, 'start' => 0) , false, $strIn , true);
           $this->load->view('sanphamkhuyenmai' , $this->data); 
	}
    
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

      <div class="td-block-sale">
        <h2 class="block-title">Sản phẩm bán chạy</h2>
        <div class="td-block-container">
          <div class="mCustomScroll">
          <ul class="sale-list"> 
			<?php
			 foreach($sanpham_banchay  as $k=>$v){
				$price = $v->price;
				if($v->price >= $v->price_sale && $v->price_sale != 0){
					 $price = $v->price_sale;
				}
				?>
				<li class="sale-item">
				  <a class="entry-thumbs" href="<?php echo $SEO->build_link($v,"sanpham"); ?>" title="<?php echo $v->name; ?>">
					<img style="width:90px;height:106px" src="<?php echo  $v->image; ?>" class="img-responsive">
				  </a>
				  <div class="entry-name"><a href="<?php echo $SEO->build_link($v,"sanpham"); ?>" title="<?php echo $v->name; ?>"><?php echo $v->name; ?></a></div>
				  <div class="entry-price">
					  <span class="price-sale"><?php echo number_format($price); ?> đ</span>
				  </div>
				</li>
				<?php 
			 } 
			 ?>  
          </ul>
          </div>
        </div>
      </div>

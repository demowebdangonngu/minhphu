
<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="ltr" lang="vi" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="ltr" lang="vi" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" class="ltr" lang="vi">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Giỏ Hàng</title>
    <base href="http://camerano1.com/" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="http://camerano1.com/image/catalog/icon_camera.png" rel="icon" />
    <link href="catalog/view/theme/megashop/stylesheet/bootstrap.css" rel="stylesheet" />
    <link href="catalog/view/theme/megashop/stylesheet/stylesheet.css" rel="stylesheet" />
    <link href="catalog/view/theme/megashop/stylesheet/font.css" rel="stylesheet" />
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="catalog/view/javascript/jquery/magnific/magnific-popup.css" rel="stylesheet" />
    <script type="text/javascript" src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="catalog/view/javascript/common.js"></script>
    <script type="text/javascript" src="catalog/view/theme/megashop/javascript/common.js"></script>
    
    
</head>
<body class="main-menu-fixed checkout-cart page-checkout-cart layout-fullwidth">

    <section  class="row-offcanvas row-offcanvas-left">

        <div id="page">
            <section id="header-top" >
                <div id="topbar">
                    <div class="container">
                        <div class="show-desktop">
                            <div class="quick-access pull-left  hidden-sm hidden-xs">
                                <div class="login links link-active">
                                    <a href="http://camerano1.com/account/register.html">Đăng ký</a> or
                                    <a href="http://camerano1.com/account/login.html">Đăng nhập</a>
                                </div>
                            </div>
                            <!--Button -->
                            <div class="quick-top-link  links pull-right">
                                <!-- language -->
                                <div class="btn-group box-language">
                                </div>
                                <!-- currency -->
                                <div class="btn-group box-currency">
                                    <div class="quick-access">
                                        <form action="http://camerano1.com/common/currency/currency.html" method="post" enctype="multipart/form-data" id="currency">
                                          <div class="btn-groups">
                                            <div type="button" class="dropdown-toggle" data-toggle="dropdown">
                                                <span>VNĐ</span>
                                                <span>Tiền tệ </span>
                                                <i class="fa fa-angle-down"></i>
                                            </div>
                                            <div class="dropdown-menu ">
                                                <div class="box-currency inner">
                                                  <a class="currency-select" type="button" name="EUR">
                                                      €                  </a>
                                                      <a class="currency-select" type="button" name="GBP">
                                                          VNĐ                  </a>
                                                          <a class="currency-select" type="button" name="USD">
                                                              $                  </a>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <input type="hidden" name="code" value="" />
                                                  <input type="hidden" name="redirect" value="http://camerano1.com/checkout/cart.html" />
                                              </form>
                                          </div>
                                      </div>
                                      <!-- user-->
                                      <div class="btn-group box-user">
                                        <div data-toggle="dropdown">
                                            <span>Tài khoản cá nhân</span>
                                            <i class="fa fa-angle-down "></i>
                                        </div>

                                        <ul class="dropdown-menu setting-menu">
                                            <li id="wishlist">
                                                <a href="http://camerano1.com/account/wishlist.html" id="wishlist-total"><i class="fa fa-list-alt"></i>&nbsp;&nbsp;Yêu thích (0)</a>
                                            </li>
                                            <li class="acount">
                                                <a href="http://camerano1.com/account/account.html"><i class="fa fa-user"></i>&nbsp;&nbsp;Tài khoản cá nhân</a>
                                            </li>
                                            <li class="shopping-cart">
                                                <a href="http://camerano1.com/checkout/cart.html"><i class="fa fa-bookmark"></i>&nbsp;&nbsp;Giỏ hàng</a>
                                            </li>
                                            <li class="checkout">
                                                <a class="last" href="http://camerano1.com/checkout/checkout.html"><i class="fa fa-share"></i>&nbsp;&nbsp;Thanh toán</a>
                                            </li>
                                        </ul>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- header -->
                    <header id="header">
                        <div id="header-main" class="hasmainmenu">
                            <div class="container">
                                <div class="row">
                                    <div class="logo inner  col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                        <div id="logo-theme" class="logo-store pull-left">
                                            <a href="http://camerano1.com">
                                                <span>CAMERA QUAN SÁT -- CAMERA AN NINH -- THIẾT BỊ VĂN PHÒNG</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="inner col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class=" search_block">
                                            <form method="GET" action="index.php" class="input-group search-box">
                                              <div class="filter_type category_filter input-group-addon group-addon-select no-padding icon-select">
                                                  <select name="category_id" class="no-border">
                                                     <option value="0">Tất cả sản phẩm</option>
                                                     <option value="73">Camera  IP</option>
                                                     <option value="74">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera  IP AVtech</option>
                                                     <option value="77">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera  IP HDtech</option>
                                                     <option value="75">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera  IP Questek</option>
                                                     <option value="76">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera  IP Vantech</option>
                                                     <option value="67">Camera quan sát</option>
                                                     <option value="68">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vantech</option>
                                                     <option value="102">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera AHD Vantech</option>
                                                     <option value="100">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - CVI Vantech</option>
                                                     <option value="101">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - SDI Vantech</option>
                                                     <option value="103">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - TVI Vantech</option>
                                                     <option value="69">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Questek</option>
                                                     <option value="95">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera AHD Questek</option>
                                                     <option value="96">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera Analog Questek</option>
                                                     <option value="97">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - CVI Questek</option>
                                                     <option value="98">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - SDI Questek</option>
                                                     <option value="99">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera HD - TVI Questek</option>
                                                     <option value="70">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Vdtech</option>
                                                     <option value="94">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HDtech</option>
                                                     <option value="71">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sony</option>
                                                     <option value="72">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;samsung</option>
                                                     <option value="119">Linh kiện Camera</option>
                                                     <option value="83">Máy chấm công</option>
                                                     <option value="87">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Linh kiện máy chấm công</option>
                                                     <option value="85">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy chấm công thẻ giấy</option>
                                                     <option value="86">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy chấm công thẻ từ</option>
                                                     <option value="84">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy chấm công vân tay</option>
                                                     <option value="120">phần mềm </option>
                                                     <option value="118">thiết bị báo trộm</option>
                                                     <option value="78">Đầu ghi hình</option>
                                                     <option value="81">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Vantech</option>
                                                     <option value="110">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình AHD Vantech</option>
                                                     <option value="114">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình Analog Vantech</option>
                                                     <option value="111">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình HD-CVI Vantech</option>
                                                     <option value="113">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình HD-SDI Vantech</option>
                                                     <option value="82">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;questek</option>
                                                     <option value="105">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ahd Questek</option>
                                                     <option value="106">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Analog Questek</option>
                                                     <option value="107">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HD-CVI Questek</option>
                                                     <option value="112">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình HD-SDI Questek</option>
                                                     <option value="79">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Samsung</option>
                                                     <option value="104">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HDtech</option>
                                                     <option value="80">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VDtech</option>
                                                     <option value="115">Đầu ghi hình IP</option>
                                                     <option value="116">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình IP Questek</option>
                                                     <option value="117">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Đầu ghi hình IP Vantech</option>
                                                     <option value="20">Tổng đài điện thoại</option>
                                                     <option value="59">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ghi âm điện thoại</option>
                                                     <option value="60">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Panasonic</option>
                                                     <option value="26">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Linh kiện điện thoại</option>
                                                     <option value="27">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Điện thoại bàn</option>
                                                     <option value="18">Thiết bị phòng game</option>
                                                     <option value="62">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy tính Intel giá rẽ</option>
                                                     <option value="61">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;máy server bootrom cao cấp</option>
                                                     <option value="45">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bàn gế phòng nét</option>
                                                     <option value="46">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy chủ game mainboard Intel</option>
                                                     <option value="122">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy tính Intel cao cấp</option>
                                                     <option value="123">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy tính Intel VIP</option>
                                                     <option value="121">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Máy tính Intel tầm trung</option>
                                                     <option value="57">Thiết bị mạng</option>
                                                     <option value="17">Linh kiện máy tính</option>
                                                     <option value="24">thiết bị văn phòng</option>
                                                     <option value="33">Trọn bộ Camera quan sát </option>
                                                     <option value="25">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camera quan sát Gói tiết kiệm</option>
                                                     <option value="29">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 camera</option>
                                                     <option value="28">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 camera</option>
                                                     <option value="30">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8 camera</option>
                                                     <option value="31">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;16 camera</option>
                                                     <option value="32">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Camera quan sát gói Cao cấp</option>
                                                     <option value="88">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 camera</option>
                                                     <option value="89">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4 camera</option>
                                                     <option value="90">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8 camera</option>
                                                     <option value="91">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;16 camera</option>
                                                 </select>
                                             </div>
                                             <div id="search0" class="search-input-wrapper clearfix clearfixs input-group">
                                               <input type="text" value="" size="35" autocomplete="off" placeholder="Tìm sản phẩm.." name="search" class="form-control input-lg input-search">

                                               <span class="input-group-addon input-group-search">
                                                <span class="fa fa-search"></span>
                                                <span class="button-search ">
                                                    Tìm sản phẩm..                    </span>
                                                </span>
                                            </div>
                                            <input type="hidden" name="sub_category" value="1" id="sub_category"/>
                                            <input type="hidden" name="route" value="product/search"/>
                                            <input type="hidden" name="sub_category" value="true" id="sub_category"/>
                                            <input type="hidden" name="description" value="true" id="description"/>
                                        </form>
                                        <div class="clear clr"></div>
                                    </div>
                                    <script type="text/javascript">

                                        /* Autocomplete */
                                        (function($) {
                                           function Autocomplete1(element, options) {
                                              this.element = element;
                                              this.options = options;
                                              this.timer = null;
                                              this.items = new Array();

                                              $(element).attr('autocomplete', 'off');
                                              $(element).on('focus', $.proxy(this.focus, this));
                                              $(element).on('blur', $.proxy(this.blur, this));
                                              $(element).on('keydown', $.proxy(this.keydown, this));

                                              $(element).after('<ul class="dropdown-menu autosearch"></ul>');
                                              $(element).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));
                                          }

                                          Autocomplete1.prototype = {
                                              focus: function() {
                                                 this.request();
                                             },
                                             blur: function() {
                                                 setTimeout(function(object) {
                                                    object.hide();
                                                }, 200, this);
                                             },
                                             click: function(event) {
                                                 event.preventDefault();
                                                 value = $(event.target).parent().attr("href");
                                                 if (value) {
                                                    window.location = value.replace(/&amp;/gi,'&');
                                                }
                                            },
                                            keydown: function(event) {
                                             switch(event.keyCode) {
				case 27: // escape
               this.hide();
               break;
               default:
               this.request();
               break;
           }
       },
       show: function() {
         var pos = $(this.element).position();

         $(this.element).siblings('ul.dropdown-menu').css({
            top: pos.top + $(this.element).outerHeight(),
            left: pos.left
        });

         $(this.element).siblings('ul.dropdown-menu').show();
     },
     hide: function() {
         $(this.element).siblings('ul.dropdown-menu').hide();
     },
     request: function() {
         clearTimeout(this.timer);

         this.timer = setTimeout(function(object) {
            object.options.source($(object.element).val(), $.proxy(object.response, object));
        }, 200, this);
     },
     response: function(json) {
         console.log(json);
         html = '';

         if (json.length) {
            for (i = 0; i < json.length; i++) {
               this.items[json[i]['value']] = json[i];
           }

           for (i = 0; i < json.length; i++) {
               if (!json[i]['category']) {
                  html += '<li class="media" data-value="' + json[i]['value'] + '">';
                  if(json[i]['simage']) {
                     html += '	<a class="media-left" href="' + json[i]['link'] + '"><img  src="' + json[i]['image'] + '"></a>';	
                 }
                 html += '<div class="media-body">';
                 html += '	<a  href="' + json[i]['link'] + '"><span>' + json[i]['label'] + '</span></a>';
                 if(json[i]['sprice']){
                     html += '	<div>';
                     if (!json[i]['special']) {
                        html += json[i]['price'];
                    } else {
                        html += '<span class="price-old">' + json[i]['price'] + '</span><span class="price-new">' + json[i]['special'] + '</span>';
                    }
                    if (json[i]['tax']) {
                        html += '<br />';
                        html += '<span class="price-tax">text_tax : ' + json[i]['tax'] + '</span>';
                    }
                    html += '	</div> </div>';
                }
                html += '</li>';
            }
        }
				//html += '<li><a href="index.php?route=product/search&search='+g.term+'&category_id='+category_id+'&sub_category=true&description=true" onclick="window.location=this.href">'+text_view_all+'</a></li>';

				// Get all the ones with a categories
				var category = new Array();
				for (i = 0; i < json.length; i++) {
					if (json[i]['category']) {
						if (!category[json[i]['category']]) {
							category[json[i]['category']] = new Array();
							category[json[i]['category']]['name'] = json[i]['category'];
							category[json[i]['category']]['item'] = new Array();
						}
						category[json[i]['category']]['item'].push(json[i]);
					}
				}
				for (i in category) {
					html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';
					for (j = 0; j < category[i]['item'].length; j++) {
						html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
					}
				}
			}
			if (html) {
				this.show();
			} else {
				this.hide();
			}
			$(this.element).siblings('ul.dropdown-menu').html(html);
		}
	};

	$.fn.autocomplete1 = function(option) {
		return this.each(function() {
			var data = $(this).data('autocomplete');
			if (!data) {
				data = new Autocomplete1(this, option);
				$(this).data('autocomplete', data);
			}
		});
	}
})(window.jQuery);
$(document).ready(function() {
	var selector = '#search0';
	var total = 0;
	var show_image = true;
	var show_price = true;
	var search_sub_category = true;
	var search_description = true;
	var width = 102;
	var height = 102;

	$(selector).find('input[name=\'search\']').autocomplete1({
		delay: 500,
		source: function(request, response) {
			var category_id = $(".category_filter select[name=\"category_id\"]").first().val();
			if(typeof(category_id) == 'undefined')
				category_id = 0;
			var limit = 5;
			var search_sub_category = search_sub_category?'&sub_category=true':'';
			var search_description = search_description?'&description=true':'';
			$.ajax({
				url: 'index.php?route=module/pavautosearch/autocomplete&filter_category_id='+category_id+'&width='+width+'&height='+height+'&limit='+limit+search_sub_category+search_description+'&filter_name='+encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {		
					response($.map(json, function(item) {
						if($('.pavautosearch_result')){
							$('.pavautosearch_result').first().html("");
						}
						total = 0;
						if(item.total){
							total = item.total;
						}
						return {
							price:   item.price,
							speical: item.special,
							tax:     item.tax,
							label:   item.name,
							image:   item.image,
							link:    item.link,
							value:   item.product_id,
							sprice:  show_price,
							simage:  show_image,
						}
					}));
				}
			});
		},
	}); // End Autocomplete 

});// End document.ready

</script>                </div>
<div id="hidemenu" class=" inner col-lg-3 col-md-3 col-sm-3 hidden-xs">
    <div class="cart-top">
        <div id="cart" class="btn-group btn-block">
            <div type="button" data-toggle="dropdown" data-loading-text="Loading..." class="heading dropdown-toggle">
                <div class="cart-inner pull-right">
                    <div class="icon-cart"></div>
                    <h4>Xem chi tiết giỏ hàng</h4>
                    <a><span id="cart-total">1 sản phẩm - 5.700.000VNĐ<i class="fa fa-angle-down"></i></span></a>
                </div>
            </div>
            <ul class="dropdown-menu pull-right content">
                <li>
                    <table class="table table-striped">
                        <tr>
                            <td class="text-center">                        <a href="http://camerano1.com/D-u-ghi-hinh/Vantech/D-u-ghi-hinh-AHD-Vantech/VP-1660AHDM-p2179c78c81c110.html"><img src="http://camerano1.com/image/cache/catalog/DAUGHIHINH/VANTECH/p_12328_VANTECH-VP-460AHDM-70x71.jpg" alt="VP-1660AHDM" title="VP-1660AHDM" class="img-thumbnail" /></a>
                            </td>
                            <td class="text-left"><a href="http://camerano1.com/D-u-ghi-hinh/Vantech/D-u-ghi-hinh-AHD-Vantech/VP-1660AHDM-p2179c78c81c110.html">VP-1660AHDM</a>
                            </td>
                            <td class="text-right">x 1</td>
                            <td class="text-right">5.700.000VNĐ</td>
                            <td class="text-center"><button type="button" onclick="cart.remove('YToxOntzOjEwOiJwcm9kdWN0X2lkIjtpOjIxNzk7fQ==');" title="Loại bỏ" class="btn btn-link btn-xs"><i class="fa fa-times"></i></button></td>
                        </tr>
                    </table>
                </li>
                <li>
                    <div>
                        <table class="table table-bordered">
                            <tr>
                                <td class="text-right"><strong>Thành tiền:</strong></td>
                                <td class="text-right">5.700.000VNĐ</td>
                            </tr>
                            <tr>
                                <td class="text-right"><strong>Tổng cộng :</strong></td>
                                <td class="text-right">5.700.000VNĐ</td>
                            </tr>
                        </table>
                        <p class="text-right">
                            <a href="http://camerano1.com/checkout/cart.html" class="btn btn-inverse">
                                Xem chi tiết giỏ hàng                    </a>&nbsp;&nbsp;&nbsp;
                                <a href="http://camerano1.com/checkout/checkout.html" class="btn btn-outline">
                                    Thanh toán                    </a>
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</header>

<section id="pav-masshead" class="pav-masshead aligned-right">
    <div class="container">
        <div class="inner row clearfix">
            <div id="pav-mainnav">
                <div class="container">
                    

                    <div class="pav-megamenu">
                       <div class="navbar navbar-mega" role="navigation">
                          <div id="mainmenutop" class="megamenu" role="navigation">
                            <div class="show-mobile hidden-lg hidden-md">
                                <div class="quick-access pull-left">
                                    <button data-toggle="offcanvas" class="btn btn-outline visible-xs visible-sm" type="button"><span class="fa fa-bars"></span></button>
                                </div>
                            </div>
                            <div class="collapse navbar-collapse" id="bs-megamenu">
                                <ul class="nav navbar-nav megamenu"><li class="home" >
                                   <a href="http://camerano1.com/"><span class="menu-title">TRANG CHỦ</span></a></li><li class="parent dropdown " >
                                   <a class="dropdown-toggle" data-toggle="dropdown" href="pavblog/category&amp;id=25"><span class="menu-title">GIẢI PHÁP </span><span class="menu-desc">GIẢI PHÁP </span><b class="caret"></b></a><div class="dropdown-menu level1"  ><div class="dropdown-menu-inner"><div class="row"><div class="col-sm-12 mega-col" data-colwidth="12" data-type="menu" ><div class="mega-col-inner"><ul><li class=" " ><a href="pavblog/category&amp;id=26"><span class="menu-title">LẮP ĐẶT CAMERA QUAN SÁT</span><span class="menu-desc">LẮP ĐẶT CAMERA QUAN SÁT</span></a></li><li class=" " ><a href="pavblog/category&amp;id=27"><span class="menu-title">LẮP ĐẶT THIẾT BỊ CHỐNG TRỘM</span></a></li><li class=" " ><a href="pavblog/category&amp;id=28"><span class="menu-title">LẮP ĐẶT TỔNG ĐÀI ĐIỆN THOẠI</span></a></li><li class=" " ><a href="pavblog/category.html?id=40"><span class="menu-title">BÃO TRÌ MÁY TÍNH</span><span class="menu-desc">BÃO TRÌ MÁY TÍNH</span></a></li><li class=" " ><a href=""><span class="menu-title">BÃO TRÌ MÁY CHỦ - SERVER</span></a></li><li class=" " ><a href=""><span class="menu-title">THI CÔNG MẠNG LAN - WAN</span><span class="menu-desc">THI CÔNG MẠNG LAN - WAN</span></a></li></ul></div></div></div></li><li class="parent dropdown " >
                                   <a class="dropdown-toggle" data-toggle="dropdown" href="pavblog/category&amp;id=29"><span class="menu-title">GAME SERVER</span><b class="caret"></b></a><div class="dropdown-menu level1"  ><div class="dropdown-menu-inner"><div class="row"><div class="col-sm-12 mega-col" data-colwidth="12" data-type="menu" ><div class="mega-col-inner"><ul><li class=" " ><a href="index.php?route=pavblog/category&amp;id=33"><span class="menu-title">PHÒNG GAMES ĐẸP</span></a></li><li class=" " ><a href="index.php?route=pavblog/category&amp;id=31"><span class="menu-title">NÂNG CẤP ROOM GAME</span></a></li><li class=" " ><a href="index.php?route=pavblog/category&amp;id=32"><span class="menu-title">CÀI ĐẶT SERVER </span></a></li></ul></div></div></div></li><li class="" >
                                   <a href="pavblog/category&amp;id=34"><span class="menu-title">DEMO CAMERA</span></a></li><li class="" >
                                   <a href="pavblog/category&amp;id=36"><span class="menu-title">DỰ ÁN</span></a></li><li class="" >
                                   <a href="pavblog/category&amp;id=37"><span class="menu-title">BẢNG GIÁ</span></a></li><li class="" >
                                   <a href=""><span class="menu-title"></span></a></li></ul>			</div>
                               </div>
                           </div>
                       </div>

                   </div>
               </div>

           </div>
       </div>
   </section>



   <!-- /header -->
</section>
<!-- sys-notification -->
<div id="sys-notification">
    <div class="container">
        <div id="notification"></div>
    </div>
</div>
<!-- /sys-notification -->

<div class="container">
  <ul class="breadcrumb">
    <li><a href="http://camerano1.com">Trang chủ</a></li>
    <li><a href="http://camerano1.com/checkout/cart.html">Giỏ Hàng</a></li>
</ul>        <div class="row"> 

<section id="sidebar-main" class="col-md-12"><div id="content" class="wrapper clearfix">      <h1>Giỏ Hàng              </h1>
  <form action="http://camerano1.com/checkout/cart/edit.html" method="post" enctype="multipart/form-data">
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <td class="text-center">Hình ảnh</td>
            <td class="text-left">Tên sản phẩm</td>
            <td class="text-left">Mã hàng</td>
            <td class="text-left">Số lượng</td>
            <td class="text-right">Đơn Giá</td>
            <td class="text-right">Tổng cộng</td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">                  <a href="http://camerano1.com/D-u-ghi-hinh/Vantech/D-u-ghi-hinh-AHD-Vantech/VP-1660AHDM-p2179c78c81c110.html"><img src="http://camerano1.com/image/cache/catalog/DAUGHIHINH/VANTECH/p_12328_VANTECH-VP-460AHDM-70x71.jpg" alt="VP-1660AHDM" title="VP-1660AHDM" class="img-thumbnail" /></a>
            </td>
            <td class="text-left"><a href="http://camerano1.com/D-u-ghi-hinh/Vantech/D-u-ghi-hinh-AHD-Vantech/VP-1660AHDM-p2179c78c81c110.html">VP-1660AHDM</a>
            </td>
            <td class="text-left">VP-1660AHDM</td>
            <td class="text-left"><div class="input-group btn-block" style="max-width: 200px;">
                <input type="text" name="quantity[YToxOntzOjEwOiJwcm9kdWN0X2lkIjtpOjIxNzk7fQ==]" value="1" size="1" class="form-control" />
                <span class="input-group-btn">
                    <button type="submit" data-toggle="tooltip" title="Cập nhật" class="btn btn-outline"><i class="fa fa-refresh"></i></button>
                    <button type="button" data-toggle="tooltip" title="Loại bỏ" class="btn btn-inverse" onclick="cart.remove('YToxOntzOjEwOiJwcm9kdWN0X2lkIjtpOjIxNzk7fQ==');"><i class="fa fa-times-circle"></i></button></span></div></td>
                    <td class="text-right">5.700.000VNĐ</td>
                    <td class="text-right">5.700.000VNĐ</td>
                </tr>
            </tbody>
        </table>
    </div>
</form>
<h2>Bạn muốn làm gì tiếp theo?</h2>
<p>Chọn nếu bạn muốn sử dụng mã giảm giá, điểm thưởng hoặc bạn muốn ước tính phí vận chuyển.</p>
<div class="panel-group" id="accordion"><div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title"><a href="#collapse-coupon" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">Use Coupon Code <i class="fa fa-caret-down"></i></a></h4>
</div>
<div id="collapse-coupon" class="panel-collapse collapse">
    <div class="panel-body">
      <label class="col-sm-2 control-label" for="input-coupon">Enter your coupon here</label>
      <div class="input-group">
        <input type="text" name="coupon" value="" placeholder="Enter your coupon here" id="input-coupon" class="form-control" />
        <span class="input-group-btn">
            <input type="button" value="Áp dụng giảm giá" id="button-coupon" data-loading-text="Loading..."  class="btn btn-outline" />
        </span></div>
        <script type="text/javascript"><!--
            $('#button-coupon').on('click', function() {
               $.ajax({
                  url: 'index.php?route=checkout/coupon/coupon',
                  type: 'post',
                  data: 'coupon=' + encodeURIComponent($('input[name=\'coupon\']').val()),
                  dataType: 'json',
                  beforeSend: function() {
                     $('#button-coupon').button('loading');
                 },
                 complete: function() {
                     $('#button-coupon').button('reset');
                 },
                 success: function(json) {
                     $('.alert').remove();

                     if (json['error']) {
                        $('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    }

                    if (json['redirect']) {
                        location = json['redirect'];
                    }
                }
            });
});
//--></script>
</div>
</div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title"><a href="#collapse-voucher" data-toggle="collapse" data-parent="#accordion" class="accordion-toggle">Mua Phiếu Quà Tặng <i class="fa fa-caret-down"></i></a></h4>
</div>
<div id="collapse-voucher" class="panel-collapse collapse">
    <div class="panel-body">
      <label class="col-sm-2 control-label" for="input-voucher">Enter your gift voucher code here</label>
      <div class="input-group">
        <input type="text" name="voucher" value="" placeholder="Enter your gift voucher code here" id="input-voucher" class="form-control" />
        <span class="input-group-btn">
            <input type="submit" value="Áp dụng Voucher" id="button-voucher" data-loading-text="Loading..."  class="btn btn-outline" />
        </span> </div>
        <script type="text/javascript"><!--
            $('#button-voucher').on('click', function() {
              $.ajax({
                url: 'index.php?route=checkout/voucher/voucher',
                type: 'post',
                data: 'voucher=' + encodeURIComponent($('input[name=\'voucher\']').val()),
                dataType: 'json',
                beforeSend: function() {
                  $('#button-voucher').button('loading');
              },
              complete: function() {
                  $('#button-voucher').button('reset');
              },
              success: function(json) {
                  $('.alert').remove();

                  if (json['error']) {
                    $('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');
                }

                if (json['redirect']) {
                    location = json['redirect'];
                }
            }
        });
});
//--></script>
</div>
</div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title"><a href="#collapse-shipping" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion">Estimate Shipping &amp; Taxes <i class="fa fa-caret-down"></i></a></h4>
</div>
<div id="collapse-shipping" class="panel-collapse collapse">
    <div class="panel-body">
      <p>Enter your destination to get a shipping estimate.</p>
      <form class="form-horizontal">
        <div class="form-group required">
          <label class="col-sm-2 control-label" for="input-country">Country</label>
          <div class="col-sm-10">
            <select name="country_id" id="input-country" class="form-control">
              <option value=""> --- Chọn --- </option>
              <option value="244">Aaland Islands</option>
              <option value="1">Afghanistan</option>
              <option value="2">Albania</option>
              <option value="3">Algeria</option>
              <option value="4">American Samoa</option>
              <option value="5">Andorra</option>
              <option value="6">Angola</option>
              <option value="7">Anguilla</option>
              <option value="8">Antarctica</option>
              <option value="9">Antigua and Barbuda</option>
              <option value="10">Argentina</option>
              <option value="11">Armenia</option>
              <option value="12">Aruba</option>
              <option value="252">Ascension Island (British)</option>
              <option value="13">Australia</option>
              <option value="14">Austria</option>
              <option value="15">Azerbaijan</option>
              <option value="16">Bahamas</option>
              <option value="17">Bahrain</option>
              <option value="18">Bangladesh</option>
              <option value="19">Barbados</option>
              <option value="20">Belarus</option>
              <option value="21">Belgium</option>
              <option value="22">Belize</option>
              <option value="23">Benin</option>
              <option value="24">Bermuda</option>
              <option value="25">Bhutan</option>
              <option value="26">Bolivia</option>
              <option value="245">Bonaire, Sint Eustatius and Saba</option>
              <option value="27">Bosnia and Herzegovina</option>
              <option value="28">Botswana</option>
              <option value="29">Bouvet Island</option>
              <option value="30">Brazil</option>
              <option value="31">British Indian Ocean Territory</option>
              <option value="32">Brunei Darussalam</option>
              <option value="33">Bulgaria</option>
              <option value="34">Burkina Faso</option>
              <option value="35">Burundi</option>
              <option value="36">Cambodia</option>
              <option value="37">Cameroon</option>
              <option value="38">Canada</option>
              <option value="251">Canary Islands</option>
              <option value="39">Cape Verde</option>
              <option value="40">Cayman Islands</option>
              <option value="41">Central African Republic</option>
              <option value="42">Chad</option>
              <option value="43">Chile</option>
              <option value="44">China</option>
              <option value="45">Christmas Island</option>
              <option value="46">Cocos (Keeling) Islands</option>
              <option value="47">Colombia</option>
              <option value="48">Comoros</option>
              <option value="49">Congo</option>
              <option value="50">Cook Islands</option>
              <option value="51">Costa Rica</option>
              <option value="52">Cote D'Ivoire</option>
              <option value="53">Croatia</option>
              <option value="54">Cuba</option>
              <option value="246">Curacao</option>
              <option value="55">Cyprus</option>
              <option value="56">Czech Republic</option>
              <option value="237">Democratic Republic of Congo</option>
              <option value="57">Denmark</option>
              <option value="58">Djibouti</option>
              <option value="59">Dominica</option>
              <option value="60">Dominican Republic</option>
              <option value="61">East Timor</option>
              <option value="62">Ecuador</option>
              <option value="63">Egypt</option>
              <option value="64">El Salvador</option>
              <option value="65">Equatorial Guinea</option>
              <option value="66">Eritrea</option>
              <option value="67">Estonia</option>
              <option value="68">Ethiopia</option>
              <option value="69">Falkland Islands (Malvinas)</option>
              <option value="70">Faroe Islands</option>
              <option value="71">Fiji</option>
              <option value="72">Finland</option>
              <option value="74">France, Metropolitan</option>
              <option value="75">French Guiana</option>
              <option value="76">French Polynesia</option>
              <option value="77">French Southern Territories</option>
              <option value="126">FYROM</option>
              <option value="78">Gabon</option>
              <option value="79">Gambia</option>
              <option value="80">Georgia</option>
              <option value="81">Germany</option>
              <option value="82">Ghana</option>
              <option value="83">Gibraltar</option>
              <option value="84">Greece</option>
              <option value="85">Greenland</option>
              <option value="86">Grenada</option>
              <option value="87">Guadeloupe</option>
              <option value="88">Guam</option>
              <option value="89">Guatemala</option>
              <option value="256">Guernsey</option>
              <option value="90">Guinea</option>
              <option value="91">Guinea-Bissau</option>
              <option value="92">Guyana</option>
              <option value="93">Haiti</option>
              <option value="94">Heard and Mc Donald Islands</option>
              <option value="95">Honduras</option>
              <option value="96">Hong Kong</option>
              <option value="97">Hungary</option>
              <option value="98">Iceland</option>
              <option value="99">India</option>
              <option value="100">Indonesia</option>
              <option value="101">Iran (Islamic Republic of)</option>
              <option value="102">Iraq</option>
              <option value="103">Ireland</option>
              <option value="254">Isle of Man</option>
              <option value="104">Israel</option>
              <option value="105">Italy</option>
              <option value="106">Jamaica</option>
              <option value="107">Japan</option>
              <option value="257">Jersey</option>
              <option value="108">Jordan</option>
              <option value="109">Kazakhstan</option>
              <option value="110">Kenya</option>
              <option value="111">Kiribati</option>
              <option value="113">Korea, Republic of</option>
              <option value="253">Kosovo, Republic of</option>
              <option value="114">Kuwait</option>
              <option value="115">Kyrgyzstan</option>
              <option value="116">Lao People's Democratic Republic</option>
              <option value="117">Latvia</option>
              <option value="118">Lebanon</option>
              <option value="119">Lesotho</option>
              <option value="120">Liberia</option>
              <option value="121">Libyan Arab Jamahiriya</option>
              <option value="122">Liechtenstein</option>
              <option value="123">Lithuania</option>
              <option value="124">Luxembourg</option>
              <option value="125">Macau</option>
              <option value="127">Madagascar</option>
              <option value="128">Malawi</option>
              <option value="129">Malaysia</option>
              <option value="130">Maldives</option>
              <option value="131">Mali</option>
              <option value="132">Malta</option>
              <option value="133">Marshall Islands</option>
              <option value="134">Martinique</option>
              <option value="135">Mauritania</option>
              <option value="136">Mauritius</option>
              <option value="137">Mayotte</option>
              <option value="138">Mexico</option>
              <option value="139">Micronesia, Federated States of</option>
              <option value="140">Moldova, Republic of</option>
              <option value="141">Monaco</option>
              <option value="142">Mongolia</option>
              <option value="242">Montenegro</option>
              <option value="143">Montserrat</option>
              <option value="144">Morocco</option>
              <option value="145">Mozambique</option>
              <option value="146">Myanmar</option>
              <option value="147">Namibia</option>
              <option value="148">Nauru</option>
              <option value="149">Nepal</option>
              <option value="150">Netherlands</option>
              <option value="151">Netherlands Antilles</option>
              <option value="152">New Caledonia</option>
              <option value="153">New Zealand</option>
              <option value="154">Nicaragua</option>
              <option value="155">Niger</option>
              <option value="156">Nigeria</option>
              <option value="157">Niue</option>
              <option value="158">Norfolk Island</option>
              <option value="112">North Korea</option>
              <option value="159">Northern Mariana Islands</option>
              <option value="160">Norway</option>
              <option value="161">Oman</option>
              <option value="162">Pakistan</option>
              <option value="163">Palau</option>
              <option value="247">Palestinian Territory, Occupied</option>
              <option value="164">Panama</option>
              <option value="165">Papua New Guinea</option>
              <option value="166">Paraguay</option>
              <option value="167">Peru</option>
              <option value="168">Philippines</option>
              <option value="169">Pitcairn</option>
              <option value="170">Poland</option>
              <option value="171">Portugal</option>
              <option value="172">Puerto Rico</option>
              <option value="173">Qatar</option>
              <option value="174">Reunion</option>
              <option value="175">Romania</option>
              <option value="176">Russian Federation</option>
              <option value="177">Rwanda</option>
              <option value="178">Saint Kitts and Nevis</option>
              <option value="179">Saint Lucia</option>
              <option value="180">Saint Vincent and the Grenadines</option>
              <option value="181">Samoa</option>
              <option value="182">San Marino</option>
              <option value="183">Sao Tome and Principe</option>
              <option value="184">Saudi Arabia</option>
              <option value="185">Senegal</option>
              <option value="243">Serbia</option>
              <option value="186">Seychelles</option>
              <option value="187">Sierra Leone</option>
              <option value="188">Singapore</option>
              <option value="189">Slovak Republic</option>
              <option value="190">Slovenia</option>
              <option value="191">Solomon Islands</option>
              <option value="192">Somalia</option>
              <option value="193">South Africa</option>
              <option value="194">South Georgia &amp; South Sandwich Islands</option>
              <option value="248">South Sudan</option>
              <option value="195">Spain</option>
              <option value="196">Sri Lanka</option>
              <option value="249">St. Barthelemy</option>
              <option value="197">St. Helena</option>
              <option value="250">St. Martin (French part)</option>
              <option value="198">St. Pierre and Miquelon</option>
              <option value="199">Sudan</option>
              <option value="200">Suriname</option>
              <option value="201">Svalbard and Jan Mayen Islands</option>
              <option value="202">Swaziland</option>
              <option value="203">Sweden</option>
              <option value="204">Switzerland</option>
              <option value="205">Syrian Arab Republic</option>
              <option value="206">Taiwan</option>
              <option value="207">Tajikistan</option>
              <option value="208">Tanzania, United Republic of</option>
              <option value="209">Thailand</option>
              <option value="210">Togo</option>
              <option value="211">Tokelau</option>
              <option value="212">Tonga</option>
              <option value="213">Trinidad and Tobago</option>
              <option value="255">Tristan da Cunha</option>
              <option value="214">Tunisia</option>
              <option value="215">Turkey</option>
              <option value="216">Turkmenistan</option>
              <option value="217">Turks and Caicos Islands</option>
              <option value="218">Tuvalu</option>
              <option value="219">Uganda</option>
              <option value="220">Ukraine</option>
              <option value="221">United Arab Emirates</option>
              <option value="222">United Kingdom</option>
              <option value="223">United States</option>
              <option value="224">United States Minor Outlying Islands</option>
              <option value="225">Uruguay</option>
              <option value="226">Uzbekistan</option>
              <option value="227">Vanuatu</option>
              <option value="228">Vatican City State (Holy See)</option>
              <option value="229">Venezuela</option>
              <option value="230" selected="selected">Viet Nam</option>
              <option value="231">Virgin Islands (British)</option>
              <option value="232">Virgin Islands (U.S.)</option>
              <option value="233">Wallis and Futuna Islands</option>
              <option value="234">Western Sahara</option>
              <option value="235">Yemen</option>
              <option value="238">Zambia</option>
              <option value="239">Zimbabwe</option>
          </select>
      </div>
  </div>
  <div class="form-group required">
      <label class="col-sm-2 control-label" for="input-zone">Region / State</label>
      <div class="col-sm-10">
        <select name="zone_id" id="input-zone" class="form-control">
        </select>
    </div>
</div>
<div class="form-group required">
  <label class="col-sm-2 control-label" for="input-postcode">Post Code</label>
  <div class="col-sm-10">
    <input type="text" name="postcode" value="" placeholder="Post Code" id="input-postcode" class="form-control" />
</div>
</div>
<input type="button" value="Lấy trích dẫn" id="button-quote" data-loading-text="Loading..." class="btn btn-outline" />
</form>
<script type="text/javascript"><!--
    $('#button-quote').on('click', function() {
       $.ajax({
          url: 'index.php?route=checkout/shipping/quote',
          type: 'post',
          data: 'country_id=' + $('select[name=\'country_id\']').val() + '&zone_id=' + $('select[name=\'zone_id\']').val() + '&postcode=' + encodeURIComponent($('input[name=\'postcode\']').val()),
          dataType: 'json',
          beforeSend: function() {
             $('#button-quote').button('loading');
         },
         complete: function() {
             $('#button-quote').button('reset');
         },
         success: function(json) {
             $('.alert, .text-danger').remove();

             if (json['error']) {
                if (json['error']['warning']) {
                   $('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                   $('html, body').animate({ scrollTop: 0 }, 'slow');
               }

               if (json['error']['country']) {
                   $('select[name=\'country_id\']').after('<div class="text-danger">' + json['error']['country'] + '</div>');
               }

               if (json['error']['zone']) {
                   $('select[name=\'zone_id\']').after('<div class="text-danger">' + json['error']['zone'] + '</div>');
               }

               if (json['error']['postcode']) {
                   $('input[name=\'postcode\']').after('<div class="text-danger">' + json['error']['postcode'] + '</div>');
               }
           }

           if (json['shipping_method']) {
            $('#modal-shipping').remove();

            html  = '<div id="modal-shipping" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <h4 class="modal-title">Please select the preferred shipping method to use on this order.</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">';

            for (i in json['shipping_method']) {
               html += '<p><strong>' + json['shipping_method'][i]['title'] + '</strong></p>';

               if (!json['shipping_method'][i]['error']) {
                  for (j in json['shipping_method'][i]['quote']) {
                     html += '<div class="radio">';
                     html += '  <label>';

                     if (json['shipping_method'][i]['quote'][j]['code'] == '') {
                        html += '<input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" checked="checked" />';
                    } else {
                        html += '<input type="radio" name="shipping_method" value="' + json['shipping_method'][i]['quote'][j]['code'] + '" />';
                    }

                    html += json['shipping_method'][i]['quote'][j]['title'] + ' - ' + json['shipping_method'][i]['quote'][j]['text'] + '</label></div>';
                }
            } else {
              html += '<div class="alert alert-danger">' + json['shipping_method'][i]['error'] + '</div>';
          }
      }

      html += '      </div>';
      html += '      <div class="modal-footer">';
      html += '        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>';

      html += '        <input type="button" value="Áp dụng vận chuyển" id="button-shipping" data-loading-text="Loading..." class="btn btn-outline" disabled="disabled" />';
      
      html += '      </div>';
      html += '    </div>';
      html += '  </div>';
      html += '</div> ';

      $('body').append(html);

      $('#modal-shipping').modal('show');

      $('input[name=\'shipping_method\']').on('change', function() {
       $('#button-shipping').prop('disabled', false);
   });
  }
}
});
});

$(document).delegate('#button-shipping', 'click', function() {
	$.ajax({
		url: 'index.php?route=checkout/shipping/shipping',
		type: 'post',
		data: 'shipping_method=' + encodeURIComponent($('input[name=\'shipping_method\']:checked').val()),
		dataType: 'json',
		beforeSend: function() {
			$('#button-shipping').button('loading');
		},
		complete: function() {
			$('#button-shipping').button('reset');
		},
		success: function(json) {
			$('.alert').remove();

			if (json['error']) {
				$('.breadcrumb').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');
			}

			if (json['redirect']) {
				location = json['redirect'];
			}
		}
	});
});
//--></script>
<script type="text/javascript"><!--
    $('select[name=\'country_id\']').on('change', function() {
       $.ajax({
          url: 'index.php?route=checkout/shipping/country&country_id=' + this.value,
          dataType: 'json',
          beforeSend: function() {
             $('select[name=\'country_id\']').after(' <i class="fa fa-circle-o-notch fa-spin"></i>');
         },
         complete: function() {
             $('.fa-spin').remove();
         },
         success: function(json) {
          $('.fa-spin').remove();

          if (json['postcode_required'] == '1') {
            $('input[name=\'postcode\']').parent().parent().addClass('required');
        } else {
            $('input[name=\'postcode\']').parent().parent().removeClass('required');
        }

        html = '<option value=""> --- Chọn --- </option>';

        if (json['zone'] != '') {
            for (i = 0; i < json['zone'].length; i++) {
               html += '<option value="' + json['zone'][i]['zone_id'] + '"';

               if (json['zone'][i]['zone_id'] == '') {
                  html += ' selected="selected"';
              }

              html += '>' + json['zone'][i]['name'] + '</option>';
          }
      } else {
        html += '<option value="0" selected="selected"> --- Không --- </option>';
    }

    $('select[name=\'zone_id\']').html(html);
},
error: function(xhr, ajaxOptions, thrownError) {
 alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
}
});
});

$('select[name=\'country_id\']').trigger('change');
//--></script>
</div>
</div>
</div></div>
<br />
<div class="row">
    <div class="col-sm-4 col-sm-offset-8">
      <table class="table table-bordered">
        <tr>
          <td class="text-right"><strong>Thành tiền::</strong></td>
          <td class="text-right">5.700.000VNĐ</td>
      </tr>
      <tr>
          <td class="text-right"><strong>Tổng cộng ::</strong></td>
          <td class="text-right">5.700.000VNĐ</td>
      </tr>
  </table>
</div>
</div>
<div class="buttons">
    <div class="pull-left"><a href="http://camerano1.com" class="btn btn-default">Tiếp tục mua hàng</a></div>
    <div class="pull-right"><a href="http://camerano1.com/checkout/checkout.html" class="btn btn-outline">Thanh toán</a></div>
</div>
</div>
</section> 
</div>
</div>
 <!-- 
  $ospans: allow overrides width of columns base on thiers indexs. format array( column-index=>span number ), example array( 1=> 3 )[value from 1->12]
-->






<footer id="footer">
   
  <section class="footer-top " id="pavo-footer-top">
    <div class="container">
        <div class="inner">
            <div class="row">
                <div class="col-md-3 col-sm-12">
                    <div class="box-content">                                            <div class="address">                                                <p><span style="color: #ffa200;">HỖ TRỢ KHÁCH HÀNG</span></p>                                                <div class="box-addres">                                                    <div class="link-address icon">                                                       <span class="fa fa-phone">&nbsp;</span><span>Phone: 0933.149.990</span></div>                                                    <div class="link-mobile icon">                                                        <span class="fa fa-phone">&nbsp;</span><span>Phone: 0168.258.6825</span>                                                    </div>                                                    <div class="link-mail icon">                                                        <span class="fa fa-envelope">&nbsp;</span>                                                        <span>Email: ledinhhung.ktp@gmail.com</span>                                                    </div>                                                </div>                                            </div>                                        </div>                </div>
                    <div class="col-md-2 col-sm-4">
                        <h5 class="box-heading"><span>Chăm sóc khách hàng</span></h5>
                        <ul class="list-unstyled">
                            <li><a href="http://camerano1.com/information/contact.html">Liên hệ</a></li>
                            <li><a href="http://camerano1.com/account/return/add.html">Trả hàng</a></li>
                            <li><a href="http://camerano1.com/information/sitemap.html">Sơ đồ trang</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <h5 class="box-heading"><span>Chức năng khác</span></h5>
                        <ul class="list-unstyled">
                            <li><a href="http://camerano1.com/product/manufacturer.html">Thương hiệu</a></li>
                            <li><a href="http://camerano1.com/account/voucher.html">Phiếu quà tặng</a></li>
                            <li><a href="http://camerano1.com/affiliate/account.html">Đại lý</a></li>
                            <li><a href="http://camerano1.com/product/special.html">Khuyến mãi</a></li>
                        </ul>
                    </div>
                    <div class="col-md-2 col-sm-4">
                        <h5 class="box-heading"><span>Tài khoản của tôi</span></h5>
                        <ul class="list-unstyled">
                            <li><a href="http://camerano1.com/account/account.html">Tài khoản của tôi</a></li>
                            <li><a href="http://camerano1.com/account/order.html">Lịch sử đơn hàng</a></li>
                            <li><a href="http://camerano1.com/account/wishlist.html">Danh sách yêu thích</a></li>
                            <li><a href="http://camerano1.com/account/newsletter.html">Thư thông báo</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="box pav-custom  ">                                        <div class="box-heading">                                            <span>LIÊN HỆ</span>                                        </div>                                        <div class="box-content">                                            <div class="address">                                                <p><span style="color: #ffa200;">TRUNG TÂM THIẾT BỊ AN NINH VÀ GIÁM SÁT CÔNG NGHỆ CAO TH </span></p>                                                <div class="box-addres">                                                    <div class="link-address icon">                                                        <span class="fa fa-map-marker">&nbsp;</span><span>6/1/16 DT743 ,Đồng an 3 ,Bình Hòa ,Thuận An, Bình Dương.</span>                                                    </div>                                                    <div class="link-mobile icon">                                                        <span class="fa fa-phone">&nbsp;</span><span>Phone: 0168.258.6825</span>                                                    </div>                                                    <div class="link-mail icon">                                                        <span class="fa fa-envelope">&nbsp;</span>                                                        <span>Email: support@gmail.com</span>                                                    </div>                                                </div>                                            </div>                                        </div>                                    </div>                </div>
                    </div>
                </div>
            </div>
        </section>
        


        

        
    </footer>


    <div class="outsite">
    </div>


</div>
<div class="sidebar-offcanvas  visible-xs visible-sm">
  <div class="offcanvas-inner panel panel-offcanvas">
      <div class="offcanvas-heading">
          <button type="button" class="btn btn-outline btn-close " data-toggle="offcanvas"> <span class="fa fa-times"></span></button>
      </div>
      <div class="offcanvas-body panel-body">
         
        <div class="category box box-highlights theme">
            <div class="box-heading"><span>Danh Mục</span></div>
            <div class="box-content tree-menu">
                <ul id="719247095accordion" class="box-category box-panel">
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/Camera-IP-c73.html">Camera  IP (0)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/Camera-quan-sat-c67.html">Camera quan sát (30)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/Linh-ki-n-Camera-c119.html">Linh kiện Camera (0)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/May-ch-m-cong-c83.html">Máy chấm công (0)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/ph-n-m-m-c120.html">phần mềm  (0)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/thi-t-b-bao-tr-m-c118.html">thiết bị báo trộm (0)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/D-u-ghi-hinh-c78.html">Đầu ghi hình (6)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/D-u-ghi-hinh-IP-c115.html">Đầu ghi hình IP (0)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/T-ng-dai-di-n-tho-i-c20.html">Tổng đài điện thoại (0)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/Thi-t-b-phong-game-c18.html">Thiết bị phòng game (22)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/Thi-t-b-m-ng-c57.html">Thiết bị mạng (0)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/Linh-ki-n-may-tinh-c17.html">Linh kiện máy tính (0)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/thi-t-b-van-phong-c24.html">thiết bị văn phòng (0)</a>
                    </li>
                    <li class="accordion-group clearfix">
                        <a href="http://camerano1.com/Tr-n-b-Camera-quan-sat-c33.html">Trọn bộ Camera quan sát  (12)</a>
                    </li>
                </ul>
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function(){
                var active = $('.collapse.in').attr('id');
                $('span[data-target=#'+active+']').html("-");

                $('.collapse').on('show.bs.collapse', function () {
                    $('span[data-target=#'+$(this).attr('id')+']').html("-");
                });
                $('.collapse').on('hide.bs.collapse', function () {
                    $('span[data-target=#'+$(this).attr('id')+']').html("+");
                });
            });
        </script>

    </div>
    <div class="offcanvas-footer panel-footer">
      <div class="input-group" id="offcanvas-search">
        <input type="text" class="form-control" placeholder="Search" value="" name="search">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
      </span>
  </div>
</div>
</div> 
</div></section>
























<script type="text/javascript">
    function hide_float_right() {
        var content = document.getElementById('float_content_right');
        var hide = document.getElementById('hide_float_right');
        if (content.style.display == "none")
            {content.style.display = "block"; hide.innerHTML = '<a href="javascript:hide_float_right()">Tắt quảng cáo [X]</a>'; }
        else { content.style.display = "none"; hide.innerHTML = '<a href="javascript:hide_float_right()">Xem quảng cáo...</a>';
    }
}
</script>
<style>
    .float-ck { position: fixed; bottom: 0px; z-index: 9000}
    * html .float-ck {position:absolute;bottom:auto;top:expression(eval (document.documentElement.scrollTop+document.docum entElement.clientHeight-this.offsetHeight-(parseInt(this.currentStyle.marginTop,10)||0)-(parseInt(this.currentStyle.marginBottom,10)||0))) ;}


</style>
<div class="float-ck" style="left: 0px" >

    <div id="float_content_right">
        <!-- Start quang cao-->
        <a href="http://bittuot.blogspot.com/" taget="_blank" ><img src="./image/support3.png" width="219" height="143"></a>
        <!-- End quang cao -->
    </div>
</div>














</body></html> 
<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
    'js' => array(
		'//themes/front/js/player/jwplayer.js',
		'//themes/front/js/jquery-ui.min.js',
		'//themes/front/js/jquery.bxslider.js',
		'//themes/front/js/jquery.mCustomScrollbar.js',
		'//themes/front/js/yt_libs.js',
		'//themes/front/js/videos.js',			
	),
     'css' => array(
     	'//themes/front/css/reset.css',
     	'//themes/front/css/jquery.mCustomScrollbar.css',
     	'//themes/front/css/style.css',
     	'//themes/front/css/responsive.css',
     	'//themes/front/css/style1.css',
     ),
);
